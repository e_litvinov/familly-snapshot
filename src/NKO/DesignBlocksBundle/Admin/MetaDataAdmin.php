<?php

namespace NKO\DesignBlocksBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MetaDataAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code',null, array('attr' => array(
                'readonly' => true,
            )))
            ->add('title')
            ->add('meta_description',  TextareaType::class, array('required' => false))
            ->add('meta_keywords', TextType::class);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('title')
            ->add('meta_description', 'html')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array()
                )));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('title')
            ->add('meta_description', 'html')
            ->add('meta_keywords');

    }
}