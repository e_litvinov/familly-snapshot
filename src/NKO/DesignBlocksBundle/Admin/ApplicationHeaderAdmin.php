<?php

namespace NKO\DesignBlocksBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ApplicationHeaderAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $types = $this->getConfigurationPool()->getContainer()
            ->get('nko.list_application')
            ->getApplicationsClass();

        $formMapper
            ->add('title', TextType::class)
            ->add('content', CKEditorType::class)
            ->add('tabs', CollectionType::class, array(
                'entry_type' => TextType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
                ))
            ->add('applicationClass', ChoiceType::class, array(
                'translation_domain' => 'NKOOrderBundle',
                'choices' => $types));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('content', 'html');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('content', 'html')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'delete'=>array(),
                    'edit' => array()
                )));
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Изменение заголовка формы заявки', $object->getTitle());
    }
}
