<?php

namespace NKO\DesignBlocksBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class BlockAdmin extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, [
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('value', CKEditorType::class);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('value', 'html')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array()
                )));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('value', 'html');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $choiceOptions = array( 'Footer'=>'footer', 'Main Page' => "main_page");
        $datagridMapper
            ->add('category', null, array(), 'choice', array(
                'choices' => $choiceOptions
            ))
        ;
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Редактирование текстового блока на главной', $object->getTitle());
    }
}
