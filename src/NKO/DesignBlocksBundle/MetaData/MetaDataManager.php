<?php

namespace NKO\DesignBlocksBundle\MetaData;

use Doctrine\ORM\EntityManager;

class MetaDataManager
{
    /**
     *
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
         $this->em = $entityManager;
    }

    public function getTitle($code){
        $object = $this->em->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(array('code' => $code));
        if($object){
            return $object->getTitle();
        }

    }

    public function getKeywords($code){
        $object = $this->em->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(array('code' => $code));
        if($object){
            return $object->getMetaKeywords();
        }

    }

    public function getDescription($code){
        $object = $this->em->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(array('code' => $code));
        if($object){
            return $object->getMetaDescription();
        }

    }

}