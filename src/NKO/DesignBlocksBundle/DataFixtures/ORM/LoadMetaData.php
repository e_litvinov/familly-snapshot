<?php

namespace NKO\DesignBlocksBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\DesignBlocksBundle\Entity\MetaData;

class LoadMetaData implements FixtureInterface
{
    private function createMainPageData(ObjectManager &$manager, $code, $title, $metaDescription, $metaKeywords, $name)
    {
        $data = $manager
            ->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(array('code' => $code));
        if(!$data) {
            $data = new MetaData();
            $data->setCode($code);
            $data->setName($name);
            $data->setTitle($title);
            $data->setMetaDescription($metaDescription);
            $data->setMetaKeywords($metaKeywords);
            $data->setTitle($title);

            $manager->persist($data);
        }
    }


    public function load(ObjectManager $manager)
    {
        $this->createMainPageData($manager,'fond_news', 'default_title', 'default_meta_description', 'default_meta_keywords', 'Новости');
        $this->createMainPageData($manager,'methodical_materials', 'default_title', 'default_meta_description', 'default_meta_keywords', 'Методические материалы');
        $this->createMainPageData($manager,'digest', 'default_title', 'default_meta_description', 'default_meta_keywords', 'Дайджест');
        $this->createMainPageData($manager,'information_materials', 'default_title', 'default_meta_description', 'default_meta_keywords', 'Информационные материалы');
        $this->createMainPageData($manager,'default', 'Победители конкурса «Семейный фарватер» в 2016 году', 'default_meta_description', 'default_meta_keywords', 'По умолчанию');
        $manager->flush();
    }
}