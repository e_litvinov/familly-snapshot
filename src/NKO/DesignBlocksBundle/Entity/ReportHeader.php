<?php

namespace NKO\DesignBlocksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="header_report")
 */

class ReportHeader extends BaseHeader
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", mappedBy="reportHeader")
     */
    private $reportForms;

    public function __construct()
    {
        $this->reportForms = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return ReportHeader
     */
    public function addReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms[] = $reportForm;
        $reportForm->setReportHeader($this);
        return $this;
    }

    /**
     * Remove reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     */
    public function removeReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms->removeElement($reportForm);
        $reportForm->setReportHeader(null);
    }

    /**
     * Get reportForms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportForms()
    {
        return $this->reportForms;
    }
}
