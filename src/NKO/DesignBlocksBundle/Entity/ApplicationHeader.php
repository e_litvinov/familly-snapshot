<?php

namespace NKO\DesignBlocksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="header_application")
 */

class ApplicationHeader extends BaseHeader
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="application_class", type="string")
     */
    private $applicationClass;

    /**
     * Set applicationClass
     *
     * @param string $applicationClass
     *
     */
    public function setApplicationClass($applicationClass)
    {
        $this->applicationClass = $applicationClass;

        return $this;
    }

    /**
     * Get applicationClass
     *
     * @return string
     */
    public function getApplicationClass()
    {
        return $this->applicationClass;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

