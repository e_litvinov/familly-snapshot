<?php

namespace NKO\DesignBlocksBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ApplicationHeaderAdminController extends CRUDController
{
    public function deleteAction($id)
    {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $applicationClass = $this->admin->getObject($id)->getApplicationClass();

        $em = $this->getDoctrine()->getManager();
        $applications = $em->getRepository($applicationClass)->findAll();

        if ($applications) {
            $this->addFlash('sonata_flash_error', 'applications using this headers');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        return parent::deleteAction($id);
    }
}
