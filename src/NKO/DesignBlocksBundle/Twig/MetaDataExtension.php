<?php

namespace NKO\DesignBlocksBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MetaDataExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('meta_data', array($this,'metaDataFilter') ),
        );
    }

    public function metaDataFilter($type, $code)
    {
        if($type == 'title'){
            return $this->container
                ->get('design_block.meta_data_manager')
                ->getTitle($code);
        }
        elseif($type == 'keywords'){
            return $this->container
                ->get('design_block.meta_data_manager')
                ->getKeywords($code);
        }
        elseif ($type == 'description'){
            return $this->container
                ->get('design_block.meta_data_manager')
                ->getDescription($code);
        }
        else{
            return  null;
        }
    }


    public function getName()
    {
        return 'meta_data_extension';
    }

}