<?php
namespace NKO\DesignBlocksBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\DesignBlocksBundle\Entity\ReportHeader;
use Doctrine\ORM\EntityManager;
use NKO\DesignBlocksBundle\Entity\ApplicationHeader;

class ReportHeaderExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('instruction', array($this,'instructionFilter') ),
            new \Twig_SimpleFilter('tabs', array($this,'tabsFilter') ),
            new \Twig_SimpleFilter('tabs_application', array($this,'tabsApplicationFilter') ),
            new \Twig_SimpleFilter('instruction_application', array($this,'instructionApplicationFilter') ),
        );
    }

    public function instructionFilter($report)
    {
        $reportHeader = $report->getReportForm()->getReportHeader();
        if (($reportHeader) && ($reportHeader->getTitle() != null)) {
            return $reportHeader->getContent();
        }

        $reportForm = $report->getReportForm();

        $em = $this->container->get('doctrine');
        $reportFormId = $em->getRepository(ReportForm::class)
            ->find($reportForm->getId());

        $reportHeader = $reportFormId->getReportHeader();

        if ($reportHeader) {
            $content = $em->getRepository(ReportHeader::class)
                ->find($reportHeader->getId())->getContent();
            return $content;
        }
        return null;
    }

    public function tabsFilter($report)
    {
        $reportHeader = $report->getReportForm()->getReportHeader();
        if (($reportHeader) && ($reportHeader->getTitle() != null)) {
            return $reportHeader->getTabs();
        }

        $reportForm = $report->getReportForm();

        $em = $this->container->get('doctrine');
        $reportFormId = $em->getRepository(ReportForm::class)
            ->find($reportForm->getId());

        $reportHeader = $reportFormId->getReportHeader();

        if ($reportHeader) {
            $tabs = $em->getRepository(ReportHeader::class)
                ->find($reportHeader->getId())->getTabs();
            return $tabs;
        }
        return null;
    }

    public function tabsApplicationFilter($application)
    {
        $applicationClass = $application->getCompetition()->getApplicationClass();
        if ($applicationClass) {
            $em = $this->container->get('doctrine');
            return $em->getRepository(ApplicationHeader::class)
                ->findOneByApplicationClass($applicationClass)->getTabs();
        }
    }

    public function instructionApplicationFilter($application)
    {
        $applicationClass = $application->getCompetition()->getApplicationClass();
        if ($applicationClass) {
            $em = $this->container->get('doctrine');
            return $em->getRepository(ApplicationHeader::class)
                ->findOneByApplicationClass($applicationClass)->getContent();
        }
    }
}
