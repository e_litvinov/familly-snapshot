<?php

namespace NKO\DesignBlocksBundle\Twig;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class DesignBlocksExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    private $fixtures_array = array();

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('BLOCK', array($this,'dictionaryFilter') ),
        );
    }

    public function dictionaryFilter($code)
    {
        if(empty($this->fixtures_array)){
            $this->fixtures_array = $this->container
                ->get('doctrine')
                ->getManager()
                ->getRepository('NKODesignBlocksBundle:Block')
                ->findAll();
        }
        foreach($this->fixtures_array as $fixture){
            if($fixture->getCode() == $code){
                 return $fixture->getValue();
            }
        }
        return  null;
    }

    public function getName()
    {
        return 'main_page_extension';
    }
}