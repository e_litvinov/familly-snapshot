<?php

namespace NKO\OrderBundle\Loader\Report\AnalyticReport\Report2019;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Effectiveness as ReportEffectiveness;

class SecondStageApplicationLoader
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * ContinuationApplicationLoader constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $object Report
     * @param $relatedObject Application
     */
    public function load($object, $relatedObject)
    {
        $object->setDirectorName($relatedObject->getHeadOfOrganizationFullName());
        $object->setDirectorPosition($relatedObject->getHeadOfOrganizationPosition());
        $object->setAccountantName($relatedObject->getHeadOfAccountingFullName());

        $object->getPracticeImplementation()->setPurpose($relatedObject->getProjectPurpose());
        $object->getPracticeIntroduction()->setPurpose($relatedObject->getIntroductionProjectPurpose());

        $object->setExpectedMonitoringResults($relatedObject->getMonitoringProjectPurpose());

        $relatedObject->getExpectedResults()->map(function (DirectResult $result) use ($object) {
            $object->addExpectedAnalyticResult($this->generateDirectResult($result));
        });

        $relatedObject->getPracticeResults()->map(function (DirectResult $result) use ($object) {
            $object->addPracticeAnalyticResult($this->generateDirectResult($result));
        });

        $relatedObject->getEffectivenessImplementationEtcItems()->map(function (Effectiveness $result) use ($object) {
            $object->addImplementationSocialResult($this->generateEffectiveness($result));
        });

        $relatedObject->getEffectivenessDisseminationEtcItems()->map(function (Effectiveness $result) use ($object) {
            $object->addIntroductionSocialResult($this->generateEffectiveness($result));
        });
    }

    protected function generateDirectResult(DirectResult $result)
    {
        $newResult = new DirectResult();
        $this->entityManager->persist($newResult);

        $newResult->setService($result->getService());
        $newResult->setTargetGroup($result->getProblem()->getTargetGroup());
        $newResult->setIndicator($result->getLocality());
        $newResult->setServicePlanValue($result->getServicePlanValue());

        return $newResult;
    }

    protected function generateEffectiveness(Effectiveness $result)
    {
        $newResult = new ReportEffectiveness();
        $this->entityManager->persist($newResult);

        $newResult->setResult($result->getResult());
        $newResult->setCustomIndicator($result->getCustomIndicator());
        $newResult->setPlanValue($result->getPlanValue());

        return $newResult;
    }
}
