<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 18.7.18
 * Time: 14.28
 */

namespace NKO\OrderBundle\Loader\Report\AnalyticReport\Report2018;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Resolver\TransferSocialResultsResolver;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;

class FarvaterApplicationLoader implements LoaderInterface
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    private $relatedCompetitionResolver;

    private $transferSocialResultResolver;

    /**
     * FarvaterApplicationLoader constructor.
     * @param EntityManager $entityManager
     * @param RelatedCompetitionResolver $relatedCompetitionResolver
     * @param TransferSocialResultsResolver $transferSocialResultResolver
     */
    public function __construct(
        EntityManager $entityManager,
        RelatedCompetitionResolver $relatedCompetitionResolver,
        TransferSocialResultsResolver $transferSocialResultResolver)
    {
        $this->entityManager = $entityManager;
        $this->relatedCompetitionResolver = $relatedCompetitionResolver;
        $this->transferSocialResultResolver = $transferSocialResultResolver;
    }

    /**
     * @param $object Report
     * @param $relatedObject Application
     */
    public function load($object, $relatedObject)
    {
        $object->getPracticeImplementation()->setPurpose($relatedObject->getProjectPurpose());
        $object->getPracticeIntroduction()->setPurpose($relatedObject->getIntroductionProjectPurpose());
        $this->loadDataFromBriefApplication($object, $relatedObject);
        $object->setExpectedAnalyticResults($this->createDirectResults($relatedObject->getProjectDirectResults()));
        $object->setPracticeAnalyticResults($this->createDirectResults($relatedObject->getPracticeDirectResults()));
        $object->setPracticeAnalyticIndividualResults($this->createDirectResults($relatedObject->getProjectDirectIndividualResults()));
        $object->setExpectedAnalyticIndividualResults($this->createDirectResults($relatedObject->getPracticeDirectIndividualResults()));

        $this->transferSocialResultResolver->createSocialResults(['ProjectSocialResults', 'ProjectSocialIndividualResults',
            'SocialResult'], $object, $relatedObject);
        $this->transferSocialResultResolver->createSocialResults(['PracticeSocialResults', null,
            'SocialResult'], $object, $relatedObject);

    }

    /**
     * @param $object Report
     * @param $relatedObject Application
     */
    public function update($object, $relatedObject)
    {
        $object->getPracticeImplementation()->setPurpose($relatedObject->getProjectPurpose());
        $object->getPracticeIntroduction()->setPurpose($relatedObject->getIntroductionProjectPurpose());
        $this->loadDataFromBriefApplication($object, $relatedObject);
        $object->setExpectedAnalyticResults($this->addMissingDirectResults($object, $relatedObject->getProjectDirectResults(),'expectedAnalyticResults'));
        $object->setPracticeAnalyticResults($this->addMissingDirectResults($object, $relatedObject->getPracticeDirectResults(),'practiceAnalyticResults'));
        $object->setPracticeAnalyticIndividualResults($this->addMissingDirectResults($object, $relatedObject->getProjectDirectIndividualResults(), 'practiceAnalyticIndividualResults'));
        $object->setExpectedAnalyticIndividualResults($this->addMissingDirectResults($object, $relatedObject->getPracticeDirectIndividualResults(), 'expectedAnalyticIndividualResults'));
    }

    public function loadDataFromBriefApplication($object, $relatedObject)
    {
        $relatedApplication = $this->relatedCompetitionResolver->getApplicationHistory($relatedObject);
        if ($relatedApplication) {
            $relatedApplication = unserialize($relatedApplication->getData());
            $object->setDirectorName($relatedApplication->getHeadOfOrganizationFullName());
            $object->setDirectorPosition($relatedApplication->getHeadOfOrganizationPosition());
            $object->setAccountantName($relatedApplication->getHeadOfAccountingFullName());
            $object->setProjectName($relatedApplication->getProjectName());
        }
    }

    /**
     * @param $results Collection
     *
     * @return array
     */
    private function createDirectResults($results)
    {
        /**@var  DirectResult $result*/
        $newResults = [];
        foreach ($results as $result) {
            $newResults[] = $this->createDirectResult($result);
        }

        return $newResults;
    }

    /**
     * @param $object Report
     * @param $results Collection
     * @param $type string
     *
     * @return array
     */
    private function addMissingDirectResults($object, $results, $type)
    {
        /**@var  DirectResult $result*/
        /**@var  Report $object*/
        $newResults = [];
        foreach($results as $result) {
            if(!$object->isExistResult($result,$type, "linkedFarvaterResult")) {
                $newResults[] = $this->createDirectResult($result);
            }
        }

        return $newResults;
    }


    /**
     * @param $result Report
     *
     * @return DirectResult
     */
    private function createDirectResult($result)
    {
        $newResult = new DirectResult();
        $newResult->setLinkedFarvaterResult($result);
        $newResult->setIsFeedback(true);

        $this->entityManager->persist($newResult);

        return $newResult;
    }
}