<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.7.18
 * Time: 16.51
 */

namespace NKO\OrderBundle\Loader\Report\AnalyticReport\Report2018;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Resolver\CacheResolver;
use Sonata\AdminBundle\Admin\Pool;

class MonitoringReportLoader implements LoaderInterface
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var CacheResolver $cache
     */
    private $cache;

    /**
     * @var Pool $cache
     */
    private $adminPool;

    /**
     * LoaderInterface constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, CacheResolver $cache, Pool $adminPool)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
        $this->adminPool = $adminPool;
    }

    /**
     * @param $object Report
     * @param $relatedObject  MonitoringReport
     */
    public function load($object, $relatedObject)
    {
        $amount = $relatedObject->getAmountInMonitoringReport();

        if (!$amount) {
            return;
        }

        for ($i = 0; $i < $amount; $i++) {
            $object->addFactorAttachment(new FactorAttachment());
        }
    }


    /**
     * @param $object Report
     * @param $relatedObject  MonitoringReport
     */
    public function update($object, $relatedObject)
    {
        $amount = $relatedObject->getAmountInMonitoringReport() - count($object->getFactorAttachments());
        if ($amount > 0) {
            for ($i = 0; $i < $amount; $i++) {
                $object->addFactorAttachment(new FactorAttachment());
            }
        } elseif ($amount < 0) {
            $collection = $object->getFactorAttachments();
            while ($amount < 0) {
                $collection->removeElement($collection->last());
                $amount++;
            }
        }

        $admin = $this->adminPool->getAdminByClass(get_class($object));
        $this->cache->generateItem($admin, $object->getId());

        $this->cache->delete();
    }
}
