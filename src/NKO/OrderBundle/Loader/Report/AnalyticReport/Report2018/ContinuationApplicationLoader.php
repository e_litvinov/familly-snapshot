<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 9.7.18
 * Time: 14.36
 */

namespace NKO\OrderBundle\Loader\Report\AnalyticReport\Report2018;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Continuation\Application;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Resolver\TransferSocialResultsResolver;

class ContinuationApplicationLoader implements LoaderInterface
{
    const FIELDS = [
        'expectedAnalyticResults' => 'reportExpectedResult',
        'practiceAnalyticResults' => 'reportPracticeResult'
    ];


    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    private $transferSocialResultResolver;

    /**
     * ContinuationApplicationLoader constructor.
     * @param EntityManager $entityManager
     * @param TransferSocialResultsResolver $transferSocialResultResolver
     */
    public function __construct(EntityManager $entityManager, TransferSocialResultsResolver $transferSocialResultResolver)
    {
        $this->entityManager = $entityManager;
        $this->transferSocialResultResolver = $transferSocialResultResolver;
    }

    /**
     * @param $object Report
     * @param $relatedObject Application
     */
    public function load($object, $relatedObject)
    {
        $object->getPracticeImplementation()->setPurpose($relatedObject->getProjectPurpose());
        $object->getPracticeIntroduction()->setPurpose($relatedObject->getIntroductionProjectPurpose());
        $object->setExpectedAnalyticResults($this->createDirectResults($relatedObject->getExpectedResults()));
        $object->setPracticeAnalyticResults($this->createDirectResults($relatedObject->getPracticeResults()));
        $this->transferSocialResultResolver->createSocialResults(['EffectivenessImplementationItems', 'EffectivenessImplementationEtcItems',
            'Result'], $object, $relatedObject);
        $this->transferSocialResultResolver->createSocialResults(['EffectivenessDisseminationItems', 'EffectivenessDisseminationEtcItems',
            'Result'], $object, $relatedObject);
        $this->getOrganizationInfo($object, $relatedObject);
    }

    /**
     * @param $object Report
     * @param $relatedObject Application
     */
    public function update($object, $relatedObject)
    {
        $object->getPracticeImplementation()->setPurpose($relatedObject->getProjectPurpose());
        $object->getPracticeIntroduction()->setPurpose($relatedObject->getIntroductionProjectPurpose());
        $object->setExpectedAnalyticResults($this->addMissingDirectResults($object, $relatedObject->getExpectedResults(), 'expectedAnalyticResults'));
        $object->setPracticeAnalyticResults($this->addMissingDirectResults($object, $relatedObject->getPracticeResults(), 'practiceAnalyticResults'));

        $this->removeExtraResults($object, $relatedObject, $relatedObject->getExpectedResults(), 'expectedAnalyticResults');
        $this->removeExtraResults($object, $relatedObject, $relatedObject->getPracticeResults(), 'practiceAnalyticResults');

        $this->transferSocialResultResolver->updateSocialResults(['EffectivenessImplementationItems', 'EffectivenessImplementationEtcItems',
            'Result'], $object, $relatedObject);
        $this->transferSocialResultResolver->updateSocialResults(['EffectivenessDisseminationItems', 'EffectivenessDisseminationEtcItems',
            'Result'], $object, $relatedObject);
        $this->getOrganizationInfo($object, $relatedObject);
    }

    /**
     * @param $result Report
     *
     * @return DirectResult
     */
    private function createDirectResult($result)
    {
        $newResult = new DirectResult();
        $newResult->setLinkedResult($result);
        $this->entityManager->persist($newResult);

        return $newResult;
    }

    /**
     * @param $results Collection
     *
     * @return array
     */
    private function createDirectResults($results)
    {
        /**@var  DirectResult $result*/
        $newResults = [];
        foreach ($results as $result) {
            $newResults[] = $this->createDirectResult($result);
        }

        return $newResults;
    }

    /**
     * @param $object Report
     * @param $results Collection
     * @param $type string
     *
     * @return array
     */
    private function addMissingDirectResults($object, $results, $type)
    {
        /**@var  DirectResult $result*/
        /**@var  Report $object*/
        $newResults = [];

        foreach ($results as $result) {
            if (!$object->isExistResult($result, $type, "linkedResult")) {
                $newResults[] = $this->createDirectResult($result);
            }
        }

        return $newResults;
    }

    private function getOrganizationInfo($object, $relatedObject)
    {
        $object->setDirectorName($relatedObject->getHeadOfOrganizationFullName());
        $object->setDirectorPosition($relatedObject->getHeadOfOrganizationPosition());
        $object->setAccountantName($relatedObject->getHeadOfAccountingFullName());
        $object->setProjectName($relatedObject->getProjectName());
    }

    public function removeExtraResults($object, $relatedObject, $results, $nameOfCollection)
    {
        $results = $this->entityManager
            ->createQueryBuilder()
            ->select('d')
            ->from(DirectResult::class, 'd')
            ->innerJoin('d.'.self::FIELDS[$nameOfCollection], 'r')
            ->innerJoin('r.reportForm', 'rf')
            ->innerJoin('r.author', 'a')
            ->where('d.linkedResult NOT IN (:results)')
            ->andWhere('r.author = :author')
            ->andWhere('rf.competition = :competition')
            ->setParameters([
                'results' => $results,
                'author' => $object->getAuthor(),
                'competition' => $relatedObject->getCompetition(),
            ])
            ->getQuery()
            ->getResult();

        $method = 'remove'.substr($nameOfCollection, 0, -1);
        if (method_exists($object, $method)) {
            foreach ($results as $result) {
                $object->$method($result);
            }
        }
    }
}
