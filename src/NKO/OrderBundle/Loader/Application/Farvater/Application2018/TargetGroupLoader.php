<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 6/26/18
 * Time: 2:46 PM
 */

namespace NKO\OrderBundle\Loader\Application\Farvater\Application2018;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedTargetGroup;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Application;
use NKO\OrderBundle\Traits\CollectionHandler;
use NKO\OrderBundle\Traits\Loader\FarvaterLoaderTrait;

class TargetGroupLoader implements LoaderInterface
{
    use CollectionHandler;
    use FarvaterLoaderTrait;

    /**
     * @var EntityManager $em
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $application Application
     * @param $relatedApplication BriefApplication
     */
    public function load($application, $relatedApplication)
    {
        $relatedTargetGroups = $this->getRelatedTargetGroups($relatedApplication);

        foreach ($relatedTargetGroups as $value) {
            $this->entityManager->persist($this->createObject(RelatedTargetGroup::class, $value, $application));
        }
    }

    public function update($application, $relatedApplication)
    {
        $relatedTargetGroups = $this->getRelatedTargetGroups($relatedApplication);
        $targetGroups = $this->entityManager->getRepository(RelatedTargetGroup::class)->findBy([
            'projectApplication' => $application
        ]);

        $this->updateCollection($targetGroups, $relatedTargetGroups, RelatedTargetGroup::class, $application, false);
    }

    /**
     * @param $relatedApplication BriefApplication
     * @return array|null
     */
    private function getRelatedTargetGroups($relatedApplication)
    {
        $beneficiaryTargetGroups = $this->modifyToStringArray($relatedApplication->getPeopleCategories());
        $relatedTargetGroups = $this->modifyToStringArray($relatedApplication->getOtherBeneficiaryGroups(), $beneficiaryTargetGroups);

        return $relatedTargetGroups;
    }
}