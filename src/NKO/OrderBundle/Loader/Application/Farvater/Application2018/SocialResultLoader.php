<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 6/26/18
 * Time: 2:47 PM
 */

namespace NKO\OrderBundle\Loader\Application\Farvater\Application2018;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication;
use NKO\OrderBundle\Traits\CollectionHandler;
use NKO\OrderBundle\Traits\Loader\FarvaterLoaderTrait;

class SocialResultLoader implements LoaderInterface
{
    use FarvaterLoaderTrait;
    use CollectionHandler;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * LoaderInterface constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $application Application
     * @param $relatedApplication BriefApplication
     */
    public function load($application, $relatedApplication)
    {
        $relatedSocialResults = $this->getRelatedSocialResults($relatedApplication);

        foreach ($relatedSocialResults as $value) {
            $this->entityManager->persist($this->createObject(RelatedSocialResult::class, $value, $application));
        }
    }

    public function update($application, $relatedApplication)
    {
        $relatedSocialResults = $this->getRelatedSocialResults($relatedApplication);
        $socialResults = $this->entityManager->getRepository(RelatedSocialResult::class)->findBy([
            'projectApplication' => $application
        ]);

        $this->updateCollection($socialResults, $relatedSocialResults, RelatedSocialResult::class, $application, false);
    }

    /**
     * @param $relatedApplication BriefApplication
     * @return array|null
     */
    private function getRelatedSocialResults($relatedApplication)
    {
        $relatedSocialResults = [];

        foreach ($relatedApplication->getBeneficiaryProblems() as $problem) {
            $relatedSocialResults = $this->modifyToStringArray($problem->getSocialResults(), $relatedSocialResults);
        }

        foreach ($relatedApplication->getBeneficiaryProblems() as $problem) {
            $title = $problem->getCustomSocialResult();
            if (array_search($title, $relatedSocialResults) === false && $title && $title != "-") {
                $relatedSocialResults[] = $title;
            }
        }

        return $relatedSocialResults;
    }
}