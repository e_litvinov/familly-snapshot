<?php

namespace NKO\OrderBundle\Loader\Application\Continuatuin\KNS;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Loader\LoaderInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Entity\Farvater2017\EmploymentRelationship;

class ContinuationLoader implements LoaderInterface
{
    const TEXT_FIELDS = [
        'abbreviation', 'legalPostCode', 'legalCity', 'legalStreet', 'isAddressesEqual', 'mission',
        'actualPostCode', 'actualCity', 'actualStreet', 'dateRegistrationOfOrganization', 'email',
        'phoneCode', 'phone', 'headOfOrganizationFullName', 'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode', 'headOfOrganizationPhone', 'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone', 'headOfOrganizationEmeil', 'headOfProjectFullName',
        'headOfProjectPosition', 'headOfProjectPhoneCode', 'headOfProjectPhone', 'countVolunteerEmployees',
        'headOfProjectMobilePhoneCode', 'headOfProjectMobilePhone', 'headOfProjectEmeil',
        'headOfAccountingFullName', 'headOfAccountingPhoneCode', 'headOfAccountingPhone',
        'headOfAccountingMobilePhoneCode', 'headOfAccountingMobilePhone', 'linkToAnnualReport',
        'headOfAccountingEmeil', 'primaryActivity',  'countStaffEmployees', 'countInvolvingEmployees',

        'iNN', 'kPP', 'oKPO', 'oKTMO', 'oKVED', 'kBK', 'bankName', 'bankLocation',
        'bankINN', 'bankKPP', 'correspondentAccount', 'bIK', 'paymentAccount',
        'personalAccount', 'nameAddressee',

        'projectName', 'knowledgeIntroductionDuringProjectImplementation',
        'projectPurpose', 'knowledgeIntroductionAfterProjectImplementation',
        'projectRelevance', 'priorityDirectionEtc',

        'organizationForm', 'legalRegion', 'actualRegion'
    ];

    const COLLECTION_FIELDS = [
        'siteLinks',
        'socialNetworkLinks',
        'publications',
        'risks',
        'projectMembers',
        'projectPartners',
        'organizationResources',
        'trainingGrounds',
        'projects'
    ];

    const FINANCING_SOURCE_TYPE ='financingSourceTypes';

    private $em;
    private $accessor;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function load($object, $relatedObject)
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $this->loadFields(self::TEXT_FIELDS, 'loadTextFields', $object, $relatedObject);
        $this->loadFields(self::COLLECTION_FIELDS, 'loadCollectionFields', $object, $relatedObject);
    }

    public function update($object, $relatedObject)
    {
    }

    private function loadFields($fields, $method, $object, $relatedObject)
    {
        foreach ($fields as $property) {
            if ($this->accessor->isWritable($object, $property)) {
                $value = $this->accessor->getValue($relatedObject, $property);
                if (method_exists($this, $method)) {
                    call_user_func_array([$this, $method], [$property, $value, $object]);
                }
            }
        }
    }

    private function loadTextFields($property, $value, $object)
    {
        if (is_object($value) && !$value instanceof \DateTime) {
            $value = $this->em->getRepository(get_class($value))->find($value);
        }

        $this->accessor->setValue($object, $property, $value);
    }

    private function loadCollectionFields($property, $value, $object)
    {
        foreach ($value as $item) {
            if ($item instanceof TrainingGround) {
                $this->addItemToCollection($object, $property, $item);
                continue;
            }

            if ($item instanceof Project) {
                $item = $this->em->getRepository(get_class($item))->find($item);
            }

            $class = get_class($item);
            $newInstance = new $class;
            $rowProperties = array_keys($this->getProperties($newInstance));
            $this->addItemToCollection($object, $property, $newInstance, true);
            $this->loadFields($rowProperties, 'loadEmbeddedFields', $newInstance, $item);
            $this->em->persist($newInstance);
        }
    }

    private function loadEmbeddedFields($property, $value, $object)
    {
        //тектовые поля вложенных админ-классов
        if (!is_object($value)) {
            $this->accessor->setValue($object, $property, $value);
        }

        //ManyToOne
        if ($value instanceof EmploymentRelationship) {
            $this->accessor->setValue($object, $property, $this->em->getRepository(get_class($value))->find($value));
        }

        //ManyToMany
        if ($property === self::FINANCING_SOURCE_TYPE) {
            foreach ($value as $item) {
                $this->addItemToCollection($object, $property, $item);
            }
        }
    }

    private function addItemToCollection($object, $method, $item, $notFetch = null)
    {
        $method = 'add'. substr($method, 0, -1);
        if (method_exists($object, $method)) {
            $item = $notFetch ? $item : $this->em->getRepository(get_class($item))->find($item);
            call_user_func([$object, $method], $item);
        }
    }

    private function getProperties($object)
    {
        return (new \ReflectionClass($object))->getDefaultProperties();
    }
}
