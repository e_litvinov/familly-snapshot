<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 2.10.18
 * Time: 12.36
 */

namespace NKO\OrderBundle\Loader\Application\Continuatuin\SecondStage;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use NKO\OrderBundle\Entity\Farvater2017\ProjectPartner as OldProjectPartner;
use NKO\OrderBundle\Entity\Application\Continuation\Team;
use NKO\OrderBundle\Entity\Farvater2017\ProjectMember;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as RelatedApplication;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication;

class FarvaterLoader implements LoaderInterface
{
    /** @var EntityManager $em */
    private $em;

    /** @var ContainerAwareInterface $relatedCompetitionResolver */
    private $relatedCompetitionResolver;

    public function __construct(EntityManager $em, ContainerAwareInterface $container)
    {
        $this->em = $em;
        $this->relatedCompetitionResolver= $container;
    }

    /**
     * @param  $object Application
     * @param $relatedObject RelatedApplication
     */
    public function load($object, $relatedObject)
    {
        $briefApplicationHistory = $this->relatedCompetitionResolver->getApplicationHistory($relatedObject, true);
        /** @var BriefApplication $briefApplication */
        $briefApplication = $briefApplicationHistory ? unserialize($briefApplicationHistory->getData()) : null;

        $object->setBriefPracticeDescription($briefApplication->getBriefPracticeDescription());
        $object->setProjectPurpose($relatedObject->getProjectPurpose());
        $object->setIntroductionProjectPurpose($relatedObject->getIntroductionProjectPurpose());
        $object->setMonitoringProjectPurpose($relatedObject->getMonitoringProjectPurpose());

        if ($briefApplication && $briefApplication->getPriorityDirection()) {
            //Связываем сущность с em, чтобы em не подумал, что это новая сущность
            $direction = $this->em->find(PriorityDirection::class, $briefApplication->getPriorityDirection()->getId());
            $object->setPriorityDirection($direction);
        }

        if ($relatedObject->getProjectMembers()->count()) {
            $object->removeProjectTeam($object->getProjectTeams()->first());
        }
        /** @var ProjectMember $projectTeam */
        foreach ($relatedObject->getProjectMembers() as $projectTeam) {
            $newProjectTeam = new Team();
            $newProjectTeam->setName($projectTeam->getFullName());
            $newProjectTeam->setDescription($projectTeam->getBriefInformation());
            $newProjectTeam->setResponsibility($projectTeam->getFunctionalResponsibilities());
            $newProjectTeam->setRole($projectTeam->getRole());

            $this->em->persist($newProjectTeam);
            $object->addProjectTeam($newProjectTeam);
        }

        if ($relatedObject->getProjectPartners()->count()) {
            $object->removeProjectContPartner($object->getProjectContPartners()->first());
        }
        /** @var OldProjectPartner $projectContPartner */
        foreach ($relatedObject->getProjectPartners() as $projectContPartner) {
            $newPartner = new ProjectPartner();

            $newPartner->setOrganizationName($projectContPartner->getOrganizationName());
            $newPartner->setParticipation($projectContPartner->getProjectParticipation());
            $newPartner->setBriefDescription($projectContPartner->getBriefInformation());

            $this->em->persist($newPartner);
            $object->addProjectContPartner($newPartner);
        }
    }

    public function update($object, $relatedObject)
    {
        // TODO: Implement update() method.
    }
}
