<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 2.10.18
 * Time: 12.35
 */

namespace NKO\OrderBundle\Loader\Application\Continuatuin\SecondStage;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Continuation\LaborRelationship;
use NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application;
use NKO\OrderBundle\Entity\Application\Continuation\Application as RelatedApplication;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Entity\Application\Continuation\Team;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use NKO\OrderBundle\Loader\LoaderInterface;
use NKO\OrderBundle\Entity\Competition;

class ContinuationLoader implements LoaderInterface
{
    /** @var EntityManager $em */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param  $object Application
     * @param $relatedObject RelatedApplication
     */
    public function load($object, $relatedObject)
    {
        $competition = $this->em->getRepository(Competition::class)->findOneBy([
            'applicationClass' => BriefApplication::class
        ]);

        /** @var BriefApplication $briefApplication */
        $briefApplicationHistory = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
            'isSend' => true,
            'author' => $relatedObject->getAuthor(),
            'competition' => $competition,
        ]);
        $briefApplication = $briefApplicationHistory ? unserialize($briefApplicationHistory->getData()) : null;

        $object->setBriefPracticeDescription($relatedObject->getBriefPracticeDescription());
        $object->setProjectPurpose($relatedObject->getProjectPurpose());
        $object->setIntroductionProjectPurpose($relatedObject->getIntroductionProjectPurpose());
        $object->setMonitoringProjectPurpose($relatedObject->getMonitoringProjectPurpose());

        if ($briefApplication && $briefApplication->getPriorityDirection()) {
            //Связываем сущность с em, чтобы em не подумал, что это новая
            $direction = $this->em->find(PriorityDirection::class, $briefApplication->getPriorityDirection()->getId());
            $object->setPriorityDirection($direction);
        }

        if ($relatedObject->getProjectTeams()->count()) {
            $object->removeProjectTeam($object->getProjectTeams()->first());
        }
        /** @var Team $projectTeam */
        foreach ($relatedObject->getProjectTeams() as $projectTeam) {
            $newProjectTeam = new Team();
            $newProjectTeam->setName($projectTeam->getName());
            $newProjectTeam->setDescription($projectTeam->getDescription());
            $newProjectTeam->setIsNew($projectTeam->getIsNew());
            $newProjectTeam->setResponsibility($projectTeam->getResponsibility());
            $newProjectTeam->setRole($projectTeam->getRole());

            //Связываем сущность с em, чтобы em не подумал, что это новая сущность
            $this->em->find(LaborRelationship::class, $projectTeam->getRelationship())->addTeam($newProjectTeam);

            $this->em->persist($newProjectTeam);
            $object->addProjectTeam($newProjectTeam);
        }

        if ($relatedObject->getProjectContPartners()->count()) {
            $object->removeProjectContPartner($object->getProjectContPartners()->first());
        }
        /** @var ProjectPartner $projectContPartner */
        foreach ($relatedObject->getProjectContPartners() as $projectContPartner) {
            $newPartner = new ProjectPartner();

            $newPartner->setIsNew($projectContPartner->getIsNew());
            $newPartner->setOrganizationName($projectContPartner->getOrganizationName());
            $newPartner->setParticipation($projectContPartner->getParticipation());
            $newPartner->setBriefDescription($projectContPartner->getBriefDescription());

            $this->em->persist($newPartner);
            $object->addProjectContPartner($newPartner);
        }
    }

    public function update($object, $relatedObject)
    {
    }
}
