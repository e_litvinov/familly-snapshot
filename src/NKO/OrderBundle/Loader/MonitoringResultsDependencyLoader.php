<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 12/18/17
 * Time: 6:27 PM
 */

namespace NKO\OrderBundle\Loader;

use NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;

class MonitoringResultsDependencyLoader
{
    const BORDER_INDEX_NUMBER = 13;
    const REQUIRED_INDEX_NUMBER = 16;
    const PARENT_INDICATOR_INDEX_NUMBER = 15;
    const CHILD_INDICATOR_INDEX_NUMBER = 1;

    public function load($monitoringResults, $targetReport)
    {
        $this->loadMonitoringResults($monitoringResults, $targetReport);
        $this->loadAttachments($targetReport);
    }

    public function loadMonitoringResults($monitoringResults, $targetReport)
    {
        $validMonitoringResults = $monitoringResults->filter(function ($result) {
            if ($result->getIndicator()->getCode() == IndicatorInterface::CUSTOM_VALUE
                && $result->getIndicator()->getTitle()) {
                return true;
            }

            $periodResult = $result->getFinalPeriodResult()->first();
            if ($periodResult) {
                if ($periodResult->getTotalAmount() || $periodResult->getNewAmount()) {
                    return true;
                }
            }
            return false;
        });

        $practiceMonitoringResults = $this->getPracticeMonitoringResults($validMonitoringResults);

        $targetReport->setPracticeMonitoringResults(null);
        foreach ($practiceMonitoringResults as $result) {
            $targetReport->addPracticeMonitoringResult($result);
        }

        $introductionMonitoringResults = $this->getIntroductionMonitoringResults($validMonitoringResults);

        $targetReport->setIntroductionMonitoringResults(null);
        foreach ($introductionMonitoringResults as $result) {
            $targetReport->addIntroductionMonitoringResult($result);
        }

        return $targetReport;
    }

    private function getPracticeMonitoringResults($results)
    {
        return $results->filter(function ($result) {
            $parent = $result->getIndicator()->getParent();
            if (!$parent) {
                return false;
            }

            $index = $result->getIndicator()->getIndexNumber();
            $parentIndex = $parent->getIndexNumber();
            if ($parentIndex) {
                return $parentIndex < self::BORDER_INDEX_NUMBER;
            }
            return ($index < self::BORDER_INDEX_NUMBER || $index >= self::REQUIRED_INDEX_NUMBER)  && $index != null;
        });
    }

    private function getIntroductionMonitoringResults($results)
    {
        return $results->filter(function ($result) {
            $parent = $result->getIndicator()->getParent();
            if (!$parent) {
                return false;
            }

            $index = $result->getIndicator()->getIndexNumber();
            $parentIndex = $parent->getIndexNumber();
            if ($parentIndex) {
                return $parentIndex >= self::BORDER_INDEX_NUMBER;
            }
            return $index >= self::BORDER_INDEX_NUMBER;
        });
    }

    private function loadAttachments($report)
    {
        $totalRowsNumber = $this->getRowNumber($report->getIntroductionMonitoringResults());
        $existedRowsNumber = $report->getFactorAttachments()->count();

        for(; $existedRowsNumber < $totalRowsNumber; $existedRowsNumber++) {
            $report->addFactorAttachment(new FactorAttachment());
        }
    }

    private function getRowNumber($monitoringResults)
    {
        if (!$monitoringResults) {
            return null;
        }

        $resultArray = array_filter($monitoringResults, function ($result) {
            $indicator = $result->getIndicator();
            if ($indicator->getIndexNumber() == self::CHILD_INDICATOR_INDEX_NUMBER && $indicator->getParent()->getIndexNumber() == self::PARENT_INDICATOR_INDEX_NUMBER) {
                return true;
            }
            return false;
        });

        if (!$resultArray) {
            return null;
        }

        $result = reset($resultArray);
        return $result->getFinalPeriodResult()->first()->getNewAmount();
    }
}