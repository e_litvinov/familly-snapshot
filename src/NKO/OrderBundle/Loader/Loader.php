<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 9.7.18
 * Time: 16.31
 */

namespace NKO\OrderBundle\Loader;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Continuation\Application;
use NKO\OrderBundle\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use NKO\OrderBundle\Utils\Loader as LoaderUtils;

class Loader implements LoaderInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * LoaderInterface constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function load($object, $relatedObject)
    {
        $loaderName = key_exists(get_class($object), LoaderUtils::LOADER_CLASS_NAME)
            ? LoaderUtils::LOADER_CLASS_NAME[get_class($object)]
            : '\\Loader';

        $serviceName = get_class($relatedObject).$loaderName;
        if ($this->container->has($serviceName)) {
            $service = $this->container->get($serviceName);
            $service->load($object, $relatedObject);
        }
    }

    public function update($object, $relatedObject)
    {
        $loaderName = key_exists(get_class($object), LoaderUtils::LOADER_CLASS_NAME)
            ? LoaderUtils::LOADER_CLASS_NAME[get_class($object)]
            : '\\Loader';
        $serviceName = get_class($relatedObject).$loaderName;
        if ($this->container->has($serviceName)) {
            $service = $this->container->get($serviceName);
            $service->update($object, $relatedObject);
        }
    }
}
