<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/8/18
 * Time: 5:04 PM
 */

namespace NKO\OrderBundle\Loader\Effectiveness;

use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication2018;

class BriefApplication2018EffectivenessLoader extends EffectivenessLoader implements EffectivenessLoaderInterface
{
    const EFFECTIVENESS_ITEMS = 'addEffectivenessItem';
    /**
     * @param BriefApplication2018 $application
     */
    public function load($application)
    {
        $this->loadByCode($application, self::EFFECTIVENESS_ITEMS,IndicatorInterface::PRACTICE_IMPL_CODE);
    }
}