<?php

namespace NKO\OrderBundle\Loader\Effectiveness;

use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

class EffectivenessLoader
{
    private $em;

    /**
     * @var string $fieldName
     */
    private $fieldName;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $application
     * @param $fieldName
     * @param $code
     */
    public function loadByCode($application, $fieldName, $code)
    {
        $this->fieldName = $fieldName;
        $indicators = $this->createEffectiveness($application, $code);
        if ($indicators) {
            foreach ($indicators as $indicator) {
                if (!isset ($currentIndicator) || ($currentIndicator !== $indicator->getParent())) {
                    $this->createEffectiveness($application, $code, $currentIndicator = $indicator->getParent());
                }
                $this->createEffectiveness($application, $code, $indicator);
            }
        }
    }

    private function createEffectiveness(BaseApplication $application, $code, $currentIndicator = null)
    {
        if (!$currentIndicator) {
            $indicators = $this->em->getRepository(Indicator::class)->findByCodeWithoutCustom($code);
            return $indicators;
        } else {
            $effect = new Effectiveness();
            $effect->setIndicator($currentIndicator);
            $effect->setIndicatorValue(0);
            $effect->setFactValue(0);
            $effect->setPlanValue(0);
            $application->{$this->fieldName}($effect);
        }
    }
}