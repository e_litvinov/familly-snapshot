<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/8/18
 * Time: 5:04 PM
 */

namespace NKO\OrderBundle\Loader\Effectiveness;

use NKO\OrderBundle\Entity\Application\Continuation\Application;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;

class ContinuationEffectivenessLoader extends EffectivenessLoader implements EffectivenessLoaderInterface
{
    const EFFECTIVENESS_IMPLEMENTATION_ITEMS = 'addEffectivenessImplementationItem';
    const EFFECTIVENESS_DISSEMINATION_ITEMS = 'addEffectivenessDisseminationItem';
    /**
     * @param Application $application
     */
    public function load($application)
    {
        $this->loadByCode($application, self::EFFECTIVENESS_IMPLEMENTATION_ITEMS,IndicatorInterface::PRACTICE_IMPL_CODE);
        $this->loadByCode($application, self::EFFECTIVENESS_DISSEMINATION_ITEMS,IndicatorInterface::PRACTICE_DISS_CODE);
    }
}