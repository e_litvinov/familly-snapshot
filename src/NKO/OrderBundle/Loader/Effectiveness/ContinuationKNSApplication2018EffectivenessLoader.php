<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.12.18
 * Time: 9.41
 */

namespace NKO\OrderBundle\Loader\Effectiveness;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Effectiveness;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;

class ContinuationKNSApplication2018EffectivenessLoader implements EffectivenessLoaderInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function load($application)
    {
        $indicators = $this->em->getRepository(Indicator::class)->findBy([
            'code' => Farvater2018ApplicationUtils::PROJECT_SOCIAL_RESULT_INDICATOR_CODE
        ]);

        foreach ($indicators as $indicator) {
            $effect = new Effectiveness();
            $effect->setIndicator($indicator);
            $effect->setFactValue(0);
            $effect->setPlanValue(0);
            $effect->setComment('-');
            $application->addEffectivenessKNSItem($effect);
        }
    }
}
