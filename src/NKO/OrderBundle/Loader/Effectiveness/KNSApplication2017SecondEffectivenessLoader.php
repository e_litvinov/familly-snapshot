<?php

namespace NKO\OrderBundle\Loader\Effectiveness;

use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness;

class KNSApplication2017SecondEffectivenessLoader extends KNSSecondApplicationEffectivenessLoader implements EffectivenessLoaderInterface
{
    public function load($application)
    {
        foreach ($this->getIndicators() as $indicator) {
            $effect = new Effectiveness();
            $effect->setIndicator($indicator);
            $effect->setFactValue(0);
            $effect->setPlanValue(0);
            $effect->setComment('-');
            $application->addEffectivenessItem($effect);
        }
    }
}
