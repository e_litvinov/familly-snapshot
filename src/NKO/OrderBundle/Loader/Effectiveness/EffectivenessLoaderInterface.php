<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/8/18
 * Time: 5:33 PM
 */

namespace NKO\OrderBundle\Loader\Effectiveness;

use NKO\OrderBundle\Entity\BaseApplication;

interface EffectivenessLoaderInterface
{
    /**
     * @param BaseApplication $application
     * @return mixed
     */
    public function load($application);
}