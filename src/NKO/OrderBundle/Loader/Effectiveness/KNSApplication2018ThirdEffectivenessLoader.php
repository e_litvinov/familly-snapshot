<?php


namespace NKO\OrderBundle\Loader\Effectiveness;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Effectiveness;
use NKO\OrderBundle\Utils\Application\KNS2018ApplicationUtils;

class KNSApplication2018ThirdEffectivenessLoader implements EffectivenessLoaderInterface
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function load($application)
    {
        $indicators = $this->em->getRepository(Indicator::class)->findBy([
            'code' => KNS2018ApplicationUtils::EFFECTIVENESS_INDICATOR_KNS_2018_THIRD_STAGE
        ]);

        foreach ($indicators as $indicator) {
            $effect = new Effectiveness();
            $effect->setIndicator($indicator);
            $effect->setComment('-');
            $application->addEffectivenessKNSItem($effect);
        }
    }
}
