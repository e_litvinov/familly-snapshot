<?php

namespace NKO\OrderBundle\Loader\Effectiveness;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;

class KNSSecondApplicationEffectivenessLoader
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function getIndicators()
    {
        $indicators = $this->em->getRepository(Indicator::class)->findByEffectivenessIndicatorKNSSecondStage(IndicatorInterface::PRACTICE_IMPL_CODE);

        uasort($indicators, function ($a, $b) {
            if ($a->getId() == $b->getId()) {
                return 0;
            }
            return ($a->getId() < $b->getId()) ? -1 : 1;
        });

        return $indicators;
    }
}
