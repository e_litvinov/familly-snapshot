<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 6/26/18
 * Time: 2:43 PM
 */

namespace NKO\OrderBundle\Loader;

interface LoaderInterface
{
    public function load($object, $relatedObject);

    public function update($object, $relatedObject);
}