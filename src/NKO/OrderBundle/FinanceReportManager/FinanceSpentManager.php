<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 6/1/18
 * Time: 12:33 PM
 */

namespace NKO\OrderBundle\FinanceReportManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Entity\ExpenseTypeInterface;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceSpent;
use NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate;

class FinanceSpentManager
{
    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createFinanceSpentByExpenseTypes($object, $expenseTypes)
    {
        foreach ($expenseTypes as $expenseType) {
            $financeSpent = new FinanceSpent();
            $financeSpent->setExpenseType($expenseType);
            $object->addFinanceSpent($financeSpent);
        }

        return $object;
    }

    public function updateFinanceSpentByTemplate(ReportTemplate $template)
    {
        $reports = $template->getReports();

        if (!$reports) {
            return;
        }

        foreach ($reports as $report) {
            $this->updateReportFinanceSpentByTemplate($report, $template);
        }
    }

    public function updateReportFinanceSpentByTemplate(BaseReport $report, ReportTemplate $template)
    {
        $financeSpent = $template->getFinanceSpent();

        foreach ($report->getFinanceSpent() as $finance) {
            $expenseType = $finance->getExpenseType();
            $finObject = $financeSpent->filter(function ($object) use ($expenseType) {
                return $expenseType->getCode() == $object->getExpenseType()->getCode();
            });
            $finance->setApprovedSum($finObject->first()->getApprovedSum());
        }
    }

    public function updateReportBalance($reports)
    {
        if (!$reports) {
            return;
        }

        foreach ($reports as $report) {
            $this->updateBalance($report);
        }
    }

    public function updateBalance(BaseReport $report)
    {
        foreach ($report->getFinanceSpent() as $finance) {
            $balance = $finance->getApprovedSum() - $finance->getIncrementalCosts() - $finance->getPeriodCosts();
            $finance->setBalance(round($balance, 2));
        }
    }

    public function updateIncrementalCosts(BaseReport $currentReport, BaseReport $report)
    {
        $currentFinances = $currentReport->getFinanceSpent();
        $finances = $report->getFinanceSpent();
        foreach ($currentFinances as $currentFinance) {
            $currentExpenseType = $currentFinance->getExpenseType();
            $finance = $finances->filter(
                function ($object) use ($currentExpenseType) {
                    return ($object->getExpenseType() === $currentExpenseType);
                }
            )->first();

            $incrementalCosts = $currentFinance->getIncrementalCosts() + $currentFinance->getPeriodCosts();
            $finance->setIncrementalCosts(round($incrementalCosts, 2));
        }
    }

    public function updateTotalValues($reports)
    {
        if (!$reports) {
            return;
        }

        foreach ($reports as $report) {
            $receiptOfDonations = null;
            $incrementalCosts = null;
            $donationBalance = null;
            foreach ($report->getFinanceSpent() as $finance) {
                if ($finance->getExpenseType()->getCode() != ExpenseTypeInterface::SALARY_CODE) {
                    $receiptOfDonations += $finance->getApprovedSum();
                    $incrementalCosts += $finance->getIncrementalCosts() + $finance->getPeriodCosts();
                    $donationBalance += $finance->getBalance();
                }
            }

            $report->setReceiptOfDonations(round($receiptOfDonations, 2));
            $report->setIncrementalCosts(round($incrementalCosts, 2));
            $report->setDonationBalance(round($donationBalance, 2));
        }
    }
}
