/**
 *  Код для дублирования значений из таблиц Целевые группы и Иные группы
 *  в таблицу Основные проблемы целевых групп
 */

var checkedCategoriesCounter = {},
    needToClearRow = {},
    items = {},
    $table = {},
    column = {},
    chooseOnlyChild;

function init($object, objectField, key, paramChoose) {
    $table[key] = $object;
    column[key] = objectField;
    items[key] = {};
    checkedCategoriesCounter[key] = 0;
    needToClearRow[key] = 0;
    chooseOnlyChild = paramChoose;
}

/**
 * Метод для работы с чекбоксами списка peopleCategories (Целевые группы)
 * Используется для добавления/удаления выбранных значений списка peopleCategories
 * в таблицу problems (Основные проблемы целевых групп)
 */
function addCheckboxesHandler(selector, type) {
    var $checkbox = $("input[type='"+type+"'][id*='"+selector+"']");

    $checkbox.each(function(){
        if($(this).attr("checked")) {
            var currentVal = $(this).val();
            if (chooseOnlyChild && !($(this).closest("li").find("ul:first").find("li").length === 0)) {
                return;
            }
            items[selector][currentVal] = $(this).closest("label").find("span").text();
        }
    });

    $checkbox.on("ifChecked", function() {
        var selectedOption = $(this).closest("label").find("span").text(),
            currentVal = $(this).val();

        if (chooseOnlyChild && !($(this).closest("li").find("ul:first").find("li").length === 0)) {
            return;
        }

        if (Object.values(items[selector]).indexOf(selectedOption) === -1) {
            items[selector][currentVal] = selectedOption;
            addNewRow(selector);
            addToTable(selectedOption, selector);
            checkedCategoriesCounter[selector]++;
        }
    });

    $checkbox.on("ifUnchecked", function() {
        var selectedOption = $(this).closest("label").find("span").text(),
            currentVal = $(this).val();

        if (chooseOnlyChild && !($(this).closest("li").find("ul:first").find("li").length === 0)) {
            return;
        }

        if ($(this).closest("ul").siblings("div").find('span').text() != selectedOption) {
            checkedCategoriesCounter[selector]--;
            needToClearRow[selector] = (checkedCategoriesCounter[selector] === 0 && $table[selector].find("tr:not(:hidden)").length === 1);
            removeRow(selectedOption, selector);
            delete items[selector][currentVal];
        }
        clearRows(selector);
    });
}

function addNewRow(key) {
    if($table[key].find("tr:not(:hidden) textarea[id$='" + column[key] +"']").length < Object.keys(items[key]).length) {
        $table[key].find("tr:not(:hidden):last #add_btn").trigger("click");
    }
}

function addToTable(item, key) {
    var id = $table[key].find("tr:not(:hidden):last [id$='" + column[key] +"']").attr("id").split("_")[2];
    $table[key].find("tr:not(:hidden):last").find('select').select2('data', null);

    if(needToClearRow[key])
        $table[key].find("[id$='" + column[key] +"']").val(item);
    else {
        $table[key].find("[id$='"+ id + column[key] +"']").val(item);
    }
}

function changeRowValue(oldValue, newValue, key) {
    $table[key].find("input, textarea").each(function () {
        if($(this).val() === oldValue) {
            $(this).val(newValue);
        }
    });
}

function removeRow(value, key) {
    $table[key].find("input, textarea").each(function () {
        if($(this).val() === value && !needToClearRow[key] && $table[key].find("tr:not(:hidden)").length > 2) {
            $(this).closest("tr").find("#delete_btn").trigger("click", false);
            return true;
        }
        else if(($(this).val() === value && needToClearRow[key]) || $table[key].find("tr:not(:hidden)").length <= 2) {
            needToClearRow[key] = false;
        }
    });
}

function addSpecialInputHandler(input, inputId, fieldKey) {
    $(input).each(function () {
        var id = $(this).attr("id").split("_")[2],
            key = "other_" + id;

        if ($(this).val() !== '')
            items[fieldKey][key] = $(this).val();
    });

    $('body').on('click', inputId + " #delete_btn", function () {
        var $input = $(this).closest("tr").find(input),
            id = $input.attr("id").split("_")[2],
            key = "other_" + id;
        
        if (Object.keys(items[fieldKey]).length === 1) {
            needToClearRow[fieldKey] = true;
            removeRow(items[fieldKey][Object.keys(items[fieldKey])[0]], fieldKey);
            items[fieldKey] = {};
        }
        else {
            removeRow(items[fieldKey][key], fieldKey);
            delete items[fieldKey][key];
        }
    });


    $('body').on('change', input, function () {
        var id = $(this).attr("id").split("_")[2],
            key = "other_" + id;

        if (!(key in items[fieldKey])) {
            items[fieldKey][key] = $(this).val();
            addNewRow(fieldKey);
            addToTable($(this).val(), fieldKey);
        }
        else {
            if ($(this).val() === "" && $table[fieldKey].find("tr:not(:hidden)").length >= 1) {
                removeRow(items[fieldKey][key], fieldKey);
                delete items[fieldKey][key];
            }
            else {
                changeRowValue(items[fieldKey][key], $(this).val(), fieldKey);
                items[fieldKey][key] = $(this).val();
            }
        }
        clearRows(fieldKey);
    });
}

function clearRows(fieldKey) {
    if (Object.keys(items[fieldKey]).length == 0) {
        var row = $table[fieldKey].find("tr:not(:hidden)");
        row.find('select').select2('data', null);
        row.find('textarea').val('');

    }
}