$(document).ready(function() {
    var managerAutosave = new ManagerAutosave();
    var isAdmin = managerAutosave.init();
    if (!isAdmin) {
        managerAutosave.process();
    }
});

function ManagerAutosave() {
    var object = this,
        routes, initService, timers = {},
        checkbox = $("input[id='auto_saver_checkbox']"),
        ajaxFactory = new AjaxFactory(),
        form = 'form[role="form"]';

    BaseClass.apply(object);

    this.init = function()
    {
        if (object.checkAccess()) {
            return true;
        }

        initService = object.configurationPool.getService(InitService);
        initService.process();
        routes = InitService.options.routes;
    };

    this.process = function()
    {
        if ($.cookie('auto_saver') === undefined) {
            object.processCookie(true);
        }

        if (object.processCookie()) {
            object.setTimers();
            checkbox.iCheck('check');
        }

        checkbox.on('ifChanged', function (e) {
            object.processCookie(e.target.checked);

            if (e.target.checked) {
                object.setTimers();
                object.tuggleAutosave(true);
            } else {
                object.clearTimers();
                object.clearCache();
                object.tuggleAutosave(false);
            }
        });

        $(form).submit(function() {
            object.clearTimers();
            object.clearCache();
        });
    };

    this.checkAccess = function()
    {
        return $('input[id="is_admin_role"]').val();
    };

    this.setTimers = function()
    {
        timers['save'] = new Timer($.extend(
            object.generateTimerOptions('save', 10000), {})
        );

        timers['edit'] = new Timer($.extend(
            object.generateTimerOptions('edit', 420000), {
                'saveTimer' : timers['save']
            })
        );

        for (var key in timers) {
            timers[key].process();
        }
    };

    this.clearTimers = function()
    {
        for (var key in timers) {
            timers[key].clear();
        }
    };

    this.clearCache = function()
    {
        var func = ajaxFactory.get('clear');
        func({
            'url' : routes['clear'],
            'selector' : form
        });
    };

    this.tuggleAutosave = function(status)
    {
        var func = ajaxFactory.get('allowAutosave');
        func({
            'url' : routes['allowAutosave'],
            'selector' : form,
            'status' : status
        });
    };

    this.generateTimerOptions = function(action, time)
    {
        return {
            'ajaxFactory' : ajaxFactory,
            'selector' : form,
            'url' : routes[action],
            'action': action,
            'time' : time
        }
    };

    this.processCookie = function(option)
    {
        if (option === undefined) {
            return $.cookie('auto_saver') === 'true';
        }

        $.cookie('auto_saver', option);
    };
};

function Timer(options) {
    var timer;

    this.process = function()
    {
        var ajaxFactory = options['ajaxFactory'];
        var func = ajaxFactory.get(options['action']);

        timer = setInterval(function () {
            func(options);
        }, options['time']);
    };

    this.clear = function()
    {
        clearInterval(timer);
    };
};

function InitService(){
    var object = this,
        files = {};

    this.process = function()
    {
        object.createRoutes();
        object.detectFiles();
    };

    this.createRoutes = function()
    {
        var key,
            name = $('input[id$="_token"]')[0].name,
            currentUrl = object.getUrl(),
            actions = ['save', 'clear', 'edit', 'allowAutosave'];

        key = name.substring(0, name.indexOf("[_token]"));

        for (var i=0; i<actions.length; i++) {
            var actionKey = actions[i];
            InitService.options.routes[actionKey] = currentUrl + actionKey + '?uniqid=' + key;
        }
    };


    //ПЕРЕДЕЛАНО!!!! ПРИМЕНИТЬ К НОВОЙ ВЕРСИИ JS АВТОСОХРАНЕНИЯ!!!!!!!!!!!!!!!!!!!
    this.detectFiles = function(table)
    {
        //находим на изначальной форме загруженные файлы
        var selectors;
        if (table !== undefined) {
            selectors = table.find('div[class="itm_file_preview_ico_c"] a');
            files = Object.assign(files, this.findFiles(selectors));
        } else {
            selectors = $('div[class="itm_file_preview_ico_c"] a');
            files = this.findFiles(selectors);
        }

        InitService.options.files = '&' + $.param(files);
    };

    this.findFiles = function(selectors)
    {
        var href, path, id, nameField, tableName, arr = {};
        selectors.each( function() {
            href = $(this).attr('href');
            path = href.substring(href.lastIndexOf('/') + 1);
            id = $(this).parent().siblings('input').attr('id');

            nameField = id.substring(id.indexOf('_') + 1);
            tableName = nameField.substring(0, nameField.indexOf('_') );

            if (arr[tableName] === undefined) {
                arr[tableName] = {};
            }

            arr[tableName][nameField] = path;
        });

        return arr;
    };
    //---------------------------------------------------------------------------

    this.getUrl = function()
    {
        var href = $(location)[0].href;
        return href.substring(0, href.lastIndexOf('/') + 1);
    };
};


InitService.options = {
    routes : {},
    files : ""
};


function AjaxFactory(){
    var object = this;

    this.get = function(action)
    {
        if (action === undefined || object[action] === undefined) {
            return null;
        }

        return object[action];
    };

    this.allowAutosave = function(options)
    {
        if (options['url'] === undefined || options['url'] === undefined) {
            return;
        }

        $.ajax({
            url:     options['url'],
            data:    {status: options['status']},
            type:     "POST",
            dataType: "html",
            error: function(response) {
                console.log("Cache not cleared");
            }
        });
    };

    this.clear = function(options)
    {
        if (options['url'] === undefined) {
            return;
        }

        $.ajax({
            url:     options['url'],
            type:     "POST",
            dataType: "html",
            error: function(response) {
                console.log("Cache not cleared");
            }
        });
    };

    this.edit = function(options)
    {
        if (options['url'] === undefined || options['selector'] === undefined) {
            return;
        }

        if (options['saveTimer']) {
            options['saveTimer'].clear();
        }

        $(options['selector']).ajaxSubmit({
            data: {'autosaver' : true},
            url:     options['url'],
            type:     "POST",
            error: function(response) {
                console.log("Saving ended with an error ");
            },
            success: function(response) {
                options['saveTimer'].process();
            }
        });
    };

    this.save = function(options)
    {
        if (options['url'] === undefined || options['selector'] === undefined) {
            return;
        }

        $.ajax({
            url:     options['url'],
            type:     "POST",
            dataType: "html",
            data: $(options['selector']).serialize() + InitService.options.files,
            error: function(response) {
                console.log("Saving ended with an error ");
            }
        });
    };
};