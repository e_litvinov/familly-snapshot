'use strict';

$(document).ready(function ($) {

    var $addressEqualCheckbox = $('div[id*="_isAddressesEqual"] input[type="checkbox"]');

    var $legalInputs = $('div[id*="_legal"] input');
    var $actualInputs = $('div[id*="_actual"] input');

    var $legalRegionSelect =  $('div[id*="_legalRegion"] select');
    var $actualRegionSelect =  $('div[id*="_actualRegion"] select');

    var $legalFederalDistrictSelect =  $('div[id*="_legalFederalDistrict"] select');
    var $actualFederalDistrictSelect =  $('div[id*="_actualFederalDistrict"] select');

    var $actualAddrSelector = $('div[id*="_actual"]');
    var $isAddressEqualSelector = $('div[id*="_isAddressesEqual"]');


    //если checkbox checked, скрыть блок фактического адреса
    if($('div[id*="_isAddressesEqual"] .iCheck-helper').parent().hasClass('checked')) {
        $actualAddrSelector.hide();
        $("#_actual_span").hide();
    }

    /**
     * Метод обработки изменений значений инпутов юридического адреса.
     * При изменении и установленном чекбоксе значения копируются в поля фактического адреса
     */
    $legalInputs.change(function(){
        if($('div[id*="_isAddressesEqual"] .iCheck-helper').parent().hasClass('checked')) {
            //копирование полей юридеческого адреса в поля фактического
            insertValues();
        }
    });

    /**
     * Метод обработки анчека чекбокса
     * При анчеке поля фактического адреса очищаются и показываются
     */
    $addressEqualCheckbox.on('ifUnchecked', function() {
        $actualAddrSelector.each(function() {

            //выводить все элементы под заданным селктором кроме тех, что имеют класс modal
            if(!$(this).hasClass('modal')) {
                $(this).show();
            }
        });
        $("#_actual_span").show();
        $actualInputs.val('');
        $actualRegionSelect.val('');
        $actualRegionSelect.trigger('change.select2');
        
        $actualFederalDistrictSelect.val('');
        $actualFederalDistrictSelect.trigger('change.select2');


    });

    /**
     * Метод обработки чека чекбокса
     */
    $addressEqualCheckbox.on('ifChecked', function() {
        //копирование полей юридеческого адреса в поля фактического
        insertValues();

        $actualAddrSelector.hide();
        $("#_actual_span").hide();
    });

    /**
     * Обработка изменения select юридического региона
     */
    $legalRegionSelect.on('change', function (e) {
        if($('div[id*="_isAddressesEqual"] .iCheck-helper').parent().hasClass('checked')) {
            $actualRegionSelect.val(e.val);
            $actualRegionSelect.trigger('change.select2');
        }
        else{
            $legalRegionSelect.val(e.val);
        }
    });

    /**
     * Обработка изменения select юридического региона
     */
    $legalFederalDistrictSelect.on('change', function (e) {
        if($('div[id*="_isAddressesEqual"] .iCheck-helper').parent().hasClass('checked')) {
            $actualFederalDistrictSelect.val(e.val);
            $actualFederalDistrictSelect.trigger('change.select2');
        }
        else{
            $legalFederalDistrictSelect.val(e.val);
        }
    });

    /**
     * Функция для копирования значений из полей юридического адреса
     * в поля фактического адреса
     */
    function insertValues() {
        var values = [];
        $actualRegionSelect.val($legalRegionSelect.val());
        $actualRegionSelect.trigger('change.select2');
        
        $actualFederalDistrictSelect.val($legalFederalDistrictSelect.val());
        $actualFederalDistrictSelect.trigger('change.select2');

        $legalInputs.each(function() {

            //пропуск автосгенерированных инпутов
            if($(this).prop('id').indexOf('autogen') != -1
                || $(this).prop('id').indexOf('search') != -1){
                return true;
            }

            //push значений инпутов в массив
            values.push($(this).val());
        });

        //индекс для прохода по массиву значений полей юридического адреса
        var myInd = 0;
        $actualInputs.each(function(index) {

            //пропуск автосгенерированных инпутов
            if($(this).prop('id').indexOf('autogen') != -1
                || $(this).prop('id').indexOf('search') != -1){
                return true;
            }

            //вставка значений в инпуты фактического адреса
            $(this).val(values[myInd]);
            myInd++;
        });
    }

});