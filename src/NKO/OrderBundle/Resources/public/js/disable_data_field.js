'use strict';

function disableDateField() {
    $("input[class*='sonata-medium-date']").prop('readonly', true);
}