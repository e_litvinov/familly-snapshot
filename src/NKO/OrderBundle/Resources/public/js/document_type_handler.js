'use strict';

$(document).ready(function ($) {

    var url = Routing.generate('document_folder_type_path');
    var documentsFolderSelect = "select[id$='_folder']";
    var documentsTableSelect = "select[id*='documents'][id$='_expenseType']";
    var $documentsRowInputs = $("input[id$='_row']");
    var isDOMChangeHandled = true;
    var nameOfTypeField = $("[id*='documents'][id$='_expenseType']").length ? 'expenseType' : 'tableType';

    addTableHandler("div[id*='_documents']");

    function addTableHandler(selector) {
        addButtonClickEvent(selector);
        bindDOMSubtreeModified(selector);
    }

    /**
     * Обработка клика на кнопку добавления строки в таблицу
     * @param selector
     */
    function addButtonClickEvent(selector) {
        $('body').on('click', selector + " .sonata-ba-action", function() {
            isDOMChangeHandled = false;

            $("select[id*='_folder']").each(function(){

                var selectVal = $(this).val(),
                    id = $(this).attr('id'),
                    idRegEx = /_([0-9]+)_/,
                    idNum = id.match(idRegEx),
                    folderTypeId = $("body").find("select[id$='_documents_"+idNum[1]+"_document_type']").val();

                ajaxForFolderType(id, selectVal, folderTypeId);
            });
        });
    }

    /**
     * Функция, связывающая блок под селектором с функцией
     * обработки изменения содержимого этого блока
     * @param selector
     */
    function bindDOMSubtreeModified(selector) {
        $(selector).bind ("DOMSubtreeModified", function() {
            handleDOMChangeWithDelay(selector)
        });
    }

    /**
     * Обработка изменения блока с таблицей с задержкой
     * @param selector
     */
    function handleDOMChangeWithDelay (selector) {
        setTimeout (function() {
            handleDOMChange(selector)
        }, 1);
    }

    /**
     * Обработка изменения содержимого таблицы
     * @param selector
     */
    function handleDOMChange (selector) {
        if(!isDOMChangeHandled) {
            var val = $(selector).find(documentsFolderSelect).last().val(),
                id = $(selector).find(documentsFolderSelect).last().attr("id");

            ajaxForFolderType(id, val);

            //установка флага обработанного изменения DOM
            isDOMChangeHandled = true;
        }
    }

    $("body").on("change", documentsFolderSelect, function (e) {
        var selectVal = $(e.currentTarget).val(),
            id = $(this).attr('id');

        ajaxForFolderType(id, selectVal);
    });

    $documentsRowInputs.each(function(){
        var rowName = $(this).attr("name"),
            rowId = $(this).attr("id"),
            rowVal = $(this).val(),
            $documentsRowSelect,
            selectVal = $(this)
                            .closest('tr')
                            .find("select[id*='documents'][id$='_" + nameOfTypeField + "']")
                            .find('option:selected')
                            .text();

        $(this).closest("td").append("<select name='"+rowName+"' id='"+ rowId +"'></select>");

        $documentsRowSelect = $(this).closest("tr").find("select#"+rowId);
        $documentsRowSelect.select2({width : "130px"});

        if(nameOfTypeField === 'expenseType') {
            processDocumentRowSelect($documentsRowSelect, selectVal, rowVal);
        } else {
            $documentsRowSelect = processOldDocumentRowSelect($documentsRowSelect, selectVal, 0);

            $documentsRowSelect.val(rowVal);
            $documentsRowSelect.trigger('change');
        }

        $(this).remove();
    });

    $("body").on("change", documentsTableSelect, function (e) {
        var $documentsRowSelect = $(this).closest("tr").find("select[id$='_row']");
        var selectedText = $(this).find('option:selected').text();
        
        processDocumentRowSelect($documentsRowSelect, selectedText);
    });

    /**
     * Метод для получения списка типов документов по виду папки
     * @param id
     * @param select_val
     * @param folderTypeId
     */
    function ajaxForFolderType(id, selectVal, folderTypeId){
        var idRegEx = /_([0-9]+)_/,
            idNum = id.match(idRegEx),
            folderTypeSelect = "select[id$='_documents_"+idNum[1]+"_document_type']";

        $.ajax({
            url: url,
            type: 'post',
            data: selectVal,
            success: function (response) {

                $("body").find(folderTypeSelect).html('');

                //добавление опций в список типов документов
                for (var key in response) {
                    if (response.hasOwnProperty(key)) {
                        var newOption = new Option(response[key], key, true, true);
                        $("body").find(folderTypeSelect).append(newOption).trigger('change');
                    }
                }

                if(folderTypeId != undefined)
                    $("body").find(folderTypeSelect).val(folderTypeId).trigger('change');
            }
        });
    }

    function processDocumentRowSelect($documentsRowSelect, selectVal, rowVal) {
        var titleInput = $('textarea[id*="registers"][id$="expenseType_title"]');
        var rowsCount = 0;
        var newOption = new Option("", 0, true, true);

        titleInput.each(function(){
            if($(this).val() === selectVal) {
                rowsCount = $(this)
                    .closest('span[id^="field_widget"]:not([id$="expenseType"])')
                    .find('table tbody tr:last td:first').text();
            }
        });

        $documentsRowSelect.html('');
        $documentsRowSelect.append(newOption).trigger('change');

        for(var i = 1; i <= rowsCount; i++) {
            newOption = new Option(i, i, true, true);
            $documentsRowSelect.append(newOption).trigger('change');
        }

        $documentsRowSelect.val(rowVal || "0");
        $documentsRowSelect.trigger("change");
    }
});


function getSelectTextVal(selectVal) {
    var selectTextVal;

    switch(parseInt(selectVal)) {
        case 0:
            selectTextVal = 'transportCosts';
            break;
        case 1:
            selectTextVal = 'involvedCosts';
            break;
        case 2:
            selectTextVal = 'eventCosts';
            break;
        case 3:
            selectTextVal = 'otherCosts';
            break;
        default:
            selectTextVal = 'transportCosts';
    }

    return selectTextVal;
}

function processOldDocumentRowSelect($documentsRowSelect, selectVal, lenDif) {

    var selectTextVal = getSelectTextVal(selectVal),
        $tableRows = $("body").find("div[id*='" + selectTextVal + "'] table tbody tr").not(".rowHidden"),
        len = $tableRows.length - lenDif,
        currentAttr = $documentsRowSelect.find("option:selected").attr("cost_key");

    $documentsRowSelect.html('');

    var newOption = new Option("", 0, true, true);
    $documentsRowSelect.append(newOption).trigger('change');

    $tableRows.each(function(i){
        if(i == len)
            return false;
        newOption = new Option((i+1) + ". " + $(this).find("td:eq(1) textarea").val(), i+1, true, true);
        newOption.setAttribute("cost_key", $(this).find("input[id$='cost_key']").val());
        $documentsRowSelect.append(newOption).trigger('change');
    });

    $documentsRowSelect.val("0");
    $documentsRowSelect.find("option[cost_key='"+ currentAttr +"']").prop("selected", true);
    $documentsRowSelect.trigger("change");

    return $documentsRowSelect;
}

