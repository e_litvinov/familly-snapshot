$(document).ready(function ($) {
    var sumGrantInput = 'input[id*="_sumGrant"]';
    var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''};
    $(sumGrantInput).autoNumeric('init', autoNumericOptions);

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('specialistProblems').configs({
            'hideLastColumn' : true
        })
        .table('beneficiaryProblems').configs({
            'hideLastColumn' : true
        })
        .table('effectivenessImplementationItems').configs({
            'removeTracking' : true
        })
        .table('effectivenessDisseminationItems').configs({
            'removeTracking' : true
        });
    tableManager.process();

    listenProblems();
    autosize($('textarea'));
});