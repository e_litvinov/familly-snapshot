$(document).ready(function ($) {
    availableFieldAfterChoice (0, 'div[id*="_nameAddressee"]');
    autosize.update($("textarea[id*='resultCriteria']"));
    autosize($('textarea'));

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('publications').configs({
            'limit' : 5
        })
        .table('otherBeneficiaryGroups').configs({
            'limit' : 3
        })
        .table('projectResults').configs({
            'removeLastColumn' : true,
            'removeFirstColumn' : true
        });
    tableManager.process();


    var requestedFinancingMoney = 'input[id*="_requestedFinancingMoney"]';
    var cofinancingMoney = 'input[id*="_cofinancingMoney"]';
    var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''};
    $(requestedFinancingMoney).autoNumeric('init', autoNumericOptions);
    $(cofinancingMoney).autoNumeric('init', autoNumericOptions);
});

