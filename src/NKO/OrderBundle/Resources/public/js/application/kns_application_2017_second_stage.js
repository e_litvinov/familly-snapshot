$(document).ready(function ($) {
    activeRadioButton($('input[id*="_organizationForm"]'));
    availableFieldAfterChoice (0, 'div[id*="_nameAddressee"]');
    autosize($('textarea'));

    var $problems = $("body").find("span[id$='_beneficiaryProblems']"),
        tableColumn = '_targetGroup',
        otherBeneficiaryGroupsNameInput = "input[id*='_otherBeneficiaryGroups'][id$='_name']",
        inputId = "[id*='_otherBeneficiaryGroups']",
        checkboxFieldName = "peopleCategories";

    init($problems, tableColumn, checkboxFieldName);
    addSpecialInputHandler(otherBeneficiaryGroupsNameInput, inputId, checkboxFieldName);
    addCheckboxesHandler(checkboxFieldName, 'checkbox');

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('effectivenessItems').configs({
            'removeLastColumn' : true,
            'removeFirstColumn' : true
    })
        .table('beneficiaryProblems').configs({
            'hideLastColumn' : true
    })
        .table('organizationResources').configs({
            'removeLastColumn' : true,
            'removeFirstColumn' : true
    });
    tableManager.process();
});