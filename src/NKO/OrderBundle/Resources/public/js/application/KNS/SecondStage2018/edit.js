$(document).ready(function ($) {
    var budgetFields = [
        'input[id*="_entirePeriodEstimatedFinancing"]',
        'input[id*="_entirePeriodEstimatedCoFinancing"]',
        'input[id*="_firstYearEstimatedFinancing"]',
        'input[id*="_firstYearEstimatedCoFinancing"]',
    ];

    var autoNumericOptions = { lZero: 'keep', aSep: '', altDec: ',', mDec: "5"};
    setAutonumericForArrayOfSelectors(budgetFields, autoNumericOptions);
    setAutoNumericForValueInput()
});

function setAutonumericForArrayOfSelectors(fieldSelectors, autoNumericOptions) {
    fieldSelectors.forEach(function (item) {
        $(item).autoNumeric('init', autoNumericOptions).wrap('<div class="input-group">');
        $(item).parent().append('<span class="input-group-addon">тыс. руб.</span>');
    });
}

function setAutoNumericForValueInput() {
    var autoNumericOptions = { lZero: 'keep', aSep: '', eDec: "0", mDec: "0"};
    var valueFields = [
        'div[id$="individualSocialResults"]',
        'div[id$="effectivenessItems"]'
    ];
    valueFields.forEach(function (item) {
            $(item).find('input[id$="Value"]').each(function () {
                $(this).autoNumeric('init', autoNumericOptions)
            })
        }
    );
}

