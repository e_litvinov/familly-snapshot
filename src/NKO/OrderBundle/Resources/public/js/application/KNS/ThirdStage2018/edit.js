var DUPLICATES = {
    'projectName' : {
        'divSelectorFrom' : 'textarea[id$="projectName"]:last',
        'divSelectorTo' : 'input[id$="projectNameDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'priorityDirection' : {
        'divSelectorFrom' : 'trainingGrounds',
        'divSelectorTo' : 'input[id$="priorityDirectionDuplicate"]',
        'functionToRelay' : relayToTextFromRadioTree,
        'configs' : CONFIGS['radioTree'],
        'parameters' : {
            'separator' : "/"
        }
    },
    'deadLineStart' : {
        'divSelectorFrom' :
            'input[id$="deadLineStart"]',
        'divSelectorTo' : 'input[id$="deadLineStartDuplicate"]',
        'functionToRelay' : relayFromDateToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'deadLineFinish' : {
        'divSelectorFrom' : 'input[id$="deadLineFinish"]',
        'divSelectorTo' : 'input[id$="deadLineFinishDuplicate"]',
        'functionToRelay' : relayFromDateToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'sumGrantDuplicate' : {
        'divSelectorFrom' : 'input[id$="sumGrant"]',
        'divSelectorTo' : 'input[id$="sumGrantDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' :  CONFIGS['text'],
        'parameters' : {}
    },
    'headOfOrganization' : {
        'divSelectorFrom' : [
            'input[id$="headOfOrganizationFullName"]',
            'input[id$="headOfOrganizationPosition"]'
        ],
        'divSelectorTo' : 'input[id$="headOfOrganizationDuplicate"]',
        'functionToRelay' : relayToTextFromTexts,
        'configs' : CONFIGS['text'],
        'parameters' : {
            // 'separator' : " на должности: "
            'separator' : " , "
        }
    },
    'headOfAccounting' : {
        'divSelectorFrom' : 'input[id$="headOfAccountingFullName"]',
        'divSelectorTo' : 'input[id$="headOfAccountingDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'phone' : {
        'divSelectorFrom' : [
            'input[id$="phoneCode"]',
            'input[id$="phone"]'
        ],
        'divSelectorTo' : 'input[id$="phoneDuplicate"]',
        'functionToRelay' : relayToTextFromTexts,
        'configs' : CONFIGS['text'],
        'parameters' : {
            // 'separator' : " на должности: "
            'separator' : " ",
            'start' : "+7 "
        }
    },
    'email' : {
        'divSelectorFrom' : 'input[id$="_email"]',
        'divSelectorTo' : 'input[id$="emailDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
};

$(document).ready(function ($) {
    activeRadioButton($('input[id*="_organizationForm"]'));
    availableFieldAfterChoice (0, 'div[id*="_nameAddressee"]');
    autosize($('textarea'));

    processAllItems(DUPLICATES);

    var $problems = $("body").find("span[id$='_beneficiaryProblems']"),
        $effectivenessKNSItems = $("body").find("span[id$='_effectivenessKNSItems']"),
        tableColumn = '_targetGroup',
        otherBeneficiaryGroupsNameInput = "textarea[id*='_otherBeneficiaryGroups'][id$='_name']",
        inputId = "[id*='_otherBeneficiaryGroups']",
        checkboxFieldName = "peopleCategories";

    init($problems, tableColumn, checkboxFieldName);
    addSpecialInputHandler(otherBeneficiaryGroupsNameInput, inputId, checkboxFieldName);

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('effectivenessKNSItems').configs({
        'removeTracking' : true
    })
        .table('beneficiaryProblems').configs({
        'hideLastColumn' : true
    })
        .table('organizationResources').configs({
        'removeLastColumn' : true,
        'removeFirstColumn' : true
    });
    $problems.find('th:nth-child(2)').css('min-width', '300px');
    $effectivenessKNSItems.find('th:nth-child(1)').css('min-width', '300px');
    tableManager.process();

    var autoNumericOptions = { lZero: 'keep', aSep: '', eDec: "0", mDec: "0"};

    setAutoNumericForValueInput(autoNumericOptions);

    $('input[id$="sumGrant"]').autoNumeric('init', autoNumericOptions)
});

function setAutoNumericForValueInput(autoNumericOptions) {
    var valueFields = [
        'div[id$="individualSocialResults"]',
        'div[id$="effectivenessKNSItems"]'
    ];
    valueFields.forEach(function (item) {
            $(item).find('input[id$="Value"]').each(function () {
                $(this).autoNumeric('init', autoNumericOptions)
            })
        }
    );
}

