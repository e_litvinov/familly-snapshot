'use strict';

$(document).ready(function ($) {
    var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''},
        sumGrantInput = 'input[id$="_sumGrant"]';

    $(sumGrantInput).autoNumeric('init', autoNumericOptions);
    allowOnlyNumeric('input[id$="Value"]');
    listenProblems();

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('beneficiaryProblems').configs({
            'hideLastColumn' : true
        })
        .table('specialistProblems').configs({
            'hideLastColumn' : true
        })
        .table('expectedResults').configs({
            'transform' : function () {
                var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
                    th = arguments[0].find("thead tr:last th");

                $("<tr><th colspan='2'>Целевые значения</th></tr>").insertBefore(tr);
                this.prependWithRowspan(th.eq(5), tr);
                this.prependWithRowspan(th.eq(4), tr);
                this.prependWithRowspan(th.eq(2), tr);
                this.prependWithRowspan(th.eq(1), tr);
                this.prependWithRowspan(th.eq(0), tr);
                this.appendWithRowspan(th.eq(8), tr);
                this.appendWithRowspan(th.eq(10), tr);
            }
        })
        .table('practiceResults').configs({
            'transform' : function () {
                var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
                    th = arguments[0].find("thead tr:last th");

                $("<tr><th colspan='2'>Целевые значения</th></tr>").insertBefore(tr);
                this.prependWithRowspan(th.eq(6), tr);
                this.prependWithRowspan(th.eq(5), tr);
                this.prependWithRowspan(th.eq(3), tr);
                this.prependWithRowspan(th.eq(2), tr);
                this.prependWithRowspan(th.eq(1), tr);
                this.prependWithRowspan(th.eq(0), tr);
                this.appendWithRowspan(th.eq(9), tr);
                this.appendWithRowspan(th.eq(11), tr);
            }
        })

        .table('effectivenessImplementationEtcItems').configs({
            'transform' : addBaseTitle
        })
        .table('effectivenessDisseminationEtcItems').configs({
            'transform' : addBaseTitle
        })
    ;
    tableManager.process();


    function addBaseTitle(){
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $("<tr><th colspan='2'>Целевые значения</th></tr>").insertBefore(tr);
        this.prependWithRowspan(th.eq(2), tr);
        this.prependWithRowspan(th.eq(1), tr);
        this.prependWithRowspan(th.eq(0), tr);
        this.appendWithRowspan(th.eq(5), tr);
        this.appendWithRowspan(th.eq(7), tr);
    }
});