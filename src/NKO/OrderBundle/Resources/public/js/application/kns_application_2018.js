$(document).ready(function ($) {
    availableFieldAfterChoice (0, 'div[id*="_nameAddressee"]');
    autosize.update($("textarea[id*='resultCriteria']"));
    autosize($('textarea'));

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('publications').configs({
            'limit' : 5
        })
        .table('projectResults').configs({
            'removeLastColumn' : true,
            'removeFirstColumn' : true
        });
    tableManager.process();
});

