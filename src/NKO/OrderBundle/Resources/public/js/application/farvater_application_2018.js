$(document).ready(function ($) {
    var $specialistProblems = $('body').find("span[id$='_specialistProblems']"),
        tableColumn = '_targetGroup',
        otherSpecialistTargetGroup = "input[id*='_otherSpecialistTargetGroups'][id$='_name']",
        specialistInputId = "[id*='_otherSpecialistTargetGroups']",
        checkboxTargetGroupFieldName = 'specialistTargetGroups';

    init($specialistProblems, tableColumn, checkboxTargetGroupFieldName);
    addSpecialInputHandler(otherSpecialistTargetGroup, specialistInputId, checkboxTargetGroupFieldName);
    addCheckboxesHandler(checkboxTargetGroupFieldName, 'checkbox');

    var autoNumericOptions = { vMin: '0', mDec: "0", lZero: 'keep', aSep: ''};
    $('input[id*="_targetValue"]').autoNumeric('init', autoNumericOptions);
    $('input[id*="_approximateValue"]').autoNumeric('init', autoNumericOptions);
    $('input[id*="_firstYearTargetValue"]').autoNumeric('init', autoNumericOptions);
    $('input[id*="_secondYearTargetValue"]').autoNumeric('init', autoNumericOptions);
    $('input[id*="_thirdYearTargetValue"]').autoNumeric('init', autoNumericOptions);

    var $fieldsBudgetSelector = $('div[id*="_entirePeriodEstimatedFinancing"]').parent();
    $fieldsBudgetSelector.find('span').text('тыс. руб.');

    autosize($('textarea'));

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('otherSpecialistTargetGroups').configs({
            'limit' : 3
        })
        .table('specialistProblems').configs({
            'hideLastColumn' : true
        })
        .table('introductionPractices').configs({
            'removeLastColumn' : true
        })
        .table('organizationResources').configs({
            'removeLastColumn' : true
    });
    tableManager.process();
});