$(document).ready(function ($) {
    var budgetFields = [
        'input[id*="_firstYearEstimatedFinancing"]',
    ];

    var autoNumericOptions = { lZero: 'keep', aSep: '', altDec: ',', mDec: "5"};
    setAutonumericForArrayOfSelectors(budgetFields, autoNumericOptions);

    var tableManager = new TableManagerClass;
    tableManager.process();
});

function setAutonumericForArrayOfSelectors(fieldSelectors, autoNumericOptions) {
    fieldSelectors.forEach(function (item) {
        $(item).autoNumeric('init', autoNumericOptions).wrap('<div class="input-group">');
        $(item).parent().append('<span class="input-group-addon">тыс. руб.</span>');
    });
}