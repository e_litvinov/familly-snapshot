$(document).ready(function ($) {
    var $problems = $("body").find("span[id$='_problems']"),
        tableColumn = '_targetGroup',
        otherBeneficiaryGroupsNameInput = "input[id*='_otherBeneficiaryGroups'][id$='_name']",
        inputId = "[id*='_otherBeneficiaryGroups']",
        checkboxFieldName = "peopleCategories";

    init($problems, tableColumn);
    addCheckboxesHandler(checkboxFieldName);
    addSpecialInputHandler(otherBeneficiaryGroupsNameInput, inputId);

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('problems').configs({
            'hideLastColumn' : true
        })
        .table('socialResults').configs({
            'removeFirstColumn' : true
        })
        .table('financingSources').configs({
            'removeFirstColumn' : true,
            'removeLastColumn' : true
        })
        .table('projectResults').configs({
            'removeLastColumn' : true
        })
    ;
    tableManager.process();
});