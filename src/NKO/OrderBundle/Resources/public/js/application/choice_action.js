function availableFieldAfterChoice(fieldsChoice, targetField, fieldName) {
    var $targetField = $(targetField);

    if (fieldName === undefined) {
        fieldName = 'organizationForm';
    }

    $targetField.hide();
    if (fieldsChoice instanceof Array) {
        $.each(fieldsChoice, function(index, val){
            bindAction(val, fieldName, $targetField);
        });
    } else {
        bindAction(fieldsChoice, fieldName, $targetField);
    }
}

function activeRadioButton($selector) {
    var isChecked = false;

    $selector.each(function () {
        if ($(this).attr('checked')) {
            isChecked = true;
        }
    });

    if(isChecked === false){
        $selector.first().attr('checked', 'checked');
        $selector.first().parent().addClass('checked');
    }
}

function bindAction(val, fieldName, $targetField) {
    var $field = $('input[id$="_'+ fieldName +'_'+ val +'"]');
    showField($field, $targetField);

    $field.on("ifChecked", function() {
        $targetField.show();
    });

    $field.on("ifUnchecked", function(){
        $targetField.hide();
    });
}

function showField($field, $targetField) {
    if ($field.parent().hasClass('checked')) {
        $targetField.show();
    }
}