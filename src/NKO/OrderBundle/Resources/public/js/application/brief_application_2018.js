$(document).ready(function ($) {
    var $problems = $("body").find("span[id$='_beneficiaryProblems']"),
        tableColumn = '_targetGroup',
        otherBeneficiaryGroupsNameInput = "input[id*='_otherBeneficiaryGroups'][id$='_name']",
        inputId = "[id*='_otherBeneficiaryGroups']",
        checkboxFieldName = "peopleCategories";

    init($problems, tableColumn, checkboxFieldName);
    addSpecialInputHandler(otherBeneficiaryGroupsNameInput, inputId, checkboxFieldName);
    addCheckboxesHandler(checkboxFieldName, 'checkbox');

    activeRadioButton($('input[id*="_organizationForm"]'));
    activeRadioButton($('input[id*="_priorityDirection"]'));

    var autoNumericOptions = { vMin: '0', mDec: "0", lZero: 'keep', aSep: ''};
    var indicatorValue = 'input[id*="_indicatorValue"]';
    $(indicatorValue).autoNumeric('init', autoNumericOptions);
    autosize($('textarea'));

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('financingSources').configs({
            'removeLastColumn' : true
    })
        .table('beneficiaryProblems').configs({
            'hideLastColumn' : true
    })
        .table('effectivenessItems').configs({
            'removeTracking' : true
    });
    tableManager.process();
});