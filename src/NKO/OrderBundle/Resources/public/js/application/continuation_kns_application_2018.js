var DUPLICATES = {
    'authorNKOName' : {
        'divSelectorFrom' : 'input[id$="author__nko_name"]',
        'divSelectorTo' : 'input[id$="authorNKONameDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'projectName' : {
        'divSelectorFrom' : 'textarea[id$="projectName"]:last',
        'divSelectorTo' : 'input[id$="projectNameDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'priorityDirection' : {
        'divSelectorFrom' : 'trainingGrounds',
        'divSelectorTo' : 'input[id$="priorityDirectionDuplicate"]',
        'functionToRelay' : relayToTextFromRadioTree,
        'configs' : CONFIGS['radioTree'],
        'parameters' : {
            'separator' : "/"
        }
    },
    // 'deadline' : {
    //     'divSelectorFrom' : [
    //         'input[id$="deadLineStart"]',
    //         'input[id$="deadLineFinish"]'
    //     ],
    //     'divSelectorTo' : 'input[id$="deadlineDuplicate"]',
    //     'functionToRelay' : relayToTextFromTexts,
    //     'configs' : CONFIGS['radioTree'],
    //     'parameters' : {
    //         'separator' : " - "
    //     }
    // },
    'deadLineStart' : {
        'divSelectorFrom' :
            'input[id$="deadLineStart"]',
        'divSelectorTo' : 'input[id$="deadLineStartDuplicate"]',
        'functionToRelay' : relayFromDateToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'deadLineFinish' : {
        'divSelectorFrom' : 'input[id$="deadLineFinish"]',
        'divSelectorTo' : 'input[id$="deadLineFinishDuplicate"]',
        'functionToRelay' : relayFromDateToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'sumGrantDuplicate' : {
        'divSelectorFrom' : 'input[id$="sumGrant"]',
        'divSelectorTo' : 'input[id$="sumGrantDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' :  CONFIGS['text'],
        'parameters' : {}
    },
    'headOfOrganization' : {
        'divSelectorFrom' : [
            'input[id$="headOfOrganizationFullName"]',
            'input[id$="headOfOrganizationPosition"]'
        ],
        'divSelectorTo' : 'input[id$="headOfOrganizationDuplicate"]',
        'functionToRelay' : relayToTextFromTexts,
        'configs' : CONFIGS['text'],
        'parameters' : {
            // 'separator' : " на должности: "
            'separator' : " , "
        }
    },
    'headOfAccounting' : {
        'divSelectorFrom' : 'input[id$="headOfAccountingFullName"]',
        'divSelectorTo' : 'input[id$="headOfAccountingDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
    'phone' : {
        'divSelectorFrom' : [
            'input[id$="phoneCode"]',
            'input[id$="phone"]'
        ],
        'divSelectorTo' : 'input[id$="phoneDuplicate"]',
        'functionToRelay' : relayToTextFromTexts,
        'configs' : CONFIGS['text'],
        'parameters' : {
            // 'separator' : " на должности: "
            'separator' : " ",
            'start' : "+7 "
        }
    },
    'email' : {
        'divSelectorFrom' : 'input[id$="_email"]',
        'divSelectorTo' : 'input[id$="emailDuplicate"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
};

$(document).ready(function () {


    availableFieldAfterChoice([0,1], 'div[id*="_nameAddressee"]');

    var params = { vMin: 0, mDec: 2, aSep: '', lZero: 'keep'};
    var autoNumericOptions = { vMin: '0', mDec: "0", lZero: 'keep', aSep: ''};
    $('input[id*="_firstYearTargetValue"]').autoNumeric('init', autoNumericOptions);
    $('input[id*="_targetValue"]').autoNumeric('init', autoNumericOptions);
    $('input[id$="sumGrant"]').autoNumeric('init', params);

    autosize($('textarea'));
    processAllItems(DUPLICATES);

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('publications').configs({
            'limit' : 5
        })
        .table('effectivenessKNSItems').configs({
            'removeTracking' : true
        })
        .table('organizationResources').configs({
            'removeLastColumn' : true,
            'removeFirstColumn' : true
        })
    ;
    tableManager.process();
});