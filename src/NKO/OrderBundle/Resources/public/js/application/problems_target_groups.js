
function listenProblems() {
    var $problems = $("body").find("span[id$='_beneficiaryProblems']"),
        $specialistProblems = $('body').find("span[id$='_specialistProblems']"),
        tableColumn = '_targetGroup',

        otherBeneficiaryGroupsNameInput = "input[id*='_otherBeneficiaryGroups'][id$='_name']",
        otherSpecialistTargetGroup = "input[id*='_otherSpecialistTargetGroups'][id$='_name']",

        beneficiaryInputId = "[id*='_otherBeneficiaryGroups']",
        specialistInputId = "[id*='_otherSpecialistTargetGroups']",

        checkboxFieldName = "peopleCategories",
        checkboxTargetGroupFieldName = 'specialistTargetGroups';

    var $peopleCategoriesInput = $("input[id*='peopleCategories']");

    $peopleCategoriesInput.each(function(){
        if($(this).attr("checked")) {
            $(this).closest("li").find('ul:first').show();
        }
    });

    initialization($problems, tableColumn, checkboxFieldName, otherBeneficiaryGroupsNameInput, beneficiaryInputId);
    initialization($specialistProblems, tableColumn, checkboxTargetGroupFieldName, otherSpecialistTargetGroup, specialistInputId);
}

function initialization($problems, tableColumn, checkboxFieldName, groupsNameInput, inputId, paramChoose, isNeededHide) {
    init($problems, tableColumn, checkboxFieldName, paramChoose, isNeededHide);
    addSpecialInputHandler(groupsNameInput, inputId, checkboxFieldName);
    addCheckboxesHandler(checkboxFieldName, 'checkbox');
}