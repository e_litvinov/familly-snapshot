'use strict';

var isDOMChangeHandled = true;
var isMoreThanLimit = false;
var removeFirstColumn = true;
var removeLastColumn = true;
var transformation;
var changeSelectNewRow;
var preAddRow;




function buttonHandlers() {
    /**
     * Обработка клика на кнопку добавления новой записи в таблицу, если нет других записей
     */
    $('body').on('click', '#add_btn_0', function() {
        //показывает первую строку
        var $currentTr = $(this).siblings('div.sonata-ba-field').find('table tbody tr:first').show();

        //очищение областей ввода
        clearNewRow($currentTr.find('input'));
        clearNewRow($currentTr.find('textarea'));

        //инициализация iCheck
        $currentTr.find('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });

        //удаление лишнего блока
        $currentTr.find('div.icheckbox_square-blue').next('ins').remove();

        //если чекбокс был выделен, удаляем выделение и иницируем клик
        if($currentTr.find('div.icheckbox_square-blue').hasClass('checked')){
            $currentTr.find('div.icheckbox_square-blue').removeClass('checked');
            $currentTr.find('div.icheckbox_square-blue').trigger('click');
        }

        //переиндексация таблицы
        indexTable($currentTr);

        //смена id и name в соответсвии с порядком следования
        $currentTr.nextAll('tr').find('input').each( function() {
            changeIdAndName($(this));
        });

        //смена id и name в соответсвии с порядком следования
        $currentTr.nextAll('tr').find('textarea').each( function() {
            changeIdAndName($(this));
        });

        //удаление кнопки добавления
        $(this).remove();
    });

    /**
     * Обработка клика на кнопку добавления записи в таблицу
     */
    $('body').on('click', '#add_btn', function() {
        indexTable($(this));

        //в содержательном отчете КНС 18 публкаций может быть больше 5
        if (preAddRow !== undefined) {
            preAddRow();
        }
        // --------

        //не выполнять, если изменяемая таблица для данных о публикациях и количество записей больше/равно 5
        if((($(this).closest('div.form-group').attr('id').indexOf('_publications') !== -1 && !isMoreThanLimit)
                || ($(this).closest('div.form-group').attr('id').indexOf('_publications') === -1))
            && ((!$(this).closest('tr').find('input:first').attr('limit')) ||
                ($(this).closest('tr').find('input:first').attr('limit').length && !isMoreThanLimit))) {
            //поиск строки, в которой содержится кнопка
            var $currentTr = $(this).closest('tr');

            $currentTr.find('select').select2('destroy');
            //клонирование строки
            var $newTr = $currentTr.clone();

            $currentTr.find('select').select2({width : "130px"});
            $newTr.find('select').select2({width : "130px"});

            $currentTr.find('select[id*="_nameResourceType"]').select2({width : "100%"});
            $currentTr.find('select[id$="_monitoringResult"]').select2({width : "250px"});
            $currentTr.find('select[id$="_period"]').select2({width : "250px"});
            $currentTr.find('select[id$="_practiceFormat"]').select2({width : "250px"});
            $currentTr.find('select[id$="_socialResults"]').select2({width : "100%"});
            //$currentTr.find('select[id*="_practiceAnalyticResults"]').select2({width : "74px"});
            $currentTr.find('select[id*="_monitoringAttachments"]').select2({width : "600px"});
            var isChangeFirstIndex = parseInt($(this).closest('div').attr('indexFlag'));

            //очистка областей ввода
            clearNewRow($newTr.find('input'), isChangeFirstIndex);
            clearNewRow($newTr.find('textarea'), isChangeFirstIndex);
            clearNewRow($newTr.find('select'), isChangeFirstIndex);

            $currentTr.find('select[id*="Items"]').select2({width : "100%"});
            $newTr.find('select[id*="Items"]').select2({width : "100%"});
            $currentTr.find('select[id*="projectTeams"]').select2({width : "100%"});
            $newTr.find('select[id*="projectTeams"]').select2({width : "100%"});
            $currentTr.find('select[id*="specialistProblems"]').select2({width : "100%"});
            $newTr.find('select[id*="specialistProblems"]').select2({width : "100%"});

            if (changeSelectNewRow !== undefined) {
                changeSelectNewRow($newTr, $currentTr);
            }

            //переинициализация iCheck
            $newTr.find('input[type="checkbox"]').unwrap().iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });

            //удаление ненужного блока ins
            $newTr.find('div.icheckbox_square-blue').next('ins').remove();

            //если чекбос был чекнут, удаляем класс checked и инициируем клик по нему
            if($newTr.find('div.icheckbox_square-blue').hasClass('checked')){
                $newTr.find('div.icheckbox_square-blue').removeClass('checked');
                $newTr.find('div.icheckbox_square-blue').trigger('click');
            }

            //вставляем склонированную строку после текущей
            $currentTr.after($newTr);
            $newTr.find('select[id*="_nameResourceType"]').select2({width : "100%"});
            $newTr.find("input[id$='cost_key']").val(new Date().getTime() + Math.random());

            $newTr.find('select[id$="_expenseType"]').select2({width : "100%"});
            $currentTr.find('select[id$="_expenseType"]').select2({width : "100%"});

            $currentTr.find('select[id*="financingSourceTypes"]').select2({width : "100%"});
            $newTr.find('select[id*="financingSourceTypes"]').select2({width : "100%"});
            $currentTr.find('select[id*="linkedMethod"]').select2({width : "100%"});
            $newTr.find('select[id*="linkedMethod"]').select2({width : "100%"});

            //переиндексация таблицы
            indexTable($(this));

            //пересчёт id и name атрибутов для input
            $newTr.nextAll('tr').find('input').each( function() {
                //пропуск поля с итоговой суммой
                if($(this).attr("id").indexOf("total") === -1 && !$(this).hasClass("select2-focusser"))
                    changeIdAndName($(this), isChangeFirstIndex);
            });

            //пересчёт id и name атрибутов для textarea
            $newTr.nextAll('tr').find('textarea').each( function() {
                changeIdAndName($(this), isChangeFirstIndex);
            });

            $newTr.nextAll('tr').find('select').each( function() {
                changeIdAndName($(this), isChangeFirstIndex);
            });

            //если делается добавление строк в таблицу финансовых отчётов, то тригерим
            //изменение селекта с выбором таблицы для обновления количества строк
            if($currentTr.closest('div').attr('id').indexOf('registers')!== -1) {
                var documentsTableSelect = $("select[id*='documents'][id$='_expenseType']");
                documentsTableSelect.trigger('change');
            }
            if($newTr.find('select')!== -1) {
                var documentsTableSelect = $newTr.find('select');
                documentsTableSelect.trigger('newTr');
            }
        }

        $newTr.trigger('loaded');
        $('.date').datetimepicker({
            locale: 'RU',
            format: 'DD-MM-YYYY',
            language: 'ru',
            pickTime: false
        });
    });

    /**
     * Обработка клика на кнопку удаления
     */
    $('body').on('click', '#delete_btn', function(event, needToAsk) {
        var answer;
        if(needToAsk === undefined)
            needToAsk = true;

        needToAsk ? answer =  confirm("Вы уверены?") : answer = true;

        //если подтверждено
        if(answer) {
            //скрывает удаляему строку

            $(this).closest('tr').addClass("rowHidden").hide();

            //вызов события клика на стандартном чекбоксе удаления
            $(this).siblings('div.checkbox').find('div.icheckbox_square-blue').trigger('click');

            $($(this).closest("tr").find("input[id$='_sum']")).val(0);
            $($(this).closest("tr").find("input[id$='_sum']")).trigger("change");

            //переиндексация таблицы
            indexTable($(this));

            // $("button[name='send']").remove();
            if($(this).closest('div').attr('id').indexOf('registers')!== -1) {
                var documentsTableSelect = $("select[id*='documents'][id$='_expenseType']");
                documentsTableSelect.trigger('change');
            }
        }
    });

    $('.field-container').each(function () {
        if($(this).find("tbody tr").length > 0) {
            $(this).find('span[id*="field_actions"]').hide();
        }
    });
}

function prependWithRowspan($selector, to, value) {
    if(value === undefined)
        value = 2;
    $selector.prependTo(to);
    $selector.attr("rowspan", value);
}

function appendWithRowspan($selector, to, value) {
    if(value === undefined)
        value = 2;
    $selector.appendTo(to);
    $selector.attr("rowspan", value);
}

/**
 * Метод, выполняющий начальную отрисовку таблицы
 * @param selector
 * @param columnName
 */
function changeTableView(selector, isFirstColumnMustBeRemoved, isLastColumnMustBeRemoved, columnName, isChangeFirstIndex) {
    if(columnName == undefined)
        columnName = "Номер";

    if(isFirstColumnMustBeRemoved == undefined)
        isFirstColumnMustBeRemoved = false;
    if(isLastColumnMustBeRemoved == undefined)
        isLastColumnMustBeRemoved = false;

    if(isChangeFirstIndex == undefined) {
        isChangeFirstIndex = false;
    }
    $(selector+ ' div.checked').closest('tr').hide();
    $(selector+ ' table .checkbox').hide();

    $(selector ).attr("indexFlag", isChangeFirstIndex ? 1 : 0);

    //скрытие поля с Итоговой суммой, потому что изначально оно не в таблице
    if(selector.indexOf("Cost") !== -1){
        var idPart = selector.split("'")[1];
        $("div[id$='total"+ idPart[0].toUpperCase() + idPart.substring(1) +"']").hide();
    }

    // getTotalSum(selector);

    startTableInit(selector, columnName);
    moveActionTh(selector);
    addButtonClickEvent(selector, columnName);
    bindDOMSubtreeModified(selector, columnName);
    hideColumn(selector);

    // addTotalCostTr(selector);
    reformatTable(selector, isFirstColumnMustBeRemoved, isLastColumnMustBeRemoved);
    // autosize($('textarea'));
}

function reformatTable(selector, isFirstColumnMustBeRemoved, isLastColumnMustBeRemoved) {
    if(isFirstColumnMustBeRemoved) {
        $(selector).find("table th:first").remove();
        $(selector).find("table tr").each(function(){
            $(this).find("td:first").remove();
        });
    }

    if(isLastColumnMustBeRemoved) {
        $(selector).find("table th:last").remove();
        $(selector).find("table tr").each(function(){
            $(this).find("td:last").remove();
        });
    }

    var reportResultsTransform = function () {
            var immediateResultTheadTr = selector + " thead tr:first",
                $immediateResultTheadTh = $(selector + " thead tr:last th");

            $(selector + " thead").prepend("<tr><th colspan='3'>Целевые значения</th></tr>");
            $immediateResultTheadTh.prependTo(selector + " thead tr:last");

            prependWithRowspan($immediateResultTheadTh.eq(2), immediateResultTheadTr);
            prependWithRowspan($immediateResultTheadTh.eq(1), immediateResultTheadTr);
            prependWithRowspan($immediateResultTheadTh.eq(0), immediateResultTheadTr);
            appendWithRowspan($immediateResultTheadTh.eq(6), immediateResultTheadTr);
            appendWithRowspan($immediateResultTheadTh.eq(7), immediateResultTheadTr);
            appendWithRowspan($immediateResultTheadTh.eq(8), immediateResultTheadTr);
        },
        socialResultTransform = function () {
            var indexesTheadTr = selector + " thead tr:first",
                $indexesTheadTh = $(selector + " thead tr:last th");

            $(selector + " thead").prepend("<tr><th colspan='3'>Ожидаемые значения показателя</th></tr>");

            prependWithRowspan($indexesTheadTh.eq(0), indexesTheadTr);
            appendWithRowspan($indexesTheadTh.eq(4), indexesTheadTr);
        },

        beneficiaryImprovementTransform = function () {
            var indexesTheadTr = selector + " thead tr:first",
                $indexesTheadTh = $(selector + " thead tr:last th");

            $(selector + " thead").prepend("<tr><th colspan='3'>Ожидаемые значения показателя, кол-во детей</th></tr>");

            prependWithRowspan($indexesTheadTh.eq(0), indexesTheadTr);
            appendWithRowspan($indexesTheadTh.eq(4), indexesTheadTr);
        },

        otherBeneficiaryTransform = function () {
            var otherBeneficiaryImprovementIndexesTheadTr = selector + " thead tr:first",
                $otherBeneficiaryImprovementIndexesTheadTh = $(selector + " thead tr:last th");

            $(selector + " thead").prepend("<tr><th colspan='3'>Ожидаемые значения показателя, кол-во детей</th></tr>");

            prependWithRowspan($otherBeneficiaryImprovementIndexesTheadTh.eq(1), otherBeneficiaryImprovementIndexesTheadTr);
            prependWithRowspan($otherBeneficiaryImprovementIndexesTheadTh.eq(0), otherBeneficiaryImprovementIndexesTheadTr);
            appendWithRowspan($otherBeneficiaryImprovementIndexesTheadTh.eq(5), otherBeneficiaryImprovementIndexesTheadTr);
            appendWithRowspan($otherBeneficiaryImprovementIndexesTheadTh.eq(6), otherBeneficiaryImprovementIndexesTheadTr);
        },

        immediateResultsTransform = function () {
            var immediateResultsTheadTr = selector + " thead tr:first",
                $immediateResultsTheadTh = $(selector + " thead tr:last th");

            $("<tr>" +
                "<th colspan='4'>Целевые значения</th>" +
                "</tr>")
                .insertBefore(immediateResultsTheadTr);

            $("<tr>" +
                "<th colspan='2'>На конец I года</th>" +
                "<th colspan='2'>На конец III года</th>" +
                "</tr>")
                .insertAfter(immediateResultsTheadTr);

            prependWithRowspan($immediateResultsTheadTh.eq(2), immediateResultsTheadTr, 3);
            prependWithRowspan($immediateResultsTheadTh.eq(1), immediateResultsTheadTr, 3);
            prependWithRowspan($immediateResultsTheadTh.eq(0), immediateResultsTheadTr, 3);
            appendWithRowspan($immediateResultsTheadTh.eq(7), immediateResultsTheadTr, 3);
            appendWithRowspan($immediateResultsTheadTh.eq(8), immediateResultsTheadTr, 3);
        },

        practiceActivityIndexesTransform = function () {
            var practiceActivityIndexesTheadTr = selector + " thead tr:first",
                $practiceActivityIndexesTheadTh = $(selector + " thead tr:last th");

            $("<tr>" +
                "<th colspan='2'>Результаты и целевые значения </th>" +
                "</tr>")
                .insertBefore(practiceActivityIndexesTheadTr);

            prependWithRowspan($practiceActivityIndexesTheadTh.eq(1), practiceActivityIndexesTheadTr);
            prependWithRowspan($practiceActivityIndexesTheadTh.eq(0), practiceActivityIndexesTheadTr);
            appendWithRowspan($practiceActivityIndexesTheadTh.eq(4), practiceActivityIndexesTheadTr);
            appendWithRowspan($practiceActivityIndexesTheadTh.eq(5), practiceActivityIndexesTheadTr);
        },

        expensesTransform = function () {
            $(selector).closest('table').find('thead:first th:first').hide();
            $(selector).closest('table').find('tbody:first > tr').each(function() {
                $(this).find('td:first').hide();
            });
        },

        expectedResultsTransform = function () {
            console.log('aaaaa');
            var resultsTheadTr = selector + " thead tr:first",
                $resultsTheadTh = $(selector + " thead tr:last th");

            $("<tr>" +
                "<th colspan='4'>Целевые значения</th>" +
                "</tr>")
                .insertBefore(resultsTheadTr);

            $("<tr>" +
                "<th colspan='2'>Факт 2017 г.</th>" +
                "<th colspan='2'>План 2018 г</th>" +
                "</tr>")
                .insertAfter(resultsTheadTr);

            prependWithRowspan($resultsTheadTh.eq(6), resultsTheadTr, 3);
            prependWithRowspan($resultsTheadTh.eq(4), resultsTheadTr, 3);
            prependWithRowspan($resultsTheadTh.eq(3), resultsTheadTr, 3);
            prependWithRowspan($resultsTheadTh.eq(2), resultsTheadTr, 3);
            prependWithRowspan($resultsTheadTh.eq(1), resultsTheadTr, 3);
            prependWithRowspan($resultsTheadTh.eq(0), resultsTheadTr, 3);
            appendWithRowspan($resultsTheadTh.eq(11), resultsTheadTr, 3);
            appendWithRowspan($resultsTheadTh.eq(12), resultsTheadTr, 3);
            appendWithRowspan($resultsTheadTh.eq(13), resultsTheadTr, 3);
            appendWithRowspan($resultsTheadTh.eq(14), resultsTheadTr, 3);
        },

        practiceResultsTransform = function () {
            var practiceTheadTr = selector + " thead tr:first",
                $practiceTheadTh = $(selector + " thead tr:last th");

            $("<tr>" +
                "<th colspan='4'>Целевые значения</th>" +
                "</tr>")
                .insertBefore(practiceTheadTr);

            $("<tr>" +
                "<th colspan='2'>Факт 2017 г.</th>" +
                "<th colspan='2'>План 2018 г</th>" +
                "</tr>")
                .insertAfter(practiceTheadTr);

            prependWithRowspan($practiceTheadTh.eq(7), practiceTheadTr, 3);
            prependWithRowspan($practiceTheadTh.eq(5), practiceTheadTr, 3);
            prependWithRowspan($practiceTheadTh.eq(4), practiceTheadTr, 3);
            prependWithRowspan($practiceTheadTh.eq(3), practiceTheadTr, 3);
            prependWithRowspan($practiceTheadTh.eq(2), practiceTheadTr, 3);
            prependWithRowspan($practiceTheadTh.eq(1), practiceTheadTr, 3);
            prependWithRowspan($practiceTheadTh.eq(0), practiceTheadTr, 3);
            appendWithRowspan($practiceTheadTh.eq(12), practiceTheadTr, 3);
            appendWithRowspan($practiceTheadTh.eq(13), practiceTheadTr, 3);
            appendWithRowspan($practiceTheadTh.eq(14), practiceTheadTr, 3);
            appendWithRowspan($practiceTheadTh.eq(15), practiceTheadTr, 3);
        };

    //перерисовка таблицы с результатами и изменениями
    if (!checkKey("ReportResults", selector) && selector.indexOf("ReportResults") !== -1){
        reportResultsTransform();
    }

    if (!checkKey("socialResultIndexes", selector) && selector.indexOf("socialResultIndexes") !== -1 ||
        selector.indexOf("additionalIndicatorIndexes") !== -1
    ) {
        socialResultTransform();
    }

    if (!checkKey("beneficiaryImprovementIndexes", selector) && selector.indexOf("beneficiaryImprovementIndexes") !== -1) {
        beneficiaryImprovementTransform();
    }

    if (!checkKey("otherBeneficiaryImprovementIndexes", selector) && selector.indexOf("otherBeneficiaryImprovementIndexes") !== -1) {
        otherBeneficiaryTransform();
    }

    if (!checkKey("immediateResults", selector) && selector.indexOf("immediateResults") !== -1) {
        immediateResultsTransform();
    }

    if (!checkKey("practiceActivityIndexes", selector) && selector.indexOf("practiceActivityIndexes") !== -1) {
        practiceActivityIndexesTransform();
    }

    if (!checkKey("expenses", selector) && selector.indexOf("expenses") !== -1) {
        expensesTransform();
    }

    if (!checkKey("expectedResults", selector) && selector.indexOf('expectedResults') !== -1) {
        expectedResultsTransform();
    }

    if (!checkKey("practiceResults", selector) && selector.indexOf('practiceResults') !== -1) {
        practiceResultsTransform();
    }
}

function addButtonClickEvent(btnSelector) {
    $('body').on('click', btnSelector + " .sonata-ba-action", function() {
        isDOMChangeHandled = false;
    });
}

/**
 * Функция, связывающая блок под селектором с функцией
 * обработки изменения содержимого этого блока
 * @param selector
 * @param columnName
 */
function bindDOMSubtreeModified(selector, columnName) {
    if(columnName == undefined)
        columnName = "Номер";
    $(selector).bind ("DOMSubtreeModified", function() {
        handleDOMChangeWithDelay(selector, columnName)
    });
}

/**
 * Обработка изменения блока с таблицей с задержкой
 * @param selector
 * @param columnName
 */
function handleDOMChangeWithDelay (selector, columnName) {
    setTimeout (function() {
        handleDOMChange(selector, columnName)
    }, 1);
}

/**
 * Обработка изменения содержимого таблицы
 * @param selector
 * @param columnName
 */
function handleDOMChange (selector, columnName) {
    if(!isDOMChangeHandled) {
        // $(selector + " .sonata-ba-action").remove();
        $(selector + " .sonata-ba-action").hide();
        //перерисовка таблицы
        startTableInit(selector);

        //сдвиг столбца с кнопками действий
        moveActionTh(selector);

        $('table .checkbox').hide();
        //установка флага обработанного изменения DOM
        isDOMChangeHandled = true;

        //addTotalCostTr(selector);
        reformatTable(selector);
    }
}

/**
 * Изменение id и name селектора
 * @param $selector
 */
function changeIdAndName($selector, isChangeFirstIndex) {
    //удаляет текущее значение области ввода
    $(this).val('');
    var id = $selector.attr('id');
    var name = $selector.attr('name');
    if(isChangeFirstIndex === undefined) {
        isChangeFirstIndex = false;
    }

    if(isChangeFirstIndex) {
        //regExp для поиска индекса в id
        var idRegEx = /(_([0-9]+)_)/;

        //regExp для поиска индекса в name
        var nameRegEx = /(\[([0-9]+)\])/;
    }
    else {
        //regExp для поиска индекса в id
        var idRegEx = /(_([0-9]+)_)(?!.*(_([0-9]+)_))/;

        //regExp для поиска индекса в name
        var nameRegEx = /(\[([0-9]+)\])(?!.*(\[([0-9]+)\]))/;
    }

    if(!$selector.hasClass("select2-focusser") && !$selector.hasClass("select2-input")) {
        var idNum = id.match(idRegEx)[2];

        //увеличение индекса в id на 1
        var nextId = parseInt(idNum) + 1;
        $selector.attr('id', id.replace(idRegEx, '_'+ nextId + '_'));
        //увеличение индекса в name на 1
        $selector.attr('name', name.replace(nameRegEx, '['+ nextId + ']'));
    }
    if($selector.is('textarea')) {
        var script = $selector.closest('td').find('script');
        var newScriptText = script.text().replace(new RegExp(id,'g'), id.replace(idRegEx, '_'+ nextId + '_'));
        script.remove();
        var newScript = document.createElement( 'script' );
        newScript.type = 'text/javascript';
        newScript.text = newScriptText;
        $selector.closest('td')[0].appendChild(newScript);
    }
}

/**
 * Очистка области ввода, удаление блока с ошибками
 * @param $selector
 */
function clearNewRow($selector, isChangeFirstIndex){
    if(isChangeFirstIndex == undefined) {
        isChangeFirstIndex = false;
    }

    $selector.each(function() {

        changeIdAndName($(this), isChangeFirstIndex);

        if($(this).is('select')) {
            $(this).select2({width : "130px"});
            if($(this).attr('id').indexOf('monitoringResult') != -1 ||
                $(this).attr('id').indexOf('period') != -1 ||
                $(this).attr('id').indexOf('practiceFormat') != -1
            ) {
                $(this).select2({width : "250px"});
            }
            // if($(this).attr('id').indexOf('practiceAnalyticResults') != -1 ) {
            //     $(this).select2({width : "74px"});
            // }
            if($(this).attr('id').indexOf('monitoringAttachments') != -1 ) {
                $(this).select2({width : "600px"});
            }
            if($(this).attr('id').indexOf('beneficiaryProblem') != -1 ) {
                $(this).select2({width : "100%"});
            }
            if($(this).is('[id*=_document_type]'))
                $(this).html('');
        } else {
            $(this).val('');
            $(this).trigger('change');
        }

        $(this).closest('td').find('div.help-inline').remove();
        if($(this).siblings("div[class*='itm_file']")) {
            $(this).siblings("div[class*='itm_file']").remove();
        }
    });
}

/**
 * Метод начальной инициализации таблицы
 * @param selector
 * @param columnName
 */
function startTableInit(selector, columnName) {
    //добавление колонки с номерами строк таблицы
    if (columnName == undefined)
        columnName = "Номер";

    $(selector + " table thead tr").each(function() {
        $(this).prepend('<th>' + columnName + '</th>');
    });

    $(selector + " table tbody tr").each(function(){
        $(this).prepend('<td class="selector_id"></td>');
    });
    //добавляет стили к каждому элементу первой колонки
    $(".selector_id").css({
        'width' : '5%',
        'text-align': 'center',
        'vertical-align' : 'middle',
        'top' : '50%'
    });

    //индексация таблицы
    $(selector + " tr").each(function () {
        indexTable($(this));
    })
}

/**
 * перемещение столбца удаления в конец таблицы
 * и перименовка его
 */
function moveActionTh(selector){
    $(selector).find("th#action_th").each(function() {
        // if(selector.indexOf("_documents") != -1)
        //     $(this).text('8. Действие');
        // else
        $(this).text('Действие');
        $(this).closest('tr').append($(this));
    });

    $(selector+ ' tbody td:nth-child(2)').each(function() {
        $(this).closest('tr').append($(this));
    })
}


function hideColumn(selector) {
    var index,
        emptySelectors = $(selector).find('input[type=hidden]').parent();
    emptySelectors.each(function () {
        if ($(this).prop('tagName') == 'TD') {
            index = $(this).index();
            $(this).hide();
            $(selector).find('thead th:nth-child('+ ++index +')').hide();
        }
    })
}

/**
 * Индексация таблицы
 * @param $selector
 * @returns {number}
 */
function indexTable($selector) {

    //находим таблицу
    var $table = $selector.closest('table'),
        tableIndex = 0,
        limit = parseInt($selector.closest('tr').find('input').attr('limit'));

    //проходим по всем строкам таблицы
    $table.find('tbody tr').each(function() {
        //если строка не скрыта, увеличиваем индекс
        if($(this).css('display') != 'none' && !$(this).find("td.totalCost").length){
            tableIndex++;
        }
        //вставляем индекс в колонку с индесами
        $(this).find('td.selector_id').text(tableIndex);

        //вроверка количества записей для талицы публикаций
        //если больше 5, то выставляется флаг isMoreThanLimit
        if(($(this).closest('div.form-group').attr('id').indexOf('_publications') != -1 && tableIndex >= 5)
            || (limit > 0 && tableIndex >= limit)){
            isMoreThanLimit = true;
        }
        else isMoreThanLimit = false;
    });

    if(tableIndex == 1) {
        $table.find("tbody tr #delete_btn").attr("disabled", true);
    }
    else {
        $table.find("tbody tr #delete_btn").attr("disabled", false);
    }

    //если в таблице нет записей, то добавлить кнопку добавления
    if(tableIndex == 0){
        //удадение предыдущей кнопки добавления
        $selector.closest('div.form-group').find('button#add_btn_0').remove();

        if($table.find("tbody tr").length > 0) {
            //вставка новой кнопки
            $selector.closest('div.form-group').append('<button type="button" class="btn btn-success btn-sm" id="add_btn_0">' +
                '<i class="fa fa-plus-circle"></i> Добавить </button>');
        }
    }
    else {
        //иначе удаление кнопки добавления
        $selector.closest('div.form-group').find('button#add_btn_0').remove();
    }

    return tableIndex;
}

function changeTableWithFile(selector, isChangeFirstIndex) {

    if(isChangeFirstIndex == undefined) {
        isChangeFirstIndex = false;
    }

    $(selector ).attr("indexFlag", isChangeFirstIndex ? 1 : 0);
    $(selector+ ' div.checked').closest('tr').hide();
    indexTable($(selector + " tr:first"));
    bindDOMSubtreeModified($(selector));
}

function checkKey(key, selector) {

    if(selector.indexOf(key) !== -1) {
        if (transformation !== undefined && transformation[key] !== undefined) {
            transformation[key](selector);
            return true;
        }
    }

    return false;
}

function insertBaseTitle(selector, countCol, name) {
    if (name == undefined) {
        name = 'Целевые значения';
    }

    $("<tr>" +
        "<th colspan='"+ countCol +"'>"+ name + "</th>" +
        "</tr>")
        .insertBefore(selector);
}


