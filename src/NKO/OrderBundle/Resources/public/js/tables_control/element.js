'use strict';

function BaseHtmlElemClass() {
    var self = this;
    BaseClass.apply(self);
};


function SelectClass() {
    var self = this;
    BaseHtmlElemClass.apply(self);

    //применение изменений
    self.applyChange = function()
    {
        self._elem.trigger('change');
    };

    //очистить выпадающий список
    self.clear = function()
    {
        self._elem.empty();
    };

    //установить Value (выбранное значение)
    self.setValue = function(value)
    {
        self._elem.select2("val", value);
        self.applyChange();
    };

    //добавить в выпадающий список новый вариант
    self.appendOption = function(value, description)
    {
        self._elem.append($('<option>', {
            value: value,
            text: description
        }));
    };

    self.setReadonly = function()
    {
        self._elem.select2("readonly", true);
    };

    //установить ширину селекта
    self.setWidth = function(width)
    {
        self._elem.select2({width : width});
    };

    //открыть выпадающий список
    self.open = function()
    {
        self._elem.select2('open');
    };

    self.destroy = function()
    {
        self._elem.select2('destroy');
    };

    self.define = function(row)
    {
        self._elem.select2();
        self.setPlaceholder(row);
        self.applyChange();
    };

    self.getVal = function()
    {
        return self._elem.select2('data');
    };

    self.isRequired = function()
    {
        return !self._elem.find('option[value=""]').length;
    };
};

SelectClass.getInstance = function(options) {
    if (SelectClass.instance === undefined) {
        SelectClass.instance = {
            'MultipleSelectClass' : new MultipleSelectClass(),
            'SimpleSelectClass' : new SimpleSelectClass()
        }
    }

    return $(options).attr('multiple') ? 'MultipleSelectClass' : 'SimpleSelectClass';
};


function SimpleSelectClass() {
    var self = this;
    SelectClass.apply(self);

    self.setPlaceholder = function(row)
    {
        //не изменяем селекты строки, которою копировали. Либо значение выбрано
        if ((row !== undefined && !row.isNew) || self.getVal()) {
            return;
        }

        var inspector = self.configurationPool.getService(InspectorClass),
            stateSelects = inspector.doAction(row ? row._elem : self._elem, 'stateSelects', true);
        //от параметра require
        if (stateSelects === undefined) {
            if (self.isRequired()) {
                self.setFirstOption();
            }
        }

        if (stateSelects === false) {
            self._elem.select2('val', null);
            return null;
        }

        if (stateSelects === true) {
            self._elem.find('option:eq(1)').prop('selected',true);
            self.applyChange();
        }
    };

    self.clearSelection = function()
    {
        self._elem.val(null);
        self.applyChange();
    };

    self.trim = function()
    {
        var span = self._elem.siblings().find('a').find('.select2-chosen');
        span.text(span.text().trim());
    };

    self.setFirstOption = function()
    {
        self._elem.find('option:eq(0)').prop('selected',true);
    };
};


function MultipleSelectClass() {
    var self = this;
    SelectClass.apply(self);

    self.setPlaceholder = function()
    {
        self._elem.select2({
            placeholder: 'Не выбран',
            width : '100%'
        });
        self.applyChange();
    };

    self.clearSelection = function()
    {
        self._elem.val([]);
        self.setPlaceholder();
        self.applyChange();
    };
};



function InputClass() {
    var self = this;
    BaseHtmlElemClass.apply(self);

    self.setPlaceholder = function(message)
    {
        self._elem.attr('placeholder', message);
    };

    self.clearSelection = function()
    {
        self._elem.val('');

        var itmFile = "div[class*='itm_file']";
        if (self._elem.siblings(itmFile)) {
            self._elem.siblings(itmFile).remove();
        }
    };
};


function TextAreaClass() {
    var self = this;
    BaseHtmlElemClass.apply(self);

    self.setPlaceholder = function(message)
    {
        self._elem.attr('placeholder', message);
    };

    self.clearSelection = function()
    {
        self._elem.val('');
    };
};


function getInstance(context, argument) {
    if (context.instance === undefined) {
        context.instance = argument;
    }
    return context.instance;
}