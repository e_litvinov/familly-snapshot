'use strict';


function TableClass() {
    var self = this;
    BaseClass.apply(self);
    self.inspector = self.configurationPool.getService(InspectorClass);
    self.initPlaceHolders = null;

    //переопределение сеттера, если не строка, то ищем TR, поднимаясь вверх
    var parentSetter = self.set;
    self.set = function(elem)
    {
        parentSetter(self.checkSelector(elem, 'TABLE'));
    };

    self.index = function()
    {
        var tableIndex = 0,
            row = self.configurationPool.getService(RowClass);

        self._elem.find('tbody tr').each(function() {
            row.set($(this));

            //если строка не скрыта, увеличиваем индекс
            if (!row.isHide()){
                tableIndex++;
            }

            //вставляем индекс в колонку с индесами
            if (!self.inspector.doAction(self._elem, 'removeFirstColumn')) {
                row.setIndex(tableIndex);
            }

            if (self.initPlaceHolders) {
                row.setPlaceHolderSelect();
            }
        });

        self.configurationPool.setNumberRowsByTable(self.inspector.getKey(self._elem), tableIndex);
        var limit = self.inspector.doAction(self._elem, 'limit', true);

        if (limit) {
            self.inspector.controlAdd(self._elem, tableIndex >= limit ? true : null);
        }

        self.inspector.controlDelete(self._elem, tableIndex === 1 ? true : false);
    };

    self.hideCheckboxes = function()
    {
        self._elem.find("div.checked").closest('tr').hide();
        self._elem.find('.checkbox').hide();
        self._elem.closest('.field-container').find('span[id*="field_actions"]').hide();
    };

    //Базовый запуск работы
    self.process = function()
    {
        self.initPlaceHolders = true;
        self.hideCheckboxes();
        self.localProcess();
        self.initPlaceHolders = null;
    };
};

TableClass.getInstance = function(options) {
    if (TableClass.instance === undefined) {
        TableClass.instance = {
            'SimpleTableClass' : new SimpleTableClass(),
            'TableWithFileClass' : new TableWithFileClass()
        };
        TableClass.count = {};
    }
    return options === undefined || options['contentFile'] === undefined ? 'SimpleTableClass' : 'TableWithFileClass';
};


function SimpleTableClass() {
    TableClass.apply(this);
    var self = this;

    self.localProcess = function()
    {
        self.reformat();
        self.index();
    };

    self.reformat = function()
    {
        var removeLastColumn = self.inspector.doAction(self._elem, 'removeLastColumn'),
            removeFirstColumn = self.inspector.doAction(self._elem, 'removeFirstColumn'),
            hideLastColumn = self.inspector.doAction(self._elem, 'hideLastColumn'),
            countRowThead = self.inspector.doAction(self._elem, 'countRowThead', true);

        //Редактирование tHead
        var theadTr = self._elem.find("thead tr:first"),
            actionTh = theadTr.find("th:first");

        if (removeLastColumn) {
            actionTh.remove();
        } else {
            actionTh.text('Действие');
            theadTr.append(actionTh);
            actionTh.attr('rowspan', countRowThead !== undefined ? countRowThead : 1);
        }

        if (hideLastColumn) {
            actionTh.hide();
        }

        if (!removeFirstColumn) {
            var columnName = self.inspector.doAction(self._elem, 'columnName', true);
            columnName = columnName ? columnName : 'Номер';
            theadTr.prepend('<th rowspan="'+ (countRowThead !== undefined ? countRowThead : 1) +'">' + columnName + '</th>');
        }

        //Редактирование tBody
        var bodyTr = self._elem.find("tbody tr");
        bodyTr.each(function(){

            var actionTd = $(this).find("td:first");
            removeLastColumn ? actionTd.remove() : $(this).append(actionTd);

            if (hideLastColumn) {
                actionTd.hide();
            }

            if (!removeFirstColumn) {
                $(this).prepend('<td class="selector_id" style="width:5%; text-align:center; vertical-align:middle; top:50%"></td>');
            }
        });
    };
};

function TableWithFileClass() {
    TableClass.apply(this);
    var self = this;

    self.localProcess = function()
    {
        self.index();
    };
};