'use strict';


function TableManagerClass() {
    var self = this;
    BaseClass.apply(self);

    //------------- ТОЛЬКО ЭТИ МЕТОДЫ ДОСТУПНЫ ИЗВНЕ--------
    //PUBLIC, получение объекта, в котором указываются конфиги
    self.getConfigureInstance = function()
    {
        return self.configurationPool.getService(TableOptionsClass);
    };

    //PUBLIC, запуск всей системы
    self.process = function()
    {
        self.configurationPool.processCustomConfigs();
        changeTables();

        (self.configurationPool.getService(BtnHandlerClass)).init();
        (self.configurationPool.getService(EventManager)).init();
    };
    //---------------------------------------------------

    //PRIVATE, первоначальная обработка таблиц
    function changeTables()
    {
        var tables = self.configurationPool.getTables(),
            configs = self.configurationPool.getConfigs(),
            transformManager = self.configurationPool.getService(TransformManagerClass),
            selectSizeManager = self.configurationPool.getService(SelectSizeManager);

        for (var key in tables) {
            var tableObj = self.configurationPool.getService(TableClass, configs[key]);
            tableObj.set(tables[key]);
            tableObj.process();
            transformManager.process(tables[key]);
        }
        //Полнотекстовые селекты
        selectSizeManager.init();
    }
}


function inherit(proto) {
    function F() {}
    F.prototype = proto;
    return new F;
}

if (!Object.create) {
    Object.create = inherit;
}





