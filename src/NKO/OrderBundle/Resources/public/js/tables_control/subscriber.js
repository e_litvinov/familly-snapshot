'use strict';

function EventManager() {
    BaseClass.apply(this);
    this.events = {};

    //инициализация
    this.init = function () {
        var events = EventManager.subscribers;
        for (var item in events) {
            if (!Array.isArray(events[item])) {
                this.addSubscriber(item, new window[events[item]]);
                continue;
            }

            for (var key = 0; key < events[item].length; key++) {
                this.addSubscriber(item, new window[events[item][key]]);
            }
        }
    };

    //добавление сабскрайбера
    this.addSubscriber = function (eventName, subscriber) {
        //
        if (this.events[eventName] === undefined) {
            this.events[eventName] = [];
        }

        //заносим обработчик
        this.events[eventName].push(subscriber);
    };

    //метод, в котором происходит генерация события
    this.generate = function (eventName, event) {

        //проверка наличия обработчиков к событию, если есть - оповещаем их о произошедшем событии
        if (this.events[eventName] !== undefined) {
            try {
                this.notify(eventName, event);
            } catch (error) {
                if (error instanceof MethodExistsError || error instanceof EventPropertyError) {
                    console.log(error.message);
                }
            }
        }
    };

    //оповещение сабскрайберов о событии
    this.notify = function (eventName, event) {
        var action = event.data.action;
        if (action === undefined) {
            throw new EventPropertyError("action");
        }

        //вызов у обработчиков нужный метод и передача аргумента-события
        var eventSubscribers = this.events[eventName];
        for (var item = 0; item < eventSubscribers.length; item++) {
            if (eventSubscribers[item][action] === undefined) {
                throw new MethodExistsError(action, eventSubscribers[item].constructor.name);
            }
            this.events[eventName][item].process(event);
        }
    }
}


function SubscriberEvent(data) {
    this.data = data;
}




function ChangeToActualFiles() {
    BaseClass.apply(self);
    this.pre = function (event) {};

    this.post = function (event) {
        var inspector = self.configurationPool.getService(InspectorClass),
            initService = self.configurationPool.getService(InitService);

        if (event.data.selector === undefined) {
            throw new EventPropertyError("selector", "ChangeToActualFiles");
        }

        var selector = self.configurationPool.getTables(inspector.getKey(event.data.selector));
        initService.detectFiles(selector);
    };
}





function EventPropertyError(property, subscriber) {
    this.name = "EventPropertyError";
    this.subscriber = subscriber !== undefined ? ' (Subscriber "' + subscriber + '")' : "";
    this.message = 'Ошибка! В объекте Event ожидается свойство data.' + property + this.subscriber;
}
EventPropertyError.prototype = Object.create(Error.prototype);


function MethodExistsError(action, object) {
    this.name = "MethodExistsError";
    this.message = 'Ошибка! В объекте '+ object +' не существует метода ' + action;
}
MethodExistsError.prototype = Object.create(Error.prototype);







EventManager.subscribers = {
    'changeColRowsWithFile' : ['ChangeToActualFiles']
};

