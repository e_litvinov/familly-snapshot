'use strict';


function RowClass() {
    var self = this;
    BaseClass.apply(self);
    self.inspector = self.configurationPool.getService(InspectorClass);
    self.selectSizeManager = self.configurationPool.getService(SelectSizeManager);
    self.isNew = null;

    //переопределение сеттера, если не строка, то ищем TR, поднимаясь вверх
    var parentSetter = self.set;
    self.set = function(elem)
    {
        parentSetter(self.checkSelector(elem, 'TR'));
    };

    self.clear = function()
    {
        self.clearByElem(SelectClass, self._elem.find('select'));
        self.clearByElem(InputClass, self._elem.find('input'));
        self.clearByElem(TextAreaClass, self._elem.find('textarea'));
    };

    self.remove = function()
    {
        self.hide();

        //если есть реестры, то при удалении строки пересчитывать общее
        self.recountAfterRemoveRegisterRow();

        self._elem.find('div.icheckbox_square-blue').trigger('click');
    };

    self.appendAfterCurrent = function(elem)
    {
        self._elem.after(elem.get());
    };

    self.clearByElem = function(object, selectors)
    {
        selectors.each(function() {
            var instance = self.configurationPool.getService(object, $(this));
            instance.set($(this));
            instance.clearSelection();

            //удяляем ошибки валидации
            $(this).closest('td').find('div.help-inline').remove();
        });
    };

    self.isHide = function()
    {
        return self._elem.css('display') === 'none';
    };

    self.hide = function()
    {
        self._elem.addClass("rowHidden").hide();
    };

    self.setIndex = function(index)
    {
        self._elem.find('td.selector_id').text(index);
    };

    self.clone = function()
    {
        var newRow = new RowClass();
        newRow.isNew = true;
        self.changeSelectForClone();
        newRow.set(self._elem.clone());
        self.changeSelectForClone(true);

        return newRow;
    };

    self.changeSelectForClone = function(actually)
    {
        self._elem.find('select').each(function() {
            var instance = self.configurationPool.getService(SelectClass, $(this));
            instance.set($(this));

            if (actually === undefined) {
                instance.destroy();
            } else {
                instance.define(self);

                if (instance instanceof SimpleSelectClass) {
                    instance.trim();
                }
            }
        });
    };

    self.changeServiceData = function()
    {
        for (var i = 0; i < 2; i++) {
            var trs = i === 0 ? self._elem : self._elem.nextAll('tr');

            trs.find('textarea').each( function() {
                self.changeIdAndName($(this));
            });

            trs.find('select').each( function() {
                self.changeIdAndName($(this));
            });

            trs.find('input').each( function() {
                self.changeIdAndName($(this));
            });
        }
    };

    self.changeIdAndName = function(selector)
    {
        var id = selector.attr('id'),
            name = selector.attr('name'),
            isChangeFirstIndex = self.inspector.doAction(self._elem, 'changeFirstIndex'),

            //regExp для поиска индекса в id
            idRegEx = isChangeFirstIndex ? /(_([0-9]+)_)/ : /(_([0-9]+)_)(?!.*(_([0-9]+)_))/,

            //regExp для поиска индекса в name
            nameRegEx = isChangeFirstIndex ? /(\[([0-9]+)\])/ : /(\[([0-9]+)\])(?!.*(\[([0-9]+)\]))/;

        if (!selector.hasClass("select2-focusser") && !selector.hasClass("select2-input")) {
            var idNum = id.match(idRegEx)[2];

            //увеличение индекса в id на 1
            var nextId = parseInt(idNum) + 1;
            selector.attr('id', id.replace(idRegEx, '_'+ nextId + '_'));
            //увеличение индекса в name на 1
            selector.attr('name', name.replace(nameRegEx, '['+ nextId + ']'));
        }
        if (selector.is('textarea')) {
            var script = selector.closest('td').find('script');
            var newScriptText = script.text().replace(new RegExp(id,'g'), id.replace(idRegEx, '_'+ nextId + '_'));
            script.remove();
            var newScript = document.createElement( 'script' );
            newScript.type = 'text/javascript';
            newScript.text = newScriptText;
            selector.closest('td')[0].appendChild(newScript);
        }
    };

    self.initRowElems = function()
    {
        //переинициализация iCheck
        self._elem.find('input[type="checkbox"]').unwrap().iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });

        //удаление ненужного блока ins
        self._elem.find('div.icheckbox_square-blue').next('ins').remove();


        //если чекбос был чекнут, удаляем класс checked и инициируем клик по нему
        if (self._elem.find('div.icheckbox_square-blue').hasClass('checked')){
            self._elem.find('div.icheckbox_square-blue').removeClass('checked');
            self._elem.find('div.icheckbox_square-blue').trigger('click');
        }

        self._elem.find('.date').datetimepicker({
            locale: 'RU',
            format: 'DD-MM-YYYY',
            language: 'ru',
            pickTime: false
        });

        //инициализируем селекты
        self.selectSizeManager.initNewRowSelects(self._elem);

        //автонумерик для инпутов
        var autoNumericOptions = self.inspector.doAction(self._elem, 'autoNumeric', true);
        if (autoNumericOptions) {
            for (var key in autoNumericOptions) {
                var items = self._elem.find(key);
                items.autoNumeric('init', autoNumericOptions[key]);
            }
        }

    };

    self.setPlaceHolderSelect = function()
    {
        self._elem.find('select[multiple="multiple"]').each( function() {
            var instance = self.configurationPool.getService(SelectClass, $(this));
            instance.set($(this));
            instance.setPlaceholder();
        });

        self._elem.find('select:not([multiple="multiple"])').each( function() {
            var instance = self.configurationPool.getService(SelectClass, $(this));
            instance.set($(this));
            instance.setPlaceholder();
            instance.applyChange();
        });
    };

    self.recountAfterRemoveRegisterRow = function()
    {
        if ((self.inspector.getKey(self._elem)).indexOf('registers') !== -1) {
            var selector = self._elem.find('input[id*="_sum"]');
            if (selector.length) {
                selector.val(0);
                selector.trigger('change');
            }
        }
    };
};

