'use strict';


function BaseClass() {
    var self = this;
    self._elem = null;

    //объявляем configurationPool, доступный всегда. singlton
    (function(){
        if (ConfigurationPool.instance === undefined) {
            ConfigurationPool.instance = new ConfigurationPool();
        }
        self.configurationPool = ConfigurationPool.instance;
    }());

    self.get = function()
    {
        return self._elem;
    };

    self.set = function(elem)
    {
        self._elem = elem;
    };

    self.checkSelector = function(selector, tag)
    {
        return selector.prop('tagName') === tag ? selector : selector.closest(tag);
    };

    self.runAfterCheckSelector = function(func, tag)
    {
        return function() {
            var options = [];
            if (arguments[0].prop('tagName') === tag) {
                options = arguments;
            } else {
                for (var i = 0; i < arguments.length; i++) {
                    if (i === 0) {
                        options.push(arguments[0].closest(tag));
                        continue;
                    }
                    options.push(arguments[i]);
                }
            }

            return func.apply(self, options);
        }
    };
};


function TableOptionsClass() {
    var self = this;
    BaseClass.apply(self);

    var options = {},
        currentTable,
        tables = self.configurationPool.getTables();

    self.table = function(name)
    {
        if (tables[name] !== undefined) {
            currentTable = name;
        } else {
            currentTable = undefined;
        }

        return self;
    };

    // removeFirstColumn (удалить столбец Нумерации),
    // removeLastColumn (удалить столбец Действие, когла количество строк фиксированое и не может изменить во время выполнения),
    // hideLastColumn (скрыть столбец Действие, в самой таблице возможности удаления/добавления нет, но можно через код (напирмер dropDownList)),
    // contentFile (таблице с файлами);
    // limit (ограничение количества строк)
    // removeTracking (не отслеживать таблицу);
    // columnName (название 1-го столбца)
    // changeFirstIndex (с вложенными таблицами),
    // transform - в этом файле есть объект "defaultTableTransformations", который перестраивает thead определенных таблиц по умолчанию.
    //             чтобы отметинить - поставить false,
    //             чтобы препеопределить перестройку - написать другую функцию
    // countRowThead - количество строк в thead, чтобы после трансформации поля 'Номер' и 'Действие' с нужным RowSpan
    // autoNumeric - {селектор для поиска : параметры}. будет применен к склонированной строке
    // stateSelects - undefined - по умолч (в зависимости от required). false - пустые, true - первый пункт
    self.configs = function(val)
    {
        options[currentTable] = val;
        return self;
    };

    self.getAll = function()
    {
        //удаляем таблицы, которые не следует отслеживать
        for (var item in options) {
            if (options[item]['removeTracking']) {
                delete tables[item];
            }
        }

        return options;
    };
};

function BtnHandlerClass() {
    var self = this;
    BaseClass.apply(self);
    var inspector = self.configurationPool.getService(InspectorClass),
        eventManager = self.configurationPool.getService(EventManager);

    self.init = function()
    {
        self.handleAdd();
        self.handleDelete();
    };

    //обработчик клика Добавить
    self.handleAdd = function()
    {
        $('body').on('click', '#add_btn', function() {
            var row = getRow($(this)),
                newRow = row.clone();

            newRow.clear();
            row.appendAfterCurrent(newRow);
            newRow.changeSelectForClone(true);
            newRow.changeServiceData();
            newRow.initRowElems();
            reindexTable($(this));

            if (inspector.doAction($(this), 'contentFile')) {
                eventManager.generate('changeColRowsWithFile', new SubscriberEvent({selector: $(this), action : "post"}));
            }
        });
    };

    //обработчик клика Удалить
    self.handleDelete = function()
    {
        $('body').on('click', '#delete_btn', function() {

            var needToAsk = inspector.doAction($(this), 'hideLastColumn');
            if (!needToAsk) {
                if (!confirm("Вы уверены?")) {
                    return;
                }
            }

            var row = getRow($(this));
            if (row.isHide()) {
                return;
            }

            row.remove();
            reindexTable($(this));

            if (inspector.doAction($(this), 'contentFile')) {
                eventManager.generate('changeColRowsWithFile', new SubscriberEvent({selector: $(this), action : "post"}));
            }
        });
    };

    function reindexTable(selector) {
        var table = self.configurationPool.getService(TableClass);
        table.set(selector);
        table.index();
    }

    function getRow(selector) {
        var row = self.configurationPool.getService(RowClass);
        row.set(selector);
        return row;
    }
};

function InspectorClass() {
    var self = this;
    BaseClass.apply(self);
    var config = self.configurationPool.getConfigs(),
        tables = self.configurationPool.getTables();

    // table - селектор таблицы
    // option - проверяемый параметр
    // strict :
    //     true - возвращается значение, которое записано в проверяемом конфиге, либо undefined
    //     undefined - логическое значение, либо undefined
    self.doAction = function(table, option, strict)
    {
        var key = self.getKey(table),
            existsOption = checkExistOption(key, option);

        if (!key || !existsOption) {
            return undefined;
        }

        return strict === undefined ? existsOption : config[key][option];
    };

    function checkExistOption(table, option)
    {
        if (config[table] !== undefined && config[table][option] !== undefined) {
            return true;
        }
        return null;
    }

    self.getKey = self.runAfterCheckSelector(function(table)
    {
        for (var item in tables) {
            if (tables[item][0] === table[0]) {
                return item;
            }
        }
    }, 'TABLE');

    self.controlDelete = self.runAfterCheckSelector(function(table, disable) {
        table.find("tbody tr #delete_btn").attr("disabled", disable);
    }, 'TABLE');

    self.controlAdd = self.runAfterCheckSelector(function(table, disable) {
        table.find("tbody tr #add_btn").attr("disabled", disable);
    }, 'TABLE');
};

function ConfigurationPool() {
    var self = this,
        configs = {},
        tables = {},
        numberRowsTable = {};

    //Установка параметров  -----------------
    (function() {
        var tableSelectors = $('table');
        tableSelectors.each(function () {
            var id = $(this).closest('.form-group').attr('id');
            //id = id.substring(id.lastIndexOf('_') + 1);
            id = id.substring(id.indexOf('_') + 1);
            tables[id] = $(this);
        });
    }());

    self.processCustomConfigs = function()
    {
        var configurationObj = self.getService(TableOptionsClass);
        configs = configurationObj.getAll();
    };

    //---------------------------------------

    //получение таблиц с их селектором. Либо всех, либо конкретной таблицы
    self.getTables = function(table)
    {
        var outputData = tables;
        if (table !== undefined) {
            outputData = tables[table] === undefined ? {} : tables[table]
        }
        return outputData;
    };

    //получение конфигов, либо всех, либо для конкретной таблицы
    self.getConfigs = function(table)
    {
        var outputData = configs;
        if (table !== undefined) {
            outputData = configs[table] === undefined ? {} : configs[table]
        }
        return outputData;
    };

    self.setNumberRowsByTable = function(table, number)
    {
        numberRowsTable[table] = number;
    };

    //получение актуальное количество строк в таблицах
    self.getNumberRowsByTable = function(table)
    {
        return numberRowsTable[table];
    };

    //получение сервиса, синглтоном
    self.getService = function(context, config)
    {
        if (context.getInstance !== undefined) {
            var key = context.getInstance.apply(context, [config]);
            return context.instance[key];
        }

        if (context.instance === undefined) {
            context.instance = new context();
        }
        return context.instance;
    };
};

function TransformManagerClass() {
    var self = this;
    BaseClass.apply(self);
    var inspector = self.configurationPool.getService(InspectorClass);

    self.process = function(table)
    {
        var tableKey = inspector.getKey(table),
            transform = inspector.doAction(table, 'transform', true);

        if (transform === false) {
            return null;
        }

        if (transform === undefined && defaultTableTransformations[tableKey] !== undefined) {
            defaultTableTransformations[tableKey].apply(self, [table, tableKey]);
        }

        if (typeof(transform) === 'function') {
            transform.apply(self, [table, tableKey]);
        }
    };

    self.prependWithRowspan = function($selector, to, value)
    {
        if (value === undefined) {
            value = 2;
        }

        $selector.prependTo(to);
        $selector.attr("rowspan", value);
    };

    self.appendWithRowspan = function($selector, to, value)
    {
        if (value === undefined) {
            value = 2;
        }

        $selector.appendTo(to);
        $selector.attr("rowspan", value);
    };
};





var defaultTableTransformations = {
    reportResults : function (selector) {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $(selector + " thead").prepend("<tr><th colspan='3'>Целевые значения</th></tr>");
        th.prependTo(selector + " thead tr:last");

        this.prependWithRowspan(th.eq(2), tr);
        this.prependWithRowspan(th.eq(1), tr);
        this.prependWithRowspan(th.eq(0), tr);
        this.appendWithRowspan(th.eq(6), tr);
        this.appendWithRowspan(th.eq(7), tr);
        this.appendWithRowspan(th.eq(8), tr);
    },

    socialResult : function (selector) {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $(selector + " thead").prepend("<tr><th colspan='3'>Ожидаемые значения показателя</th></tr>");
        this.prependWithRowspan(th.eq(0), tr);
        this.appendWithRowspan(th.eq(4), tr);
    },

    beneficiaryImprovement : function (selector) {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $(selector + " thead").prepend("<tr><th colspan='3'>Ожидаемые значения показателя, кол-во детей</th></tr>");
        this.prependWithRowspan(th.eq(0), tr);
        this.appendWithRowspan(th.eq(4), tr);
    },

    otherBeneficiary : function(selector) {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $(selector + " thead").prepend("<tr><th colspan='3'>Ожидаемые значения показателя, кол-во детей</th></tr>");
        this.prependWithRowspan(th.eq(1), tr);
        this.prependWithRowspan(th.eq(0), tr);
        this.appendWithRowspan(th.eq(5), tr);
        this.appendWithRowspan(th.eq(6), tr);
    },

    immediateResults : function(selector) {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $("<tr>" +
            "<th colspan='4'>Целевые значения</th>" +
            "</tr>")
            .insertBefore(tr);

        $("<tr>" +
            "<th colspan='2'>На конец I года</th>" +
            "<th colspan='2'>На конец III года</th>" +
            "</tr>")
            .insertAfter(tr);

        this.prependWithRowspan(th.eq(2), tr, 3);
        this.prependWithRowspan(th.eq(1), tr, 3);
        this.prependWithRowspan(th.eq(0), tr, 3);
        this.appendWithRowspan(th.eq(7), tr, 3);
        this.appendWithRowspan(th.eq(8), tr, 3);
    },

    practiceActivityIndexes : function (selector) {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $("<tr>" +
            "<th colspan='2'>Результаты и целевые значения </th>" +
            "</tr>")
            .insertBefore(tr);

        this.prependWithRowspan(th.eq(1), tr);
        this.prependWithRowspan(th.eq(0), tr);
        this.appendWithRowspan(th.eq(4), tr);
        this.appendWithRowspan(th.eq(5), tr);
    },

    expenses : function () {
        arguments[0].find('thead:first th:first').hide();
        arguments[0].find('tbody:first > tr').each(function() {
            $(this).find('td:first').hide();
        });
    },

    expectedResults : function () {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $("<tr>" +
            "<th colspan='4'>Целевые значения</th>" +
            "</tr>")
            .insertBefore(tr);

        $("<tr>" +
            "<th colspan='2'>Факт 2017 г.</th>" +
            "<th colspan='2'>План 2018 г</th>" +
            "</tr>")
            .insertAfter(tr);

        this.prependWithRowspan(th.eq(6), tr, 3);
        this.prependWithRowspan(th.eq(4), tr, 3);
        this.prependWithRowspan(th.eq(3), tr, 3);
        this.prependWithRowspan(th.eq(2), tr, 3);
        this.prependWithRowspan(th.eq(1), tr, 3);
        this.prependWithRowspan(th.eq(0), tr, 3);
        this.appendWithRowspan(th.eq(11), tr, 3);
        this.appendWithRowspan(th.eq(12), tr, 3);
        this.appendWithRowspan(th.eq(13), tr, 3);
        this.appendWithRowspan(th.eq(14), tr, 3);
    },

    practiceResults : function () {
        var tr = "div[id$='_" + arguments[1] + "'] thead tr:first",
            th = arguments[0].find("thead tr:last th");

        $("<tr>" +
            "<th colspan='4'>Целевые значения</th>" +
            "</tr>")
            .insertBefore(tr);

        $("<tr>" +
            "<th colspan='2'>Факт 2017 г.</th>" +
            "<th colspan='2'>План 2018 г</th>" +
            "</tr>")
            .insertAfter(tr);

        this.prependWithRowspan(th.eq(7), tr, 3);
        this.prependWithRowspan(th.eq(5), tr, 3);
        this.prependWithRowspan(th.eq(4), tr, 3);
        this.prependWithRowspan(th.eq(3), tr, 3);
        this.prependWithRowspan(th.eq(2), tr, 3);
        this.prependWithRowspan(th.eq(1), tr, 3);
        this.prependWithRowspan(th.eq(0), tr, 3);
        this.appendWithRowspan(th.eq(12), tr, 3);
        this.appendWithRowspan(th.eq(13), tr, 3);
        this.appendWithRowspan(th.eq(14), tr, 3);
        this.appendWithRowspan(th.eq(15), tr, 3);
    }
};

