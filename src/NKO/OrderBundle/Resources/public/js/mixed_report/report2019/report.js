'use strict';

$(document).ready(function () {
    var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''};
    var autoNumericOptions2 = { lZero: 'keep', aSep: '', altDec: ',', mDec: "0"};
    var values = 'input[id$="Value"]';
    $(values).autoNumeric('init', autoNumericOptions2);

    initAutoNumeric();
    autosize($('textarea'));

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('mixedResults').configs({
            'removeTracking' : true
        })
        .table('mixedEmployeeResults').configs({
            'countRowThead' : 2,
            'autoNumeric' : {
                'input[id$="Value"]' : autoNumericOptions2
            }
        })
        .table('mixedBeneficiariesResults').configs({
            'countRowThead' : 2,
            'autoNumeric' : {
                'input[id$="Value"]' : autoNumericOptions2
            }
        })
        .table('documents').configs({
            'columnName' : '1.Номер',
            'contentFile' : true
        })
        .table('analyticAttachments').configs({
            'contentFile' : true
        })
        .table('financeSpent').configs({
            'removeTracking' : true
        });
    tableManager.process();

    $("div[id$='_mixedResults'] tbody tr:nth-child(1) td:nth-child(4) input").attr("disabled","disabled");
    $("div[id$='_mixedResults'] tbody tr:nth-child(2) td:nth-child(2) input").attr("disabled","disabled");
    $("div[id$='_mixedResults'] tbody tr:nth-child(2) td:nth-child(3) input").attr("disabled","disabled");


    var actions = function() {
        sumInIncrementalCosts();
        sumInBalanceMixed();
        sumInFinanceSpentTable(6);
    };

    //Изначальные вычисления
    addTrToFinanceSpentTable(3);
    actions();
    reformatTableName("titleWithNumber");

    //пересчёт значений при измении значения инпута с суммой register
    $('body').on('keyup paste change', sumInput, function(){
        $(this).autoNumeric('init', autoNumericOptions);
        countSums(($(this).attr('id')), actions); //текущий input суммы register
    });
    setTableHead('documents', '1.Номер');
    setTableHead('analyticAttachments', '№ п/п');
    $("select[id*='_mixedResults'][id*='_measurementMethod']").select2({placeholder: 'Не выбран', width : '100%'}).trigger('change');
});

function setTableHead(selector, firstTd) {
    $("div[id$='" + selector + "'] thead tr th:nth-child(1)").html(firstTd);
}