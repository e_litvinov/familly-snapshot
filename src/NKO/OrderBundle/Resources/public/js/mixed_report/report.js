'use strict';

$(document).ready(function () {
    var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''};
    initAutoNumeric();
    autosize($('textarea'));

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('mixedResults').configs({
            'removeTracking' : true
        })
        .table('mixedEmployeeResults').configs({
            'countRowThead' : 2
        })
        .table('mixedBeneficiariesResults').configs({
            'countRowThead' : 2
        })
        .table('documents').configs({
            'columnName' : '1.Номер',
            'contentFile' : true
        })
        .table('analyticAttachments').configs({
            'contentFile' : true
        })
        .table('financeSpent').configs({
            'removeTracking' : true
        });
    tableManager.process();

    $("div[id$='_mixedResults'] tbody tr:nth-child(1) td:nth-child(4) input").attr("disabled","disabled");
    $("div[id$='_mixedResults'] tbody tr:nth-child(2) td:nth-child(2) input").attr("disabled","disabled");
    $("div[id$='_mixedResults'] tbody tr:nth-child(2) td:nth-child(3) input").attr("disabled","disabled");

    var actions = function() {
        sumInBalanceMixed();
        sumInFinanceSpentTable(6);
    };

    reformatTableName("titleWithNumber");
    addTrToFinanceSpentTable(3);
    actions();
    disableDateField();
    //пересчёт значений при измении значения инпута с суммой register
    $('body').on('keyup paste change', sumInput, function(){
        $(this).autoNumeric('init', autoNumericOptions);
        countSums(($(this).attr('id')), actions); //текущий input суммы register
    });

    allowOnlyNumeric('input[id$="_count_member"]');
    allowOnlyNumeric('input[id$="Value"]');
});
