'use strict';

$(document).ready(function ($) {
   //селектор каждого таба
    var $tabSelector = $('.col-md-12 .col-md-12');
    // autosize($("textarea"));
    //селектор кнопок на таббаре
    var $tabButtonSelector = $('div.btn-group button[type="button"]');
    var $nextTabBtn = $("#next-tab");
    var tabsCount = $tabSelector.length;

    //скрывает содержимое всех табов кроме первого
    $tabSelector.each(function(index)
    {
        if ($.cookie('tabBarIndex')) {
            index == $.cookie('tabBarIndex') ? $(this).show() : $(this).hide();
        } else {
            index == 0 ? $(this).show() : $(this).hide();
        }
    });

    $tabButtonSelector.each(function(index)
    {
        $(this).removeClass("btn-info active")
            .addClass("btn-default");
        if($.cookie('tabBarIndex')){
            if(index == $.cookie('tabBarIndex')) {
                $(this).removeClass("btn-default")
                    .addClass("btn-info active");
            }
        }
        else {
            if(index == 0) {
                $(this).removeClass("btn-default")
                    .addClass("btn-info active");
            }
        }

    });

    $("select[id$='_monitoringResult']").select2({width : "250px"});
    $("select[id$='_period']").select2({width : "250px"});
    $("select[id$='_index']").closest("td").css("width", "30%");
    $("select[id$='_index']").select2({width : "350px"});
    //при выборе файла текст видимым
    $('input[type="file"]').change(function() {
        $(this).css('color', 'black');
    });

    //отключение редактирования определнных ячеек таблицы
    // $('input[id$="immediateResults_0_expectedValue"]').prop("disabled", true);
    // $('input[id$="immediateResults_1_planValue"]').prop("disabled", true);
    // $('input[id$="immediateResults_1_factValue"]').prop("disabled", true);

    $('[class*="socialResultIndexes-indexName"] textarea[id$="_indexName"]').attr('readonly', true);
    $('[class*="beneficiaryImprovementIndexes-indexName"] textarea[id$="_indexName"]').attr('readonly', true);
    $('[class*="additionalIndicatorIndexes-indexName"] textarea[id$="_indexName"]').attr('readonly', true);
    $('[class*="publicationIndexes-indexName"] textarea[id$="_indexName"]').attr('readonly', true);
    $('[class*="introductionIndexes-indexName"] textarea[id$="_indexName"]').attr('readonly', true);
    $('[class*="organizationResources-name"] input[id$="_name"]').attr('readonly', true);

    handleTabButtonClick();
    addErrorsNumberToTab();

    formatFundsFlowTable();
    // addValidationToImmediateResultsTable();

    /**
     * Функция обработки клика кнопки таббара
     */
    function handleTabButtonClick()
    {
        $tabButtonSelector.click(function(){
            //определяет индекс кликнутой кнопки
            //(индекс кнопки совпадает с индексом отображаемого блока)
            var indexToShow = $(this).index();

            $.cookie('tabBarIndex', indexToShow);

            //отображает только блок, соответствующий нажатой кнопке
            $tabSelector.each(function(index){
                index == indexToShow ? $(this).show() : $(this).hide();
            });

            //делает все кнопки таббара неактивными
            $tabButtonSelector.removeClass("btn-info active")
                .addClass("btn-default");

            //делает нажатую кнопку таббара активной
            $(this).removeClass("btn-default")
                .addClass("btn-info active");

            if (indexToShow < (tabsCount - 1))
                $nextTabBtn.show();

            // autosize.update($('textarea'));
        });
    }

    /**
     * Функция добавления количество ошибок на кнопки таббара
     */
    function addErrorsNumberToTab()
    {
        //селектор таббара
        var $tabSelector = $('.col-md-12 .col-md-12');
        var text = 'ошибок';
        //проходит по всем таббарам
        $tabSelector.each(function(index){

            //количество ошибок в открытой вкладке
            var errors = $(this).find('div.help-inline.sonata-ba-field-error-messages').length
                + $(this).find('div.help-block.sonata-ba-field-error-messages').length;

            if(window.location.href.indexOf("report") != -1 &&
                window.location.href.indexOf("edit?uniqid") != -1) {
                $(this).find("div[id$='_immediateResults'] input").each(function(){
                    if($(this).val() == '' && !$(this).is(":disabled")) {
                        errors++;
                    }
                });
            }

            //если есть ошибки
            if(errors > 0){
                //проверяет на количество ошибок и в зависимости от этого подставляет текст в нужном падеже
                if(errors >= 10 && errors <= 20){
                    text = 'ошибок';
                }
                else if(errors % 10 == 1)
                {
                    text = 'ошибка';
                }
                else if(errors % 10 > 1 && errors % 10 < 5) {
                    text = 'ошибки';
                }
                else {
                    text = 'ошибок';
                }
                //вставка текста с количеством ошибок в таббар
                $('div.btn-group button[type="button"]').eq(index).append('<br><h6 style="color:#a94442;">' +
                    '(<i class="fa fa-exclamation-circle">&nbsp;' +  errors  + ' ' + text +'</i>)</h6>');

            }
        });
    }

    // --- Форматирование таблицы Непосредственные результаты ---
    // var immediateResultTheadTr = "span[id$='_immediateResults'] thead tr:first",
    //     $immediateResultTheadTh = $("span[id$='_immediateResults'] thead tr:last th");
    //
    // $("span[id$='_immediateResults'] thead").prepend("<tr><th colspan='3'>Целевые значения</th></tr>");
    //
    // $immediateResultTheadTh.prependTo("span[id$='_immediateResults'] thead tr:last")
    //
    // prependWithRowspan($immediateResultTheadTh.eq(0), immediateResultTheadTr);
    // appendWithRowspan($immediateResultTheadTh.eq(4), immediateResultTheadTr);
    // appendWithRowspan($immediateResultTheadTh.eq(5), immediateResultTheadTr);
    //
    // function prependWithRowspan($selector, to)
    // {
    //     $selector.prependTo(to);
    //     $selector.attr("rowspan", 2);
    // }
    //
    // function appendWithRowspan($selector, to)
    // {
    //     $selector.appendTo(to);
    //     $selector.attr("rowspan", 2);
    // }
    // --- Конец Форматирования таблицы Непосредственные результаты ---

    /**
     * Форматирование таблицы Движение денежных средств
     */
    function formatFundsFlowTable()
    {
        var $receiptOfDonations = $("div[id*='_receiptOfDonations']"),
            $periodCosts = $("div[id*='_periodCosts']"),
            $donationBalance = $("div[id*='_donationBalance']");

        $("div[id*='_receiptOfDonations'], div[id*='_periodCosts'], div[id*='_donationBalance']")
            .wrapAll("<div class='field-container'>" +
                "<table class='table table-bordered' id='fundsFlowTable'>" +
                "<thead>" +
                "<tr>" +
                "<th></th>" +
                "<th><b>Сумма в рублях</b></th>" +
                "</tr>" +
                "</thead>" +
                "<tbody>" +
                "<tr>" +
                "<td></td>" +
                "<td></td>" +
                "</tr>" +
                "<tr>" +
                "<td></td>" +
                "<td></td>" +
                "</tr>" +
                "<tr>" +
                "<td></td>" +
                "<td></td>" +
                "</tr>" +
                "</tbody>" +
                "</table>" +
                "</div>");
        $receiptOfDonations.find("label").appendTo("#fundsFlowTable tbody tr:first td:first");
        $receiptOfDonations.find(".sonata-ba-field").appendTo("#fundsFlowTable tbody tr:first td:last");

        $periodCosts.find("label").appendTo("#fundsFlowTable tbody tr:nth-child(2) td:first");
        $periodCosts.find(".sonata-ba-field").appendTo("#fundsFlowTable tbody tr:nth-child(2) td:last");

        $donationBalance.find("label").appendTo("#fundsFlowTable tbody tr:last td:first");
        $donationBalance.find(".sonata-ba-field").appendTo("#fundsFlowTable tbody tr:last td:last");
    }

    // function addValidationToImmediateResultsTable()
    // {
    //
    //     var errorMessage = '<div class="help-inline sonata-ba-field-error-messages">' +
    //         '<ul class="list-unstyled">' +
    //         '<li><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Неверное значение (поле должно быть не пустым)</li>' +
    //         '</ul>' +
    //         '</div>';
    //
    //     $("div[id$='_immediateResults'] input, input[id$='count_member']").autoNumeric('init', {vMin: 0, mDec: 0, wEmpty: 'zero', lZero: 'keep'});
    //
    //     $("input[id$='_sum'], input[id*='financeSpent']").not(":disabled").autoNumeric('init', {aSep: "", aDec: ".", wEmpty: 'zero', lZero: 'keep'});
    //
    //     if(window.location.href.indexOf("report") != -1 &&
    //         window.location.href.indexOf("edit?uniqid") != -1) {
    //         $("div[id$='_immediateResults'] input").each(function(){
    //             if($(this).val() == '' && !$(this).is(":disabled")) {
    //                 $(this).parent().append(errorMessage);
    //             }
    //         });
    //     }
    // }

    $("#next-tab").on("click", function(e)
    {
        e.preventDefault();
        $tabButtonSelector.each(function(index){

            if(index < (tabsCount - 1) && $(this).hasClass("active")) {

                $(this).removeClass("btn-info active")
                    .addClass("btn-default");

                $tabButtonSelector.eq(index + 1)
                    .removeClass("btn-default")
                    .addClass("btn-info active")
                    .trigger("click");

                return false;
            }
            else {
                $nextTabBtn.hide();
            }
        });
    });

    $("select.brief_application_practice_region").closest("div.form-group").css("width", "100%");

    // $("input[id$='_targetGroup'], input[id*='_socialResults_'][id$='_name']").attr("readonly", true);

    $("div[id$='_socialResults']").find("thead tr th:first").hide();
    $("div[id$='_socialResults']").find("tbody tr").each(function(){
        $(this).find("td:first").hide();
    });

    addSelect2Limit($("select[id$='_socialResults']"));

    function addSelect2Limit($selector) {
        var selectLimit = parseInt($selector.attr("limit"));
        $selector.select2({
            maximumSelectionSize: selectLimit,
            width: "100%"
        });
    }

    $("input[id$='_achievedValue']").autoNumeric('init', {vMin: 0, mDec: 0, lZero: 'keep', aSep: ''});
    $("input[id$='_percent']").autoNumeric('init', {vMin: '0.00', vMax: '100.00', lZero: 'keep', aSep: ''});

    // (function () {
    //     var $priorityDirectionLi = $("ul[id$='_priorityDirection'] li:not(':first')"),
    //         $priorityDirectionInput = $("input[id$='_priorityDirectionEtc']"),
    //         $priorityDirectionDiv = $("div[id$='_priorityDirection']");
    //
    //     if($priorityDirectionInput.val() !== "")
    //         $priorityDirectionDiv.find("input[type='radio']").iCheck('disable');
    //
    //     $priorityDirectionLi.each(function(){
    //         if($(this).find(".iradio_square-blue").hasClass("checked")){
    //             $priorityDirectionInput.attr("readonly", true);
    //         }
    //     });
    //
    //     $priorityDirectionDiv.find("input[type='radio']").on("ifChecked", function(){
    //         if($(this).closest('li').index() > 0){
    //             $priorityDirectionInput.attr("readonly", true);
    //             $priorityDirectionInput.val("");
    //         }
    //         else
    //             $priorityDirectionInput.attr("readonly", false);
    //     });
    //
    //     $priorityDirectionInput.on("change", function(){
    //         if($(this).val() !== "") {
    //             $priorityDirectionDiv.find("input[type='radio']").iCheck('disable');
    //         }
    //         else {
    //             $priorityDirectionDiv.find("input[type='radio']").iCheck('enable');
    //         }
    //     });
    // })();
});
