$(document).ready(function () {
    var reportForm = 'div[id*="bannedReportForm"]';
    var form = '[class="form-group"]';
    var mask = 'div[id*="organizationsAccess"][class="form-group"]';

    var config = {};
    config[reportForm + form] = 'list';

    clearFieldsMaskType(mask, config);
});
