'use strict';

function checkInteger(selector){
    $('body').on('keyup paste change', selector, function(){
        this.value = this.value.replace(/[^0-9]/g, false);
    });
}