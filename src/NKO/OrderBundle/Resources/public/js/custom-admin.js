$(document).ajaxComplete(function () {
    if ($( '.x-editable' ).hasClass('not-last')) {
        $( '.content ' ).prepend("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\"" +
            " data-dismiss=\"alert\" aria-hidden=\"true\">×</button>Не принят финансовый отчёт за предыдущий период</div>")
    }
});


$(document).change(function () {
    $('input[type=file]').each(function() {
        if ($(this).get(0).files.length !== 0) {
            $(this).addClass( "file-here" );
        }
    });
});