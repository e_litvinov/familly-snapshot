$(document).ready(function () {
    choiceTypeOfReport();
});

function showTakingDataFromSpreadApplication(){
    $("div[id*='_isDataTakesFromActualApplication']").show();
}
function showMonitoringReport(){
    $("div[id*='_reportPeriods']").show();
    $("div[id*='_isSelectable']").show();
    $("div[id*='_isTransferable']").show();
}

function showFinanceReport(){
    $("div[id*='_reportPeriods']").show();
    $("div[id*='_expenseTypeConfig']").show();
    $("div[id*='_isFilledByExpert']").show();
}

function hideAllReport(){
    $("div[id*='_isSelectable']").hide();
    $("div[id*='_isTransferable']").hide();
    $("div[id*='_reportPeriods']").hide();
    $("div[id*='_expenseTypeConfig']").hide();
    $("div[id*='_isFilledByExpert']").hide();
    $("div[id*='_isDataTakesFromActualApplication']").hide();
}

function takeSelectedType(){
    return $("div[id*='_reportClass'][class*='container']").children("a[class*='select2-choice']").children("[id*='select2-chosen-1']").text();
    //return $("[id*='select2-chosen-1']").text();
}

function setForms(thisType, types){
    switch (thisType){
        case types["NKO\\OrderBundle\\Entity\\Report\\MonitoringReport\\Report"]:
        case types["NKO\\OrderBundle\\Entity\\Report\\MonitoringReport\\Harbor2020Report\\Report"]:
            showMonitoringReport();
            break;
        case types["NKO\\OrderBundle\\Entity\\Report\\FinanceReport\\Report"]:
        case types["NKO\\OrderBundle\\Entity\\Report\\FinanceReport\\Report2018\\Report"]:
        case types["NKO\\OrderBundle\\Entity\\Report\\FinanceReport\\Report2019\\Report"]:
            showFinanceReport();
            break;
        case types["NKO\\OrderBundle\\Entity\\Report\\MixedReport\\KNS\\Report"]:
            showFinanceReport();
            break;
        case types["NKO\\OrderBundle\\Entity\\Report\\MixedReport\\KNS\\Report2019\\Report"]:
        case types["NKO\\OrderBundle\\Entity\\Report\\MixedReport\\KNS\\Report2020\\Report"]:
            showFinanceReport();
            showTakingDataFromSpreadApplication();
            break;
        default:
    }
}

function getAllTypes(){
    var options = [];
    var selects = $("select[id*='_reportClass']").children("option").each(function () {
        options[this.value] = this.text;
    });
    return options;
}

function choiceTypeOfReport(){
    var types = getAllTypes();
    hideAllReport();
    setForms(takeSelectedType(), types);
    $("select[id*='_reportClass']").change(function () {
        hideAllReport();
        setForms(takeSelectedType(), types);
    })
}