'use strict';

$(document).ready(function ($) {

    var url = Routing.generate('report_periods_path');

    $("button").click(function(e) {
        var value = $(this).attr('index');
        var reportPath = $(this).attr('reportPath');

        var periodLinks = $('div[id="period-' + value + '"]');
        var messages = $('p[id="message-' + value + '"]');
        $.ajax({
            url: url,
            type: "POST",
            data: value,
            success: function(result) {
                periodLinks.html('');
                if (Object.keys(result).length) {
                    for (var key in result) {
                        periodLinks.append(
                            '<a href="' + reportPath + '&period_id=' + key + '">' + result[key] + '</a></br>'
                        );
                    }
                }
                else {
                    messages.css('display', 'inline');
                }
            }
        });
    });
});

