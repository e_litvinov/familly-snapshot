'use strict';

function SelectSizeManager()
{
    var selectSelectors = "a[class*='select2-choice'] span[id*='select2-chosen']",
        self = this;

    self.init = function() {
        setSelectWidth();
        self.deleteSpace();
        setEventHandlers();
    };

    function setEventHandlers(selector) {

        if (selector !== undefined) {
            selector = selector.find("select");
        } else {
            selector = $("select");
        }

        //событие выпадающего списка. Делаем выпадающий список такой жу ширины, что и сам селект
        selector.on('select2-open', function (event) {
            var tagA = $(event.target).siblings(),
                dropDown = $("div[id=select2-drop]");

            $('.select2-results').bind("mousewheel", function() {
                return false;
            });

            $(this).select2('open');
            dropDown.outerWidth(tagA.width());
            dropDown.offset({left: tagA.offset().left});
            dropDown.width('auto');
        });

        //если выбран пункт "Не выбрано" (placeholder), то появляются лишние прбелы. таким образом их удаляем
        selector.on('select2-close', function (event) {
            self.deleteSpace($(event.target).siblings().find('span[id*="select2-chosen"]'));
        });

        //событие выбора/удаления значения в селекте
        selector.on('change', function (event) {
            var tagA = $(event.target).siblings().find('a');
            if (tagA.children('span').length > 0) {
                setSelectHeight(tagA);
            }
        });
    }

    self.deleteSpace = function(selects) {
        if (selects === undefined) {
            selects = $(selectSelectors);
        }

        selects.each(function () {
            $(this).text($(this).text().trim());
        });
    };

    self.initNewRowSelects = function(selector) {
        setSelectWidth(selector);
        setEventHandlers(selector);
    };

    function setSelectHeight(tagA) {
        tagA.find('span').text() !== "" ? tagA.height('auto') : tagA.height('34px');
    }

    function setSelectWidth(event) {
        var selectors;

        if (event !== undefined) {
            selectors = $(event).closest('tbody').find("a span[id*=select2-chosen]");
        } else {
            selectors = $("a span[id*=select2-chosen]");
        }

        selectors.each(function() {
            if (!$(this).parent().closest('div').hasClass('entity_readonly')) {
                $(this).css("cssText", "white-space: pre-line !important;");
                $(this).closest('div').css("cssText", "width: 100% !important;");
                setSelectHeight($(this).parent());
            }
        });
    }
}

