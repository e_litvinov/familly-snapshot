function fileNameLoader(documentsTableBody) {
    $(documentsTableBody).find('tr').each(function () {
        var splits = [];
        var href;

        href = $(this).find('div[class*="itm_file_preview_ico_c"] a').attr('href');
        if (href !== undefined) {
            splits = href.split('/');
            $(this).find('input[id*="fileName"]').val(splits[splits.length - 1]);
        }
    });
}