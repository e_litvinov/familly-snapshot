'use strict';

$(document).ready(function () {
    $(window).load(function() {
        $('.spinner').hide();
        $('.overlay').hide();
    });

    $('body').on('click', function (event) {
        var $targetObject = $(event.target),
            targetTag = $targetObject.prop('tagName'),
            href = $targetObject.attr('href'),
            id = $targetObject.attr('id');

        if ((targetTag === 'A' && (href && href !=='#') && !$targetObject.attr('target') && (id && id.indexOf('cke') == -1)) ||
            (targetTag === 'BUTTON') && $targetObject.attr('type') === 'submit' && !$targetObject.hasClass('editable-submit')) {
            $('.overlay').show();
            $('.spinner').show();
        }
    });
});

