'use strict';


var url = Routing.generate('organizations_list_path');
/**
 *  Сырой вариант
 */
$(document).ready(function ($) {

    var $radioBtn = $("div[id*='_traineeshipTopic'] .iCheck-helper");
    var $traineeshipTopic = $("div[id*='_traineeshipTopic']");

    var $organizationSelector = $('body').find("div[id$='_organization']");
    var $organizationsList = $('body').find("ul[id$='_organization'] li");
    var organizationFormId = '#'+$organizationSelector.attr('id');

    /**
     * вставляет список организаций после чекнутого элемента списка стажировок
     * со смещением вправо
     */
    $radioBtn.parent().each(function() {
        if($(this).hasClass('checked')) {
           if($organizationsList.length == 1){
               getOrganizationsList($(this))
            }

            $organizationSelector.css('margin-left', '50px').insertAfter($(this).closest('label'));
            $organizationSelector.children('label').remove();
            $organizationSelector.find('li:first').remove();
        }
    });

    $traineeshipTopic.find('li:first').hide();
    var $radioLabelSelector = $("div[id*='_traineeshipTopic'] > div > div > span > ul > li > div.radio > label");

    var checkedId = "";

    //поиск id чекнутого элемента списка стажировок
    if($radioLabelSelector.find('.checked')) {
        checkedId = $radioLabelSelector.find('.checked input').attr('id');
    }

    //обработка клика radiobutton списка стажировок
    $radioBtn.click(function(){
        $(this).closest('label').trigger('click');
    });

    /**
     *  обработка клика на элемент списка стажировок
     *  выолнение ajax запроса и обработка пришедших данных
     */
    $radioLabelSelector.click(function(){

        var $selector = $(this);

        // если данный селектор содержит чекнутый radiobutton и
        // id radiobuttonа не совпадает с id элемента, который был чекнут при загрузке страницы
        if ($selector.find('.checked') && $selector.find('input').attr('id') != checkedId) {
            checkedId = "";
            //поиск ближайшей формы
            var $form = $(this).closest('form');

            getOrganizationsList($selector);
        }
    });

    /**
     * Метод добавления стилей к блоку со списком организаций
     * @param $selector
     */
    function addStylesToOrganizationsBlock($selector) {
        $selector.wrapInner('<ul class="list-unstyled"></ul>');
        $selector.find('input').each(function(){
            var $nextLabel = $(this).next('label');

            $nextLabel.andSelf().wrapInner(
                '<li><div class="radio"><label></label></div></li>'
            );

            var text = $nextLabel.text();
            $nextLabel.replaceWith(
                '<span class="control-label__text">' + text +'</span>'
            );
        });

        //удаление надписей с текстом organization и organization name
        $('div[id$="_organization"] label.control-label.required').remove();
        $('div[id$="_organizationName"] > label').remove();
        $('div[id$="_organizationName"]  li:first').remove();

        $(organizationFormId + ' input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    }

    function getOrganizationsList($selector) {
        var data;
        // установка данных для передачи
        // данные содержат поле value чекнутого radiobuttona
        data = $selector.find('input').attr('value');

        $.ajax({
            url: url,
            type: 'post',
            data: data,
            dataType: 'text',
            success: function (html) {
                //удаляет содержимое предыдущего блока со списком организаций
                $organizationSelector.children().remove();

                //вставляет пришедшие данные в блок организаций
                $(organizationFormId).html(html);

                //добавляет стили к блоку организаций
                addStylesToOrganizationsBlock($('#organization_organizationName'));

                //удаление неиспользуемого скрытого инпута,
                //если не удалить, то при сабмите формы передаются неправильные данные
                $(organizationFormId + ' input[type="hidden"]').remove();

                //уникальная часть id блока организаций
                var uniqTextFromId = organizationFormId.split('_')[0].split('-')[4];

                //добавление к name инпута уникальной части блока
                $(organizationFormId + ' input').attr('name', uniqTextFromId + '[organization]');

                //сдвиг списка организаций вправо
                $selector.parent().append(
                    $organizationSelector.css('margin-left', '50px')
                );
            }
        });
    }
});
