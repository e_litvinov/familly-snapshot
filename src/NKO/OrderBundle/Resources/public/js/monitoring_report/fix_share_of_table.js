'use strict';

function fixShareTable(selector) {
    $(selector).closest('.box-primary').addClass('fix-share');
    // закрепление шапки при скролинге вверх
    var upwardMovement;
    $(".fix-share").scroll(function () {
        var position = $(".fix-share").scrollTop();
        if ($(".fix-share").scrollTop() > 40) {
            $(".fix-share thead th").css('top', position - 40);
        } else if (position < upwardMovement) {
            $(".fix-share thead th").css('top', position);
        }
        upwardMovement = position;
    });

    //запрепление первых двух столбцов таблицы при скроллинге вправо
    $(".fix-share thead tr:first-child th:first-child").attr('class', 'two-fields-header');
    $(".fix-share thead tr:first-child th:nth-child(2)").attr('class', 'two-fields-header');
    $(".fix-share tbody tr:not(:first-child) td:first-child").attr('class', 'first-column-table');
    $(".fix-share tbody tr:not(:first-child) td:nth-child(2)").attr('class', 'second-column-table');

    $(".fix-share").scroll(function () {
        var position = $(".fix-share").scrollLeft();
        $(".fix-share tbody tr:not(:first-child) td:first-child").css('left', position);
        $(".fix-share tbody tr:not(:first-child) td:nth-child(2)").css('left', position);
        $(".fix-share thead tr:first-child th:first-child").css('left', position);
        $(".fix-share thead tr:first-child th:nth-child(2)").css('left', position);
    });
}