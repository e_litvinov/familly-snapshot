'use strict';

function onChangeCustomIndicatorHandler() {
    $('textarea[id$="_indicator_title"]').on('change', function () {
        updateSelectData();
    });
}