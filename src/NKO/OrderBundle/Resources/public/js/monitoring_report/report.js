
$(document).ready(function ($) {
    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('documents').configs({
            'contentFile' : true
        })
        .table('monitoringResults').configs({
            'removeTracking' : true
        });
    tableManager.process();

    fixShareTable('.monitoring-table');

    inputHandlers();
    updateCustomIndicators();
    updateReportValues();
    colorColumn();

    updateSelectData();
    onChangeCustomIndicatorHandler();
});