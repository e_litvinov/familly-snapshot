'use strict';

var $monitoringResultTableBody = $('div[id$="monitoringResults"] table tbody');
var countCodes = [
    'family',
    'children',
    'graduates',
    'parent',
    'expert',
    'organization'
];

var blueArray = ['1', '1.1', '1.2', '1.3', '2', '2.1', '2.2', '3', '3.1', '3.2', '4', '4.1', '4.2',
'5', '5.1', '5.2', '5.3', '5.4', '5.5', '5.6', '13', '13.1', '13.2', '14', '14.1', '14.2', '14.3', '14.4', '14.5', '14.6',
'15', '15.1'];

var greenArray = ['6', '6.1', '6.2', '6.3', '7', '7.1', '7.2', '7.3', '8', '8.1', '8.2', '8.3', '9', '10', '11',
'11.1', '11.2', '11.3', '11.4', '12', '12.1', '12.2', '12.3', '12.4'];

var autoNumericOptions = { vMin: '-9999999', mDec: "0", lZero: 'keep', aSep: ''};
var totalAmount = 'input[id*="_totalAmount"]';
var newAmount = 'input[id*="_newAmount"]';
var totalValue = 'input[id*="_totalValue"]';
var $isPlanAccepted = $("input[id$='isPlanAccepted']");
var $isSelecteble = $("input[id$='isSelecteble']");



function inputHandlers() {
    $(totalAmount).autoNumeric('init', autoNumericOptions);
    $(newAmount).autoNumeric('init', autoNumericOptions);
    $(totalValue).autoNumeric('init', autoNumericOptions);

    $('body').on('keyup paste change', newAmount, function(){
        updateReportValues();
    });

    $('body').on('keyup paste change', totalAmount, function(){
        updateReportValues();
    });

    $('[id*="expand"]').on("click", function() {
        $(this).closest("div").addClass("fullscreen-textarea");
    });
    $('[id*="close"]').on("click", function() {
        $(this).closest("div").removeClass("fullscreen-textarea");
    });
}

function removeReadonly(selector) {
    $(selector).attr('readonly', false);
}

function updateCustomIndicators() {
    var isSelectable = $isSelecteble ? $isSelecteble.val() : undefined;
    var isPlanAccepted = $isPlanAccepted ? $isPlanAccepted.val() : undefined;
    $monitoringResultTableBody.find('input[id$=_code][value="custom"]').closest('td').each(function () {
        var textArea = $(this).find('textarea');
        var planValue = undefined;
        if(isSelectable && isPlanAccepted) {                    //Для мониторинговых 2018 + если приняты показатели
            planValue = $(this).parent().find('input[id*="totalAmount"]').filter(':last').val();
            if (planValue === undefined || planValue == 0) {
                removeReadonly(textArea);
            }
        } else if(!isSelectable) {
            planValue = $(this).find('input[id*="totalAmount"]').filter(':last').val();
            if (planValue === undefined || planValue == 0) {
                removeReadonly(textArea);
            }
        } else {                                               //Для мониторинговых 2018 + если не приняты показатели
            removeReadonly(textArea);
        }
    });
}

function colorColumn() {
    $('.monitoring-table tr').each(function(){
        // if($(this).find("td:first").text().match(/^[1-5]+(\.\d+)?(?=$| )/)) {
        //     $(this).find("td:first").css('background-color','blue');
        //     console.log($(this).find("td:first").text());
        // };

        if(blueArray.indexOf($(this).find("td:first").text()) != -1) {
            $(this).find("td:first").css('background-color','#3366FF');
        }
        if(greenArray.indexOf($(this).find("td:first").text()) != -1) {
            $(this).find("td:first").css('background-color','#66FF33');
        }
    });

    $('.monitoring-table th').each(function(){
        if($(this).text() == 'План') {
            var index = $(this).index() + 3;
            $('.monitoring-table td:nth-child(' + index + ')').css('background-color','#ffff33');
        }
    });

}

function updateReportValues() {
    sumNewAmount();
    sumPeriodTotalAmount();
    setTotalProjectValue();
    countCodes.forEach(function (item) {
        sumTotalValuesByCode(item);
    });
}

function sumNewAmount() {
    $monitoringResultTableBody.find('tr').each(function(){
        var sum = 0;
        $(this).find(newAmount).not(':last').each(function() {
            var value = $(this).val() || 0;
                sum += parseInt(value);
        });

        $(this).find(newAmount + ':last').val(sum);
    });
}

function sumTotalValuesByCode(code) {
    var codeInput = 'input[id$="_code"][value="' + code + '"]';
    var periodResultInput = 'input[id*="_periodResults"]';
    var totalValues = {};

    $monitoringResultTableBody.find(codeInput + ':not(:last)').closest('tr').each(function(){
        $(this).find(periodResultInput).each(function (index) {
            var value = $(this).val() || 0;
            if(!(index in totalValues)) {
                totalValues[index] = parseInt(value);
            } else {
                totalValues[index] += parseInt(value);
            }
        });
    });

    $monitoringResultTableBody.find(codeInput + ':last').closest('tr')
        .find(periodResultInput).each(function (index) {
            $(this).val(totalValues[index]);
    });
}

function sumPeriodTotalAmount() {
    var newAmountValues = {};

    $monitoringResultTableBody.find('input').closest('tr').each(function(){
        $(this).find(newAmount).not(':last').each(function (index) {
            var value = $(this).val() || 0;
            newAmountValues[index] = parseInt(value);
        });

        $(this).find(totalAmount).not(':last').each(function (index) {
            var value = 0;

            for (var key in newAmountValues) {
                if (key <= index) {
                    value += newAmountValues[key];
                }
            }

            $(this).val(value);
        });
    });
}

function setTotalProjectValue() {
    var prevTotalValueInput = 'input[id*="_prevTotalValue"]';

    $monitoringResultTableBody.find('input').closest('tr').each(function(){
        var value = $(this).find(newAmount + ':last').val() || 0;
        var prevValue = $(this).find(prevTotalValueInput).val() || 0;
        $(this).find(totalValue).val(parseInt(value) + parseInt(prevValue));
    });
    }