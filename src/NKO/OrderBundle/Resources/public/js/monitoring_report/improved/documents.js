'use strict';
var monitoringResultSelect = 'select[id*="documents"][id*="monitoringResult"]';
var periodSelect = 'select[id*="documents"][id*="period"]';

function setSelectorRelatedHandlers() {
    $(isVisibleInput).on('ifChecked', function () {
        var resultId;

        resultId = getIndicatorId($(this));
        hideOption(monitoringResultSelect, resultId);
    });

    $(isVisibleInput).on('ifUnchecked', function () {
        var resultId;

        resultId = getIndicatorId($(this));
        showOption(monitoringResultSelect, resultId);
    });

    $(periodInput).on('ifChecked', function () {
        hideOption(periodSelect, $(this).val());
    });

    $(periodInput).on('ifUnchecked', function () {
        showOption(periodSelect, $(this).val());
    });

    var observer = new MutationObserver(function(mutations) {
        setEnablePeriod();
    });
    observer.observe($('div[id$="documents"]:eq(0) tbody')[0], {subtree: false, attributes:true});
}

function select2OpenHandler(select) {
    $('body').on('select2-open', select, function (e) {
        var options;

        options = $(this).find('option').filter(function () {
            if ($(this).val()) {
                return true;
            }

            return false;
        });

        $('div[id*="select2-drop"]').find('li').each(function (index) {
            if ($(options[index]).attr('hidden') == 'hidden') {
                $(this).remove();
            }
        });
    });
}

function hideOption(select, resultId) {
    $('body').find(select).each( function () {
        var option;

        var action = function() {
            var parent = $(option).parent();
            if (parent.val() == $(option).val()) {
                parent.select2("val", "");
            }
            $(option).attr('hidden', true);
        };

        if (resultId.constructor === Array) {
            for (var i = 0; i < resultId.length; i++) {
                option = $(this).find('option[value=' + resultId[i] + ']');
                action(option);
            }
            return;
        }
        option = $(this).find('option[value=' + resultId + ']');
        action(option);

    });
    setEnablePeriod()
}

function showOption(select, resultId) {
    $('body').find(select).each( function () {
        $(this).find('option[value=' + resultId + ']').removeAttr('hidden');
    });
}

function selectedValue($select) {
    var isSelected = false;
    $select.find('option').each(function () {
        if ($(this).attr('selected') == 'selected') {
            isSelected = true;
        }
    });

    return isSelected;
}


function setEnablePeriod() {
    var periodTds = $('div[id$="documents"]:eq(0)').find('td[class$="period control-group"]');
    periodTds.each(function () {
        if ($(this).find("option:selected").length === 0) {
            var option = $(this).find("option:not([hidden]):eq(0)").val();
            $(this).find('select').val(option).trigger('change')
        }
    });


}