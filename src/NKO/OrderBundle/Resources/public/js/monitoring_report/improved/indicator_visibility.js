
var visibilityTableBody = 'div[id*="visibility"] table tbody';
var monitoringResultsTable = 'div[id$="monitoringResults"] table tbody';
var isVisibleInput = 'input[id*="isVisible"]';
var resultIdInput = 'input[id$="_id"]';
var indicatorParentIdInput = 'input[id*="indicator_parentId"]';
var indicatorIdInput = 'input[id$="indicator_id"]';

function setIndicatorVisibilityHandlers() {
    // скрытие строки соотвествующего индикатора при выборе чекбокса
    $(isVisibleInput).on('ifChecked', function () {
        hideRowsByInput(this);
        updateParentIndicator($(this).closest('tr').find(indicatorParentIdInput).val());
    });

    // показ строки соотвествующего индикатора если чекбокс не выбран
    $(isVisibleInput).on('ifUnchecked', function () {
        var id;

        id = getIndicatorId($(this));
        showIndicatorRow(id);
        checkParentIndicator($(this));
        updateParentIndicator($(this).closest('tr').find(indicatorParentIdInput).val());
    });
}

function isNotEmptyInput($input) {
    return ($input.val() != 0 && $input != '');
}

function hideRowsByInput(input) {
    var id;

    id = getIndicatorId($(input));
    hideIndicatorRow(id);
}

function getIndicatorId($input) {
    return $input.closest('tr').find(resultIdInput).val();
}

function hideIndicatorRow(id) {
    $(monitoringResultsTable).find(resultIdInput).each(function () {
        if (id.constructor === Array) {
            if (id.indexOf($(this).val()) != -1) {
                $(this).closest('tr').hide();
            }
            return;
        }

        if ($(this).val() == id) {
            $(this).closest('tr').hide();
        }
    });
}

function showIndicatorRow(id) {
    $(monitoringResultsTable).find(resultIdInput).each(function () {
        if ($(this).val() == id) {
            $(this).closest('tr').show();
        }
    });
}

function updateParentIndicator(parentId) {
    var checkedCount = 0;
    var parentIdInputs;

    parentIdInputs = $(visibilityTableBody).find(indicatorParentIdInput + '[value=' + parentId + ']');
    $(parentIdInputs).each(function() {
        $(this).closest('tr').find(':checked').each(function () {
            checkedCount++;
        });
    });

    if ($(parentIdInputs).length == checkedCount) {
        updateParentIndicatorCheckbox(parentId, 'enable');
    } else {
        updateParentIndicatorCheckbox(parentId, 'disable');
    }
}

function updateParentIndicatorCheckbox(id, method) {
    $(monitoringResultsTable).find(indicatorIdInput).each(function () {
        if (id == $(this).val()) {
            updateIndicatorCheckbox($(this), method);
        }
    });
}

function checkParentIndicator($input) {
    var checkedCount = 0;
    var parentId;
    var parentIdInputs;

    parentId = $input.closest('tr').find(indicatorParentIdInput).val();

    parentIdInputs = $(visibilityTableBody).find(indicatorParentIdInput + '[value=' + parentId + ']');
    $(parentIdInputs).each(function() {
        $(this).closest('tr').find(':checked').each(function () {
            checkedCount++;
        });
    });

    if (($(parentIdInputs).length - checkedCount) == 1) {
        $(visibilityTableBody).find(indicatorIdInput + '[value=' + parentId + ']').closest('tr').find(isVisibleInput).iCheck('uncheck');
    }
}
