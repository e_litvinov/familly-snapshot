'use strict';

$(document).ready(function ($) {
    var visibilityTableBody = 'div[id*="visibility"] table tbody';
    var isVisibleInput = 'input[id*="isVisible"]';
    var newAmountInput = 'input[id*="_newAmount"]';
    var periodInput = 'input[id*="_periods"]';
    var monitoringResultSelect = 'select[id*="documents"][id*="monitoringResult"]';
    var monitoringResultsTable = 'div[id$="monitoringResults"] table tbody';
    var periodSelect = 'select[id*="documents"][id*="period"]';
    var indicatorArr = [],
        periodArr = [];
    var identifierTr;

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('documents').configs({
            'contentFile' : true,
            'changeFirstIndex' : true
        })
        .table('monitoringResults').configs({
            'removeTracking' : true
        })
        .table('visibility').configs({
            'removeTracking' : true
        });
    tableManager.process();

    fixShareTable('.monitoring-table');

    inputHandlers();
    updateCustomIndicators();
    updateReportValues();


    blueArray = [];

    greenArray = [];


    colorColumn();
    updateSelectData();
    onChangeCustomIndicatorHandler();

    //Доступность чекбоксов на управляющей вкладке
    (function () {
        $(monitoringResultsTable).find(newAmountInput).each(function () {

            if ($(this).attr('indicator_id') !== 'undefined') {
                if (isNotEmptyInput($(this))) {
                    addFilledField($(this), true);
                } else if ($(this).attr('indicator_id')!='undefined' && identifierTr !== $(this).attr('indicator_id')) {
                    identifierTr = $(this).attr('indicator_id');
                    if (!isSetTotalValue($(this))) {
                        addFilledField($(this));
                    }
                }
            }
        });

        updateCheckboxes(indicatorArr, periodArr);
    }());

    function addFilledField(selector, both) {
        if (indicatorArr && indicatorArr.indexOf(selector.attr('indicator_id')) == -1) {
            indicatorArr.push(selector.attr('indicator_id'));
        }

        if (both && periodArr && periodArr.indexOf(selector.attr('period_id')) == -1) {
            periodArr.push(selector.attr('period_id'));
        }
    }

    setSelectorRelatedHandlers();
    setIndicatorVisibilityHandlers();
    setPeriodVisibilityHandlers();
    setChangeInputHandler();
    select2OpenHandler(monitoringResultSelect);
    select2OpenHandler(periodSelect);

    var currentParentId;
    $(visibilityTableBody).find('tr:has(input[id*="isVisible"])').each(function (){
        var newParentId = $(this).closest('tr').find(indicatorParentIdInput).val();
        if (newParentId == currentParentId) {
            return;
        } else {
            currentParentId = newParentId;
            updateParentIndicator(currentParentId);
        }
    });

    //Скрытие чекнутых на вкладке управления "Показатель" и "Период"
    (function () {
        periodArr = [];
        indicatorArr = [];

        $(visibilityTableBody).find(isVisibleInput + ':checked').each(function () {
            indicatorArr.push(getIndicatorId($(this)));
        });

        $('body').find(periodInput + ':checked').each(function () {
            periodArr.push($(this).val());
        });

        hideColumns(periodArr);
        hideIndicatorRow(indicatorArr);
        hideOption(periodSelect, periodArr);
        hideOption(monitoringResultSelect, indicatorArr);
    }());

    $('body').find(monitoringResultSelect).each(function () {
        if (!selectedValue($(this))) {
            $(this).select2();
            $(this).val('').trigger('change');
        }
    });

    // удаляет описание файла, которое дублируется от нижестоящего элемента таблицы
    $('.table-bordered tr').each(function(){
        if($(this).text().indexOf('Пожалуйста, загрузите файл pdf или excel или doc') !== -1) {
            $(this).find(".itm_file_preview_ico_c").remove();
            $(this).find(".itm_file_preview_info_c").remove();
        }
    });

    // Запрет ввода в input'ы любых символов, кроме цифт. Необходимо для исключеемя неверных значений
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    $("input").inputFilter(function(value) {
        return /^\d*$/.test(value);    // пропускает только числа
    });
});


$(document).change(function () {
    $('input[type=file]').each(function() {
        if ($(this).get(0).files.length !== 0) {
            $(this).addClass( "file-here" );
        }
    });
});