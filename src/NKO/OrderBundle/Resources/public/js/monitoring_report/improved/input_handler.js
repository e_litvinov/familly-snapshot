
var newAmountInput = 'input[id*="_newAmount"]';
var totalAmountInput = 'input[id*="_totalAmount"]';

function setChangeInputHandler() {
    // изменение кликабельности чекбокса при вводе значений в поле "В т.ч. новых"
    $('body').on('keyup paste change', newAmountInput, function() {
        if (isNotEmptyInput($(this))) {
            updateIndicatorCheckbox($(this), 'disable');
            updatePeriodCheckbox($(this), 'disable');
        }

        var isEmpty = true;

        $(this).closest('tr').find(newAmountInput).each(function () {
            if (isNotEmptyInput($(this))) {
                isEmpty = false;
            }
        });

        if (isEmpty) {
            if (!isSetTotalValue($(this))) {
                updateIndicatorCheckbox($(this), 'enable');
            }
            updatePeriodCheckbox($(this), 'enable')
        }
    });
}

function updateIndicatorCheckbox($input, method) {
    var id;

    id = getIndicatorId($input);
    $(visibilityTableBody).find(resultIdInput).each(function () {
        if ($(this).val() == id) {
            $(this).closest('tr').find(isVisibleInput).iCheck(method);
        }
    });
}

function updatePeriodCheckbox($input, method) {
    $('input[id*="_periods"]').each(function () {
        if ($(this).val() == $input.attr('period_id')) {
            $(this).iCheck(method);
        }
    });
}

function isSetTotalValue($newAmountInput) {
    var isEmpty = true;

    $newAmountInput.closest('tr').find(totalAmountInput).each(function () {
        if (($(this).val() != 0 && $(this) != '')) {
            isEmpty = false;
        }
    });

    return isEmpty;
}

function updateCheckboxes(indicatorArr, periodArr) {
    $(visibilityTableBody).find(resultIdInput).each(function () {
        if(indicatorArr.indexOf($(this).val()) !== -1) {
            $(this).closest('tr').find(isVisibleInput).iCheck('disable');
        }
    });

    $('input[id*="_periods"]').each(function () {
        if(periodArr.indexOf($(this).val()) !== -1) {
            $(this).iCheck('disable');
        }
    });
}