
var monitoringResultsTable = 'div[id$="monitoringResults"] table tbody';
var monitoringResultsThead = 'div[id$="monitoringResults"] thead';
var periodInput = 'input[id*="_periods"]';
var periodResultsInput = 'input[name*="periodResults"]';
var attr = 'period_id';

function setPeriodVisibilityHandlers() {
    $(periodInput).on('ifChecked', function () {
        hideColumns($(this));
    });

    $(periodInput).on('ifUnchecked', function () {
        showColumns($(this));
    });
}

function hideColumns($input) {
    var actionArr = function(selector, tag) {
        if ($input.constructor === Array) {
            if ($input.indexOf(selector.attr(attr)) != -1) {
                selector.closest(tag).hide();
            }
            return true;
        }
    };

    $(monitoringResultsTable).find(periodResultsInput).each(function () {
        if (actionArr($(this), 'td')) {
            return;
        }

        if ($input.val() == $(this).attr(attr)) {
            $(this).closest('td').hide();
        }
    });

    $(monitoringResultsThead).find('th').each(function () {
        if (actionArr($(this), 'th')) {
            return;
        }

        if ($input.val() == $(this).attr(attr)) {
            $(this).hide();
        }
    })
}

function showColumns($input) {
    $(monitoringResultsTable).find(periodResultsInput).each(function () {
        if ($input.val() == $(this).attr(attr)) {
            $(this).closest('td').show();
        }
    });

    $(monitoringResultsThead).find('th').each(function () {
        if ($input.val() == $(this).attr(attr)) {
            $(this).show();
        }
    })
}