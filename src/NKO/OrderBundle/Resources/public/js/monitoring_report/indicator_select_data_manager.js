'use strict';

var monitoringResultSelect = 'select[id$="_monitoringResult"]';
var documentTableBody = $('div[id$="documents"] table tbody');

function setSelectData(select) {
    var newOption = new Option("", 0, false, false);

    var tableRows = $('div[id$="_monitoringResults"]').find('tbody tr');
    var indicatorTitle = 'textarea[id$="_indicator_title"]';
    var isSum = false;

    tableRows.each(function() {
        var label = $(this).find(indicatorTitle).val();
        if (label) {
            var isParent = false;
            var parentCode;
            var code;
            var value = 0;

            var number = $(this).find('td:first-child').html() + " ";
            value = $(this).find('input[id$="_id"]').val();
            parentCode = $(this).find('input[id$="_parentCode"]').val();
            code = $(this).find('input[id$="_code"]').val();
            if (parentCode === code && code !== '') {
                isParent = true;
            }

            if (!isSum) {
                if (!(isSum = (isParent === true && label === 'Итоги:'))) {
                    newOption = new Option(number + label, value, false, false);
                    if (isParent === true) {
                        newOption.disabled = true;
                    }

                    // if (label === '') {
                    //     newOption.wrap("<span>")
                    //     newOption.parent().hide();
                    // }

                    select.append(newOption);
                }
            } else {
                if(isParent) {
                    newOption = new Option(number + label, value, false, false);
                    newOption.disabled = true;
                    isSum = false;
                    select.append(newOption);
                }
            }
        }
    });
}

function updateSelectData() {
    documentTableBody.find(monitoringResultSelect).each(function () {
        var selected = $(this).find(':selected').val();
        $(this).empty();
        setSelectData($(this));
        if (selected) {
            $(this).find('option[value=' + selected + ']').attr('selected', 'selected');
        }
    });
}