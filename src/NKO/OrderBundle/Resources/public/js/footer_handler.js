'use strict';

$(document).ready(function () {
    (function(){
        var tabFooter = new TabFooter($(document));
        tabFooter.init();
        $('div.btn-group button[type="button"]').on("click", function() {
            tabFooter.changeTab();
        });
    })();
});

function TabFooter(document){
    var $tabSelector = $('.col-md-12 .col-md-12'),
        footer  = $('.sonata-ba-form-actions'),
        height = $('.sidebar').height() - $('.content-header').height(),
        heightTabs = [],
        object = this,
        scroll;

    this.init = function()
    {
        object.changeTab();
        object.handleScroll();
    };

    this.changeTab = function() {
        heightTabs.length = 0;
        $tabSelector.each(function()
        {
            object.fillHeightTabs($(this));
        });
        object.stickFooter();
    };

    this.handleScroll = function() {
        document.scroll(function() {
            scroll = document.scrollTop();
            object.stickFooter();
        });
    };

    this.stickFooter = function() {
        if (footer.length) {
            var currentPosition,
                needStick;

            currentPosition = height + (scroll ? scroll : 0);
            needStick = (currentPosition / heightTabs[object.getCurrentTab()]) * 100;

            if (needStick < 98) {
                footer.addClass('stuck');
            } else {
                footer.removeClass('stuck');
            }
        }
    };

    this.fillHeightTabs = function(selector)
    {
        heightTabs.push(selector.outerHeight());
    };

    this.getCurrentTab = function() {
        return $.cookie('tabBarIndex') ? $.cookie('tabBarIndex') : 0;
    };
};