'use strict';

$(document).ready(function ($) {

    addCheckboxHandler('div[id*="_isOrganizationHeadEqualProjectHead"]', 'div[id*="_headOfOrganization"]', 'div[id*="_headOfProject"]');
    addCheckboxHandler('div[id*="_isOrganizationHeadEqualHeadOfAccounting"]', 'div[id*="_headOfOrganization"]', 'div[id*="_headOfAccounting"]');

    function addCheckboxHandler(checkboxSelector, mainFormSelector, equalFormSelector) {
        var $checkbox = $(checkboxSelector + ' input[type="checkbox"]'),
            $mainFormSelectorInputs = $(mainFormSelector + ' input'),
            $equalFormSelectorInputs = $(equalFormSelector + ' input'),
            $equalFormSelector = $(equalFormSelector),
            $isEqualCheckbox = $(checkboxSelector),
            $headOfProjectLabel = $(".head-of-project"),
            $headOfAccountingLabel = $(".head-of-accounting");

        if($isEqualCheckbox.find('.iCheck-helper').parent().hasClass('checked')) {

            $equalFormSelector.hide();
            if(equalFormSelector.indexOf("headOfProject") !== -1)
                $headOfProjectLabel.hide();

            if(equalFormSelector.indexOf("headOfAccounting") !== -1)
                $headOfAccountingLabel.hide();
        }

        $mainFormSelectorInputs.change(function(){
            if($isEqualCheckbox.find('.iCheck-helper').parent().hasClass('checked')) {
                insertValues();
            }
        });

        $checkbox.on('ifUnchecked', function() {
            $equalFormSelector.each(function() {
                $(this).show();
                if(equalFormSelector.indexOf("headOfProject") !== -1)
                    $headOfProjectLabel.show();
                if(equalFormSelector.indexOf("headOfAccounting") !== -1)
                    $headOfAccountingLabel.show();
            });
            $equalFormSelectorInputs.val('');
        });

        $checkbox.on('ifChecked', function() {
            insertValues();

            $equalFormSelector.hide();

            if(equalFormSelector.indexOf("headOfProject") !== -1)
                $headOfProjectLabel.hide();
            if(equalFormSelector.indexOf("headOfAccounting") !== -1)
                $headOfAccountingLabel.hide();
        });

        function insertValues() {
            var formValues = {};

            $mainFormSelectorInputs.each(function() {
                var idParts = $(this).attr("id").match(/[A-Z][a-z]+/g),
                    keys = getKeys(idParts);

                for(var i = 0; i < keys.length; i++) {
                    if(!(keys[i] in formValues)) {
                        formValues[keys[i]] = $(this).val();
                        break;
                    }
                }
            });

            $equalFormSelectorInputs.each(function() {
                var idParts = $(this).attr("id").match(/[A-Z][a-z]+/g),
                    keys = getKeys(idParts);

                for(var i = 0; i < keys.length; i++) {
                    if(keys[i] in formValues) {
                        $(this).val(formValues[keys[i]]);
                        delete formValues[keys[i]];
                        break;
                    }
                }
            });
        }

        function getKeys(idParts) {
            var keys = [];
            idParts.reverse().reduce(function (first, second) {
                keys.push(second + first);
                return second + first;
            }, '');

            return keys;
        }
    }
});