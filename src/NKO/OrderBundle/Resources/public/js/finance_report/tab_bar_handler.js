'use strict';

$(document).ready(function ($) {

   //селектор каждого таба
    var $tabSelector = $('.col-md-12 .col-md-12');
    autosize($("textarea"));
    //селектор кнопок на таббаре
    var $tabButtonSelector = $('div.btn-group button[type="button"]');
    var $nextTabBtn = $("#next-tab");
    var tabsCount = $tabSelector.length;

    //скрывает содержимое всех табов кроме первого
    $tabSelector.each(function(index)
    {
        if($.cookie('tabBarIndex'))
            index == $.cookie('tabBarIndex') ? $(this).show() : $(this).hide();
        else
            index == 0 ? $(this).show() : $(this).hide();
    });

    $tabButtonSelector.each(function(index)
    {
        $(this).removeClass("btn-info active")
            .addClass("btn-default");
        if($.cookie('tabBarIndex')){
            if(index == $.cookie('tabBarIndex')) {
                $(this).removeClass("btn-default")
                    .addClass("btn-info active");
            }
        }
        else {
            if(index == 0) {
                $(this).removeClass("btn-default")
                    .addClass("btn-info active");
            }
        }

    });

    //при выборе файла текст видимым
    $('input[type="file"]').change(function() {
        $(this).css('color', 'black');
    });

    handleTabButtonClick();
    addErrorsNumberToTab();

    /**
     * Функция обработки клика кнопки таббара
     */
    function handleTabButtonClick()
    {
        $tabButtonSelector.click(function(){

            //определяет индекс кликнутой кнопки
            //(индекс кнопки совпадает с индексом отображаемого блока)
            var indexToShow = $(this).index();

            $.cookie('tabBarIndex', indexToShow, { path: '/' });

            //отображает только блок, соответствующий нажатой кнопке
            $tabSelector.each(function(index){
                index == indexToShow ? $(this).show() : $(this).hide();
            });

            //делает все кнопки таббара неактивными
            $tabButtonSelector.removeClass("btn-info active")
                .addClass("btn-default");

            //делает нажатую кнопку таббара активной
            $(this).removeClass("btn-default")
                .addClass("btn-info active");

            if (indexToShow < (tabsCount - 1))
                $nextTabBtn.show();
        });
    }

    /**
     * Функция добавления количество ошибок на кнопки таббара
     */
    function addErrorsNumberToTab()
    {
        //селектор таббара
        var $tabSelector = $('.col-md-12 .col-md-12');
        var text = 'ошибок';
        //проходит по всем таббарам
        $tabSelector.each(function(index){

            //количество ошибок в открытой вкладке
            var errors = $(this).find('div.help-inline.sonata-ba-field-error-messages').length
                + $(this).find('div.help-block.sonata-ba-field-error-messages').length;

            if(window.location.href.indexOf("report") != -1 &&
                window.location.href.indexOf("edit?uniqid") != -1) {
                $(this).find("div[id$='_immediateResults'] input").each(function(){
                    if($(this).val() == '' && !$(this).is(":disabled")) {
                        errors++;
                    }
                });
            }

            //если есть ошибки
            if(errors > 0){
                //проверяет на количество ошибок и в зависимости от этого подставляет текст в нужном падеже
                if(errors >= 10 && errors <= 20){
                    text = 'ошибок';
                }
                else if(errors % 10 == 1)
                {
                    text = 'ошибка';
                }
                else if(errors % 10 > 1 && errors % 10 < 5) {
                    text = 'ошибки';
                }
                else {
                    text = 'ошибок';
                }
                //вставка текста с количеством ошибок в таббар
                $('div.btn-group button[type="button"]').eq(index).append('<h5 style="color:#a94442;">' +
                    '(<i class="fa fa-exclamation-circle">&nbsp;' +  errors  + ' ' + text +'</i>)</h5>');

            }
        });
    }

    $("#next-tab").on("click", function(e)
    {
        e.preventDefault();
        $tabButtonSelector.each(function(index){

            if(index < (tabsCount - 1) && $(this).hasClass("active")) {

                $(this).removeClass("btn-info active")
                    .addClass("btn-default");

                $tabButtonSelector.eq(index + 1)
                    .removeClass("btn-default")
                    .addClass("btn-info active")
                    .trigger("click");

                return false;
            }
            else {
                $nextTabBtn.hide();
            }
        });
    });
});