'use strict';

$(document).ready(function ($) {
    var $expencesTableBody = $('div[id$="expenses"] table tbody');
    var $financeSpentTableBody = $('div[id$="financeSpent"] table tbody');

    initAutoNumeric();
    reportSalary=true;

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('documents').configs({
            'columnName' : '1.Номер'
    })
        .table('financeSpent').configs({
            'removeTracking' : true
    });
    tableManager.process();

    fillIncrementalCosts();
    fileNameLoader('div[id$="documents"] table tbody');

    var actions = function() {
        sumInIncrementalCosts();
        sumInBalance();
        sumInFinanceSpentTable(3);
    };

    //пересчёт значений при измении значения инпута с суммой register
    $('body').on('keyup paste change', sumInput, function(){
        $(this).autoNumeric('init', autoNumericOptions);
        countSums(($(this).attr('id')), actions); //текущий input суммы register
        sumSalary();
    });

    //Изначальные вычисления
    addTrToFinanceSpentTable(4);
    actions();
    reformatTableName("titleWithNumber");
    sumSalary();
    setDefaultValue($expencesTableBody);
    setDefaultValue($financeSpentTableBody);
    var selectSizeManager = new SelectSizeManager;
    selectSizeManager.init();

    $('body').on('loaded', 'tr', function() {
        if ($(this).closest('span').attr('id').indexOf('documents') == -1) {
            setDefaultValue($(this));
        }
    });

    autosize($('textarea'));

    function setDefaultValue($element) {
        $element.find('input[type="text"]').each(function () {
            var value = 0;
            if ($(this).val() == "" && $(this).attr('class').indexOf('select') == -1) {
                $(this).val(value.toFixed(2));
            }

            if ($.isNumeric($(this).val())) {
                $(this).val(parseFloat($(this).val()).toFixed(2));
            }
        });

        $element.find('textarea').each(function () {
            if ($(this).val() == "") {
                $(this).val('-');
            }
        });
    }
});