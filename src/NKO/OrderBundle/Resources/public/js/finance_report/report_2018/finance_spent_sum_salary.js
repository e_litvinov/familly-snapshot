'use strict';


function sumSalary () {
    var financeSpentRow = {};
    var parentCode = 'salary';
    var financeTableBody = $('span[id$="financeSpent"] table tbody');

    financeTableBody.find('input:hidden[id$="_parentCode"]').each(function (index) {
        if (index === 0) {
            return;
        }
        if ($(this).val() === parentCode) {
            $(this).closest('tr').find('td:not(:first) input').not('[id*="_incrementalCosts"]').each(function (index) {
                var value = $(this).val() || 0;
                if (!(index in financeSpentRow)) {
                    financeSpentRow[index] = conversionToFloat(value);
                } else {
                    financeSpentRow[index] += conversionToFloat(value);
                }
            })
        }
    });

    financeTableBody.find('tr:first td:not(:first)').not('[class*=incrementalCosts]').each(function (index) {
        $(this).find('input').val((financeSpentRow[index]).toFixed(2));
    });
}