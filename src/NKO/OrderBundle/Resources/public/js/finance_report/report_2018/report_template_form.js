'use strict';

$(document).ready(function ($) {

    var approvedSum = 'input[id*="_approvedSum"]';
    var requiredSum = 'input[id*="_requiredSum"]:not(:first)';
    var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''};

    autosize($('textarea'));
    $(approvedSum).autoNumeric('init', autoNumericOptions);
    $(requiredSum).autoNumeric('init', autoNumericOptions);

    $('span[id$="financeSpent"] table tbody').append(
        '<tr>' +
        '<td><input type="text" class="form-control" disabled value="ВСЕГО"/></td>' +
        '<td><input type="text" class="form-control" disabled value=""/></td>' +
        '<td><input type="text" class="form-control" disabled value=""/></td>' +
        '</tr>'
    );

    $('input[id$="_approvedSum"]').each(function () {
        var value = 0;
        if ($(this).val() == "") {
            $(this).val(value.toFixed(2));
        }
    });

    reportSalary = true;
    $('input[id*="_requiredSum"]:first').attr('readonly', true);

    sumSalary();
    sumInFinanceSpentTable(0);

    //пересчёт значений при измении значения инпута
    $('body').on('paste change', requiredSum, function(){
        $(this).autoNumeric('init', autoNumericOptions);
        sumSalary();
        sumInFinanceSpentTable(0);
    });
});