

var finalSum = 0;
var parentId = /registers(_([0-9]+))/;
var sumInput = 'input[id*="_sum"]';
var totalExpenseInput = 'input[id$="_totalExpenseValue"]';
var costsSum = 0;
var approvedSum = 'input[id*="_approvedSum"]';
var periodCosts = 'input[id*="_periodCosts"]';
var incrementalCosts = 'input[id*="_incrementalCosts"]';
var incrementalCostsInitialValues = {};
var balance = 'input[id*="_balance"]';
var summaryIncrementalCosts = 'input[id*="_summaryIncrementalCosts"]';
var autoNumericOptions = { vMin: '-9999999.99', lZero: 'keep', aSep: ''};
var reportSalary;

function initAutoNumeric() {
    $(approvedSum).autoNumeric('init', autoNumericOptions);
    $(periodCosts).autoNumeric('init', autoNumericOptions);
    $(balance).autoNumeric('init', autoNumericOptions);
    $(summaryIncrementalCosts).autoNumeric('init', autoNumericOptions);
    $(sumInput).autoNumeric('init', autoNumericOptions);
}

function reformatTableName(field) {
    var title ="_expenseType_" + (field || "title");
    $('textarea[id*="_registers"][id$='+title+']')
        .each(function() {
            $("<label><h4><b>" + $(this).val() + "</b></h4></label>").insertAfter($(this));
            $(this).hide();
        });

    $('span[id*="field_actions"][id*="registers"]').hide();
}

function countSums(id, actions) {
    var idPart = id.match(parentId)[0];
    var parentDiv = $('div[id$="' + idPart + '"]');
    var currentTotalExpenseInput = parentDiv.find(totalExpenseInput);
    var code = parentDiv.find('input:hidden[id$="_code"]').val();
    var parentCode = parentDiv.find('input:hidden[id$="_parentCode"]').val();

    parentDiv.find(sumInput).each(function() {
        var inputValue = conversionToFloat($(this).val());
        finalSum += inputValue;
    });

    costsSum = conversionToFloat($('div[id$="_financeSpent"]')
        .find('input[value="' + parentCode + '"]')
        .closest('tr')
        .find('input[id$="periodCosts"]')
        .val());

    updateFinanceSpentTable(code, finalSum);

    //подставляем значение в инпут с итоговой суммой таблицы registers
    currentTotalExpenseInput.val(finalSum.toFixed(2));

    if (actions) {
        actions();
    }

    //обнуляем счётчик суммы
    finalSum = 0;
}


function updateFinanceSpentTable(code, value) {
    var financeSpentTable = $('div[id$="_financeSpent"]');

    financeSpentTable.find('input:hidden[id$="_code"]').each(function(index){
        if (reportSalary && index === 0) {
            return;
        }

        var periodCosts = $(this).closest('tr').find('input[id$="_periodCosts"]');
        if ($(this).val() === code) {
            periodCosts.val(value.toFixed(2));
        }
    });
}


function sumInFinanceSpentTable(financeMoving) {

    var financeSpent = {};
    var financeTableBody = $('span[id$="financeSpent"] table tbody');
    var financeMovingTab = $('.col-md-12:nth-child('+financeMoving+')');

    financeTableBody.find('tr:not(:last)').each(function(index){
        if (reportSalary && index === 0) {
            return;
        }
        $(this).find('td:not(:first) input').not('[id*="_incrementalCosts"]').each(function(index) {
            var value = $(this).val() || 0;
            if (!(index in financeSpent)) {
                financeSpent[index] = conversionToFloat(value).toFixed(2);
            } else {
                financeSpent[index] = (conversionToFloat(financeSpent[index]) + conversionToFloat(value)).toFixed(2);
            }
        });
    });

    financeTableBody.find('tr:last td:not(:first)').each(function(index){
        $(this).find('input').val((financeSpent[index]));
    });

    financeMovingTab.find('input').each(function(index) {
        if (financeSpent[index] !== undefined) {
            $(this).val((financeSpent[index]));
        }
    });
}


function sumInIncrementalCosts() {
    $(summaryIncrementalCosts).each(function(index) {
        if (reportSalary && index === 0) {
            return;
        }
        var initialValue = incrementalCostsInitialValues[index];
        var periodCostsValue = $($(periodCosts)[index]).val() || 0;
        $(this).val((initialValue + conversionToFloat(periodCostsValue)).toFixed(2));
    });
}


function sumInBalance() {
    $(balance).each(function(index) {
        if (reportSalary && index === 0) {
            return;
        }
        var approvedValue = conversionToFloat($($(approvedSum)[index]).val());
        var incrementalCostsValue = conversionToFloat($($(summaryIncrementalCosts)[index]).val());
        $(this).val((approvedValue - incrementalCostsValue).toFixed(2));
    });
}


function addTrToFinanceSpentTable(count) {
    var row;
    for (i=0; i<count; i++) {
        row = row +'<td><input type="text" class="form-control" disabled value=""/></td>';
    }

    $('span[id$="financeSpent"] table tbody').append(
        '<tr><td><input type="text" class="form-control" disabled value="ВСЕГО"/></td>'+ row +'</tr>'
    );
}


function fillIncrementalCosts(){
    $(incrementalCosts).each(function(index) {
        if(!(index in incrementalCostsInitialValues)) {
            incrementalCostsInitialValues[index] = conversionToFloat($(this).val());
        }
    });
}


function conversionToFloat(val) {
    if (!val) {
        return 0;
    }

    val = val.replace(',', '.');
    val = parseFloat(val);
    val = val.toFixed(2);
    return parseFloat(val);
}


function sumInBalanceMixed() {
    $(balance).each(function(index) {
        var approvedValue = conversionToFloat($($(approvedSum)[index]).val());
        var periodCostsValue = conversionToFloat($($(periodCosts)[index]).val());

        $(this).val((approvedValue - periodCostsValue).toFixed(2));
    });
}