$(document).ready(function ($) {

    var $expencesTableBody = $('div[id$="expenses"] table tbody');
    var $financeSpentTableBody = $('div[id$="financeSpent"] table tbody');

    initAutoNumeric();

    reportSalary=true;

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('documents').configs({
        'columnName' : '1.Номер'
    })
        .table('financeSpent').configs({
        'removeTracking' : true
    });
    tableManager.process();

    fillIncrementalCosts();
    fileNameLoader('div[id$="documents"] table tbody');

    var actions = function() {
        sumInIncrementalCosts();
        sumInBalance();
        sumInFinanceSpentTable(3);
    };

    //пересчёт значений при измении значения инпута с суммой register
    $('body').on('keyup paste change', sumInput, function(){
        $(this).autoNumeric('init', autoNumericOptions);
        countSums(($(this).attr('id')), actions); //текущий input суммы register
        sumSalary();
    });

    //Изначальные вычисления
    addTrToFinanceSpentTable(4);
    actions();
    reformatTableName("titleWithNumber");
    sumSalary();
    setDefaultValueTextArea($financeSpentTableBody);
    setDefaultValueText($financeSpentTableBody);

    $('body').on('loaded', 'tr', function() {
        if ($(this).closest('span').attr('id').indexOf('documents') == -1) {
            setDefaultValue($(this));
        }
    });

    autosize($('textarea'));

    function setDefaultValueText($element) {
        $element.find('input[type="text"]').each(function () {
            var value = 0;
            if ($(this).val() == "" && $(this).attr('class').indexOf('select') == -1) {
                $(this).val(value.toFixed(2));
            }

            if ($.isNumeric($(this).val())) {
                $(this).val(parseFloat($(this).val()).toFixed(2));
            }
        });
    }

    function setDefaultValueTextArea($element) {
        $element.find('textarea').each(function () {
            if ($(this).val() == "") {
                $(this).val('-');
            }
        });
    }

    var autoNumericOptions = { lZero: 'keep', aSep: '', altDec: ','};
    $('input[id$="sum"]').autoNumeric('init', autoNumericOptions);
    $('div[id$="documents"]').find("th[id='action_th']").text('7.Действия');

    $(totalExpenseInput).each(function () {
            var element = $(this);
            element.val( (Number(element.val())) .toFixed(2));
        }
    )
});