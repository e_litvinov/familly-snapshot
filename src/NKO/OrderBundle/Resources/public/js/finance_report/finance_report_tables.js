'use strict';

$(document).ready(function ($) {
    initAutoNumeric();

    reportSalary=false;

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('documents').configs({
            'columnName' : '1.Номер'
        })
        .table('financeSpent').configs({
            'removeTracking' : true
    });
    tableManager.process();

    var actions = function() {
        sumInIncrementalCosts();
        sumInBalance();
        sumInFinanceSpentTable(3);
    };

    //Изначальные вычисления
    fillIncrementalCosts();
    addTrToFinanceSpentTable(4);
    actions();
    reformatTableName();
    var selectSizeManager = new SelectSizeManager;
    selectSizeManager.init();

    //пересчёт значений при измении значения инпута с суммой register
    $('body').on('keyup paste change', sumInput, function(){
        $(this).autoNumeric('init', autoNumericOptions);
        countSums(($(this).attr('id')), actions); //текущий input суммы register
    });

    $('body').on('change', 'select', function() {
        $(this).select2({width : '130px'});
    });
});