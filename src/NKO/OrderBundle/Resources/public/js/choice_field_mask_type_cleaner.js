function clearFieldsMaskType(selector, childrens) {
    $(selector).on('change', function () {
        $.each(childrens, function (i, field) {
            clearField(i, field, $(selector));
        })
    })
}

function clearField(field, type, selector) {
    if (type === 'list') {
        clearList(field, selector);
    }
}

function clearList(field, selector) {
    $(field + ' li[class$=choice]').each(function (i, object) {
        object.remove();
    });
    $(field + ' option[selected]').each(function (i, object) {
        $(object).removeAttr('selected');
    });
}
