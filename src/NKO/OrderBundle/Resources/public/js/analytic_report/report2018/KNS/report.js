'use strict';

$(document).ready(function ($) {
    disableDateField();
    addListener("div[id$='trainingGrounds']", getCompetition);
    allowOnlyNumeric("div[id$='expectedAnalyticResults'] input[id$='Value']");
    allowOnlyNumeric("div[id$='practiceAnalyticResults'] input[id$='Value']");
    allowOnlyNumeric("div[id$='humanResources'] input[placeholder='0']");
    disableAddButton("div[id$='_humanResources']");

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('practiceFeedbackAttachments').configs({
            'contentFile' : true,
            'changeFirstIndex' : true
        })
        .table('practiceAnalyticResults').configs({
            'countRowThead' : 2
        })
        .table('expectedAnalyticResults').configs({
            'countRowThead' : 2
        })
        .table('practiceSuccessStoryAttachments').configs({
            'contentFile' : true
        });
    tableManager.process();

    // окрашивает название файла, когда он выбирается в input[type=file]
    $(document).change(function () {
        $('input[type=file]').each(function() {
            if ($(this).get(0).files.length !== 0) {
                $(this).addClass( "file-here" );
            }
        });
    });

});

function addListener(select, method, parameters) {
    var observer = new MutationObserver(function() {
        method(parameters);
    });
    var config = { subtree: true, attributes: true};
    var target = $(select)[0];
    observer.observe(target, config);
    method(parameters);
}

function getCompetition() {
    var text = $("div[id$='trainingGrounds'] div.checked + span")[1] ?
        $("div[id$='trainingGrounds'] div.checked + span")[1].innerHTML : "";
    $("div[id$='trainingGroundsSecond'] textarea").val(text);
}

function disableAddButton(selector) {
    $(selector).find('button[id*=add]').attr('disabled', true);
}
