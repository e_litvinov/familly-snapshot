'use strict';

$(document).ready(function ($) {
    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('practiceFeedbackAttachments').configs({
            'contentFile' : true,
            'changeFirstIndex' : true
        })
        .table('practiceSpreadFeedbackAttachments').configs({
            'contentFile' : true,
            'changeFirstIndex' : true
        })
        .table('practiceAnalyticResults').configs({
            'removeLastColumn' : true,
            'countRowThead' : 3
        })
        .table('expectedAnalyticResults').configs({
            'removeLastColumn' : true,
            'countRowThead' : 3
        })
        .table('practiceSuccessStoryAttachments').configs({
            'contentFile' : true
        })
        .table('practiceSpreadSuccessStoryAttachments').configs({
            'contentFile' : true
        })
        .table('publicationAttachments').configs({
            'contentFile' : true
        })
        .table('materialAttachments').configs({
            'contentFile' : true
        })
        .table('factorAttachments').configs({
            'contentFile' : true
        })
        .table('practiceAnalyticIndividualResults').configs({
            'removeLastColumn' : true,
            'countRowThead' : 2
        })
        .table('expectedAnalyticIndividualResults').configs({
            'removeLastColumn' : true,
            'countRowThead' : 2
        })
        .table('analyticPublications').configs({
            'removeLastColumn' : true
        })
        .table('monitoringDevelopmentAttachments').configs({
            'contentFile' : true
        });
    tableManager.process();

    //проверка значения на INT
    [
        "input[id$='_executorsCount']",
        "input[id$='_substitutionsCount']",
        "input[id$='_ratesCount']",
        "input[id$='_individualJobSpecialistCount']"
    ].forEach(function (item) {
        checkInteger(item);
    });

    disableDateField();
    removeAddButton("div[id$='_factorAttachments']");
    limitRemoveElements("div[id$='_factorAttachments']");
});

function removeAddButton(selector) {
    $(selector).find('button[id*=add]').remove();
}

function limitRemoveElements(selector) {
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            disableRemoveElements(selector)
        });
    });
    var config = { attributes: true};
    $(selector + " tr").each(function () {
        observer.observe((this), config);
    });
}

function disableRemoveElements(selector) {
    var minimalAmount = $(selector).children('.parameters').attr('amount');
    var amount = $(selector).find('tr[class!="rowHidden"]').length - 1;

    if(amount <= minimalAmount){
        $(selector).find('button[id*=delete]').attr('disabled', true);
    }
}