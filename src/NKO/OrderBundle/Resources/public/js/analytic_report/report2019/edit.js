'use strict';

$(document).ready(function ($) {

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('expectedAnalyticResults').configs({
        'countRowThead' : 2,
    })
        .table('implementationSocialResults').configs({
        'countRowThead' : 2,
    })
        .table('practiceAnalyticResults').configs({
        'countRowThead' : 2,
    })
        .table('introductionSocialResults').configs({
        'countRowThead' : 2,
    })
        .table('practiceFeedbackAttachments').configs({
        'contentFile' : true,
        'changeFirstIndex' : true
    })
        .table('practiceSocialFeedbackAttachments').configs({
        'contentFile' : true,
        'changeFirstIndex' : true
    })
        .table('practiceSpreadFeedbackAttachments').configs({
        'contentFile' : true,
        'changeFirstIndex' : true
    })
        .table('practiceSocialSpreadFeedbackAttachments').configs({
        'contentFile' : true,
        'changeFirstIndex' : true
    })
        .table('practiceSuccessStoryAttachments').configs({
        'contentFile' : true
    })
        .table('practiceSpreadSuccessStoryAttachments').configs({
        'contentFile' : true
    })
        .table('publicationAttachments').configs({
        'contentFile' : true
    })
        .table('materialAttachments').configs({
        'contentFile' : true
    })
        .table('factorAttachments').configs({
        'contentFile' : true
    })
        .table('monitoringDevelopmentAttachments').configs({
        'contentFile' : true
    })
        .table('analyticPublications').configs({
        'removeLastColumn' : true
    })
    ;
    tableManager.process();

    makeReadOnly();
    addOverflow();

    removeAddButton("div[id$='_factorAttachments']");
    disableAddButton("div[id$='_humanResources']");
    limitRemoveElements("div[id$='_factorAttachments']");
    changeErrorTextById(
        "div[id$='_analyticPublications']",
        'Значение не должно быть пустым.',
        'Неверное значение (поле должно быть не пустым). Если значение - ноль, введите его вручную.'
    );

    $('body').on('click', '#add_btn', function() {
        //block remove buttons if plan value != 0
        blockDeleteIfNotNull("input[id$='_servicePlanValue']");
        blockDeleteIfNotNull("input[id$='_planValue']");
    });

    //убирает (id:) из записей в выпадающих списках на 6-й вкладке
    $("span[class*='select2']").each(function(index, elem) {
        var new_text = $(elem).text().replace(/ *\(id:[^)]*\) */g, "");
        $(elem).text(new_text);
    });
});

function removeAddButton(selector) {
    $(selector).find('button[id*=add]').remove();
}

function removeDeleteButton(selector) {
    $(selector).find('button[id*=delete]').remove();
}

function disableAddButton(selector) {
    $(selector).find('button[id*=add]').attr('disabled', true);
}

function disableDeleteButton(selector) {
    $(selector).find('button[id*=delete]').attr('disabled', true);
}

function limitRemoveElements(selector) {
    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            disableRemoveElements(selector)
        });
    });
    var config = { attributes: true};
    $(selector + " tr").each(function () {
        observer.observe((this), config);
    });
}

function disableRemoveElements(selector) {
    var minimalAmount = $(selector).children('.parameters').attr('amount');
    var amount = $(selector).find('tr[class!="rowHidden"]').length - 1;

    if(amount <= minimalAmount){
        $(selector).find('button[id*=delete]').attr('disabled', true);
    }
}

function makeReadOnly() {
    var selectors = [
        'textarea[id$="projectName"]',
        // 'input[id$="directorName"]',
        // 'input[id$="directorPosition"]',
        // 'input[id$="accountantName"]',
    ];
    selectors.forEach(function (selector) {
        $(selector).attr('readonly', 'readonly');
    })
}

function addOverflow() {
    $('div[id$="Results"]').each(function () {
        $(this).find('[class="sonata-ba-field sonata-ba-field-inline-table"]').css('overflow', 'scroll');
        $(this).find('textarea').css('min-width', '150px');
        $(this).find('input[id$="Value"]').css('min-width', '70px');
        $(this).find('input[id$="customMethods"]').css('min-width', '120px');
    });
}

function blockDeleteIfNotNull(field) {
    $("tr").each(function() {
        if ($(this).find(field).val() || $(this).find(field).val() === 0) {
            disableDeleteButton($(this));
        }
    });
}


function changeErrorTextById(element, message, messageForReplace) {
    $(element).find('li').each(function () {
        if ($(this).text().search(message)) {
            $(this).text(messageForReplace);
        }
    });
}