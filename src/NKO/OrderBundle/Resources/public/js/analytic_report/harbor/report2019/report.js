'use strict';
var DUPLICATES = {
    'projectName' : {
        'divSelectorFrom' : 'input[id$="_projectName"]',
        'divSelectorTo' : 'input[id$="_practice"]',
        'functionToRelay' : relayFromTextToText,
        'configs' : CONFIGS['text'],
        'parameters' : {}
    },
};

$(document).ready(function ($) {
    disableDateField();
    allowOnlyNumeric("div[id$='expectedAnalyticResults'] input[id$='Value']");
    allowOnlyNumeric("div[id$='practiceAnalyticResults'] input[id$='Value']");
    allowOnlyNumeric("div[id$='humanResources'] input[placeholder='0']");
    processAllItems(DUPLICATES);

    var tableManager = new TableManagerClass;
    tableManager.getConfigureInstance()
        .table('practiceFeedbackAttachments').configs({
            'contentFile' : true,
            'changeFirstIndex' : true
        })
        .table('expectedAnalyticResults').configs({
            'countRowThead' : 2
        })
        .table('practiceAnalyticResults').configs({
            'countRowThead' : 2
        })
        .table('practiceSuccessStoryAttachments').configs({
            'contentFile' : true
        })
        .table('practiceSpreadSuccessStoryAttachments').configs({
            'contentFile' : true
        })
        .table('humanResources').configs({
            'removeLastColumn' : true
        })
    ;
    tableManager.process();

    $('div[id$="practiceSpreadSuccessStoryAttachments"] thead th:nth-child(2)').html('Название приложения');
});
