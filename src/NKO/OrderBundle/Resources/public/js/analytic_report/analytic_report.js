'use strict';

$(document).ready(function ($) {
    changeTableView("div[id$='_materialAttachments']");
    changeTableView("div[id$='_publicationAttachments']");
    changeTableView("div[id$='_practiceFeedbackAttachments']", false, false, 'Номер', true);
    changeTableView("div[id$='_practiceSpreadFeedbackAttachments']", false, false, 'Номер', true);
    changeTableView("div[id$='_practiceSuccessStoryAttachments']");
    changeTableView("div[id$='_practiceSpreadSuccessStoryAttachments']");
    changeTableView("div[id$='_factorAttachments']");

    changeTableView("div[id$='_practiceAnalyticResults']");
    changeTableView("div[id$='_expectedAnalyticResults']");

    removeNumber("practiceAnalyticResults");
    removeNumber('expectedAnalyticResults');

    addSpans('practiceFeedbackAttachments', 2);
    addSpans('practiceSpreadFeedbackAttachments', 2);
    addSpans('publicationAttachments', 2);
    addSpans('materialAttachments', 2);
    $('ul[id*=priorityDirection] li:first-child').remove();
    $( "div.scroll" ).scrollTop( 300 );

    $('td[class*="_practiceAnalyticResults-isFeedback"] select').each(function () {
        changeSelect($(this));
    });
    $('td[class*="_expectedAnalyticResults-isFeedback"] select').each(function () {
        changeSelect($(this));
    });

    $('td[class*="_practiceAnalyticResults-isFeedback"] select').change(function(){
        changeSelect($(this));
    });

    $('td[class*="_expectedAnalyticResults-isFeedback"] select').change(function(){
        changeSelect($(this));
    });

    function changeSelect(select)
    {
        if(select.find(":selected").val() === '0') {
            var feedbackId = select.attr('id');
            var measureId = feedbackId.replace('isFeedback','') + 'measurementMethod';
            var select = $('select[id*="'+ measureId +'"]');
            select.find(":selected").text('нет').attr('selected', true);
            select.trigger('change');
        }
    }

    function addSpans(selectorId, count){
        $('span[id*=' + selectorId + '] table thead tr:first-child th:last-child').attr('rowspan', count);
        $('span[id*=' + selectorId + '] table thead tr th:first-child').not(':first').remove();
        $('span[id*=' + selectorId + '] table thead tr:first-child th:first-child').attr('rowspan', count);
    }

    function removeNumber(selectorIdt){
        $('span[id*=' + selectorIdt + '] table thead tr th:first-child').not(':first').remove();
    }
});
