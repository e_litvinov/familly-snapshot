$(document).ready(function ($) {
    var competitionSelect = $('select[id*="_competition"]');
    var value = competitionSelect.select2("val");
    if (value) {
        competitionSelect.select2("readonly", true);
    }
});