/** Structure 0f duplicates in processAllItems
 '$name$' : {
        'divSelectorFrom' : '$div[id="id"]$',
        'divSelectorTo' : '$div[id="id"]$',
        'functionToRelay' : $relayToTextFromTexts$,
        'configs' : CONFIGS['$text$'],
        'parameters' : {$
            here can be:
            'separator' : "$/$",
            'start' : "$start$"
            'finish' : "$end$"
        $}
    }
 */


var MUTATION = 'mutation';
var EVENT = 'event';

var CONFIGS = {
    'radioTree' : {observer: MUTATION, subtree: true, attributes: true},
    'text' : {observer: EVENT, attributes: true},
};

function processAllItems(duplicates) {
    for (var key in duplicates) {
        var parameters = duplicates[key];
        relay(parameters['divSelectorFrom'], parameters['divSelectorTo'], parameters['configs'], parameters['functionToRelay'], parameters['parameters'])
    }
}

function relay(divSelectorFrom, divSelectorTo, configs, functionToRelay, parameters)
{
    functionToRelay(divSelectorFrom, divSelectorTo, configs, parameters);
}

function createListener(selector, config, sender, divSelectorFrom, divSelectorTo, parameters)
{
    var type = config['observer'];
    var configs =  Object.assign({}, config);
    delete configs['observer'];

    if (type === MUTATION) {
        createMutationListener(selector, configs, sender, divSelectorFrom, divSelectorTo, parameters);
    } else if (type === EVENT) {
        createEventListener(selector, configs, sender, divSelectorFrom, divSelectorTo, parameters);
    }
}

function createEventListener(selector, configs, sender, divSelectorFrom, divSelectorTo, parameters)
{
    $(selector).change(
        function (){
            sender(divSelectorFrom, divSelectorTo, parameters)
        });
}

function createMutationListener(selector, configs, sender, divSelectorFrom, divSelectorTo, parameters)
{
    var observer = new MutationObserver(function(mutations) {
        sender(divSelectorFrom, divSelectorTo, parameters);
    });
    observer.observe($(selector)[0], configs);
}

function relayToTextFromTexts(divSelectorFrom, divSelectorTo, configs, parameters)
{
    for (var key in divSelectorFrom) {
        createListener(divSelectorFrom[key] ,configs , sendToTextFromTexts, divSelectorFrom, divSelectorTo, parameters);
    }

    sendToTextFromTexts(divSelectorFrom, divSelectorTo, parameters);
}

function sendToTextFromTexts(divSelectorFrom, divSelectorTo, parameters)
{
    var texts = takeFromTextsField(divSelectorFrom);
    var start = ('start' in parameters) ? parameters['start'] : '';
    var finish = ('finish' in parameters) ? parameters['finish'] : '';
    var text = start + texts.join(parameters['separator']) + finish;
    setToTextField(divSelectorTo, text);
}

function relayToTextFromRadioTree(divSelectorFrom, divSelectorTo, configs, parameters)
{
    var  selector = "div[id*='" + divSelectorFrom + "']";
    createListener(selector ,configs , sendToTextFromRadioTree, divSelectorFrom, divSelectorTo, parameters);

    sendToTextFromRadioTree(divSelectorFrom, divSelectorTo, parameters)
}

function sendToTextFromRadioTree(divSelectorFrom, divSelectorTo, parameters)
{
    var texts = takeFromRadioTree(divSelectorFrom);
    var start = ('start' in parameters) ? parameters['start'] : '';
    var finish = ('finish' in parameters) ? parameters['finish'] : '';
    var text = start + texts.join(parameters['separator']) + finish;
    setToTextField(divSelectorTo, text);
}

function relayFromTextToText(divSelectorFrom, divSelectorTo, configs, parameters)
{
    createListener(divSelectorFrom, configs, sendFromTextToText, divSelectorFrom, divSelectorTo, parameters);

    sendFromTextToText(divSelectorFrom, divSelectorTo);
}

function relayFromDateToText(divSelectorFrom, divSelectorTo, configs, parameters)
{
    createListener($(divSelectorFrom).closest('div'), configs, sendFromTextToText, divSelectorFrom, divSelectorTo, parameters);

    sendFromTextToText(divSelectorFrom, divSelectorTo);
}

function sendFromTextToText(divSelectorFrom, divSelectorTo, configs)
{
    setToTextField(divSelectorTo, takeFromTextField(divSelectorFrom));
}

function setToTextField(divSelectorTo, text) {
    $(divSelectorTo).val(text);
}

function takeFromTextsField(divSelectorsFrom) {
    var texts = [];
    for (var key in divSelectorsFrom) {
        texts.push($(divSelectorsFrom[key]).val());
    }

    return texts;
}

function takeFromTextField(divSelectorFrom) {
    return $(divSelectorFrom).val();
}

function takeFromRadioTree(selectorFrom) {
    var texts = [];

    $("input[id*='" + selectorFrom + "']:checked").each(function () {
        texts.push($(this).closest('label').find("span").text());
    });
    
    return texts;
}