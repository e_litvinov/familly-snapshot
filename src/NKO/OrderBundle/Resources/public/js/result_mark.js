'use strict';

$(document).ready(function ($) {
    //итоговая оценка
    var finalMark = 0,
        $finalDecisionDefaultRadio = $('input[id*="_finalDecision"][type_decision="1"]'),
        $finalDecisionRadios = $('input[id*="_finalDecision"]'),
        isMarkListReset = $('input[id*="isMarkListReset"]').val()
    ;

    sumMarks();
    /**
     * Функция для подсчёта итоговой оценки
     */
    function sumMarks() {
        //проход по всем инпутам с оценками
        $('td[class*="_marks-mark"] input').each(function(index) {

            //получаем целочисленное значение инпута
            var inputValue = parseInt($(this).val(), 10);

            //если оценка в первом инпуте 0
            if(index ==0 &&  inputValue == 0 && isMarkListReset){
                //во все поля оценок заносятся нули
                finalMark = 0;
                $('td[class*="_marks-mark"] input').val(0);
                $finalDecisionDefaultRadio.iCheck("check");
                $finalDecisionRadios.closest(".radio").addClass("readonly");

                $finalDecisionDefaultRadio.attr("checked", true);
            }
            else if(inputValue){
                finalMark += inputValue;
                $finalDecisionRadios.closest(".radio").removeClass("readonly");
            }
        });

        //подставляем значение в инпут с итоговой оценкой
        $('div[id*="_finalMark"] input').val(finalMark);
        $('div[id*="_markWithCoeff"] input').val(finalMark * $("input[id$='_coefficient']").val());

        //обнуляем счётчик оценок
        finalMark = 0;
    }

    $finalDecisionRadios.on("ifUnchecked", function(){
        $(this).attr("checked", false);
    });

    $finalDecisionRadios.on("ifChecked", function(){
        $(this).attr("checked", true);
    });

    //пересчёт значений при измении значени инпута с оценками
    $('td[class*="_marks-mark"] input').on('change', function(){
        // ($(this).val() && $(this).val() != 0) ? sumMarks() : $('div[id*="_finalMark"] input').val(0);
        sumMarks();
    });

    autosize($("table textarea"));

    //Скрывает заголовок столбца с максимальным значением оценки
    $('table tbody tr').each(function() {
        // var $helpBlock = $(this).find('p[id="questions"]'),
        var   $helpRange = $(this).find('td[id="help-range"]');
        var   $helpQuestion = $(this).find('td[id="help-question"]');
        // //Скрывает столбец с вопросами
        // $helpBlock.parent().hide();
        //
        // //Скрывает столбец с максимальным значением оценки
        // $rangeMarkBlock.parent().hide();
        //
        //Вставка вопросов перед каждой строкой
        // $("<tr><td colspan='3'><span>"+$helpBlock.text()+"</span></td></tr>").insertBefore($(this));

        //Вставка максимального значения оценки как help блока
        $(this).find('td[class*="_marks-mark"]:last').append('<label class="help">'
            + $helpRange.text() +'</label>');
        $(this).find('td[class="questionText"]:last').append('<label class="help">'
            + $helpQuestion.text() +'</label>');

        $(this).find('td[class*="_marks-rationale"]:last')
            .append('<label class="help">Минимальное количество символов 100</label>');

    });
});