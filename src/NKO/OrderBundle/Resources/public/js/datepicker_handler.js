'use strict';

$(document).ready(function ($) {
    /**
     * Установка параметров датапикера при фокусе в области инпута, отвечающего за дату
     */
    $('body').on('focus', 'input.datepicker', function () {
        //вытягивает параметры для датапикера из атрибутов инпута
        var startDate = $(this).attr('data-date-start-date');
        var dateFormat = $(this).attr('data-date-format');
        var endDate = $(this).attr('data-date-end-date');

        $(this).attr('readonly', true);
        //инициализирует датапикер
        $(this).datetimepicker({
            viewMode: 'years',
            format: dateFormat,
            language: 'ru',
            defaultDate: startDate,
            minDate: startDate,
            maxDate: endDate,
            pickTime: false
        }).on('change', function(){
            $('.bootstrap-datetimepicker-widget').hide();
        });
    });
});