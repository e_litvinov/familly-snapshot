'use strict';

function setPlaceHolderToSelect(selector) {
    $.each(selector, function() {
        $(this).select2({
            placeholder: 'Не выбран',
            width : '100%'
        });
        $(this).trigger('change');
    });
}