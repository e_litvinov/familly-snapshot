function allowOnlyNumeric(selector, isFloat) {
    $(selector).each(function () {
        if (isFloat === undefined) {
            $(this).on("keypress", function (evt) {
                if ((evt.which < 48 || evt.which > 57) && evt.which !== 8) {
                    evt.preventDefault();
                }
            });
        } else {
            $(this).on("keypress", function (evt) {
                if ((evt.which < 48 || evt.which > 57) && evt.which !== 8 && evt.which !== 44 && evt.which !== 46) {
                    evt.preventDefault();
                }
            });
        }
    });
}