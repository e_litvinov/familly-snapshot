<?php

namespace NKO\OrderBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use NKO\UserBundle\Entity\NKOUser;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;

class CompetitionAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'agreement' => 'Добавьте образец согласия на обработку персональных данных',
        'realizationYear' => 'Год реализации',
        'relatedCompetition' => 'Связанный конкурс',
        'relatedReportForm' => 'Связанный мастер отчёта',
        'organizations' => 'Выберите организации',
        'bannedOrganizations' => 'Выбрать запрещённые организации',
        'organizationsAccess' => 'Доступ организаций',
        'isNeedContacts' => 'Показывать Контактную информацию',
        'isEditableByWinner' => 'Редактируется Победителями',
        'bannedReportForm' => 'Выберите мастер отчета',
        'isMarkListReset' => 'Обнуление оценки.',
        'expertNumber' => 'Количество экспертов для "Не стандартного" распределения.',
    ];

    const HELP = [
        'bannedReportForms' =>
            'Если организация имеет сумму гранта для одного из выбранных мастеров отчета, то данный конкурс действует по правилам поля "Доступ организаций".',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getEntityManager();
        $organizations = $em->createQueryBuilder()->select('o')
            ->from(NKOUser::class, 'o')
            ->orderBy('o.nko_name')
            ->getQuery()
            ->getResult();
        $types = $this->getConfigurationPool()->getContainer()
            ->get('nko.list_application')
            ->getApplicationsClass();
        $formMapper
            ->add('name', TextType::class)
            ->add('realizationYear', DateTimePickerType::class, array(
                    'required' => false,
                    'label' => self::DESCRIPTION ['realizationYear'],
                    'dp_view_mode' => 'years',
                    'format' => 'yyyy',
                    'dp_pick_time' => false,
            ))
            ->add('applicationClass', ChoiceType::class, array(
                'translation_domain' => 'NKOOrderBundle',
                'choices' => $types))
            ->add('relatedCompetition', 'sonata_type_model', [
                'required' => false,
                'btn_add' => false,
                'label' =>self::DESCRIPTION['relatedCompetition']
            ])
            ->add('relatedReportForm', 'sonata_type_model', [
                'required' => false,
                'btn_add' => false,
                'label' =>self::DESCRIPTION['relatedReportForm']
            ])
            ->add('start_date', 'sonata_type_datetime_picker', array(
                'format' => 'dd.MM.yyyy, HH:mm',
                'attr' => array(
                    'data-date-format' => 'DD.MM.YYYY, HH:mm',
                )
            ))
            ->add('finish_date', 'sonata_type_datetime_picker', array(
                'dp_language' => 'ru',
                'format' => 'dd.MM.yyyy, HH:mm',
            ))
            ->add('finishEstimate', 'sonata_type_datetime_picker', array(
                'required' => false,
                'format' => 'dd.MM.yyyy, HH:mm',
            ))
            ->add('finalDecisions')
            ->add('helpForRationaleFinalDecision', null, array(
                'help' => 'Подсказка для обоснования итогового решения в оценке эксперта. По умолчанию подсказка "Пожалуйста, подробно обоснуйте свое решение (не менее 50 символов)"'
            ))
            ->add('organizationsAccess', ChoiceFieldMaskType::class, [
                'label' => self::DESCRIPTION['organizationsAccess'],
                'choices' => [
                    self::DESCRIPTION['organizations'] => 0,
                    self::DESCRIPTION['bannedOrganizations'] => 1],
                'map' => [
                    1 => ['organizations', 'bannedReportForms'],
                    0 => ['organizations', 'bannedReportForms'],
                ],
                'required' => true,
            ])
            ->add('organizations', null, array(
                'label' => self::DESCRIPTION['organizations'],
                'choice_label' => 'nko_name',
                'by_reference' => false,
                'choices' => $organizations,
            ), array(
                'admin_code' => 'sonata.admin.nko.nko_user'
            ))
            ->add('bannedReportForms', null, [
                'by_reference' => false,
                'label' => self::DESCRIPTION['bannedReportForm'],
                'required' => false,
                'help' => self::HELP['bannedReportForms'],
            ])
            ->add('agreement', CKEditorType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['agreement']
            ))
            ->add('expertNumber', TextType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['expertNumber'],
            ])
            ->add('isNeedContacts', CheckboxType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['isNeedContacts'],
            ])
            ->add('isEditableByWinner', CheckboxType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['isEditableByWinner'],
            ])
            ->add('isMarkListReset', CheckboxType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['isMarkListReset'],
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('start_date', null, array(
                'pattern' => 'dd-MM-Y HH:mm'
            ))
            ->add('finish_date', null, array(
                'pattern' => 'dd-MM-Y HH:mm'
            ))
            ->add('finishEstimate', null, array(
            'pattern' => 'dd-MM-Y HH:mm'
            ));
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Изменение настроек заявки', $object->getName());
    }
}
