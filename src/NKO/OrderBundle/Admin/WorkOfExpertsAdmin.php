<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/21/17
 * Time: 10:42 AM
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use NKO\UserBundle\Traits\CurrentUserTrait;

class WorkOfExpertsAdmin extends AbstractAdmin
{
    use CurrentUserTrait;

    protected $baseRouteName = 'sonata_admin_nko_order_work_of_experts';

    protected $baseRoutePattern = 'work_of_experts';

    public function getFilterParameters()
    {
        $this->datagridValues = array_merge([
            'competitions' => [
                'value' => $this->getCurrentCompetition()
            ],
        ],

            $this->datagridValues
        );

        return parent::getFilterParameters();
    }

    public function getCurrentCompetition()
    {
        $competitions = $this
            ->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getRepository('NKOOrderBundle:Competition')
            ->findCompetitionsForEstimate();
        if(!$competitions){
            $competitions = $this
                ->getConfigurationPool()
                ->getContainer()
                ->get('doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->findAll();
        }

        if($competitions){

            return reset($competitions)->getId();
        }

        return null;
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('surname')
            ->add('name')
            ->add('patronymic')
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('competitions')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        $collection->remove('edit');
        $collection->add('clean_expert_activity');
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if ($this->getCurrentUser()->hasRole('ROLE_EMPLOYEE_ADMIN') || $this->getCurrentUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addChild('clean_expert_activity', array('uri' => $this->generateUrl('clean_expert_activity', [
                'competition' => $this->getFilterParameters()['competitions']['value']
            ])));
        }
    }
}