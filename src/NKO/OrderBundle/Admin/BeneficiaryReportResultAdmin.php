<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 25.10.16
 * Time: 11:24
 */
namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BeneficiaryReportResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('result', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Результат',
                ))
            ->add('indicator', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Показатель',
                ))
            ->add('planValue', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'План (на дату окончания проекта, согласно заявке)',
                ))
            ->add('factValue', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Факт (на дату окончания проекта)',
                ))
            ->add('expectedValue', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ожидается (на 01.06.2017) ',
                ))
            ->add('methodMeasurement', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Способ измерения',
                ))
            ->add('comments', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                ))
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$listMapper

    }

}