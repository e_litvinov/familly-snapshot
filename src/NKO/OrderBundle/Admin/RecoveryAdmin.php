<?php

namespace NKO\OrderBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class RecoveryAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->add('revert', $this->getRouterIdParameter().'/revert');
        $collection->add('download', $this->getRouterIdParameter().'/download');
        $collection->add('show_documents_list', $this->getRouterIdParameter().'/show_documents_list');
        $collection->add('download_document', $this->getRouterIdParameter().'/download_document');
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username')
            ->add('nko_name')
            ->add('psrn')
            ->add('updatedAt', null, ['template' => 'NKODefaultBundle:Admin:list__document.html.twig'])
            ->add('_action', null, array(
                'actions' => array(
                    'download' => array( 'template' =>  'NKOOrderBundle:CRUD:list__action_download_send.html.twig'),
                    'revert' =>array( 'template' =>  'NKOOrderBundle:CRUD:list__action_revert.html.twig'),
                    'show_documents_list' => array(
                        'template' =>  array('NKOOrderBundle:CRUD:list__action_download_document.html.twig')
                    ),
                )
            ));
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->select(
            'partial ' .
            $query->getRootAlias() .
            '.{id, username, nko_name, psrn, createdAt}'
        );

        return $query;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('nko_name')
            ->add('psrn')
            ->add('competition')
        ;
    }
}
