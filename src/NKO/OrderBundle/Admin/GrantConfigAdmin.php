<?php

namespace NKO\OrderBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use NKO\OrderBundle\Entity\ApplicationHistory;

class GrantConfigAdmin extends AbstractAdmin
{
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('reportForm.competition.name', null, [
                'label' => 'Конкурс'
            ])
            ->add('reportForm.title', null, [
                'label' => 'Мастер отчета'
            ])
            ->add('sumGrant', 'text', array(
                'label' => 'Сумма гранта',
                'editable' => true
            ))
            ->add('contract', null, array(
                'label' => 'Номер договора',
                'editable' => true
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'prefill_report' => array('template' => 'NKOOrderBundle:CRUD:list__action_prefill_report.html.twig'),
                    'checkout_values' => array('template' => 'NKOOrderBundle:CRUD:list__action_checkout_values.html.twig'),
                    'undo_accepting' => ['template' => 'NKOOrderBundle:CRUD:list__action_undo_accepting.html.twig'],
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('applicationHistory', null, array(
                'label' => 'Организация',
                'admin_code' => 'sonata.admin.nko.order.expert_mark',
            ), null, array(
                'choices' => $this->getHistories()
            ))
        ;
    }

    public function getHistories()
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $competition = $this->getRequest()->get('competition_id');

        $query = $em
            ->createQueryBuilder()
            ->select('a.id')
            ->from(ApplicationHistory::class, 'a')
            ->where('a.isSpread = :isSpread');

        if ($competition) {
            $query->andWhere('a.competition = :competition');
        }

        $arr = $query
            ->setParameters([
                'competition' => $competition,
                'isSpread' => true,
            ])
            ->getQuery()
            ->getResult();

        return $em->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->where('a.id IN (:ids)')
            ->setParameters(['ids' => array_merge_recursive(...$arr)['id']])
            ->getQuery()
            ->getResult();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        $collection->add('prefill_report', $this->getRouterIdParameter().'/prefill_report');
        $collection->add('checkout_values', $this->getRouterIdParameter().'/checkout_values');
        $collection->add('undo_accepting', $this->getRouterIdParameter().'/undo_accepting');
    }
}
