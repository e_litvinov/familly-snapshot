<?php

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuestionMarkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('criteria', 'sonata_type_model', [
                'required' => false,
                'btn_add' => 'Добавить',
            ])
            ->add('competitions')
            ->add('question',  TextareaType::class)
            ->add('helpField', null, ['help' => 'Оставьте поле пустым, чтобы отображать подсказку по умолчанию'])
            ->add('rangeMark');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('question')
            ->add('criteria')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => []
                ]
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('competitions');
    }
}