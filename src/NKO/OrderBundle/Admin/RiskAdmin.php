<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.08.16
 * Time: 0:30
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class RiskAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('keyRisk', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ключевые риски',
                ))
            ->add('actionToReduceRisk', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Действия, нацеленные на снижение риска',
                ));
    }
}