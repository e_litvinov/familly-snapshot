<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application as KNSApplucation2017;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;

class ProjectMemberAdmin extends EmbeddedAdminDecorator
{

    const DESCRIPTION = [
        'custom' => [
            'functionalResponsibilities' => 'Функциональные обязанности',
            'fullName' => 'ФИО участника проекта<i>(полностью)</i>',
            'role' => 'Роль в проекте <i>(бухгалтер, менеджер, психолог и пр.)</i>',
            'employmentRelationship' => 'Трудовые отношения с организацией на период проекта <br>собственный сотрудник/ привлеченный сотрудник / доброволец',
            'briefInformation' => 'Краткие сведения об участнике <i>(опыт работы, квалификация и пр.)</i>',
        ],
        'NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application' => [
            'fullName' => 'ФИО',
            'role' => 'Роль в проекте',
            'employmentRelationship' => '<b>Трудовые отношения с организацией</b> на период проекта',
            'briefInformation' => 'Краткие сведения об участнике ',
        ]
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $class = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());
        $description =  key_exists($class, self::DESCRIPTION) ? $class : 'custom';

        $formMapper
            ->add('fullName', TextType::class, array(
                'label' => self::DESCRIPTION[$description]['fullName'],
                'required' => false,
                'attr' => array(
                    'placeholder' => '-',
                    'maxlength' => 255
                )
            ))
            ->add('role', TextType::class, array(
                'label' => self::DESCRIPTION[$description]['role'],
                'required' => false,
                'attr' => array(
                    'placeholder' => '-',
                    'maxlength' => 255
                )
            ));

        if (!$this->getParentFieldDescription()->getAdmin()->getSubject() instanceof KNSApplucation2017) {
                $formMapper
                    ->add('functionalResponsibilities', FullscreenTextareaType::class, array(
                        'label' => self::DESCRIPTION[$description]['functionalResponsibilities'],
                        'required' => false,
                        'attr' => array(
                            'placeholder' => '-',
                        )
                    ));
        }

        $formMapper
            ->add('employmentRelationship', EntityType::class, array(
                'class' => 'NKOOrderBundle:Farvater2017\EmploymentRelationship',
                'label' => self::DESCRIPTION[$description]['employmentRelationship'],
                'required' => false,
                'expanded' => false,
                'multiple' => false
            ))
            ->add('briefInformation', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION[$description]['briefInformation'],
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }
}
