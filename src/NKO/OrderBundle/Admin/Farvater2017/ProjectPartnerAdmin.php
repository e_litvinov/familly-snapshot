<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ProjectPartnerAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('organizationName', FullscreenTextareaType::class, array(
                'label' => 'Название организации',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('briefInformation', FullscreenTextareaType::class, array(
                'label' => 'Краткие сведения об организации',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('projectParticipation', FullscreenTextareaType::class, array(
                'label' => 'Участие в проекте',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }
}
