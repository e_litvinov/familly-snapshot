<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrganizationResourceAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, array(
                'label' => 'Наименование ресурса',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('description', FullscreenTextareaType::class, array(
                'label' => 'Описание ресурса',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }
}
