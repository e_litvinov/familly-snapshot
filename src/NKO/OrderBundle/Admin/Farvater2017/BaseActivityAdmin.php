<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BaseActivityAdmin extends EmbeddedAdminDecorator
{
    const DESCRIPTION = [
        'custom' => [
            'expectedResults' => 'Основные ожидаемые результаты'
        ],
        'NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application' => [
            'expectedResults' => 'Результаты'
        ]
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $class = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());
        $description =  key_exists($class, self::DESCRIPTION) ? $class : 'custom';

        $formMapper
            ->add('activity', FullscreenTextareaType::class, array(
                'label' => 'Задача / мероприятие / действие',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('deadLine', TextType::class, array(
                'label' => 'Сроки',
                'required' => false,
                'attr' => array(
                    'placeholder' => '-',
                    'maxlength' => 255
                )
            ))
            ->add('expectedResults', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION[$description]['expectedResults'],
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }
}
