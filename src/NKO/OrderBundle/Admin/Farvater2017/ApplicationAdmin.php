<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Form\CustomTextType;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use NKO\OrderBundle\AdminConfigurator\BankDetailsAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;

class ApplicationAdmin extends BaseApplicationAdmin
{
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                "Farvater-2017",
                "Budget",
                'BankDetails',
            ]
        );

        $this->description = $this->description + array(
            'expectedProjectResultsLabel' => '2.3. <b>Ожидаемые результаты проекта</b> в отношении представленной Практики<br><br>
                <em>Целевые значения результатов проекта в целом (к концу третьего года) указываются ориентировочные – при подаче промежуточной отчетности (в конце первого и второго года реализации проекта) возможно уточнение целевых значений, показателей и результатов.</em><br><br>',
            'activityLabel' => '5.1. Перечислите <b>основные задачи / мероприятия / действия</b>, которые будут осуществлены <b>в первый год</b> реализации проекта<br><br>',
            'activityDescriptionLabel' => '5.2. Кратко опишите основные <b>задачи / мероприятия / действия</b>, которые планируются <b>в течение второго и третьего годов</b> реализации проекта<br><br>',
            'appsLabel' => '<b>При подаче заявки Заявитель должен представить в электронном виде скан-копии (или ссылки, если это оговорено дополнительно) следующих документов, заверенных подписью руководителя и печатью организации: </b><br><br>',
            'expectedPracticeSpreadResultsLabel' => '3.5. Ожидаемые <b>результаты распространения и внедрения Практики</b> в деятельность других организаций<br><br>
                <em>Целевые значения результатов проекта в целом (к концу третьего года) указываются ориентировочные – при подаче промежуточной отчетности (в конце первого и второго годов реализации проекта) возможно уточнение целевых показателей.</em><br><br>',
            );
        $this->description['bankCredentials'] = '1.1 <b>Банковские реквизиты организации</b>';
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('РАЗДЕЛ 1 БАНКОВСКИЕ РЕКВИЗИТЫ');
        BankDetailsAdmin::configureFormFields($formMapper, AdminConfigurator::BANK_DETAILS[get_class($this->getSubject())]);
        $formMapper
            ->end()
            ->with('РАЗДЕЛ 2 ОПИСАНИЕ ПРАКТИКИ')
            ->add('briefPracticeDescription', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '2.1. <b>КРАТКОЕ ОПИСАНИЕ РЕАЛИЗОВАННОЙ ПРАКТИКИ (основные шаги, «изюминки»; чтобы у экспертов сложилось понимание сути вашей практики)</b><br><br>',
            ])
            ->add('projectPurpose', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '2.2. <b>Цель проекта (реализация практики)</b><br><br>
                        <em>Какую цель вы ставите в данном проекте в отношении реализации представленной на Конкурс практики? Например, внедрение в нее новых элементов, увеличение масштабов и пр.</em><br><br>',
                    'attr' => array('maxlength' => 255)
            ])
            ->add('expectedProjectResultsLabel', CustomTextType::class, [
                    'help' => $this->description['expectedProjectResultsLabel'],
            ])
            ->add('immediateResults', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '2.3.1 <b>Непосредственные результаты</b><br><br>
                        <em>Укажите, в каких мероприятиях примут участие представители целевых групп Практики в рамках проекта, какие именно услуги они получат. Приведите данные с разбивкой по целевым группам и годам.</em><br><br>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('socialResultIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '2.3.2. <b>Социальные результаты</b>',
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('beneficiaryImprovementIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '2.3.3. <b>Улучшение благополучия детей</b><br><br>
                        <em>Как улучшится благополучие детей благодаря реализации проекта? Укажите ориентировочное количество детей, благополучие которых улучшится – как минимум по одному из показателей из списка ниже.</em><br><br>',
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('otherBeneficiaryImprovementIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>Другое</b>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('additionalIndicatorIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '2.3.4. <b>Дополнительные показатели </b><br><br>
                        <em>Какие еще результаты будут достигнуты благодаря реализации проекта? Укажите по каждому из представленных ниже показателей.</em><br><br>',
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('expectedResultsStability', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '2.3.5. <b>Устойчивость ожидаемых результатов Практики</b><br><br>
                        <em>За счет чего изменения, достигнутые в процессе реализации проекта, сохранятся и после завершения проекта?</em><br><br>',
                    'attr' => array('maxlength' => 3000)
            ])
            ->end()
            ->with('3. Распространение практики')
            ->add('practiceMotivation', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '3.1. <b>Мотивация</b>, заинтересованность вашей организации в распространении и внедрении Практики в деятельность других организаций<br><br>
                        В какой степени у вашей организации есть интерес, желание и готовность стать отраслевым региональным ресурсным центром по распространению представленной на Конкурс Практики (в т.ч. после окончания проекта); выступать стажировочной площадкой (в т.ч. в рамках других Конкурсов Фонда Тимченко)?<br><br>Укажите, какой опыт распространения Практики уже есть у вашей организации.'
            ])
            ->add('targetAudiences', EntityType::class, [
                    'class' => 'NKOOrderBundle:Farvater\TargetAudience',
                    'required' => false,
                    'label' => '3.2. <b>Целевая аудитория</b> (распространение Практики)<br><br>
                        <em>Как вы считаете, какие организации сферы защиты детства будут заинтересованы во внедрении вашей Практики в свою деятельность?</em><br><br>',
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $this->getTargetAudiences(array('Farvater-2017'))
            ])
            ->add('targetAudienceEtc', TextType::class, [
                    'required' => false,
                    'label' => 'Иные (укажите)',
            ])
            ->add('practiceSpreadFormats', EntityType::class, [
                    'class' => 'NKOOrderBundle:Farvater2017\PracticeSpreadFormat',
                    'required' => false,
                    'label' => '3.3. <b>Какие форматы распространения Практики будут задействованы в рамках данного проекта?</b>',
                    'expanded' => true,
                    'multiple' => true,
            ])
            ->add('practiceSpreadFormatEtc', TextType::class, [
                    'required' => false,
                    'label' => 'Иные (укажите)',
            ])
            ->add('practiceSpreadTechnology', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '3.4. <b>Технология распространения и внедрения Практики в рамках проекта</b><br><br>
                        <em>Подробно опишите, как именно вы планируете распространять и внедрять вашу Практику в рамках трёхлетнего проекта. По возможности, приведите обоснования выбора форматов.<br><br>
                        Обратите внимание, что в рамках Конкурса приветствуются проактивные форматы, нацеленные на то, чтобы другие организации сферы защиты детства внедрили её в свою деятельность, а не только повысили осведомлённость.<br><br></em>'
            ])
            ->add('expectedPracticeSpreadResultsLabel', CustomTextType::class, [
                    'help' => $this->description['expectedPracticeSpreadResultsLabel'],
            ])
            ->add('practiceActivityIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '3.5.1. <b>Мероприятия / услуги по распространению Практики</b><br><br>
                        <em>При заполнении таблицы ориентируйтесь на сведения, указанные в п.3.3.</em><br><br>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('publicationIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '3.5.2. <b>Публикации</b>',
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('publicationIndexComment', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '3.5.2.1. Комментарий (при необходимости)'
            ])
            ->add('introductionIndexes', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '3.5.3. <b>Внедрение</b>',
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('introductionIndexComment', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '3.5.3.1. Комментарий (при необходимости)'
            ])
            ->add('spreadResultsStability', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '3.6. <b>Устойчивость результатов распространения Практики</b><br><br>
                        <em>Устойчивость результатов распространения и внедрения практики в деятельность других организаций и специалистов сферы защиты детства. Возможность создания и дальнейшей работы на базе организации-заявителя отраслевого ресурсного центра (стажировочной площадки) в области профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей.</em><br><br>'
            ])
            ->add('introductionFactors', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '3.7. <b>Факторы, способствующие внедрению</b> Практики другими специалистами (организациями)<br><br>
                        <em>Укажите, какие факторы способствуют успешному внедрению вашей Практики? Насколько сложно её внедрить в деятельность других специалистов и организаций? Какие ресурсы, компетенции потребуются от организаций и специалистов?</em><br><br>'
            ])
            ->end()
            ->with("РАЗДЕЛ 4 Мониторинг и оценка результатов. Развитие организационного потенциала")
            ->add('organizationalCapacity', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '4.1. <b>Организационный потенциал</b> вашей организации в области планирования, мониторинга, измерения и оценки социальных программ<br><br>
                        <em>Какими ресурсами, знаниями, навыками и компетенциями уже обладает ваша организация в данной сфере?</em><br><br>'
            ])
            ->add('expectedResults', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '4.2. Ожидаемые <b>результаты</b> проекта по блоку «Мониторинг и оценка результатов, повышение организационного потенциала»<br><br>
                        <em>Что именно вы планируете усилить и развить в рамках проекта для создания / развития системы мониторинга и оценки, доказательной базы эффективности практики за первый год реализации проекта? Как вы узнаете о достижении результата? </em><br><br>'
            ])
            ->end()
            ->with('РАЗДЕЛ 5 ПЛАН-ГРАФИК ПРОЕКТА')
            ->add('activityLabel', CustomTextType::class, [
                    'help' => $this->description['activityLabel'],
            ])
            ->add('practiceImplementationActivities', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>Реализация Практики</b>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'admin_code' => 'nko_order.admin.farvater2017.practice_implementation_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('practiceSpreadActivities', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>Распространение Практики</b>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'admin_code' => 'nko_order.admin.farvater2017.practice_spread_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('monitoringResultsActivities', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>Мониторинг и оценка результатов. Развитие организационного потенциала</b>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'admin_code' => 'nko_order.admin.farvater2017.monitoring_results_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('activityDescriptionLabel', CustomTextType::class, [
                    'help' => $this->description['activityDescriptionLabel'],
            ])
            ->add('practiceImplementationDescription', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '5.2.1. <b>Реализация практики проекта</b>'
            ])
            ->add('practiceSpreadDescription', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '5.2.2. <b>Распространение практики проекта</b>'
            ])
            ->add('monitoringResultsDescription', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => '5.2.3. <b>Мониторинг и оценка результатов. Развитие организационного потенциала</b>'
            ])
            ->end()
            ->with('РАЗДЕЛ 6 РЕСУРСНОЕ ОБЕСПЕЧЕНИЕ ПРОЕКТА')
            ->add('projectMembers', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '6.1. <b>Команда</b> проекта<br><br>
                        <em>Укажите специалистов, которые будут вовлечены в непосредственную реализацию проекта, а также их квалификацию, роль в Проекте и характер трудовых отношений с Организацией. Если ФИО участника пока неизвестно, то укажите его роль в проекте. Например, «Психолог 1».</em><br><br>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('projectPartners', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '6.2. <b>Партнеры и доноры Проекта</b> (при наличии)<br><br>
                        <em>Перечислите иные организации, участвующие в реализации проекта, с указанием их роли в проекте</em><br><br>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('organizationResources', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '6.3. <b>Имеющиеся у Организации ресурсы, необходимые для реализации Проекта (на период реализации проекта)</b><br><br>
                        <em>Укажите, какие ресурсы уже есть в распоряжении организации (собственные средства) или будут привлечены со стороны партнёров для реализации Проекта</em><br><br>',
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->end()
            ->with('РАЗДЕЛ 7 БЮДЖЕТ ПРОЕКТА')
            ->add('entirePeriodEstimatedFinancing', MoneyType::class, [
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 0,
                    'label' => '7.1. Предполагаемая сумма запрашиваемого финансирования <b>на весь период</b> реализации проекта 
                        <em>(в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»</em> (в рублях)'
            ])
            ->add('entirePeriodEstimatedCoFinancing', MoneyType::class, [
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 0,
                    'label' => '7.2. Предполагаемая сумма софинансирования <b>на весь период</b> реализации проекта 
                        <em>(в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»</em> (в рублях)'
            ])
            ->add('firstYearEstimatedFinancing', MoneyType::class, [
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 0,
                    'label' => '7.3. Сумма запрашиваемого финансирования <b>на первый год</b> реализации проекта 
                        <em>(в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»</em> (в рублях)'
            ])
            ->add('firstYearEstimatedCoFinancing', MoneyType::class, [
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 0,
                    'label' => '7.4. Сумма софинансирования <b>на первый год</b> реализации проекта 
                        <em>(в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»</em> (в рублях)'
            ])
            ->end()
            ->with('РАЗДЕЛ 8 РИСКИ ПРОЕКТА')
            ->add('risks', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '8.1. Обстоятельства, которые могут воспрепятствовать успешной реализации Проекта, и действия, которые предприняты (или будут предприняты) для снижения рисков.<br><br>
                        <em>Отсутствие финансирования проекта не является риском</em><br><br>',
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'admin_code' => 'sonata.admin.nko.order.kns2017.risk',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->end()
            ->with('ПРИЛОЖЕНИЯ')
            ->add('appsLabel', CustomTextType::class, [
                    'help' => $this->description['appsLabel'],
            ])
            ->add('regulation', FilePreviewType::class, [
                    'label' => '1. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа (прикрепляется повторно); pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('projectBudget', FilePreviewType::class, [
                    'label' => '2. Бюджет проекта – заполненный файл установленного образца; excel',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('bankLetterExistenceAccount', FilePreviewType::class, [
                    'label' => '3. Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта (скан-копия); pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('processingPersonalDataConsent', FilePreviewType::class, [
                    'label' => '4. Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке; pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('confirmingAuthorityPersonDocument', FilePreviewType::class, [
                    'label' => '5. Документ, подтверждающий полномочия лица, которое будет подписывать договор (<b>если</b> договор будет подписывать не руководитель организации); pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('confirmingLegalStatusEntityDocument', FilePreviewType::class, [
                    'label' => '6. Документ, подтверждающий статус юридического лица (решение о создании учреждения и т.п.) (<b>только</b> для государственных и муниципальных учреждений); pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('latestAnnualReport', FilePreviewType::class, [
                    'label' => '7. Последний годовой отчёт <b>(при наличии)</b> (скан-копия с подписью руководителя и печатью организации); pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('latestAnnualReportLink', UrlType::class, [
                    'required' => false,
                    'label' => 'Ссылка на его версию в сети Интернет (прикладывается либо файл, либо ссылка)',
                    'help' => 'URL должен начинаться с http:// или https://'
            ])
            ->add('ministryJusticeReport', FilePreviewType::class, [
                    'label' => '8. Отчёт в Министерство юстиции Российской Федерации /иной регистрирующий орган за предшествующий отчетный период (скан-копия, с подписью руководителя и печатью организации) (<b>за исключением</b> организаций-заявителей – государственных и муниципальных учреждений). pdf',
                    'data_class' => null,
                    'required' => false
            ])
            ->add('ministryJusticeReportLink', UrlType::class, [
                    'required' => false,
                    'label' => 'Ссылка на его версию, размещенную на Информационном портале Министерства юстиции Российской Федерации по адресу: <a href="http://unro.minjust.ru/">http://unro.minjust.ru/</a> (прикладывается либо файл, либо ссылка)',
                    'help' => 'URL должен начинаться с http:// или https://'
            ])
        ;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig')
        );
    }

    public function getTargetAudiences($application_types)
    {
        $audiences = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:Farvater\TargetAudience')
            ->findBy(
                array('applicationType' => $application_types)
            );

        return $audiences;
    }
}
