<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 4/7/17
 * Time: 1:16 PM
 */

namespace NKO\OrderBundle\Admin\Farvater2017;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BaseIndexAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $entityClass = $this->getClass();

        switch ($entityClass){
            case 'NKO\OrderBundle\Entity\Farvater2017\SocialResultIndex':
                $indexNameLabel = 'Показатели, важные для Фонда';
                $firstYearTargetValueLabel = 'на конец I года';
                $secondYearTargetValueLabel = 'на конец II года';
                $thirdYearTargetValueLabel = 'на конец III года';
                break;
            case 'NKO\OrderBundle\Entity\Farvater2017\BeneficiaryImprovementIndex':
                $indexNameLabel = 'Показатель';
                $firstYearTargetValueLabel = 'на конец I года';
                $secondYearTargetValueLabel = 'на конец II года';
                $thirdYearTargetValueLabel = 'на конец III года';
                break;
            case 'NKO\OrderBundle\Entity\Farvater2017\OtherBeneficiaryImprovementIndex':
                $indexNameLabel = 'Показатель';
                $firstYearTargetValueLabel = 'на конец I года';
                $secondYearTargetValueLabel = 'на конец II года';
                $thirdYearTargetValueLabel = 'на конец III года';
                break;
            case 'NKO\OrderBundle\Entity\Farvater2017\AdditionalIndicatorIndex':
                $indexNameLabel = 'Дополнительные показатели';
                $firstYearTargetValueLabel = 'на конец I года';
                $secondYearTargetValueLabel = 'на конец II года';
                $thirdYearTargetValueLabel = 'на конец III года';
                break;
            case 'NKO\OrderBundle\Entity\Farvater2017\PublicationIndex':
                $indexNameLabel = 'Показатель';
                $firstYearTargetValueLabel = 'Целевое значение на конец I года';
                $thirdYearTargetValueLabel = 'Целевое значение на конец III года';
                break;
            case 'NKO\OrderBundle\Entity\Farvater2017\IntroductionIndex':
                $indexNameLabel = 'Показатель';
                $firstYearTargetValueLabel = 'Целевое значение на конец I года';
                $secondYearTargetValueLabel = 'Целевое значение на конец II года';
                $thirdYearTargetValueLabel = 'Целевое значение на конец III года';
                break;
            default:
                $indexNameLabel = '';
                $firstYearTargetValueLabel = '';
                $thirdYearTargetValueLabel = '';
                break;
        }

        $formMapper
            ->add('indexName', TextareaType::class, array(
                'label' => $indexNameLabel,
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('firstYearTargetValue', NumberType::class, array(
                'label' => $firstYearTargetValueLabel,
                'required' => false,
                'scale' => 0,
                'attr' => array('placeholder' => '-')
            ));
            if($entityClass != 'NKO\OrderBundle\Entity\Farvater2017\PublicationIndex' &&
                $entityClass != 'NKO\OrderBundle\Entity\Farvater2017\PracticeActivityIndex') {
                $formMapper
                    ->add('secondYearTargetValue', NumberType::class, array(
                        'label' => $secondYearTargetValueLabel,
                        'required' => false,
                        'scale' => 0,
                        'attr' => array('placeholder' => '-')
                    ));
            }
        $formMapper
            ->add('thirdYearTargetValue', NumberType::class, array(
                'label' => $thirdYearTargetValueLabel,
                'required' => false,
                'scale' => 0,
                'attr' => array('placeholder' => '-')
            ))
            ->add('measurementMethod', FullscreenTextareaType::class, array(
                'label' => 'Способ измерения',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }

}