<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class PracticeActivityIndexAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('indexName', FullscreenTextareaType::class, array(
                'label' => 'Мероприятия / услуги',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('firstYearTargetValue', FullscreenTextareaType::class, array(
                'label' => 'на конец I года',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('thirdYearTargetValue', FullscreenTextareaType::class, array(
                'label' => 'на конец III года',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('measurementMethod', FullscreenTextareaType::class, array(
                'label' => 'Способ измерения',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }
}
