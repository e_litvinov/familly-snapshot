<?php

namespace NKO\OrderBundle\Admin\Farvater2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ImmediateResultAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('activity', FullscreenTextareaType::class, array(
                'label' => 'Услуги, мероприятия и пр.',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('targetGroup', FullscreenTextareaType::class, array(
                'label' => 'Целевая группа',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
            ->add('firstYearActivitiesCount', NumberType::class, array(
                'label' => 'Число' . PHP_EOL . 'услуг / меро-' . PHP_EOL . 'приятий',
                'required' => false,
                'scale' => 0,
                'attr' => array('placeholder' => '-')
            ))
            ->add('firstYearBeneficiariesCount', NumberType::class, array(
                'label' => 'Число' . PHP_EOL . 'благо-' . PHP_EOL . 'полу-'. PHP_EOL . 'чателей',
                'required' => false,
                'scale' => 0,
                'attr' => array('placeholder' => '-')
            ))
            ->add('thirdYearActivitiesCount', NumberType::class, array(
                'label' => 'Число' . PHP_EOL . 'услуг / меро-' . PHP_EOL . 'приятий',
                'required' => false,
                'scale' => 0,
                'attr' => array('placeholder' => '-')
            ))
            ->add('thirdYearBeneficiariesCount', NumberType::class, array(
                'label' => 'Число' . PHP_EOL . 'благо-' . PHP_EOL . 'полу-'. PHP_EOL .'чателей',
                'required' => false,
                'scale' => 0,
                'attr' => array('placeholder' => '-')
            ))
            ->add('measurementMethod', FullscreenTextareaType::class, array(
                'label' => 'Способ измерения',
                'required' => false,
                'attr' => array('placeholder' => '-')
            ))
        ;
    }
}
