<?php

namespace NKO\OrderBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\BaseApplication;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface as MenuItemInterface;

class SendBaseApplicationAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
        '_sort_by' => 'createdAt'
    );

    protected $baseRouteName = 'admin_sonata_nko_order_send_application';

    protected $baseRoutePattern = 'send_application';

    public function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('delete');
        $collection->add('download', $this->getRouterIdParameter().'/download');
        $collection->add('show_documents_list', $this->getRouterIdParameter().'/show_documents_list');
        $collection->add('download_document', $this->getRouterIdParameter().'/download_document');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username')
            ->add('nko_name')
            ->add('psrn')
            ->add("legalRegion")

            ->add('isAppropriate', null, ['editable' => true])
            ->add('isAccepted', null, ['editable' => true])
            ->add('winner', null, array(
                'editable' => true
            ))
            ->add('competition')
            ->add('_action', null, array(
                'actions' => array(
                    'download' => array( 'template' =>  'NKOOrderBundle:CRUD:list__action_download_send.html.twig'),
                    'show_documents_list' => array(
                        'template' => 'NKOOrderBundle:CRUD:list__action_download_document.html.twig'
                    ),
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('nko_name')
            ->add('psrn')
            ->add('isAppropriate')
            ->add('isAccepted')
            ->add('competition')
            ->add('winner');
        ;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAlias().'.isSend = 1');
        $query->select(
            'partial ' .
            $query->getRootAlias() .
            '.{id, username, nko_name, psrn, legalRegion, isAppropriate, isAccepted, winner}'
        );
        return $query;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'NKOOrderBundle:CRUD:base_list_marks.html.twig';
                break;
            case 'mark_list_outer_row':
                return 'NKOOrderBundle:CRUD:base_list_mark_list_outer_row.html.twig';
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function preUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine');

        $unserializedObject = unserialize($object->getData());
        $application = $em->getRepository(BaseApplication::class)
            ->find($unserializedObject->getId());

        $application->setIsAccepted($object->getIsAccepted());
        $application->setWinner($object->getWinner());

        if ($unserializedObject instanceof Application) {
            $briefApplicationHistory = $container
                ->get('nko_order.resolver.related_competition_resolver')
                ->getApplicationHistory($unserializedObject, $object->getIsSpread());
            $briefApplicationHistory->setWinner($object->getWinner());
            $briefApplicationHistory->setIsAccepted($object->getIsAccepted());

            $briefApplication = unserialize($briefApplicationHistory->getData());
            $briefApplication = $em->getRepository(BaseApplication::class)
                ->find($briefApplication->getId());
            $briefApplication->setWinner($object->getWinner());
            $briefApplication->setIsAccepted($object->getIsAccepted());
        }
    }
}
