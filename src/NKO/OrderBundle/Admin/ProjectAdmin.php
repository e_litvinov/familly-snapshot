<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.08.16
 * Time: 11:20
 */

namespace NKO\OrderBundle\Admin;

use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNSApplication2018;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application as KNSSecondStage2017;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNSSecondStage2018;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNS;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNSApplication2019;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as HarborApplication2019;
use NKO\OrderBundle\Traits\AdminFieldsTrait;
use NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018\Application as KNSThirdStage2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Application as KNSApplication2020;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application as KNSSecondStage2019;

class ProjectAdmin extends EmbeddedAdminDecorator
{
    use AdminFieldsTrait;

    const DESCRIPTIONS = [
        2018 => [
            'deadline' => 'Период реализации',
            'result' => 'Основные результаты',
            'donorsPartnersAndTheirRole' => 'Источник финансирования'
        ],
        'default' => [
            'deadline' => 'Сроки реализации',
            'result' => 'Результаты',
            'donorsPartnersAndTheirRole' => 'Доноры и партнеры и их роль'
        ]
    ];

    const TYPES = [
        2018 => [
            Application::class,
            KNSApplication2018::class,
            KNSSecondStage2017::class,
            KNSSecondStage2018::class,
            ContinuationKNS::class,
            KNSApplication2019::class,
            HarborApplication2019::class,
            KNSApplication2020::class,
            KNSSecondStage2019::class,
        ]
    ];

    const FINANCING_SOURCE_TYPE = [
        KNSApplication2018::class,
        KNSSecondStage2017::class,
        KNSSecondStage2018::class,
        KNSThirdStage2018::class,
        ContinuationKNS::class,
        KNSApplication2019::class,
        HarborApplication2019::class,
        KNSApplication2020::class,
        KNSSecondStage2019::class,
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $application = $this->getParentFieldDescription()->getAdmin()->getSubject();
        $type = $this->generateType(self::TYPES);

        $formMapper
            ->add('deadline', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['deadline'],
                    'attr' => array('maxlength' => 255)
                ))
            ->add('projectName', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => 'Название проекта',
                ));

        if (in_array(get_class($application), self::FINANCING_SOURCE_TYPE)) {
            $formMapper
                ->add('financingSourceTypes', 'sonata_type_model', array(
                    'label' => self::DESCRIPTIONS[$type]['donorsPartnersAndTheirRole'],
                    'multiple' => true,
                    'by_reference' => false,
                    'btn_add' =>false,
                    'query' => $em->getRepository(FinancingSourceType::class)
                        ->createQueryBuilder('t')
                        ->where('t.types like :type')
                        ->setParameter('type', '%' . ApplicationTypes::KNS_APPLICATION_2018 . '%')
                ), array('admin_code' => 'nko_order.admin.indicator'));
        } else {
            $formMapper
                ->add('donorsPartnersAndTheirRole', TextareaType::class, array(
                        'required' => false,
                        'label' => self::DESCRIPTIONS[$type]['donorsPartnersAndTheirRole'],
                        'attr' => array('maxlength' => 255)
                    ));
        }

        $formMapper
            ->add('result', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['result'],
                    'attr' => array('maxlength' => 1500)
                ));
    }
}
