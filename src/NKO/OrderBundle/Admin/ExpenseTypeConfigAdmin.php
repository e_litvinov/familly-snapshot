<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.8.18
 * Time: 13.30
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Traits\NestedListOptionsTrait;

class ExpenseTypeConfigAdmin extends AbstractAdmin
{
    use NestedListOptionsTrait;

    const DESCRIPTION = [
        'number' => 'Номер',
        'expenseType' => 'Типы реестра расходов',
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $expenseTypes = $container->get('doctrine')
            ->getRepository(ExpenseType::class)->findAll();

        $formMapper
            ->add('number', null, [
                'label' =>self::DESCRIPTION['number']
            ])
            ->add('expenseType', 'entity', array(
                'class' => ExpenseType::class,
                'choices' => $this->getOptionsByParent($expenseTypes, 'title'),
                'label' =>self::DESCRIPTION['expenseType'],
                'required' => false
            ), ['admin_code' => 'nko_order.admin.expense_type'])
        ;
    }
}
