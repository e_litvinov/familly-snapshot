<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.08.16
 * Time: 11:45
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PublicationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('publicationName',  TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Название публикации'
                ))
            ->add('yearPublication',  TextType::class,
                array(
                    'required' => false,
                    'label' => 'Год публикации',
                    'attr' => array('maxlength' => 4)
                ))
            ->add('link', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ссылка на публикацию (если есть)',
                    'attr' => array('help' => 'Если ссылки на публикацию нет, оставьте поле пустым. URL должен начинаться с http:// или https://')
                ));
    }
}