<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/6/17
 * Time: 3:08 PM
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class FinalDecisionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('competitions', 'sonata_type_model', array('multiple' => true, 'by_reference' => false, 'btn_add' =>false))
            ->add('name')
            ->add('typeDecision', CheckboxType::class,
                array(
                    'label' => 'По умолчанию отклонить',
                    'required' => false,
                    'value' => false,

                ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('competitions', null)
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array())))
        ;

    }

}