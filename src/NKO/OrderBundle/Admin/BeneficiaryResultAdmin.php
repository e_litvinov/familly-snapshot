<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.08.16
 * Time: 0:28
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BeneficiaryResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('result', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Результат',
                ))
            ->add('indicator', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Показатель',
                ))
            ->add('targetValue', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Целевое значение на 25.12.2016',
                ))
            ->add('approximateTargetValue', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ориентировочное целевое значение на 01.06.2017',
                ))
            ->add('methodMeasurement', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Способ измерения',
                ));
    }
}