<?php


namespace NKO\OrderBundle\Admin;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\PeriodReport;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as Finance2018Report;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as Finance2019Report;

class SendBaseReportAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
        '_sort_by' => 'createdAt'
    );

    protected $baseRouteName = 'admin_sonata_nko_order_send_base_report';

    protected $baseRoutePattern = 'send_base_report';

    private $entityManager;

    public function __construct($code, $class, $baseControllerName, EntityManager $em)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->entityManager = $em;
    }

    private function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->add('contacts');
        $collection->remove('edit');
        $collection->remove('delete');

        $collection->add('redirect_to_documents_list', $this->getRouterIdParameter().'/redirect_to_document_list');
        $collection->add('download_report', $this->getRouterIdParameter().'/download_report');
        $collection->add('results');
        $collection->add('download_summary_report', $this->getRouterIdParameter().'/download_summary_report');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $user = $this->getCurrentUser();
        if ($user->hasRole('ROLE_MAIN_EXPERT')) {
            $datagridMapper
                ->add('reportForm', null, [], null, [
                    'choices' => $user->getReportForms()
                ]);
        } elseif (!$user->hasRole('ROLE_FOUNDER')) {
            $datagridMapper
                ->add('reportForm');
        } else {
            $datagridMapper
                ->add('reportForm', null, array(), 'entity', [
                    'class' => ReportForm::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->innerJoin('u.reportHistories', 'p')
                            ->andWhere('u.reportClass != :reportClass')
                            ->setParameter('reportClass', 'NKO\OrderBundle\Entity\Report')
                            ->orderBy('u.year', 'DESC')
                            ;
                    }
                ]);
        }

        $datagridMapper
            ->add('period')
            ->add('nkoName')
            ->add('psrn')
            ->add('isAccepted')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nkoName')
            ->add('psrn')
            ->add('reportForm.title', null, array(
                'label' => 'Тип отчета'
            ))
            ->add('period.name', null, array(
                'label' => 'Период'
            ))
            ->add('underConsideration', null, array(
                'label' => 'На рассмотрении',
                'editable' => true
            ))
            ->add('isAccepted', null, array(
                'editable' => true,
                'template' => 'NKOOrderBundle:CRUD:list_boolean.html.twig'))
            ->add('_action', null, array(
                'actions' => array(
                    'download_summary_report' => array(
                        'template' => 'NKOOrderBundle:CRUD:list__action_download_summary_report.html.twig'
                    ),
                    'download_report' => array (
                        'template' =>  'NKOOrderBundle:CRUD:list__action_download_report.html.twig'
                    ),
                    'redirect_to_documents_list' => array(
                        'template' => 'NKOOrderBundle:CRUD:list__action_download_document_report.html.twig'
                    )
                )
            ));
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $user = $this->getCurrentUser();

        if ($user->hasRole('ROLE_MAIN_EXPERT')) {
            $query
                ->where($query->getRootAlias().'.isSend = 1')
                ->andWhere($query->getRootAlias().'.reportForm IN (:reportForms)')
                ->setParameters([
                    'reportForms' => $user->getReportForms()
                ])
            ;

            return $query;
        }

        if (!$user->hasRole('ROLE_FOUNDER')) {
            $query
                ->where($query->getRootAlias().'.isSend = 1');
        } else {
            $query
                ->where($query->getRootAlias().'.isSend = 1')
                ->innerJoin($query->getRootAlias() . '.reportForm', 'p')
                ->andWhere('p.reportClass != :reportClass')
                ->setParameter('reportClass', 'NKO\OrderBundle\Entity\Report')
            ;
        }
        $query->select(
            'partial ' .
            $query->getRootAlias() .
            '.{id, nkoName, psrn, reportForm, period, underConsideration, isAccepted}'
        );

        return $query;
    }

    public function preUpdate($object)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine');

        $report = $em->getRepository(BaseReport::class)
            ->find(unserialize($object->getData())->getId());

        $isAccepted = $report->getIsAccepted();
        if ($isAccepted !== $object->getIsAccepted() &&
            (
                $report instanceof FinanceReport ||
                $report instanceof Finance2018Report ||
                $report instanceof Finance2019Report
            )
        ) {
            $nextPeriods = $em->getRepository(PeriodReport::class)->findNextReportPeriods($object);
            if ($nextPeriods) {
                $this->declineIsAccepted($object, $nextPeriods, ReportHistory::class);
                $this->declineIsAccepted($object, $nextPeriods, BaseReport::class);
            }
        }

        $report->setIsAccepted($object->getIsAccepted());
        $report->setUnderConsideration($object->getUnderConsideration());


    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'NKOOrderBundle:CRUD:base_list_reports.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function declineIsAccepted($object, $nextPeriods, $class)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $findParam = ['period' => $nextPeriods, 'reportForm' => $object->getReportForm(), 'psrn' => $object->getPsrn()];
        if ($class === ReportHistory::class) {
            $findParam['isSend'] = true;
        }
        $reports = $em->getRepository($class)->findBy($findParam);
        foreach ($reports as $report) {
            $report->setIsAccepted(false);
        }
    }

    public function sendEmail($object)
    {
        if ($object->getIsAccepted()) {
            if ($object instanceof ReportHistory) {
                $object = unserialize($object->getData());
                $container = $this->getConfigurationPool()->getContainer();

                $body = $container->get('templating')->render(
                    'NKOOrderBundle:Emails:send_email.html.twig',
                    array('name' =>$object->getApplicationHistory()->getNkoName())
                );

                $message = \Swift_Message::newInstance()
                    ->setSubject('Курс на семью')
                    ->setFrom($container->getParameter('send_report_mail_from'))
                    ->setTo($object->getApplicationHistory()->getUsername())
                    ->setBody(
                        $body,
                        'text/html'
                    );

                $container->get('mailer')->send($message);
            }
        }
    }
}
