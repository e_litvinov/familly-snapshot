<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 14.08.16
 * Time: 21:35
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MeasureAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('action', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Мероприятие / действие',
                ))
            ->add('deadline', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сроки',
                ))
            ->add('actionLocation', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Место проведения',
                ));

    }
}
