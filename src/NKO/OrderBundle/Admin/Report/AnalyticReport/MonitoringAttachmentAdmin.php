<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class MonitoringAttachmentAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $report = $this->getParentFieldDescription()->getAdmin()->getSubject();

        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('NKOOrderBundle:Report\AnalyticReport\MonitoringElement', 'p')
            ->where('p.report = :report')
            ->setParameters(['report' => $report])
            ->getQuery();

        $formMapper
            ->add('monitoringElement', 'sonata_type_model', [
                'label' => 'Элементы добавленные в систему мониторинга и оценки',
                'query' => $query,
                'btn_add' => false,
            ])
            ->add('file', FilePreviewType::class, [
                'link_target' => '_blank'
            ]);
    }
}
