<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class TripleContextAdmin extends AbstractAdmin
{
    const LABEL_NAMES = [
        'monitoringResults' => [
            'first' => 'Задача / мероприятие / действие',
            'second' => 'Основные ожидаемые результаты',
            'third' => 'Комментарий о достижении результата'
        ],
    ];
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $first = key_exists($this->getParentFieldDescription()->getFieldName(), self::LABEL_NAMES) ?
            self::LABEL_NAMES[$this->getParentFieldDescription()->getFieldName()]['first'] : false;

        $second = key_exists($this->getParentFieldDescription()->getFieldName(), self::LABEL_NAMES) ?
            self::LABEL_NAMES[$this->getParentFieldDescription()->getFieldName()]['second'] : false;

        $third = key_exists($this->getParentFieldDescription()->getFieldName(), self::LABEL_NAMES) ?
            self::LABEL_NAMES[$this->getParentFieldDescription()->getFieldName()]['third'] : false;

        $formMapper
            ->add('first', FullscreenTextareaType::class, ['label' => $first, 'attr' => ['maxlength' => 2500]])
            ->add('second', FullscreenTextareaType::class, ['label' => $second, 'attr' => ['maxlength' => 2500]])
            ->add('third', FullscreenTextareaType::class, ['label' => $third, 'attr' => ['maxlength' => 1500]])
        ;
    }
}
