<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuadrupleContextAdmin extends AbstractAdmin
{
    const LABEL_NAMES = [
        'partnerActivities' => [
            'first' => 'Название организации',
            'second' => 'Запланированное участие в проекте',
            'third' => 'Фактическое участие в проекте (ваш комментарий)',
            'four' => 'Планируемые изменения в характере взаимодействия на следующий год (выход из проекта, изменение роли и др.)'
        ],
    ];
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('first', FullscreenTextareaType::class, ['label' => $this->getLabelText('first'), 'attr' => ['maxlength' => 1024]])
            ->add('second', FullscreenTextareaType::class, ['label' => $this->getLabelText('second'), 'attr' => ['maxlength' => 1024]])
            ->add('third', FullscreenTextareaType::class, ['label' => $this->getLabelText('third'), 'attr' => ['maxlength' => 1024]])
            ->add('fours', FullscreenTextareaType::class, ['label' => $this->getLabelText('four'), 'attr' => ['maxlength' => 1024]])
        ;
    }

    private function getLabelText($key)
    {
        $label = key_exists($this->getParentFieldDescription()->getFieldName(), self::LABEL_NAMES) ?
            self::LABEL_NAMES[$this->getParentFieldDescription()->getFieldName()][$key] : false;

        return $label;
    }
}
