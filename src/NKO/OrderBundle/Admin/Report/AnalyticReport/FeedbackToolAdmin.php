<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class FeedbackToolAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', FilePreviewType::class, array(
                'link_target' => '_blank'
            ));

        FileNameAdmin::configureFormFields($formMapper);
    }
}
