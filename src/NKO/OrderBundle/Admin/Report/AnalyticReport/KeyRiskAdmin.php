<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class KeyRiskAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('keyRisk', FullscreenTextareaType::class, ['label' => 'Ключевые риски', 'attr' => ['maxlength' => 1024]])
            ->add('isRealized', ChoiceType::class, [
                'label' => 'Реализовывались ли данные риски',
                'translation_domain' => 'SonataCoreBundle',
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]])
            ->add('act', FullscreenTextareaType::class, ['label' => 'Действия, нацеленные на снижение риска (если предпринимались)', 'attr' => ['maxlength' => 1024]])
            ->add('result', FullscreenTextareaType::class, ['label' => 'Результаты', 'attr' => ['maxlength' => 1024]])
            ->add('planRisk', FullscreenTextareaType::class, ['label' => 'Риски, предполагаемые в следующем году', 'attr' => ['maxlength' => 1024]])
            ->add('planAct', FullscreenTextareaType::class, ['label' => 'Действия, нацеленные на снижение риска в следующем году', 'attr' => ['maxlength' => 1024]])
        ;
    }
}
