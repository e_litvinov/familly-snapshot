<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/1/17
 * Time: 2:29 PM
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\KNS\Stage1;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\PhoneType;
use NKO\OrderBundle\Form\RadioButtonTree;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ReportAdmin extends BaseReportAdmin
{
    const DESCRIPTION = [
        'contactsLabel' => 'КОНТАКТНАЯ ИНФОРМАЦИЯ',
        'recipientLabel' => '<b>1. Получатель пожертвования</b> (полное наименование организации):',
        'contractLabel' => '<b>2. Договор</b> пожертвования №:',
        'projectNameLabel' => '<b>3. Название Проекта</b>',
        'trainingGrounds' => '<b>4-5.  Тематика</b> стажировки. <b>Организация</b> - стажировочная площадка:',
        'deadLineLabel' => '<b>6. Сроки реализации Проекта:</b>',
        'startDateLabel' => 'Дата начала',
        'finishDateLabel' => 'Дата окончания',
        'grantLabel' => '<b>7. Сумма пожертвования, в руб.</b>',
        'directorLabel' => '<b>8. Уполномоченное лицо</b>',
        'fullNameLabel' => 'ФИО',
        'positionLabel' => 'Должность',
        'personPhone' => 'Городской телефон:',
        'personMobilePhone' => 'Мобильный телефон:',
        'personEmail' => 'Адрес электронной почты:',
        'projectPurpose' => '<b>9. Цель проекта:</b>',
        'eventExpected' => '<b>10. Информация об основном обучающем мероприятии (стажировке)</b><br><br><b>10.1. Ожидания и реальность. </b><br><br>В какой мере оправдались Ваши ожидания в отношении стажировочной площадки? Если нет, то в чем именно были отличия ожиданий и реальности?',
        'eventDeadline' => '<b>10.2. Сроки проведения стажировки </b>(обучающего мероприятия)',
        'trainingFormat' => '<b>10.3. Формат стажировки</b> (обучающего мероприятия)',
        'comment' => 'Комментарий:',
        'trainingEvents' => '<b>10.4. Перечислите все обучающие мероприятия, которые были проведены в рамках проекта (включая саму стажировку и мероприятия для коллег и благополучателей)</b><br><i>Списки участников (с указанием ФИО и роли / должности в организации) прикрепляются во вкладке «Приложения».</i>',
        'lessons' => '<b>16. Извлеченные уроки</b>
                <br><br>
                <i>Трудности или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс реализации Проекта; «мелочи», которые упустили, но которые оказались важны. 
                Пути решения проблем/способы выхода из затруднительных ситуаций. Краткий анализ, что мешало, что помогало.
                </i>',
        'successStories' => '<b>17. Истории успеха</b>',
        'feedbackItems' => '<b>18. Прямая обратная связь от сотрудников организации (членов общественного объединения) и благополучателей </b><i>(наиболее показательная)</i>',
        'projectResultTab' => 'РУЗУЛЬТАТЫ ПРОЕКТА',
        'projectExperience' => '<b>11. Внедрение полученных знаний и опыта в деятельность организации</b>
                <br><br><i>11.1. Как именно в организации используются знания и опыт, полученные в ходе проекта? Перечислите конкретные действия (мероприятия), которые предприняты в период реализации проекта (до 15 декабря текущего года). </i>',
        'nextExperience' => '<i>11.2. Как в дальнейшем планируется использовать знания и опыт, полученные в рамках проекта? Перечислите конкретные действия, которые планируется предпринять в период до 01 июня следующего года.</i>',
        'directEmployeeResults' => '<b>12. Непосредственные результаты для сотрудников организации (членов общественного объединения)</b>',
        'qualitativeEmployeeResults' => '<b>13.  Качественные результаты для сотрудников организаций / членов общественного объединения</b>
            <br><br><i>Перечислите, какие ожидались качественные изменения у сотрудников организации (членов общественного объединения), которые произойдут в период реализации проекта (согласно заявке). Укажите планируемые и фактически достигнутые целевые значения. В случае различия целевых значений Плана/Факта, поясните в поле «Комментарий», почему так произошло. </i>',
        'otherQualitativeEmployeeResults' => '<i>В случае, если достигнуты также <b>иные, не указанные в заявке результаты,</b> также их укажите.</i>',
        'socialResults' => '<b>14. Социальные результаты проекта</b>',
        'otherSocialResults' => '<i>В случае, если достигнуты также <b>иные, не указанные в заявке результаты,</b> также их укажите.</i>',
        'trainingEventAttachments' => '<i>Закачайте приложения к проекту, в том числе списки участников обучающих мероприятий проекта (если необходимо – в зашифрованном виде).</i>
            <br><br><b>3.1. Обучающие мероприятия</b>',
        'etcAttachments' => '<b>3.2. Другие приложения</b>',
        'attachmentsTab' => 'ПРИЛОЖЕНИЯ',
        'beneficiaryLabel' => '<b>15. РЕЗУЛЬТАТЫ ДЛЯ БЛАГОПОЛУЧАТЕЛЕЙ</b>',
        'beneficiaryResults' => '<b>15.1 Качественные изменения у благополучателей</b><br><br>
            <i>Перечислите, какие ожидались качественные изменения у благополучателей организации, которые произойдут в период реализации проекта (согласно заявке), а также фактически достигнутые целевые значения (в случае различия целевых значений Плана/Факта, в поле «Комментарий» укажите, с чем это связано). Укажите также, каких целевых значений вы планируете достигнуть к 01 июня следующего года.</i>'
    ];

    const HELPS = [
        'phone' => '+7 (код - 3 цифры) (номер – 7 цифр)'
    ];

    const PUBLICATIONS_INDICATOR = [

    ];

    const MIN_COUNT = 1;

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $report = $this->getSubject();
        if ($report) {
            $grant = $em->getRepository('NKOOrderBundle:GrantConfig')
                ->findOneBy([
                    'reportForm' => $report->getReportForm(),
                    'applicationHistory' => $report->getApplicationHistory()
                ]);
        }

        $formMapper
            ->with(self::DESCRIPTION['contactsLabel'])
            ->add('author.nko_name', null, array(
                'label' => self::DESCRIPTION['recipientLabel'],
                'attr' => array(
                    'readonly' => true
                )
            ), array(
                'admin_code' => 'sonata.admin.nko.nko_user'
            ))
            ->add('contract', TextType::class, array(
                'label' => self::DESCRIPTION['contractLabel'],
                'data' => $grant->getContract(),
                'mapped' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('projectName', TextareaType::class, array(
                'label' => self::DESCRIPTION['projectNameLabel'],
                'required' => false,
                'attr' => ['maxlength' => 450]
            ))
            ->add('trainingGrounds', RadioButtonTree::class,
                array(
                    'class' => 'NKOOrderBundle:KNS2017\TrainingGround',
                    'label' => self::DESCRIPTION['trainingGrounds'],
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.competition = :name')
                            ->setParameters(array('name' => 'kns2017-2'));
                    },
                    'required' => false,
                    'multiple' => true,
                ))
            ->add('deadLineLabel', CustomTextType::class,
                array(
                    'help' => self::DESCRIPTION['deadLineLabel'],
                    'required' => false
                ))
            ->add('startDateProject', 'sonata_type_datetime_picker', array(
                'label' => self::DESCRIPTION['startDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,
            ))
            ->add('finishDateProject', DateTimePickerType::class, array(
                'label' => self::DESCRIPTION['finishDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false
            ))
            ->add('sumGrant', TextType::class, array(
                'label' => self::DESCRIPTION['grantLabel'],
                'data' => $grant->getSumGrant(),
                'mapped' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('directorLabel', CustomTextType::class,
                array(
                    'help' => self::DESCRIPTION['directorLabel'],
                ))
            ->add('directorName', null, array(
                'label' => self::DESCRIPTION['fullNameLabel'],
                'required' => false
            ))
            ->add('directorPosition', null, array(
                'label' => self::DESCRIPTION['positionLabel'],
                'required' => false
            ))
            ->add('directorPhone', PhoneType::class, array(
                'label' => self::DESCRIPTION['personPhone'],
                'required' => false,
                'help' => self::HELPS['phone']
            ))
            ->add('directorMobilePhone', PhoneType::class, array(
                'label' => self::DESCRIPTION['personMobilePhone'],
                'required' => false,
                'help' => self::HELPS['phone']
            ))
            ->add('directorEmail', null, array(
                'label' => self::DESCRIPTION['personEmail'],
                'required' => false
            ))
            ->add('projectPurpose', TextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['projectPurpose'],
                'attr' => array(
                )
            ])
            ->add('expectationsAndReality', TextareaType::class, [
                'label' => self::DESCRIPTION['eventExpected'],
                'required' => false,
                'attr' => ['maxlength' => 1500]])
            ->add('deadLineEventLabel', CustomTextType::class,
                array(
                    'help' => self::DESCRIPTION['eventDeadline'],
                    'required' => false
                ))
            ->add('trainingStart', 'sonata_type_datetime_picker', array(
                'label' => self::DESCRIPTION['startDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,))
            ->add('trainingEnd', 'sonata_type_datetime_picker', array(
                'label' => self::DESCRIPTION['finishDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,))
            ->add('traineeshipFormats', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => self::DESCRIPTION['trainingFormat'],
                    "property" => "formatName",
                    'expanded' => true,
                    'multiple' => true
                ))
            ->add('comment', TextType::class, [
                'label' => self::DESCRIPTION['comment'],
                'required' => false
            ])
            ->add('reportTrainingEvents', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' =>  self::DESCRIPTION['trainingEvents'],
                    'btn_add' => 'Добивить',
                    'by_reference' => false,
                    'type_options' => array('delete' => true, 'btn_add' => true)

                ),
                array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                )
            )
            ->end()
            ->with(self::DESCRIPTION['projectResultTab'])
            ->add('projectExperience', TextareaType::class, [
                'label' => self::DESCRIPTION['projectExperience'],
                'required' => false
            ])
            ->add('nextExperience', TextareaType::class, [
                'label' => self::DESCRIPTION['nextExperience'],
                'required' => false
            ])
            ->add('directEmployeeResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['directEmployeeResults'],
                'btn_add' => false,
                'type_options' => array(
                    'delete' => false
                ),
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('qualitativeEmployeeResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['qualitativeEmployeeResults'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result_with_indicator',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('otherQualitativeEmployeeResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['otherQualitativeEmployeeResults'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result_with_indicator',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('socialReportResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['socialResults'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result_without_plan',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('otherSocialResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['otherSocialResults'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result_with_indicator',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('beneficiaryLabel', CustomTextType::class,
                array(
                    'help' => self::DESCRIPTION['beneficiaryLabel'],
                    'required' => false
                ))
            ->add('beneficiaryResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['beneficiaryResults'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result_with_target_group',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('otherBeneficiaryResults', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['otherQualitativeEmployeeResults'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ],
                [
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.report_result_with_custom_target_group',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('lessons', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['lessons'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ),
                array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('successStories', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['successStories'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ),
                array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('feedbackItems', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['feedbackItems'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ),
                array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))

            ->end()
            ->with(self::DESCRIPTION['attachmentsTab'])
            ->add('trainingEventAttachments', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['trainingEventAttachments'],
                'btn_add' => 'Добавить',
                'by_reference' => false
            ),
                array(
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.attachment',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('etcAttachments', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['etcAttachments'],
                'btn_add' => 'Добавить',
                'by_reference' => false
            ),
                array(
                    'admin_code' => 'nko_order.admin.analytic_report_kns_1.attachment',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->end();

    }

}