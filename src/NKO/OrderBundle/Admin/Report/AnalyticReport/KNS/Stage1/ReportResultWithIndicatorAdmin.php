<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/5/17
 * Time: 11:19 AM
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\KNS\Stage1;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ReportResultWithIndicatorAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();

        $formMapper
            ->add('result', TextareaType::class, ['label' => 'Результат', 'attr' => ['style'=>"min-width: 150px;"]])
            ->add('indicator', TextareaType::class, ['label' => 'Показатель', 'attr' => ['style'=>"min-width: 150px;"]])
            ->add('plan', NumberType::class, [
                'label' => '<b>Целевые значения. План</b><i> (на дату окончания проекта)</i>',
                'attr' => ['placeholder' => 0,]
            ])
            ->add('fact', NumberType::class, [
                'label' => '<b>Целевые значения. Факт</b><i> (на дату окончания проекта)</i>',
                'attr' => ['placeholder' => 0,]
            ])
            ->add('expected', NumberType::class, ['label' => '<b>Целевые значения. Ожидается</b><i> на 01.06 следующего года</i>',
                'attr' => ['placeholder' => 0,]
            ])
            ->add('measurementMethod', null, [
                'label' => '<b>Способ измерения</b>',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ReportTypes::ANALYTIC_REPORT_KNS_1 . '%');
                }
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => '<b>Комментарий</b>',
                'attr' => ['style'=>"min-width: 150px;"]
            ])
        ;
    }

}