<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\KNS\Stage1;

use Doctrine\ORM\EntityRepository;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use ITM\FilePreviewBundle\ITMFilePreviewBundle;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report;
use NKO\UserBundle\Entity\NKOUser;
use NKO\UserBundle\Traits\CurrentUserTrait;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AttachmentAdmin extends AbstractAdmin
{
    use CurrentUserTrait;

    const DESCRIPTION = [
        'trainingEventAttachments' => [
            'type' => '<b>Обучающие мероприятия</b>',
            'contentLabel' => '<b>Название мероприятия</b>',
            'fileLabel' => '<b>Приложение в формате pdf</b>',
        ],
        'etcAttachments' => [
            'type' => '<b>Другие приложения</b>',
            'contentLabel' => '<b>Название приложения</b>',
            'fileLabel' => '<b>Приложение в формате pdf</b>',
        ]
    ];

    const LIST_DESCRIPTION = [
        'contentLabel' => 'Название',
    ];

    const DATAGRID_DESCRIPTION = [
        'organization' => 'Название организации',
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $fieldName = $this->getParentFieldDescription()->getFieldName();
        if (array_key_exists($fieldName, self::DESCRIPTION)) {
            $formMapper
                ->add('content', TextType::class, [
                    'label' => self::DESCRIPTION[$fieldName]['contentLabel']
                ])
                ->add('file', FilePreviewType::class, [
                    'label' => self::DESCRIPTION[$fieldName]['fileLabel'],
                    'link_target' => '_blank'
                ])
            ;
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->parameters = [
            'listmapper' => [
                'type' => 'PDF',
                'fieldName' => 'file'
            ]
        ];

        $listMapper
            ->add('report', null, [
                'label' => 'Организация'
            ])
            ->add('content', null, ['label' => self::LIST_DESCRIPTION['contentLabel']])
            ->add('file', 'string', array('template' => "NKOOrderBundle:Admin:preview_file_document.html.twig"))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('report', 'doctrine_orm_callback', array(
                'label' => self::DATAGRID_DESCRIPTION['organization'],
                'operator_type' => 'hidden',
                'advanced_filter' => false,
                'callback' => [$this, 'getByReportFilter'],
                'show_filter' => false,
            ), 'entity', [
                'class' => Report::class,
                'query_builder' => function (EntityRepository $er) {
                    $user = $this->getCurrentUser();
                    if ($user->hasRole('ROLE_NKO')) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.psrn = :report')
                            ->setParameters(array('report' => $this->getCurrentUser()->getPsrn()));
                    }
                }
            ]);
    }

    public function getByReportFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }

        $queryBuilder
            ->where($alias.'.reportEtc = :reportId')
            ->orWhere($alias.'.reportTrainingEvents = :reportId')
            ->setParameter('reportId', $value['value'])
        ;

        return true;
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept('list');
    }
}
