<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/4/17
 * Time: 12:03 PM
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\KNS\Stage1;


use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class TrainingEventAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('trainingEvent', FullscreenTextareaType::class, [
                'label' => 'Обучающее мероприятие',
            ])
            ->add('start', 'sonata_type_datetime_picker', array(
                'label' => 'Дата начала',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,))
            ->add('end',  'sonata_type_datetime_picker', array(
                'label' => 'Дата завершения',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,))
            ->add('place', FullscreenTextareaType::class, [
                'label' => 'Место проведения',
            ])
            ->add('participantCount', NumberType::class, [
                'label' => 'Число сотрудников, принявших участие в мероприятии',
                'attr' => ['placeholder' => 0,]
            ])
        ;

    }

}