<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/13/17
 * Time: 3:20 PM
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ExpectedAnalyticResultAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $grantees = $em->getRepository(Grantee::class)->findGranteesByTypeData('brief_sf-2016');

        $formMapper
            ->add('service', TextareaType::class, ['attr' => ['maxlength' => 255]])
            ->add('practiceFormat')
            ->add('targetGroup')
            ->add('serviceFactValue', null, [ 'attr' =>['placeholder' => '0']])
            ->add('servicePlanValue', null, [ 'attr' =>['placeholder' => '0']])
            ->add('beneficiaryFactValue', null, [ 'attr' =>['placeholder' => '0']])
            ->add('beneficiaryPlanValue', null, [ 'attr' =>['placeholder' => '0']])
            ->add('startDateFact', 'sonata_type_datetime_picker', array('label' => 'С',
                'format' => 'dd.MM.yyyy', 'dp_pick_time' => false
            ))
            ->add('finishDateFact', 'sonata_type_datetime_picker', array(
                'label' => 'По',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false
            ))
            ->add('startDatePlan', 'sonata_type_datetime_picker', array(
                'label' => 'С',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false
            ))
            ->add('finishDatePlan', 'sonata_type_datetime_picker', array(
                'label' => 'По',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false
            ))
            ->add('measurementMethod', null, [
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ApplicationTypes::CONTINUATION_APPLICATION_2018 . '%');
                }
            ])
            ->add('isFeedback', ChoiceType::class, [
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]])
            ->add('grantee', 'sonata_type_model', [
                'btn_add' => false,
                'choices' => $grantees,
            ])
            ->add('comment', FullscreenTextareaType::class, ['attr' => ['maxlength' => 450]])
            ->add('idDirectResult', HiddenType::class, [
                'label' => false,
                'mapped' => false,
                'data' => $this->getSubject() ? $this->getSubject()->getId() : null
            ])
        ;
    }
}
