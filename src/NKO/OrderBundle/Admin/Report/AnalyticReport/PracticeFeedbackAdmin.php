<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Effectiveness;

class PracticeFeedbackAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'practice' => 'Услуги, мероприятия и пр., по которым собиралась обратная связь',
        'comment' => 'Комментарий (выводы, цитаты)',
        'administrativeSolution' => 'Принимались ли управленческие решения по результатам обратной связи',
    ];

    const REPORT_FIELDS = [
        'practiceFeedbackAttachments' => [
            'admin_code' => 'nko_order.admin.analytic_report.practice_analytic_result',
            AnalyticReport2018::class => 'reportExpectedResult',
            AnalyticReport2019::class => 'reportExpectedResult',
            AnalyticReport::class => 'reportPracticeResult'
        ],
        'practiceSocialFeedbackAttachments' => [
            'admin_code' => 'nko_order.admin.report.analytic_report.report2019.effectiveness',
            AnalyticReport2019::class => 'implementationReport',
        ],
        'practiceSpreadFeedbackAttachments' => [
            'admin_code' => 'nko_order.admin.analytic_report.expected_analytic_result',
            AnalyticReport2018::class => 'reportPracticeResult',
            AnalyticReport2019::class => 'reportPracticeResult',
            AnalyticReport::class =>  'reportExpectedResult'
        ],
        'practiceSocialSpreadFeedbackAttachments' => [
            'admin_code' => 'nko_order.admin.report.analytic_report.report2019.effectiveness',
            AnalyticReport2019::class => 'introductionReport',
        ]
    ];

    const REPORT_FIELDS_CLASS = [
        'practiceFeedbackAttachments' => [
            AnalyticReport2019::class => DirectResult::class,
        ],
        'practiceSocialFeedbackAttachments' => [
            AnalyticReport2019::class => Effectiveness::class,
        ],
        'practiceSpreadFeedbackAttachments' => [
            AnalyticReport2019::class => DirectResult::class,
        ],
        'practiceSocialSpreadFeedbackAttachments' => [
            AnalyticReport2019::class => Effectiveness::class,
        ]
    ];

    const PROPERTY_PATH = [
        'practiceFeedbackAttachments' => [
            AnalyticReport2019::class => 'service',
            AnalyticReport2018::class => 'relatedService',
            AnalyticReport::class => 'relatedService',
        ],
        'practiceSocialFeedbackAttachments' => [
            AnalyticReport2019::class => 'result',
        ],
        'practiceSpreadFeedbackAttachments' => [
            AnalyticReport2019::class => 'service',
            AnalyticReport2018::class => 'relatedService',
            AnalyticReport::class => 'relatedService',
        ],
        'practiceSocialSpreadFeedbackAttachments' => [
            AnalyticReport2019::class => 'result',
        ]
    ];

    const REPORT_2018_DESCRIPTION = [
        'administrativeSolution' => 'Какие принимались управленческие решения по результатам обратной связи?'
    ];

    const DESCRIPTION_FEEDBACK = [
        'practice' => 'Услуги, мероприятия и пр., по которым собиралась обратная связь',
        'administrativeSolution' => 'Какие принимались управленческие решения по результатам обратной связи?',
    ];

    const DESCRIPTION_FEEDBACK_SOCIAL = [
        'practice' => 'Социальные результаты, по которым собиралась обратная связь',
        'administrativeSolution' => 'Какие принимались управленческие решения по результатам обратной связи?',
    ];

    const CUSTOM_DESCRIPTION = [
        AnalyticReport2019::class => [
            'practiceFeedbackAttachments' => self::DESCRIPTION_FEEDBACK,
            'practiceSocialFeedbackAttachments' => self::DESCRIPTION_FEEDBACK_SOCIAL,
            'practiceSpreadFeedbackAttachments' => self::DESCRIPTION_FEEDBACK,
            'practiceSocialSpreadFeedbackAttachments' => self::DESCRIPTION_FEEDBACK_SOCIAL,
        ],
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $query = null;
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();

        if (array_key_exists($this->getParentFieldDescription()->getFieldName(), self::REPORT_FIELDS)) {
            $report = $this->getParentFieldDescription()->getAdmin()->getSubject();
            $fieldName = $this->getParentFieldDescription()->getFieldName();
            $reportClass = get_class($report);

            $linkToReportFromResult = array_key_exists($reportClass, self::REPORT_FIELDS[$fieldName]) ?
                self::REPORT_FIELDS[$fieldName][$reportClass] : null;
            $linkToReportClass = array_key_exists($reportClass, self::REPORT_FIELDS_CLASS[$fieldName]) ?
                self::REPORT_FIELDS_CLASS[$fieldName][$reportClass] : DirectResult::class;
            $propertyPath = array_key_exists($reportClass, self::PROPERTY_PATH[$fieldName]) ?
                self::PROPERTY_PATH[$fieldName][$reportClass] : 'relatedService';
            $practiceFieldName = ($linkToReportClass == DirectResult::class) ? 'practice' : 'practiceEffectiveness';

            $query = $em->createQueryBuilder()
                ->select('p')
                ->from($linkToReportClass, 'p')
                ->where('p.' . $linkToReportFromResult . ' = :report')
                ->andWhere('p.isFeedback = 1')
                ->setParameters(['report' => $report])
                ->getQuery();

            $description = self::DESCRIPTION;
            if ($report instanceof AnalyticReport2018 && $fieldName == 'practiceSpreadFeedbackAttachments') {
                $description = array_merge(self::DESCRIPTION, self::REPORT_2018_DESCRIPTION);
            }

            if (key_exists($reportClass, self::CUSTOM_DESCRIPTION)) {
                $description = array_merge(self::DESCRIPTION, self::CUSTOM_DESCRIPTION[$reportClass][$fieldName]);
            }

            $formMapper
                ->add($practiceFieldName, 'sonata_type_model', [
                    'query' => $query,
                    'attr' => ['style' => 'max-width: 300px'],
                    'btn_add' => false,
                    'required' => false,
                    'property' => $propertyPath,
                    'label' => $description['practice']
                ], [
                    'admin_code' => self::REPORT_FIELDS[$fieldName]['admin_code']
                ])
                ->add('comment', FullscreenTextareaType::class, [
                    'attr' => [
                        'maxlength' => 1500
                    ],
                    'label' => $description['comment']
                ])
                ->add('administrativeSolution', FullscreenTextareaType::class, [
                    'attr' => [
                        'maxlength' => 1500,
                    ],
                    'label' => isset($description['administrativeSolution']) ? $description['administrativeSolution'] : self::DESCRIPTION['administrativeSolution'],
                ])
                ->add('tools', 'sonata_type_collection', array(
                        'required' => false,
                        'btn_add' => 'Добавить',
                        'by_reference' => false,
                    ), array (
                        'edit' => 'inline',
                        'delete' => 'inline',
                        'inline' => 'table',
                    ))
            ;
        }
    }
}
