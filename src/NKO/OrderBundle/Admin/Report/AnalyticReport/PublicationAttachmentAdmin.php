<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PublicationAttachmentAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'author' => 'ФИО автора',
        'name' => 'Название публикации',
        'date' => 'Дата публикации',
        'mediaName' => 'Название СМИ',
        'mediaLink' => 'Ссылка на публикацию (для онлайн-СМИ)',
        'file' => 'Скан публикации в формате pdf (для печатных СМИ)',
    ];

    const ATTR = [
        'style' => 'width:200px',
        'rows' => 3
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('author', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['author']
            ])
            ->add('name', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['name']
            ])
            ->add('date', DateTimePickerType::class, array(
                    'format' => 'dd.MM.yyyy',
                    'dp_pick_time' => false,
                    'required' => false,
                    'label' => self::DESCRIPTION['date']
                )
            )
            ->add('mediaName', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['mediaName']
            ])
            ->add('mediaLink', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['mediaLink']
            ])
            ->add('file', FilePreviewType::class, [
                'link_target' => '_blank',
                'label' => self::DESCRIPTION['file']
            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }
}
