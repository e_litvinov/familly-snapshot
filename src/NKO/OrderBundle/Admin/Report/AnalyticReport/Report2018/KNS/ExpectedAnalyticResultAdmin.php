<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 7.7.18
 * Time: 14.59
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ExpectedAnalyticResultAdmin extends AbstractAdmin
{
    const ROW_SIZE = 7;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('targetGroup', TextareaType::class, [
                'attr' => [
                    'rows' => self::ROW_SIZE
                ],
            ])
            //        Используется вместо "Социальный результат (для благополучателей)" чтобы не дополнять entity лишними полями
            ->add('service', TextareaType::class, [
                'attr' => [
                    'maxlength' => 255,
                    'rows' => self::ROW_SIZE
                ],
            ])
            ->add('indicator', TextareaType::class, [
                    'required' => false,
                    'attr' => [
                        'rows' => self::ROW_SIZE
                    ],
            ])
            ->add('servicePlanValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
            ])
            ->add('serviceFactValue', TextType::class, [
                'attr' =>['placeholder' => '0']
            ])
            ->add('measurementMethod', null, [
                'placeholder' => 'Не выбрано',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ReportTypes::ANALYTIC_REPORT_2018 . '%');
                }
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'attr' => ['maxlength' => 450]
            ])
        ;
    }
}
