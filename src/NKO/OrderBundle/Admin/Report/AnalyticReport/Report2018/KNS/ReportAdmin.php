<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 2.8.18
 * Time: 11.33
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ProjectDescriptionAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ResourceAdmin;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\EventListener\DocumentListener;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Form\RadioButtonTree;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\EventListener\DeleteRowListener;

class ReportAdmin extends BaseReportAdmin
{
    const ATTACHMENT_DESCRIPTION = '<p id="warning">Перед началом работы на вкладке “Приложения” сохраните, пожалуйста, отчет.
    Загружайте документы по одному. После добавления каждого документа – сохранитесь, 
    дождитесь сообщения о загрузке документа, откройте его и проверьте.';

    const DOCUMENTS_FIELD_NAME = [
        'practiceFeedbackAttachments' => 'tools',
        'practiceSuccessStoryAttachments',
    ];

    const TAB_DESCRIPTION = [
        'projectCard' => '1. КАРТОЧКА ПРОЕКТА',
        'projectRealization' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
        'practiceSpread' => 'РАСПРОСТРАНЕНИЕ ПРАКТИКИ',
        'resource' => 'РЕСУРСНОЕ ОБЕСПЕЧЕНИЕ ПРОЕКТА',
        'attachment' => 'ПРИЛОЖЕНИЯ',
    ];

    const PROJECT_CARD = [
        'trainingGroundsHelp' => "Поле заполняется автоматически после заполнения поля 2.2.",
    ];

    const REALIZATION_PRACTICE = [
        'trainingGrounds' => "2.1.-2.2. <b> Приоритетное направление Конкурса и стажировочная площадка</b>",
        'projectPurpose' => "<b>2.3. Цель проекта</b>",
        'territories' => "<b>2.4. Территория реализации проекта</b>",
        'traineeshipPractice' => "<b>2.5. Практика стажировочной площадки, которая внедрялась в рамках проекта</b>",
        'practiceIntroduction' => "<b>2.6. Внедрение Практики стажировочной площадки</b><br><i> 
Что именно, какие элементы Практики стажировочной площадки уже внедрены в деятельность вашей организации?
 Какие вы видите особенности внедрения Практики, какие выводы вы для себя сделали? 
 Что «работает» хорошо, а что «не сработало» (от чего отказались, не смогли или не стали внедрять и пр.), с чем это связано? 
 Какие, возможно, изменения вы внесли в Практику 
 (адаптировали под новые целевые группы, добавили / исключили ряд элементов и пр.), с чем они связаны?</i>",
        'traineeshipSupport' => "<b>2.7. Поддержка со стороны стажировочной площадки</b> <br>
<i>Как складывались ваши отношения со стажировочной площадкой, какая поддержка была вам оказана в процессе реализации Практики (в ходе проекта)?</i>",
        'practiceAnalyticResults' => '<b>2.8 Непосредственные результаты </b>',
        'expectedAnalyticResults' => '<b>2.9 Социальные результаты </b>',
        'successStories' => "<b>2.10. Истории, факторы успеха</b><br>
<i>Кратко опишите, какие были успехи в ходе реализации проекта. Какие факторы способствовали успешной реализации проекта?

В случае наличия подробных историй успеха (описание успешного кейса/активности; 
прямая речь благополучателей или сотрудников, работающих с благополучателями), прикрепите их как приложение в П.5.2.</i>",
        'realizationProblem' => "<b>2.11. Трудности в реализации проекта</b> 
<ul><i><li>Трудности или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на 
    процесс реализации проекта что из запланированного не удалось выполнить и почему;</li>
    <li>Пути решения проблем / способы выхода из затруднительных ситуаций или пояснения, почему их нельзя решить силами вашего проекта.</li></i></ul>",
        'lessons' => "<b>2.12. Извлечённые уроки</b><br><i> Какие уроки вы для себя извлекли в ходе реализации проекта (в т.ч. благодаря сбору обратной связи)?</i>",
    ];

    const SPREAD_PRACTICE = [
        'publications' => "<b>3.1 Публикации</b>",
        'realizationExperience' => "<b> 3.2. Распространение Практики, опыта реализации проекта</b><br><i>Как распространялся опыт внедрения Практики, реализации проекта в целом?
 Перечислите соответствующие мероприятия, в рамках которых участвовала ваша организация, формат вашего участия (выступление, супервизии, консультации и пр.),
  укажите количественные и качественные характеристики аудитории мероприятий.</i>",
        'projectExperience' => "<b>3.3. Основные выводы о распространении Практики, опыта реализации проекта (в т.ч. истории успеха, трудности)</b>",
    ];

    const ORGANIZATION_INFO_DESCRIPTION = [
        'email' => '<b>Адрес электронной почты</b> (для оперативного контакта с организацией)',
        'phoneLabel' => '<b>Телефон</b> для оперативного контакта с организацией',
        'phoneLabelHelp' => '<label class="control-label">Телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'internationalCode' => '<label class="code">+7</label>',
        'competition' => 'Конкурс',
    ];

    const RESOURCE = [
        'partnerActivities' => '<b>4.1. Участие партнеров и доноров проекта</b>',
        'humanResources' => '<b>4.2. Кадровые ресурсы проекта</b>',
    ];

    const ATTACHMENT = [
        'practiceFeedbackAttachments' => '<b>5.1. Обратная связь</b>',
        'practiceSuccessStoryAttachments' => '<b>5.2. Истории успеха</b>',
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => array("KNS-AnalyticReport-2018")
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $report = $this->getSubject();
        $reportFormYear = null;
        $updatedAt = null;

        if ($report) {
            $grant = $em->getRepository(GrantConfig::class)->getGrantByReport($report);
            $reportFormYear = $report->getReportForm()->getYear()->format('Y');
        }

        $parameters = AdminConfigurator::PROJECT_DESCRIPTION[get_class($this->getSubject())];
        $parameters['value'] = [
            'contract' => isset($grant) ? $grant->getContract() : null,
            'sumGrant' => isset($grant) ? $grant->getSumGrant() : null,
            'updatedAt' => $updatedAt,
            'year' => $reportFormYear,
            'projectName' => $report->getProjectName(),
        ];

        $formMapper
            ->with(self::TAB_DESCRIPTION['projectCard'])
            ->add('reportForm.competition.name', TextType::class, [
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['competition'],
                'attr' => [
                    'readonly' => true,
                ]
            ]);
        ProjectDescriptionAdmin::configureFormFields($formMapper, $parameters, null, self::PROJECT_CARD);
        $formMapper
            ->add('phoneLabel', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['phoneLabel'],
            ])
            ->add('phoneLabelHelp', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['phoneLabelHelp'],
            ])
            ->add('internationalCode', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['internationalCode'],
            ])
            ->add('phoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ]
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['email'],
                'required' => false,
            ])
            ->end();

        $formMapper->with(self::TAB_DESCRIPTION['projectRealization']);
        $formMapper
            ->add('trainingGrounds', RadioButtonTree::class, [
                'class' => TrainingGround::class,
                'label' => self::REALIZATION_PRACTICE['trainingGrounds'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.competition = :name')
                        ->setParameters(['name' => ApplicationTypes::KNS_APPLICATION_2018]);
                },
                'required' => false,
                'multiple' => true,
            ])
            ->add('projectPurpose', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['projectPurpose'],
                'required' => false,
            ])
            ->add('territories', 'sonata_type_collection', [
                'label' => self::REALIZATION_PRACTICE['territories'],
                'required' => false,

            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('traineeshipPractice', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['traineeshipPractice'],
                'required' => false,
            ])
            ->add('practiceIntroduction', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['practiceIntroduction'],
                'required' => false,
            ])
            ->add('traineeshipSupport', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['traineeshipSupport'],
                'required' => false,
            ])
            ->add('practiceAnalyticResults', 'sonata_type_collection', [
                'label' => self::REALIZATION_PRACTICE['practiceAnalyticResults'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.practice_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('expectedAnalyticResults', 'sonata_type_collection', [
                'label' => self::REALIZATION_PRACTICE['expectedAnalyticResults'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.expected_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('successStories', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['successStories'],
                'required' => false,
            ])
            ->add('realizationProblem', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['realizationProblem'],
                'required' => false,
            ])
            ->add('lessons', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['lessons'],
                'required' => false,
            ]);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['practiceSpread']);
        $formMapper
            ->add('publications', 'sonata_type_collection', [
                'label' => self::SPREAD_PRACTICE['publications'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('realizationExperience', FullscreenTextareaType::class, [
                'label' => self::SPREAD_PRACTICE['realizationExperience'],
                'required' => false,
            ])
            ->add('projectExperience', FullscreenTextareaType::class, [
                'label' => self::SPREAD_PRACTICE['projectExperience'],
                'required' => false,
            ]);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['resource']);
        ResourceAdmin::configureFormFields(
            $formMapper,
            AdminConfigurator::RESOURCE_ANALYTIC_REPORT[get_class($this->getSubject())],
            null,
            self::RESOURCE
        );
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['attachment']);
        $formMapper->add('warning', CustomTextType::class, [
            'help' => self::ATTACHMENT_DESCRIPTION,
        ]);
        $formMapper
            ->add('practiceFeedbackAttachments', 'sonata_type_collection', [
                'label' => self::ATTACHMENT['practiceFeedbackAttachments'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.feedback',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('practiceSuccessStoryAttachments', 'sonata_type_collection', [
                'label' => self::ATTACHMENT['practiceSuccessStoryAttachments'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.feedback_success_story',
                'edit' => 'inline',
                'inline' => 'table',
            ]);
        $formMapper->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new DocumentListener(self::DOCUMENTS_FIELD_NAME));
    }
}
