<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 8.8.18
 * Time: 17.38
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class HumanResourceAdmin extends AbstractAdmin
{
    const LABEL_DESCRIPTION = [
        'executorsCount' => 'Количество исполнителей проекта на начало года (любой тип занятости)',
        'substitutionsCount' => 'Количество замен в составе исполнителей проекта в течение года',
        'ratesCount' => 'Количество введенных дополнительно ставок в проекте',
        'individualJobSpecialistCount' => 'Количество специалистов, дополнительно привлекавшихся в проект для отдельных работ',
        'comment' => 'Комментарий к кадровым изменениям за прошедший год',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('executorsCount', TextType::class, [
                'label' => self::LABEL_DESCRIPTION['executorsCount'],
                'attr' => [
                    'placeholder' => '0',
                    ]
            ])
            ->add('substitutionsCount', TextType::class, [
                'label' => self::LABEL_DESCRIPTION['substitutionsCount'],
                'attr' => [
                    'placeholder' => '0',
                ]
            ])
            ->add('ratesCount', TextType::class, [
                'label' => self::LABEL_DESCRIPTION['ratesCount'],
                'attr' => [
                    'placeholder' => '0',
                ]
            ])
            ->add('individualJobSpecialistCount', TextType::class, [
                'label' => self::LABEL_DESCRIPTION['individualJobSpecialistCount'],
                'attr' => [
                    'placeholder' => '0',
                ]
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => self::LABEL_DESCRIPTION['comment'],
                'attr' => [
                    'maxlength' => 1024,
                    'rows' => 3
                ]
            ])
        ;
    }
}
