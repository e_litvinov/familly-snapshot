<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 9.8.18
 * Time: 19.04
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class FeedbackSuccessStoryAttachmentsAdmin extends AbstractAdmin
{
    const FIELDS = [
        'title' => 'Название истории успеха',
        'file' => 'Приложение (в формате pdf, doc)',
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', FullscreenTextareaType::class, [
                'label' => self::FIELDS['title'],
            ])
            ->add('file', FilePreviewType::class, [
                'label' => self::FIELDS['file'],
                'link_target' => '_blank'
            ])
        ;

        FileNameAdmin::configureFormFields($formMapper);
    }
}
