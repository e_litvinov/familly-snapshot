<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 8.8.18
 * Time: 17.02
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuadrupleContextAdmin extends AbstractAdmin
{
    const LABEL_DESCRIPTION = [
        'first' => 'Партнер/донор',
        'second' => 'Запланированное участие в проекте',
        'third' => 'Фактическое участие в проекте',
    ];

    const ROWS = 3;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('first', TextareaType::class, [
                'label' => self::LABEL_DESCRIPTION['first'],
                'attr' => [
                    'maxlength' => 1024,
                    'rows' => self::ROWS,
                ],
                ])
            ->add('second', TextareaType::class, [
                'label' => self::LABEL_DESCRIPTION['second'],
                'attr' => [
                    'maxlength' => 1024,
                    'rows' => self::ROWS,
                ],
                ])
            ->add('third', TextareaType::class, [
                'label' => self::LABEL_DESCRIPTION['third'],
                'attr' => [
                    'maxlength' => 1024,
                    'rows' => self::ROWS,
                ],
                ])
        ;
    }
}
