<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 7.7.18
 * Time: 11.55
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class PracticeAnalyticResultAdmin extends AbstractAdmin
{
    const ROW_SIZE = 7;
    const NONE = 'Не выбрано';

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();

        $formMapper
            ->add('service', TextareaType::class, [
                'attr' => [
                    'maxlength' => 255,
                    'rows' => self::ROW_SIZE
                ],
            ], [
            ])
            ->add('targetGroup', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => self::ROW_SIZE
                ],
            ])
            ->add('indicator', TextareaType::class, [
                    'required' => false,
                    'attr' => [
                        'rows' => self::ROW_SIZE
                    ],
            ])
            ->add('servicePlanValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
            ])
            ->add('serviceFactValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
            ])
            ->add('measurementMethod', null, [
                'placeholder' => 'Не выбрано',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ReportTypes::ANALYTIC_REPORT_2018 . '%');
                },
                'attr' => [
                    'placeholder' => self::NONE
                ],           ])
            ->add('isFeedback', ChoiceType::class, [
                'choices' => ['label_type_yes' => 1, 'label_type_no' => 0],
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'attr' => ['maxlength' => 450],
                'required' => false,

            ])
        ;
    }
}
