<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 6.8.18
 * Time: 11.34
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class PublicationAdmin extends AbstractAdmin
{
    const FIELD_DESCRIPTION = [
        'author' => 'Автор(ы)',
        'title' => 'Название',
        'date' => 'Дата',
        'location' => 'Место',
        'link' => 'Ссылка (при наличии)',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('author', FullscreenTextareaType::class, [
                'label' => self::FIELD_DESCRIPTION['author'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('title', FullscreenTextareaType::class, [
                'label' => self::FIELD_DESCRIPTION['title'],
                'required' => false,
            ])
            ->add('date', 'sonata_type_datetime_picker', [
                'label' => self::FIELD_DESCRIPTION['date'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('location', FullscreenTextareaType::class, [
                'label' => self::FIELD_DESCRIPTION['location'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                ],
            ])
            ->add('link', FullscreenTextareaType::class, [
                'label' => self::FIELD_DESCRIPTION['link'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                    'placeholder' => '—',
                    ],
            ])
            ;
    }
}
