<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 9.8.18
 * Time: 18.26
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\KNS;

use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class FeedbackAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'service' => 'Услуги, мероприятия и пр., по которым собиралась обратная связь',
        'comment' => 'Комментарий (выводы, цитаты)',
        'administrativeSolution' => 'Какие управленческие решения принимались по результатам обратной связи',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('service', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['service'],
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['comment'],
                'attr' => [
                    'maxlength' => 1500,
                    'rows' => 3
                ]
            ])
            ->add('administrativeSolution', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['administrativeSolution'],
                'attr' => [
                    'maxlength' => 1500,
                    'rows' => 3
                ]
            ])
            ->add('tools', 'sonata_type_collection', [
                'required' => false,
                'by_reference' => false,
            ])
        ;

        FileNameAdmin::configureFormFields($formMapper);
    }
}
