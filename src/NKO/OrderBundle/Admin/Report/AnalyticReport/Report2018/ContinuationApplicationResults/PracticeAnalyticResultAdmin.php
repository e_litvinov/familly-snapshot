<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 7.7.18
 * Time: 11.55
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\ContinuationApplicationResults;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class PracticeAnalyticResultAdmin extends AbstractAdmin
{
    const ROW_SIZE = 8;

    const DIRECT_RESULT_DESCRIPTION = [
        'service' => 'Услуги, мероприятия и пр.',
        'targetGroup' => 'Целевая группа',
        'serviceFactValue' => 'Факт',
        'servicePlanValue' => 'План',
        'beneficiaryFactValue' => 'Факт',
        'beneficiaryPlanValue' => 'План',
        'measurementMethod' => 'Способ измерения ',
        'isFeedback' => 'Собирали ли обратную связь',
        'comment' => 'Комментарий',
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $grantees = $em->getRepository(Grantee::class)->findGranteesByTypeData('brief_sf-2016');

        $formMapper
            ->add('linkedResult.service', TextareaType::class, [
                'attr' => ['maxlength' => 255, 'rows' => self::ROW_SIZE],
                'disabled' => true,
            ], [
            ])
            ->add('practiceFormat')
            ->add('linkedResult.problem', TextareaType::class, [
                'attr' => ['rows' => self::ROW_SIZE],
                'disabled' => true,
            ], [
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.problem'
            ])

            ->add('serviceFactValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
            ])
            ->add('linkedResult.servicePlanValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
                'disabled' => true,
            ])
            ->add('beneficiaryFactValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
            ])
            ->add('linkedResult.beneficiaryPlanValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
                'disabled' => true,
            ])
            ->add('measurementMethod', null, [
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ReportTypes::ANALYTIC_REPORT_2018 . '%');
                },
            ])
            ->add('isFeedback', ChoiceType::class, [
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1],
            ])
            ->add('grantee', 'sonata_type_model', [
                'btn_add' => false,
                'choices' => $grantees,
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'attr' => ['maxlength' => 450, 'rows' => self::ROW_SIZE]
            ])
            ->add('idDirectResult', HiddenType::class, [
                'label' => false,
                'mapped' => false,
                'data' => $this->getSubject() ? $this->getSubject()->getId() : null
            ])
        ;
    }
}
