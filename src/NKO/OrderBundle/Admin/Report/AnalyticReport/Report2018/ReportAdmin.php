<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018;

use NKO\OrderBundle\AdminConfigurator\AnalyticReport\PriorityDirectionAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\SpreadPracticeAdmin;
use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\AttachmentAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\MonitoringAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ProjectDescriptionAdmin;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Utils\Admin\Report\AnalyticReport\Report2018\PriorityDirection;
use NKO\OrderBundle\Utils\Admin\Report\AnalyticReport\Report2018\SpreadPractice;
use NKO\OrderBundle\EventListener\AnalyticPublicationsListener;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ResourceAdmin;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\EventListener\DocumentListener;
use NKO\OrderBundle\EventListener\DeleteRowListener;
use NKO\OrderBundle\EventListener\ChangeFeedbackAttachmentListener;

class ReportAdmin extends BaseReportAdmin
{
    const TAB_DESCRIPTION = [
        'projectCard' => 'КАРТОЧКА ПРОЕКТА',
        'projectRealization' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
        'spreadPractice' => 'РАСПРОСТРАНЕНИЕ И ВНЕДРЕНИЕ ПРАКТИКИ',
        'monitoring' => 'МОНИТОРИНГ И ОЦЕНКА',
        'resource' => 'РЕСУРСЫ ПРОЕКТА',
        'attachment' => 'ПРИЛОЖЕНИЯ',
    ];

    const RESOURCE_DESCRIPTION = [
        'partnerActivities' => '<b>5.1. Участие партнеров и доноров проекта</b>',
        'humanResources' => '<b>5.2. Кадровые ресурсы проекта</b><br><i>В случае отсутствия значения, введите "0"</i>',
    ];

    const MONITORING_DESCRIPTION = [
        'newMonitoringElements' =>'<b>4.2 Какие новые элементы были добавлены в систему мониторинга и оценки вашего проекта за отчетный период?</b>',
        'monitoringResults' =>'<b>4.1 Результаты проекта по блоку «Мониторинг и оценка»</b>',
    ];

    const CARD_PROJECT_INFO_DESCRIPTION = [
        'projectNameLabel' => '<b>Название Проекта/Практики</b>',
        'grantLabel' => '<b>Сумма пожертвования (за отчетный период)</b>',
        'accountantLabel' => '<b>Главный бухгалтер</b>'
    ];

    const ATTACHMENT_DESCRIPTION = [
        'warning' => '<p id="warning">Перед началом работы на вкладке ПРИЛОЖЕНИЯ сохраните, пожалуйста, отчет.<br> 
            Загружайте документы по одному. После добавления каждого документа – сохранитесь, дождитесь сообщения о сохранении отчета, откройте его и проверьте.</p>',
        'practiceSpreadFeedbackAttachments' => '<b>6.1.2. Обратная связь. Распространение и внедрение практики</b>',
        'practiceSuccessStoryAttachments' => '<b>6.2.1. Истории успеха. Реализация практики</b><br><i>Необязательный для заполнения пункт</i>',
        'practiceSpreadSuccessStoryAttachments' => '<b>6.2.2. Истории успеха. Распространение и внедрение практики</b><br><i>Необязательный для заполнения пункт</i>',
        'factorAttachments' => '<b>6.5. Организации, внедрившие Практику</b>',
        'monitoringDevelopmentAttachments' => '<b> 6.6. Разработки по системе мониторинга и оценки организации</b>'
    ];

    const HELPS = [
        'feedbackPracticeAttachments' => '<i>Список строк формируется по строкам таблицы 2.4., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'feedbackSpreadPracticeAttachments' => '<i>Список строк формируется по строкам таблицы 3.2., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'publicationAttachments' => '<i>Количество строк таблицы определяется значением “Количество сообщений в СМИ, инициированных в рамках проекта. Факт” в таблице 3.3.</i>',
        'materialAttachments' => '<i>Количество строк таблицы определяется значением “Количество изданных информационных / методических материалов. Факт” в таблице 3.3.</i>',
        'practiceSpreadSuccessStoryAttachments' => false,
        'practiceSuccessStoryAttachments' => false,
        'factorAttachments' => '<i>Количество строк в зависимости от значения показателя 15.1. Факт мониторингового отчета.</i><br><i>Пожалуйста, приложите файлы, подтверждающие факты внедрения практики.</i>',
    ];

    const DOCUMENTS_FIELD_NAME = [
        'practiceFeedbackAttachments' => 'tools',
        'practiceSpreadFeedbackAttachments' => 'tools',
        'practiceSuccessStoryAttachments',
        'practiceSpreadSuccessStoryAttachments',
        'factorAttachments',
        'publicationAttachments',
        'materialAttachments',
        'factorAttachments',
        'monitoringDevelopmentAttachments'
    ];

    const FEEDBACK_ATTACHMENT = [
        'expectedAnalyticResults' => 'practiceFeedbackAttachments',
        'practiceAnalyticResults' => 'practiceSpreadFeedbackAttachments'
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => array("AnalyticReport-2018")
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $report = $this->getSubject();
        $reportFormYear = null;
        $updatedAt = null;

        if ($report) {
            $grant = $em->getRepository(GrantConfig::class)->getGrantByReport($report);
            $currentDate = new \DateTime();
            $updatedAt = $report->getUpdatedAt() ? $report->getUpdatedAt()->format('d.m.Y H:m') : $currentDate->format('d.m.Y H:m');
            $reportFormYear = $report->getReportForm()->getYear()->format('Y');
        }

        $parameters = AdminConfigurator::PROJECT_DESCRIPTION[get_class($this->getSubject())];
        $parameters['value'] = [
            'contract' => isset($grant) ? $grant->getContract() : null,
            'sumGrant' => isset($grant) ? $grant->getSumGrant() : null,
            'updatedAt' => $updatedAt,
            'reportFormYear' => $reportFormYear,
            'projectName' => $report->getProjectName(),
        ];

        $applicationType = $report->getReportForm()->getCompetition()->getApplicationClass();
        //'implementationSocialResults' => 'nko_order.admin.analytic_report_2018.social_result'
        $priorityDirectionParameters = key_exists($applicationType, PriorityDirection::PRIORITY_DIRECTION_CONFIGURATOR) ?
            PriorityDirection::PRIORITY_DIRECTION_CONFIGURATOR[$applicationType] : null;
        $spreadPracticeParameters = key_exists($applicationType, SpreadPractice::SPREAD_PRACTICE_CONFIGURATOR) ?
            SpreadPractice::SPREAD_PRACTICE_CONFIGURATOR[$applicationType] : null;

        $formMapper->with(self::TAB_DESCRIPTION['projectCard']);
        ProjectDescriptionAdmin::configureFormFields($formMapper, $parameters, null, self::CARD_PROJECT_INFO_DESCRIPTION);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['projectRealization']);
        PriorityDirectionAdmin::configureFormFields($formMapper, $priorityDirectionParameters);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['spreadPractice']);
        SpreadPracticeAdmin::configureFormFields($formMapper, $spreadPracticeParameters);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['monitoring']);
        MonitoringAdmin::configureFormFields($formMapper, AdminConfigurator::MONITORING_INFO[get_class($this->getSubject())], null, self::MONITORING_DESCRIPTION);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['resource']);
        ResourceAdmin::configureFormFields($formMapper, AdminConfigurator::RESOURCE_ANALYTIC_REPORT[get_class($this->getSubject())], null, self::RESOURCE_DESCRIPTION);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['attachment']);
        $formMapper->add('warning', CustomTextType::class, [
            'help' => self::ATTACHMENT_DESCRIPTION['warning'],
        ]);
        AttachmentAdmin::configureFormFields(
            $formMapper,
            AdminConfigurator::ANALYTIC_ATTACHMENT[get_class($this->getSubject())],
            null,
            self::ATTACHMENT_DESCRIPTION,
            self::HELPS
        );
        $formMapper->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new DocumentListener(self::DOCUMENTS_FIELD_NAME));
        $formMapper->getFormBuilder()->addEventSubscriber(new AnalyticPublicationsListener());
        $formMapper->getFormBuilder()->addEventSubscriber(new ChangeFeedbackAttachmentListener(self::FEEDBACK_ATTACHMENT));
    }
}
