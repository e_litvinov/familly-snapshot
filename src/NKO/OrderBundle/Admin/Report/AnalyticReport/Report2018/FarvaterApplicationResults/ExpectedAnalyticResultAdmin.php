<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 7.7.18
 * Time: 14.59
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018\FarvaterApplicationResults;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ExpectedAnalyticResultAdmin extends AbstractAdmin
{
    const INDICATOR = [
        'expectedAnalyticResults' => 'linkedFarvaterResult.indicator',
        'practiceAnalyticIndividualResults' => 'linkedFarvaterResult.customIndicator'
    ];

    const ROW_SIZE = 9;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $parentFieldName = $this->getParentFieldDescription()->getName();
        $indicator = $parentFieldName && array_key_exists($parentFieldName, self::INDICATOR) ?  self::INDICATOR[$parentFieldName] : null;

        $formMapper
            ->add('linkedFarvaterResult.service', TextareaType::class, [
                'attr' => ['maxlength' => 255, 'rows' => self::ROW_SIZE],
                'disabled' => true,
            ])
            ->add('linkedFarvaterResult.targetGroup', TextareaType::class, [
                'attr' => ['rows' => self::ROW_SIZE],
                'disabled' => true,
                ])

            ->add($indicator, TextareaType::class, [
                'attr' => ['rows' => self::ROW_SIZE],
                'disabled' => true,
            ])
            ->add('linkedFarvaterResult.targetValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
                'disabled' => true,
            ])
            ->add('serviceFactValue', TextType::class, [
                'attr' =>['placeholder' => '0'],
            ])
            ->add('measurementMethod', null, [
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ReportTypes::ANALYTIC_REPORT_2018 . '%');
                }
            ])
            ->add('isFeedback', ChoiceType::class, [
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]])
            ->add('comment', FullscreenTextareaType::class, ['attr' => ['maxlength' => 450]])
            ->add('idDirectResult', HiddenType::class, [
                'label' => false,
                'mapped' => false,
                'data' => $this->getSubject() ? $this->getSubject()->getId() : null
            ])
        ;
    }
}