<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 5.7.18
 * Time: 14.49
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class PublicationAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('indicator', TextareaType::class, ['label' => 'Показатель', 'disabled' => true])
            ->add('fact', IntegerType::class, ['label' => 'Факт', 'attr' => ['placeholder' => '0']])

        ;
    }
}