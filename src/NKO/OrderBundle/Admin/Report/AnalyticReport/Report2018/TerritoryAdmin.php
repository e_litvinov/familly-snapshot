<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TerritoryAdmin extends AbstractAdmin
{
    const DESCRIPTION =[
        'region' => 'Регион',
        'locality' => 'Населенные пункты'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('region', 'sonata_type_model', [
                'btn_add' => false,
                'label' => self::DESCRIPTION['region'],
                'required' => false,
            ])
            ->add('locality', TextType::class, [
                'label' => self::DESCRIPTION['locality'],
                'required' => false,
            ])
        ;
    }
}
