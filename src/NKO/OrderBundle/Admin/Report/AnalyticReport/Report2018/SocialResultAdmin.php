<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018;

use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\TransferResult;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication2018;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplication2018;

class SocialResultAdmin extends AbstractAdmin
{
    const DESCRIPTION =[
        'result' => 'Cоциальный результат',
        'comment' => 'Комментарий о достижении результата'
    ];

    const QUERY_OPTIONS = [
        'implementationSocialResults' => [
            ContinuationApplication2018::class => 'EffectivenessImplementationItems',
            FarvaterApplication2018::class => 'ProjectSocialResults'
        ],
        'introductionSocialResults' => [
            ContinuationApplication2018::class => 'EffectivenessDisseminationItems',
            FarvaterApplication2018::class => 'PracticeSocialResults',
        ]
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $code = null;
        $fieldDescription = $this->getParentFieldDescription()->getFieldName();
        $object = $this->getParentFieldDescription()->getAdmin()->getSubject();
        $relatedApplication = $object->getReportForm()->getCompetition()->getApplicationClass();

        if (array_key_exists($fieldDescription, self::QUERY_OPTIONS)) {
            $code = array_key_exists($relatedApplication, self::QUERY_OPTIONS[$fieldDescription]) ? self::QUERY_OPTIONS[$fieldDescription][$relatedApplication] : null;
        }

        $formMapper
            ->add('result', 'sonata_type_model',  [
                'attr' => ['style' => 'max-width: 550px',],
                'btn_add' => false,
                'label' => self::DESCRIPTION['result'],
                'query' => $this->getConfigurationPool()->getContainer()
                    ->get('Doctrine')->getRepository(TransferResult::class)->createQueryBuilder('p')
                    ->where('p.code = :code')
                    ->andWhere('p.report = :report')
                    ->setParameters(['code' => $code, 'report'=> $object->getId()])
                    ->getQuery(),
            ], [
                'admin_code' => 'nko_order.admin.indicator'
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['comment']
            ])
        ;
    }
}
