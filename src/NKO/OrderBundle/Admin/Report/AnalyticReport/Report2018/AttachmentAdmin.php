<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 12.7.18
 * Time: 12.25
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2018;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class AttachmentAdmin extends AbstractAdmin
{
    const FIELDS = [
        'practiceSpreadSuccessStoryAttachments' => [
            'title' => 'Название истории успеха',
            'file' => 'Приложение в формате pdf',
        ],
        'practiceSuccessStoryAttachments' => [
            'title' => 'Название истории успеха',
            'file' => 'Приложение в формате pdf',
        ],
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if(array_key_exists($this->getParentFieldDescription()->getFieldName(), self::FIELDS)) {
            $formMapper
                ->add('title', FullscreenTextareaType::class, [
                    'label' => self::FIELDS[$this->getParentFieldDescription()->getFieldName()]['title'],
                ])
                ->add('file', FilePreviewType::class, [
                    'label' => self::FIELDS[$this->getParentFieldDescription()->getFieldName()]['file'],
                    'link_target' => '_blank'
                ])
            ;

            FileNameAdmin::configureFormFields($formMapper);
        }
    }
}
