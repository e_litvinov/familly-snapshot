<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;

class MaterialAttachmentAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'author' => 'ФИО автора',
        'name' => 'Название материала',
        'date' => 'Дата издания',
        'edition' => 'Тираж (для печатных изданий)',
        'mediaLink' => 'Ссылка на публикацию материала (при наличии)',
        'comment' => 'Комментарий по распространению методических материалов',
        'file' => 'Макет в формате pdf',
    ];

    const ATTR = [
        'style' => 'width:200px',
        'rows' => 3
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('author', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['author']
            ])
            ->add('name', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['name']
            ])
            ->add('date', DateTimePickerType::class, array(
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,
                'label' => self::DESCRIPTION['date']
            ))
            ->add('edition', null, [
                'label' => self::DESCRIPTION['edition']
            ])
            ->add('mediaLink', TextareaType::class, [
                'attr' => self::ATTR,
                'label' => self::DESCRIPTION['mediaLink']
            ])
            ->add('comment', TextareaType::class, [
                'attr' => array_merge(['maxlength' => 1024], self::ATTR),
                'label' => self::DESCRIPTION['comment']
            ])
            ->add('file', FilePreviewType::class, [
                'link_target' => '_blank',
                'label' => self::DESCRIPTION['file']
            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }
}
