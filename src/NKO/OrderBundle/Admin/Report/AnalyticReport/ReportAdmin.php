<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\MonitoringAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ProjectDescriptionAdmin;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection;
use NKO\OrderBundle\EventListener\AnalyticPublicationsListener;
use NKO\OrderBundle\Utils\Admin\Report\AnalyticReport\ReportAdminDescription;
use NKO\OrderBundle\Utils\Admin\Report\AnalyticReport\ReportAdminValidation;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Valid;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ResourceAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\AttachmentAdmin as AttachmentAdmin;
use NKO\OrderBundle\EventListener\DeleteRowListener;

class ReportAdmin extends BaseReportAdmin
{
    const HELPS = [
        'socialNotSolvedProblems' => 'Данное поле не обязательно для заполнения каждый месяц.
            Пожалуйста, заполните его в свободной форме, если в прошедшем месяце вы сталкивались с трудностями в реализации практики.
            <ul>
                <li>Трудности или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов 
                и/или повлиявших на процесс реализации Проекта (в отношении практики); что из запланированного не удалось выполнить и почему</li>
                <li>Пути решения проблем/способы выхода из затруднительных ситуаций или пояснения, почему их нельзя решить силами вашего проекта.</li>
            </ul>',
        'monitoringAttachments' => 'Выпадающий список формируется на основе значений строк таблицы 4.2'
    ];

    const PUBLICATIONS_INDICATOR = [
        'Число сообщений в СМИ, инициированных в рамках проекта',
        'Число изданных информационных / методических материалов',
        'Тираж печатных информационных / методических материалов',
        'Количество распространенных экземпляров информационных / методических материалов по проекту'
    ];

    const LISTENER_TABLES = [
        'practiceAnalyticResults',
        'expectedAnalyticResults',
        'practiceFeedbackAttachments',
        'practiceSpreadFeedbackAttachments'
    ];

    const MIN_COUNT = 1;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();

        $report = $this->getSubject();
        $reportFormYear = null;
        $updatedAt = null;
        $grant = null;

        if ($report) {
            $grant = $em->getRepository('NKOOrderBundle:GrantConfig')
                ->findOneBy([
                    'reportForm' => $report->getReportForm(),
                    'applicationHistory' => $report->getApplicationHistory()
                ]);

            $currentDate = new \DateTime();
            $updatedAt = $report->getUpdatedAt() ? $report->getUpdatedAt()->format('d.m.Y H:m') : $currentDate->format('d.m.Y H:m');
            $reportFormYear = $report->getReportForm()->getYear()->format('Y');
        }
        $description = $this->setDescription();

        $parameters = AdminConfigurator::PROJECT_DESCRIPTION[get_class($this->getSubject())];
        $parameters['value'] = [
            'updatedAt' => $updatedAt,
            'contract' => $grant->getContract(),
            'sumGrant' => $grant->getSumGrant(),
            'reportFormYear' => $reportFormYear,
        ];

        $formMapper->with($description['contactsLabel']);
        ProjectDescriptionAdmin::configureFormFields($formMapper, $parameters);
        $formMapper->end();

        $formMapper->with($description['practiceLabel'])
            ->add('priorityDirection', 'sonata_type_model', array(
                'label' => $description['priorityDirection'],
                'property' => 'name',
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'query' => $em->createQueryBuilder()
                    ->select('p')
                    ->from(PriorityDirection::class, 'p')
                    ->where('p.type = :type')
                    ->setParameters(['type' => 'brief_sf-2016'])
            ))
            ->add('priorityDirectionEtc', null, [
                'label' => $description['priorityDirectionEtc'],
                'disabled' => false
            ])
            ->add('projectPurpose', TextareaType::class, [
                'label' => $description['projectPurpose'],
                'attr' => array(
                    'maxlength' => 450,
                    'readonly' => true
                )
            ])
            ->add('practiceAnalyticResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => $description['realizationResults'],
                    'btn_add' => 'Добавить',
                    'by_reference' => false,
                ), array(
                    'admin_code' => 'nko_order.admin.analytic_report.practice_analytic_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('practiceMonitoringResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => $description['practiceResultsLabel'],
                    'btn_add' => false,
                    'type_options' => array(
                        'delete' => false
                    )
                ), array(
                    'admin_code' => 'nko_order.admin.analytic_report.monitoring_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('socialSolvedProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['socialSolvedProblems'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('socialNotSolvedProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['socialNotSolvedProblems'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
                'help' => self::HELPS['socialNotSolvedProblems']
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('socialMeasures', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['socialMeasures'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('nextSocialMeasures', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['nextSocialMeasures'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('realizationFactors', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['realizationFactors'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->end();

        $formMapper->with($description['practiceDisseminationLabel'])
            ->add('expectedAnalyticResults', 'sonata_type_collection', [
                    'label' => $description['expectedResultsLabel'],
                    'required' => false,
                    'btn_add' => 'Добавить',
                    'by_reference' => false,
                ], [
                    'admin_code' => 'nko_order.admin.analytic_report.expected_analytic_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('introductionMonitoringResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => $description['introductionResultsLabel'],
                    'btn_add' => false,
                    'type_options' => array(
                        'delete' => false
                    )
                ), array(
                    'admin_code' => 'nko_order.admin.analytic_report.monitoring_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('analyticPublications', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['publications'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'admin_code' => 'nko_order.admin_analytic_publication',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('practiceSolvedProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['practiceSolvedProblems'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('practiceNotSolvedProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['practiceNotSolvedProblems'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
                'help' => self::HELPS['socialNotSolvedProblems']
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('resultStability', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['resultStability'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('introductionFactors', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['introductionFactors'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->end();

        $formMapper->with($description['monitoring']);
        MonitoringAdmin::configureFormFields($formMapper, AdminConfigurator::MONITORING_INFO[get_class($this->getSubject())]);
        $formMapper->end();

        $formMapper->with($description['commonResults']);
        ResourceAdmin::configureFormFields($formMapper, AdminConfigurator::RESOURCE_ANALYTIC_REPORT[get_class($this->getSubject())]);
        $formMapper->end();

        $formMapper->with($description['attachment']);
        AttachmentAdmin::configureFormFields($formMapper, AdminConfigurator::ANALYTIC_ATTACHMENT[get_class($this->getSubject())]);
        $formMapper
            ->add('monitoringAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['monitoringAttachments'],
                    'btn_add' => 'Добавить',
                    'by_reference' => false,
                    'help' => self::HELPS['monitoringAttachments']
                ], [
                    'admin_code' => 'nko_order.admin.analytic_report.monitoring_attachment',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new DeleteRowListener(self::LISTENER_TABLES));
        $formMapper->getFormBuilder()->addEventSubscriber(new AnalyticPublicationsListener());
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $subject = $this->getSubject();
        if (!$subject) {
            return;
        }

        $this->validateRelations($errorElement, ReportAdminValidation::DEFAULT_VALIDATION_FIELDS);

        $applicationClass = $this->getApplicationClass($subject);
        switch ($applicationClass) {
            case Application::class:
                $this->validateKns($errorElement);
                return $this->validateRelations($errorElement, ReportAdminValidation::KNS_VALIDATION_FIELDS);
            default:
                return $this->validateRelations($errorElement, ReportAdminValidation::FARVATER_VALIDATION_FIELDS);
        }
    }

    private function validateRelations(ErrorElement $errorElement, $properties)
    {
        foreach ($properties as $property) {
            $errorElement
                ->with($property)
                    ->addConstraint(new Valid())
                ->end();
        }
    }

    private function validateKns(ErrorElement $errorElement)
    {
        $errorElement
            ->with('accountantName')
                ->assertNotBlank()
            ->end()
            ->with('accountantPosition')
                ->assertNotBlank()
            ->end()
        ;
    }

    private function getApplicationClass($report)
    {
        return $report->getReportForm()->getCompetition()->getApplicationClass();
    }

    private function setDescription()
    {
        $subject = $this->getSubject();
        if (!$subject) {
            return false;
        }

        $applicationClass = $this->getApplicationClass($subject);
        if ($applicationClass == Application::class) {
            return ReportAdminDescription::KNS_DESCRIPTION;
        }

        return ReportAdminDescription::FARVATER_DESCRIPTION;
    }
}
