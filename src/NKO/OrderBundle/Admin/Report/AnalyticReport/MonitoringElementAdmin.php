<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class MonitoringElementAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('text', FullscreenTextareaType::class, [
                'label' => 'Название элемента', 'attr' => ['maxlength' => 1500]])
        ;
    }
}
