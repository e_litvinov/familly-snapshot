<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Admin\Report\MonitoringReport\PeriodResultAdmin;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;

class MonitoringResultAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'indicator' => 'Показатели, важные для Фонда',
        'targetValues' => 'Целевые значения (итого за год)',
        'methods' => 'Способ измерения',
        'comment' => 'Комментарии'
    ];

    const TEXT_MAX_LENGTH = 1024;
    const STRING_MAX_LENGTH = 256;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $index = null;
        $result = $this->getSubject();
        if ($result) {
            $parentIndicator = $result->getIndicator()->getParent();
            if ($parentIndicator && $parentIndicator->getIndexNumber()) {
                $index = $parentIndicator->getIndexNumber() . '.' . $result->getIndicator()->getIndexNumber();
            }
            else {
                $index = $result->getIndicator()->getIndexNumber();
            }
        }
        $formMapper
            ->add('index', TextType::class, array(
                'label' => '№',
                'data' => $index,
                'mapped' => false,
                'attr' => array(
                    'readonly' => true,
                    'style' => 'width: 50px'
                )
            ))
            ->add('indicator', 'sonata_type_admin', array(
                'label' => self::DESCRIPTION['indicator'],
            ), array (
                'admin_code' => 'nko_order.admin.indicator'
            ))
            ->add('finalPeriodResult', 'sonata_type_collection', array(
                'label' => self::DESCRIPTION['targetValues'],
                'by_reference' => false,
                'type_options' => ['delete' => false]
            ), array(
                'admin_code' => 'nko_order.admin.period_result',
            ))
            ->add('method', 'sonata_type_model', array(
                'label' => self::DESCRIPTION['methods'],
                'by_reference' => false,
                'required' => false,
            ))
            ->add('comment', TextareaType::class, array(
                'label' => self::DESCRIPTION['comment'],
                'attr' => array(
                    'placeholder' => '-',
                    'maxlength' => self::TEXT_MAX_LENGTH,
                    'readonly' => true
                )
            ))
        ;
    }
}
