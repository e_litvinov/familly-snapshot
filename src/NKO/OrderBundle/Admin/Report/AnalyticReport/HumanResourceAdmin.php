<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class HumanResourceAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('executorsCount', IntegerType::class, [
                'label' => 'Количество исполнителей проекта на начало года (любой тип занятости)',
                'attr' => [
                    'placeholder'=> 0,
                ]
            ])
            ->add('substitutionsCount', IntegerType::class, [
                'label' => 'Количество замен в составе исполнителей проекта в течение года',
                'attr' => [
                    'placeholder'=> 0,
                ]
            ])
            ->add('ratesCount', IntegerType::class, [
                'label' => 'Количество введенных дополнительно ставок в проекте',
                'attr' => [
                    'placeholder'=> 0,
                ]
            ])
            ->add('individualJobSpecialistCount', IntegerType::class, [
                'label' => 'Количество специалистов, дополнительно привлекавшихся в проект для отдельных работ',
                'attr' => [
                    'placeholder'=> 0,
                ]
            ])
            ->add('comment', FullscreenTextareaType::class, ['label' => 'Комментарий к кадровым изменениям за прошедший год', 'attr' => ['maxlength' => 1024]])
        ;
    }
}
