<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as Report2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as Report2019;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;

class FactorAttachmentAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        Report::class => [
            'name' => 'Название',
            'person' => 'Контакты (контактное лицо, телефон, mail)',
            'link' => 'Ссылка на материалы организаций, иллюстрирующе внедрение вышей практики (если есть)',
            'comment' => 'Комментарий',
            'file' => 'Подтверждающие документы'
        ],
        Report2018::class => [
            'name' => 'Организация',
            'person' => 'Контакты (контактное лицо, телефон, mail)',
            'link' => 'Ссылка на материалы организаций, иллюстрирующе внедрение вышей практики (если есть)',
            'comment' => 'Комментарий',
            'file' => 'Подтверждающие документы'
        ],
        Report2019::class => [
            'name' => 'Организация',
            'person' => 'Контакты (контактное лицо, телефон, mail)',
            'link' => 'Ссылка на материалы организаций, иллюстрирующе внедрение вышей практики (если есть)',
            'comment' => 'Комментарий',
            'file' => 'Подтверждающие документы в формате pdf'
        ]
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $parentClass = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());
        $formMapper
            ->add('name', FullscreenTextareaType::class, ['label' => self::DESCRIPTION[$parentClass]['name']])
            ->add('person', FullscreenTextareaType::class, ['attr' => ['maxlength' => 450], 'label' => self::DESCRIPTION[$parentClass]['person']])
            ->add('link', FullscreenTextareaType::class, ['label' => self::DESCRIPTION[$parentClass]['link']])
            ->add('comment', FullscreenTextareaType::class, ['attr' => ['maxlength' => 1000], 'label' => self::DESCRIPTION[$parentClass]['comment']])
            ->add('file', FilePreviewType::class, [
                'label' => self::DESCRIPTION[$parentClass]['file'],
                'link_target' => '_blank'
            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }
}
