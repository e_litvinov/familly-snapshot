<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Harbor\Report2019;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ProjectDescriptionAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ResourceAdmin;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\TrainingGround;
use NKO\OrderBundle\EventListener\DocumentListener;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;

class ReportAdmin extends BaseReportAdmin
{
    const TAB_DESCRIPTION = [
        'projectCard' => '1. КАРТОЧКА ПРОЕКТА',
        'projectRealization' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
        'practiceSpread' => 'РАСПРОСТРАНЕНИЕ ПРАКТИКИ, ОПЫТА РЕАЛИЗАЦИИ ПРОЕКТА',
        'resource' => 'РЕСУРСНОЕ ОБЕСПЕЧЕНИЕ ПРОЕКТА',
        'attachment' => 'ПРИЛОЖЕНИЯ',
    ];

    const PROJECT_CARD = [
        'projectNameLabel' => "Название практики",
        'accountantLabel' => '<b>Главный бухгалтер/ бухгалтер:</b>',
    ];

    const ORGANIZATION_INFO_DESCRIPTION = [
        'email' => '<b>Адрес электронной почты</b> (для оперативного контакта с организацией)',
        'phoneLabel' => '<b>Контактные данные организации</b>',
        'phoneLabelHelp' => '<label class="help">Телефон</label>',
        'internationalCode' => '<label class="code">+7</label>',
        'competition' => 'Конкурс',
        'normativeAct' => '<b>Нормативные акты, которые применяются в деятельности организации</b>',
    ];

    const REALIZATION_PRACTICE = [
        'practice' => "2.1. <b>Практика:</b>",
        'trainingGround' => "2.1. <b>Приоритетное направление Конкурса</b>",
        'beneficiaryGroups' => "<b>2.3. Целевые группы (благополучатели) практики: </b>",
        'projectPurpose' => "<b>2.4. Цель проекта</b>",
        'territories' => "<b>2.5. Территория реализации проекта</b>",
        'practiceAnalyticResults' => '<b>2.6 Непосредственные результаты проекта</b>',
        'practiceAnalyticResultCommentYear' => '<b>2.6.1 Комментарий к таблице 2.6 (по результатам реализации проекта в этом году)</b>',
        'practiceAnalyticResultComment' => '<b>2.6.2 Комментарий к таблице 2.6 (по результатам реализации всего проекта)</b>',
        'expectedAnalyticResults' => '<b>2.7 Социальные результаты проекта</b>',
        'expectedAnalyticResultCommentYear' => '<b>2.7.1 Комментарий к таблице 2.7 (по результатам реализации проекта в этом году)</b>',
        'expectedAnalyticResultComment' => '<b>2.7.2 Комментарий к таблице 2.7 (по результатам реализации всего проекта)</b>',
        'additionalField' => "<b>2.8. Анализ деятельности по проекту</b>",
        'projectResume' => "<b>2.8.1. Краткие выводы, резюме по реализации проекта</b><br>
            <i>Охарактеризуйте в целом ход реализации проекта.</i>
            <ul><i>
                <li>Какой цели хотели добиться? Получается ли? Что самого интересного было в ходе реализации проекта?</li>
                <li>Удается ли быть в графике или есть определенные проблемы по достижению запланированных целевых значений?</li>
                <li>Есть ли непредвиденные результаты?</li>
                <li>Какие были существенные изменения в ходе реализации проекта (категории благополучателей,
                 мероприятия, сроки, объемы деятельности, изменения в системе мониторинга и оценки и пр.); с чем они были связаны?</li>
            </i></ul>
            <i>Сфокусируйтесь на самой значимой информации.</i>
            ",
        'realizationProblem' => "<b>2.8.2. Трудности в реализации проекта</b> 
            <ul><i>
                <li>Что из первоначально запланированного не удалось выполнить и почему?</li>
                <li>Трудности или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс реализации проекта; </li>
                <li>Пути решения проблем / способы выхода из затруднительных ситуаций или пояснения, почему их нельзя было решить вашими силами</li>
            </i></ul>",
        'successStories' => "<b>2.8.3. Истории, факторы успеха</b><br>
            <i>Кратко опишите, какие были успехи в ходе реализации проекта. Какие факторы способствовали успешной реализации проекта?
            Поделитесь историями успеха: опишите успешный кейс; дайте прямую речь благополучателей или 
            сотрудников, работающих с благополучателями, прикрепите их как приложение в П.5.2.</i>",
        'lessons' => "<b>2.8.4. Извлечённые уроки, изменения в практике</b>
            <br><i>Какие уроки вы для себя извлекли в ходе реализации проекта (в т.ч. благодаря сбору обратной связи)? 
            Как это уже повлияло или повлияет на реализацию практики в будущем?
            Какие новые элементы были внедрены в практику за 
            время реализации проекта и почему? Как вы оцениваете эти изменения, на что это повлияло? 
            Что вы стали делать по-другому и почему? К каким результатам это привело?</i>",
    ];

    const SPREAD_PRACTICE = [
        'publications' => "<b>3.1. Публикации в рамках проекта</b>",
        'experience' => "<b> 3.2. Распространение Практики, опыта реализации проекта</b><br><i>Как вы распространяли опыт внедрения Практики, реализации проекта в целом? Перечислите соответствующие мероприятия, в 
            которых участвовала ваша организация, целевая группа (кто и сколько), формат распространения практики.</i>",
        'projectExperience' => "<b>3.3. Основные выводы о распространении Практики, опыте реализации проекта (в т.ч. истории успеха, трудности)</b><br><i>
            Поделитесь своими основными наблюдениями и выводами о распространении Практики, опытом реализации проекта. С какими трудностями вы столкнулись в ходе распространения опыта; какие были истории успеха?
             Что получилось, а что – не очень, почему? Что вы намерены делать по-другому в следующем году?</i>",
        'lastYearReportLink' => "<b>3.4. Ссылка на последний годовой отчёт организации (при наличии): </b>",
    ];

    const RESOURCE = [
        'partnerActivities' => '<b>4.1. Участие партнеров и доноров проекта</b>',
        'commandConclusion' => '<b>4.1.1.Выводы относительно команды проекта и её работы</b><br>
            <i>Как вы в целом оцениваете работу вашей команды, взаимодействие внутри команды в ходе проекта?
             Что получилось, а что – не очень? Что можно было бы сделать по-другому?<br>
            Какие были изменения в составе команды? Какие позитивные и негативные изменения произошли в 
            команде и её работе в ходе реализации проекта (например, повышение профессионального уровня сотрудников, 
            профессиональные награды, или наоборот – рост нагрузки, ухудшение климата в организации, текучка кадров и пр.)?</i>
            ',
        'humanResources' => '<b>4.2. Кадровые ресурсы проекта. Количество замен в составе исполнителей проекта в течение года</b>',
        'partnerConclusion' => '<b>4.2.1 Выводы относительно участия в проекте партнёров и доноров</b><br>
            <i>Как вы в целом оцениваете участие партнёров и доноров проекта? кого особенно хотелось бы поблагодарить и 
            за что? Кто и почему фактически участвовал не так, как вы планировали, кто «подвёл»?</i>',
    ];

    const ATTACHMENT_DESCRIPTION = '<p id="warning">Перед началом работы на вкладке “Приложения” сохраните, пожалуйста, отчет.
        Загружайте документы по одному. После добавления каждого документа – сохранитесь, 
        дождитесь сообщения о загрузке документа, откройте его и проверьте.';

    const ATTACHMENT = [
        'practiceFeedbackAttachments' => '<b>5.1. Обратная связь</b>',
        'practiceSuccessStoryAttachments' => '<b>5.2. Истории успеха</b>',
        'practiceSpreadSuccessStoryAttachments' => '<b>5.3. Иные приложения</b><br>
            <i>
            Здесь вы можете прикрепить любые иные документы и материалы по проекту.</i>',
    ];

    const DOCUMENTS_FIELD_NAME = [
        'practiceFeedbackAttachments' => 'tools',
        'practiceSuccessStoryAttachments',
        'practiceSpreadSuccessStoryAttachments',
    ];


    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = [
            'validation_groups' => ["HarborAnalyticReport-2019"]
        ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        /** @var AnalyticHarborReport2019 $report */
        $report = $this->getSubject();
        $reportFormYear = null;
        $updatedAt = null;

        if ($report) {
            $grant = $em->getRepository(GrantConfig::class)->getGrantByReport($report);
            $reportFormYear = $report->getReportForm()->getYear()->format('Y');
        }

        $projectName = $report->getProjectName();

        $parameters = AdminConfigurator::PROJECT_DESCRIPTION[get_class($this->getSubject())];
        $parameters['value'] = [
            'contract' => isset($grant) ? $grant->getContract() : null,
            'sumGrant' => isset($grant) ? $grant->getSumGrant() : null,
            'updatedAt' => $updatedAt,
            'year' => $reportFormYear,
            'projectName' => $projectName,
        ];

        $formMapper
            ->with(self::TAB_DESCRIPTION['projectCard'])
            ->add('reportForm.competition.name', TextType::class, [
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['competition'],
                'attr' => [
                    'readonly' => true,
                ]
            ]);

        ProjectDescriptionAdmin::configureFormFields($formMapper, $parameters, null, self::PROJECT_CARD);
        $formMapper
            ->add('phoneLabel', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['phoneLabel'],
            ])
            ->add('phoneLabelHelp', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['phoneLabelHelp'],
            ])
            ->add('internationalCode', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['internationalCode'],
            ])
            ->add('phoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ]
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['email'],
                'required' => false,
            ])
            ->add('normativeActs', EntityType::class, [
                'class' => NormativeAct::class,
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['normativeAct'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ReportTypes::HARBOR_REPORT_2019]);
                },
                'multiple' => true,
            ])
            ->end();

        $formMapper->with(self::TAB_DESCRIPTION['projectRealization']);
        $formMapper
            ->add('practice', TextType::class, [
                'label' => self::REALIZATION_PRACTICE['practice'],
                'attr' => [
                    'readonly' => 'readonly',
                    'multiple' => false,
                    'expanded' => true,
                ]
            ])
            ->add('trainingGround', EntityType::class, [
                'class' => TrainingGround::class,
                'label' => self::REALIZATION_PRACTICE['trainingGround'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ReportTypes::HARBOR_REPORT_2019]);
                },
                'multiple' => false,

            ])
            ->add('beneficiaryGroups', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['beneficiaryGroups'],
                'required' => false,
                'attr' => [
                    'maxlength' => 450,
                ]
            ])
            ->add('projectPurpose', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['projectPurpose'],
                'required' => false,
            ])
            ->add('territories', 'sonata_type_collection', [
                'label' => self::REALIZATION_PRACTICE['territories'],
                'required' => false,

            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('practiceAnalyticResults', 'sonata_type_collection', [
                'label' => self::REALIZATION_PRACTICE['practiceAnalyticResults'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.harbor_report2019.practice_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('practiceAnalyticResultCommentYear', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['practiceAnalyticResultCommentYear'],
                'required' => false,
            ])
            ->add('practiceAnalyticResultComment', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['practiceAnalyticResultComment'],
                'required' => false,
            ])
            ->add('expectedAnalyticResults', 'sonata_type_collection', [
                'label' => self::REALIZATION_PRACTICE['expectedAnalyticResults'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.harbor_report2019.expected_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('expectedAnalyticResultCommentYear', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['expectedAnalyticResultCommentYear'],
                'required' => false,
            ])
            ->add('expectedAnalyticResultComment', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['expectedAnalyticResultComment'],
                'required' => false,
            ])
            ->add('additionalField', CustomTextType::class, [
                'help' => self::REALIZATION_PRACTICE['additionalField'],
            ])
            ->add('projectResume', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['projectResume'],
                'required' => false,
            ])
            ->add('realizationProblem', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['realizationProblem'],
                'required' => false,
            ])
            ->add('successStories', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['successStories'],
                'required' => false,
            ])
            ->add('lessons', FullscreenTextareaType::class, [
                'label' => self::REALIZATION_PRACTICE['lessons'],
                'required' => false,
            ]);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['practiceSpread']);
        $formMapper
            ->add('publications', 'sonata_type_collection', [
                'label' => self::SPREAD_PRACTICE['publications'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('experience', CollectionType::class, [
                'label' => self::SPREAD_PRACTICE['experience'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.harbor_report2019.direct_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('projectExperience', FullscreenTextareaType::class, [
                'label' => self::SPREAD_PRACTICE['projectExperience'],
                'required' => false,
            ])
            ->add('lastYearReportLink', TextType::class, [
                'label' => self::SPREAD_PRACTICE['lastYearReportLink'],
                'required' => false,
            ]);
        $formMapper->end();

        $formMapper->with(self::TAB_DESCRIPTION['resource']);
        ResourceAdmin::configureFormFields(
            $formMapper,
            AdminConfigurator::RESOURCE_ANALYTIC_REPORT[get_class($this->getSubject())],
            null,
            self::RESOURCE
        );
        $formMapper->end();


        $formMapper->with(self::TAB_DESCRIPTION['attachment']);
        $formMapper->add('warning', CustomTextType::class, [
            'help' => self::ATTACHMENT_DESCRIPTION,
        ]);
        $formMapper
            ->add('practiceFeedbackAttachments', 'sonata_type_collection', [
                'label' => self::ATTACHMENT['practiceFeedbackAttachments'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.feedback',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('practiceSuccessStoryAttachments', 'sonata_type_collection', [
                'label' => self::ATTACHMENT['practiceSuccessStoryAttachments'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.feedback_success_story',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            // practiceSpreadSuccessStoryAttachments used for not creating new field.
            ->add('practiceSpreadSuccessStoryAttachments', 'sonata_type_collection', [
                'label' => self::ATTACHMENT['practiceSpreadSuccessStoryAttachments'],
                'by_reference' => false,
                'required' => false,
                'btn_add' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.kns.feedback_success_story',
                'edit' => 'inline',
                'inline' => 'table',
            ]);
        $formMapper->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new DocumentListener(self::DOCUMENTS_FIELD_NAME));
    }
}
