<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Harbor\Report2019;

use NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DirectResultAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'action'         => 'Мероприятие',
        'targetGroup'       => 'Целевая группа ',
        'practiceFormat'    => 'Формат распространения практики',
        'result'         => 'Результаты',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('action', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['action'],
            ])
            ->add('targetGroup', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['targetGroup'],
            ])
            ->add('practiceFormat', EntityType::class, [
                'class' => PracticeFormat::class,
                'placeholder' => 'Не выбран',
                'required' => false,
                'label' => self::DESCRIPTION['practiceFormat'],
            ])
            ->add('result', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['result'],
            ])
        ;
    }
}
