<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DoubleContextAdmin extends AbstractAdmin
{
    const LABEL_NAMES = [
        'socialNotSolvedProblems' => [
            'title' => 'Название трудности',
            'context' => 'Описание трудности и ее природы'
        ],
        'socialSolvedProblems' => [
            'title' => 'Название трудности',
            'context' => 'Описание трудности и предпринятых действий'
        ],
        'practiceSuccessStories' => [
            'title' => 'Название истории успеха',
            'context' => 'Истории успеха'
        ],
        'introductionFactors' => [
            'title' => 'Название фактора',
            'context' => 'Комментарий'
        ],
        'monitoringDevelopments' => [
            'title' => 'Название разработки',
            'context' => 'Комментарий'
        ],
        'realizationFactors' => [
            'title' => 'Название фактора',
            'title_attr' => ['maxlength' => 450],
            'context' => 'Комментарий',
            'context_attr' => ['maxlength' => 1000]
        ],
        'practiceSolvedProblems' => [
            'title' => 'Название трудности',
            'context' => 'Описание трудности и предпринятых действий'
        ],
        'practiceNotSolvedProblems' => [
            'title' => 'Название трудности',
            'context' => 'Описание трудности и ее природы'
        ]

    ];
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $labelTitle = $this->getValue($this->getParentFieldDescription()->getFieldName(), 'title');

        $labelContext = $this->getValue($this->getParentFieldDescription()->getFieldName(), 'context');
        $titleAttrDefault = [];
        $contextAttrDefault = ['maxlength' => 1500];

        $titleAttr = $this->getValue($this->getParentFieldDescription()->getFieldName(), 'title_attr') ?
            $this->getValue($this->getParentFieldDescription()->getFieldName(), 'title_attr') :
            $titleAttrDefault;
        $contextAttr = $this->getValue($this->getParentFieldDescription()->getFieldName(), 'title_attr') ?
            $this->getValue($this->getParentFieldDescription()->getFieldName(), 'context_attr') :
            $contextAttrDefault;

        $formMapper
            ->add('title', FullscreenTextareaType::class, ['label' => $labelTitle, 'attr' => $titleAttr])
            ->add('context', FullscreenTextareaType::class, ['label' => $labelContext,  'attr' => $contextAttr])
        ;
    }

    private function getValue($field, $key)
    {
        if(key_exists($field, self::LABEL_NAMES)) {
            return key_exists($key, self::LABEL_NAMES[$field]) ?
                self::LABEL_NAMES[$field][$key] : false;
        }

        return false;
    }
}
