<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContextAdmin extends AbstractAdmin
{

    const LABEL_NAMES = [
        'socialMeasures' => 'Меры',
        'nextSocialMeasures' => 'Меры',
        'practiceLessons' => 'Извлеченные уроки',
        'resultStability' => 'Меры, принятые для достижения устойчивости',
        'monitoringChanges' => 'Изменения',
        'lessons' => 'Извлеченные уроки',
        'successStories' => 'История успеха',
        'feedbackItems' => 'Прямая обратная связь'
    ];
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $label = key_exists($this->getParentFieldDescription()->getFieldName(), self::LABEL_NAMES) ?
            self::LABEL_NAMES[$this->getParentFieldDescription()->getFieldName()] : false;

        $formMapper
            ->add('context', FullscreenTextareaType::class, [
                'label' => $label, 'attr' => ['maxlength' => 1500]
            ])
        ;
    }
}
