<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MonitoringDevelopmentAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $query = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->createQueryBuilder()
            ->select('e')
            ->from('NKOOrderBundle:Report\AnalyticReport\Event', 'e')
            ->where('e.code like \'analytic_report\'')
            ->getQuery();

        $formMapper
            ->add('event', 'sonata_type_model', [
                'query' => $query,
                'label' => 'Задача / мероприятие / действие',
                'btn_add' => false
            ])
            ->add('expectedResult', FullscreenTextareaType::class, ['attr' => ['maxlength' => 1500], 'label' => 'Основные ожидаемые результаты'])
            ->add('comment', FullscreenTextareaType::class, ['attr' => ['maxlength' => 1500], 'label' => 'Комментарий к задаче / мероприятию / действию'])

        ;
    }
}
