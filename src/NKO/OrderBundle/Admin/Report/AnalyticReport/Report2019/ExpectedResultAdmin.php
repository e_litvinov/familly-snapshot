<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2019;

use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application as SecondStageApplication;
use NKO\OrderBundle\Entity\Application\Continuation\ResultDirection;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ExpectedResultAdmin extends AbstractAdmin
{
    const MODEL_FIELD_DESCRIPTION = [
        'experienceItems',
        'processItems',
        'qualificationItems',
        'resourceBlockItems'
    ];
    const CUSTOM_MODEL_FIELD_DESCRIPTION = [
        AnalyticReport2019::class => [
            'experienceItems' => ResultDirection::EXPERIENCE_TYPE,
            'processItems' => ResultDirection::PROCESS_TYPE,
            'qualificationItems' => ResultDirection::QUALIFICATION_CONTINUATION_SECOND_STAGE,
            'resourceBlockItems' => ResultDirection::RESOURCE_TYPE
        ]
    ];

    const TEXT_FIELD_DESCRIPTION = [
        'experienceEtcItems',
        'processEtcItems',
        'qualificationEtcItems',
        'resourceBlockEtcItems'
    ];

    const DESCRIPTION = [
        'direction' => 'Направления и ориентировочные задачи',
        'indicator' => 'Показатель',
        'measurementMethod' => 'Способ измерения',
        'comment' => 'Комментарий'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $fieldName = $this->getParentFieldDescription()->getFieldName();
        $parentClassName = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());

        if (in_array($fieldName, self::MODEL_FIELD_DESCRIPTION)) {
            if (key_exists($parentClassName, self::CUSTOM_MODEL_FIELD_DESCRIPTION)) {
                $fieldName = self::CUSTOM_MODEL_FIELD_DESCRIPTION[$parentClassName][$fieldName];
            }

            $formMapper
                ->add('direction', null, [
                    'required' => true,
                    'label' => self::DESCRIPTION['direction'],
                    'choices' => $this->getConfigurationPool()
                        ->getContainer()
                        ->get('doctrine')
                        ->getRepository(ResultDirection::class)
                        ->findBy(['type' => $fieldName])
                ])
            ;
        } elseif (in_array($fieldName, self::TEXT_FIELD_DESCRIPTION)) {
            $formMapper
                ->add('customDirection', FullscreenTextareaType::class, [
                    'label' => self::DESCRIPTION['direction']
                ])
            ;
        }

        $formMapper
            ->add('indicator', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['indicator'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
            ->add('measurementMethod', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['measurementMethod'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['comment'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
        ;
    }
}
