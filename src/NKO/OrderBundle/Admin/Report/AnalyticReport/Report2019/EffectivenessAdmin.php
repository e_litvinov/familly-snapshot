<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2019;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;

class EffectivenessAdmin extends EmbeddedAdminDecorator
{
    const DESCRIPTION = [
        'result' => 'Социальные результаты',
        'indicator' => 'Показатель',
        'factValue' => 'Факт',
        'planValue' => 'План',
        'measurementMethod' => 'Методы сбора данных',
        'isFeedback' => 'Собирали ли обратную связь'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();
        $competition = $this->getParentFieldDescription()->getAdmin()->getSubject()->getReportForm()->getCompetition();

        $formMapper
            ->add('result', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['result'],
                'attr' => ['maxlength' => 800]
            ])
        ;

        $formMapper
            ->add('customIndicator', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['indicator']
            ))
        ;

        $formMapper
            ->add('planValue', NumberType::class, [
                'label' => self::DESCRIPTION['planValue'],
                'required' => false,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('factValue', NumberType::class, [
                'label' => self::DESCRIPTION['factValue'],
                'required' => false,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('methods', null, [
                'required' => false,
                'multiple' => true,
                'label' => self::DESCRIPTION['measurementMethod'],
                'query_builder' => function (EntityRepository $er) use ($competition) {
                    if ($competition->getRealizationYear()->format('Y') >= 2020) {
                        $extraMethod = 'свой метод';
                    } else {
                        $extraMethod = 'свой способ';
                    }
                    return $er->createQueryBuilder('o')
                        ->andWhere('o.name != :extraMethod')
                        ->andWhere('o.type like :type')
                        ->setParameter('type', '%' . ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE . '%')
                        ->setParameter('extraMethod', $extraMethod)
                        ;
                },            ])
            ->add('customMethod', TextType::class, [
                'attr' => [
                    'placeholder' => 'Введите свой способ'
                ]
            ])
            ->add('isFeedback', ChoiceType::class, [
                'placeholder' => 'Не выбрано',
                'label' => self::DESCRIPTION['isFeedback'],
                'choices' => [
                    'label_type_yes' => 1,
                    'label_type_no' => 0,
                ]
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => 'Комментарий',
            ])
        ;
    }
}
