<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2019;

use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\AttachmentAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ProjectDescriptionAdmin;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ResourceAdmin;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\EventListener\AnalyticPublicationsListener;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ReportAdmin extends BaseReportAdmin
{
    const TAB_DESCRIPTION = [
        'projectCard' => 'КАРТОЧКА ПРОЕКТА',
        'projectRealization' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
        'spreadPractice' => 'РАСПРОСТРАНЕНИЕ И ВНЕДРЕНИЕ ПРАКТИКИ',
        'monitoring' => 'МОНИТОРИНГ И ОЦЕНКА',
        'resource' => 'РЕСУРСЫ ПРОЕКТА',
        'attachment' => 'ПРИЛОЖЕНИЯ',
    ];

    const PROJECT_CARD = [
        'accountantLabel' => '<b>Главный бухгалтер:</b>',
        'grantLabel' => '<b>Сумма пожертвования</b> (за отчетный период, руб):',

    ];

    const PROJECT_REALIZATION = [
        'priorityDirectionLabel' => '<b>2.1. Приоритетное направление Конкурса:</b>',
        'priorityDirectionEtcLabel' => '<b>Иные эффективные практики</b> в сфере профилактики социального сиротства и 
            семейного устройства детей-сирот и детей, оставшихся без попечения родителей (укажите, какие именно)',
        'purpose' => '<b>2.2 Цель проекта (в отношении реализации Практики)</b>',
        'territory' => '<b>2.3. Территория реализации практики</b>',
        'expectedAnalyticResults' => '<b>2.4. Непосредственные результаты</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'practiceAnalyticIndividualResults' => '<b>Индивидуальные показатели</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'unplannedImmediateResults' => '<b>2.4.1. Незапланированные непосредственные результаты</b>',
        'practiceChanges' => '<b>2.5. Были ли внесены изменения в Практику в рамках реализации проекта (новые элементы, 
            иные целевые группы и пр.)? Если да, то что именно было сделано? Какие выводы Вы для себя сделали?</b>',
        'baseResultPracticeRealization' => '<b>2.6. Основные выводы о реализации практики, достигнутых результатах</b>',
        'unplannedSocialResults' => '<b>2.6.1.1. Незапланированные социальные результаты</b>',
        'successStories' => '<b>2.6.2. Истории, факторы успеха</b><br><i>Кратко опишите, какие были успехи в ходе реализации 
            Практики. Какие факторы способствовали успешной реализации Практики? В случае наличия подробных историй успеха 
            (описание успешного кейса/активности; прямая речь благополучателей или сотрудников, работающих с благополучателями), 
            прикрепите их как приложение в П.6.2.1.</i>',
        'difficulties' => '<b>2.6.3. Трудности в реализации практики (в рамках проекта)</b><br><i><ul><li>Трудности или 
            непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс 
            реализации Проекта (в отношении реализации Практики); что из запланированного не удалось выполнить и почему;</li>
            <li>Пути решения проблем / способы выхода из затруднительных ситуаций или пояснения, почему их нельзя 
            решить силами вашего проекта.</li></ul></i>',
        'lessons' => '<b>2.6.4. Извлечённые уроки</b><br><i>Какие уроки вы для себя извлекли в ходе реализации Практики 
            (в т.ч. благодаря сбору обратной связи)? </i>',
        'socialResults' => '<b>2.6. Основные выводы о реализации практики, достигнутых результатах</b><br><br>
            <b>2.6.1. Социальные результаты </b>',
        'comments' => '<b>Комментарий к таблице (при необходимости):</b>'
    ];

    const SPREAD_PRACTICE = [
        'purpose' => '<b>3.1. Цель проекта (распространение и внедрение практики) </b>',
        'practiceAnalyticResults' => '<b>3.2. Непосредственные результаты</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'expectedAnalyticIndividualResults' => '<b>Индивидуальные показатели</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'unplannedImmediateResults' => '<b>3.2.1. Незапланированные непосредственные результаты</b>',
        'publications' => '<b>3.3. Публикации</b><br><br>
            <i>После заполнения таблицы загрузите сканы публикаций или укажите гиперссылки на них во вкладке ПРИЛОЖЕНИЯ в поле 6.3.</i>',
        'unplannedSocialResults' => '<b>3.4.1.1. Незапланированные социальные результаты</b>',
        'successStories' => '<b>3.4.2. Истории, факторы успеха</b><br><i>Кратко опишите, какие были успехи в ходе распространения 
            и внедрения Практики. Какие позитивные изменения произошли в работе организаций, внедривших вашу практику? 
            Какие факторы способствовали успешному внедрению Практики? В случае наличия подробных историй успеха (описание 
            успешного кейса/активности; прямая речь обучавшихся специалистов), прикрепите их как приложение в П.6.2.2.</i>',
        'difficulties' => '<b>3.4.3. Трудности в распространении и внедрении практики (в рамках проекта)</b><br><i><ul><li>Трудности 
            или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс 
            распространения и внедрения практики; что из запланированного не удалось выполнить и почему;</li>
            <li></li>Пути решения проблем / способы выхода из затруднительных ситуаций или пояснения, почему их нельзя решить 
            силами вашего проекта.</ul></i>',
        'lessons' => '<b>3.4.4. Извлечённые уроки</b><br><i>Какие уроки вы для себя извлекли в ходе распространения и внедрения 
            Практики (в т.ч. благодаря сбору обратной связи)? </i>',
        'socialResults' => '<b>3.4. Основные выводы о распространении и внедрении практики, достигнутых результатах</b><br><br>
            <b>3.4.1. Социальные результаты </b>',
            'comments' => '<b>Комментарий к таблице (при необходимости):</b>'
    ];

    const MONITORING = [
        'expectedMonitoringResults' => '<b>4.1. Цель проекта (в отношении блока мониторинга и оценки):</b>',
        'practicePurpose' => '<b>4.2. Результаты проекта по блоку «Мониторинг и оценка результатов»</b>',
        'practicePurposeHelp' => '<br><i>Пожалуйста, выберите варианты из списков по каждому блоку (см. ниже)
            и/или добавьте свои варианты </i><br>
            <b>4.2.1. Опыт (результаты)</b>',
        'experienceEtc' => '<b>Иные задачи по блоку “Опыт (результаты)”</b>',
        'process' => '<b>4.2.2. Процессы</b>',
        'processEtc' => '<b>Иные задачи по блоку “Процессы”</b>',
        'qualification' => '<b>4.2.3. Повышение уровня знаний и навыков сотрудников</b>',
        'qualificationEtc' => '<b>Иные задачи по блоку “Повышение уровня знаний и навыков сотрудников”</b>',
        'resourceBlock' => '<b>4.2.4. Ресурсы</b>',
        'resourceBlockEtc' => '<b>Иные задачи по блоку “Ресурсы”</b>',
        'newMonitoringElements' =>'<b>4.3 Какие новые элементы были добавлены в систему мониторинга и оценки вашего проекта за отчетный период?</b>',
        'monitoringResults' =>'<b>4.4. Что вы изменили / стали делать иначе в системе мониторинга и оценки вашего проекта за отчетный период?</b>',
    ];

    const RESOURCE_DESCRIPTION = [
        'partnerActivities' => '<b>5.1. Партнеры и доноры проекта</b>',
        'humanResources' => '<b>5.2. Кадровые ресурсы проекта</b><br><i>В случае отсутствия значения, введите "0"</i>',
    ];


    const ATTACHMENT_DESCRIPTION = [
        'warning' => '<p id="warning">Перед началом работы на вкладке ПРИЛОЖЕНИЯ сохраните, пожалуйста, отчет.<br> 
            Загружайте документы по одному. После добавления каждого документа – сохранитесь, дождитесь сообщения о сохранении отчета, откройте его и проверьте.</p>',
        'practiceFeedback' => '<b>6.1.1. Реализация практики – непосредственные результаты</b>',
        'practiceSocialFeedback' => '<b>6.1.2. Реализация практики – социальные результаты</b>',
        'practiceSpreadFeedback' => '<b>6.1.3. Распространение и внедрение практики – непосредственные результаты</b>',
        'practiceSocialSpreadFeedback' => '<b>6.1.4. Распространение и внедрение практики – социальные результаты</b>',
        'practiceSuccessStoryAttachments' => '<b>6.2.1. Истории успеха. Реализация практики</b><br><i>Необязательный для заполнения пункт</i>',
        'practiceSpreadSuccessStoryAttachments' => '<b>6.2. Истории успеха</b><br><br><b>6.2.2. Истории успеха. Распространение и внедрение практики</b><br><i>Необязательный для заполнения пункт</i>',
        'publicationAttachments' => '<b>6.3. Публикации в СМИ</b>',
        'factorAttachments' => '<b>6.5. Организации, внедрившие Практику</b>',
        'monitoringDevelopmentAttachments' => '<b> 6.6. Разработки по системе мониторинга и оценки организации</b>'
    ];
    const HELPS = [
        'feedbackPracticeAttachments' => '<i>Список строк формируется по строкам таблицы 2.4., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'feedbackPracticeSocialAttachments' => '<i>Список строк формируется по строкам таблицы 2.6.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'feedbackSpreadPracticeAttachments' => '<i>Список строк формируется по строкам таблицы 3.2., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'feedbackSpreadPracticeSocialAttachments' => '<i>Список строк формируется по строкам таблицы 3.4.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'publicationAttachments' => '<i>Количество строк таблицы определяется значением “Количество сообщений в СМИ, инициированных в рамках проекта. Факт” в таблице 3.3.</i>',
        'materialAttachments' => '<i>Количество строк таблицы определяется значением “Количество изданных информационных / методических материалов. Факт” в таблице 3.3.</i>',
        'practiceSpreadSuccessStoryAttachments' => false,
        'practiceSuccessStoryAttachments' => false,
        'factorAttachments' => '<i>Количество строк в зависимости от значения показателя 15.1. Факт мониторингового отчета.</i><br><i>Пожалуйста, приложите файлы, подтверждающие факты внедрения практики.</i>',
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => array("AnalyticReport-2019")
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $report = $this->getSubject();

        if ($report) {
            $grant = $em->getRepository(GrantConfig::class)
                ->findOneBy([
                    'reportForm' => $report->getReportForm(),
                    'applicationHistory' => $report->getApplicationHistory()
                ]);
            $currentDate = new \DateTime();
            $updatedAt = $report->getUpdatedAt() ? $report->getUpdatedAt()->format('d.m.Y') : $currentDate->format('d.m.Y');
            $reportFormYear = $report->getReportForm()->getYear()->format('Y');
        }

        $application = unserialize($report->getApplicationHistory()->getData());
        $projectName = $application->getProjectName();

        $parameters = AdminConfigurator::PROJECT_DESCRIPTION[get_class($this->getSubject())];

        $parameters['value'] = [
            'contract' => isset($grant) ? $grant->getContract() : null,
            'sumGrant' => isset($grant) ? $grant->getSumGrant() : null,
            'projectName' => $projectName,
            'year' => $reportFormYear,
            'updatedAt' => $updatedAt,
        ];

        $formMapper
            ->with(self::TAB_DESCRIPTION['projectCard']);
        ProjectDescriptionAdmin::configureFormFields($formMapper, $parameters, $em, self::PROJECT_CARD);
        $formMapper->end();

        $formMapper
            ->with(self::TAB_DESCRIPTION['projectRealization']);
        $this->configureFormFieldsPriorityDirection($formMapper);
        $formMapper->end();

        $formMapper
            ->with(self::TAB_DESCRIPTION['spreadPractice']);
        $this->configureFormSpreadPractice($formMapper);
        $formMapper->end();

        $formMapper
            ->with(self::TAB_DESCRIPTION['monitoring']);
        $this->configureFormMonitoringResults($formMapper);
        $formMapper->end();

        $formMapper
            ->with(self::TAB_DESCRIPTION['resource']);
        ResourceAdmin::configureFormFields($formMapper, AdminConfigurator::RESOURCE_ANALYTIC_REPORT[get_class($this->getSubject())], null, self::RESOURCE_DESCRIPTION);
        $formMapper->end();

        $formMapper
            ->with(self::TAB_DESCRIPTION['attachment']);
        $formMapper->add('warning', CustomTextType::class, [
            'help' => self::ATTACHMENT_DESCRIPTION['warning'],
        ]);
        AttachmentAdmin::configureFormFields(
            $formMapper,
            AdminConfigurator::ANALYTIC_ATTACHMENT[get_class($this->getSubject())],
            null,
            self::ATTACHMENT_DESCRIPTION,
            self::HELPS
        );
        $formMapper->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new AnalyticPublicationsListener());
    }

    private static function configureFormFieldsPriorityDirection(FormMapper $formMapper)
    {
        $description = self::PROJECT_REALIZATION;

        $formMapper
            ->add('priorityDirection', 'sonata_type_model', [
                'label' => $description['priorityDirectionLabel'],
                'required' => false,
                'expanded'=>true,
            ])
            ->add('priorityDirectionEtc', TextType::class, [
                'label' => $description['priorityDirectionEtcLabel'],
                'required' => false
            ])
            ->add('practiceImplementation.purpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['purpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('territories', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['territory'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.territory',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('expectedAnalyticResults', 'sonata_type_collection', array(
                'by_reference' => false,
                'required' => false,
                'label' => $description['expectedAnalyticResults'],
            ), array(
                'admin_code' => 'nko_order.admin.report.analytic_report.report2019.practice_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('expectedResultsComment', FullscreenTextareaType::class, [
                'label' => $description['comments'],
                'required' => false,
            ])
            ->add('practiceImplementation.unplannedImmediateResults', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['unplannedImmediateResults'],
            ])
            ->add('practiceImplementation.practiceChanges', FullscreenTextareaType::class, [
                'by_reference' => false,
                'required' => false,
                'label' => $description['practiceChanges'],
            ])
            ->add('implementationSocialResults', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['socialResults'],
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2019.effectiveness',
                'edit' => 'inline',
                'inline' => 'table',
                'delete' => 'inline',
            ])
            ->add('implementationSocialResultsComment', FullscreenTextareaType::class, [
                'label' => $description['comments'],
                'required' => false,
            ])
            ->add('practiceImplementation.unplannedSocialResults', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['unplannedSocialResults'],
            ])
            ->add('practiceImplementation.successStories', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['successStories'],
            ])
            ->add('practiceImplementation.difficulties', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['difficulties'],
            ])
            ->add('practiceImplementation.lessons', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['lessons'],
            ])
        ;
    }

    private static function configureFormSpreadPractice($formMapper)
    {
        $description = self::SPREAD_PRACTICE;
        $formMapper
            ->add('practiceIntroduction.purpose', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['purpose'],
                'attr' => array('maxlength' => 450),
            ])
            ->add('practiceAnalyticResults', 'sonata_type_collection', [
                'label' => $description['practiceAnalyticResults'],
                'required' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2019.practice_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('practiceIntroduction.unplannedImmediateResults', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['unplannedImmediateResults'],
            ])
            ->add('analyticPublications', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['publications'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                    'admin_code' => 'nko_order.admin.report.analytic_report.report2018.publication',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('introductionSocialResults', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['socialResults'],
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2019.effectiveness',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('introductionSocialResultsComment', FullscreenTextareaType::class, [
                'label' => $description['comments'],
                'required' => false,
            ])

            ->add('practiceIntroduction.unplannedSocialResults', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['unplannedSocialResults'],
            ])
            ->add('practiceIntroduction.successStories', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['successStories'],
            ])
            ->add('practiceIntroduction.difficulties', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['difficulties'],
            ])
            ->add('practiceIntroduction.lessons', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['lessons'],
            ])
        ;
    }

    private static function configureFormMonitoringResults(FormMapper $formMapper)
    {
        $formMapper
            ->add('expectedMonitoringResults', TextareaType::class, [
                'label' => self::MONITORING['expectedMonitoringResults'],
                'required' => false,
            ])
            ->add('experienceItems', 'sonata_type_collection', [
                'label' => self::MONITORING['practicePurpose'].self::MONITORING['practicePurposeHelp'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('experienceEtcItems', 'sonata_type_collection', [
                'label' => self::MONITORING['experienceEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('processItems', 'sonata_type_collection', [
                'label' => self::MONITORING['process'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('processEtcItems', 'sonata_type_collection', [
                'label' => self::MONITORING['processEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('qualificationItems', 'sonata_type_collection', [
                'label' => self::MONITORING['qualification'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('qualificationEtcItems', 'sonata_type_collection', [
                'label' => self::MONITORING['qualificationEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceBlockItems', 'sonata_type_collection', [
                'label' => self::MONITORING['resourceBlock'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceBlockEtcItems', 'sonata_type_collection', [
                'label' => self::MONITORING['resourceBlockEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.report.analytic.report2019.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('newMonitoringElements', 'sonata_type_collection', [
                'required' => false,
                'label' => self::MONITORING['newMonitoringElements'],
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.analytic_report.monitoring_element',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ])
            ->add('monitoringChanges', 'sonata_type_collection', [
                'required' => false,
                'label' => self::MONITORING['monitoringResults'],
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.analytic_report.context',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ])
            ;
    }
}
