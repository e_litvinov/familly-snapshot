<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport\Report2019;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PracticeAnalyticResultAdmin extends AbstractAdmin
{
    const ROW_SIZE = 7;
    const PRACTICE_FORMAT = [
        'practiceAnalyticResults'
    ];
    const GRANTEE = [
        'practiceAnalyticResults'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $parentFieldName = $this->getParentFieldDescription()->getName();
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $competition = $this->getParentFieldDescription()->getAdmin()->getSubject()->getReportForm()->getCompetition();

        $grantees = $em->getRepository(Grantee::class)->findGranteesByTypeData('analytic_report_2019_2');
        $formMapper
            ->add('service', FullscreenTextareaType::class, [
                'attr' => [
                    'maxlength' => 255,
                    'rows' => self::ROW_SIZE
                ],
            ], [
            ]);

        if (in_array($parentFieldName, self::PRACTICE_FORMAT)) {
            $formMapper->add('practiceFormat');
        }

        $formMapper
            ->add('targetGroup', FullscreenTextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => self::ROW_SIZE
                ],
            ])
            ->add('indicator', FullscreenTextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => self::ROW_SIZE
                ],
            ])
            ->add('servicePlanValue', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => '0'],
            ])
            ->add('serviceFactValue', TextType::class, [
                'required' => false,
                'attr' => ['placeholder' => '0'],
            ])
            ->add('methods', null, [
                'query_builder' => function (EntityRepository $er) use ($competition) {
                    if ($competition->getRealizationYear()->format('Y') >= 2020) {
                        $extraMethod = ' свой метод';
                    } else {
                        $extraMethod = 'свой способ';
                    }
                    return $er->createQueryBuilder('o')
                        ->andWhere('o.name != :extraMethod')
                        ->andWhere('o.type like :type')
                        ->setParameter('type', '%' . ReportTypes::ANALYTIC_REPORT_2018 . '%')
                        ->setParameter('extraMethod', $extraMethod)
                    ;
                },
            ])
            ->add('customMethods', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Введите свой способ'
                ]
            ])
            ->add('isFeedback', ChoiceType::class, [
                'placeholder' => 'Не выбрано',
                'choices' => ['label_type_yes' => 1, 'label_type_no' => 0],
            ]);
        if (in_array($parentFieldName, self::GRANTEE)) {
            $formMapper->add('grantee', 'sonata_type_model', [
                'btn_add' => false,
                'choices' => $grantees,
            ]);
        }
        $formMapper
            ->add('comment', FullscreenTextareaType::class, [
                'attr' => ['maxlength' => 450],
                'required' => false,

            ])
        ;
    }
}
