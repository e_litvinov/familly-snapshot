<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/13/17
 * Time: 4:30 PM
 */

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PublicationAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('indicator', TextareaType::class, ['label' => 'Индикатор', 'disabled' => true])
            ->add('plan', IntegerType::class, ['label' => 'План', 'attr' => ['placeholder' => '0']])
            ->add('fact', IntegerType::class, ['label' => 'Факт', 'attr' => ['placeholder' => '0']])

        ;
    }
}