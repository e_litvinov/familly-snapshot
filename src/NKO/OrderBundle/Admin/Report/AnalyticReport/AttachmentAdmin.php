<?php

namespace NKO\OrderBundle\Admin\Report\AnalyticReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;

class AttachmentAdmin extends AbstractAdmin
{
    const FIELDS = [
        'practiceSpreadSuccessStoryAttachments' => [
            'title' => 'Название истории успеха',
            'file' => 'Приложение в формате pdf',
            'text' => 'История успеха'
        ],
        'practiceSuccessStoryAttachments' => [
            'title' => 'Название истории успеха',
            'file' => 'Приложение в формате pdf',
            'text' => 'История успеха'
        ],
        'monitoringAttachments' => [
            'title' => 'Название разработки',
            'text' => 'Название элемента',
            'file' => 'Приложение в формате pdf, xls, doc',
        ],
        'monitoringDevelopmentAttachments' => [
            'title' => 'Название разработки',
            'text' => 'Комментарий',
            'file' => 'Приложение в формате pdf, xls, doc',
        ],
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $length = ['maxlength' => 2500];

        if($this->getParentFieldDescription()->getFieldName() == 'monitoringAttachments' || $this->getParentFieldDescription()->getFieldName() == 'monitoringDevelopmentAttachments') {
            $length = ['maxlength' => 1500];
        }

        if(array_key_exists($this->getParentFieldDescription()->getFieldName(), self::FIELDS)) {
            $formMapper
                ->add('title', FullscreenTextareaType::class, [
                    'label' => self::FIELDS[$this->getParentFieldDescription()->getFieldName()]['title'],
                ])
                 ->add('text', FullscreenTextareaType::class, [
                     'label' => self::FIELDS[$this->getParentFieldDescription()->getFieldName()]['text'],
                     'attr' => $length
                 ])
                ->add('file', FilePreviewType::class, [
                    'label' => self::FIELDS[$this->getParentFieldDescription()->getFieldName()]['file'],
                    'link_target' => '_blank'
                ]);
            if ($this->getParentFieldDescription()->getAdmin()->getSubject() instanceof AnalyticReport2018) {
                FileNameAdmin::configureFormFields($formMapper);
            }
        }
    }
}
