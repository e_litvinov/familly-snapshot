<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport;

use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Admin\FinanceSpentAdmin as BaseFinanceSpentAdmin;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\FinanceReport\FinanceSpentAdmin as FinanceSpentConfigurator;

class FinanceSpentAdmin extends BaseFinanceSpentAdmin
{
    const OPTIONS = [
        FinanceReport2018::class => [
            'periodCosts' => 'Текущие расходы',
            'summaryIncrementalCosts' => 'Расходы по всему отчетному периоду, руб.'
        ],
        FinanceReport2019::class => [
            'periodCosts' => 'Текущие расходы',
            'summaryIncrementalCosts' => 'Расходы по всему отчетному периоду, руб.'
        ],
        MixedReport::class => [
            'approvedSum' => 'Утвержденная сумма по статье, руб',
            'periodCosts' => 'Расходы отчетного периода, руб.',
        ],
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $report = $this->getParentFieldDescription()->getAdmin()->getSubject();
        $parameters = array_key_exists(get_class($report), AdminConfigurator::FINANCE_SPENT) ? AdminConfigurator::FINANCE_SPENT[get_class($report)] : null;
        $description = array_key_exists(get_class($report), self::OPTIONS) ? self::OPTIONS[get_class($report)] : null;
        FinanceSpentConfigurator::configureFormFields($formMapper, $parameters, null, $description);
    }
}
