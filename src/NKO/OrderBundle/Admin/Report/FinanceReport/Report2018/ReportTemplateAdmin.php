<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport\Report2018;

use NKO\OrderBundle\Admin\Report\BaseReportTemplateAdmin;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use NKO\OrderBundle\FinanceReportManager\FinanceSpentManager;
use NKO\UserBundle\Traits\CurrentUserTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS2018;

class ReportTemplateAdmin extends BaseReportTemplateAdmin
{
    use CurrentUserTrait;

    protected $baseRouteName = 'admin_nko_order_report_financereport_report2018_reporttemplate';
    protected $baseRoutePattern = '/nko/order/report-financereport-report2018template';

    const TEMPLATES = [
        FinanceReport2018::class => 'NKOOrderBundle:CRUD:report/finance_report/report_2018/edit_finance_report_template.html.twig',
        FinanceReport2019::class => 'NKOOrderBundle:CRUD:report/finance_report/report_2018/edit_finance_report_template.html.twig',
        MixedReportKNS2018::class => 'NKOOrderBundle:CRUD:report/mixed_report/edit_mixed_report_template.html.twig'
    ];

    /**
     * @var FinanceSpentManager $financeSpentManager
     */
    protected $financeSpentManager;

    public function __construct($code, $class, $baseControllerName, FinanceSpentManager $financeSpentManager)
    {
        $this->financeSpentManager = $financeSpentManager;
        parent::__construct($code, $class, $baseControllerName);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('reportForm.title', null, array(
                'label' => 'Текущий мастер отчета',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('author.nkoName', TextareaType::class, array(
                'label' => 'Организация',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('financeSpent', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false,
                'type_options' => array(
                    'delete' => false,
                )
            ), array(
                'admin_code' => 'nko_order.admin.report.finance_report.report_2018.finance_spent_template',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('sumGrant', TextType::class, array(
                'label' => 'Сумма гранта',
                'attr' => array(
                    'readonly' => true
                )
            ))
        ;

        if (!$this->getCurrentUser()->hasRole('ROLE_NKO')) {
            $formMapper
                ->add('isPlanAccepted', CheckboxType::class, [
                'label' => 'ПРИНЯТЬ ЗАПРАШИВАЕМЫЕ ПЛАНОВЫЕ ПОКАЗАТЕЛИ',
                'required' => false,
                ])
                ->add('isPlanDeclined', CheckboxType::class, [
                    'label' => 'ОТКЛОНИТЬ ЗАПРАШИВАЕМЫЕ ПЛАНОВЫЕ ПОКАЗАТЕЛИ',
                    'required' => false,
                ])
            ;
        }
    }

    public function getTemplate($name)
    {
        $reportClass = $this->getSubject() ? $this->getSubject()->getReportForm()->getReportClass() : null;
        switch ($name) {
            case 'edit':
                if ($reportClass) {
                    return array_key_exists($reportClass, self::TEMPLATES) ? (self::TEMPLATES[$reportClass]) : (parent::getTemplate($name));
                }
                break;
            default:
                return parent::getTemplate($name);
        }
    }

    public function preUpdate($object)
    {
        $financeSpent = $object->getFinanceSpent();

        if ($this->getCurrentUser()->hasRole('ROLE_NKO')) {
            $isEqual = $object->getIsEqualFinanceSpent();

            if (!$isEqual) {
                $object->setIsPlanAccepted(false);
                $object->setIsPlanDeclined(false);
            }
            $this->logAction('Сохранение показателей', $object);
        } else {
            if ($object->getIsPlanDeclined()) {
                $this->logAction('Отклонение показателей', $object);

                foreach ($financeSpent as $item) {
                    $item->setRequiredSum(0);
                }
            }

            if ($object->getIsPlanAccepted()) {
                $this->logAction('Принятие показателей', $object);

                foreach ($financeSpent as $item) {
                    $item->setApprovedSum($item->getRequiredSum());
                    $item->setRequiredSum(0);
                }

                $this->financeSpentManager->updateFinanceSpentByTemplate($object);
                $this->financeSpentManager->updateReportBalance($object->getReports());
                $this->financeSpentManager->updateTotalValues($object->getReports());
            }
        }

        return parent::preUpdate($object);
    }

    protected function logAction($action, BaseReportTemplate $template)
    {
        $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\ReportTemplateLogger')
            ->logAction($action, $template);
    }
}
