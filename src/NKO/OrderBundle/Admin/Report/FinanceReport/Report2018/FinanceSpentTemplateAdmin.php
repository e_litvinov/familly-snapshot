<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport\Report2018;

use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Admin\FinanceSpentAdmin as BaseFinanceSpentAdmin;

class FinanceSpentTemplateAdmin extends BaseFinanceSpentAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('expenseType', 'sonata_type_admin', [
                'label' => 'Статьи расходов',
            ], [
                'admin_code' => 'nko_order.admin.expense_type',
            ])
            ->add('approvedSum', 'number', [
                'required' => false,
                'label' => 'Утвержденная сумма по статье на весь период реализации проекта, руб',
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('requiredSum', 'number', [
                'label' => 'Запрашиваемая сумма по статье на весь период реализации проекта, руб',
                'attr' => [
                    'placeholder' => '0.00'
                ]
            ]);
    }
}
