<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport\Report2018;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ExpenseTypeAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code', HiddenType::class)
            ->add('parentCode', HiddenType::class)
            ->add('titleWithNumber', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
        ;
    }
}
