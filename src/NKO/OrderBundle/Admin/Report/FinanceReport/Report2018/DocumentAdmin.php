<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport\Report2018;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Report\FinanceReport\BaseDocumentAdmin;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Route\RouteCollection;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReport;
use NKO\OrderBundle\Utils\ReportTypes;

class DocumentAdmin extends BaseDocumentAdmin
{
    const DESCRIPTION = [
        'date' => '2. Дата',
        'expenseType' => '3. Статья расходов',
        'documentType' => '4. Тип документа',
        'file' => '5. Загрузите файл в формате .pdf, максимальный размер - 50Mb',
        'action' => '6. Действие'
    ];

    const REPORT_TYPES = [
        FinanceReport2018::class => ReportTypes::FINANCE_REPORT_2018,
        MixedReport::class => ReportTypes::MIXED_KNS_REPORT
    ];

    /**
     * @var EntityManager $em
     */
    private $em;

    public function __construct($code, $class, $baseControllerName, EntityManager $em)
    {
        $this->em = $em;
        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $expenseTypes = [];
        $period = null;

        if ($this->hasParentFieldDescription()) {
            $report = $this->getParentFieldDescription()->getAdmin()->getSubject();
            $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

            $period = $report->getPeriod();
            $reportForm = $report->getReportForm();

            $reportType = array_key_exists(get_class($report), self::REPORT_TYPES) ? self::REPORT_TYPES[get_class($report)] : null;
            $expenseTypes = $this->em->getRepository(ExpenseType::class)->findExpenseTypesByReportForm($reportForm, $reportType);
        }

        $formMapper
            ->add('date', DateType::class, [
                'label' => self::DESCRIPTION['date'],
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd.MM.yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-date-format' => 'DD.MM.YYYY',
                    'data-date-start-date' => $period ? $period->getStartDate()->format('d.m.Y') : null,
                    'data-date-end-date' => $period ? $period->getFinishDate()->format('d.m.Y') : null,
                    'maxlength' => 10
                ]
            ])
            ->add('expenseType', 'entity', [
                'label' => self::DESCRIPTION['expenseType'],
                'class' => ExpenseType::class,
                'choices' => $expenseTypes,
                'choice_label'=>'titleWithNumber',
                'multiple' => false,
                'required' => true
            ], [
                'admin_code' => 'nko_order.admin.expense_type',
            ])
            ->add('documentType', 'sonata_type_model', array(
                'label' => self::DESCRIPTION['documentType'],
                'by_reference' => false,
                'btn_add' =>false,
                'query' => $em->getRepository(TypeDocument::class)
                    ->createQueryBuilder('t')
                    ->where('t.types like :type')
                    ->setParameter('type', '%' . $reportType . '%')
            ), array('admin_code' => 'nko_order.admin.indicator'))


            ->add('file', FilePreviewType::class, [
                'label' => self::DESCRIPTION['file'],
                'data_class' => null,
                'required' => false
            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->parameters = [
            'listmapper' => [
                'type' => 'PDF',
                'fieldName' => 'file'
            ]
        ];

        $listMapper
            ->add('report.author', null, [
                'label' => 'Организация',
                'admin_code' => 'sonata.admin.nko.nko_user'
            ])
            ->add('file', 'string', array('template' => "NKOOrderBundle:Admin:preview_file_document.html.twig"))
            ->add('documentType')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('documentType', null, array( 'operator_type' => 'hidden','advanced_filter' => false))
            ->add('date', 'doctrine_orm_date_range', array('input_type' => DateType::class, 'operator_type' => 'hidden','advanced_filter' => false));
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'NKOOrderBundle:CRUD:base_list.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
}
