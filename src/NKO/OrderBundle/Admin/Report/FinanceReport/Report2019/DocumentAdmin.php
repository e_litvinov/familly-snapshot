<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 22.02.19
 * Time: 10:50
 */

namespace NKO\OrderBundle\Admin\Report\FinanceReport\Report2019;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Report\FinanceReport\BaseDocumentAdmin;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DocumentAdmin extends BaseDocumentAdmin
{
    const DESCRIPTION = [
        'list' => [
            'author' => 'Организация',
            'expenseType' => 'Статья расходов',
            'expenseName' => 'Наименование расхода',
            'documentName' => 'Документ',
            'file' => "Файл",
        ],
        'edit' => [
            'date' => '2.Дата',
            'expenseType' => '3.Статья расходов',
            'expenseName' => '4.Наименование расхода',
            'documentName' => '5.Документ',
            'file' => "6.Загрузите файл в формате .pdf, максимальный размер - 50Mb",
        ]
    ];

    const STRING_MAX_LENGTH = 255;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $expenseTypes = [];
        $period = null;

        if ($this->hasParentFieldDescription()) {
            $report = $this->getParentFieldDescription()->getAdmin()->getSubject();

            $period = $report->getPeriod();
            $reportForm = $report->getReportForm();
            $expenseTypes = $reportForm->getExpenseTypeFromExpenseTypeConfig();
        }

        $description = self::DESCRIPTION['edit'];
        $formMapper
            ->add('date', 'sonata_type_datetime_picker', [
                    'label' => $description['date'],
                    'format' => 'dd.MM.yyyy',
                    'dp_pick_time' => false,
                    'dp_default_date' => $period ? $period->getStartDate()->format('d.m.Y') : null,
                    'required' => false,
                    'dp_use_current' => false,
                    'dp_min_date' => $period ? $period->getStartDate()->format('d.m.Y') : null,
                    'dp_max_date' => $period ? $period->getFinishDate()->format('d.m.Y') : null,
            ])
            ->add('expenseType', 'entity', [
                'label' => $description['expenseType'],
                'class' => ExpenseType::class,
                'choices' => $expenseTypes,
                'multiple' => false,
                'required' => true
            ], [
                'admin_code' => 'nko_order.admin.expense_type',
            ])
            ->add('expenseName', FullscreenTextareaType::class, [
                'label' => $description['expenseName'],
                'required' => false,
                'attr' => [
                    'placeholder' => '-',
                    'maxlength' => self::STRING_MAX_LENGTH
                ]
            ])
            ->add('documentName', FullscreenTextareaType::class, [
                'label' => $description['documentName'],
                'required' => false,
                'attr' => [
                    'placeholder' => '-',
                    'maxlength' => self::STRING_MAX_LENGTH
                ]
            ])
            ->add('file', FilePreviewType::class, [
                'label' => $description['file'],
                'data_class' => null,
                'required' => false
            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->parameters = [
            'listmapper' => [
                'type' => 'PDF',
                'fieldName' => 'file'
            ]
        ];
        
        $description = self::DESCRIPTION['list'];
        $listMapper
            ->add('report.author', null, [
                'label' => $description['author'],
                'admin_code' => 'sonata.admin.nko.nko_user'
            ])
            ->add('file', 'string', [
                'template' => "NKOOrderBundle:Admin:preview_file_document.html.twig",
                'label' => $description['file']
            ])
            ->add('expenseType', null, [
                'label' => $description['expenseType'],
                'admin_code' => 'nko_order.admin.expense_type',
            ])
            ->add('expenseName', null, [
                'label' => $description['expenseName']
            ])
            ->add('documentName', null, [
                'label' => $description['documentName']
            ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
}
