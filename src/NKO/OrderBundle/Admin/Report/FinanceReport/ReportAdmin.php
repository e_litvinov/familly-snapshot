<?php


namespace NKO\OrderBundle\Admin\Report\FinanceReport;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\UserBundle\Services\CurrentUserService;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DatagridBundle\ProxyQuery\ProxyQueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ReportAdmin extends BaseReportAdmin
{
    const DESCRIPTION = [
        'registerLabel' => 'I. РЕЕСТР РАСХОДОВ',
        'financeSpentLabel' => 'II. ИЗРАСХОДОВАНО СРЕДСТВ',
        'financeMovingLabel' => 'III. ДВИЖЕНИЕ ДЕНЕЖНЫХ СРЕДСТВ',
        'attachmentLabel' => 'ПРИЛОЖЕНИЯ',
        'financeSpentTotalLabel' => '<b>ВСЕГО:</b>',
        'receiptOfDonationsLabel' => '<b>Поступление пожертвования</b>',
        'periodCostsLabel' => '<b>Расходы отчетного периода</b>',
        'incrementalCostsLabel' => '<b>Расходы по нарастающему итогу</b>',
        'donationBalanceLabel' => '<b>Остаток суммы пожертвования</b> (при наличии)',
        'documents' => '<b>Документы</b>'
    ];

    private $container;

    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        $this->container = $container;

        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array('validation_groups' => array("FinanceReport"));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with(self::DESCRIPTION['registerLabel'])
            ->add('registers', 'sonata_type_collection', [
                'btn_add' => false,
                'label' => false,
                'required' => false,
                'by_reference' => false,
                'type_options' => [
                    'delete' => false
                ]
            ], [
                'admin_code' => 'nko_order.admin.register',
            ])
            ->end()
            ->with(self::DESCRIPTION['financeSpentLabel'])
            ->add('financeSpent', 'sonata_type_collection', [
                    'required' => false,
                    'label' => false,
                    'btn_add' => false,
                    'by_reference' => false,
                    'type_options' => [
                        'delete' => false,
                    ]
                ], [
                    'admin_code' => 'nko_order.admin.report.finance_report.finance_spent',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->end()
            ->with(self::DESCRIPTION['financeMovingLabel'])
            ->add('receiptOfDonationsLabel', CustomTextType::class, [
                    'help' => self::DESCRIPTION['receiptOfDonationsLabel'],
                ])
            ->add('receiptOfDonations', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('periodCostsLabel', CustomTextType::class, [
                    'help' => self::DESCRIPTION['periodCostsLabel'],
                ])
            ->add('periodCosts', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('incrementalCostsLabel', CustomTextType::class, [
                    'help' => self::DESCRIPTION['incrementalCostsLabel'],
            ])
            ->add('incrementalCosts', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('donationBalanceLabel', CustomTextType::class, [
                    'help' => self::DESCRIPTION['donationBalanceLabel'],
            ])
            ->add('donationBalance', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->end()
            ->with('Attachment')
            ->add('documents', 'sonata_type_collection', [
                'required' => false,
                'label' => self::DESCRIPTION['documents'],
                'by_reference' => false,
                'btn_add' => "Добавить"
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'sonata.admin.nko.order.document_report',
            ])
            ->end()
        ;
        parent::configureFormFields($formMapper);
    }

    public function postPersist($object)
    {
        parent::postUpdate($object); // TODO: Change the autogenerated stub
    }

    public function postUpdate($object)
    {
        /**
         * @var Report $object
         */
        $author = $object->getAuthor();
        $period = $object->getPeriod();
        $reportForm = $object->getReportForm();



            $em = $this->container->get('doctrine')->getManager();
            $reports = $em->createQueryBuilder()
                ->select('r')
                ->from(get_class($object), 'r')
                ->innerJoin('r.period', 'p', Join::WITH, 'r.periodId = p.id')
                ->where('r.reportForm = :reportForm')
                ->andWhere('r.author = :author')
                ->andWhere('p.startDate > :currentPeriodDate')
                ->orderBy('p.startDate', 'ASC')
                ->getQuery()
                ->setParameters([
                    'reportForm' => $reportForm,
                    'author' => $author,
                    'currentPeriodDate' => $period->getStartDate()
                ])
                ->getResult()
            ;
            
            if (!$reports) {
                return parent::postUpdate($object);
            }

            $reportHistory = $em->getRepository(ReportHistory::class)->findOneBy([
                'reportForm' => $reportForm,
                'psrn' => $author->getPsrn(),
                'period' => $period
            ], [
                'id' => 'DESC'
            ]);

            if (!$reportHistory) {
                $this->updateFollowingReports($reports, $object);
                return parent::postUpdate($object);
            }

            $report = unserialize($reportHistory->getData());

            if ($this->isFinanceSpentChanged($object->getFinanceSpent(), $report->getFinanceSpent())) {
                $this->updateFollowingReports($reports, $object);
            }


        return parent::postUpdate($object);
    }

    protected function updateFollowingReports($followingReports, $report)
    {
        $em = $this->container->get('doctrine')->getManager();

        $reportForm = $report->getReportForm();
        $author = $report->getAuthor();
        $currentReport = $report;
        foreach ($followingReports as $report) {
            $history = $em->getRepository('NKOOrderBundle:ReportHistory')->findOneBy([
                'isSend' => true,
                'reportForm' => $reportForm,
                'psrn' => $author->getPsrn(),
                'period' => $report->getPeriod()
            ]);

            if ($history) {
                $report->setIsAccepted(false);
                $history->setIsAccepted(false);
                $report->setIsSend(false);
                $history->setIsSend(false);
                $report->setUnderConsideration(false);
                $history->setUnderConsideration(false);
            }

            $this->container->get('nko_order.finance_report_manager.finance_spent')->updateIncrementalCosts($currentReport, $report);
            $currentReport = $report;
        }

        $this->container->get('nko_order.finance_report_manager.finance_spent')->updateReportBalance($followingReports);

        $em->flush();
    }

    protected function isFinanceSpentChanged($financeSpent, $prevFinanceSpent)
    {
        foreach ($financeSpent as $key => $finance) {
            $equal = $this->objectsAreEqual($finance, $prevFinanceSpent[$key]);
            if (!$equal) {
                return true;
            }
        }

        return false;
    }

    protected function objectsAreEqual($object1, $object2)
    {
        $properties1 = $this->getObjectProperties($object1);

        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($properties1 as $key => $property) {
            $value = $accessor->getValue($object1, $property->getName());
            if (is_object($value)) {
                if ($value->getId() != $accessor->getValue($object2, $property->getName())->getId()) {
                    return false;
                }
            } else {
                if ($value != $accessor->getValue($object2, $property->getName())) {
                    return false;
                }
            }
        }

        return true;
    }

    protected function getObjectProperties($object)
    {
        $reflection = new \ReflectionObject($object);

        $properties = $reflection->getProperties();
        $parentReflection = $reflection->getParentClass();
        if ($parentReflection) {
            $properties = array_merge($properties, $parentReflection->getProperties());
        }

        return $properties;
    }
}
