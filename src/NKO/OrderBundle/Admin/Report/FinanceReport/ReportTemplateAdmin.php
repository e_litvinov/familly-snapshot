<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport;

use NKO\OrderBundle\Admin\Report\BaseReportTemplateAdmin;
use NKO\OrderBundle\FinanceReportManager\FinanceSpentManager;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ReportTemplateAdmin extends BaseReportTemplateAdmin
{
    /**
     * @var FinanceSpentManager $financeSpentManager
     */
    protected $financeSpentManager;

    public function __construct($code, $class, $baseControllerName, FinanceSpentManager $financeSpentManager)
    {
        $this->financeSpentManager = $financeSpentManager;
        parent::__construct($code, $class, $baseControllerName);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('reportForm.title', null, array(
                'label' => 'Текущий мастер отчета',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('author.nkoName', TextareaType::class, array(
                'label' => 'Организация',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('financeSpent', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false,
                'type_options' => array(
                    'delete' => false,
                )
            ),
            array(
                'admin_code' => 'nko_order.admin.report.finance_report.finance_spent_template',
                'edit' => 'inline',
                'inline' => 'table',
            ))
        ;
    }

    public function preUpdate($object)
    {
        $this->financeSpentManager->updateFinanceSpentByTemplate($object);

        return parent::preUpdate($object);
    }
}
