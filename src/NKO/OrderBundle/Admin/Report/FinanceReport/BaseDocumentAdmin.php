<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.02.19
 * Time: 15:40
 */

namespace NKO\OrderBundle\Admin\Report\FinanceReport;

use Sonata\AdminBundle\Admin\AbstractAdmin;

abstract class BaseDocumentAdmin extends AbstractAdmin
{
    public function createQuery($context = 'list')
    {
        $report_id = $this->getRequest()->query->get('report_id');
        $query = parent::createQuery($context);

        $query
            ->innerJoin($query->getRootAlias().'.report', 'r')
            ->where('r.id = :id')
            ->setParameter('id', $report_id)
        ;
        return $query;
    }
}
