<?php

namespace NKO\OrderBundle\Admin\Report\FinanceReport;

use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Admin\FinanceSpentAdmin as BaseFinanceSpentAdmin;

class FinanceSpentTemplateAdmin extends BaseFinanceSpentAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('expenseType', 'sonata_type_admin', array(
                'label' => 'Статьи расходов',
            ), array(
                'admin_code' => 'nko_order.admin.expense_type',
            ))
            ->add('approvedSum', 'number',
                array(
                    'required' => false,
                    'label' => 'Утвержденная сумма по статье на весь период реализации проекта, руб',
            ));
    }
}