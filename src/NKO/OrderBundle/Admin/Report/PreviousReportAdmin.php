<?php

namespace NKO\OrderBundle\Admin\Report;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class PreviousReportAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'admin_nko_order_admin_previous_report';

    protected $baseRoutePattern = 'previous_report';

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('email', null, array(
                'translation_domain' => 'NKOUserBundle'
            ))
            ->add('psrn', null, array(
                'translation_domain' => 'NKOUserBundle'
            ))
            ->add('nko_name', null, array(
                'translation_domain' => 'NKOUserBundle'
            ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nko_name', null, array(
                'translation_domain' => 'NKOUserBundle'
            ))
            ->add('psrn', null, array(
                'translation_domain' => 'NKOUserBundle'
            ))
            ->add('email', null, array(
                'translation_domain' => 'NKOUserBundle'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'add_reports' => array('template' => 'NKOOrderBundle:CRUD:list__action_add_reports.html.twig'),
                )
            ))
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('add_reports', $this->getRouterIdParameter().'/add_reports')
            ->remove('create')
        ;
    }
}
