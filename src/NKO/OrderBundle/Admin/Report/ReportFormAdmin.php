<?php

namespace NKO\OrderBundle\Admin\Report;

use NKO\OrderBundle\Traits\NestedListOptionsTrait;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ReportFormAdmin extends AbstractAdmin
{
    use NestedListOptionsTrait;

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array(
                'label' => 'Название мастера отчета'
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $types = $container->get('nko.report_type_manager')->getReportsClass();

        $formMapper
            ->add('title', TextType::class, [
                'label' => 'Название мастера отчета',
                'required' => false,
            ])
            ->add('reportClass', 'choice', array(
                'translation_domain' => 'NKOOrderBundle',
                'required' => false,
                'choices' => $types))
            ->add('year', 'sonata_type_datetime_picker', array(
                'label' => 'Год заполнения отчетности',
                'dp_language' => 'ru',
                'required' => false,
                'dp_view_mode' => 'years',
                'format' => 'yyyy',
            ))
            ->add('startDate', 'sonata_type_datetime_picker', [
                'required' => false,
                'format' => 'dd.MM.yyyy',
            ])
            ->add('finishDate', 'sonata_type_datetime_picker', array(
                'dp_language' => 'ru',
                'required' => false,
                'format' => 'dd.MM.yyyy',
            ))
            ->add('competition')
            ->add('reportPeriods')
            ->add('linkedForm')
            ->add('expenseTypeConfigs', 'sonata_type_collection', [
                'required' => false,
                'label' => 'Типы реестра расходов',
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.expense_type_config',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('isSelectable', null, [
                'label' => 'Показатели выбираются пользователем'
            ])
            ->add('isTransferable', null, [
                'label' => 'Разрешить перенос значений из предыдущего отчета'
            ])
            ->add('isFilledByExpert', null, [
                'label' => 'Запретить ввод плановых показателей организацией'
            ])
            ->add('isDataTakesFromActualApplication', null, [
                'label' => 'Брать данные в отчёт из текущей версии(не распределённой)'
            ])







        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $types = $container->get('nko.report_type_manager')->getReportsClass();
        $datagridMapper
            ->add('title', null, array(
                "label" => "Название"
            ))
            ->add('reportClass', 'doctrine_orm_callback', array(
                'callback'   => array($this, 'getWithOpenReportClassFilter')
            ), 'choice', array(
                'choices' => $types,
            ))
            ->add('competition')
            ->add('linkedForm')
        ;
    }

    public function getWithOpenReportClassFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return;
        }

        $queryBuilder
            ->andWhere($alias.'.reportClass = :reportClass')
            ->setParameter('reportClass', $value['value'])
        ;

        return true;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig'
            )
        );
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Изменение настроек отчета', $object->getTitle());
    }
}
