<?php

namespace NKO\OrderBundle\Admin\Report\MixedReport\KNS\Report2019;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ReportTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ConstResultAdmin extends AbstractAdmin
{
    const ROWS = 5;

    const TYPES = [
        MixedReport2019::class => ReportTypes::MIXED_KNS_REPORT_2019,
        MixedReport2020::class => ReportTypes::MIXED_KNS_REPORT_2020,
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $parentClass = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());
        $type = key_exists($parentClass, self::TYPES) ?
            self::TYPES[$parentClass] :
            ReportTypes::MIXED_KNS_REPORT;

        $formMapper
            ->add('linkedTypes.name', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => self::ROWS,
                    'readonly' => 'readonly',
                ]
            ])
            ->add('servicePlanValue', TextType::class, [
                'required' => false,
                'attr' =>['placeholder' => '0'],
            ])
            ->add('serviceFactValue', TextType::class, [
                'required' => false,
                'attr' =>['placeholder' => '0']
            ])
            ->add('serviceExpectedValue', TextType::class, [
                'required' => false,
                'attr' =>['placeholder' => '0']
            ])
            ->add('measurementMethod', null, [
                'required' => false,
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($type) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' .$type . '%');
                }
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'required' => false,
                'attr' => ['maxlength' => 450]
            ])
        ;
    }
}
