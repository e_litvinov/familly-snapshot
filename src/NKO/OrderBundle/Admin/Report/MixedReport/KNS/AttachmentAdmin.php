<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.8.18
 * Time: 12.04
 */

namespace NKO\OrderBundle\Admin\Report\MixedReport\KNS;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AttachmentAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'download_mixed_report_attachment_path';
    protected $baseRouteName = 'download_mixed_report_attachment_path';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('documents', 'sonata_type_collection')
            ->add('analyticAttachments', 'sonata_type_collection');
    }
}