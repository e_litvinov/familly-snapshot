<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.8.18
 * Time: 10.05
 */

namespace NKO\OrderBundle\Admin\Report\MixedReport\KNS;

use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\EventListener\DeleteRowListener;
use NKO\OrderBundle\EventListener\DocumentListener;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\AdminConfigurator\AnalyticReport\ProjectDescriptionAdmin;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Utils\Report\AdminConfigurator;
use NKO\OrderBundle\Form\DropDownListType;
use NKO\OrderBundle\Traits\ExpenseTypeTrait;

class ReportAdmin extends BaseReportAdmin
{
    use ExpenseTypeTrait;

    const DOCUMENTS_FIELD_NAME = [
        'analyticAttachments',
        'documents'
    ];

    const TAB_DESCRIPTION = [
        'projectCard' => '1. КАРТОЧКА ПРОЕКТА',
        'practice' => '2. ОПИСАНИЕ ПРОЕКТА',
        'practiceResult' => '3. РЕЗУЛЬТАТЫ ПРОЕКТА',
        'registerLabel' =>'4. РЕЕСТР РАСХОДОВ',
        'financeSpentLabel' => '5. ИЗРАСХОДОВАНО СРЕДСТВ',
        'financeMovingLabel' => '6. ДВИЖЕНИЕ ДЕНЕЖНЫХ СРЕДСТВ',
        'attachment' => '7. ПРИЛОЖЕНИЯ',
    ];

    const DESCRIPTION = [
      'purpose' => '<b>2.1 Цель проекта:</b>',
      'events' => '<b>2.2 Обучающие мероприятия, в которых приняли участие сотрудники организации в рамках проекта:</b><br><br>
           <i>Не забудьте прикрепить список участников с указанием ФИО, должности и подписями в разделе Приложения.</i>',
      'financeDocuments' => 'Приложения к финансовому отчету'
    ];

    const ORGANIZATION_INFO_DESCRIPTION = [
        'email' => 'Адрес электронной почты',
        'phoneLabel'=> '<b>Контактные даные организации</b>',
        'phoneLabelHelp' => '<label class="control-label">Телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'internationalCode' => '<label class="code">+7</label>',
        'competition' => '<b>Конкурс:</b>',
        'grantLabel' => '<b>Сумма пожертвования (за отчетный период), руб:</b>',
        'directorLabel' => '<b>Уполномоченное лицо:</b>'
    ];

    const ATTACHMENT = [
      'attachment' => '<b>Приложения к содержательному отчету</b>',
    ];

    const PRACTICE_RESULT = [
        'knowledgeUsageBase' => '<b>3. Внедрение полученных знаний и опыта в деятельность организации</b>',
        'knowledgeUsage' => '3.1. Как именно в организации используются знания и опыт, полученные в ходе проекта? Перечислите конкретные действия, 
            которые предприняты в период реализации проекта. ',
        'planOfKnowledgeUsage' => '3.2. Как в дальнейшем планируется использовать знания и опыт, полученные в рамках проекта? 
            Перечислите конкретные действия, которые планируется предпринять.',
        'pastEvents' => '<b>6. Были ли проведены все мероприятия, которые планировалось провести в отчетный период в полном объеме и в запланированные сроки?</b> 
            Если нет, то укажите, какие именно мероприятия не были проведены в срок (или в полном объеме) и почему.',
        'difficulties' => '<b>7. Извлеченные уроки</b><ul><li>Трудности или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс реализации Проекта;
            «мелочи», которые упустили, но которые оказались важны.</li><li>Пути решения проблем/способы выхода из затруднительных ситуаций. Краткий анализ, что мешало, что помогало.</li></ul>',
        'successStory' => '<b>8. Истории успеха</b>',
        'feedback' => '<b> 9. Прямая обратная связь от сотрудников организации (членов общественного объединения) и благополучателей (наиболее показательная)</b>',
        'label' => '<b>4 Результаты проекта для сотрудников организации (членов общественного объединения)</b>',
        'results' => '<b>4.1 Непосредственные результаты</b>',
        'employeesResultMixedReports' => '<b>4.2 Результаты, изменения у сотрудников организации (членов общественного объединения):</b><br> 
            Перечислите, какие ожидались качественные изменения у сотрудников организации (членов общественного объединения), 
            которые произойдут в период реализации проекта (согласно заявке). Укажите планируемые и фактически достигнутые целевые значения. 
            В случае различия целевых значений Плана/Факта, поясните в поле «Комментарий», почему так произошло.
            
            В случае, если достигнуты также иные, не указанные в заявке результаты, также их укажите.',
        'beneficiariesResultMixedReports' => '<b>5.2. Результаты, изменения у благополучателей:</b><br>
            Перечислите, какие ожидались качественные изменения у благополучателей организации, 
            которые произойдут в период реализации проекта (согласно заявке). 
            Укажите планируемые и фактически достигнутые целевые значения. 
            В случае различия целевых значений Плана/Факта, в поле «Комментарий» укажите, почему так произошло.
            
            В случае, если достигнуты также иные, не указанные в заявке результаты, то также их укажите.',
        'beneficiaryGroupsLabel' => '<b>5. Результаты для благополучателей</b><br><br> <b>5.1. Основные целевые группы благополучателей (согласно заявке)</b><br><br> 
            <b>Укажите <b>основные целевые группы благополучателей</b>, на которых повлияла реализация проекта:</b><br>',
        'otherBeneficiaryGroups' => 'Иные группы (укажите)'
    ];

    const FINANCE_MOVING_DESCRIPTION = [
        'receiptOfDonationsLabel' => 'Поступление пожертвования',
        'periodCostsLabel' => 'Расходы отчетного периода',
        'incrementalCostsLabel' => 'Остаток суммы пожертвования',
        'donationBalanceDocumentLabel' => 'Документ, подтверждающий поступление денежных средств',
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = ['validation_groups' => ["MixedReportKNS-2018"]];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();
        $report = $this->getSubject();
        ExpenseTypeTrait::addReportForm($report);

        if ($report) {
            $grant = $em->getRepository(GrantConfig::class)
                ->findOneBy([
                    'reportForm' => $report->getReportForm(),
                    'applicationHistory' => $report->getApplicationHistory()
                ]);
        }

        $application = unserialize($report->getApplicationHistory()->getData());
        $projectName = $application->getProjectName();
        $trainingGrounds = $application->getTrainingGrounds();

        if (!$trainingGrounds->isEmpty()) {
            foreach ($trainingGrounds as $trainingGround) {
                if ($trainingGround->getParent()) {
                    $trainingGroundsSecond = $trainingGround;
                    break;
                }
            }
        }

        $parameters = AdminConfigurator::PROJECT_DESCRIPTION[get_class($this->getSubject())];
        $parameters['value'] = [
            'contract' => isset($grant) ? $grant->getContract() : null,
            'sumGrant' => isset($grant) ? $grant->getSumGrant() : null,
            'trainingGroundsSecond' => isset($trainingGroundsSecond) ? $trainingGroundsSecond : null,
            'projectName' => $projectName,
        ];

        $formMapper
            ->with(self::TAB_DESCRIPTION['projectCard'])
            ->add('reportForm.competition.name', TextType::class, [
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['competition'],
                'attr' => [
                    'readonly' => true,
                ]
            ]);

        ProjectDescriptionAdmin::configureFormFields($formMapper, $parameters, null, self::ORGANIZATION_INFO_DESCRIPTION);
        $formMapper
            ->add('phoneLabel', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['phoneLabel'],
            ])
            ->add('phoneLabelHelp', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['phoneLabelHelp'],
            ])
            ->add('internationalCode', CustomTextType::class, [
                'help' => self::ORGANIZATION_INFO_DESCRIPTION['internationalCode'],
            ])
            ->add('phoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ]
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => self::ORGANIZATION_INFO_DESCRIPTION['email'],
                'required' => false,
            ])
            ->end()
            ->with(self::TAB_DESCRIPTION['practice'])
            ->add('purpose', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['purpose'],
                'required' => false,
            ])
            ->add('events', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['events'],
                'required' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.mixed_report.kns.training_event',
            ])
            ->end()
            ->with(self::TAB_DESCRIPTION['practiceResult'])
            ->add('knowledgeUsageBase', CustomTextType::class, [
                'help' => self::PRACTICE_RESULT['knowledgeUsageBase'],
            ])
            ->add('knowledgeUsage', FullscreenTextareaType::class, [
                'label' => self::PRACTICE_RESULT['knowledgeUsage'],
                'required' => false,
            ])
            ->add('planOfKnowledgeUsage', FullscreenTextareaType::class, [
                'label' => self::PRACTICE_RESULT['planOfKnowledgeUsage'],
                'required' => false,
            ])
            ->add('label', CustomTextType::class, [
                'help' => self::PRACTICE_RESULT['label'],
            ])
            ->add('mixedResults', 'sonata_type_collection', [
                'label' => self::PRACTICE_RESULT['results'],
                'by_reference' => false,
                'required' => false,
                'type_options' => [
                    'delete' => false,
                ]
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.mixed_report.kns.result_const_table',
            ])
            ->add('mixedEmployeeResults', 'sonata_type_collection', [
                'label' => self::PRACTICE_RESULT['employeesResultMixedReports'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.mixed_report.kns.result',
            ])
            ->add('beneficiaryGroupsLabel', CustomTextType::class, [
                    'help' => self::PRACTICE_RESULT['beneficiaryGroupsLabel'],
                ])
            ->add('peopleCategories', DropDownListType::class, [
                    'class' => 'NKOOrderBundle:BriefApplication2017\PeopleCategory',
                    'required' => false,
                    'label' => false,
                    'multiple' => true
                ])
            ->add('otherBeneficiaryGroups', 'sonata_type_collection', [
                    'required' => false,
                    'label' => self::PRACTICE_RESULT['otherBeneficiaryGroups'],
                    'btn_add' => "Добавить",
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table'
                ])
            ->add('mixedBeneficiariesResults', 'sonata_type_collection', [
                'label' => self::PRACTICE_RESULT['beneficiariesResultMixedReports'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.mixed_report.kns.result',
            ])
            ->add('pastEvents', FullscreenTextareaType::class, [
                'label' => self::PRACTICE_RESULT['pastEvents'],
                'required' => false,
            ])
            ->add('difficulties', FullscreenTextareaType::class, [
                'label' => self::PRACTICE_RESULT['difficulties'],
                'required' => false,
            ])
            ->add('successStory', FullscreenTextareaType::class, [
                'label' => self::PRACTICE_RESULT['successStory'],
                'required' => false,
            ])
            ->add('feedback', FullscreenTextareaType::class, [
                'label' => self::PRACTICE_RESULT['feedback'],
                'required' => false,
            ])
            ->end()
            ->with(self::TAB_DESCRIPTION['registerLabel'])
            ->add('registers', 'sonata_type_collection', [
                'label' => false,
                'required' => false,
                'by_reference' => false,
                'type_options' => [
                    'delete' => false
                ]
            ], [
                'admin_code' => 'nko_order.admin.mixed_report.register',
            ])
            ->end()
            ->with(self::TAB_DESCRIPTION['financeSpentLabel'])
            ->add('financeSpent', 'sonata_type_collection', [
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false,
                'type_options' => [
                    'delete' => false,
                ]
            ], [
                'admin_code' => 'nko_order.admin.report.finance_report.finance_spent',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->end()

            ->with(self::TAB_DESCRIPTION['financeMovingLabel'])

            ->add('receiptOfDonations', TextType::class, array(
                'label' => self::FINANCE_MOVING_DESCRIPTION['receiptOfDonationsLabel'],
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('periodCosts', TextType::class, array(
                'label' => self::FINANCE_MOVING_DESCRIPTION['periodCostsLabel'],
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('incrementalCosts', TextType::class, [
                'label' => self::FINANCE_MOVING_DESCRIPTION['incrementalCostsLabel'],
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('donationBalanceDocument', TextType::class, [
                'label' => self::FINANCE_MOVING_DESCRIPTION['donationBalanceDocumentLabel'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255,
                ]
            ])
            ->end()
            ->with(self::TAB_DESCRIPTION['attachment'])
            ->add('documents', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['financeDocuments'],
                'required' => false,
                'by_reference' => false,
                'btn_add' => true
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                ])
            ->add('analyticAttachments', 'sonata_type_collection', [
                'label' => self::ATTACHMENT['attachment'],
                'required' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.mixed_report.kns.attachment',
            ])
            ->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new DocumentListener(self::DOCUMENTS_FIELD_NAME));
    }
}
