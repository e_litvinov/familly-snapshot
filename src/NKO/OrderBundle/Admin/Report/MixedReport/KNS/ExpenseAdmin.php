<?php

namespace NKO\OrderBundle\Admin\Report\MixedReport\KNS;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ExpenseAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'proofProductExpenseDocument' => 'Тип документа',
        'sum' => 'Сумма, руб',
        'name' => 'Реквизиты документа'
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('proofProductExpenseDocument', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => self::DESCRIPTION['proofProductExpenseDocument'],
                ])
            ->add('name', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => self::DESCRIPTION['name'],
                ])
            ->add('sum', NumberType::class, [
                    'required' => false,
                    'label' => self::DESCRIPTION['sum'],
                ])
        ;
    }
}
