<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 20.8.18
 * Time: 14.33
 */

namespace NKO\OrderBundle\Admin\Report\MixedReport\KNS;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TrainingEventAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'eventName' => 'Название мероприятия',
        'time' => 'Сроки',
        'place' => 'Место проведения (с указанием города и организации)',
        'count_member' => 'Число участников',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('eventName', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['eventName'],
            ])
            ->add('time', TextType::class, [
                'label' => self::DESCRIPTION['time'],
            ])
            ->add('place', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['place'],
            ])
            ->add('count_member', TextType::class, [
                'label' => self::DESCRIPTION['count_member'],
            ])
            ;
    }
}
