<?php

namespace NKO\OrderBundle\Admin\Report\MixedReport\KNS;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegisterAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('expenseType', 'sonata_type_admin', [
                'label' => false,
            ], ['admin_code' => 'nko_order.admin.finance_report.expense_type'])

            ->add('expenses', 'sonata_type_collection', [
                'required' => false,
                'btn_add' => false,
                'label' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.mixed_report.expense',
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('totalExpenseValue', TextType::class, [
                'required' => false,
                'label' => '<b>ИТОГО</b>',
                'attr' => [
                    'readonly' => true
                ]
            ]);
    }
}
