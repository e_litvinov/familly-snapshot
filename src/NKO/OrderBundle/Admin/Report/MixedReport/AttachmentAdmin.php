<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 22.8.18
 * Time: 15.24
 */

namespace NKO\OrderBundle\Admin\Report\MixedReport;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class AttachmentAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
      'content' => 'Название приложения',
      'file' => 'Приложение в формате pdf, doc, xls',
    ];
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('content', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['content'],
            ])
            ->add('file', FilePreviewType::class, [
                'link_target' => '_blank',
                'label' => self::DESCRIPTION['file']
            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }
}
