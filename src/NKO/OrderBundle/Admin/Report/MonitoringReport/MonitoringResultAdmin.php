<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use NKO\OrderBundle\Entity\Report\MonitoringReport\Method;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use NKO\OrderBundle\Form\PeriodResultType;

class MonitoringResultAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'indicator' => 'Показатель',
        'totalValue' => 'Итого за весь период реализации практики',
        'methods' => 'Метод сбора данных (выбор из вариантов )',
        'customMethod' => 'Метод сбора данных (добавить свой)',
        'comment' => 'Комментарии'
    ];

    const TEXT_MAX_LENGTH = 1024;
    const STRING_MAX_LENGTH = 256;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $result = $this->getSubject() ?: null;
        $id = $this->getSubject() ? $result->getId() : null;
        $report = $this->getParentFieldDescription()->getAdmin()->getSubject();
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        $methods = $em->getRepository(Method::class)
            ->createQueryBuilder('m')
            ->getQuery()
            ->useResultCache(true)
            ->getResult();

        $formMapper
            ->add('id', HiddenType::class, [
                'data' => $id,
                'mapped' => false
                ])
            ->add('indicator', 'sonata_type_admin', [
                'label' => 'Показатель',
                ], [
                'admin_code' => 'nko_order.admin.indicator'
                ])
            ->add('periodResults', CollectionType::class, [
                'required' => false,
                'entry_type' => PeriodResultType::class,
                'entry_options' => ['label' => false],
                ], [
                    'admin_code' => 'nko_order.admin.period_result'
                ])
            ->add('prevTotalValue', HiddenType::class, [
                'mapped' => false,
                'data' => $result ? $this->getPrevReportValues($report, $result, $em) : null
                ])
            ->add('totalValue', TextType::class, [
                'label' => 'Итого за весь период реализации проекта',
                'attr' => [
                    'readonly' => true,
                    'placeholder' => '0'
                ]
            ]);

        if ($report && $report->getReportForm()->getIsSelectable()) {
            $formMapper->add('linkedMethods', 'sonata_type_model', array(
                'choices' => $methods,
                'by_reference' => false,
                'multiple' => true
            ));
        } else {
            $formMapper
                ->add('method', 'sonata_type_model', array(
                    'label' => self::DESCRIPTION['methods'],
                    'choices' => $methods,
                    'by_reference' => false,
                    'required' => false,
                    'multiple' => false
                ));
        }

        $formMapper
            ->add('customMethod', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION['customMethod'],
                'attr' => array(
                    'placeholder' => '-',
                    'maxlength' => self::STRING_MAX_LENGTH
                )
            ))
            ->add('comment', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION['comment'],
                'attr' => array(
                    'placeholder' => '-',
                    'maxlength' => self::TEXT_MAX_LENGTH
                )
            ))
        ;
    }

    private function getPrevReportValues($report, $result, $em)
    {
        if ($report->getReportForm()->getIsTransferable()) {
            $prevReport = $em->getRepository(MonitoringReport::class)->findPrevReport($report->getAuthor(), $report->getReportForm());

            if ($prevReport) {
                $prevResult = $prevReport->getMonitoringResults()->filter(function ($object) use ($result) {
                    return $object->getIndicator()->getId() == $result->getIndicator()->getId();
                })->first();
            }
        }

        return (isset($prevResult) && $prevResult) ? $prevResult->getTotalValue() : null;
    }
}
