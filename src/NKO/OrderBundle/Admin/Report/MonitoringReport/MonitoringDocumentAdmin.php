<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use Doctrine\ORM\EntityRepository;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\UserBundle\Entity\NKOUser;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use NKO\OrderBundle\AdminConfigurator\FileNameAdmin;

class MonitoringDocumentAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'author' => 'Организация',
        'file' => 'Документ',
        'period' => 'Период',
        'indicator' => 'Показатель',
        'parent_indicator' => 'Родительский показатель',
    ];
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        if ($this->getCurrentUser() instanceof NKOUser) {
            $datagridMapper
                ->add('report', null, [
                    'label' => 'Организация',
                    'operator_type' => 'hidden',
                    'advanced_filter' => false,
                    'admin_code' => 'nko_order.admin.report.monitoring_report.report',
                ], 'entity', [
                    'class' => BaseReport::class,

                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.psrn = :report')
                            ->setParameters(array('report' => $this->getCurrentUser()->getPsrn()));
                    }
                ])
                ->add('harbor2020Report', null, [
                    'label' => 'Организация',
                    'operator_type' => 'hidden',
                    'advanced_filter' => false,
                    'admin_code' => 'nko_order.admin.report.monitoring_report.report',
                ], 'entity', [
                    'class' => BaseReport::class,

                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.psrn = :report')
                            ->setParameters(array('report' => $this->getCurrentUser()->getPsrn()));
                    }
                ])
            ;
        } else {
            $datagridMapper
                ->add('report', null, [
                    'label' => 'Организация',
                    'operator_type' => 'hidden',
                    'advanced_filter' => false,
                    'admin_code' => 'nko_order.admin.report.monitoring_report.report',
                    'show_filter' => false
                ])
                ->add('harbor2020Report', null, [
                    'label' => 'Организация',
                    'operator_type' => 'hidden',
                    'advanced_filter' => false,
                    'admin_code' => 'nko_order.admin.report.monitoring_report.report',
                    'show_filter' => false
                ])
            ;
        }

        $datagridMapper
            ->add('period', null, [
                'advanced_filter' => false,
            ], null, ['choices' => $this->getPeriods()])
            ->add('monitoringResult.indicator', null, [
                'advanced_filter' => false,
            ], null, ['choices' => $this->getIndicators()])
        ;
    }

    private function getPeriods()
    {
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $parameters = $this->getFilterParameters();

        if (array_key_exists('report', $parameters) && $parameters['report']['value']) {
            $report = $em->getRepository('NKOOrderBundle:BaseReport')->find($parameters['report']['value']);

            return $report->getReportForm()->getReportPeriods();
        }

        if (array_key_exists('harbor2020Report', $parameters) && $parameters['harbor2020Report']['value']) {
            $report = $em->getRepository('NKOOrderBundle:BaseReport')->find($parameters['harbor2020Report']['value']);

            return $report->getReportForm()->getReportPeriods();
        }

        return $em->getRepository(PeriodReport::class)->findAll();
    }

    private function getIndicators()
    {
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $indicators = $em->getRepository(Indicator::class)
            ->createQueryBuilder('i')
            ->where('i.parent is not null')
            ->andWhere("i.code != 'custom'")
            ->orWhere("i.code is null")
            ->getQuery()
            ->getResult();

        return $indicators;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $this->parameters = [
            'listmapper' => [
                'fieldName' => 'file'
            ]
        ];

        $listMapper
            ->add('id')
            ->add('report.author.nkoName', null, [
                'label' => self::DESCRIPTION['author'],
                'admin_code' => 'sonata.admin.nko.nko_user'
            ])
            ->add('file', 'string', [
                'label' => self::DESCRIPTION['file'],
                'template' => "NKOOrderBundle:Admin:preview_file_document.html.twig"
            ])
            ->add('period.name', null, [
                'label' => self::DESCRIPTION['period'],
            ])
            ->add('monitoringResult.indicator.title', null, [
                'label' => self::DESCRIPTION['indicator'],
            ])
            ->add('monitoringResult.indicator.parent.title', null, [
                'label' => self::DESCRIPTION['parent_indicator'],
            ])
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $report = $this->getParentFieldDescription()->getAdmin()->getSubject();

        $formMapper
            ->add('monitoringResult', null, array(
                'label' => 'Показатель',
                'required' => false,
                'query_builder' => function (EntityRepository $er) use ($report) {
                    if ($report->getId()) {
                        return $er->createQueryBuilder('r')
                            ->where('r.report = ' . $report->getId());
                    }
                }
            ), array(
                'admin_code' => 'nko_order.admin.monitoring_result'
            ))
            ->add('period', null, array(
                'multiple' => false,
                'class' => PeriodReport::class,
                'query_builder' => function (EntityRepository $er) use ($report) {
                        return $er->createQueryBuilder('p')
                            ->join('p.reportForms', 'r', 'WITH', 'r.id = ' . $report->getReportForm()->getId())
                            ;
                }, ))

            ->add('file', FilePreviewType::class, [
                'label' => 'Файл',
                'link_target' => '_blank',

            ]);

        FileNameAdmin::configureFormFields($formMapper);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('file')
        ;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('download_report_document', $this->getRouterIdParameter().'/download_report_document')
        ;
    }

    private function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }
}
