<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport\Harbor2020Report;

use NKO\OrderBundle\Admin\Report\MonitoringReport\AcceptPlanAdmin as BaseAcceptPlanAdmin;

class AcceptPlanAdmin extends BaseAcceptPlanAdmin
{
    protected $baseRouteName = 'admin_nko_order_admin_report_monitoring_report_harbor_2020_accept_plan';
    protected $baseRoutePattern = 'accept_plan_harbor_2020';
}
