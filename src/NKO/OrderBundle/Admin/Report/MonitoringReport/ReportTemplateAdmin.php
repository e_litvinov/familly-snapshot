<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use NKO\OrderBundle\Admin\Report\BaseReportTemplateAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ReportTemplateAdmin extends BaseReportTemplateAdmin
{
    const DESCRIPTION = [
        'reportFormTitle' => 'Текущий мастер отчета',
        'competitionName' => 'Форма мониторинга по проектам, поддержанным в рамках конкурса',
        'nkoName' => 'Организация',
        'projectName' => 'Проект'
    ];
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('reportForm.title', null, array(
                'label' => self::DESCRIPTION['reportFormTitle'],
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('reportForm.competition.name', null, array(
                'label' => self::DESCRIPTION['competitionName'],
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('author.nkoName', TextareaType::class, array(
                'label' => self::DESCRIPTION['nkoName'],
                'required' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('applicationHistory.projectName', TextareaType::class, array(
                'label' => self::DESCRIPTION['projectName'],
                'required' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('monitoringResults', 'sonata_type_collection', array(
                'by_reference' => false,
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'type_options' => array(
                    'delete' => false
                )
            ), array (
                'admin_code' => 'nko_order.admin.monitoring_result_template',
                'edit' => 'inline',
                'inline' => 'table'
            ))
        ;
    }

    public function preUpdate($object)
    {
        $template = $object;

        $reports = $template->getReports();

        if (!$reports) {
            return;
        }

        $monitoringResults = $template->getMonitoringResults();

        foreach ($reports as $report) {
            foreach ($report->getMonitoringResults() as $result) {
                $indicator = $result->getIndicator();
                $monitoringResult = $monitoringResults->filter(function ($object) use ($indicator) {
                    return $indicator->getId() == $object->getIndicator()->getId();
                })->first();

                if ($monitoringResult) {
                    $periodResult = $monitoringResult->getPeriodResults()->filter(function ($object) {
                       return $object->isFinalResult();
                    })->first();

                    $reportPeriodResult = $result->getPeriodResults()->filter(function ($object) {
                        return $object->isFinalResult();
                    })->first();

                    if ($reportPeriodResult) {
                        $reportPeriodResult->setTotalAmount($periodResult->getTotalAmount());
                        $reportPeriodResult->setNewAmount($periodResult->getNewAmount());
                    }    
                }
            }
        }
    }
}
