<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class AcceptPlanAdmin extends AbstractAdmin
{
    const LABEL = 'Плановые показатели организации';
    protected $baseRouteName = 'admin_nko_order_admin_report_monitoring_report_accept_plan';
    protected $baseRoutePattern = 'accept_plan';

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = ['validation_groups' => ["accept_value"]];
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with(self::LABEL)
            ->add('monitoringResults', 'sonata_type_collection', array(
                'by_reference' => false,
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'attr' => [
                    'readonly' => true,
                    'placeholder' => '0',
                ],
                'type_options' => array(
                    'delete' => false
                ),
            ), array (
                'admin_code' => 'nko_order.admin.monitoring_result_template',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
