<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class PeriodResultAcceptAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('totalAmount', 'number', [
                'label' => false,
                'attr' => [
                    'readonly' => true,
                    'placeholder' => '0'
                ]
            ]);
    }
}
