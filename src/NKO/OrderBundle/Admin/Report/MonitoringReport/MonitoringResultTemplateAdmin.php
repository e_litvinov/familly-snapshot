<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MonitoringResultTemplateAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $rootValue = false;
        $result = $this->getSubject();
        if ($result) {
            if (!$result->getIndicator()->getParent()) {
                $rootValue = true;
            }
        }

        $adminCode = $this->getParentFieldDescription()->getAdmin()->getCode() == 'nko_order.admin.accept_plan' ?
            'nko_order.admin.period_result_accept' : 'nko_order.admin.period_result';

        $formMapper
            ->add('indicator', 'sonata_type_admin', array(
                'label' => 'Показатель',
            ), array (
                'admin_code' => 'nko_order.admin.indicator'
            ))
        ;

        if (!$rootValue) {
            $formMapper
                ->add('periodResults', 'sonata_type_collection', array(
                    'label' => 'Итого за год. План',
                    'by_reference' => false,
                    'required' => false,
                    'btn_add' => false,
                    'type_options' => array(
                        'delete' => false
                    )
                ), array(
                    'admin_code' => $adminCode,
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ;
        }
    }
}
