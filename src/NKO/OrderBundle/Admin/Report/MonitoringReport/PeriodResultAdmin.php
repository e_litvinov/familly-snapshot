<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PeriodResultAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'totalAmount' => 'Всего',
        'newAmount' => 'В т.ч. новых',
    ];

    const FINAL_DESCRIPTION = [
        'newAmount' => 'Факт',
        'totalAmount' => 'План',
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $labels = self::FINAL_DESCRIPTION;
        $result = $this->getSubject();
        if ($result) {
            if ($result->isFinalResult()) {
                $labels = self::FINAL_DESCRIPTION;
            }
            else {
                $labels = self::DESCRIPTION;
                $formMapper->add('period.id', HiddenType::class);
            }
        }

        $formMapper
            ->add('totalAmount', 'number', array(
                'label' => $labels['totalAmount'],
                'attr' => array(
                    'placeholder' => '0'
                )
            ))
            ->add('newAmount', 'number', array(
                'label' => $labels['newAmount'],
                'attr' => array(
                    'readonly' => true,
                    'placeholder' => '0'
                )
            ))
        ;
    }
}
