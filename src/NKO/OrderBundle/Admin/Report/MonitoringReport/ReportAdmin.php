<?php


namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use NKO\OrderBundle\EventListener\DocumentListener;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;

class ReportAdmin extends BaseReportAdmin
{
    const DESCRIPTION = [
        'managerTab' => 'УПРАВЛЕНИЕ ОТЧЕТОМ',
        'reportTab' => 'ОТЧЁТ',
        'documentsTab' => 'ПРИЛОЖЕНИЯ'
    ];

    const DOCUMENTS_FIELD_NAME = ['documents'];
    const FILE_FIELD_NAME = ['file'];

    private $container;

    private $isSelectable;

    public function __construct($code, $class, $baseControllerName, ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct($code, $class, $baseControllerName);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $object = $this->getSubject();
        $em = $this->container->get('doctrine')->getManager();
        $reportPeriods = $object->getReportForm()->getReportPeriods();
        $this->isSelectable = $object && $object->getReportForm()->getIsSelectable() ?: null;
        $monitoringResults = $em->getRepository(MonitoringResult::class)->findMonitoringResults($object->getId());

        if ($this->isSelectable) {
            $filterResults = $monitoringResults->filter(function ($result) {
                $indicator = $result->getIndicator();
                if ($indicator->getCode() !== IndicatorInterface::CUSTOM_VALUE &&
                    $indicator->getParentCode() !== IndicatorInterface::INDIVIDUAL_RES_CODE &&
                    $indicator->getCode() !== IndicatorInterface::TOTAL_CODE &&
                    $indicator->getParentCode() !== IndicatorInterface::TOTAL_CODE) {
                    return true;
                }
                return false;
            });

            $formMapper
                ->with(self::DESCRIPTION['managerTab'])
                ->add('periods', null, [
                    'label' => 'Выберите периоды с нулевыми показателями. <br> <i><b>Нельзя скрыть период, по которому было введено значение.<b/></i>',
                    'choices' => $reportPeriods,
                    'expanded' => true
                ])
                ->add('visibility', 'sonata_type_collection', [
                    'required' => false,
                    'label' => 'Отметьте галочками показатели, которые необходимо скрыть на форме отчета<br><i><b>Нельзя скрыть основной показатель, 
                        если не скрыты все уточняющие показатели по нему.<br> Нельзя скрыть показатель, по которому введено значение. 
                        Нельзя скрыть показатель, по которому принят план.</b></i>',
                    'mapped' => false,
                    'data' => $filterResults
                ], array(
                    'admin_code' => 'nko_order.admin.monitoring_result_visibility',
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
                ->end();
        }

        $formMapper
            ->with(self::DESCRIPTION['reportTab'])
            ->add('isPlanAccepted', HiddenType::class)
            ->add('monitoringResults', 'sonata_type_collection', array(
                'label' => false,
                'required' => false,
                'data' => $monitoringResults,
                'type_options' => array(
                    'delete' => false,
                )
            ), array(
                'admin_code' => 'nko_order.admin.monitoring_result',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->end()
            ->with(self::DESCRIPTION['documentsTab'])
            ->add('documents', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'by_reference' => false
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->end();

        $formMapper->getFormBuilder()->addEventSubscriber(new DocumentListener(self::DOCUMENTS_FIELD_NAME, self::FILE_FIELD_NAME));
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                if ($this->isSelectable) {
                    return 'NKOOrderBundle:CRUD:report/monitoring_report/edit_improved.html.twig';
                }
                return 'NKOOrderBundle:CRUD:report/monitoring_report/edit.html.twig';
            default:
                return parent::getTemplate($name);
        }
    }

    public function preUpdate($object)
    {
        $analyticReports = $this->container->get('doctrine')->getRepository(BaseReport::class)
            ->findReportsByLinkedReportForm($object);

        foreach ($analyticReports as $report) {
            $this->container->get('NKO\OrderBundle\Loader\Loader')->update($report, $object);
        }

        $reports = $this->getSubject()->getAuthor()->getReports();

        $currentReportForm = $this->getSubject()->getReportForm();
        $report = $reports->filter(function ($report) use ($currentReportForm) {
            $reportForm = $report->getReportForm();
            if ($reportForm->getReportClass() == AnalyticReport::class
                && $reportForm->getYear() == $currentReportForm->getYear()) {
                return true;
            }
        })->first();

        if (!$report) {
            return parent::preUpdate($object);
        }

        $this->getConfigurationPool()
            ->getContainer()
            ->get('nko_order.loader.monitoring_results')
            ->load($this->getSubject()->getMonitoringResults(), $report);
    }
}
