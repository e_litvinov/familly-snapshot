<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use NKO\OrderBundle\Form\CustomTextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class IndicatorAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', HiddenType::class)
            ->add('parentCode', HiddenType::class)
            ->add('parentId', HiddenType::class)
            ->add('code', HiddenType::class)
            ->add('title', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
        ;
    }
}
