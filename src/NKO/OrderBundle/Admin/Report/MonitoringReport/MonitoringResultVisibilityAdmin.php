<?php

namespace NKO\OrderBundle\Admin\Report\MonitoringReport;

use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;

class MonitoringResultVisibilityAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'indicator' => 'Показатель',
    ];

    const TEXT_MAX_LENGTH = 1024;
    const STRING_MAX_LENGTH = 256;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $id = null;
        if ($this->getSubject()) {
            $id = $this->getSubject()->getId();
        }
        $formMapper
            ->add('id', HiddenType::class, array(
                'data' => $id,
                'mapped' => false
            ))
            ->add('indicator', 'sonata_type_admin', array(
                'label' => self::DESCRIPTION['indicator'],
            ), array (
                'admin_code' => 'nko_order.admin.indicator'
            ))
            ->add('isVisible')
        ;
    }
}
