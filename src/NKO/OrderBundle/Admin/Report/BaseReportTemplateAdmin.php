<?php

namespace NKO\OrderBundle\Admin\Report;

use NKO\UserBundle\Traits\CurrentUserTrait;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;

class BaseReportTemplateAdmin extends AbstractAdmin
{
    use CurrentUserTrait;

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('reportForm.title', null, [
                'label' => 'Название мастера отчета',
            ])
            ->add('reportForm.competition', null, [
                'label' => 'Мастер заявки',
            ])
            ->add('author.nko_name', null, [
                'label' => 'Название организации',
            ])
            ->add('author.psrn', null, [
                'label' => 'ОГРН',
            ])
            ->add('isChanged', null, array(
                'editable' => false
            ))
            ->add('_action', null, [
                'actions' => array(
                    'edit' => [
                        'template' => 'NKOOrderBundle:CRUD:list__action_edit_report_template.html.twig'
                    ],
                )
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        if (!$this->getCurrentUser()->hasRole('ROLE_NKO')) {
            $datagridMapper
                ->add('author.psrn')
                ->add('author.username')
                ->add('isChanged')
                ->add('author', null, [], null, [], [
                    'admin_code' => 'sonata.admin.nko.nko_user'
                ]);
        }
        $datagridMapper
            ->add('reportForm');
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $user = $this->getCurrentUser();
        $query
            ->innerJoin($query->getRootAlias().'.reportForm', 'rf')
            ->andWhere(':current_date between rf.startDate and rf.finishDate')
            ->andWhere('rf.reportClass IN (:reportClass)')
            ->setParameters(['current_date'=> new \DateTime(), 'reportClass' => [
                FinanceReport2018::class,
                FinanceReport2019::class,
                MixedReportKNS::class,
                MixedReport2019::class,
                MixedReport2020::class,
            ]]);

        if ($user->hasRole('ROLE_NKO')) {
            $query
                ->andWhere($query->getRootAlias() . '.author = :user')
                ->setParameter('user', $user);
        }

        return $query;
    }

    public function postUpdate($object)
    {
        $cache = $this->getConfigurationPool()->getContainer()->get('nko_order.resolver.cache_resolver');
        $user = $this->getCurrentUser();
        if (!$user->hasRole('ROLE_NKO')) {
            $reports = $object->getReports();
            foreach ($reports as $report) {
                $admin = $this->getConfigurationPool()->getAdminByClass(get_class($report));
                $cache->generateItem($admin, $report->getId(), $report->getAuthor()->getId());
                $cache->delete();
            }
        }
    }
}
