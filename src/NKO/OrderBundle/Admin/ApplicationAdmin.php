<?php

namespace NKO\OrderBundle\Admin;

use NKO\OrderBundle\Entity\Organization;
use NKO\OrderBundle\Form\CustomTextType;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Sonata\AdminBundle\Route\RouteCollection;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\AdminConfigurator\BankDetailsAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\AdminConfigurator\OrganizationActivityAdmin;

class ApplicationAdmin extends BaseApplicationAdmin
{
    const KNS2016_ASSOCIATION_ENTITY_NAME = 'KNS-2016';
    const COMMON_KNS_ASSOCIATION_ENTITY_NAME = 'KNS';
    const COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME = 'application';

    public function __construct($code, $class, $baseControllerName)
    {
        $this->formOptions = array(
            'validation_groups' => [
                "KNS-2016",
                "AdditionBudget",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        );

        parent::__construct($code, $class, $baseControllerName);
        $this->description = $this->description + [
                'countEmployeesLabel' => '*Количество <b>штатных и привлеченных сотрудников</b> организации на дату подачи заявки',
                'deadLineLabel' => '*Сроки реализации проекта (не ранее 01 ноября 2016 и не позднее 15 декабря 2016)',
                'primaryActionInfoLabel' => '<b>*Информация об основном обучающем мероприятии</b>',
                'employeeLabel' => '<p><b>Сотрудники организации</b> (члены общественного объединения), которые непосредственно примут участие в обучающих мероприятиях (стажировке).</p>
<p>Укажите, ФИО конкретных сотрудников, которые примут участие в стажировке (обучающих мероприятиях), их роль в организации, выполняемые функции, образование и опыт. В графе "Обучающие мероприятия" укажите те мероприятия, в которых непосредственно примет участие сотрудник.</p>',
                'knowledgeIntroductionLabel' => '<p>Как именно в организации будут использоваться знания и опыт, полученные в ходе проекта? Перечислите конкретные действия, которые будут предприняты в период реализации проекта, а также после его окончания. 
</p><p>В срок до 01.06.2017 Фонд проведет мониторинг проектов, поддержанных в рамках Конкурса. По итогам мониторинга, организации, успешно реализовавшие свои проекты и продемонстрировавшие конкретные результаты внедрения полученных знаний и навыков, смогут принять участие во втором (закрытом) этапе Конкурса и получить дополнительную финансовую поддержку на дальнейшее развитие своего проекта в 2017 - 2018 гг.</p>',
                'actionLabel' => '<b>*План-график мероприятий проекта</b>',
                'projectResultLabel' => '<b>Ожидаемые результаты проекта</b>',
                'resultLabel' => '<b>*Качественные изменения у благополучателей и сотрудников организации (членов общественного объединения)</b>',
                'knowledgeImplementationLabel' => '<b>*Внедрение полученных знаний и опыта</b>',
            ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($competition_id = $this->getRequest()->get('competition')) {
            $competition = $this->getConfigurationPool()->getContainer()
                ->get('Doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->find($competition_id)
            ;

            $isCurrentCompetition = $this->getConfigurationPool()->getContainer()
                ->get('Doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->isCurrentCompetition($competition);

            if ($isCurrentCompetition) {
                $this->getSubject()->setCompetition($competition);
            }
        }

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper
            ->with('Application Info');
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], $em);
        OrganizationActivityAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())]);
        $formMapper
            ->end()
            ->with('Banking Details');
        BankDetailsAdmin::configureFormFields($formMapper, AdminConfigurator::BANK_DETAILS[get_class($this->getSubject())]);
        $formMapper
            ->end()
            ->with('Project Description')
            ->add('projectName', TextType::class, [
                    'required' => false,
                    'label' => '*Название проекта',
                    'attr' => array('maxlength' => 450),
            ])
            ->add('deadLineLabel', CustomTextType::class, [
                    'help' => $this->description['deadLineLabel'],
            ])
            ->add('deadLineStart', DateType::class, [
                    'required' => false,
                    'label' => 'с',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',

                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2016',
                        'data-date-end-date' => '15-12-2016',
                        'maxlength' => 10
                    ]
            ])
            ->add('deadLineFinish', DateType::class, [
                    'required' => false,
                    'label' => 'по',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',

                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2016',
                        'data-date-end-date' => '15-12-2016',
                        'maxlength' => 10
                    ]
            ])
            ->add('projectPurpose', TextareaType::class, [
                    'required' => false,
                    'label' => '<b>*Цель</b> проекта',
                    'attr' => array('maxlength' => 450),
            ])
            ->add('projectRelevance', TextareaType::class, [
                    'required' => false,
                    'label' => '<b>*Обоснование актуальности и важности</b> проекта',
                    'attr' => array('maxlength' => 1000),
            ])
            ->add('beneficiaryGroups', 'sonata_type_model', [
                    'required' => false,
                    'label' => '*Укажите <b>основные целевые группы благополучателей</b>, на которых повлияет реализация проекта',
                    "property" => "groupName",
                    'expanded' => true,
                    'multiple' => true,
            ])
            ->add('beneficiaryGroupName', TextType::class, [
                    'required' => false,
                    'label' => 'иные группы (укажите, какие именно)',
            ])
            ->add('childrenCategories', 'sonata_type_model', [
                    'required' => false,
                    'label' => 'Укажите, если проект ориентирован <b>преимущественно</b> на детей 
                        «сложных» для семейного устройства категорий:',
                    "property" => "categoryName",
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $this->getChildrenCategoriesBy([
                            self::KNS2016_ASSOCIATION_ENTITY_NAME,
                            self::COMMON_KNS_ASSOCIATION_ENTITY_NAME,
                            self::COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME
                        ])
            ])
            ->add('childrenCategoryName', TextType::class, [
                    'required' => false,
                    'label' => 'иные группы (укажите, какие именно)',
            ])
            ->add('projectImplementation', TextareaType::class, [
                    'required' => false,
                    'label' => '<b>*Механизм реализации проекта</b>',
                    'attr' => array('maxlength' => 1500),
            ])
            ->add('primaryActionInfoLabel', CustomTextType::class, [
                    'help' => $this->description['primaryActionInfoLabel'],
            ])
            ->add('traineeshipTopic', 'sonata_type_model', [
                    'required' => false,
                    "property" => "topicName",
                    'label' => '<b>Тематика стажировки</b> (обучающего мероприятия)',
                    'expanded' => true,
            ]);

        $topicId = ($this->getSubject()
            && $this->getSubject()->getId()
            && $this->getSubject()->getTraineeshipTopic())
            ? $this->getSubject()->getTraineeshipTopic()->getId() : null;
        $formMapper
            ->add('organization', 'sonata_type_model', [
                    'query' => $this->getConfigurationPool()->getContainer()
                        ->get('Doctrine')->getRepository('NKOOrderBundle:Organization')->createQueryBuilder('p')
                        ->where('p.traineeshipTopicId = :id')
                        ->setParameter('id', $topicId)
                        ->getQuery(),
                    'property' => 'organizationName',
                    'expanded' => true,
                    'required' => false,
                    'btn_add' => false
            ])
            ->add('dateStartOfInternship', DateType::class, [
                    'required' => false,
                    'label' => '<b>Дата начала проведения стажировки</b> (обучающего мероприятия) (не ранее 01.11.2016, не позднее 15.12.2016)',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',

                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2016',
                        'data-date-end-date' => '15-12-2016',
                        'maxlength' => 10
                    ]
            ])
            ->add('traineeshipFormats', 'sonata_type_model', [
                    'required' => false,
                    'label' => '<b>Формат стажировки</b> (обучающего мероприятия)',
                    "property" => "formatName",
                    'expanded' => true,
                    'multiple' => true
            ])
            ->add('traineeshipFormatName', TextType::class, [
                    'required' => false,
                    'label' => 'другое (укажите)',
            ])
            ->add('employeeLabel', CustomTextType::class, [
                    'help' => $this->description['employeeLabel'],
            ])
            ->add('employees', 'sonata_type_collection', [
                    'required' => false,
                    'label' => ' ',
                    'btn_add' => "Добавить"
            ], [
                    'admin_code' => 'sonata.admin.nko.order.employee',
                    'edit' => 'inline',
                    'inline' => 'table',
            ])
            ->add('knowledgeImplementationLabel', CustomTextType::class, [
                    'help' => $this->description['knowledgeImplementationLabel'],
            ])
            ->add('knowledgeIntroductionLabel', CustomTextType::class, [
                    'help' => $this->description['knowledgeIntroductionLabel'],
            ])
            ->add('knowledgeIntroductionDuringProjectImplementation', TextareaType::class, [
                    'required' => false,
                    'label' => 'В период реализации проекта(до  15.12.2016)',
                    'attr' => array('maxlength' => 1500),
            ])
            ->add('knowledgeIntroductionAfterProjectImplementation', TextareaType::class, [
                    'required' => false,
                    'label' => 'После окончания реализации проекта(до 01.06.2017)',
                    'attr' => array('maxlength' => 1500),
            ])
            ->add('actionLabel', CustomTextType::class, [
                    'help' => $this->description['actionLabel'],
            ])
            ->add('traineeships', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>СТАЖИРОВКА (обучающее мероприятие)</b>',
                    'btn_add' => "Добавить"
            ], [
                    'admin_code' => 'sonata.admin.nko.order.traineeship',
                    'edit' => 'inline',
                    'inline' => 'table',
            ])
            ->add('measures', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>МЕРОПРИЯТИЯ ПО ВНЕДРЕНИЮ ПОЛУЧЕННЫХ ЗНАНИЙ И ОПЫТА</b>',
                    'btn_add' => "Добавить"
            ], [
                    'admin_code' => 'sonata.admin.nko.order.measure',
                    'edit' => 'inline',
                    'inline' => 'table',
            ])
            ->add('projectResultLabel', CustomTextType::class, [
                    'help' => $this->description['projectResultLabel'],
            ])
            ->add('projectResults', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '*Укажите, <b>какие результаты 
                        будут получены благодаря реализации проекта, и как вы об этом узнаете.</b>',
                    'type_options' => array('delete' => false)
                ], [
                    'admin_code' => 'sonata.admin.nko.order.project_result',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('resultLabel', CustomTextType::class, [
                    'help' => $this->description['resultLabel'],
            ])
            ->add('beneficiaryResults', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>РЕЗУЛЬТАТЫ ДЛЯ БЛАГОПОЛУЧАТЕЛЕЙ</b>',
                    'btn_add' => "Добавить"
            ], [
                    'admin_code' => 'sonata.admin.nko.order.beneficiary_result',
                    'edit' => 'inline',
                    'inline' => 'table',
            ])
            ->add('employeeResults', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>РЕЗУЛЬТАТЫ ДЛЯ СОТРУДНИКОВ ОРГАНИЗАЦИЙ / ЧЛЕНОВ ОБЩЕСТВЕННОГО ОБЪЕДИНЕНИЯ</b>',
                    'btn_add' => "Добавить"
                ], [
                    'admin_code' => 'sonata.admin.nko.order.employee_result',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('risks', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>*Риски</b>. Что может препятствовать внедрению результатов проекта?',
                    'btn_add' => "Добавить"
            ], [
                    'admin_code' => 'sonata.admin.nko.order.risk',
                    'edit' => 'inline',
                    'inline' => 'table',
            ])
            ->add('requiredMoney', MoneyType::class, [
                    'currency' => 'RUB',
                    'scale' => 2,
                    'required' => false,
            ])
            ->end()
            ->with('ДОКУМЕНТЫ К ЗАЯВКЕ  ')
            ->add('regulation', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false
            ])
            ->add('organizationCreationResolution', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false
            ])
            ->add('budget', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false
            ])
            ->end();
            $admin = $this;
        $formMapper->getFormBuilder()->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) use ($formMapper, $admin) {
                $form = $event->getForm();
                if ($form->has('organization')) {
                    $form->remove('organization');
                }

                $data = $event->getData();

                $organizations = $admin->getConfigurationPool()->getContainer()
                    ->get('Doctrine')->getRepository('NKOOrderBundle:Organization')->createQueryBuilder('p')
                    ->where('p.traineeshipTopicId = :id')
                    ->setParameter('id', $data['traineeshipTopic'])
                    ->getQuery()
                    ->getResult();

                $form->add('organization', EntityType::class, [
                    'class' => Organization::class,
                    'choices' => $organizations,
                    'expanded' => true,
                    'required' => false,
                ]);
            }
        );
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('return')
            ->remove('list')
            ->add('send', $this->getRouterIdParameter().'/send')
        ;
        parent::configureRoutes($collection);
    }

    public function prePersist($application)
    {
        $application->setSendVersion('0');
        $this->preUpdate($application);
    }

    public function preUpdate($application)
    {
        $application->setPublications($application->getPublications());
        $application->setProjects($application->getProjects());
        $application->setSiteLinks($application->getSiteLinks());
        $application->setSocialNetworkLinks($application->getSocialNetworkLinks());
        $application->setEmployees($application->getEmployees());
        $application->setTraineeships($application->getTraineeships());
        $application->setMeasures($application->getMeasures());
        $application->setBeneficiaryResult($application->getBeneficiaryResults());
        $application->setEmployeeResults($application->getEmployeeResults());
        $application->setRisks($application->getRisks());
        $application->setProjectResults($application->getProjectResults());
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig')
        );
    }

    /**
     * @param string $action
     * @param mixed  $object
     *
     * @return array
     */
    public function getActionButtons($action, $object = null)
    {
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $countCompetitions = $em
            ->getRepository("NKOOrderBundle:Competition")
            ->findCountAvailiableUserCompetition($user);

        $list = $this->configureActionButtons($action, $object);

        foreach ($this->getExtensions() as $extension) {
            // TODO: remove method check in next major release
            if (method_exists($extension, 'configureActionButtons')) {
                $list = $extension->configureActionButtons($this, $list, $action, $object);
            }
        }
        foreach ($list as $key => &$value) {
            if ($key == 'create' && !$countCompetitions) {
                unset($list[$key]);
            }
            if ($key == 'list') {
                foreach ($value as $k => &$v) {
                    if ($k =='template') {
                        $v = 'NKOOrderBundle:Button:list_button.html.twig';
                    }
                }
            }
            if ($key == 'create') {
                foreach ($value as $k => &$v) {
                    if ($k =='template') {
                        $v = 'NKOOrderBundle:Button:create_button.html.twig';
                    }
                }
            }
        }

        return $list;
    }

    public function getChildrenCategoriesBy($associationEntityNames)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:ChildrenCategory')
            ->findBy(
                array('type' => $associationEntityNames)
            );
        return $directions;
    }
}
