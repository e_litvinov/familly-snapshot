<?php

namespace NKO\OrderBundle\Admin\Decorator;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Component\Form\FormBuilderInterface;
use NKO\OrderBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\FormMapper as BaseFormMapper;
use NKO\OrderBundle\Form\WrappedFormMapper;
use Doctrine\ORM\Query;

abstract class EmbeddedAdminDecorator extends AbstractAdmin
{
    private $fields = [];
    private $isEmbeddedAdmin = true;
    protected $sortedData;

    public function defineFormBuilder(FormBuilderInterface $formBuilder)
    {
        $mapper = new FormMapper($this->getFormContractor(), $formBuilder, $this);
        $this->sort();
        $this->configureFormFields($mapper);
        $this->buildFormMapper(new BaseFormMapper($this->getFormContractor(), $formBuilder, $this));
        $this->attachInlineValidator();
    }

    public function buildFormMapper(BaseFormMapper $formMapper)
    {
        $this->changeForm();

        foreach ($this->fields['default'] as $key => $field) {
            $field['options'] = $this->getResultForChoices($field['options']);
            $formMapper->add($key, $field['type'], $field['options'], $field['additional']);
        }
    }

    protected function changeForm()
    {
        $formHandler = FormHandler::getInstance();
        $reflection = $formHandler->getReflection();
        $configs = $formHandler->getConfigs($this->getParentFieldDescription()->getFieldName());
        $wrappedFormMapper = new WrappedFormMapper($this->fields, ['default'], $this, $reflection);

        if (isset($configs)) {
            foreach ($configs as $key => $config) {
                foreach ($config as $fieldDescription) {
                    $wrappedFormMapper->$key(...$fieldDescription);
                }
            }
        }

        $wrappedFormMapper->end();
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function addField($name, array $description)
    {
        $this->fields['default'][$name] = $description;
    }

    public function getIsEmbeddedAdmin()
    {
        return $this->isEmbeddedAdmin;
    }

    public function getResultForChoices($options)
    {
        $choices = array_key_exists('choices', $options) ? $options['choices'] : null;
        if ($choices instanceof Query) {
            $options['choices'] = $choices->getResult();
        }

        return $options;
    }

    protected function sort(array $items = [])
    {
        if (!$items) {
            return;
        }

        $unitsGenerator = $this->getConfigurationPool()->getContainer()->get('training_ground_units_generator');
        foreach ($items as $key => $item) {
            $baseGround = $unitsGenerator->generate($item);
            $this->sortedData[$key] = $baseGround->generate();
        }
    }
}
