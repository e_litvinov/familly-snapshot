<?php

namespace NKO\OrderBundle\Admin\Decorator;

class FormHandler
{
    private static $instance = null;
    private $values = [];
    private $subject;
    private $table;
    private $reflection;

    private function __construct($subject, $reflection)
    {
        $this->subject = $subject;
        $this->reflection = $reflection;
    }

    public static function getInstance($subject = null, $reflection = null)
    {
        if (!self::$instance) {
            self::$instance = new self($subject, $reflection);
            return self::$instance;
        }

        if ($subject && self::$instance->subject !== $subject) {
            self::$instance->clear($subject);
        }

        return self::$instance;
    }

    public function with($table)
    {
        $this->table = $table;

        return $this;
    }

    public function __call($method, $arguments)
    {
        $this->values[$this->table][$method][] = $arguments;

        return $this;
    }

    public function getConfigs($table)
    {
        return isset($this->values[$table]) ? $this->values[$table] : null;
    }

    public function getReflection()
    {
        return $this->reflection;
    }

    public function clear($subject = null)
    {
        self::$instance->subject = $subject;
        self::$instance->reflection = null;
        self::$instance->values = [];
    }
}
