<?php

namespace NKO\OrderBundle\Admin\Decorator;

use NKO\OrderBundle\Form\WrappedFormMapper;
use Sonata\AdminBundle\Form\FormMapper as BaseFormMapper;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Builder\FormContractorInterface;
use Symfony\Component\Form\FormBuilderInterface;

class WrappedFormMapperAdapter extends BaseFormMapper
{
    private $wrappedFormMapper;

    public function __construct(FormContractorInterface $formContractor, FormBuilderInterface $formBuilder, AdminInterface $admin, WrappedFormMapper $wrappedFormMapper)
    {
        parent::__construct($formContractor, $formBuilder, $admin);
        $this->wrappedFormMapper = $wrappedFormMapper;
    }

    public function add($name, $type = null, array $options = [], array $fieldDescriptionOptions = [])
    {
        $this->wrappedFormMapper->add($name, null, $type, $options, $fieldDescriptionOptions);
        return $this;
    }

    public function remove($name)
    {
        $this->wrappedFormMapper->remove($name);
        return $this;
    }
}
