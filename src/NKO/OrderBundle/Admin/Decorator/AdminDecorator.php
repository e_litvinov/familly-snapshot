<?php

namespace NKO\OrderBundle\Admin\Decorator;

use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use Symfony\Component\Form\FormBuilderInterface;
use NKO\OrderBundle\Form\FormMapper;
use NKO\OrderBundle\Form\WrappedFormMapper;
use Sonata\AdminBundle\Form\FormMapper as BaseFormMapper;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use Doctrine\ORM\Query;

abstract class AdminDecorator extends BaseApplicationAdmin
{
    private $fields = [];
    private $tabs = [];
    private $currentGroup;
    private $relatedAdmin;
    private $isEmbeddedAdmin = null;
    private $wrappedFormMapper = null;
    private $relatedObject = null;
    private $formBuilder = null;
    private $wrappedFormMapperAdapter = null;
    protected $description = [];
    protected $tabDescription = [];
    protected $em;
    protected $sortedData;

    public function __construct($code, $class, $baseControllerName, $admin = null, $em = null)
    {
        $this->relatedAdmin = $admin;
        $this->em = $em;
        if ($this->relatedAdmin) {
            $this->relatedAdmin->setIsEmbeddedAdmin(true);
        }

        parent::__construct($code, $class, $baseControllerName);
    }

    public function defineFormBuilder(FormBuilderInterface $formBuilder)
    {
        if (method_exists($this, 'changeEmbeddedAdmin')) {
            $this->formBuilder = $formBuilder;
            $this->changeEmbeddedAdmin(FormHandler::getInstance($this->getSubject(), $this->getConstants()));
        }

        $mapper = new FormMapper($this->getFormContractor(), $formBuilder, $this);
        $this->configureFormFields($mapper);

        foreach ($this->getExtensions() as $extension) {
            $extension->configureFormFields($mapper);
        }

        $this->attachInlineValidator();
    }

    public function buildFormMapper(BaseFormMapper $formMapper)
    {
        foreach ($this->tabs as $key => $tab) {
            if (array_key_exists($key, $this->fields)) {
                $formMapper->with($tab);

                foreach ($this->fields[$key] as $item => $field) {
                    $formMapper->add($item, $field['type'], $field['options'], $field['additional']);
                }

                $formMapper->end();
            }
        }
    }

    public function getWrappedFormMapper()
    {
        if ($this->relatedAdmin && !$this->wrappedFormMapper) {
            $this->getRelatedAdmin()->setRelatedObject($this->getSubject());
            $this->relatedAdmin->buildForm();
            $this->wrappedFormMapper = new WrappedFormMapper(
                $this->relatedAdmin->getFields(),
                $this->relatedAdmin->getTabs(),
                $this,
                $this->getConstants()
            );
        }

        return $this->wrappedFormMapper;
    }

    public function setGroup($groupName)
    {
        if ($groupName !== 'default' && !array_key_exists($groupName, $this->fields)) {
            $index = array_search($groupName, static::TABS);

            if ($index) {
                $this->tabs[$index] = $groupName;
            }

            $this->fields[$index] = [];
            $this->currentGroup = $index;
        }
    }

    public function getFinalStructure()
    {
        $this->configureEmbeddedFormFields($this->getWrappedFormMapper());
        return $this->fields;
    }

    public function getRelatedAdmin()
    {
        return $this->relatedAdmin;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function addField($name, array $description)
    {
        if ($this->fields) {
            $this->fields[$this->currentGroup][$name] = $description;
        }
    }

    public function getIsEmbeddedAdmin()
    {
        return $this->isEmbeddedAdmin;
    }

    public function setIsEmbeddedAdmin($isEmbeddedAdmin = null)
    {
        $this->isEmbeddedAdmin = $isEmbeddedAdmin;
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
    }

    public function getRelatedObject()
    {
        return $this->relatedObject;
    }

    public function setRelatedObject($relatedObject)
    {
        $this->relatedObject = $relatedObject;
    }

    public function getTabs()
    {
        return $this->tabs;
    }

    public function setTabs($tabs)
    {
        $this->tabs = $tabs;
    }

    public function getConstants()
    {
        $className = (new \ReflectionClass(static::class))->getNamespaceName().'\Description';
        $reflection =  (new \ReflectionClass($className));
        $this->description = $reflection->hasConstant('CURRENT_DESCRIPTIONS') ?  $reflection->getConstant('CURRENT_DESCRIPTIONS') : null;
        $this->tabDescription = $reflection->hasConstant('CURRENT_TAB_DESCRIPTIONS') ?  $reflection->getConstant('CURRENT_TAB_DESCRIPTIONS') : null;

        return $reflection;
    }

    /**
     * Метод вызывается автоматически при использовании декоратора
     */
    protected function configureFormFields(\Sonata\AdminBundle\Form\FormMapper $formMapper)
    {
        $wrappedFormMapper = $this->getWrappedFormMapper();
        $this->sort();
        $this->configureEmbeddedFormFields($wrappedFormMapper);
        $wrappedFormMapper->end();
        $this->buildFormMapper($formMapper);
    }

    /**
     * ЧТОБЫ ИМЕТЬ РЕДАКТИРОВАНИЕ ПОЛЕЙ КОНФИГУРАТОРА В КОНТЕКСТЕ НОВОЙ ЗАЯВКИ, ЭТОТ МЕТОД ВЫЗЫВАТЬ В САМОМ НАЧАЛЕ!
     * А так же должен быть объявлен  метод changeEmbeddedAdmin
     *
     * @param $tab, Ключ-имя вкладки (не description!)
     * @param $class, Имя класса-конфигуратора
     * @param array, $options параметры
     * @param array, $description описание полей
     */
    protected function useConfigurator($tab, $class, array $options = [], $description = [])
    {
        if (!$this->wrappedFormMapperAdapter) {
            $this->wrappedFormMapperAdapter = new WrappedFormMapperAdapter($this->getFormContractor(), $this->formBuilder, $this, $this->getWrappedFormMapper());
        }

        if (!$tab || !$class) {
            throw new \RuntimeException("Function's options is not correct");
        }

        if ($this->wrappedFormMapper->issetTab($tab)) {
            $this->wrappedFormMapper->setCurrentTab($tab);
        } else {
            $this->wrappedFormMapper->addTab($tab, 'defaultName');
        }

        if ((new \ReflectionClass($class))->implementsInterface(AdminConfiguratorInterface::class)) {
            call_user_func_array([$class, 'configureFormFields'], [$this->wrappedFormMapperAdapter, $options, $this->em, $description]);
        }
    }

    protected function sort(array $items = [])
    {
        if (!$items) {
            return;
        }

        $unitsGenerator = $this->getConfigurationPool()->getContainer()->get('training_ground_units_generator');
        foreach ($items as $key => $item) {
            $baseGround = $unitsGenerator->generate($item);
            $this->sortedData[$key] = $baseGround->generate();
        }
    }
}
