<?php

namespace NKO\OrderBundle\Admin\MarkListSequence;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Entity\QuestionMark;

class SubSectionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $questions = [];
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $criteriaAdmin = $this->getParentFieldDescription()->getAdmin();

        if ($criteriaAdmin->getSubject()) {
            $criteriaId = $criteriaAdmin->getSubject()->getCriteria()->getId();
            $subject = $criteriaAdmin->getParentFieldDescription()->getAdmin()->getSubject();
            $competitionId = $subject->getCompetition()->getId();
            $questions = $em->getRepository(QuestionMark::class)->findByCompetitionAndCriteria($competitionId, $criteriaId);
        }

        $formMapper
            ->add('question', 'sonata_type_model', array(
                'required' => false,
                'choices' => $questions,
                'btn_add' => false,
                'label' => false,
            ))
        ;
    }
}
