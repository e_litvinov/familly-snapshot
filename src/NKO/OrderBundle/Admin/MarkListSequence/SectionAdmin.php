<?php

namespace NKO\OrderBundle\Admin\MarkListSequence;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Entity\MarkCriteria;

class SectionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $subject = $this->getParentFieldDescription()->getAdmin()->getSubject();
        $competition = $subject->getCompetition();
        $criterias =$em->getRepository(MarkCriteria::class)->findByCompetition($competition->getId());

        $formMapper
            ->add('criteria', 'sonata_type_model', array(
                'required' => false,
                'btn_add' => false,
                'choices' => $criterias,
                'label' => 'Критерий',
            ));

        if ($this->getParentFieldDescription()->getFieldName() === 'doubleSections') {
            $formMapper
                ->add('subSections', 'sonata_type_collection', [
                    'required' => false,
                    'label' => 'Вопросы',
                    'by_reference' => false,
                    'btn_add' => false,
                    'type_options' => [
                        'delete' => false
                    ]
                ], [
                    'admin_code' => 'nko_order.admin.mark_list_sequence.subsection',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ;
        }
    }
}
