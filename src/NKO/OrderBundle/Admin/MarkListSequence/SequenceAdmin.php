<?php

namespace NKO\OrderBundle\Admin\MarkListSequence;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Entity\MarkListSequence\Sequence;
use NKO\OrderBundle\Entity\QuestionMark;

class SequenceAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'competition' => 'Конкурс:',
        'sections' => '<b>Расставьте критерии в нужном порядке:<b/>',
        'doubleSections' => '<b>Расставьте вопросы в нужном порядке:<b/>',
        'competitionInfo' => '<i><b>После выбора конкурса НЕОБХОДИМО ПРОИЗВЕСТИ СОХРАНЕНИЕ!<b/></i>',
        'relatedCompetitions' => 'Дополнительные конкурсы:',
    ];

    private $em;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $subject = $this->getSubject();
        $competition = $subject->getCompetition();
        $this->em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();

        $formMapper
            ->add('competition', 'sonata_type_model', [
                'choices' => $this->getCompetitions($this->getAvailableCompetitions()),
                'btn_add' =>false,
                'required' => false
            ]);

        if (!$competition) {
            $formMapper
                ->add('competitionInfo', CustomTextType::class, [
                    'help' => self::DESCRIPTION['competitionInfo'],
                ]);
        } else {
            $formMapper
                ->add('relatedCompetitions', EntityType::class, [
                    'class' => Competition::class,
                    'label' => self::DESCRIPTION['relatedCompetitions'],
                    'required' => false,
                    'choices' => $this->getCompetitions($this->availableRelatedCompetitions($competition, $this->getAvailableCompetitions())),
                    'multiple' => true,
                ])
                ->add('sectionLabel', CustomTextType::class, [
                    'help' => self::DESCRIPTION['sections'],
                ])
                ->add('sections', 'sonata_type_collection', [
                    'required' => false,
                    'btn_add' => true,
                    'label' => false,
                    'by_reference' => false,
                    'btn_add' => false,
                    'type_options' => [
                        'delete' => false
                    ]], [
                    'admin_code' => 'nko_order.admin.mark_list_sequence.section',
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
                ->add('doubleSectionsLabel', CustomTextType::class, [
                    'help' => self::DESCRIPTION['doubleSections'],
                ])
                ->add('doubleSections', 'sonata_type_collection', [
                    'required' => false,
                    'btn_add' => "Добавить",
                    'label' => false,
                    'by_reference' => false,
                    'btn_add' => false,
                    'type_options' => [
                        'delete' => false
                    ]], [
                    'admin_code' => 'nko_order.admin.mark_list_sequence.section',
                    'edit' => 'inline',
                    'inline' => 'table',
                ]);
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('usesCompetitions')
            ->add('_action', null, [
                'actions' => [
                    'delete'=> [],
                    'edit' => []
                ]
            ]);
    }

    /**
     * здесь получаются доступные конкурсы, которые не задействованы в других последовательностях
     * + отбрасываются конкурсы, по которым не установлены вопросы
     */
    protected function getAvailableCompetitions()
    {
        $selectedCompetitions = [];
        $sequences = $this->em->getRepository(Sequence::class)->findAllWithoutCurrent($this->getSubject()->getId());
        foreach ($sequences as $sequence) {
            $this->generateUniqueCompetitions($selectedCompetitions, $sequence->getCompetition());
            $additional = $sequence->getRelatedCompetitions();
            foreach ($additional as $item) {
                $this->generateUniqueCompetitions($selectedCompetitions, $item);
            }
        }
        $competitionsWithQuestions = array_column($this->em->getRepository(Competition::class)->getCompetitionsByMarkQuestions(), 'id');

        return array_diff($competitionsWithQuestions, $selectedCompetitions);
    }

    /**
     * Получаются конкурсы, которые еще не задействованы в других последовательностях
     * + у них схожие вопросы с текущим конкурсом
     */
    protected function availableRelatedCompetitions($competition, $competitions)
    {
        $available = [];
        $repository = $this->em->getRepository(QuestionMark::class);
        $baseQuestions = $repository->findByCompetition($competition);

        foreach ($competitions as $item) {

            if ($item === $competition->getId()) {
                continue;
            }

            $questionsCur = $repository->findByCompetition($item);
            if (!array_diff($baseQuestions, $questionsCur)) {
                $available[] = $item;
            }
        }

        return $available;
    }

    protected function generateUniqueCompetitions(&$arr, $competition)
    {
        if ($competition && !in_array($competition->getId(), $arr)) {
            $arr[] = $competition->getId();
        }
    }

    /**
     * Получение коллекции объектов для choices
     */
    protected function getCompetitions($ids)
    {
        return $this->em->getRepository(Competition::class)->getCompetitionsByIds($ids);
    }
}
