<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 17.08.16
 * Time: 4:56
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProjectResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('resultCriteria',  TextareaType::class,
                array(
                    'label' => 'Результат',
                    'disabled' => true,
                ))
            ->add('targetValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Целевое значение, на дату окончания проекта (к 25.12.2016)',
                ))
            ->add('approximateTargetValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Ориентировочное целевое значение (на 01.06.2017)',
                ))
            ->add('methodMeasurement', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Метод измерения',
                ));
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }
}