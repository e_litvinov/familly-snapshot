<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 21.11.18
 * Time: 10.42
 */

namespace NKO\OrderBundle\Admin\Diagrams;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class DiagramAdmin extends AbstractAdmin
{
    protected $baseRouteName = "admin_nko_order_diagram";
    protected $baseRoutePattern = "/nko/order/diagram";

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        $collection->remove('show');
        $collection->remove('batch');
        $collection->remove('edit');
        $collection->add('get_winners', '/admin/nko/order/diagram/get_winners');
    }
}
