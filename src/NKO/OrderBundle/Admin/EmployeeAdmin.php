<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 16.08.16
 * Time: 9:32
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EmployeeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('fullName', TextType::class,
                array(
                    'label' => 'ФИО',
                    'required' => false,
                    'attr' => array('maxlength' => 255)
                ))
            ->add('position', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Должность и выполняемые функции в организации',
                ))
            ->add('experienceAndEducation', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Опыт и образование',
                ))
            ->add('educationalActivity', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Обучающие мероприятия',
                ))
        ;
    }
}