<?php


namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ImmediateResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('resultCriteria', TextareaType::class,
                array(
                    'required' => false,
                    'disabled' => true,
                    'label' => 'Результат'
            ))
            ->add('planValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'План (на дату окончания проекта, согласно заявке)'
            ))
            ->add('factValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Факт (на дату окончания проекта)'
                ))
            ->add('expectedValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Ожидается (на 01.06.2017)'
                ))
            ->add('methodMeasurement', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Способ измерения'
                ))
            ->add('comment', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий'
                ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$listMapper

    }

}