<?php

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\NumberType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FinanceSpentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('cost_item', TextType::class, array(
                    'disabled' => true,
                    'label' => 'Статьи расходов и их наименование'
                ), array (
                    'admin_code' => 'nko_order.admin.expense_type'
                ))
            ->add('approvedSum', 'number', array(
                    'required' => false,
                    'label' => 'Утвержденная сумма по статье, руб.',
                ))
            ->add('periodCosts', 'number', array(
                    'label' => 'Расходы отчетного периода, руб.',
                    'attr' => array(
                        'readonly' => true
                    )
                ))
            ->add('balance', 'number', array(
                    'label' => 'Баланс (остаток / перерасход), руб.',
                    'attr' => array(
                        'readonly' => true
                    )
                ))
        ;
    }
}
