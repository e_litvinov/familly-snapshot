<?php

namespace NKO\OrderBundle\Admin;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\DocumentType;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Traits\NestedListOptionsTrait;
use NKO\UserBundle\Entity\NKOUser;
use phpDocumentor\Reflection\Types\Parent_;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OldDocumentReportAdmin extends AbstractAdmin
{
    const TABLE_DESCRIPTION = [
        'transportCostLabel' => 'Транспортные и прочие расходы на поездку к месту стажировки/обучающего мероприятия',
        'involvedCostLabel' => 'Оплата привлеченных организаций/ специалистов (включая налоги и страховые взносы) и сопутствующие расходы',
        'eventCostLabel' => 'Расходы на последующие мероприятия по внедрению полученных знаний и опыта',
        'otherCostLabel' => 'Иные расходы',
    ];

    use NestedListOptionsTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('doctrine');

        $folders = [];
        $expenseTypes = [];
        if ($this->hasParentFieldDescription()) {
            $parent = $this->getParentFieldDescription()->getAdmin()->getSubject();
            $folders = $doctrine->getRepository('NKOOrderBundle:Folder')->findBy(['reportClass' => get_class($parent)]);
            $expenseTypes = $parent->getReportForm()->getExpenseTypes();
        }

        $formMapper
            ->add('date', DateType::class, array(
                    'required' => false,
                    'label' => '2. Дата',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD.MM.YYYY',
                        'maxlength' => 10
                    ]
            ))
            ->add('folder', null, array(
                'label' => "3. Выберите тип документа",
                'multiple' => false,
                'required' => false,



            ))
            ->add('document_type', 'entity', array(
                'class'=> DocumentType::class,
                'label' => "4. Выберите, в которую из папок данного типа документ поместить",
                'multiple' => false,
                'required' => false
            ))
            ->add('document_type', 'entity', array(
                'class'=> DocumentType::class,
                'label' => "4. Выберите, в которую из папок данного типа документ поместить"
            ))
            ->add('tableType', 'choice', array(
                'choices' => array(
                    self::TABLE_DESCRIPTION['transportCostLabel'] => 0,
                    self::TABLE_DESCRIPTION['involvedCostLabel'] => 1,
                    self::TABLE_DESCRIPTION['eventCostLabel'] => 2,
                    self::TABLE_DESCRIPTION['otherCostLabel'] => 3
                ),
                'label' => "5. Выберите, к какой из таблиц РЕЕСТРА РАСХОДОВ в отчете относится документ",
                'required' => false,
            ))
            ->add('row', TextType::class, array(
                'required' => false,
                'label' => "6. Выберите название строки данной таблицы, к которой относится документ"
            ))
            ->add('file', FilePreviewType::class, array(
                    'label' => '7. Загрузите файл в формате .pdf, максимальный размер - 50Mb',
                    'data_class' => null,
                    'required' => false
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $reportId = parent::getFilterParameters()['report']['value'];

        if ($this->getCurrentUser() instanceof NKOUser) {
            $datagridMapper
                ->add('report', null, array('operator_type' => 'hidden', 'advanced_filter' => false), 'entity', array(
                        'class' => Report::class,
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                                ->andWhere('u.psrn = :report')
                                ->setParameters(array('report' => $this->getCurrentUser()->getPsrn()));
                        }
                ));
        } else {
            $datagridMapper
                ->add('report', null, [
                    'show_filter' => false,
                    'label' => 'Организация',
                    'operator_type' => 'hidden',
                    'advanced_filter' => false
                ], null, [
                    'query_builder' => function (EntityRepository $er) use ($reportId) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.id = :id')
                            ->setParameters([
                                'id' => $reportId
                            ]);
                    }
                ]);
        }

        $datagridMapper
            ->add('document_type', null, array( 'operator_type' => 'hidden','advanced_filter' => false))
            ->add('folder', null, array( 'operator_type' => 'hidden','advanced_filter' => false))
            ->add('date', 'doctrine_orm_date_range', array('input_type' => DateType::class,
                'operator_type' => 'hidden','advanced_filter' => false))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->report_id = $this->getRequest()->query->get('report_id');

        $this->parameters = [
            'listmapper' => [
                'type' => 'PDF',
                'fieldName' => 'file'
            ]
        ];

        $listMapper
            ->add('report')
            ->add('file', 'string', array('template' => "NKOOrderBundle:Admin:preview_file_document.html.twig"))
            ->add('document_type')
            ->add('folder')
        ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'NKOOrderBundle:CRUD:base_list.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    private function getCurrentUser()
    {
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }
}
