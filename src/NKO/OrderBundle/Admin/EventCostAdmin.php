<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 25.10.16
 * Time: 11:24
 */
namespace NKO\OrderBundle\Admin;

use Doctrine\DBAL\Types\StringType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventCostAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('proofProductCostDocument', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Документ, подтверждающий произведенные
                    расходы (№, дата и наименование документа)',
                ))
            ->add('proofPaidGoodsDocument', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Документ(-ы), подтверждающий (-ие)
                    получение оплаченных товаров/услуг (№, дата и наименование документа)',
                ))
            ->add('name', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Наименование расхода',
                ))
            ->add('sum', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Сумма, руб.',
                ))
            ->add('cost_key', HiddenType::class,
                array(
                    'attr' => array(
                        'hidden' => true
                    )
                ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$listMapper

    }

}