<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.08.16
 * Time: 10:24
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BeneficiaryGroupAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('groupName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'группа благополучателей'
                ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('groupName');
    }

    protected function configureShowFields(ShowMapper $showMapper) {

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }
}