<?php


namespace NKO\OrderBundle\Admin;

use Blameable\Fixture\Document\Type;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use NKO\OrderBundle\Entity\Organization;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReportRevertAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->add('download_report', $this->getRouterIdParameter().'/download_report');
        $collection->add('get_report_history_documents', $this->getRouterIdParameter().'/get_report_history_documents');
        $collection->add('revert_report', $this->getRouterIdParameter().'/revert_report');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nkoName')
            ->add('psrn')
            ->add('reportForm.title', null, array(
                'label' => 'Тип отчета'
            ))
            ->add('period.name', null, array(
                'label' => 'Период'
            ))
            ->add('createdAt', null, ['template' => 'NKODefaultBundle:Admin:list__document.html.twig'])
            ->add('_action', null, array(
                'actions' => array(
                    'download_report' => ['template' =>  'NKOOrderBundle:CRUD:list__action_download_report.html.twig'],
                    'get_report_history_documents' => array(
                        'template' => 'NKOOrderBundle:CRUD:list__action_report_history_documents_list.html.twig'
                    ),
                    'revert_report' =>array( 'template' =>  'NKOOrderBundle:CRUD:list__action_revert_report.html.twig'),
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('psrn')
            ->add('reportForm')
            ->add('period')
            ->add('nkoName')
            ->add('createdAt', 'doctrine_orm_date_range', ['field_type'=>'sonata_type_datetime_range_picker'])
        ;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->select('partial ' . $query->getRootAlias() . '.{id, nkoName, psrn, reportForm, period, createdAt}');

        return $query;
    }
}
