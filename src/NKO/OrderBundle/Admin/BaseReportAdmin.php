<?php


namespace NKO\OrderBundle\Admin;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Traits\SessionListFilterTrait;
use NKO\OrderBundle\Utils\RevertManagerUtils;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticKNSReport2018;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReport;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplication;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use Knp\Menu\ItemInterface as MenuItemInterface;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;
use NKO\OrderBundle\Entity\Autosave\ReportAdditionalFields;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;

class BaseReportAdmin extends AbstractAdmin
{
    use SessionListFilterTrait;

    const ENDING_ENTITY_RESOLVER = '\EntityResolver';

    const RESULT_FARVATER = [
        'expectedAnalyticResults',
        'practiceAnalyticResults',
        'expectedAnalyticIndividualResults',
        'practiceAnalyticIndividualResults',
    ];

    const RESULT_CONTINUATION = [
        'expectedAnalyticResults',
        'practiceAnalyticResults'
    ];

    /**
     * @var ContainerAwareInterface $container
     */
    private $container;

    private $isHistoryCreated = null;

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('periodId', HiddenType::class);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('redirect_to_documents_list', $this->getRouterIdParameter().'/redirect_to_document_list');
        $collection->add('download_report', $this->getRouterIdParameter().'/download_report');
        $collection->add('send_report', $this->getRouterIdParameter() . '/send_report');
        $collection->add('save', $this->getRouterIdParameter() . '/save');
        $collection->add('clear', $this->getRouterIdParameter() . '/clear');
        $collection->add('allowAutosave', $this->getRouterIdParameter() . '/allowAutosave');
        $collection->add('complete_edition', $this->getRouterIdParameter() . '/complete_edition');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('reportForm')
            ->add('period')
            ->add('isSend')
            ->add('isBlocked')
        ;
        if (!$this->getCurrentUser()->hasRole('ROLE_NKO')) {
            $datagridMapper
                ->add('author', null, array(), null, array(), array(
                    'admin_code' => 'sonata.admin.nko.nko_user'
                ));
        }
    }

    public function getFilterParameters()
    {
        $isReseted = $this->resetSessionFilters();

        return parent::getFilterParameters();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        if (!$this->getCurrentUser()->hasRole('ROLE_NKO')) {
            $listMapper
                ->add('author.nkoName', null, array(
                    'label' => 'Название организации'
                ), array(
                    'admin_code' => 'sonata.admin.nko.nko_user'
                ));
        }

        $listMapper
            ->add('reportForm.title', null, array(
                'label' => 'Тип отчета'
            ))
            ->add('period.name', null, array(
                'label' => 'Период'
            ))
            ->add('isSend', null, array(
                'label' => 'Отправлен'
            ))
            ->add('underConsideration', null, array(
                'label' => 'На рассмотрении'
            ))
            ->add('isAccepted', null, array(
                'label' => 'Принят'
            ))
            ->add('isBlocked', null, array(
                'label' => 'Отчёт заблокирован'
            ))
        ;

/*        if ($this->getCurrentUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $listMapper->add('isAutosaved');
        }*/

        $listMapper
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array( 'template' =>  'NKOOrderBundle:CRUD:list__action_edit_report.html.twig'),
                    'delete' => array(),
                    'download_report' => [ 'template' =>  'NKOOrderBundle:CRUD:list__action_download_report.html.twig'],
                    'redirect_to_documents_list' => [
                        'template' => 'NKOOrderBundle:CRUD:list__action_download_document_report.html.twig'
                    ],

                )
            ));
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this;


        $menu->addChild('link_filter_without_values', [
            'uri' => $admin->generateUrl('list', [
                'without_competition' => 1
            ]),
            'label' => 'link_filter_without_values_report'
        ]);
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig'
            )
        );
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        /**
         * @var UserInterface $user
         */

        $user = $this->getCurrentUser();

        if ($user->hasRole('ROLE_NKO')) {
            $query
                ->andWhere($query->getRootAlias() . '.author = :user')
                ->leftJoin($query->getRootAlias() . '.period', 'p')

                ->setParameter('user', $user);
        }
        $query
            ->select(
                'partial ' .
                $query->getRootAlias() .
                '.{id, author, reportForm, period, isSend, underConsideration, lastModified, isAccepted, isBlocked, userBlockedFinanceReport}'
                //'.{id, author, reportForm, period, isSend, isAutosaved, underConsideration, lastModified, isAccepted}'
            );

        return $query;
    }

    public function getCurrentUser()
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();
    }

    public function prePersist($object)
    {
        $this->preUpdate($object);
    }

    public function preUpdate($object)
    {
        $object->setLastModified(new \DateTime());
    }

    public function postUpdate($object)
    {
        if ($this->isHistoryCreated) {
            return;
        }

        $this->container = $this->getConfigurationPool()->getContainer();
        $em = $this->container->get('doctrine')->getManager();
        $em->detach($object);

        $object = $this->container->get('nko.resolver.proxy_resolver')->resolveProxies($object);
        $object = $this->container->get('nko.resolver.collection_resolver')->fetchCollections($object);
        $object = $this->resolveNestedCollections($object);

        $history = $this->container->get('nko.archiver.report_archiver')->archive($object);

        $em->clear();

        $period = $object->getPeriod();
        if ($period) {
            $period = $period->getId();
        }

        if ($object->getIsSend() && !$object->getIsAutosaved()) {
            $prevHistory = $em->getRepository('NKOOrderBundle:ReportHistory')->findOneBy([
                'isSend' => true,
                'reportForm' => $object->getReportForm()->getId(),
                'psrn' => $object->getAuthor()->getPsrn(),
                'period' => $period
            ]);
            if ($prevHistory) {
                $prevHistory->setIsSend(false);
            }
        }

        //из-за невозможности обновления таблицы report_history, дополнительные поля вынесены в др. сущность
        $reportAdditionalFields = new ReportAdditionalFields();
        $reportAdditionalFields->setReportHistory($history);
        $reportAdditionalFields->setIsAutosaved($object->getIsAutosaved());
        $em->persist($reportAdditionalFields);
        //---------------------------------

        if ($history->getPeriod()) {
            $period = $em->getRepository('NKOOrderBundle:PeriodReport')->find($history->getPeriod()->getId());
            $history->setPeriod($period);
        }
        $reportForm = $em->getRepository('NKOOrderBundle:Report\ReportForm')->find($history->getReportForm()->getId());
        $history->setReportForm($reportForm);

        $em->persist($history);
        $em->flush();
    }

    private function resolveNestedCollections($report)
    {
        switch (get_class($report)) {
            case FinanceReport2019::class:
                $this->fetchRegisterCollection($report);
                break;
            case FinanceReport2018::class:
                $this->fetchRegisterCollection($report);
                break;
            case FinanceReport::class:
                $this->fetchRegisterCollection($report);
                break;
            case MonitoringReport::class:
            case Harbor2020MonitoringReport::class:
                foreach ($report->getMonitoringResults() as $monitoringResult) {
                    $this->container->get('nko.resolver.collection_resolver')->fetchCollections($monitoringResult);
                }
                foreach ($report->getDocuments() as $document) {
                    $this->container->get('nko.resolver.collection_resolver')->fetchCollections($document);
                    $this->container->get('nko.resolver.proxy_resolver')->resolveProxies($document);
                }
                break;
            case AnalyticReport::class:
                $this->fetchFeedback($report);
                break;
            case AnalyticReport2018::class:
                $this->accessor = PropertyAccess::createPropertyAccessor();

                $applicationClass = $report->getReportForm()->getCompetition()->getApplicationClass();

                if ($applicationClass == FarvaterApplication::class) {
                    foreach (self::RESULT_FARVATER as $field) {
                        $this->fetchLinkedResult($report, $field);
                    }
                } elseif ($applicationClass == ContinuationApplication::class) {
                    foreach (self::RESULT_CONTINUATION as $field) {
                        $this->fetchResult($report, $field);
                    }
                }

                $this->fetchFeedback($report);
                break;
            case AnalyticReport2019::class:
                $this->accessor = PropertyAccess::createPropertyAccessor();
                $this->fetchCollections($report, RevertManagerUtils::COLLECTIONS_TO_RESOLVE[AnalyticReport2019::class]);
                break;
            case AnalyticKNSReport2018::class:
                $this->fetchFeedback($report);
                break;
            case AnalyticHarborReport2019::class:
                $this->fetchFeedback($report);
                break;
            case MixedReport::class:
                $this->fetchRegisterCollection($report);
                break;
            case MixedReport2019::class:
            case MixedReport2020::class:
                $this->fetchRegisterCollection($report);
                break;
        }

        return $report;
    }

    public function preRemove($object)
    {
        $entityResolverService = $this->getConfigurationPool()->getContainer()->get(get_class($object).self::ENDING_ENTITY_RESOLVER);
        $entityResolverService->preDeleteAction($object, null);

        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getEntityManager();
        $histories = $em->getRepository(ReportHistory::class)->findByReport($object);
        $this->deleteHistories($histories, $em, $object->getId());
    }

    private function fetchLinkedResult($report, $field)
    {
        $results = $this->accessor->getValue($report, $field);
        /** @var EntityManager $em */
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getEntityManager();

        foreach ($results as $result) {
            $result = $result->getLinkedFarvaterResult();
            $targetGroup = $result->getTargetGroup();
            if ($targetGroup) {
                $em->refresh($targetGroup);
            }

            $indicator = $result->getIndicator();
            if ($indicator) {
                $em->refresh($indicator);
            }
        }
    }

    private function fetchResult($report, $field)
    {
        $results = $this->accessor->getValue($report, $field);
        /** @var EntityManager $em */
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getEntityManager();

        foreach ($results as $result) {
            $result = $result->getLinkedResult();

            $problem = $result->getProblem();
            if ($problem) {
                $em->refresh($problem);
            }
        }
    }

    private function fetchRegisterCollection($report)
    {
        foreach ($report->getRegisters() as $register) {
            $this->container->get('nko.resolver.collection_resolver')->fetchCollections($register);
        }
    }

    private function fetchCollections(BaseReport $report, $collections)
    {
        foreach ($collections as $collectionName) {
            $collection = $this->accessor->getValue($report, $collectionName);
            foreach ($collection as $element) {
                $this->container->get('nko.resolver.collection_resolver')->fetchCollections($element);
            }
        }
    }

    private function fetchFeedback($report)
    {
        foreach ($report->getPracticeFeedbackAttachments() as $feedbackAttachment) {
            $this->container->get('nko.resolver.collection_resolver')->fetchCollections($feedbackAttachment);
        }

        foreach ($report->getPracticeSpreadFeedbackAttachments() as $feedbackAttachment) {
            $this->container->get('nko.resolver.collection_resolver')->fetchCollections($feedbackAttachment);
        }
    }

    /**
     * @param $historiesArray array
     * @param $em EntityManager
     *
     */
    private function deleteHistories($historiesArray, $em, $id)
    {
        foreach ($historiesArray as $history) {
            if (unserialize($history->getData())->getId() === $id) {
                $em->remove($history);
            }
        }
    }

    public function setIsHistoryCreated($isHistoryCreated)
    {
        $this->isHistoryCreated = $isHistoryCreated;
    }

    public function setNonGroups()
    {
        $this->setValidationGroups(['None']);
    }

    public function setValidationGroups(array $groups)
    {
        $this->formOptions['validation_groups'] = $groups;
    }

    protected function getApplicationFromReport($report)
    {
        /** @var BaseReport $report */
        $application = unserialize($report->getApplicationHistory()->getData());

        if ($report->getReportForm()->isDataTakesFromActualApplication()) {
            $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
            $application = $em->getRepository(BaseApplication::class)->find($application->getId());
        }

        return $application;
    }
}
