<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 14.08.16
 * Time: 13:43
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChildrenCategoryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('categoryName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'категория детей'
                ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('categoryName');
    }

    protected function configureShowFields(ShowMapper $showMapper) {

    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }
}