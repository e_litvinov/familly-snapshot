<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.09.16
 * Time: 10:10
 */

namespace NKO\OrderBundle\Admin;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Traits\SessionListFilterTrait;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface as MenuItemInterface;
use NKO\OrderBundle\Entity\MarkList;
use NKO\UserBundle\Entity\ExpertUser;

class ExpertMarkAdmin extends AbstractAdmin
{
    use SessionListFilterTrait;

    const SPREAD = 1;
    const NOT_SPREAD = 0;
    const NOT_EMPTY_SUM = 1;

    protected $baseRouteName = 'admin_sonata_nko_order_expert_mark';

    protected $baseRoutePattern = 'expert_mark';

    /**
     * @var BaseApplicationAdmin
     */
    protected $helpAdmin;

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
        '_sort_by' => 'createdAt',
    );

    public function getFilterParameters()
    {
        if (!$this->resetSessionFilters()) {
            $competitionId = null;
            $competitions = $this
                ->getConfigurationPool()
                ->getContainer()
                ->get('doctrine')
                ->getRepository(Competition::class)
                ->findCompetitionsForEstimate();
            $competition = reset($competitions);
            if ($competition) {
                $competitionId = $competition->getId();
            }

            $this->datagridValues = array_merge([
                'competition' => [
                    'value' => $competitionId
                ]
    //            'sumGrant' => array (
    //                'type' => EqualType::TYPE_IS_EQUAL,
    //                'value' => self::NOT_EMPTY_SUM)
            ], $this->datagridValues);
        }

        return parent::getFilterParameters();
    }

    public function getCurrentCompetition()
    {
        $competitions = $this
            ->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getRepository(Competition::class)
            ->findCurrentCompetitions();
        if ($competitions) {
            return reset($competitions)->getId();
        }

        return null;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $query = null;
        if ($this->getSubject() && $this->getSubject()->getCompetition()) {
            $query = $this
                ->getConfigurationPool()
                ->getContainer()
                ->get('doctrine')
                ->getManager()
                ->createQueryBuilder('e')
                ->select('e')
                ->from('NKOUserBundle:ExpertUser', 'e')
                ->join('e.competitions', 'c')
                ->where('c.id = :competitionId')
                ->setParameters(['competitionId' => $this->getSubject()->getCompetition()->getId()]);
        }
        $formMapper
            ->add('experts', 'sonata_type_model', array(
                'property' => 'full_name',
                'multiple' => true,
                'btn_add' => false,
                'query' => $query,
                'class' => "NKOUserBundle:ExpertUser",
                'help' => "Эксперты добавляются из выпадающего списка, при нажатии на поле",
            ), [
                'admin_code' => 'sonata.admin.nko.expert_user'
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nko_name')
        ;

        if ($this->getCurrentUser()->hasRole('ROLE_EXPERT')) {
            $datagridMapper
                ->add('competition', null, array(), 'entity', [
                    'choices' =>
                        $this->getConfigurationPool()
                            ->getContainer()
                            ->get('doctrine')
                            ->getRepository('NKOOrderBundle:Competition')
                            ->findCompetitionsForEstimate()
                ])
            ;
        } else {
            $datagridMapper
                ->add('competition', null, [], null, [
                    'class' => Competition::class,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.start_date', 'DESC')
                            ;
                    }
                ])
                ->add('psrn')
                ->add('username')
                ->add('experts', null, array('admin_code' => 'sonata.admin.nko.expert_user'))
                ->add('isSpread', 'doctrine_orm_callback', [
                    'callback' => function ($queryBuilder, $alias, $field, $value) {
                        if ($value['value'] === 1) {
                            $queryBuilder
                                ->andWhere($alias.'.isSpread = 1');
                        } elseif ($value['value'] === 0) {
                            $queryBuilder->andWhere($alias.'.isSpread = 0');
                        }

                        if ($value['value'] !== null) {
                            return true;
                        }
                    }], 'choice', [
                        'choices' => [
                            'Распределенные заявки' => self::SPREAD,
                            'Не распределенные заявки' => self::NOT_SPREAD
                        ]
                    ])
                ->add('legalRegion')
                ->add('number')
            ;
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('number', null, array(
                'header_style' => 'width: 4%; text-align: center',
                'row_align' => 'center'
            ))
            ->add('nko_name');

        if ($this->getCurrentUser()->hasRole('ROLE_EXPERT')) {
            $listMapper
                ->add('legalRegion')
                ->add('isAutosaved', 'boolean')
                ->add('finalMark');
        } else {
            $listMapper
                ->add('username')
                ->add('competition.name', null, array(
                    'label' => 'Конкурс'))
                /** TDOD this was used for old competition
                ->add('sumGrant', null, [
                    'editable' => true
                ])
                ->add('contract', null, [
                    'editable' => true
                ])
                 */
            ;
        }
        $listMapper
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array('template' => 'NKOOrderBundle:CRUD:list__action_edit_experts.html.twig'),
                    'download' => array('template' => 'NKOOrderBundle:CRUD:list__action_expert_mark_download_application.html.twig'),
                    'show_documents_list' => array( 'template' =>'NKOOrderBundle:CRUD:list__action_expert_mark_download_document.html.twig'),
                    'download_mark' => array('template' => 'NKOOrderBundle:CRUD:list__action_download_one_mark.html.twig'),
                    'mark_list' => array('template' => 'NKOOrderBundle:CRUD:list__action_mark_list.html.twig'),
                    //'send_mark' => array('template' => 'NKOOrderBundle:CRUD:list__action_send_mark.html.twig'),
                    'show_all_marks' => array('template' => 'NKOOrderBundle:CRUD:list__action_show_all_marks.html.twig'),
                    'set_grants' => array('template' => 'NKOOrderBundle:CRUD:list__action_set_grants.html.twig'),


                )));
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->add('mark_list', $this->getRouterIdParameter().'/mark_list');
        $collection->add('show_marks', $this->getRouterIdParameter().'/show_marks');
        $collection->add('download_mark', $this->getRouterIdParameter().'/download_mark');
        $collection->add('download_all_marks', $this->getRouterIdParameter().'/download_all_marks');
        $collection->add('spread');
        $collection->add('spread_for_custom_expert_number');
        $collection->add('download_summary_table');
        $collection->add('contacts');
        $collection->add('show_documents_list', $this->getRouterIdParameter().'/show_documents_list');
        $collection->add('download_document', $this->getRouterIdParameter().'/download_document');
        $collection->add('send_mark', $this->getRouterIdParameter().'/send_mark');
        $collection->add('download', $this->getRouterIdParameter().'/download');
        $collection->add('set_number');
        $collection->add('winner_info');
        $collection->add('set_grants', $this->getRouterIdParameter().'/set_grants');
        $collection->add('spread_without_expert');
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this;
        $menu->addChild('link_filter_without_values', [
            'uri' => $admin->generateUrl('list', [
                'without_competition' => 1
            ]),
            'label' => 'link_filter_without_values_application'
        ]);

        if ($this->getCurrentUser()->hasRole('ROLE_EMPLOYEE_ADMIN') || $this->getCurrentUser()->hasRole('ROLE_SUPER_ADMIN')) {
            if (key_exists('competition', $this->getFilterParameters())) {
                $menu->addChild('spread', array('uri' => $admin->generateUrl('spread', [
                    'competition' => $this->getFilterParameters()['competition']['value']
                ])));
                $menu->addChild('spread_for_custom_expert_number', array('uri' => $admin->generateUrl('spread_for_custom_expert_number', [
                    'competition' => $this->getFilterParameters()['competition']['value']
                ])));
                $menu->addChild('download_summary_table', array('linkAttributes' => ['target' => '_blank'],'uri' => $admin->generateUrl('download_summary_table', [
                    'competition' => $this->getFilterParameters()['competition']['value']
                ])));









            }
        }

        if ($this->getCurrentUser()->hasRole('ROLE_SUPER_ADM
        IN')) {
            $menu->addChild('spread_without_expert', ['uri' => $admin->generateUrl('spread_without_expert', [
                'competition' => $this->getFilterParameters()['competition']['value']
            ])]);
        }
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if ($this->getCurrentUser()->hasRole('ROLE_EXPERT')) {
            $query
                ->innerJoin($query->getRootAlias().'.experts', 'e')

                ->where($query->getRootAlias().'.isSpread = 1 AND '.$query->getRootAlias().'.isSend = 0 AND e.id = :id')
                ->orWhere($query->getRootAlias().'.isSpread = 1 AND '.$query->getRootAlias().'.isSend = 1 AND e.id = :id')
                ->setParameter('id', $this->getCurrentUser())



                ->select('partial ' . $query->getRootAlias() . '.{id, number, nko_name, legalRegion}, partial e.{id}')
            ;
        } else {
            $query
                ->where($query->getRootAlias().'.isSend = 1 AND '.$query->getRootAlias().'.isAppropriate = 1')
                ->orWhere($query->getRootAlias().'.isSpread = 1 AND '.$query->getRootAlias().'.isSend = 0')
            ;
        }
        return $query;
    }

    private function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'NKOOrderBundle:CRUD:base_list_marks.html.twig';
                break;
            case 'mark_list_outer_row':
                return 'NKOOrderBundle:CRUD:base_list_mark_list_outer_row.html.twig';
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function preUpdate($object)
    {
        $unitOfWork = $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager()->getUnitOfWork();
        $unitOfWork->computeChangeSets();
        if ($unitOfWork->getScheduledCollectionUpdates()) {
            $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\ApplicationLogger');
            $service->logAction('Смена экспертов, назначенных заявке', unserialize($object->getData()));
        }
    }

    public function postUpdate($object)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $expertsApplication = $em->createQueryBuilder()
            ->select('e')
            ->from(ExpertUser::class, 'e')
            ->where(':application MEMBER OF e.applications')
            ->setParameters(array('application'=>$object))
            ->getQuery()
            ->getResult();

        $unnecessaryMarkList = $em->createQueryBuilder()
            ->select('m')
            ->from(MarkList::class, 'm')
            ->where('m.application=:application')
            ->andWhere('m.expert NOT IN (:expert)')
            ->setParameters(['application'=>$object, 'expert'=>$expertsApplication])
            ->getQuery()
            ->getResult();

        $historyManager = $this->getConfigurationPool()->getContainer()->get('mark_list_history_manager');
        if ($unnecessaryMarkList) {
            foreach ($unnecessaryMarkList as $markList) {
                $object->removeMarkList($markList);
                $em->remove($markList);
                $historyManager->removeByMarkList($markList);
            }
        }
        $em->flush();
    }
}
