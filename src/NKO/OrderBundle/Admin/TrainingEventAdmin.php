<?php


namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class TrainingEventAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('eventName',TextType::class,
                array(
                    'required' => false,
                   ))
            ->add('format',TextType::class,
                array(
                    'required' => false,
                ))
            ->add('time',TextType::class,
                array(
                    'required' => false,
                ))
            ->add('place',TextType::class,
                array(
                    'required' => false,
                ))
            ->add('count_member', NumberType::class,  array('required' => false));

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //$listMapper

    }

}