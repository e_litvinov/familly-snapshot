<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.08.16
 * Time: 15:13
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrganizationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('organizationName', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Организация - стажировочная площадка',
                ))
            ->add('traineeshipTopicId');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('organizationName');
    }

    protected function configureShowFields(ShowMapper $showMapper) {

    }








}