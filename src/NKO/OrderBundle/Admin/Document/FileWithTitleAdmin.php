<?php

namespace NKO\OrderBundle\Admin\Document;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class FileWithTitleAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'title' => 'Инструмент',
        'file' => 'Приложение (в формате pdf, doc)',
    ];

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('title', FullscreenTextareaType::class, [
                'label' => $this->getParentFieldDescription()->getOption('titleLabel', self::DESCRIPTION['title'])
            ])
            ->add('file', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => $this->getParentFieldDescription()->getOption('fileLabel', self::DESCRIPTION['file'])
            ])
        ;
    }
}
