<?php

namespace NKO\OrderBundle\Admin\Document;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class FileWithFieldsAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'firstField' => 'Инструмент',
        'secondField' => 'Инструмент',
        'file' => 'Приложение (в формате pdf, doc)',
    ];

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('firstField', FullscreenTextareaType::class, [
                'label' => $this->getParentFieldDescription()->getOption('firstFieldLabel', self::DESCRIPTION['firstField'])
            ])
            ->add('secondField', FullscreenTextareaType::class, [
                'label' => $this->getParentFieldDescription()->getOption('secondFieldLabel', self::DESCRIPTION['secondField'])
            ])
            ->add('file', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => $this->getParentFieldDescription()->getOption('fileLabel', self::DESCRIPTION['file'])
            ])
        ;
    }
}
