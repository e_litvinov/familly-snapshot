<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.08.16
 * Time: 11:55
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteLinkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('link', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сайт организации в сети Интернет',
                    'attr' => array('help-bottom' => 'URL должен начинаться с http:// или https://')
                ));
    }
}