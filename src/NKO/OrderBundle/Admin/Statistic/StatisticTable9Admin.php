<?php

namespace NKO\OrderBundle\Admin\Statistic;

class StatisticTable9Admin extends BaseStatisticAdmin
{
    const TABLE_NUMBER = 9;
    const REPORT_NAME = 'Мониторинговый отчёт СГ-2020';
    const RESOLVER = 'NKO\OrderBundle\Admin\Statistic\StatisticTable9Admin';
}
