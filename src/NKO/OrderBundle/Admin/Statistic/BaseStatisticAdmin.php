<?php

namespace NKO\OrderBundle\Admin\Statistic;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class BaseStatisticAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        $collection->remove('show');
        $collection->remove('batch');
        $collection->remove('edit');
        $collection->add('get_ajax_data', '/admin/nko/order/statistic/get_ajax_data');
    }

    public function getRoutes()
    {
        $name = (new \ReflectionClass(static::class))->getShortName();
        $name = str_replace("admin", "", strtolower($name));
        $this->baseRouteName = 'admin_nko_order_statistic_'.$name;
        $this->baseRoutePattern = '/nko/order/statistic-'.$name;
        return parent::getRoutes();
    }

    public function getTableNumberFromConstant()
    {
        return (new \ReflectionClass(static::class))->getConstant('TABLE_NUMBER');
    }
}
