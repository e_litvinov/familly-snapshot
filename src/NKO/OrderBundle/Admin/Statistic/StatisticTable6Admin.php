<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 31.01.19
 * Time: 10:58
 */

namespace NKO\OrderBundle\Admin\Statistic;

class StatisticTable6Admin extends BaseStatisticAdmin
{
    const TABLE_NUMBER = 6;
    const YEAR_REPORT_PERIOD = 'Выберите начальный и конечный годы, за которые необходимо сформировать таблицу, c:';
}
