<?php

namespace NKO\OrderBundle\Admin\Statistic;

class StatisticTable4Admin extends BaseStatisticAdmin
{
    const TABLE_NUMBER = 4;
    const REPORT_NAME = 'Содержательный отчет';
    const RESOLVER = 'NKO\OrderBundle\Admin\Statistic\StatisticTable3Admin';
}
