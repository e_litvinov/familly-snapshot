<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/30/17
 * Time: 11:52 AM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;

class SocialResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('name', TextType::class,
                array(
                    'required' => false,
                ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
        ;
    }
}