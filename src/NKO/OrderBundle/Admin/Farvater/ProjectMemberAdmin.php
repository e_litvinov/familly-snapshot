<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/24/17
 * Time: 12:53 AM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProjectMemberAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('fullName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'ФИО участника проекта',
                ))
            ->add('role', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Роль в проекте',
                ))
            ->add('functionalResponsibilities', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Функциональные обязанности',
                ))
            ->add('employmentRelationship', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => 'Трудовые отношения с организацией',
                    'property' => 'name',
                ))
            ->add('personalInformation', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Краткие сведения об участнике',
                ))
        ;
    }
}