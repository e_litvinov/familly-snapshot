<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/24/17
 * Time: 1:45 AM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProjectPartnerAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('organizationName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Наименование организации',
                ))
            ->add('organizationInformation', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Краткие сведения об организации',
                ))
            ->add('projectParticipation', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Участие в проекте',
                ))
        ;
    }
}