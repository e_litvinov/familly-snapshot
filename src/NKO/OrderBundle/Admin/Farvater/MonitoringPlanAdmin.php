<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/24/17
 * Time: 12:36 AM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MonitoringPlanAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('activity', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Задача / мероприятие / действие',
                ))
            ->add('term', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сроки',
                ))
            ->add('expectedResult', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Основной ожидаемый результат',
                ))
        ;
    }
}