<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/23/17
 * Time: 11:51 AM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GrantAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('projectName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Название проекта',
                ))
            ->add('implementationPeriod', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сроки реализации проекта',
                ))
            ->add('financingOrganizationName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Название организации, предоставившей финансирование',
                ))
        ;
    }
}