<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/23/17
 * Time: 6:35 PM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;

class IntroductionIndexAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('name', TextType::class,
                array(
                    'required' => false,
                ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
        ;
    }
}