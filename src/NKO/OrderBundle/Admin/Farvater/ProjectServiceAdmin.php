<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/23/17
 * Time: 4:42 PM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProjectServiceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('service', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Услуга/мероприятие',
                ))
            ->add('targetValue', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Целевое значение на 01.12.2016',
                ))
            ->add('approximateValue', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Ориентировочное целевое значение на 01.12.2018',
                ))
            ->add('measureMethod', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Способ измерения',
                ))
        ;
    }
}