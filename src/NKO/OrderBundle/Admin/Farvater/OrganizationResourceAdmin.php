<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/24/17
 * Time: 1:10 AM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OrganizationResourceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('resourceType', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => 'Наименование ресурса',
                    'property' => 'name',
                ))
            ->add('resourceDescription', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Описание ресурса',
                ))
        ;
    }
}