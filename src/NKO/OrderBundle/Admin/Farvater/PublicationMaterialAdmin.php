<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/12/17
 * Time: 2:08 PM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PublicationMaterialAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('name', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Материал'
                ))
        ;
    }
}