<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/20/17
 * Time: 12:07 PM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Sonata\AdminBundle\Datagrid\ListMapper;

class FarvaterApplicationAdmin extends BaseApplicationAdmin
{
    CONST FARVATER_ASSOCIATION_ENTITY_NAME = 'farvater';
    CONST COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME  = 'application';
    CONST FARVATER_AND_KNS_2016_ASSOCIATION_ENTITY_NAME = 'farvaterAndKNS2016';

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->description = $this->description + [
                'countEmployeesLabel' => 'Количество <b>штатных,привлеченных и добровольных сотрудников</b> организации на дату подачи заявки',
                'deadLineLabel' => '<b>Сроки реализации Проекта</b>',
                'immediateResultsLabel' => '<b>Непосредственные результаты</b><br>
                    <em>Подробнее о непосредственных результатах см. <a href="%s" target="_blank">Руководство (методические рекомендации) для Заявителя</a></em><br><br>
                    <b>ЦЕЛЕВЫЕ ГРУППЫ, ПРИНИМАЮЩИЕ УЧАСТИЕ В ПРОЕКТЕ, ПОЛУЧАЮЩИЕ УСЛУГУ И ПР.</b>',
                'socialResults' => '<b>Социальные результаты</b>',
                'projectActivityLabel' => '<b>Перечислите основные задачи / мероприятия / действия, которые будут реализованы в 2016 году (с 16.04.2016 по 01.12.2016) в рамках реализации Проекта</b>',
                'taskActivityLabel' => '<b>Кратко опишите основные задачи / мероприятия / действия, которые планируются в 2017 и 2018 гг. в рамках реализации проекта </b>',
                'practiceSpread' => '<b>РАСПРОСТРАНЕНИЕ ИНФОРМАЦИИ О ПРАКТИКЕ</b><br>
                    <em>В таблице ниже представлены возможные формулировки результатов. При подготовке заявки укажите подходящие для вашего проекта.<br>
                    Целевые значения результатов проекта в целом (к 01.12.2018г.) указываются ориентировочные. В конце 2016 и 2017 года, при подаче промежуточной отчетности, возможны уточнения, обоснования и коррекция целевых показателей проекта в целом.</em>',
                'practiceRegionLabel' => '<label class="control-label">Территория реализации практики проекта | Дополнительная информация</label><label class="help"></label>',
                'personalDataLabel' => '<b>Фамилия Имя Отчество (полностью) лица, подающего заявку</b><br>
                    <em>Руководитель организации или лицо, которое будет подписывать договор в случае поддержки проекта (если договор будет подписывать не руководитель организации).</em>',
                'contractLabel' => '<em>При заключении договора необходимо представить оригиналы (с подписью) Информированного согласия физического лица на 
                    обработку персональных данных в рамках организации и проведения Всероссийского конкурса «Семейный фарватер», заполненных отдельно на каждого члена команды проекта, включая руководителя и бухгалтера организации.
                    <a href="%s" target="_blank">Скачать образец.</a></em>',
                'practiceResultsLabel' => '<b>Ожидаемые результаты проекта – в отношении представленной практики</b><br>
                        <em>Заполните таблицу ниже. В таблице представлены условные формулировки результатов. 
                        При подготовке заявки, укажите подходящую для вашего проекта. При ориентации практики проекта на «сложные» категории детей (пункт 3.2.1),
                        укажите по данным категориям планируемые показатели и целевые значения дополнительно.
                        При оценке проекта эксперты будут учитывать и сравнивать информацию как из краткой заявки, 
                        так и предоставленную в полной – существенное расхождение крайне нежелательно. 
                        При изменении, а также в иных случаях (если это необходимо) предоставьте обоснование или более подробную информацию в полях «Комментарий».<br>
                        Целевые значения результатов проекта в целом (к 01.12.2018 г.) указываются ориентировочные. 
                        В конце 2016 и 2017 года, при подаче промежуточной отчетности, возможна также уточнение, 
                        обоснование и коррекция целевых показателей проекта в целом.</em>',
                'practiceExpectingResultsStabilityLabel' => '<b>Устойчивость ожидаемых результатов практики</b><br>
                    <em>Устойчивость достигнутых социальных результатов (в отношении практики) после завершения реализации проекта. За счет чего социальные изменения сохранятся по завершении проекта?</em>',
                'practiceSpreadPurposeLabel' => '<b>Цель проекта в отношении распространения представленной практики</b><br>
                    <em>Какая цель ставится в отношении распространения представленной на Конкурс практики?</em>',
                'practiceExpectingSpreadResultsLabel' => '<b>Ожидаемые результаты распространения и внедрения практики</b><br>
                        <em>При оценке заявки эксперты будут учитывать и сравнивать информацию как из краткой заявки, так и предоставленную в полной – существенное расхождение крайне нежелательно. При изменении, а также в иных случаях (если это необходимо) предоставьте обоснование или более подробную информацию в комментарии.</em>',
                'spreadActivitiesLabel' => 'Число мероприятий / услуг по распространению практики среди специалистов и организаций<br>
                    Укажите по строкам, где применимо, или укажите свое мероприятие',
                'organizationResultsLabel' => '<b>Результаты для организации-заявителя (в части распространения и внедрения практики проекта)</b><br>
                    <em>Укажите, каковы ожидаемые результаты от реализации проекта (в части распространения и внедрения практики проекта) непосредственно для вашей организации. Например, наём дополнительных сотрудников, привлечение волонтеров, увеличение объема частных и/или корпоративных пожертвований, повышение репутации, узнаваемости вашей организации, увеличение числа партнеров и пр.</em>',
                'practiceResultsStabilityLabel' => '<b>Устойчивость результатов</b><br>
                    <em>Устойчивость результатов распространения и внедрения практики в деятельность других организаций и специалистов сферы защиты детства. Возможность создания и дальнейшей работы на базе организации-заявителя отраслевого ресурсного центра (стажировочной площадки) в области профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей.</em>',
                'monitoringReviewLabel' => '<b>Опишите, как будут происходить мониторинг, измерение и оценка результатов проекта (непосредственных и социальных) – в отношении практики проекта, а также ее распространения и внедрения</b><br>
                    <em>Кто, когда и кому будет предоставлять данные? Кто из участников проекта будет ответственен за эти вопросы? Какие будут выделены ресурсы (человеческие и пр.)?</em>',
                'projectMembersLabel' => '<b>Команда проекта</b><br>
                    <em>Укажите состав участников, которые будут вовлечены в непосредственную реализацию проекта, а также их квалификацию, роль в Проекте и характер трудовых отношений с Организацией. Если ФИО некоторых участников пока неизвестны, то в соответствующей графе укажите их роль и функциональные обязанности в проекте. Например, «Психолог 1»</em>',
                'organizationResourcesLabel' => '<b>Имеющиеся у Организации ресурсы, необходимые для реализации Проекта></b><br>
                    <em>Укажите, какие ресурсы, необходимые для реализации Проекта, уже есть в распоряжении организации (собственные средства) или будут привлечены со стороны партнеров</em>',
                'projectPartnersLabel' => '<b>Партнеры и доноры Проекта</b><br>
                    <em>Перечислите иные организации, участвующие в реализации проекта, с указанием их роли в Проекте</em>',
                'budgetYearLabel' => '<b>Сумма запрашиваемого финансирования на 2016 год</b><em>(тысяч рублей)</em><br>
                    <em>(в точном соответствии с суммой, указанной в файле-приложении <a href="%s" target="_blank">«Бюджет проекта»</a>)</em>',
                'appsLabel' => '<em>При подаче заявки Заявитель должен представить в электронном виде скан-копии (или ссылки, если это оговорено дополнительно) следующих документов, заверенных подписью руководителя и печатью организации:</em>',
                'projectServicesLabel' => '<b>Услуги, мероприятия по проекту<br><br>
                    Количество оказанных услуг, организованных мероприятий для целевых групп, обращений за помощью и пр. в рамках проекта.</b><br>
                    <em>Укажите, что именно. При необходимости создайте дополнительные строки.</em>',
                'legalAddressLabel' => 'Юридический <b>адрес</b>',
                'actualAddressLabel' => '<span id="_actual_span">Фактический <b>адрес</b></span>',
                'headOfOrganizationLabel' => '<label class="control-label"><b>Руководитель организации</b></label>',
                'headOfProjectLabel' => '<b class="head-of-project">Руководитель проекта</b>',
                'headOfAccountingLabel' => '<b class="head-of-accounting">Главный бухгалтер</b>',
                'phoneLabel' => '<label class="control-label"><b>Телефон</b> для оперативного контакта с организацией</label>',
                'headPhoneLabel' => '<label class="control-label">Телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
                'headPhoneMobileLabel' => '<label class="control-label">Мобильный телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
                'internationalCode' => '<label class="code">+7</label>',
                'bankCredentials' => '<b>*Банковские реквизиты организации</b>',
                'headOfProjectPhoneLabel' => '<label class="control-label head-of-project">Телефон</label><label class="help head-of-project">Код - 3 цифры, номер - 7 цифр</label>',
                'headOfProjectPhoneMobileLabel' => '<label class="control-label head-of-project">Мобильный телефон</label><label class="help head-of-project">Код - 3 цифры, номер - 7 цифр</label>',
                'headOfProjectInternationalCode' => '<label class="code head-of-project">+7</label>',
                'headOfAccountingPhoneLabel' => '<label class="control-label head-of-accounting">Телефон</label><label class="help head-of-accounting">Код - 3 цифры, номер - 7 цифр</label>',
                'headOfAccountingPhoneMobileLabel' => '<label class="control-label head-of-accounting">Мобильный телефон</label><label class="help head-of-accounting">Код - 3 цифры, номер - 7 цифр</label>',
                'headOfAccountingInternationalCode' => '<label class="code head-of-accounting">+7</label>'

            ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $processing_template_link = $this->getConfigurationPool()->getContainer()->get('assets.packages')->getUrl('bundles/nkoorder/resources/farvater_2016.docx');
        $rukovodstvo_template_link = $this->getConfigurationPool()->getContainer()->get('assets.packages')->getUrl('bundles/nkoorder/resources/farvater_2016_rukovodstvo_2.pdf');
        $budget_template_link = $this->getConfigurationPool()->getContainer()->get('assets.packages')->getUrl('bundles/nkoorder/resources/farvater_2016_budget_2.xlsx');
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        $formMapper
            ->with('1. Контактная информация')
            ->add('projectName', TextType::class,
                array(
                    'label' => '<b>Название проекта</b>',
                    'required' => false,
                ))
            ->add('deadLine', CustomTextType::class,
                array(
                    'help' => '<b>Сроки реализации Проекта</b></b> <br>с «16» апреля 2016г. по «01» декабря 2018г.',
                    'required' => false
                ))
            ->add('name', TextType::class,
                array(
                    'label' => '<br>Полное <b>наименование</b> организации',
                    'required' => false,
                ))
            ->add('abbreviation', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сокращенное наименование организации',
                    'attr' => array('maxlength' => 255)
                ))
            ->add('pSRN', TextType::class,
                array(
                    'required' => false,
                    'data' => get_class($user) == 'NKO\UserBundle\Entity\NKOUser' ? $user->getPsrn() : '',
                    'disabled' => true,
                    'label' => 'ОГРН организации',
                    'attr' => array('maxlength' => 13,
                        'help' => '<label class="help">ОГРН должен состоять из 13 цифр без пробелов.</label>')
                ))
            ->add('legalAddressLabel', CustomTextType::class,
                array(
                    'help' => $this->description['legalAddressLabel'],
                ))
            ->add('legalPostCode', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Индекс',
                    'attr' => array('maxlength' => 6)
                ))
            ->add('legalRegion', 'sonata_type_model',
                array(
                    'required' => false,
                    "property" => "name",
                    'label' => 'Регион'
                ))
            ->add('legalCity', TextType::class,
                array(
                    'label' => 'Населенный пункт',
                    'required' => false,
                    'attr' => array('maxlength' => 255),
                ))
            ->add('legalStreet', TextType::class,
                array(
                    'label' => 'Улица, дом, корпус, номер офиса',
                    'required' => false,
                ))
            ->add('actualAddressLabel', CustomTextType::class,
                array(
                    'help' => $this->description['actualAddressLabel'],
                ))
            ->add('actualPostCode', TextType::class,
                array(
                    'label' => 'Индекс',
                    'required' => false,
                    'attr' => array('maxlength' => 6)))
            ->add('actualRegion', 'sonata_type_model',
                array(
                    'required' => false,
                    "property" => "name",
                    'label' => 'Регион'
                ))
            ->add('actualCity', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Населенный пункт',
                    'attr' => array('maxlength' => 255),
                ))
            ->add('actualStreet', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Улица, дом, корпус, номер офиса'
                ))
            ->add('siteLinks', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<b>Сайт</b> организации в сети Интернет',
                    'btn_add' => 'Добавить',
                ),
                array(
                    'admin_code' => 'sonata.admin.nko.order.site_link',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('socialNetworkLinks', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Страницы организации в <b>социальных сетях</b>',
                    'btn_add' => "Добавить"),
                array(
                    'admin_code' => 'sonata.admin.nko.order.social_network_link',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('email', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Адрес <b>электронной</b> почты для оперативного контакта с организацией '))
            ->add('phoneLabel', CustomTextType::class,
                array(
                    'help' => $this->description['phoneLabel'],
                ))
            ->add('phoneLabelHelp', CustomTextType::class,
                array(
                    'help' => $this->description['headPhoneLabel'],
                ))
            ->add('internationalCode', CustomTextType::class,
                array(
                    'help' => $this->description['internationalCode'],
                ))
            ->add('phoneCode', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 3,
                        'placeholder' => 'Код')
                ))
            ->add('phone', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 7,
                        'placeholder' => 'Номер')
                ))
            ->add('headOfOrganizationLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headOfOrganizationLabel'],
                ))
            ->add('headOfOrganizationFullName', TextType::class,
                array(
                    'label' => 'ФИО',
                    'required' => false,
                ))
            ->add('headOfOrganizationPosition', TextType::class,
                array(
                    'label' => 'Должность',
                    'required' => false,
                ))
            ->add('headOfOrganizationPhoneLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headPhoneLabel'],
                ))
            ->add('headOfOrganizationPhoneInternationalCode', CustomTextType::class,
                array(
                    'help' => $this->description['internationalCode'],
                ))

            ->add('headOfOrganizationPhoneCode', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 3,
                        'placeholder' => 'Код'),
                ))
            ->add('headOfOrganizationPhone', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 7,
                        'placeholder' => 'Номер'),
                ))
            ->add('headOfOrganizationMobilePhoneLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headPhoneMobileLabel'],
                ))
            ->add('headOfOrganizationMobilePhoneInternationalCode', CustomTextType::class,
                array(
                    'help' => $this->description['internationalCode'],
                ))
            ->add('headOfOrganizationMobilePhoneCode', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 3,
                        'placeholder' => 'Код'),
                ))
            ->add('headOfOrganizationMobilePhone',  TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 7,
                        'placeholder' => 'Номер'),
                ))
            ->add('headOfOrganizationEmeil', TextType::class,
                array(
                    'label' => 'Адрес электронной почты',
                    'required' => false,
                ))
            ->add('headOfProjectLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headOfProjectLabel'],
                ))
            ->add('headOfProjectFullName', TextType::class,
                array(
                    'label' => 'ФИО',
                    'required' => false,
                ))
            ->add('headOfProjectPhoneLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headPhoneLabel'],
                ))
            ->add('headOfProjectPhoneInternationalCode', CustomTextType::class,
                array(
                    'help' => $this->description['internationalCode'],
                ))
            ->add('headOfProjectPhoneCode', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 3,
                        'placeholder' => 'Код'),
                ))
            ->add('headOfProjectPhone', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 7,
                        'placeholder' => 'Номер'),
                ))
            ->add('headOfProjectMobilePhoneLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headPhoneMobileLabel'],
                ))
            ->add('headOfProjectMobilePhoneInternationalCode', CustomTextType::class,
                array(
                    'help' => $this->description['internationalCode'],
                ))
            ->add('headOfProjectMobilePhoneCode', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 3,
                        'placeholder' => 'Код'),
                ))
            ->add('headOfProjectMobilePhone', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 7,
                        'placeholder' => 'Номер'),
                ))
            ->add('headOfProjectEmeil', TextType::class,
                array(
                    'label' => 'Адрес электронной почты',
                    'required' => false,
                ))
            ->add('headOfAccountingLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headOfAccountingLabel'],
                ))
            ->add('headOfAccountingFullName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'ФИО',
                ))
            ->add('headOfAccountingPhoneLabel', CustomTextType::class,
                array(
                    'help' => $this->description['headPhoneLabel'],
                ))
            ->add('headOfAccountingPhoneInternationalCode', CustomTextType::class,
                array(
                    'help' => $this->description['internationalCode'],
                ))
            ->add('headOfAccountingPhoneCode', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 3,
                        'placeholder' => 'Код'),
                ))
            ->add('headOfAccountingPhone', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 7,
                        'placeholder' => 'Номер'),
                ))
            ->add('headOfAccountingEmeil', TextType::class,
                array(
                    'label' => 'Адрес электронной почты',
                    'required' => false,
                ))
            ->add('bankCredentials', CustomTextType::class,
                array(
                    'help' => $this->description['bankCredentials'],
                ))
            ->add('iNN', TextType::class,
                array(
                    "label" => "ИНН",
                    "required" => false,
                    'attr' => array('maxlength' => 10),
                    'help' => 'ИНН должен состоять из 10 цифр'
                ))
            ->add('kPP', TextType::class,
                array(
                    "label" => "КПП",
                    "required" => false,
                    'attr' => array('maxlength' => 9),
                    'help' => 'КПП должен состоять из 9 цифр'
                ))
            ->add('oKPO',  TextType::class,
                array(
                    "label" => "ОКПО",
                    "required" => false,
                    'attr' => array('maxlength' => 10),
                    'help' => 'ОКПО должен состоять из 8 или 10 цифр'
                ))
            ->add('oKVED', TextType::class,
                array(
                    "label" => "ОКВЭД (через точку с запятой)",
                    "required" => false,
                    'attr' => array('maxlength' => 255)
                ))
            ->add('bankName', TextType::class,
                array(
                    "label" => "Наименование учреждения банка",
                    "required" => false,
                    'attr' => array('maxlength' => 255)
                ))
            ->add('bankLocation', TextType::class,
                array(
                    "label" => "Местонахождение банка ",
                    "required" => false,
                    'attr' => array('maxlength' => 255)
                ))
            ->add('bankINN', TextType::class,
                array(
                    "label" => "ИНН банка",
                    "required" => false,
                    'attr' => array('maxlength' => 10),
                    'help' => 'ИНН банка должен состоять из 10 цифр'
                ))
            ->add('bankKPP', TextType::class,
                array(
                    "label" => "КПП банка",
                    "required" => false,
                    'attr' => array('maxlength' => 9),
                    'help' => 'КПП банка должен состоять из 9 цифр'
                ))
            ->add('correspondentAccount', TextType::class,
                array(
                    "label" => "Корреспондентский счёт",
                    "required" => false,
                    'attr' => array('maxlength' => 20),
                    'help' => 'Корреспондентский счёт банка должен состоять из 20 цифр'
                ))
            ->add('bIK', TextType::class,
                array(
                    "label" => "БИК",
                    "required" => false,
                    'attr' => array('maxlength' => 9),
                    'help' => 'БИК банка должен состоять из 9 цифр'
                ))
            ->add('paymentAccount', TextType::class,
                array(
                    "label" => "Расчётный счёт",
                    "required" => false,
                    'attr' => array('maxlength' => 20),
                    'help' => 'Расчётный счёт банка должен состоять из 20 цифр'
                ))
            ->add('firstSectionComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий к разделу 1',
                    'attr' => array('maxlength' => 450)
                ))
            ->end()
            ->with('2. Организация-заявитель')
            ->add('dateRegistrationOfOrganization', DateType::class,
                array(
                    'required' => false,
                    'label' => '<b>Дата регистрации организации</b>',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-01-1900',
                        'maxlength' => 10
                    ]))
            ->add('primaryActivity', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Основные виды деятельности организации</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('mission', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Миссия организации</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('countEmployeesLabel', CustomTextType::class,
                array(
                    'help' => $this->description['countEmployeesLabel'],
                ))
            ->add('countStaffEmployees', TextType::class,
                array(
                    'required' => false,
                    'label' => 'штатных сотрудников, чел.',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('countInvolvingEmployees', TextType::class,
                array(
                    'required' => false,
                    'label' => 'привлеченных сотрудников, чел.',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('countVolunteerEmployees', TextType::class,
                array(
                    'required' => false,
                    'label' => 'добровольцев организации, чел.',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('grants', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Укажите, если за последние два года ваша Организация имела опыт получения грантов, субсидий, пожертвований, государственного/муниципального заказа и иного финансирования на конкурсной основе (по теме конкурса)',
                    'btn_add' => 'Добавить'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('publicationMaterials', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Публикации об организации за последние 2 года',
                    'btn_add' => 'Добавить'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('secondSectionComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий к разделу 2',
                    'attr' => array('maxlength' => 450)
                ))
            ->end()
            ->with('3. Описание практики')
            ->add('priorityDirections', 'sonata_type_model',
                array(
                    'choices' => $this->getDirectionsBy(['application', 'farvater_application']),
                    'required' => false,
                    'label' => 'Приоритетное направление Конкурса, в рамках которого реализуется практика',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('priorityDirectionEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Иные направления(укажите, какие именно)',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('targetGroups', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => 'Целевые группы практики',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('targetGroupEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Иные группы(укажите, какие именно)',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('childProtectionSpecialists', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Специалисты сферы защиты детства:',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('childrenCategories', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => '<b>Ориентация практики преимущественно на детей «сложных» категорий</b>',
                    "property" => "categoryName",
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $this->getChildrenCategoriesBy(array(
                        self::COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME,
                        self::FARVATER_ASSOCIATION_ENTITY_NAME,
                        self::FARVATER_AND_KNS_2016_ASSOCIATION_ENTITY_NAME
                    ))
                ))
            ->add('childrenCategoryName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Иные «сложные» категории детей',
                ))
            ->add('disabilitiesChildren', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Дети с ограниченными возможностями здоровья',
                ))
            ->add('specialNeedsEducationChildren', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Дети со специальными потребностями в обучении и др.',
                ))
            ->add('practicePreview', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Краткое описание практики',
                ))
            ->add('practicePreviewComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('practiceEvidence', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Доказательства эффективности практики',
                ))
            ->add('practiceEvidenceComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('practicePurpose', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Цель проекта в отношении представленной практики',
                ))
            ->add('practicePurposeComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('practiceMechanism', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Описание механизма достижения поставленной цели',
                ))
            ->add('practiceMechanismComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('practiceRegionLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceRegionLabel'],
                ))
            ->add('practiceRegion', 'sonata_type_model',
                array(
                    'required' => false,
                    "property" => "name",
                    'label' => false,
                    'attr' => array('placeholder' => 'Территория реализации')
                ))
            ->add('practiceRegionEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 450,
                        'placeholder' => 'Дополнительная информация')
                ))
            ->add('practiceResultsLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceResultsLabel'],
                ))
            ->add('practiceResults', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))













            ->add('immediateResultsLabel', CustomTextType::class,
                array(
                    'help' => sprintf($this->description['immediateResultsLabel'], $rukovodstvo_template_link),
                ))
            ->add('participatingTargetGroups', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<br><b>Количество представителей</b>',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('projectServicesLabel', CustomTextType::class,
                array(
                    'help' => $this->description['projectServicesLabel'],
                ))
            ->add('projectServices', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('immediateResultsComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('socialResultsLabel', CustomTextType::class,
                array(
                    'help' => $this->description['socialResults'],
                ))
            ->add('beneficiaryResults', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Результаты для благополучателей',
                    'btn_add' => 'Добавить'
                ),
                array(
                    'admin_code' => 'sonata.admin.nko.order.beneficiary_result',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('employeeResults', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Результаты для профессионального сообщества – специалистов сферы защиты детства',
                    'btn_add' => 'Добавить'
                ),
                array(
                    'admin_code' => 'sonata.admin.nko.order.employee_result',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('socialResultsComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('practiceExpectingResultsStabilityLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceExpectingResultsStabilityLabel']
                ))
            ->add('practiceExpectingResultsStability', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('thirdSectionComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий к разделу 3',
                    'attr' => array('maxlength' => 450)
                ))
            ->end()
            ->with('4. Распространение практики')
            ->add('targetAudiences', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => 'Целевая аудитория',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $this->getTargetAudiences(array('Farvater-2016'))
                ))
            ->add('targetAudienceEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Иные организации и специалисты в сфере защиты детства',
                ))
            ->add('practiceMotivation', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Мотивация, заинтересованность вашей организации в распространении и внедрении практики в деятельность других организаций и специалистов',
                ))
            ->add('practiceSpreadPurposeLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceSpreadPurposeLabel']
                ))
            ->add('practiceSpreadPurpose', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('practiceTech', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Технология распространения и внедрения практики',
                ))
            ->add('practiceTechComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('practiceExpectingSpreadResultsLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceExpectingSpreadResultsLabel']
                ))
            ->add('practiceExpectingSpreadResults', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('practiceSpreadLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceSpread'],
                ))
            ->add('awareTargetGroups', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<br>Количество представителей целевых групп, осведомленных о практике проекта',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('spreadActivitiesLabel', CustomTextType::class,
                array(
                    'help' => $this->description['spreadActivitiesLabel'],
                ))
            ->add('spreadActivities', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('publications', 'sonata_type_collection',
                array(
                                        'required' => false,
                    'label' => 'Публикации',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('participations', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Участие',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('introductions', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Внедрение',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('practiceSpreadComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('organizationResultsLabel', CustomTextType::class,
                array(
                    'help' => $this->description['organizationResultsLabel'],
                ))
            ->add('organizationResults', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('practiceResultsStabilityLabel', CustomTextType::class,
                array(
                    'help' => $this->description['practiceResultsStabilityLabel'],
                ))
            ->add('practiceResultsStability', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('fourthSectionComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий к разделу 4',
                    'attr' => array('maxlength' => 450)
                ))
            ->end()
            ->with('5. Мониторинг и оценка результатов')
            ->add('monitoringPurpose', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Цель проекта в отношении проведения мониторинга и оценки результатов проекта, повышения организационного потенциала в этой сфере',
                ))
            ->add('monitoringReviewLabel', CustomTextType::class,
                array(
                    'help' => $this->description['monitoringReviewLabel'],
                ))
            ->add('monitoringReview', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('organizationalCapacity', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Организационный потенциал в области планирования, мониторинга, измерения и оценки социальных проектов',
                ))
            ->add('organizationalCapacity', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Организационный потенциал в области планирования, мониторинга, измерения и оценки социальных проектов',
                ))
            ->add('monitoringComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                ))
            ->add('expectedMonitoringResults', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ожидаемые результаты проекта по блоку «Мониторинг и оценка результатов, повышение организационного потенциала»',
                ))
            ->end()
            ->with('6. План-график проекта')
            ->add('projectActivityLabel', CustomTextType::class,
                array(
                    'help' => $this->description['projectActivityLabel'],
                ))
            ->add('realizationPlans', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<br>Реализация практики проекта',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('spreadPlans', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Распространение практики проекта',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('monitoringPlans', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => 'Мониторинг и оценка результатов. Развитие организационного потенциала',
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('baseActivityComment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Комментарий',
                ))
            ->add('taskActivityLabel', CustomTextType::class,
                array(
                    'help' => $this->description['taskActivityLabel'],
                ))
            ->add('taskRealization', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Реализация практики проекта',
                ))
            ->add('taskSpread', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Распространение практики проекта',
                ))
            ->add('taskMonitoring', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Мониторинг и оценка результатов. Развитие организационного потенциала',
                ))
            ->end()
            ->with('7. Ресурсное обеспечение проекта')
            ->add('projectMembersLabel', CustomTextType::class,
                array(
                    'help' => $this->description['projectMembersLabel'],
                ))
            ->add('projectMembers', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('organizationResourcesLabel', CustomTextType::class,
                array(
                    'help' => $this->description['organizationResourcesLabel'],
                ))
            ->add('organizationResources', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('projectPartnersLabel', CustomTextType::class,
                array(
                    'help' => $this->description['projectPartnersLabel'],
                ))
            ->add('projectPartners', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => 'Добавить'
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with('8. Риски')
            ->add('riskLabel', CustomTextType::class,
                array(
                    'help' => 'Обстоятельства, которые могут воспрепятствовать успешной реализации Проекта, и действия, которые предприняты (или будут предприняты) для снижения рисков',
                ))
            ->add('risks', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => "Добавить",),
                array(
                    'admin_code' => 'sonata.admin.nko.order.risk',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with('9. Бюджет проекта')
            ->add('fullBudget', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сумма запрашиваемого финансирования на весь срок реализации проекта<em>(тысяч рублей)</em>',
                ))
            ->add('budgetYearLabel', CustomTextType::class,
                array(
                    'help' => sprintf($this->description['budgetYearLabel'], $budget_template_link),
                ))
            ->add('budgetYear', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->end()
            ->with('Приложения')
            ->add('appsLabel', CustomTextType::class,
                array(
                    'help' => $this->description['appsLabel']
                ))
            ->add('regulation', FilePreviewType::class,
                array(
                    'label' => '<br>Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа)',
                    'data_class' => null,
                    'required' => false
                ))
            ->add('authorityDirectorDocument', FilePreviewType::class,
                array(
                    'data_class' => null,
                    'required' => false,
                    'label' => 'Документ, подтверждающий полномочия руководителя (например, протокол об избрании, приказ, доверенность)'
                ))
            ->add('authorityManagerDocument', FilePreviewType::class,
                array(
                    'data_class' => null,
                    'required' => false,
                    'label' => 'Документ, подтверждающий полномочия лица, которое будет подписывать договор (в случае, если договор будет подписывать не руководитель организации)'
                ))
            ->add('budgetDocument', FilePreviewType::class,
                array(
                    'data_class' => null,
                    'required' => false,
                    'label' => 'Бюджет проекта – заполненный файл установленного образца'
                ))
            ->add('urStatusDocument', FilePreviewType::class,
                array(
                    'data_class' => null,
                    'required' => false,
                    'label' => 'Документ, подтверждающий статус юридического лица (решение о создании учреждения и т.п.)'
                ))
            ->add('lastYearReportDocument', FilePreviewType::class,
                array(
                    'label' => 'Последний годовой отчёт',
                    'data_class' => null,
                    'required' => false,
                ))
            ->add('lastYearReportLink', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Ссылка на последний годовой отчёт'
                ))
            ->add('mailBankDocument', FilePreviewType::class,
                array(
                    'data_class' => null,
                    'required' => false,
                    'label' => 'Письмо/справка из банка/территориального органа федерального казначейства о наличии счета'
                ))
            ->add('muReportMail', FilePreviewType::class,
                array(
                    'data_class' => null,
                    'required' => false,
                    'label' => 'Отчет в Министерство юстиции Российской Федерации /иной регистрирующий орган за предшествующий отчетный период'
                ))
            ->add('muReportMailLink', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Ссылка на отчет в Министерство юстиции Российской Федерации /иной регистрирующий орган за предшествующий отчетный период'
                ))
            ->add('personalDataLabel', CustomTextType::class,
                array(
                    'help' => $this->description['personalDataLabel'],
                ))
            ->add('sendingApplicationPersonFullName', TextType::class,
                array(
                    'label' => false,
                    'required' => false,
                    'attr' => array('maxlength' => 255),
                ))
            ->add('isCorrectInfo', CheckboxType::class,
                array(
                    'label' => 'Подтверждаю достоверность информации (в том числе документов), представленной в составе заявки на участие во Всероссийском конкурсе «Семейный фарватер»',
                    'required' => false,
                    'data' => true,
                ))
            ->add('contractLabel', CustomTextType::class,
                array(
                    'help' => sprintf($this->description['contractLabel'], $processing_template_link),
                ))
        ;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig')
        );

    }

    public function getDirectionsBy($application_types)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:Farvater\PriorityDirection')
            ->findBy(
                array('applicationType' => $application_types)
            );

        return $directions;
    }

    public function getChildrenCategoriesBy($associationEntityNames)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:ChildrenCategory')
            ->findBy(
                array('type' => $associationEntityNames)
            );
        return $directions;
    }

    public function getTargetAudiences($application_types)
    {
        $audiences = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:Farvater\TargetAudience')
            ->findBy(
                array('applicationType' => $application_types)
            );

        return $audiences;
    }

}