<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/3/17
 * Time: 11:14 PM
 */

namespace NKO\OrderBundle\Admin\Farvater;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Route\RouteCollection;

class TargetGroupIndexAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('name', TextType::class,
                array(
                    'required' => false,
                ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
        ;
    }
}