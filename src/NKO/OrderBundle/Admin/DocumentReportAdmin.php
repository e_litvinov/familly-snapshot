<?php

namespace NKO\OrderBundle\Admin;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\DocumentType;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Traits\NestedListOptionsTrait;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report;

class DocumentReportAdmin extends AbstractAdmin
{
    use NestedListOptionsTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('doctrine');

        $folders = [];
        $expenseTypes = [];
        if ($this->hasParentFieldDescription()) {
            $parent = $this->getParentFieldDescription()->getAdmin()->getSubject();
            $folders = $doctrine->getRepository('NKOOrderBundle:Folder')->findBy(['reportClass' => get_class($parent)]);
            $expenseTypes = $parent->getReportForm()->getExpenseTypes();
        }

        $query = $doctrine->getManager()->createQueryBuilder('f')
            ->select('f')
            ->from('NKOOrderBundle:Folder', 'f')
            ->where('f.parent is not null');

        $formMapper
            ->add('date', DateType::class,
                array(
                    'required' => false,
                    'label' => '2. Дата',
                    'widget' => 'single_text',
                    'format' => 'dd.MM.yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD.MM.YYYY',


                        'maxlength' => 10
                    ]))
            ->add('folder', null, array(
                'label' => "3. Выберите тип документа",
                'multiple' => false,
                'required' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('f')->where('f.parent is not null');
                }
            ))
            ->add('document_type', 'entity', array(
                'class'=> DocumentType::class,
                'label' => "4. Выберите, в которую из папок данного типа документ поместить",
                'multiple' => false,

                'required' => false
            ))
            ->add('expenseType', 'entity', array(
                'class' => ExpenseType::class,
                'choices' => $expenseTypes,
                'label' => "5. Выберите, к какой из таблиц РЕЕСТРА РАСХОДОВ в отчете относится документ",
                'multiple' => false,

                'required' => true
            ), array(
                'admin_code' => 'nko_order.admin.expense_type',
            ))
            ->add('row', IntegerType::class, array(
                'required' => false,
                'label' => "6. Выберите название строки данной таблицы, к которой относится документ"
            ))
            ->add('file', FilePreviewType::class,
                array(
                    'label' => '7. Загрузите файл в формате .pdf, максимальный размер - 50Mb',
                    'data_class' => null,
                    'required' => false
                ))
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        if ($this->getCurrentUser() instanceof NKOUser) {
            $datagridMapper
                ->add('report', null, array('operator_type' => 'hidden', 'advanced_filter' => false), 'entity', array(
                        'class' => Report::class,
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                                ->andWhere('u.psrn = :report')
                                ->setParameters(array('report' => $this->getCurrentUser()->getPsrn()));
                        }
                    )
                );
        } else {
            $datagridMapper
                ->add('report', null, array( 'operator_type' => 'hidden','advanced_filter' => false));
        }

        $datagridMapper
            ->add('document_type', null, array( 'operator_type' => 'hidden','advanced_filter' => false))
            ->add('folder', null, array( 'operator_type' => 'hidden','advanced_filter' => false))
            ->add('date', 'doctrine_orm_date_range', array('input_type' => DateType::class,
                'operator_type' => 'hidden','advanced_filter' => false))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->report_id = $this->getRequest()->query->get('report_id');

        $this->parameters = [
            'listmapper' => [
                'type' => 'PDF',
                'fieldName' => 'file'
            ]
        ];

        $listMapper
            ->add('file', 'string', array('template' => "NKOOrderBundle:Admin:preview_file_document.html.twig"))
            ->add('document_type')
            ->add('folder')
            ;
    }

    public function getTemplate($name)
    {
        switch($name) {
            case 'list':
                return 'NKOOrderBundle:CRUD:base_list.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

    private function getCurrentUser()
    {
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }
}