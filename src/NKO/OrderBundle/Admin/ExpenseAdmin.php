<?php

namespace NKO\OrderBundle\Admin;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as Report2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as Report2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as Report;

class ExpenseAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        Report2018::class => [
            'typeDocument' => 'Тип документа',
            'name' => 'Номер документа',
            'proofProductExpenseDocument' => false,
            'proofPaidGoodsDocument' => false,
            'sum' => 'Сумма, руб',
        ],
        Report2019::class => [
            'typeDocument' => 'Тип документа',
            'proofProductExpenseDocument' => false,
            'proofPaidGoodsDocument' => false,
            'name' => 'Наименование расхода',
            'sum' => 'Сумма, руб',
        ],
        MixedReport2019::class => [
            'typeDocument' => 'Тип документа',
            'proofProductExpenseDocument' => false,
            'proofPaidGoodsDocument' => false,
            'name' => 'Наименование расхода',
            'sum' => 'Сумма, руб',
        ],
        MixedReport2020::class => [
            'typeDocument' => 'Тип документа',
            'proofProductExpenseDocument' => false,
            'proofPaidGoodsDocument' => false,
            'name' => 'Наименование расхода',
            'sum' => 'Сумма, руб',
        ],
        Report::class => [
            'typeDocument' => 'Тип документа',
            'proofProductExpenseDocument' => 'Документ, подтверждающий произведенные
                    расходы (№, дата и наименование документа)',
            'proofPaidGoodsDocument' => 'Документ(-ы), подтверждающий (-ие)
                    получение оплаченных товаров/услуг (№, дата и наименование документа)',
            'name' => 'Наименование расхода',
            'sum' => 'Сумма, руб',
        ]
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $reportClass = get_class($this->getParentFieldDescription()->getAdmin()->getParentFieldDescription()->getAdmin()->getSubject());
        if ($reportClass == Report2018::class) {
            $formMapper
                ->add('typeDocument', 'sonata_type_model', [
                    'required' => true,
                    'btn_add' => false,
                    'label' => self::DESCRIPTION[$reportClass]['typeDocument']
                ], [
                    'admin_code' => 'nko_order.admin.indicator'
                ]);
        } else {
            $formMapper
                ->add('proofProductExpenseDocument', FullscreenTextareaType::class, [
                        'required' => false,
                        'label' => self::DESCRIPTION[$reportClass]['proofProductExpenseDocument'],
                    ])
                ->add('proofPaidGoodsDocument', FullscreenTextareaType::class, [
                        'required' => false,
                        'label' => self::DESCRIPTION[$reportClass]['proofPaidGoodsDocument'],
                ]);
        }

        $formMapper
            ->add('name', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => self::DESCRIPTION[$reportClass]['name'],
            ])
            ->add('sum', NumberType::class, [
                    'required' => false,
                    'label' => self::DESCRIPTION[$reportClass]['sum'],
            ])
        ;
    }
}
