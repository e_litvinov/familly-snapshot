<?php


namespace NKO\OrderBundle\Admin;

use Blameable\Fixture\Document\Type;
use NKO\OrderBundle\Entity\BaseReport;
use function Sodium\add;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NKO\OrderBundle\Entity\Organization;

class ReportAdmin extends BaseReportAdmin
{
    private $entityManager;

    const DESCRIPTION = [
        'deadLineLabel' => '<b>5. Сроки реализации</b> проекта',
        'authorizedPersonLabel' => '<b>7. Уполномоченное лицо</b>',
        'phoneLabel' => '<label class="control-label">Городской телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'phoneMobileLabel' => '<label class="control-label">Мобильный телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'difficultiesLabel' => '<b>14. Извлеченные уроки</b>',
        'costLabel' => '<b>I. РЕЕСТР РАСХОДОВ</b>',
        'financeMovingLabel' => '<b>III. ДВИЖЕНИЕ ДЕНЕЖНЫХ СРЕДСТВ</b>',
        'knowledgeIntroLabel' => '<b>10. ВНЕДРЕНИЕ ПОЛУЧЕННЫХ ЗНАНИЙ И ОПЫТА В ДЕЯТЕЛЬНОСТЬ ОРГАНИЗАЦИИ</b>',
        'resultsForEmployeesLabel' => '<b>11. РЕЗУЛЬТАТЫ ПРОЕКТА ДЛЯ СОТРУДНИКОВ ОРГАНИЗАЦИИ</b>',
        'resultsForBeneficiaryLabel' => '<b>12. РЕЗУЛЬТАТЫ ДЛЯ БЛАГОПОЛУЧАТЕЛЕЙ</b>',
        'financeSpentTotalLabel' => '<b>ВСЕГО:</b>',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with("Project Description")
            ->add('recipient', TextType::class, array(
                'label' => '<b>1. Получатель</b> пожертвования (полное наименование организации):',
                'required' => false
            ))
            ->add('contract', TextType::class, array(
                'label' => '<b>2. Договор</b> пожертвования №:',
                'required' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('projectName', TextType::class, array(
                'label' => '<b>3. Название проекта</b> :',
                'required' => false
            ))
            ->add('traineeshipTopic', 'sonata_type_model', array(
                'required' => false,
                "property" => "topicName",
                'label' => '<b>4. Тематика</b> стажировки  и стажировочная площадка:',
                'expanded' => true,
            ));

        $topicId = ($this->getSubject()
            && $this->getSubject()->getTraineeshipTopic())
            ? $this->getSubject()->getTraineeshipTopic()->getId() : null;

        $formMapper
            ->add('organization', 'sonata_type_model', array(
                    'query' => $this->getConfigurationPool()->getContainer()
                        ->get('Doctrine')->getRepository('NKOOrderBundle:Organization')->createQueryBuilder('p')
                        ->where('p.traineeshipTopicId = :id')
                        ->setParameter('id', $topicId)
                        ->getQuery(),
                    'property' => 'organizationName',
                    'expanded' => true,
                    'required' => false,
                    'btn_add' => false
            ))
            ->add('deadLineLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['deadLineLabel'],
            ))
            ->add('deadLineStart', DateType::class, array(
                    'required' => false,
                    'label' => 'с',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2016',
                        'data-date-end-date' => '15-12-2016',
                        'maxlength' => 10
                    ]
            ))
            ->add('deadLineFinish', DateType::class, array(
                    'required' => false,
                    'label' => 'по',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',

                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2016',
                        'data-date-end-date' => '15-12-2016',
                        'maxlength' => 10
                    ]
            ))
            ->add('sumGrant', MoneyType::class, array(
                    'label'=> '<b>6. Сумма</b> пожертвования, в руб.:',
                    'currency' => 'RUB',
                    'scale' => 2,
                    'required' => false,
                    'attr' => array(
                        'readonly' => true
                    )
            ))
            ->add('authorizedPersonLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['authorizedPersonLabel'],
            ))
            ->add('authorizedName', TextType::class, array(
                    'label'=>'ФИО:',
                    'required' => false
            ))
            ->add('authorizedPosition', TextType::class, array(
                    'label'=>'Должность:',
                    'required' => false
            ))
            ->add('phoneLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['phoneLabel'],
            ))
            ->add('authorizedPhoneCode', TextType::class, array(
                    'required' => false,
                    'label' => false,
                    'attr' => array(
                        'maxlength' => 3,
                        'placeholder' => 'Код'
                    )
            ))
            ->add('authorizedPhone', TextType::class, array(
                    'required' => false,
                    'label' => false,
                    'attr' => array(
                        'maxlength' => 7,
                        'placeholder' => 'Номер'
                    ),
            ))
            ->add('phoneMobileLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['phoneMobileLabel'],
            ))
            ->add('authorizedMobilePhoneCode', TextType::class, array(
                    'required' => false,
                    'label' => false,
                    'attr' => array(
                        'maxlength' => 3,
                        'placeholder' => 'Код'
                    ),
            ))
            ->add('authorizedMobilePhone', TextType::class, array(
                    'required' => false,
                    'label' => false,
                    'attr' => array(
                        'maxlength' => 7,
                        'placeholder' => 'Номер'
                    ),
            ))
            ->add('authorizedEmail', TextType::class, array(
                    'required' => false,
                    'label' => 'Адрес электронной почты:'
            ))
            ->add('purpose', TextareaType::class, array(
                    'required' => false,
                    'label' => '<b>8. Цель проекта:</b>',
                    'attr' => array('maxlength' => 450),
            ))
            ->add('events', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>9. Обучающие мероприятия, в которых приняли участие сотрудники организации в рамках проекта</b>',
                    'btn_add' => "Добавить",
                    'by_reference' => false,
                    'attr' => array(
                                'help' => "Перечислите все обучающие/стажировочные мероприятия, в которых приняли
                                участие и которые были проведены для сотрудников (членов) организации (общественного
                                объединения) в рамках проекта. Список участников каждого мероприятия должен быть
                                закачен в Приложениях отдельным файлом в формате pdf."
                        )), array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'admin_code' => 'sonata.admin.nko.order.training_event'
            ))
            ->end()
            ->with('Project Results')
            ->add('knowledgeIntroLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['knowledgeIntroLabel'],
            ))
            ->add('knowledgeUsage', null, array('label'=> '10.1. Как именно в организации <b>используются   знания   и   опыт</b>, полученные в ходе проекта? Перечислите конкретные действия, которые предприняты в период реализации проекта (до 15 декабря 2016 г.).',
                    'required' => false
            ))
            ->add('planOfKnowledgeUsage', null, array(
                    'label'=> '10.2 Как в дальнейшем  планируется   <b>использовать   знания   и   опыт</b>, полученные в рамках проекта? Перечислите конкретные действия, которые планируется предпринять в период до 01.06.2017.',
                    'required' => false
            ))
            ->add('resultsForEmployeesLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['resultsForEmployeesLabel'],
            ))
            ->add('immediateResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>11.1. Непосредственные результаты</b>',
                    'btn_add' => false,
                    'type_options' => array('delete' => false)
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table'
            ))
            ->add('employeeReportResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>11.2. Результаты, изменения у сотрудников организации (членов общественного объединения)</b>',
                    'btn_add' => "Добавить",
                    'by_reference' => false,
                    'attr' => array(
                        'help' => "Перечислите, какие ожидались качественные изменения у сотрудников (членов)
                               организации, которые произойдут в период реализации проекта (согласно заявке). Укажите
                               планируемые и фактически достигнутые целевые значения. В случае различия целевых значений
                               Плана/Факта, укажите в поле «Комментарий», почему так произошло.<br>
                               В случае,если достигнуты также иные, не указанные в заявке результаты, то также их укажите."
                    )), array(
                    'edit' => 'inline',
                    'inline' => 'table'
            ))
            ->add('resultsForBeneficiaryLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['resultsForBeneficiaryLabel'],
            ))
            ->add('beneficiaryGroups', 'sonata_type_model', array(
                    'required' => false,
                    'by_reference' => false,
                    'label' => '<b>12.1. Основные целевые группы благополучателей</b> (согласно заявке)',
                    "property" => "groupName",
                    'expanded' => true,
                    'multiple' => true
            ))
            ->add('beneficiaryGroupName', TextType::class, array(
                    'required' => false,
                    'label' => 'иные группы (укажите, какие именно)',
            ))
            ->add('beneficiaryReportResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>12.2. Результаты, изменения у благополучателей</b>',
                    'btn_add' => "Добавить",
                    'by_reference' => false,
                    'attr' => array(
                        'help' => "Перечислите, какие ожидались качественные изменения у благополучателей организации,
                                   которые произойдут в период реализации проекта (согласно заявке). Укажите планируемые и
                                   фактически достигнутые целевые значения. В случае различия целевых значений Плана/Факта,
                                   в поле «Комментарий» укажите, почему так произошло.<br>В случае, если достигнуты также иные,
                                   не указанные в заявке результаты, то также их укажите."
                    )), array(
                    'edit' => 'inline',
                    'inline' => 'table'
            ))
            ->add('pastEvents', null, array(
                'label' => '<b>13. Были   ли   проведены   все   мероприятия,   которые   планировалось   провести   в   отчетный период   в   полном   объеме   и   в   запланированные   сроки?</b>  Если нет, то укажите, какие именно мероприятия не были проведены в срок (или в полном объеме) и почему.',
                'required' => false
                ))
            ->add('difficultiesLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['difficultiesLabel'],
            ))
            ->add('difficulties', null, array(
                'label'=> '<b>14.1. Трудности</b> или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс реализации Проекта; «мелочи», которые упустили, но которые оказались важны (в отношении практики). ',
                'required' => false
            ))
            ->add('solvingProblemsWays', null, array(
                'label'=> '<b>14.2. Пути   решения   проблем</b>/способы выхода из затруднительных ситуаций. Краткий анализ, что мешало, что помогало.',
                'required' => false
            ))
            ->add('recommendations', null, array(
                'label'=> '<b>14.3. Рекомендации «самим себе»</b> по подготовке, реализации, мониторингу и оценке проекта.',
                'required' => false
            ))
            ->add('successStory', null, array(
                'label'=>'<b>15. Истории успеха</b>, чем гордитесь',
                'required' => false
            ))
            ->add('feedback', null, array(
                'label' => '<b>16. Прямая   обратная   связь   от   сотрудников   (членов)   организации   и   благополучателей</b> (наиболее показательная)',
                'required' => false
            ))
            ->end()
            ->with('Financial Report')
            ->add('costLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['costLabel'],
                ))
            ->add('transportCosts', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>1. Транспортные и прочие расходы на поездку к месту стажировки/обучающего мероприятия</b>',
                    'btn_add' => "Добавить",
                    'type_options' => array(
                        'delete' => true,
                    )
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('totalTransportCost', TextType::class, array(
                'required' => false,
                'label' => '<b>ИТОГО</b>',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('involvedCosts', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>2. Оплата привлеченных организаций/ специалистов (включая налоги и страховые взносы) и сопутствующие расходы</b>',
                    'btn_add' => "Добавить",
                    'type_options' => array(
                        'delete' => true,
                    )
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('totalInvolvedCost', TextType::class, array(
                'required' => false,
                'label' => '<b>ИТОГО</b>',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('eventCosts', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>3. Расходы на последующие мероприятия по внедрению полученных знаний и опыта</b>',
                    'btn_add' => "Добавить",
                    'type_options' => array(
                        'delete' => true,
                    )
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('totalEventCost', TextType::class, array(
                'required' => false,
                'label' => '<b>ИТОГО</b>',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('otherCosts', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>4. Иные расходы</b>',
                    'btn_add' => "Добавить",
                    'type_options' => array(
                        'delete' => true,
                    )
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('totalOtherCost', TextType::class, array(
                'required' => false,
                'label' => '<b>ИТОГО</b>',
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('financeSpent', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>II. ИЗРАСХОДОВАНО СРЕДСТВ</b>',
                    'btn_add' => false,
                    'by_reference' => false,
                    'type_options' => array(
                        'delete' => false,
                    )
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.finance_spent',
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('financeSpentTotalLabel', CustomTextType::class, [
                'help' => self::DESCRIPTION['financeSpentTotalLabel']
            ])
            ->add('financeMovingLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['financeMovingLabel'],
            ))
            ->add('receiptOfDonations', TextType::class, array(
                'label' => '<b>Поступление пожертвования</b>',
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('periodCosts', TextType::class, array(
                'label' => '<b>Расходы отчетного периода</b>',
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('donationBalance', TextType::class, array(
                'label' => '<b>Остаток суммы пожертвования</b> (при наличии)',
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('confirmingDocument', TextType::class, array(
                'label' => '<b>Документ, подтверждающий поступление средств:</b>',
                'required' => false,
            ))
            ->end()
            ->with('Attachment')
            ->add('documents', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => 'Документы',
                    'by_reference' => false,
                    'btn_add' => "Добавить",
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'admin_code' => 'sonata.admin.nko.order.old_document_report',
            ))
            ->end()
        ;
        $admin = $this;
        $formMapper->getFormBuilder()->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) use ($formMapper, $admin) {
                $form = $event->getForm();
                if ($form->has('organization')) {
                    $form->remove('organization');
                }

                $data = $event->getData();

                $organizations = $admin->getConfigurationPool()->getContainer()
                    ->get('Doctrine')->getRepository('NKOOrderBundle:Organization')->createQueryBuilder('p')
                    ->where('p.traineeshipTopicId = :id')
                    ->setParameter('id', $data['traineeshipTopic'])
                    ->getQuery()
                    ->getResult();

                $form->add('organization', EntityType::class, [
                    'class' => Organization::class,
                    'choices' => $organizations,
                    'expanded' => true,
                    'required' => false,
                ]);
            }
        );
    }

    public function __construct($code, $class, $baseControllerName, $em)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->entityManager = $em;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        if (!in_array('ROLE_NKO', $this->getCurrentUser()->getRoles())) {
            $datagridMapper
                ->add('recipient')
            ;
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('redirect_to_documents_list', $this->getRouterIdParameter().'/redirect_to_document_list');
        $collection->add('download_report', $this->getRouterIdParameter().'/download_report');
        $collection->add('send_report', $this->getRouterIdParameter() . '/send_report');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('recipient')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array( 'template' =>  'NKOOrderBundle:CRUD:list__action_edit_report.html.twig'),
                    'send_report' => array( 'template' =>  'NKOOrderBundle:CRUD:list__action_send_report.html.twig'),
                    'download_report' => array( 'template' =>  'NKOOrderBundle:CRUD:list__action_download_report.html.twig'),
                    'redirect_to_documents_list' => array('template' => 'NKOOrderBundle:CRUD:list__action_download_document_report.html.twig')
                )
            ));
    }
}
