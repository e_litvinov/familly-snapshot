<?php

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\Common\Collections\ArrayCollection;

class DocumentTypeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('documentType');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {

    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
            ->addIdentifier('documentType');
    }

    protected function configureShowFields(ShowMapper $showMapper) {

    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('delete')
        ;
    }

    public function prePersist($object)
    {
        $this->preUpdate($object);
    }

    public function preUpdate($object)
    {
        $object->setDocuments($object->getDocuments());

    }

}