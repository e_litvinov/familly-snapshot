<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/1/17
 * Time: 9:07 AM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProblemAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('targetGroup', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Целевая группа (благополучатели) Практики',
                    'attr' => array(
                        'placeholder' => '-',
                        'maxlength' => 255)
                ))
            ->add('mainProblems', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Основные проблемы целевой группы, на решение которых направлена Практика',
                    'attr' => array(
                        'placeholder' => '-',
                        'maxlength' => 4000)
                ))
            ->add('expectedResults', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Социальные результаты применения Практики для целевых групп',
                    'attr' => array(
                        'placeholder' => '-',
                        'maxlength' => 4000)
                ))
        ;
    }
}