<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/2/17
 * Time: 10:49 AM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PeopleCategoryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Поддержанные Фондом Практики',
                ))
        ;
    }
}