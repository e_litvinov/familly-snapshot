<?php

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use NKO\OrderBundle\Form\DropDownListType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Sonata\AdminBundle\Route\RouteCollection;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\OrganizationActivityAdmin;

class BriefApplicationAdmin extends BaseApplicationAdmin
{
    const ORGANIZATION_INFO_DESCRIPTION = [
        'dateRegistrationOfOrganization' => '2.1 <b>Дата регистрации организации</b>',
        'primaryActivity' => '2.2 <b>Основные виды деятельности организации</b>(согласно Уставу, соответствующие направлениям Конкурса и предлагаемому проекту)',
        'mission'=>'2.3 <b>Миссия</b> организации (при наличии)',
        'countEmployeesLabel'=>'2.4 Количество <b>сотрудников и добровольцев</b> организации',
        'projects' => '2.5 <b>Основные реализованные проекты (программы) организации</b> за последние два года (по теме данного Конкурса)',
        'publications'=>'2.6 <b>Публикации об организации за последние 2 года.</b><br><br><i>Укажите не более 5 релевантных материалов</i>',
        'linkToAnnualReport'=> '2.7 Ссылка на последний <b>годовой отчет</b> Организации (при наличии)',
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                "BriefApplication-2017",
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting'
            ]
        );

        $this->description = $this->description + [
                'countEmployeesLabel' => 'Количество <b>сотрудников и добровольцев</b> организации',
                'targetGroupsLabel' => '<b>Проблемы целевых групп, на решение которых направлена Практика; социальные результаты применения Практики для целевых групп</b>',
                'consentLabel' => 'Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке<br>',
                'personalDataLabel' => '<b>Данные лица, подающего заявку</b><br>
                    Руководитель организации или лицо, которое будет подписывать договор в случае поддержки проекта (если договор будет подписывать не руководитель организации).',
                'documentLabel' => '<b>При подаче заявки необходимо предоставить в электронном виде скан-копии следующих документов, заверенных подписью руководителя и печатью организации, и оформленных согласно инструкции <a href="http://deti.timchenkofoundation.org/informationmaterials/33" target="_blank">(ссылка)</a></b><br><br>
                    <b>Все документы должны иметь формат .pdf, максимальный размер файла - 50Mb. Закачиваемый файл нельзя архивировать. Название должно состоять из букв на английском языке и цифр без пробелов и иметь единственную точку – перед расширением, пример: a1.pdf. Система при заливке название файла меняет.</b>',
                'practiceDescriptionLabel' => '<em>В данном разделе описывается конкретная Практика вашей организации (модель, технология, методика, услуга и пр.):
                    <ul>
                    <li>которая направлена на профилактику сиротства или на семейное устройство детей-сирот и детей, оставшихся без попечения родителей;</li>
                    <li>которая уже успешно используется вашей организацией (в чем особенно сильна ваша организация, ваша «изюминка»);</li>
                    <li>в отношении которой уже есть данные, позволяющие делать вывод об ее эффективности;</li>
                    <li>опытом использования которой ваша организация готова делиться с другими специалистами и организациями.</li>
                    </ul>
                    В сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей, применяется много различных Практик. Внимательно ознакомьтесь с перечнем Практик победителей Конкурса «Семейный фарватер» (<a href="http://deti.timchenkofoundation.org/informationmaterials/24" target="_blank">ссылка</a>). Для Фонда интересно развитие таких эффективных Практик, которые: <br>
                    <ul>
                    <li>не являются полным аналогом ранее поддержанных </li>
                    или
                    <li>могут позитивно сказаться на развитии ранее поддержанных Практик (не обязательно).</li>
                    </ul></em>',
                'beneficiaryGroupsLabel' => '<br>3.4.1 . <b>Целевые группы (благополучатели) Практики</b><br>
                    <em>Конкурс «Семейный фарватер» и Программа «Семья и дети» реализуются, чтобы в жизни конкретных групп людей произошли позитивные изменения. Основные группы благополучателей представлены ниже. Укажите, на какие именно группы благополучателей преимущественно направлена ваша Практика. Вы можете также дополнительно добавить те группы благополучателей, которые отсутствуют в данном списке, <b>но не более 3.</b></em>',
                'problemsLabel' => '<b>Основные проблемы целевых групп и социальные результаты, полученные по итогам реализации Практики</b><br>
                    <em>Укажите основные проблемы тех целевых групп (благополучателей), на решение которых направлена ваша Практика. Что изменяется в жизни благополучателей (целевых групп) благодаря применению вашей Практики? Например, изменение установок, мотивов, личностных ресурсов, психологического состояния, позиции представителей значимого окружения, развитие новых навыков преодоления проблемной ситуации, навыков адаптации, появление поддерживающих ресурсов, и т.п.</em>',
                'socialResultsLabel' => '<b>Программа «Семья и дети» Фонда Тимченко направлена на достижение следующих социальных результатов:</b><br><br>
                    <ul>
                    <em><li>Увеличение числа детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства (в том числе подростков, детей с ОВЗ, сиблингов);</li><br>
                    <li>Увеличение числа детей, возвращенных в кровные семьи (в том числе подростков и детей с ОВЗ);</li><br>
                    <li>Уменьшение количества отобраний (изъятий)/отказов детей из кровных и замещающих семей;</li><br>
                    <li>Улучшение благополучия детей и семей – участников Программы;</li><br>
                    <li>Рост уровня готовности детей к самостоятельной жизни – они становятся полноценными гражданами, обеспечивающими благополучие общества.</li><br>
                    </ul>
                    В рамках реализации Программы Фонд осуществляет мониторинг по показателям, указанным ниже.<br><br></em>
                    <b>Укажите сведения о целевых значениях показателей, достигнутых в ходе реализации вашей Практики (не организации!) за 2016 год. </b><br><br>
                    <em>Обратите также внимание, что отчётность организаций-победителей Конкурса также преимущественно ориентирована на предоставление сведений о достижении социальных результатов, важных для Фонда.<br><br>
                    В случае, если ваша Практика не работает на тот или иной показатель, поставьте прочерк.<br><br>
                    В случае, если у вас нет точных сведений о целевых значениях того или иного показателя, укажите ориентировочное число.</em>',
                'achievementResultMechanizmLabel' => '<b>Механизм достижения социальных результатов для целевых групп</b><br>
                    <em>Что конкретно вы делаете в процессе реализации Практики и почему?<br>
                    Ваша задача – дать максимально полное представление о том, из чего состоит ваша Практика. Перечислите основные действия (мероприятия, услуги, процессы, виды деятельности и т.п.), которые необходимо произвести в рамках реализации Практики.<br>
                    Укажите также, как именно, за счет чего происходят позитивные изменения у целевых групп, какие социальные и психологические механизмы запускает ваша Практика.<br></em>',
                'effectivenessPracticeDataLabel' => '<b>Данные об эффективности Практики</b><br>
                    <em>Какие есть данные о том, что ваша Практика эффективна в достижении тех социальных результатов, которые вы указали в п.3.4? Какие уже получены результаты? Как вы об этом узнали? Какие показатели и инструменты используются?<br>
                    Если проводились внутренние и внешние оценочные исследования вашей практики (как вашей организацией, так и иными), публикации на профессиональных ресурсах и пр. или мониторинг достигнутых результатов, то укажите, где можно с ними ознакомиться (ссылки и пр.).</em>',
                'sourcesLabel' => '<b>Ресурсы</b> на реализацию Практики<br>',
                'financingSourcesLabel' => '<br>3.8.2 <b>Основные источники финансирования на реализацию Практики</b>(в 2016 году)<br>
                    <em>Укажите, за счет каких средств сейчас реализуется Практика в вашей организации? Укажите примерную долю в % по каждому источнику.</em>',
                'practiceRegionLabel' => '<b>Территория реализации Практики</b><br>
                    <em>В каком регионе (регионах) РФ ваша организация сейчас реализует представленную на Конкурс Практику?</em>',
                'postConsentLabel' => 'Для каждого из лиц заполняется отдельный документ, если они не совпадают<br><br>
                    <em>При заключении договора, в случае победы в Конкурсе, необходимо будет представить оригиналы (с подписью) Информированного согласия физического лица на обработку персональных данных в рамках организации и проведения II Всероссийского конкурса «Семейный фарватер», заполненных отдельно на каждого члена команды проекта, включая руководителя и бухгалтера организации.</em><br><br>
                    <a href="%s" target="_blank">Скачать образец согласия на обработку персональных данных</a><br><br>',
                'countPracticeEmployeesLabel' => 'Работники и добровольцы организации, <b>задействованные в реализации Практики</b> (на момент подачи заявки)<br>
                    <em>Укажите, сколько специалистов организации, являющихся штатными или привлеченными сотрудниками, вовлечено в реализацию Практики на момент подачи заявки; сколько добровольцев.</em>',
                'briefPracticeDescriptionLabel' => '<b>Аннотация: краткое описание сути Практики</b><br>
                    <em>Кратко опишите, в чем суть представляемой вами на Конкурс Практики<br>
                    Внимательно изучите Практики, которые уже поддержаны Фондом (победители Конкурса «Семейный фарватер»), которые работают в выбранных вами приоритетных направлениях (п.3.2). Важно указать, в чём отличие, уникальность именно вашей Практики; либо обоснуйте, как поддержка вашего проекта может усилить уже поддержанные Фондом Практики <a href="http://deti.timchenkofoundation.org/informationmaterials/34" target="_blank">(ссылка)</a>.</em>'
            ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($competition_id = $this->getRequest()->get('competition')) {
            $competition = $this->getConfigurationPool()->getContainer()
                ->get('Doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->find($competition_id)
            ;

            $isCurrentCompetition = $this->getConfigurationPool()->getContainer()
                ->get('Doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->isCurrentCompetition($competition);

            if ($isCurrentCompetition) {
                $this->getSubject()->setCompetition($competition);
            }
        }
        $processing_template_link = $this->getConfigurationPool()->getContainer()->get('assets.packages')->getUrl('bundles/nkoorder/resources/personal_data_processing_template.docx');

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper
            ->with('ВКЛАДКА I. КОНТАКТНАЯ ИНФОРМАЦИЯ');
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], $em);
        $formMapper
            ->end()
            ->with('ВКЛАДКА II. ОРГАНИЗАЦИЯ-ЗАЯВИТЕЛЬ');
        OrganizationActivityAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], null, self::ORGANIZATION_INFO_DESCRIPTION);
        $formMapper
            ->end()
            ->with('ВКЛАДКА III. ОПИСАНИЕ ПРАКТИКИ')
            ->add('practiceDescriptionLabel', CustomTextType::class, [
                    'help' => $this->description['practiceDescriptionLabel'],
            ])
            ->add('projectName', TextType::class, [
                    'required' => false,
                    'label' => '3.1 <b>Название Практики</b>',
                    'attr' => array('maxlength' => 350),
            ])
            ->add('priorityDirection', 'sonata_type_model', [
                    'choices' => $this->getDirectionsBy(['application', 'brief_application']),
                    'label' => '3.2 <b>Приоритетное направление Конкурса</b>, в рамках которого реализуется практика',
                    'property' => 'name',
                    'expanded' =>true,
                    'multiple' => false,
                    'required' => false,
                    'help' => 'Выберите, пожалуйста, одно направление или впишите своё в поле “Иные эффективное практики”.'
            ])
            ->add('priorityDirectionEtc', TextType::class, [
                    'required' => false,
                    'label' => '<b>Иные эффективные практики</b> в сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей (укажите, какие именно)',
                    'attr' => array('maxlength' => 255)
            ])
            ->add('briefPracticeDescriptionLabel', CustomTextType::class, [
                    'help' => '3.3 ' . $this->description['briefPracticeDescriptionLabel'],
            ])
            ->add('briefPracticeDescription', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 1800)
            ])
            ->add('targetGroupsLabel', CustomTextType::class, [
                    'help' => '3.4 ' . $this->description['targetGroupsLabel'],
            ])
            ->add('beneficiaryGroupsLabel', CustomTextType::class, [
                    'help' => $this->description['beneficiaryGroupsLabel'],
            ])
            ->add('peopleCategories', DropDownListType::class, [
                    'class' => 'NKOOrderBundle:BriefApplication2017\PeopleCategory',
                    'required' => false,
                    'label' => false,
                    'multiple' => true
            ])
            ->add('otherBeneficiaryGroups', 'sonata_type_collection', [
                    'required' => false,
                    'label' => '<b>Иные группы</b>(укажите)',
                    'btn_add' => "Добавить",
                    'help' => '<em>Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.</em>',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table'
                ])
            ->add('problemsLabel', CustomTextType::class, [
                    'help' => '3.4.2 ' . $this->description['problemsLabel'],
            ])
            ->add('problems', 'sonata_type_collection', [
                    'required' => false,
                    'label' => false,
                    'btn_add' => "Добавить",
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('achievementResultMechanizmLabel', CustomTextType::class, [
                    'help' => '3.5 ' . $this->description['achievementResultMechanizmLabel'],
            ])
            ->add('achievementResultMechanizm', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 4000)
            ])
            ->add('effectivenessPracticeDataLabel', CustomTextType::class, [
                    'help' => '3.6 ' . $this->description['effectivenessPracticeDataLabel'],
            ])
            ->add('effectivenessPracticeData', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 4000)
            ])
            ->add('socialResultsLabel', CustomTextType::class, [
                    'help' => '3.7 ' . $this->description['socialResultsLabel'],
            ])
            ->add('socialResults', 'sonata_type_collection', [
                    'required' => false,
                    'label' => false,
                    'btn_add' => false,
                    'help' => 'Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table'
                ])
            ->add('sourcesLabel', CustomTextType::class, [
                    'help' => '3.8 ' . $this->description['sourcesLabel'],
            ])
            ->add('countPracticeEmployeesLabel', CustomTextType::class, [
                    'help' => '3.8.1 ' . $this->description['countPracticeEmployeesLabel'],
            ])
            ->add('countPracticeStaffEmployees', TextType::class, [
                    'required' => false,
                    'label' => 'Количество штатных сотрудников, чел.',
                    'attr' => array('maxlength' => 10)
            ])
            ->add('countPracticeInvolvingEmployees', TextType::class, [
                    'required' => false,
                    'label' => 'Количество привлеченных специалистов, чел.',
                    'attr' => array('maxlength' => 10)
            ])
            ->add('countPracticeVolunteerEmployees', TextType::class, [
                    'required' => false,
                    'label' => 'Количество добровольцев, чел.',
                    'attr' => array('maxlength' => 10)
            ])
            ->add('financingSourcesLabel', CustomTextType::class, [
                    'help' => $this->description['financingSourcesLabel'],
            ])
            ->add('financingSources', 'sonata_type_collection', [
                    'required' => false,
                    'label' => false,
                    'btn_add' => "Добавить",
                    'error_bubbling' => false,
                    'by_reference' => false
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('financingSourceEtc', FullscreenTextareaType::class, [
                    'required' => false,
                    'label' => 'Другое (указать, что именно)',
                    'attr' => array('maxlength' => 500)
            ])
            ->add('practiceRegionLabel', CustomTextType::class, [
                    'help' => '3.9 ' . $this->description['practiceRegionLabel'],
            ])
            ->add('practiceRegions', 'sonata_type_model', [
                    'required' => false,
                    'property' => "name",
                    'label' => false,
                    'multiple' => true,
                    'choices' => $this->getConfigurationPool()
                        ->getContainer()
                        ->get('Doctrine')
                        ->getManager()
                        ->getRepository('NKOOrderBundle:Region')
                        ->findBy(array(), array('name' => 'ASC')),
                    'attr' => array('class' => 'brief_application_practice_region')
            ])
            ->end()
            ->with('ВКЛАДКА IV. ДОКУМЕНТЫ')
            ->add('documentLabel', CustomTextType::class, [
                    'help' => $this->description['documentLabel'],
            ])
            ->add('regulation', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => '<br>1. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа'
            ])
            ->add('authorityDirectorDocument', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => '2. Документ, подтверждающий полномочия руководителя (например, протокол об избрании, приказ, доверенность)'
            ])
            ->add('consentLabel', CustomTextType::class, [
                    'help' => '3. ' . $this->description['consentLabel'] . '<br>Согласие на обработку персональных данных заполняется следующими лицами:',
            ])
            ->add('organizationHeadConsent', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => '1. руководителем организации;'
            ])
            ->add('projectHeadConsent', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => '2. руководителем проекта'
            ])
            ->add('accountantHeadConsent', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => '3. главным бухгалтером;'
            ])
            ->add('sendingApplicationPersonConsent', FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => '4. лицом, подающим заявку'
            ])
            ->add('postConsentLabel', CustomTextType::class, [
                    'help' => sprintf($this->description['postConsentLabel'], $processing_template_link),
            ])
            ->add('personalDataLabel', CustomTextType::class, [
                    'help' => '4. ' . $this->description['personalDataLabel'],
            ])
            ->add('sendingApplicationPersonFullName', TextType::class, [
                    'label' => 'ФИО',
                    'required' => false,
                    'attr' => array('maxlength' => 255),
            ])
            ->add('isCorrectInfo', CheckboxType::class, [
                    'label' => 'Подтверждаю достоверность информации (в том числе документов), представленной в составе заявки на участие во II Всероссийском конкурсе «Семейный фарватер».',
                    'required' => false,
                    'data' => true,
            ]);
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig')
        );
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('content', 'html');
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('send', $this->getRouterIdParameter().'/send')
            ->remove('show')
            ->remove('list')
        ;
        parent::configureRoutes($collection);
    }

    public function getDirectionsBy($application_types)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository(PriorityDirection::class)
            ->findBy(
                array('applicationType' => $application_types)
            );

        return $directions;
    }

    /**
     * @param string $action
     * @param mixed  $object
     *
     * @return array
     */
    public function getActionButtons($action, $object = null)
    {
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $countCompetitions = $em
            ->getRepository("NKOOrderBundle:Competition")
            ->findCountAvailiableUserCompetition($user);

        $list = $this->configureActionButtons($action, $object);

        foreach ($this->getExtensions() as $extension) {
            // TODO: remove method check in next major release
            if (method_exists($extension, 'configureActionButtons')) {
                $list = $extension->configureActionButtons($this, $list, $action, $object);
            }
        }

        foreach ($list as $key => &$value) {
            if ($key == 'create' && !$countCompetitions) {
                unset($list[$key]);
            }
            if ($key == 'list') {
                foreach ($value as $k => &$v) {
                    if ($k =='template') {
                        $v = 'NKOOrderBundle:Button:list_button.html.twig';
                    }
                }
            }
            if ($key == 'create') {
                foreach ($value as $k => &$v) {
                    if ($k =='template') {
                        $v = 'NKOOrderBundle:Button:create_button.html.twig';
                    }
                }
            }
        }

        return $list;
    }
}
