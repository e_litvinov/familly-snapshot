<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/12/17
 * Time: 2:41 PM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SiteLinkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('link', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сайт организации в сети Интернет',
                    'attr' => array(
                        'help-bottom' => 'URL должен начинаться с http:// или https://',
                        'placeholder' => '-'),
                ));
    }
}