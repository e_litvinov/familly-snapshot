<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/1/17
 * Time: 9:55 AM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FinancingSourceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('sourceName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Собственные средства организации',
                    'attr' => array('maxlength' => 255),
                    'disabled' => true,
                ))
            ->add('percent', 'number',
                array(
                    'required' => false,
                    'label' => 'Число от 0 до 100 с плавающей точкой',
                    'scale' => 2,
                ))
        ;
    }
}