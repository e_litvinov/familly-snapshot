<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/1/17
 * Time: 9:23 AM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextAreaType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class SocialResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Показатели',
                    'attr' => array(
                        'readonly' => true
                    ))
                )
            ->add('achievedValue', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Достигнутые значения показателя (2016 год)',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
        ;
    }
}