<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/12/17
 * Time: 8:05 PM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SocialNetworkLinkAdmin extends AbstractAdmin 
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('link', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Страницы организации в социальных сетях',
                    'attr' => array(
                        'help-bottom' => 'URL должен начинаться с http:// или https://',
                        'placeholder' => '-'
                    )
                ));
    }

}