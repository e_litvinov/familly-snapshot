<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/1/17
 * Time: 12:33 AM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OtherBeneficiaryGroupAdmin extends EmbeddedAdminDecorator
{
    const DESCRIPTION = 'Целевая группа (благополучатели) Практики';
    const SPECIALIST_DESCRIPTION = 'Иные целевые группы специалистов';
    const SPECIALIST_TARGET_GROUP_FIELD = 'otherSpecialistTargetGroups';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $description = self::DESCRIPTION;
        if ($this->getParentFieldDescription()->getFieldName() == self::SPECIALIST_TARGET_GROUP_FIELD) {
            $description = self::SPECIALIST_DESCRIPTION;
        }
        $formMapper
            ->add('name', TextType::class, [
                    'required' => false,
                    'label' => $description,
                    'attr' => [
                        'maxlength' => 255,
                        'limit' => 3,
                        'placeholder' => '-'
                    ]
            ])
        ;
    }
}
