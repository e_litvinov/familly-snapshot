<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/31/17
 * Time: 12:43 PM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class PublicationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('publicationName', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Название публикации',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('link', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Ссылка на публикацию (если есть)',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
        ;
    }
}