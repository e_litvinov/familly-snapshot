<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/31/17
 * Time: 10:00 AM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ProjectAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('deadline', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Период реализации',
                    'attr' => array(
                        'placeholder' => '-',
                        'maxlength' => 255)
                ))
            ->add('projectName', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Название проекта',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('index', 'sonata_type_model',
                array(
                    'required' => false,
                    "property" => "name",
                    'label' => 'Источник финансирования',
                    'btn_add' => false,
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('result', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Основные результаты',
                    'attr' => array(
                        'placeholder' => '-',
                        'maxlength' => 1500)
                ))
        ;
    }
}