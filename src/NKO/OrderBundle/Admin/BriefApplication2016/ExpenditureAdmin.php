<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/24/17
 * Time: 12:55 PM
 */

namespace NKO\OrderBundle\Admin\BriefApplication2016;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ExpenditureAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
            ->add('purpose', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Цель',
                ))
            ->add('requestedMoney', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Запрашиваемая сумма, тысяч рублей',
                ))
            ->add('financing', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Софинансирование, тысяч рублей',
                ))
            ->add('comment', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Обоснование / комментарий',
                ))
        ;
    }

}