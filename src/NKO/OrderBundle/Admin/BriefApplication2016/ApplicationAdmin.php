<?php

namespace NKO\OrderBundle\Admin\BriefApplication2016;

use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;

class ApplicationAdmin extends BaseApplicationAdmin
{
    CONST FARVATER_ASSOCIATION_ENTITY_NAME = 'farvater';
    CONST COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME  = 'application';
    CONST FARVATER_AND_KNS_2016_ASSOCIATION_ENTITY_NAME = 'farvaterAndKNS2016';

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->description = $this->description + [
                'countEmployeesLabel' => '<b>Количество штатных,привлеченных и добровольных сотрудников организации на дату подачи заявки</b>'
            ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper
            ->with('1. Контактная информация');
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], $em);
        $formMapper
            ->end()
            ->with('2. Организация-заявитель')
            ->add('dateRegistrationOfOrganization', DateType::class,
                array(
                    'required' => false,
                    'label' => '<b>Дата регистрации организации</b>',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-01-1990',
                        'maxlength' => 10
                    ]))
            ->add('primaryActivity', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Основные виды деятельности организации</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('mission', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Миссия организации</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('countEmployeesLabel', CustomTextType::class,
                array(
                    'help' => $this->description['countEmployeesLabel'],
                ))
            ->add('countStaffEmployees', TextType::class,
                array(
                    'required' => false,
                    'label' => 'штатных сотрудников, чел.',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('countInvolvingEmployees', TextType::class,
                array(
                    'required' => false,
                    'label' => 'привлеченных сотрудников, чел.',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('countVolunteerEmployees', TextType::class,
                array(
                    'required' => false,
                    'label' => 'добровольцев организации, чел.',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('grants', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<b>Укажите, если за последние два года ваша Организация имела опыт получения грантов, субсидий, пожертвований, государственного/муниципального заказа и иного финансирования на конкурсной основе (по теме конкурса)</b>',
                    'btn_add' => 'Добавить'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->add('publicationMaterials', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<b>Публикации об организации за последние 2 года</b>',
                    'btn_add' => 'Добавить'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->end()
            ->with('3. Описание практики')
            ->add('priorityDirections', 'sonata_type_model',
                array(
                    'choices' => $this->getDirectionsBy(['application', 'farvater_application']),
                    'required' => false,
                    'label' => '<b>Приоритетное направление Конкурса, в рамках которого реализуется практика</b>',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('priorityDirectionEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => '<b>Иные направления(укажите, какие именно)</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('targetGroups', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => '<b>Целевые группы практики</b>',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('targetGroupEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => '<b>Иные группы(укажите, какие именно)</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('childProtectionSpecialists', TextType::class,
                array(
                    'required' => false,
                    'label' => '<b>Специалисты сферы защиты детства:</b>',
                    'attr' => array('maxlength' => 450)
                ))
            ->add('childrenCategories', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => '<b>Отметьте, если практика ориентирована преимущественно на детей «сложных» категорий</b>',
                    "property" => "categoryName",
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $this->getChildrenCategoriesBy(array(
                        self::COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME,
                        self::FARVATER_ASSOCIATION_ENTITY_NAME,
                        self::FARVATER_AND_KNS_2016_ASSOCIATION_ENTITY_NAME
                    ))
                ))
            ->add('childrenCategoryName', TextType::class,
                array(
                    'required' => false,
                    'label' => '<b>Иные «сложные» категории детей</b>',
                ))
            ->add('disabilitiesChildren', TextType::class,
                array(
                    'required' => false,
                    'label' => '<b>Дети с ограниченными возможностями здоровья</b>',
                ))
            ->add('specialNeedsEducationChildren', TextType::class,
                array(
                    'required' => false,
                    'label' => '<b>Дети со специальными потребностями в обучении и др.</b>',
                ))
            ->add('socialResults', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => '<b>Социальные результаты, на достижение которых направлена представляемая в проекте практика</b>',
                    "property" => "name",
                    'expanded' => true,
                    'multiple' => true
                ))
            ->add('socialResultEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Иные социальные результаты',
                ))
            ->add('practicePreview', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Краткое описание практики</b>',
                ))
            ->add('practiceEvidence', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Доказательства эффективности практики</b>',
                ))
            ->add('practicePurpose', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Цель проекта в отношении представленной практики</b>',
                ))
            ->add('practiceMechanism', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Описание механизма достижения поставленной цели</b>',
                ))
            ->add('practiceResults', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Ожидаемые результаты проекта – в отношении представленной практики</b>',
                ))
            ->add('practiceRegion', 'sonata_type_model',
                array(
                    'required' => false,
                    "property" => "name",
                    'label' => false,
                    'attr' => array('placeholder' => 'Территория реализации практики проекта')
                ))
            ->add('practiceRegionEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => false,
                    'attr' => array('maxlength' => 450,
                        'placeholder' => 'Дополнительная информация')
                ))
            ->end()
            ->with('4. Распространение практики')
            ->add('targetAudiences', 'sonata_type_model',
                array(
                    'required' => false,
                    'label' => 'Целевая аудитория',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('targetAudienceEtc', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Иные организации и специалисты в сфере защиты детства',
                ))
            ->add('practiceMotivation', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Мотивация, заинтересованность вашей организации в распространении и внедрении практики в деятельность других организаций и специалистов',
                ))
            ->add('practiceTech', TextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Технология распространения и внедрения практики',
                ))
            ->add('practiceExpectingSpreadResults', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Ожидаемые результаты распространения и внедрения практики</b>',
                ))
            ->end()
            ->with('5. Мониторинг и оценка результатов')
            ->add('organizationalCapacityMeasure', TextareaType::class,
                array(
                    'required' => false,
                    'label' => '<b>Как вы оцениваете организационный потенциал вашей организации в области планирования, мониторинга, измерения и оценки социальных программ?</b>',
                ))
            ->end()
            ->with('6. План-график проекта')
            ->add('approximateRequestedMoney', TextareaType::class,
                array(
                    'required' => false,
                    'label' => false,
                ))
            ->add('expenditures', 'sonata_type_collection',
                array(
                    'required' => false,
                    'label' => '<b>Основные статьи расходов на первый год реализации проекта (2016)</b>',
                    'btn_add' => 'Добавить',
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table'
                ))
            ->end();


    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig')
        );
    }

    public function getDirectionsBy($application_types)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:Farvater\PriorityDirection')
            ->findBy(
                array('applicationType' => $application_types)
            );
        return $directions;
    }

    public function getChildrenCategoriesBy($associationEntityNames)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:ChildrenCategory')
            ->findBy(
                array('type' => $associationEntityNames)
            );
        return $directions;
    }


}