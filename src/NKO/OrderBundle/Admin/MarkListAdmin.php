<?php

namespace NKO\OrderBundle\Admin;

use NKO\OrderBundle\Entity\Mark;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Form\CustomTextType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Security\Core\User\UserInterface;
use NKO\OrderBundle\Entity\MarkListHistory;

class MarkListAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'admin_sonata_nko_order_send_application_mark_list';

    protected $baseRoutePattern = 'mark_list';

    const HEIGHTENING_COEFF = 1.5;

    protected $coeff_message = '<i>Так как заявка относится к приоритетному направлению 
    «Активизация поддержки замещающих и кризисных семей 
    со стороны окружения (родственники, друзья, школы, 
    детские сады, соседи и пр.); развитие взаимоподдержки,
     консолидации сообществ членов замещающих семей (родителей,
      «выпускников» замещающих семей)», вводится повышающий
       коэффициент 1,5. При подсчете итогового балла суммарный 
       будет умножен на 1,5.</i>';

    public function setNonGroups()
    {
        $this->setValidationGroups(['None']);
    }

    public function setValidationGroups(array $groups)
    {
        $this->formOptions['validation_groups'] = $groups;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $finalDecisions  = null;
        $appHistory = null;
        $coefficient = null;

        $help = 'Пожалуйста, подробно обоснуйте свое решение (не менее 50 символов).';
        if ($this->getSubject() &&
            $this->getSubject()->getApplication()->getCompetition()->getHelpForRationaleFinalDecision()
        ) {
            $help = $this->getSubject()->getApplication()->getCompetition()->getHelpForRationaleFinalDecision();
        }

        if ($this->getConfigurationPool()->getContainer()->get('session')->get('create')) {
            $coefficient = $this->getConfigurationPool()->getContainer()->get('session')->get('coefficient');
        }

        if ($this->isCurrentRoute('edit') || $this->isCurrentRoute('send_mark')) {
            $competitionId = unserialize($this->getSubject()->getApplication()->getData())->getCompetition()->getId();
            $coefficient = $this
                ->getConfigurationPool()
                ->getContainer()
                ->get('nko.sum_mark_manager')
                ->getCoefficient($this->getSubject()->getApplication());
        }

        if ($coefficient == self::HEIGHTENING_COEFF) {
            $formMapper
                ->add('countEmployeesLabel', CustomTextType::class, [
                    'help' => $this->coeff_message,
                ]);
        }
        $query = $this->getConfigurationPool()->getContainer()
            ->get('Doctrine')
            ->getEntityManager()
            ->createQueryBuilder("decisions")
            ->select('d')
            ->from('NKO\OrderBundle\Entity\FinalDecision', 'd')
            ->join('d.competitions', 'c')
            ->where('c.id = :competitionId')
            ->setParameters(['competitionId' => $competitionId])
        ;

        $formMapper
            ->add('marks', 'sonata_type_collection', [
                'required' => false,
                'label' => false,
                'type_options' => array('delete' => false),
                'data_class' => null
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('finalDecision', null, [
                'query_builder' => $query,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('rationaleFinalDecision', TextareaType::class, [
                'required' => false,
                'help' => $help
            ])
            ->add('finalMark', null, ['attr' => ['readonly' => true]])
            ->add('markWithCoeff', null, ['attr' => ['readonly' => true]])
            ->add('coefficient', 'hidden', [
                'required' => false,
                'disabled' => true,
                'data' => $coefficient
            ])
            ->add('application_id', 'hidden')
            ->add('isMarkListReset', 'hidden', [
                'mapped' => false,
                'data' => $this->getSubject()->getApplication()->getCompetition()->getIsMarkListReset()
            ])
        ;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKODefaultBundle:form:form_label.html.twig')
        );
    }

    public function preUpdate($object)
    {
        $object->setMarks($object->getMarks());
    }

    public function prePersist($object)
    {
        $object->setExpert($this->getCurrentUser());
        $this->preUpdate($object);
    }

    public function postUpdate($object)
    {
        parent::postUpdate($object);

        $container = $this->getConfigurationPool()->getContainer();
        $em = $container->get('doctrine')->getManager();

        foreach ($object->getMarks() as $mark) {
            $mark->getQuestionMark()->setCriteria($container->get('nko.resolver.proxy_resolver')->resolveProxies($mark->getQuestionMark()->getCriteria()));
        }

        $history = $container->get('nko.archiver.mark_list_archiver')->archive($object);

        $em->persist($history);
        $em->flush();
    }

    /**
     * @return UserInterface
     */
    private function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->add('save', $this->getRouterIdParameter() . '/save');
        $collection->add('clear', $this->getRouterIdParameter() . '/clear');
        $collection->add('allowAutosave', $this->getRouterIdParameter() . '/allowAutosave');
    }
}
