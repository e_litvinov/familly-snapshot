<?php

namespace NKO\OrderBundle\Admin\Application\KNS\SecondStage2019;

use Doctrine\ORM\EntityRepository;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Entity\SettingEntity\FederalDistrict;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationAdmin extends AdminDecorator
{
    const TABS = [
        'budget' => 'БЮДЖЕТ ПРОЕКТА',
        'documents' => 'ВКЛАДКА VII. ДОКУМЕНТЫ'
    ];

    const DESCRIPTIONS = [
        'legalFederalDistrict' => 'Федеральный округ',
        'legalLocality' => 'Район',
        'actualFederalDistrict' => 'Федеральный округ',
        'actualLocality' => 'Район',
        'normativeAct' => '<b>1.21 Нормативные акты, которые применяются в деятельности организации</b>',
        'entirePeriodEstimatedFinancing' => '<b>6.1.</b>Ориентировочная сумма запрашиваемого финансирования 
            <b>на весь период</b> реализации проекта <i>(в тыс. рублей)</i>',
        'entirePeriodEstimatedCoFinancing' => '<b>6.2.</b>Предполагаемая сумма софинансирования 
            <b>на весь период</b> реализации проекта (в тыс. рублей)<i>(в тыс. рублей)</i>',
        'firstYearEstimatedFinancing' => '<b>6.3.</b> Сумма запрашиваемого финансирования 
            <b>на первый год</b> реализации проекта (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»)
            <i>(в тыс. рублей)</i>',
        'firstYearEstimatedCoFinancing' => '<b>6.4.</b>Общая сумма софинансирования (за счет собственных средств и средств партнеров)
            <b>на первый год</b> реализации проекта (в точном соответствии с данными, указанными в файле-приложении «Бюджет проекта»)
             <i>(в тыс. рублей)</i>',

        'budget' => '1. Бюджет проекта – заполненный файл установленного образца – в формате excel;',
        'regulation' => '2. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего 
            органа – только если были изменения и дополнения после заключения договора I этапа – в формате pdf; ',
        'anotherHead' => '3.  Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ) 
            в формате pdf',
        'authorityHead' => '4. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор 
            будет подписывать не руководитель организации) в формате pdf',
        'signedAgreement' => '5. Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке',
        'organizationCreationResolution' => '6. Документ, подтверждающий статус юридического лица (решение о создании 
            учреждения и т.п.) – только для государственных и муниципальных учреждений в формате pdf;',
        'reportForMinistryOfJustice' => '7. Отчёт в Министерство юстиции Российской Федерации /иной регистрирующий орган за 
            предшествующий отчётный период (скан-копия, заверенная подписью руководителя и печатью организации) ИЛИ ссылка 
            на его версию, размещённую на Информационном портале Министерства юстиции Российской Федерации по адресу: 
            http://unro.minjust.ru (за исключением организаций-заявителей – государственных и муниципальных учреждений) 
            в формате pdf<br> Файл в формате pdf:',
        'linkToReportForMinistryOfJustice' => "или ссылка:",
        'linkToReportForMinistryOfJusticeHelp' => "URL должен начинаться с http:// или https://",
    ];

    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);

        $this->formOptions = [
            'validation_groups' => [
                "KNS2019-2",
                'Address',
                'AdditionalAddress',
                'Budget',
                'AdditionBudget',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        ];
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->remove('competition')
            ->remove('pSRN')
            ->remove('author.nko_name')
            ->remove('author.nko_name')
            ->remove('headOfAccountingMobilePhoneLabel')
            ->remove('headOfAccountingMobilePhoneInternationalCode')
            ->remove('headOfAccountingMobilePhoneCode')
            ->remove('headOfAccountingMobilePhone')
        ;

        //First tab

        $wrappedFormMapper
            ->add('legalFederalDistrict', 'legalPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['legalFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('legalLocality', 'legalRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['legalLocality'],
            ])
            ->add('actualFederalDistrict', 'actualPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['actualFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('actualLocality', 'actualRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['actualLocality'],
            ])
            ->add('normativeActs', 'publications', EntityType::class, [
                'class' => NormativeAct::class,
                'label' => self::DESCRIPTIONS['normativeAct'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ApplicationTypes::KNS_APPLICATION_3_2018]);
                },
                'multiple' => true,
            ])
        ;


        //Third tab
        $wrappedFormMapper
            ->change('trainingGrounds', null, [
                'choices' => $this->sortedData['trainingGrounds'],
            ])
            ->change('deadLineStart', null, [
                'dp_min_date' => '15/06/2020',
                'dp_max_date' => '30/11/2021',
                'dp_default_date' => '15/06/2020',
            ])
            ->change('deadLineFinish', null, [
                'dp_min_date' => '15/06/2020',
                'dp_max_date' => '30/11/2021',
                'dp_default_date' => '15/06/2020',
            ])
        ;

        // Fifth tab




        $wrappedFormMapper
            ->addTab('budget', self::TABS['budget'])
            ->add('entirePeriodEstimatedFinancing', null, TextType::class, [
                'label' => self::DESCRIPTIONS['entirePeriodEstimatedFinancing'],
                'required' => false
            ])
            ->add('entirePeriodEstimatedCoFinancing', null, TextType::class, [
                'label' => self::DESCRIPTIONS['entirePeriodEstimatedCoFinancing'],
                'required' => false
            ])
            ->add('firstYearEstimatedFinancing', null, TextType::class, [
                'label' => self::DESCRIPTIONS['firstYearEstimatedFinancing'],
                'required' => false
            ])
            ->add('firstYearEstimatedCoFinancing', null, TextType::class, [
                'label' => self::DESCRIPTIONS['firstYearEstimatedCoFinancing'],
                'required' => false
            ])
            ->addTab('documents', self::TABS['documents'])
                ->add('agreementTemplate', null, CustomTextType::class, [
                    'help' => ($agreement = $this->subject->getCompetition()->getAgreement()) ? $agreement : false
                ])
                ->add('budget', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['budget']
                ])
                ->add('regulation', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['regulation']
                ])
                ->add('anotherHead', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['anotherHead']
                ])
                ->add('authorityHead', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['authorityHead']
                ])
                ->add('signedAgreement', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['signedAgreement']
                ])
                ->add('organizationCreationResolution', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['organizationCreationResolution']
                ])
                ->add('reportForMinistryOfJustice', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DESCRIPTIONS['reportForMinistryOfJustice']
                ])
                ->add('linkToReportForMinistryOfJustice', null, TextType::class, [
                    'required' => false,
                    'help' => self::DESCRIPTIONS['linkToReportForMinistryOfJusticeHelp'],
                    'label' => self::DESCRIPTIONS['linkToReportForMinistryOfJustice'],
                    'attr' => [
                        'placeholder' => '-'
                    ]
                ])
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('projectMembers')
                ->remove('functionalResponsibilities')
                ->change('fullName', FullscreenTextareaType::class)
                ->change('role', FullscreenTextareaType::class)
                ->change('employmentRelationship', null, [
                    'label' => 'Трудовые отношения с организацией на период проекта'
                ])
            ->with('individualSocialResults')
                ->change('socialResult', FullscreenTextareaType::class, [
                    'label' => '<b>Cоциальный результат (для благополучателей)</b><br><i>только те, которые указаны в п.3.10</i>'
                ])
                ->change('targetGroup', FullscreenTextareaType::class, [
                    'label' => '<b>Целевая группа (благополучатели)</b><br><i>только те, которые указаны в п.3.9</i>'
                ])
                ->change('indicator', FullscreenTextareaType::class)
                ->change('customMeasurementMethod', FullscreenTextareaType::class)
            ->with('practiceImplementationActivities')
                ->change('expectedResults', FullscreenTextareaType::class, [
                    'label' => 'Результаты'
                ])
        ;
    }

    protected function sort(array $items = [])
    {
        parent::sort([
            'trainingGrounds' => [
                'query' => $this->em->getRepository(TrainingGround::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2019),
                'nestedSequence' => [
                    25 => [36, 40, 42, 38, 44, 37, 43, 39, 77],
                    45 => [48, 55, 59, 54, 53, 57, 56, 49, 51, 58, 86]
                ]
            ],
            'socialResults' => [
                'query' => $this->em->getRepository(SocialResult::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2018),
                'sequence' => [2, 7, 8, 4, 5]
            ]
        ]);
    }
}
