<?php

namespace NKO\OrderBundle\Admin\Application\KNS\Application2020;

use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Regulation;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\SettingEntity\FederalDistrict;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ApplicationAdmin extends AdminDecorator
{
    const DESCRIPTION = [
        'trainingGroundsHelp' => '<b>3.7. Информация об основном обучающем мероприятии</b>',
    ];

    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);
        $this->formOptions = [
            'validation_groups' => [
                "KNS-2020",
                "AdditionBudget",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
                'actualLocality',
                'AdditionalAddress'
            ]
        ];
    }

    const NOT_CHOSEN = 'Не выбрано';

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->remove('pSRN')
            ->remove('author.nko_name')
            ->change('deadLineStart', null, [
                'dp_min_date' => '23/03/2020',
                'dp_max_date' => '30/11/2020',
                'dp_default_date' => '23/03/2020',
            ])
            ->change('deadLineFinish', null, [
                'dp_min_date' => '23/03/2020',
                'dp_max_date' => '30/11/2020',
                'dp_default_date' => '23/03/2020',
            ])
            ->change('dateStartOfInternship', null, [
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-date-format' => 'DD-MM-YYYY',
                    'data-date-start-date' => '23-03-2020',
                    'data-date-end-date' => '15-11-2020',
                    'maxlength' => 10
                ]
            ])
            ->add('trainingGroundsHelp', 'projectImplementation', CustomTextType::class, [
                'help' => self::DESCRIPTION['trainingGroundsHelp'],
            ])
            ->move('trainingGrounds', 'trainingGroundsHelp')
            ->change('trainingGrounds', null, [
                'choices' => $this->sortedData['trainingGrounds'],
            ])
            ->change('socialResults', null, [
                'choices' => $this->sortedData['socialResults'],
            ])
            ->move('authorityHead', 'regulation')
            ->move('anotherHead', 'authorityHead')
            ->move('signedAgreement', 'anotherHead')
            ->move('budget', 'signedAgreement')
            ->move('subjectRegulation', 'budget')
            ->add('regulations', 'publications', EntityType::class, [
                'class' => Regulation::class,
                'label' => $this->description['regulations'],
                'required' => false,
                'multiple' => true,
            ])
            ->add('legalLocality', 'legalRegion', TextType::class, [
                'label' => $this->description['area'],
                'required' => false,
            ])
            ->add('actualLocality', 'actualRegion', TextType::class, [
                'label' => $this->description['area'],
                'required' => false,
            ])
            ->add('legalFederalDistrict', 'legalPostCode', EntityType::class, [
                'class' => FederalDistrict::class,
                'label' => $this->description['district'],
                'required' => false,
            ])
            ->add('actualFederalDistrict', 'actualPostCode', EntityType::class, [
                'class' => FederalDistrict::class,
                'label' => $this->description['district'],
                'required' => false,
            ])
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('projectResults')
                ->change('linkedMethod', null, ['placeholder' => self::NOT_CHOSEN])
            ->with('beneficiaryResults')
                ->change('linkedMethod', null, ['placeholder' => self::NOT_CHOSEN])
                ->change('targetValue', IntegerType::class)
                ->change('approximateTargetValue', IntegerType::class)
            ->with('employeeResults')
                ->change('targetValue', IntegerType::class)
                ->change('approximateTargetValue', IntegerType::class)
                ->change('linkedMethod', null, ['placeholder' => self::NOT_CHOSEN]);
    }

    protected function sort(array $items = [])
    {
        parent::sort([
            'trainingGrounds' => [
                'query' => $this->em->getRepository(TrainingGround::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2020),
                'nestedSequence' => [
                    25 => [26, 28, 29, 30, 32, 33, 172, 35, 173, 40, 42, 38, 44, 37, 43, 77, 78, 79, 80, 81, 82, 83, 84, 174, 160, 169, 178, 179],
                    45 => [46, 48, 59, 54, 53, 57, 56, 49, 51, 166, 88, 89, 90, 91, 92, 93, 94, 175, 96, 97, 161, 171, 167],
                    60 => [61, 62, 99, 100],
                    148 => [149, 176, 164, 165],
                    70 => [71, 72, 103, 104, 74],
                    75 => [177, 105],
                ],
                'sequence' => [25, 45, 60, 148, 70, 75, 106]
            ],
            'socialResults' => [
                'query' => $this->em->getRepository(SocialResult::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2018),
                'sequence' => [2, 7, 8, 4, 5]
            ]
        ]);
    }
}
