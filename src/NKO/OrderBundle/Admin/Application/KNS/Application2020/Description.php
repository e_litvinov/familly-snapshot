<?php

namespace NKO\OrderBundle\Admin\Application\KNS\Application2020;

class Description
{
    const CHARACHTER_LIMIT_HELP = 'До 255 символов.';
    const CHARACHTER_LIMIT_HELP_450 = 'До 450 символов.';
    const CHARACHTER_LIMIT_HELP_1000 = 'До 1000 символов.';
    const CHARACHTER_LIMIT_HELP_1500 = 'До 1500 символов.';

    const DESCRIPTIONS = [
        'name' => '1.1 <b>Полное название </b>организации',
        'abbreviation' => '1.2 <b>Сокращенное название </b>организации',
        'author.pSRN' => '1.4 <b>ОГРН </b>организации',
        'email' => '1.9 Адрес <b>электронной</b> почты (для оперативного контакта с организацией)',
        'publications' => '1.20 <b>Публикации </b>об организации за последние 2 года.',
        'projectName' => '<b>3.1 Название проекта</b>',
        'projectImplementation' => '<b>3.6.Механизм реализации проекта</b>',
        'knowledgeIntroductionDuringProjectImplementation' => '<b>3.8.1. В период реализации проекта </b>',
        'knowledgeIntroductionAfterProjectImplementation' => '<b>3.8.2.  После окончания реализации проекта </b>
             (в течение 6 месяцев после окончания проекта)',
        'projectResults' => '3.10. <b>Ожидаемые результаты проекта</b><br> 
            3.10.1 <b>Укажите, какие количественные результаты будут получены благодаря реализации проекта, и как вы об этом узнаете</b>',
        'authorityHead' => '2. Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.), pdf',
        'signedAgreement' => '4. Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью), pdf',
        'budget' => '5. Бюджет проекта (файл в формате Excel по форме установленного образца), xls',
        'subjectRegulation' => '6. Устав субъекта РФ или муниципалитета, положение о профильной службе (департаменте, министерстве и т.п.) – <i>только для органов государственной власти и местного самоуправления</i>, pdf',
        'organizationCreationResolution' => '7. Документ, подтверждающий статус юридического лица государственных и муниципальных учреждений (решение о создании учреждения и т.п.) – <i>только для государственных и муниципальных учреждений</i>, pdf',
        'risks' => '3.11 <b>Риски.</b> Что может препятствовать внедрению результатов проекта?',
        'requestedFinancingMoney' => '3.12 <b>Сумма запрашиваемого финансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях',
        'cofinancingMoney' => '3.13 <b>Сумма софинансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях.',
        'trainingGrounds' => '3.7.1 <b>Тематика стажировки </b>(обучающего мероприятия) и организация – стажировочная площадка',
        'choosingGroundExplanation' => '3.7.2 Поясните, <b>почему</b> Вы выбрали именно эту площадку?',
        'dateStartOfInternship' => '3.7.3 <b>Дата начала проведения стажировки</b> (обучающего мероприятия) (не ранее 23-03-2020, не позднее 15-11-2020)',
        'traineeshipFormats' => '3.7.4 <b>Формат стажировки</b> (обучающего мероприятия)',
        'anotherHead' => '3. Документ, подтверждающий полномочия лица, которое будет подписывать договор – <i>в случае, если договор будет подписывать не руководитель организации<i>, pdf'
    ];

    const HELPS = [
        'deadLineLabel' => '3.2 Сроки реализации проекта (ограничение по диапазону дат: 23 марта 2020 года – 30 ноября 2020 года)',
        'abbreviation' => self::CHARACHTER_LIMIT_HELP,
        'legalCity' => self::CHARACHTER_LIMIT_HELP,
        'headOfOrganizationFullName' => self::CHARACHTER_LIMIT_HELP,
        'headOfOrganizationPosition' => self::CHARACHTER_LIMIT_HELP,
        'headOfProjectFullName' => self::CHARACHTER_LIMIT_HELP,
        'headOfProjectPosition' => self::CHARACHTER_LIMIT_HELP,
        'headOfAccountingFullName' => self::CHARACHTER_LIMIT_HELP,
        'oKVED' => self::CHARACHTER_LIMIT_HELP,
        'bankName' => self::CHARACHTER_LIMIT_HELP,
        'bankLocation' => self::CHARACHTER_LIMIT_HELP,
        'nameAddressee' => self::CHARACHTER_LIMIT_HELP,
        'projectName' => self::CHARACHTER_LIMIT_HELP_450,
        'projectPurpose' => self::CHARACHTER_LIMIT_HELP_450,
        'importanceProject' => self::CHARACHTER_LIMIT_HELP_1000,
        'projectImplementation' => self::CHARACHTER_LIMIT_HELP_1500,
        'knowledgeIntroductionDuringProjectImplementation' => self::CHARACHTER_LIMIT_HELP_1500,
        'knowledgeIntroductionAfterProjectImplementation' => self::CHARACHTER_LIMIT_HELP_1500,
        'employeesLabel' => '3.7.5 <b>Сотрудники организации</b> (члены общественного объединения), которые непосредственно примут участие в обучающих мероприятиях (стажировке).<br>
            <i>Укажите, ФИО конкретных сотрудников, которые примут участие в стажировке (обучающих мероприятиях), их роль в организации, выполняемые функции, образование и опыт. 
            В графе "Обучающие мероприятия" укажите те мероприятия, в которых непосредственно примет участие сотрудник.</i>',
    ];

    const TABS = [
    ];

    const CURRENT_DESCRIPTIONS = [
        'regulations' => '1.21. Нормативные акты, которые применяются в деятельности организации:',
        'district' => 'Федеральный округ',
        'area' => 'Район',
    ];

    const PROJECT_RESULTS = [

        'linkedMethod' => 'Cпособ измерения'
    ];
}
