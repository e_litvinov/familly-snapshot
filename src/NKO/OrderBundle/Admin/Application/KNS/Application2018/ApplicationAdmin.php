<?php

namespace NKO\OrderBundle\Admin\Application\KNS\Application2018;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Form\RadioButtonTree;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Form\DropDownListType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\AdminConfigurator\BankDetailsAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\OrganizationActivityAdmin;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;

class ApplicationAdmin extends AdminDecorator
{
    const TABS = [
        'contactsTab' => 'ИНФОРМАЦИЯ ОБ ОРГАНИЗАЦИИ',
        'informationAboutWinnerTab' => 'ИНФОРМАЦИЯ ДЛЯ ЗАКЛЮЧЕНИЯ ДОГОВОРА С ПОБЕДИТЕЛЕМ',
        'projectDescriptionTab' => 'ОПИСАНИЕ ПРОЕКТА',
        'bankDetailsTab' => 'БАНКОВСКИЕ РЕКВИЗИТЫ',
        'documentsTab' => 'ДОКУМЕНТЫ',
    ];

    const DESCRIPTION = [
        'trainingGrounds' => '3.7 <b>Информация об основном обучающем мероприятии</b><br>
            3.7.1 <b>Тематика стажировки</b> (обучающего мероприятия)',
        'choosingGroundExplanation' => '3.7.3 Поясните, <b>почему</b> Вы выбрали именно эту площадку?',
        'dateStartOfInternship' => '3.7.4 <b>Дата начала проведения стажировки</b> (обучающего мероприятия) (не ранее 15-05-2018, не позднее 28-02-2019)',
        'traineeshipFormats' => '3.7.5 <b>Формат стажировки</b> (обучающего мероприятия)',
        'knowledgeIntroductionDuringProjectImplementation' => '3.8.1 В период реализации проекта',
        'knowledgeIntroductionAfterProjectImplementation' => '3.8.2 После окончания реализации проекта (в течение 6 месяцев после окончания проекта)',
        'measures' => '<b>3.9. План-график мероприятий проекта</b><br>
            <i>Перечислите все мероприятия по проекту – как стажировку, так и мероприятия по распространению полученных знаний.</i>',
        'projectResults' => '<b>3.10. Ожидаемые результаты проекта</b><br>
            3.10.1 Укажите, <b>какие количественные результаты будут получены благодаря реализации проекта, и как вы об этом узнаете.</b>',
        'projectImplementation' => '3.6 <b>Механизм реализации проекта</b>',
        'employees' => '3.7.6 <b>Сотрудники организации</b> (члены общественного объединения), которые непосредственно примут участие в обучающих мероприятиях (стажировке).<br>
            <i>Укажите, ФИО конкретных сотрудников, которые примут участие в стажировке (обучающих мероприятиях), их роль в организации, выполняемые функции, образование и опыт. 
            В графе "Обучающие мероприятия" укажите те мероприятия, в которых непосредственно примет участие сотрудник.</i>',
        'employeeResults' => '3.10.2 <b>Качественные результаты для сотрудников организаций / членов общественного объединения</b>',
        'socialResults' => '3.10.3 <b>Программа «Семья и дети» Фонда Тимченко направлена на достижение социальных результатов, перечисленных ниже. 
            Выберите из списка, на достижение каких результатов направлена реализация вашего проекта. </b>',
        'beneficiaryResults' => '<b>3.10.4. Качественные изменения у благополучателей<br> РЕЗУЛЬТАТЫ ДЛЯ БЛАГОПОЛУЧАТЕЛЕЙ</b>',
        'risks' => '3.10.5 <b>Риски</b>. Что может препятствовать внедрению результатов проекта?',
        'requestedFinancingMoney' => '3.11 <b>Сумма запрашиваемого финансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях',
        'cofinancingMoney' => '3.12 <b>Сумма софинансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях.',
        'linkToAnnualReport' => '1.19 Ссылка на последний <b>годовой отчет</b> Организации (при наличии)'
    ];

    const DOCUMENT_DESCRIPTION =[
        'regulation' => '1 Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
        'organizationCreationResolution' => '2. Документ, подтверждающий статус юридического лица государственных и муниципальных учреждений 
            (решение о создании учреждения и т.п.) - только для государственных и муниципальных учреждений',
        'subjectRegulation' => '3. Устав субъекта РФ или муниципалитета, положение о профильной службе (департаменте, министерстве и т.п.) – 
            только для органов государственной власти  и местного самоуправления',
        'authorityHead' => '4. Документ, подтверждающий полномочия руководителя (например, протокол об избрании, приказ, доверенность)',
        'budget' => '5. Бюджет заявки в формате excel',
        'signedAgreement' => '6. Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке',
        'anotherHead' => '7. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
    ];

    const HELPS = [
        'introduction' => '<b>3.8 Внедрение полученных знаний и опыта</b><br>
            <i>Как именно в организации будут использоваться знания и опыт, полученные в ходе проекта? 
            Перечислите конкретные действия, которые будут предприняты в период реализации проекта, а также после его окончания.<br><br>
            В течение 6 месяцев после реализации проекта, Фонд проведет мониторинг проектов, поддержанных в рамках Конкурса. 
            Организации, успешно реализовавшие свои проекты и продемонстрировавшие конкретные результаты внедрения полученных знаний и навыков 
            (по результатам мониторинга), смогут принять участие во втором (закрытом) этапе Конкурса и 
            получить дополнительную финансовую поддержку на дальнейшее развитие своего проекта.</i>',
        'linkToAnnualReport' => 'URL должен начинаться с http:// или https://'
    ];

    const PROJECT_DESCRIPTION = [
        'projectName' => '3.1. Название проекта',
        'deadlineLabel' => '3.2. Сроки реализации проекта (ограничение по диапазону дат: 15 мая – 30 ноября 2018 года)',
        'projectPurpose' => '3.3. <b>Цель</b> проекта',
        'importanceProject' => '3.4. <b>Обоснование актуальности и важности</b> проекта',
        'beneficiaryGroupsLabel' => '3.5. Укажите <b>основные целевые группы благополучателей</b>, на которых повлияет реализация проекта:',
        'otherBeneficiaryGroups' => '<b>Иные группы</b> (укажите)',
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                "KNS-2018",
                "AdditionBudget",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $subject = $this->getSubject() ?: $this->getRelatedObject();

        $trainingGrounds = $em->getRepository(TrainingGround::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2018);
        $socialResults = $em->getRepository(SocialResult::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2018)->getResult();

        $formMapper
            ->with(self::TABS['contactsTab']);
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($subject)], $em);
        OrganizationActivityAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($subject)]);
        $formMapper
            ->end()
            ->with(self::TABS['bankDetailsTab']);
        BankDetailsAdmin::configureFormFields($formMapper, AdminConfigurator::BANK_DETAILS[get_class($subject)]);
        $formMapper
            ->end()
            ->with(self::TABS['projectDescriptionTab'])
            ->add('projectName', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['projectName'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('deadLineLabel', CustomTextType::class, array(
                    'help' => self::PROJECT_DESCRIPTION['deadlineLabel'],
                    'required' => false
            ))
            ->add('deadLineStart', 'sonata_type_datetime_picker', array(
                'label' => 'с',
                'format' => 'dd.MM.yyyy',
                'dp_min_date' => '15/05/2018',
                'dp_max_date' => '30/03/2019',
                'dp_pick_time' => false,
                'required' => false,
                'dp_use_current' => false,
            ))
            ->add('deadLineFinish', DateTimePickerType::class, array(
                'label' => 'по',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'dp_min_date' => '15/05/2018',
                'dp_max_date' => '30/03/2019',
                'required' => false,
                'dp_use_current' => false,
            ))
            ->add('projectPurpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['projectPurpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('importanceProject', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['importanceProject'],
                'attr' => array('maxlength' => 1000),
            ))
            ->add('beneficiaryGroupsLabel', CustomTextType::class, array(
                    'help' => self::PROJECT_DESCRIPTION['beneficiaryGroupsLabel'],
            ))
            ->add('peopleCategories', DropDownListType::class, array(
                'class' => 'NKOOrderBundle:BriefApplication2017\PeopleCategory',
                'required' => false,
                'label' => false,
                'multiple' => true
            ))
            ->add('otherBeneficiaryGroups', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['otherBeneficiaryGroups'],
                'btn_add' => "Добавить",
                'by_reference' => false
            ), array(
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('projectImplementation', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['projectImplementation'],
                'attr' => array('maxlength' => 1500),
            ))
            ->add('trainingGrounds', RadioButtonTree::class, array(
                'class' => TrainingGround::class,
                'choices' =>$trainingGrounds,
                'required' => false,
                'label' => self::DESCRIPTION['trainingGrounds'],
                'multiple' => true,
            ))
            ->add('choosingGroundExplanation', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['choosingGroundExplanation'],
                'attr' => array('maxlength' => 1000),
            ))
            ->add('dateStartOfInternship', DateType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['dateStartOfInternship'],
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-date-format' => 'DD-MM-YYYY',
                    'data-date-start-date' => '15-05-2018',
                    'data-date-end-date' => '28-02-2019',
                    'maxlength' => 10
                ]))
            ->add('traineeshipFormats', 'sonata_type_model', array(
                'required' => false,
                'label' => self::DESCRIPTION['traineeshipFormats'],
                "property" => "formatName",
                'expanded' => true,
                'multiple' => true
            ))
            ->add('traineeshipFormatName', TextType::class, array(
                'required' => false,
                'label' => 'другое (укажите)',
            ))
            ->add('employeesLabel', CustomTextType::class, array(
                'help' => self::DESCRIPTION['employees'],
            ))
            ->add('employees', 'sonata_type_collection', array(
                'required' => false,
                'label' => ' ',
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.employee',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('introduction', CustomTextType::class, array(
                'help' => self::HELPS['introduction'],
            ))
            ->add('knowledgeIntroductionDuringProjectImplementation', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['knowledgeIntroductionDuringProjectImplementation'],
                'attr' => array('maxlength' => 1500),
            ))
            ->add('knowledgeIntroductionAfterProjectImplementation', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['knowledgeIntroductionAfterProjectImplementation'],
                'attr' => array('maxlength' => 1500),
            ))
            ->add('measures', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['measures'],
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.measure',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('projectResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['projectResults'],
                'btn_add' => "Добавить",
                'by_reference' => false
            ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.project_result',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('employeeResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['employeeResults'],
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.employee_result',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('socialResults', EntityType::class, array(
                'class' => SocialResult::class,
                'required' => false,
                'label' => self::DESCRIPTION['socialResults'],
                'expanded' => true,
                'multiple' => true,
                'choices' => $socialResults,
            ))
            ->add('beneficiaryResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['beneficiaryResults'],
                'btn_add' => true,
                'by_reference' => false
                ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.beneficiary_result',
                'edit' => 'inline',
                'inline' => 'table'
            ))

            ->add('risks', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['risks'],
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.risk',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('requestedFinancingMoney', TextType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['requestedFinancingMoney'],
                'attr' => array('maxlength' => 10)
            ))
            ->add('cofinancingMoney', TextType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['cofinancingMoney'],
                'attr' => array('maxlength' => 10)
            ))
            ->end()
            ->with(self::TABS['documentsTab'])
            ->add('agreementTemplate', CustomTextType::class, array(
                'help' => ($agreement = $subject->getCompetition()->getAgreement()) ? $agreement : false
            ))
            ->add('regulation', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['regulation']
            ))
            ->add('organizationCreationResolution', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['organizationCreationResolution']
            ))
            ->add('subjectRegulation', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['subjectRegulation']
            ))
            ->add('authorityHead', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['authorityHead']
            ))
            ->add('budget', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['budget']
            ))
            ->add('signedAgreement', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['signedAgreement']
            ))
            ->add('anotherHead', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['anotherHead']
            ))
            ->end();
    }
}
