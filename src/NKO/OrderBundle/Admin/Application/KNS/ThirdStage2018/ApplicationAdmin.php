<?php

namespace NKO\OrderBundle\Admin\Application\KNS\ThirdStage2018;

use Doctrine\ORM\EntityRepository;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Entity\SettingEntity\FederalDistrict;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationAdmin extends AdminDecorator
{
    const TABS = [
        'budget' => 'БЮДЖЕТ ПРОЕКТА',
        'documents' => 'ВКЛАДКА VII. ДОКУМЕНТЫ',
        'projectCard' => 'КАРТОЧКА ПРОЕКТА',
    ];

    const FIRST_TAB_DESCRIPTION = [
        'competitionNameDuplicate' => '<b>Название конкурса</b>:',
        'realizationYearDuplicate' => '<b>Год реализации</b>',
        'authorNKONameDuplicate' => '<b>Получатель пожертвования:</b>',
        'contract' => '<b>Договор пожертвования N:</b>',
        'projectNameDuplicate' => '<b>Название Проекта:</b>',
        'priorityDirectionDuplicate' => '<b>Приоритетное направление:</b>',
        'deadLineLabelDuplicate' => '<b>Сроки реализации Проекта:</b>',
        'deadLineStartDuplicate' => '<b>С:</b>',
        'deadLineFinishDuplicate' => '<b>По:</b>',
        'sumGrantDuplicate' => '<b>Сумма пожертвования (за 2020 год): </b>',
        'headOfOrganizationDuplicate' => '<b>Руководитель организации</b> (ФИО, должность):',
        'headOfAccountingDuplicate' => '<b>Главный бухгалтер/ бухгалтер:</b> (ФИО, должность) ',
        'OrganizationInfo' => '<b>Контактные данные организации</b>',
        'phoneDuplicate' => 'Телефон:',
        'emailDuplicate' => 'Адрес электронной почты:',
        'sumGrantLabel' => '<b>6. Бюджет</b>',
        'sumGrant' => '<b>6.1 Сумма пожертвования, руб:</b>',
        'normativeAct' => '<b>Нормативные акты, которые применяются в деятельности организации</b>',
    ];

    const DESCRIPTIONS = [
        'updatedAt' => '<b>1.21 Дата отправки</b> заявки',
        'deadLineLabel' => '<b>3.7 Сроки реализации проекта</b>',
        'legalFederalDistrict' => 'Федеральный округ',
        'legalLocality' => 'Район',
        'actualFederalDistrict' => 'Федеральный округ',
        'actualLocality' => 'Район',
    ];

    const DOCUMENT_DESCRIPTION =[
        'regulation' => '1 Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
        'authorityHead' => '2. Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.)',
        'anotherHead' => '3. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
        'signedAgreement' => '4. Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью)',
        'budget' => '5. Бюджет заявки в формате excel',
        'subjectRegulation' => '6. Устав субъекта РФ или муниципалитета, положение о профильной службе (департаменте, министерстве и т.п.) 
            – только для органов государственной власти и местного самоуправления',
        'organizationCreationResolution' => '7. Документ, подтверждающий статус юридического лица государственных и муниципальных учреждений 
            (решение о создании учреждения и т.п.) - только для государственных и муниципальных учреждений',
    ];

    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);

        $this->formOptions = [
            'validation_groups' => [
                'KNS2018-3',
                'Address',
                'AdditionalAddress',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        ];
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper->addTab('projectInfo', 'КАРТОЧКА ПРОЕКТА', 'first');
        $this->addFirstTab($wrappedFormMapper);
        /** @var BaseApplication $subject */
        $subject = $this->getSubject() ?: $this->getRelatedObject();
        $updatedAt = $subject->getUpdatedAt();

        $wrappedFormMapper
            ->remove('competition')
            ->remove('pSRN')
            ->remove('author.nko_name')
            ->remove('headOfAccountingMobilePhoneLabel')
            ->remove('headOfAccountingMobilePhoneInternationalCode')
            ->remove('headOfAccountingMobilePhoneCode')
            ->remove('headOfAccountingMobilePhone')
            ->add('updatedAt', 'publications', TextType::class, [
                'mapped' => false,
                'required' => false,
                'data' => $updatedAt ? $updatedAt->format('Y-m-d') : (new \DateTime())->format('Y-m-d'),
                'label' => self::DESCRIPTIONS['updatedAt'],
                'attr' => ['readonly' => 'readonly'],
            ])
            ->change('deadLineStart', null, [
                'dp_min_date' => '01/02/2020',
                'dp_max_date' => '30/11/2020',
            ])
            ->change('deadLineFinish', null, [
                'dp_min_date' => '01/02/2020',
                'dp_max_date' => '30/11/2020',
            ])
            ->change('projectImplementation', FullscreenTextareaType::class)
            ->change('effectivenessComment', FullscreenTextareaType::class)
            ->change('individualSocialResultsComment', FullscreenTextareaType::class)
            ->remove('peopleCategories')
            ->remove('peopleCategoriesLabel')
            ->remove('beneficiaryGroupsLabel')
            ->remove('individualResultLabel')
            ->remove('effectivenessItems')
            ->add('effectivenessKNSItems', 'effectivenessLabel', 'sonata_type_collection', [
                'label' => false,
            ], [
                'admin_code' => 'nko_order.admin.continuation.kns.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('sumGrantLabel', 'organizationResources', CustomTextType::class, [
                'help' => self::FIRST_TAB_DESCRIPTION['sumGrantLabel']
            ])
            ->add('sumGrant', 'sumGrantLabel', TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['sumGrant'],
                'required' => false
            ])
        ;

        $wrappedFormMapper
            ->add('legalFederalDistrict', 'legalPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['legalFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('legalLocality', 'legalRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['legalLocality'],
            ])
            ->add('actualFederalDistrict', 'actualPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['actualFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('actualLocality', 'actualRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['actualLocality'],
            ])
        ;

        $wrappedFormMapper
            ->addTab('documents', self::TABS['documents'])
            ->add('agreementTemplate', null, CustomTextType::class, [
                'help' => $subject->getCompetition()->getAgreement() ?: false
            ])
            ->add('regulation', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['regulation']
            ])
            ->add('authorityHead', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['authorityHead']
            ])
            ->add('anotherHead', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['anotherHead']
            ])
            ->add('signedAgreement', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['signedAgreement']
            ])
            ->add('budget', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['budget']
            ])
            ->add('subjectRegulation', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['subjectRegulation']
            ])
            ->add('organizationCreationResolution', null, FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['organizationCreationResolution']
            ])
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('projectMembers')
                ->remove('functionalResponsibilities')
                ->change('fullName', FullscreenTextareaType::class)
                ->change('role', FullscreenTextareaType::class)
                ->change('employmentRelationship', null, [
                    'label' => 'Трудовые отношения с организацией на период проекта'
                ])
            ->with('otherBeneficiaryGroups')
                ->change('name', FullscreenTextareaType::class)
            ->with('individualSocialResults')
                ->change('socialResult', FullscreenTextareaType::class)
                ->change('targetGroup', FullscreenTextareaType::class)
                ->change('indicator', FullscreenTextareaType::class)
                ->change('customMeasurementMethod', FullscreenTextareaType::class)
        ;
    }

    private function addFirstTab(WrappedFormMapper $formMapper)
    {
        /** @var BaseApplication $object */
        $object = $this->getSubject();
        $date = $object->getCompetition()->getRealizationYear();
        $grant = $this->em->getRepository(GrantConfig::class)->findGrantByAuthorAndReportForm($object->getCompetition()->getRelatedReportForm(), $object->getAuthor());

        $formMapper
            ->add('competitionNameDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['competitionNameDuplicate'],
                'mapped' => false,
                'required' => false,
                'data' => $object->getCompetition()->getName(),
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('realizationYearDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['realizationYearDuplicate'],
                'mapped' => false,
                'required' => false,
                'data' => $date ? $date->format('Y') : (new \DateTime())->format('Y'),
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('authorNKONameDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['authorNKONameDuplicate'],
                'mapped' => false,
                'data' => $object->getAuthor()->getFullName(),
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('contract', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['contract'],
                'required' => false,
            ])
            ->add('projectNameDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['projectNameDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('priorityDirectionDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['priorityDirectionDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('deadLineLabelDuplicate', null, CustomTextType::class, [
                'help' => self::FIRST_TAB_DESCRIPTION['deadLineLabelDuplicate'],
                'required' => false
            ])
            ->add('deadLineStartDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['deadLineStartDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('deadLineFinishDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['deadLineFinishDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('sumGrantDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['sumGrantDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('headOfOrganizationDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['headOfOrganizationDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('headOfAccountingDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['headOfAccountingDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('OrganizationInfo', null, CustomTextType::class, [
                'help' => self::FIRST_TAB_DESCRIPTION['OrganizationInfo']
            ])
            ->add('phoneDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['phoneDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('emailDuplicate', null, TextType::class, [
                'label' => self::FIRST_TAB_DESCRIPTION['emailDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('normativeActs', null, EntityType::class, [
                'class' => NormativeAct::class,
                'label' => self::FIRST_TAB_DESCRIPTION['normativeAct'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ApplicationTypes::KNS_APPLICATION_3_2018]);
                },
                'multiple' => true,
            ])
        ;
    }
}
