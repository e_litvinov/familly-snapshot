<?php

namespace NKO\OrderBundle\Admin\Application\KNS\SecondStage2017;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Form\RadioButtonTree;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\DropDownListType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use NKO\OrderBundle\Entity\Region;
use NKO\OrderBundle\AdminConfigurator\BankDetailsAdmin;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\OrganizationActivityAdmin;

class ApplicationAdmin extends AdminDecorator
{
    const TABS = [
        'contactsTab' => 'ИНФОРМАЦИЯ ОБ ОРГАНИЗАЦИИ',
        'bankDetailsTab' => 'БАНКОВСКИЕ РЕКВИЗИТЫ',
        'projectDescriptionTab' => 'ОПИСАНИЕ ПРОЕКТА-II ЭТАП',
        'planTab' => 'ПЛАН-ГРАФИК ПРОЕКТА',
        'resourceTab' => 'РЕСУРСНОЕ ОБЕСПЕЧЕНИЕ ПРОЕКТА',
    ];

    const PROJECT_DESCRIPTION = [
        'projectName' => '3.1. Название проекта',
        'priorityDirection' => '3.2 <b>Приоритетное направление Конкурса</b>, в рамках которого реализуется практика',
        'priorityDirectionEtc' => '<b>Иные эффективные практики</b> в сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей (укажите, какие именно)',
        'deadlineLabel' => '<b>3.7. Сроки реализации проекта</b> (ограничение по диапазону дат: 15 июня 2018 – 30 ноября 2019 года)',
        'knowledgeIntroductionDuringProjectImplementation' => '<b>3.4. Какие практики, знания и опыт стажировочной площадки уже используются в 
            вашей организации? Что удалось внедрить в деятельность вашей организации (благодаря реализации проекта)? </b>',
        'practiceRegions' => '<b>3.6. Территория реализации Проекта</b><br><i>
            В каком регионе (регионах) РФ ваша организация сейчас реализует представленную на Конкурс Практику?</i>',
        'knowledgeIntroductionAfterProjectImplementation' => '<b>3.5. Какая планируется поддержка при внедрении практики со стороны организации –
            стажировочной площадки?</b><br><i>Опишите, какие договоренности достигнуты со стажировочной площадкой – например, проведение супервизий,
            консультирование и пр. Укажите, кто именно из сотрудников организации будет вас сопровождать, как часто, в каком формате и пр.
            Как планируется оформить ваши договоренности?</i>',
        'projectPurpose' => '3.8. <b>Цель</b> проекта',
        'peopleCategoriesLabel' => '3.9.<b>Целевые группы благополучателей</b> проекта<br><i>(оставьте только нужные варианты)</i>',
        'beneficiaryGroupsLabel' => '<b>3.10. Основные проблемы благополучателей и ожидаемые социальные результаты проекта</b>',
        'otherBeneficiaryGroups' => '<b>Иные группы</b>(укажите)',
        'practiceImplementationActivities' => '<b>4.1. План-график проекта</b>',
        'effectivenessLabel' => '<b>3.12. Ключевые показатели Фонда</b><br><i>По показателям, которые не относятся к вашему проекту, поставьте нули в целевые значения, в графе “Способ измерения” выберите “Показатель не относится к проекту”, а в поле “Свои способы измерения” проставьте прочерк. 
По показателям, где Вы применяете свой способ измерения, добавьте “Способ измерения” “Свой метод”, а в поле “Свои способы измерения” уточните, какой именно. </i>',
        'individualResultLabel' => '<b>3.13. Индивидуальные социальные результаты и показатели</b>',
        'projectImplementation' => '<b>3.14. Как вы планируете распространять знания и опыт (Практику), полученные в ходе стажировки и дальнейшего внедрения элементов практики стажировочной площадки вашей организацией?</b>',
        'risks' => '<b>3.15. Риски проекта</b>',
        'comment' => '<b>Комментарий к таблице (если есть):</b>',
        'projectRelevance' => '<b>3.11. Механизм реализации проекта</b>'
    ];

    const RESOURCE_DESCRIPTION = [
        'projectMembers' => '<b>5.1 Команда</b> проекта',
        'projectPartners' => '<b>5.2 Партнеры и доноры проекта</b> (при наличии)',
        'organizationResources' => '<b>5.3 Имеющиеся ресурсы для реализации Проекта</b>'
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                "KNS2017-2",
                "KNS2017-2_2",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $subject = $this->getSubject() ?: $this->getRelatedObject();

        $trainingGrounds  = $this->getConfigurationPool()->getContainer()
            ->get('Doctrine')->getRepository(TrainingGround::class)->createQueryBuilder('p')
            ->where('p.competition like :name')
            ->setParameter('name', '%'.ApplicationTypes::KNS_APPLICATION_2018. '%')
            ->getQuery()
            ->getResult();

        $formMapper
            ->with(self::TABS ['contactsTab']);
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($subject)], $em);
        OrganizationActivityAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($subject)]);
        $formMapper
            ->end()
            ->with(self::TABS['bankDetailsTab']);
        BankDetailsAdmin::configureFormFields($formMapper, AdminConfigurator::BANK_DETAILS[get_class($subject)]);
        $formMapper
            ->end()
            ->with(self::TABS['projectDescriptionTab'])
            ->add('projectName', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['projectName'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('trainingGrounds', RadioButtonTree::class, array(
                'class' => TrainingGround::class,
                'choices' =>$trainingGrounds,
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['priorityDirection'],
                'multiple' => true,
            ))
            ->add('priorityDirectionEtc', TextType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['priorityDirectionEtc'],
                'attr' => array('maxlength' => 255)
            ))
            ->add('knowledgeIntroductionDuringProjectImplementation', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['knowledgeIntroductionDuringProjectImplementation'],
            ))
            ->add('knowledgeIntroductionAfterProjectImplementation', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['knowledgeIntroductionAfterProjectImplementation'],
            ))
            ->add('practiceRegions', 'sonata_type_model', array(
                'required' => false,
                'property' => 'name',
                'label' => self::PROJECT_DESCRIPTION['practiceRegions'],
                'multiple' => true,
                'choices' => $em->getRepository(Region::class)->findBy([], [
                    'name' => 'ASC'
                ])
            ))
            ->add('deadLineLabel', CustomTextType::class, array(
                'help' => self::PROJECT_DESCRIPTION['deadlineLabel'],
                'required' => false
            ))
            ->add('deadLineStart', 'sonata_type_datetime_picker', array(
                'label' => 'с',
                'format' => 'dd.MM.yyyy',
                'dp_min_date' => '15/06/2018',
                'dp_max_date' => '30/11/2019',
                'dp_pick_time' => false,
                'required' => false,
                'dp_use_current' => false,
            ))
            ->add('deadLineFinish', DateTimePickerType::class, array(
                'label' => 'по',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'dp_min_date' => '15/06/2018',
                'dp_max_date' => '30/11/2019',
                'required' => false,
                'dp_use_current' => false,
            ))
            ->add('projectPurpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['projectPurpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('peopleCategoriesLabel', CustomTextType::class, array(
                'help' => self::PROJECT_DESCRIPTION['peopleCategoriesLabel'],
            ))
            ->add('peopleCategories', DropDownListType::class, array(
                'class' => 'NKOOrderBundle:BriefApplication2017\PeopleCategory',
                'required' => false,
                'label' => false,
                'multiple' => true
            ))
            ->add('otherBeneficiaryGroups', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['otherBeneficiaryGroups'],
                'by_reference' => false
            ), array(
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('beneficiaryGroupsLabel', CustomTextType::class, array(
                'help' => self::PROJECT_DESCRIPTION['beneficiaryGroupsLabel'],
            ))
            ->add('beneficiaryProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.problem',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('projectRelevance', TextareaType::class, [
                'label' => self::PROJECT_DESCRIPTION['projectRelevance'],
                'required' => false,
                'attr' => array('maxlength' => 1500),
            ])
            ->add('effectivenessLabel', CustomTextType::class, array(
                'help' => self::PROJECT_DESCRIPTION['effectivenessLabel'],
            ))
            ->add('effectivenessItems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false,
            ), array(
                'admin_code' => 'nko_order.admin.kns2017.second_stage_2.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('effectivenessComment', TextareaType::class, [
                'label' => self::PROJECT_DESCRIPTION['comment'],
                'required' => false,
            ])
            ->add('individualResultLabel', CustomTextType::class, array(
                'help' => self::PROJECT_DESCRIPTION['individualResultLabel']
            ))
            ->add('individualSocialResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false,
            ), array(
                'admin_code' => 'nko_order.admin.application.kns.second_stage2017.result',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('individualSocialResultsComment', TextareaType::class, [
                'label' => self::PROJECT_DESCRIPTION['comment'],
                'required' => false,
            ])
            ->add('projectImplementation', TextareaType::class, [
                'label' => self::PROJECT_DESCRIPTION['projectImplementation'],
                'required' => false,
            ])
            ->add('risks', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::PROJECT_DESCRIPTION['risks'],
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'sonata.admin.nko.order.kns2017.risk',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->end()
            ->with(self::TABS['planTab'])
            ->add('practiceImplementationActivities', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::PROJECT_DESCRIPTION['practiceImplementationActivities'],
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater2017.practice_implementation_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with(self::TABS['resourceTab'])
            ->add('projectMembers', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::RESOURCE_DESCRIPTION['projectMembers'],
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('projectPartners', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::RESOURCE_DESCRIPTION['projectPartners'],
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('organizationResources', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::RESOURCE_DESCRIPTION['organizationResources'],
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end();
    }
}
