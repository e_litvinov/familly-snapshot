<?php

namespace NKO\OrderBundle\Admin\Application\KNS\SecondStage2017;

use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;

class EffectivenessAdmin extends EmbeddedAdminDecorator
{
    const DESCRIPTION = [
        'indicator' => 'Показатель',
        'factValue' => 'Целевое значение на конец I года реализации проекта',
        'planValue' => 'Целевое значение на конец реализации проекта',
        'measurementMethod' => 'Способ измерения',
        'comment' => 'Свои способы измерения'
     ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('indicator', 'sonata_type_admin', array(
                'label' => self::DESCRIPTION['indicator'],
            ), array(
                'admin_code' => 'nko_order.admin.indicator'
            ))
            ->add('factValue', NumberType::class, [
                'label' => self::DESCRIPTION['factValue'],
                'required' => false,
                'attr' => [
                    'placeholder' => 0
                ]
            ])
            ->add('planValue', NumberType::class, [
                'label' => self::DESCRIPTION['planValue'],
                'required' => false,
                'attr' => [
                    'placeholder' => 0
                ]
            ])
            ->add('measurementMethod', null, [
                'required' => true,
                'label' => '<b>Способ измерения</b>',
                'placeholder' => 'Не выбрано',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ApplicationTypes::KNS_APPLICATION_2_2017 . '%');
                }
            ])
            ->add('comment', TextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['comment'],
            ))
        ;
    }
}
