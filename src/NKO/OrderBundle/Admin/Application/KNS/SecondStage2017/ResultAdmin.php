<?php

namespace NKO\OrderBundle\Admin\Application\KNS\SecondStage2017;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;

class ResultAdmin extends EmbeddedAdminDecorator
{
    const INDIVIDUAL_RESULT = [
        'socialResult' => 'Cоциальный результат (для благополучателей)',
        'targetGroup' => 'Целевая группа (благополучатели)',
        'indicator' => 'Показатель',
        'firstYearTargetValue' => 'Целевое значение на конец I года реализации проекта',
        'targetValue' => 'Целевое значение на конец реализации проекта',
        'customMeasurementMethod' => 'Свои способы измерения',
        'measurementMethod' => 'Способ измерения',
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('socialResult', TextType::class, array(
                  'label' => self::INDIVIDUAL_RESULT['socialResult'],
                  'required' => false,
            ))
            ->add('targetGroup', TextType::class, array(
                  'label' => self::INDIVIDUAL_RESULT['targetGroup'],
                  'required' => false,
            ))
            ->add('indicator', TextType::class, array(
                    'label' => self::INDIVIDUAL_RESULT['indicator'],
                    'required' => false,
            ))
            ->add('firstYearTargetValue', NumberType::class, array(
                  'label' => self::INDIVIDUAL_RESULT['firstYearTargetValue'],
                  'required' => false,
            ))
            ->add('targetValue', NumberType::class, array(
                  'label' => self::INDIVIDUAL_RESULT['targetValue'],
                  'required' => false,
            ))
            ->add('measurementMethods', null, [
                'required' => false,
                'multiple' => true,
                'label' => self::INDIVIDUAL_RESULT['measurementMethod'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('o')
                        ->where('o.type like :type')
                        ->setParameter('type', '%' . ApplicationTypes::KNS_APPLICATION_2_2017 . '%');
                }
            ])
            ->add('customMeasurementMethod', TextType::class, array(
                   'label' => self::INDIVIDUAL_RESULT['customMeasurementMethod'],
                   'required' => false,
            ))
        ;
    }
}
