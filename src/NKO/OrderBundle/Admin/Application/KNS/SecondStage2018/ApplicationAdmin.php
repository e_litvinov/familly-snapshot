<?php

namespace NKO\OrderBundle\Admin\Application\KNS\SecondStage2018;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\RadioButtonTree;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationAdmin extends AdminDecorator
{
    const TABS = [
        'budget' => 'БЮДЖЕТ ПРОЕКТА',
        'documents' => 'ВКЛАДКА VII. ДОКУМЕНТЫ'
    ];

    const DESCRIPTIONS = [
        'entirePeriodEstimatedFinancing' => '<b>6.1.</b>Ориентировочная сумма запрашиваемого финансирования 
            <b>на весь период</b> реализации проекта <i>(в тыс. рублей)</i>',
        'entirePeriodEstimatedCoFinancing' => '<b>6.2.</b>Предполагаемая сумма софинансирования 
            <b>на весь период</b> реализации проекта (в тыс. рублей)<i>(в тыс. рублей)</i>',
        'firstYearEstimatedFinancing' => '<b>6.3.</b> Сумма запрашиваемого финансирования 
            <b>на первый год</b> реализации проекта (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»)
            <i>(в тыс. рублей)</i>',
        'firstYearEstimatedCoFinancing' => '<b>6.4.</b>Общая сумма софинансирования (за счет собственных средств и средств партнеров)
            <b>на первый год</b> реализации проекта (в точном соответствии с данными, указанными в файле-приложении «Бюджет проекта»)
             <i>(в тыс. рублей)</i>',
    ];

    const DOCUMENT_DESCRIPTION =[
        'regulation' => '1 Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
        'organizationCreationResolution' => '2. Документ, подтверждающий статус юридического лица государственных и муниципальных учреждений 
            (решение о создании учреждения и т.п.) - только для государственных и муниципальных учреждений',
        'subjectRegulation' => '3. Устав субъекта РФ или муниципалитета, положение о профильной службе (департаменте, министерстве и т.п.) – 
            только для органов государственной власти  и местного самоуправления',
        'authorityHead' => '4. Документ, подтверждающий полномочия руководителя (например, протокол об избрании, приказ, доверенность)',
        'budget' => '5. Бюджет заявки в формате excel',
        'signedAgreement' => '6. Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке',
        'anotherHead' => '7. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
    ];

    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);

        $this->formOptions = [
            'validation_groups' => [
                "KNS2018-2",
                "KNS2017-2",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        ];
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $subject = $this->getSubject() ?: $this->getRelatedObject();
        $dateRegistrationOfOrganization = $this->subject ? $this->subject->getDateRegistrationOfOrganization() : null;
        $dateRegistrationOfOrganization = $dateRegistrationOfOrganization ?: new \DateTime();

        $wrappedFormMapper
            ->remove('name')
            ->remove('pSRN')
        ;

        $wrappedFormMapper
            ->change('deadLineStart', DateTimePickerType::class, [
                'label' => 'по',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'dp_min_date' => '01/06/2019',
                'dp_max_date' => '30/11/2020',
                'required' => false,
                'dp_use_current' => false,
            ])
            ->change('deadLineFinish', DateTimePickerType::class, [
                'label' => 'по',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'dp_min_date' => '01/06/2019',
                'dp_max_date' => '30/11/2020',
                'required' => false,
                'dp_use_current' => false,
            ])
            ->change('dateRegistrationOfOrganization', DateType::class, [
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-date-format' => 'DD-MM-YYYY',
                    'data-date-start-date' => '01-01-1917',
                    'maxlength' => 10
                ],
                'data' => $dateRegistrationOfOrganization,
            ])
        ;



        $wrappedFormMapper
            ->addTab('budget', self::TABS['budget'])
                ->add('entirePeriodEstimatedFinancing', null, TextType::class, [
                    'label' => self::DESCRIPTIONS['entirePeriodEstimatedFinancing'],
                    'required' => false
                ])
                ->add('entirePeriodEstimatedCoFinancing', null, TextType::class, [
                    'label' => self::DESCRIPTIONS['entirePeriodEstimatedCoFinancing'],
                    'required' => false
                ])
                ->add('firstYearEstimatedFinancing', null, TextType::class, [
                    'label' => self::DESCRIPTIONS['firstYearEstimatedFinancing'],
                    'required' => false
                ])
                ->add('firstYearEstimatedCoFinancing', null, TextType::class, [
                    'label' => self::DESCRIPTIONS['firstYearEstimatedCoFinancing'],
                    'required' => false
                ])
            ->addTab('documents', self::TABS['documents'])
                ->add('agreementTemplate', null, CustomTextType::class, [
                    'help' => ($agreement = $subject->getCompetition()->getAgreement()) ? $agreement : false
                ])
                ->add('regulation', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['regulation']
                ])
                ->add('organizationCreationResolution', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['organizationCreationResolution']
                ])
                ->add('subjectRegulation', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['subjectRegulation']
                ])
                ->add('authorityHead', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['authorityHead']
                ])
                ->add('budget', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['budget']
                ])
                ->add('signedAgreement', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['signedAgreement']
                ])
                ->add('anotherHead', null, FilePreviewType::class, [
                    'data_class' => null,
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['anotherHead']
                ])
        ;
    }


    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('projectMembers')
                ->remove('functionalResponsibilities')
                ->change('fullName', FullscreenTextareaType::class)
                ->change('role', FullscreenTextareaType::class)
                ->change('employmentRelationship', null, [
                    'label' => 'Трудовые отношения с организацией на период проекта'
                ])
            ->with('individualSocialResults')
                ->change('socialResult', FullscreenTextareaType::class)
                ->change('targetGroup', FullscreenTextareaType::class)
                ->change('indicator', FullscreenTextareaType::class)
                ->change('customMeasurementMethod', FullscreenTextareaType::class)

        ;
    }
}
