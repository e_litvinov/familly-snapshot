<?php

namespace NKO\OrderBundle\Admin\Application\KNS\Application2019;

class Description
{
    const CHARACHTER_LIMIT_HELP = 'До 255 символов.';
    const CHARACHTER_LIMIT_HELP_450 = 'До 450 символов.';
    const CHARACHTER_LIMIT_HELP_1000 = 'До 1000 символов.';
    const CHARACHTER_LIMIT_HELP_1500 = 'До 1500 символов.';

    const DESCRIPTIONS = [
        'dateStartOfInternship' => '3.7.4 <b>Дата начала проведения стажировки</b> (обучающего мероприятия) (не ранее 01-03-2019, не позднее 31-03-2020)',
        'authorityHead' => '2. Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.)',
        'anotherHead' => '3. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
        'signedAgreement' => '4. Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью)',
        'budget' => '5. Бюджет проекта (файл в формате Excel по форме установленного образца)',
        'subjectRegulation' => '6. Устав субъекта РФ или муниципалитета, положение о профильной службе (департаменте, министерстве и т.п.) – <i>только для органов государственной власти и местного самоуправления</i> ',
        'organizationCreationResolution' => '7. Документ, подтверждающий статус юридического лица государственных и муниципальных учреждений (решение о создании учреждения и т.п.) – <i>только для государственных и муниципальных учреждений</i>',
        'risks' => '3.11 Риски. Что может препятствовать внедрению результатов проекта?',
        'requestedFinancingMoney' => '3.12 <b>Сумма запрашиваемого финансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях',
        'cofinancingMoney' => '3.13 <b>Сумма софинансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях.',
        'trainingGrounds' => '3.7.1 - 3.7.2 <b>Тематика стажировки</b> (обучающего мероприятия)',
    ];

    const HELPS = [
        'deadLineLabel' => 'Сроки реализации проекта (ограничение по диапазону дат: 1 марта 2019 года – 31 марта 2020 года)
        <br>Реализация проекта возможна только в 2019 году. Пожалуйста, не выбирайте 2020 год.',
        'abbreviation' => self::CHARACHTER_LIMIT_HELP,
        'legalCity' => self::CHARACHTER_LIMIT_HELP,
        'headOfOrganizationFullName' => self::CHARACHTER_LIMIT_HELP,
        'headOfOrganizationPosition' => self::CHARACHTER_LIMIT_HELP,
        'headOfProjectFullName' => self::CHARACHTER_LIMIT_HELP,
        'headOfProjectPosition' => self::CHARACHTER_LIMIT_HELP,
        'headOfAccountingFullName' => self::CHARACHTER_LIMIT_HELP,
        'oKVED' => self::CHARACHTER_LIMIT_HELP,
        'bankName' => self::CHARACHTER_LIMIT_HELP,
        'bankLocation' => self::CHARACHTER_LIMIT_HELP,
        'personalAccount' => self::CHARACHTER_LIMIT_HELP,
        'nameAddressee' => self::CHARACHTER_LIMIT_HELP,
        'projectName' => self::CHARACHTER_LIMIT_HELP_450,
        'projectPurpose' => self::CHARACHTER_LIMIT_HELP_450,
        'importanceProject' => self::CHARACHTER_LIMIT_HELP_1000,
        'projectImplementation' => self::CHARACHTER_LIMIT_HELP_1500,
        'knowledgeIntroductionDuringProjectImplementation' => self::CHARACHTER_LIMIT_HELP_1500,
        'knowledgeIntroductionAfterProjectImplementation' => self::CHARACHTER_LIMIT_HELP_1500,
    ];

    const TABS = [
    ];

    const CURRENT_DESCRIPTIONS = [
    ];
}
