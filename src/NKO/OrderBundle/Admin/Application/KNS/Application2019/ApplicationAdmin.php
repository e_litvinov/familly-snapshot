<?php

namespace NKO\OrderBundle\Admin\Application\KNS\Application2019;

use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;

class ApplicationAdmin extends AdminDecorator
{
    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);
        $this->formOptions = [
            'validation_groups' => [
                "KNS-2019",
                "AdditionBudget",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        ];
    }

    const NOT_CHOSEN = 'Не выбрано';

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->remove('pSRN')
            ->remove('author.nko_name')
            ->change('deadLineStart', null, [
                'dp_min_date' => '01/03/2019',
                'dp_max_date' => '31/03/2020',
                'dp_default_date' => '01/03/2019',
            ])
            ->change('deadLineFinish', null, [
                'dp_min_date' => '01/03/2019',
                'dp_max_date' => '31/03/2020',
                'dp_default_date' => '01/03/2019',
            ])
            ->change('dateStartOfInternship', null, [
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-date-format' => 'DD-MM-YYYY',
                    'data-date-start-date' => '01-03-2019',
                    'data-date-end-date' => '31-03-2020',
                    'maxlength' => 10
                ]
            ])
            ->change('trainingGrounds', null, [
                'choices' => $this->sortedData['trainingGrounds'],
            ])
            ->change('socialResults', null, [
                'choices' => $this->sortedData['socialResults'],
            ])
            ->move('authorityHead', 'regulation')
            ->move('anotherHead', 'authorityHead')
            ->move('signedAgreement', 'anotherHead')
            ->move('budget', 'signedAgreement')
            ->move('subjectRegulation', 'budget')
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('projectResults')
                ->change('linkedMethod', null, ['placeholder' => self::NOT_CHOSEN])
            ->with('beneficiaryResults')
                ->change('linkedMethod', null, ['placeholder' => self::NOT_CHOSEN])
            ->with('employeeResults')
                ->change('linkedMethod', null, ['placeholder' => self::NOT_CHOSEN]);
    }

    protected function sort(array $items = [])
    {
        parent::sort([
            'trainingGrounds' => [
                'query' => $this->em->getRepository(TrainingGround::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2019),
                'nestedSequence' => [
                    25 => [36, 40, 42, 38, 44, 37, 43, 39, 77],
                    45 => [48, 55, 59, 54, 53, 57, 56, 49, 51, 58, 86]
                ]
            ],
            'socialResults' => [
                'query' => $this->em->getRepository(SocialResult::class)->findCachedByApplicationTypeQuery(ApplicationTypes::KNS_APPLICATION_2018),
                'sequence' => [2, 7, 8, 4, 5]
            ]
        ]);
    }
}
