<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class DoubleContextApplicationAdmin extends AbstractAdmin
{
    const LABEL_NAMES = [
        'title' => 'Фактор устойчивости',
        'context' => 'Действия, обеспечивающие данный фактор'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', FullscreenTextareaType::class, [
                'label' => self::LABEL_NAMES['title'],
                'attr' => ['maxlength' <= 500]])

            ->add('context', FullscreenTextareaType::class, [
                'label' => self::LABEL_NAMES['context'],
                'attr' => ['maxlength' <= 1000]]);
    }
}
