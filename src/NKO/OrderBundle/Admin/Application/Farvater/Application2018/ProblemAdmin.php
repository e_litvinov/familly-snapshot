<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProblemAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'targetGroup' => 'Целевая группа распространения Практики',
        'content' => 'Основные проблемы целевой группы, которые решает распространение Практики',
        'socialResults' => 'Социальные результаты распространения практики'
    ];

    const PLACEHOLDER = [
        'custom' => 'Добавить свои социальные результаты'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('targetGroup', TextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['targetGroup'],
                'attr' => ['readonly' => true]
            ])
            ->add('content', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['content']
            ])
            ->add('socialResults', null, [
                'required' => false,
                'by_reference' => false,
                'label' => self::DESCRIPTION['socialResults'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                        ->where('r.applicationType like :type')
                        ->setParameter('type', '%' . ApplicationTypes::FARVATER_APPLICATION_2018. '%');
                },

                'attr' => array('style' => 'margin-bottom: 10px')
             ])

            ->add('customSocialResult', TextareaType::class, [
                'required' => false,
                'attr' => array(
                    'placeholder' => self::PLACEHOLDER['custom'])
            ])

        ;
    }
}
