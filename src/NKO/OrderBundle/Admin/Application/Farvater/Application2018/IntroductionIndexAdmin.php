<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class IntroductionIndexAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'firstYearTargetValue' => 'Целевое значение на конец I года',
        'secondYearTargetValue' => 'Целевое значение на конец II года',
        'thirdYearTargetValue' => 'Целевое значение на конец III года',
        'indexName' => 'Показатель'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('indexName', TextareaType::class, ['label' => self::DESCRIPTION['indexName'], 'attr' => ['readonly'=>true]])
            ->add('firstYearTargetValue', NumberType::class, ['label' => self::DESCRIPTION['firstYearTargetValue']])
            ->add('secondYearTargetValue', NumberType::class, ['label' => self::DESCRIPTION['secondYearTargetValue']])
            ->add('thirdYearTargetValue', NumberType::class, ['label' => self::DESCRIPTION['thirdYearTargetValue']])
        ;
    }
}
