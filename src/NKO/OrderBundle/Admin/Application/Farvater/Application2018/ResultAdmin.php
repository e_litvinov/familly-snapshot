<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedTargetGroup;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ResultAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'socialResults'     => 'Cоциальные результаты проекта (в отношении благополучателей)',
        'targetGroup'       => 'Целевые группы (благополучатели)',
        'indicator'         => [
            'projectSocialResults' => 'Ключевые показатели Фонда',
            'projectSocialIndividualResults' => 'Показатель'
        ],
        'targetValue'       => 'Целевое значение на конец I года реализации проекта',
        'approximateValue'  => 'Ориентировочное целевое значение на конец реализации проекта',
        'service'           => 'Список услуг, мероприятий и пр.'
    ];

    const PRACTICE_DESCRIPTION = [
        'socialResults'     => 'Результат',
        'targetGroup'       => 'Целевые группы (благополучатели)',
        'indicator'         => 'Показатель',
        'targetValue'       => 'Целевое значение на конец I года реализации проекта',
        'approximateValue'  => 'Целевое значение на конец III года',
        'service'           => 'Список услуг, мероприятий и пр.'
    ];

    const APPLICATION_CODE = 'application';

    const FIND_BY = [
        Farvater2018ApplicationUtils::PROJECT_SOCIAL_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PROJECT_APPLICATION_FIELD_NAME,
        Farvater2018ApplicationUtils::PROJECT_SOCIAL_INDIVIDUAL_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PROJECT_APPLICATION_FIELD_NAME,
        Farvater2018ApplicationUtils::PRACTICE_SOCIAL_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PRACTICE_APPLICATION_LINKED_FIELD_NAME
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $parentFieldName = $this->getParentFieldDescription()->getFieldName();

        $description = self::DESCRIPTION;
        if ($parentFieldName == Farvater2018ApplicationUtils::PRACTICE_SOCIAL_RESULT_FIELD_NAME) {
            $description = self::PRACTICE_DESCRIPTION;
        }

        $formMapper
            ->add('socialResult', null, [
                'label' => $description['socialResults'],
                'choices' => $this->getChoicesByApplication(RelatedSocialResult::class, self::FIND_BY[$parentFieldName])
            ])
        ;

        if ($parentFieldName != Farvater2018ApplicationUtils::PRACTICE_SOCIAL_RESULT_FIELD_NAME) {
            $formMapper
                ->add('targetGroup', null, [
                    'label' => $description['targetGroup'],
                    'choices' => $this->getChoicesByApplication(RelatedTargetGroup::class, self::FIND_BY[$parentFieldName])
                ])
            ;
        }

        if ($parentFieldName == Farvater2018ApplicationUtils::PROJECT_SOCIAL_RESULT_FIELD_NAME) {
            $formMapper
                ->add('indicator', null, [
                    'label' => $description['indicator'][Farvater2018ApplicationUtils::PROJECT_SOCIAL_RESULT_FIELD_NAME],
                    'choices' => $this->getConfigurationPool()
                        ->getContainer()
                        ->get('doctrine')
                        ->getRepository(Indicator::class)
                        ->findBy([ 'code' => Farvater2018ApplicationUtils::PROJECT_SOCIAL_RESULT_INDICATOR_CODE])
                ])
            ;
        }
        else {
            $formMapper
                ->add('customIndicator', FullscreenTextareaType::class, [
                    'label' => self::DESCRIPTION['indicator'][Farvater2018ApplicationUtils::PROJECT_SOCIAL_INDIVIDUAL_RESULT_FIELD_NAME],
                ])
            ;
        }

        $formMapper
            ->add('targetValue', NumberType::class, [
                'label' => $description['targetValue']
            ])
            ->add('approximateValue', NumberType::class, [
                'label' => $description['approximateValue']
            ])
            ->add('service', FullscreenTextareaType::class, [
                'label' => $description['service'],
                'attr' => [ 'maxlength' => 1000 ]
            ])
        ;
    }

    private function getChoicesByApplication($class, $applicationFieldName)
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getRepository($class)->findBy([
                $applicationFieldName => $this->getParentFieldDescription()->getAdmin()->getSubject()
            ]);
    }
}
