<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use NKO\OrderBundle\Entity\Application\Continuation\ResultDirection;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;

class ExpectedResultAdmin extends AbstractAdmin
{
    const MODEL_FIELD_DESCRIPTION = [
        'experienceItems',
        'processItems',
        'qualificationItems',
        'resourceBlockItems'
    ];

    const TEXT_FIELD_DESCRIPTION = [
        'experienceEtcItems',
        'processEtcItems',
        'qualificationEtcItems',
        'resourceBlockEtcItems'
    ];

    const DESCRIPTION = [
        'direction' => 'Ориентировочные задачи',
        'indicator' => 'Результат',
        'comment' => 'Комментарий'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $fieldName = $this->getParentFieldDescription()->getFieldName();
        if (in_array($fieldName, self::MODEL_FIELD_DESCRIPTION)) {

            if ($fieldName == Farvater2018ApplicationUtils::QUALIFICATION_ITEMS) {
                $fieldName = Farvater2018ApplicationUtils::QUALIFICATION_FARVATER_ITEMS;
            }
            $formMapper
                ->add('direction', null, [ 
                    'required' => true,
                    'label' => self::DESCRIPTION['direction'],
                    'choices' => $this->getConfigurationPool()
                        ->getContainer()
                        ->get('doctrine')
                        ->getRepository(ResultDirection::class)
                        ->findBy(['type' => $fieldName])
                ])
            ;
        }
        elseif (in_array($fieldName, self::TEXT_FIELD_DESCRIPTION)) {
            $formMapper
                ->add('customDirection', TextType::class, [
                    'label' => self::DESCRIPTION['direction']
                ])
            ;
        }

        $formMapper
            ->add('indicator', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['indicator'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['comment'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
        ;
    }
}
