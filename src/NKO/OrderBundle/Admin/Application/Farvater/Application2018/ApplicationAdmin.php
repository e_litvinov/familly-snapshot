<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/12/18
 * Time: 6:20 PM
 */

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedTargetGroup;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Traits\CollectionHandler;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApplicationAdmin extends BaseApplicationAdmin
{
    use CollectionHandler;

    const TABS_DESCRIPTION = [
        'tab_3' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
        'tab_4' => 'РАСПРОСТРАНЕНИЕ И ВНЕДРЕНИЕ ПРАКТИКИ',
        'tab_5' => 'МОНИТОРИНГ И ОЦЕНКА. РАЗВИТИЕ ПРАКТИКИ',
        'tab_6' => 'ПЛАН-ГРАФИК ПРОЕКТА',
        'tab_7' => 'РЕСУРСНОЕ ОБЕСПЕЧЕНИЕ ПРОЕКТА',
        'tab_8' => 'БЮДЖЕТ ПРОЕКТА',
        'tab_9' => 'ДОКУМЕНТЫ'
    ];

    const HELPS = [
        'specialistTargetGroups' => '<b>4.4. Проблемы целевых групп специалистов, 
            на решение которых направлено распространение и внедрение Практики; социальные результаты распространения Практики</b>',
        'specialistProblems' => '<b>4.4.2. Основные проблемы специалистов и социальные результаты распространения Практики</b><br>
            <i>Вы можете выбрать один или несколько вариантов социальных результатов из списка и указать под ним свои.</i>',
        'expectedResultsLabel' => '<b>5.6. Ориентировочные задачи проекта в отношении блока мониторинга и оценки</b>',
        'projectSocialResults' => '<b>3.8  Ожидаемые результаты проекта в отношении представленной Практики<br>
            3.8.1. Социальные результаты</b><br>
            <i>Целевые значения результатов проекта в целом (к концу третьего года) указываются ориентировочные 
            – при подаче промежуточной отчетности (в конце первого и второго года реализации проекта) 
            возможно уточнение целевых значений, показателей и результатов.<br><br>
            Сначала в колонке <b>“Социальные результаты проекта”</b> необходимо выбрать один из социальных результатов, 
            которые Вы указали в поле 3.5.2 (краткая заявка).<br><br>
            В колонке <b>“Целевые группы (благополучатели)”</b> необходимо выбрать те целевые группы (из отмеченных Вами в п.3.5.1), 
            которые относятся к данному социальному результату, по одной в строке.<br><br>
            В колонке <b>"Показатели"</b> возможен выбор показателей социального результата (один показатель в одной строке), по одному на строку. 
            В двух следующих колонках необходимо ввести целевые значения по этим показателям на 1 и 3 года соответственно.<br><br>
            В колонке <b>“Список услуг, мероприятий и пр.“</b> Вы перечисляете те услуги и мероприятия (п.3.8.2), 
            которые должны привести к указанному в первой колонке социальному результату.<br><br>
            После того, как Вы полностью прописали один социальный результат, с новой строки Вы переходите к следующему. 
            В случае, если вам нужно использовать дополнительные показатели, вы можете это сделать в таблице ниже - «Индивидуальные показатели». </i>',
        'projectDirectResults' => '<b>3.8.2. Непосредственные результаты</b><br>
            <i>Укажите, в каких мероприятиях примут участие представители целевых групп Практики в рамках проекта, 
            какие именно услуги они получат. Приведите данные с разбивкой по целевым группам и годам.<br><br>
            Сначала в колонке <b>“Наименование услуги, мероприятия и пр.”</b> необходимо указать услугу, мероприятие и пр.<br><br>
            Далее, в колонке <b>“Целевые группы (благополучатели)”</b> необходимо выбрать те целевые группы (из отмеченных Вами в п.3.5.1), 
            которые примут участие в мероприятии, получат данную услугу и пр. Одна целевая группа – одна строка.<br><br>
            В колонке <b>“Непосредственный результат“</b> Вы приводите непосредственные результаты оказания услуги, 
            проведения мероприятия, по одному в строке. Например, лекция проведена, приемные семьи приняли участие в семинаре и пр.<br><br>
            В колонке <b>"Показатель"</b> Вы выбираете из предложенного списка показатели непосредственных результатов, <b>по одному на строку</b>. 
            В двух следующих колонках необходимо ввести целевые значения по этим показателям на 1 и 3 года соответственно.<br><br>
            После того, как Вы полностью описали одну услугу / одно мероприятие, с новой строки Вы переходите к следующей.  
            </i>',
        'practiceSocialResults' => '<b>4.7.2. Социальные результаты распространения практики</b><br><br>
            <i>Сначала в поле колонке <b>“Результат”</b> необходимо выбрать один из социальных результатов, которые были Вами указаны в поле 4.4.2.<br><br> 
            В колонке <b>"Показатель"</b> Вы вводите показатели, <b>по одному на строку</b>. В этом поле <b>Вы приводите показатели сами</b>. В случае, 
            если у вас нет точных сведений о целевых значениях того или иного показателя, укажите ориентировочное число. 
            В двух следующих колонках необходимо ввести плановые значения по этим показателям на 1 и 3 года соответственно.<br><br> 
            В колонке <b>“Список услуг, мероприятий и пр.“</b> Вы перечисляете услуги и мероприятия (п.4.7.3), 
            которые должны привести к указанному в первой колонке результату, по одному в строке.<br><br>
            После того, как Вы полностью описали один результат, с новой строки Вы переходите к следующему. 
            Сначала Вы приводите ключевые социальные результаты, потом – индивидуальные.<br><br>
            <b style="color: #952a25">ПЕРЕД ЗАПОЛНЕНИЕМ ДАННОЙ ТАБЛИЦЫ ОБЯЗАТЕЛЬНО СОХРАНИТЕ ЗАЯВКУ!</b></i><br>',
        'practiceDirectResults' => '<b>4.7.3. Непосредственные результаты распространения и внедрения практики</b><br><br>
            <i>Сначала в колонке <b>“Наименование услуги, мероприятия и пр.”</b> необходимо указать услугу, мероприятие и пр.<br><br>
            Далее, в колонке <b>“Целевая группа”</b> необходимо выбрать те целевые группы (из отмеченных Вами в п.4.4.1), 
            которые получат данную услугу, примут участие в данном мероприятию (по одной в строке).<br><br>
            В колонке <b>“Непосредственный результат“</b> Вы приводите непосредственные результаты оказания услуги, проведения мероприятия, по одному в строке.<br><br>
            В колонке <b>"Показатели"</b> Вы выбираете показатели непосредственных результатов, <b>по одному на строку</b>. 
            В двух следующих колонках необходимо ввести целевые  значения по этим показателям на 1 и 3 года соответственно.<br><br>
            После того, как Вы полностью описали одну услугу / одно мероприятие, с новой строки Вы переходите к следующей.<br><br> 
            <b style="color: #952a25">ПЕРЕД ЗАПОЛНЕНИЕМ ДАННОЙ ТАБЛИЦЫ ОБЯЗАТЕЛЬНО СОХРАНИТЕ ЗАЯВКУ!</b></i><br>'
    ];

    const DESCRIPTION = [
        'specialistTargetGroups' => '<b>4.4.1. Целевые группы специалистов</b>',
        'other' => '<b>Иные группы</b> <i>(не более 3)</i>',
        'experience' => '<b>5.6.1. Опыт</b>',
        'experienceEtc' => '<b>Иные задачи по блоку “Опыт”</b>',
        'process' => '<b>5.6.2. Процессы</b>',
        'processEtc' => '<b>Иные задачи по блоку “Процессы”</b>',
        'qualification' => '<b>5.6.3. Повышение уровня знаний и навыков сотрудников</b>',
        'qualificationEtc' => '<b>Иные задачи по блоку “Повышение уровня знаний и навыков сотрудников”</b>',
        'resourceBlock' => '<b>5.6.4. Ресурсы</b>',
        'resourceBlockEtc' => '<b>Иные задачи по блоку “Ресурсы”</b>',
        'projectMembers' => '<b>7.1. Команда</b> проекта<br><br><i>Укажите специалистов,
            которые будут вовлечены в непосредственную реализацию проекта, а также их квалификацию, роль в Проекте и характер трудовых
            отношений с Организацией. Если ФИО участника пока неизвестно, то укажите его роль в проекте. Например, «Психолог 1».</i>>',
        'projectPartners' => '<b>7.2. Партнеры и доноры Проекта</b> (при наличии)<br><br><i>Перечислите иные организации, участвующие 
            в реализации проекта, с указанием их роли в проекте</i>',
        'organizationResources' => '<b>7.3. Имеющиеся у Организации ресурсы, необходимые для реализации Проекта (на период реализации проекта)</b>
            <br><br><i>Укажите, какие ресурсы уже есть в распоряжении организации (собственные средства) или будут привлечены со 
            стороны партнёров для реализации Проекта</i>',
        'projectPurpose' => '<b>3.7. Цель проекта в отношении реализации практики (благополучателей) до 2020 года</b>',
        'tasksHeadTarget' => '<b>3.7.1. Какие задачи должны быть решены, чтобы была достигнута цель?</b><br><br>
            <i>Пункт заполняется по желанию заявителя, если для уточнения цели нужно сформулировать задачи</i>',
        'realizationPracticeApplications' => '<b>3.9. Устойчивость ожидаемых результатов Практики для благополучателей:</b><br><br>
            За счет чего изменения, достигнутые в процессе реализации проекта, сохранятся и после завершения проекта?',
        'conditionRealizationPractice' => '<b>3.10. Обстоятельства, которые могут воспрепятствовать успешной реализации Практики, 
            и действия, которые предприняты (или будут предприняты) для снижения рисков.</b>',
        'individualResults' => '<b>Индивидуальные показатели</b><br><i>В этой таблице можно указать те показатели, которых не было в предложенном списке. </i>',
        'descriptionSectionComment' => '<b>3.11. Комментарий к разделу III</b>',
        'entirePeriodEstimatedFinancing' => '<b>8.1.</b> Предполагаемая сумма запрашиваемого финансирования на весь период реализации проекта (в тыс. рублей)',
        'entirePeriodEstimatedCoFinancing' => '<b>8.2.</b>Предполагаемая сумма софинансирования <b>на весь период</b> реализации проекта (в тыс. рублей)',
        'firstYearEstimatedFinancing' => '<b>8.3.</b> Сумма запрашиваемого финансирования <b>на первый год</b> реализации проекта (в тыс. рублей)',
        'firstYearEstimatedCoFinancing' => '<b>8.4.</b> Сумма софинансирования <b>на первый год</b> реализации проекта (в тыс. рублей)',
    ];

    const SPREAD_PRACTICE_DESCRIPTION = [
        'motivationSpreadPractice' => '<b>4.2. Мотивация, заинтересованность вашей организации в распространении и 
            внедрении Практики в деятельность других организаций</b><br><br><i>В какой степени у вашей организации есть интерес, 
            желание и готовность стать отраслевым региональным ресурсным центром по распространению представленной на 
            Конкурс Практики (в т.ч. после окончания проекта); выступать стажировочной площадкой (в т.ч. в рамках других
             Конкурсов Фонда Тимченко)?</i>',
        'experienceSpread' => '<b>4.3. Укажите, какой опыт распространения Практики уже есть у вашей организации.</b>',
        'technologySpread' => '<b>4.5. Технология распространения и внедрения Практики в рамках проекта</b><br><br><i>Подробно 
            опишите, как именно вы планируете распространять и внедрять вашу Практику в рамках трёхлетнего проекта. По 
            возможности, приведите обоснования выбора форматов.Обратите внимание, что в рамках Конкурса приветствуются 
            проактивные форматы, нацеленные на то, чтобы другие организации сферы защиты детства внедрили её в свою деятельность, 
            а не только повысили осведомлённость.</i>',
        'introductionProjectPurpose' => '<b>4.6. Цель проекта (распространение и внедрение практики) до 2020 года</b>',
        'taskOfTarget' => '<b>4.6.1. Какие задачи должны быть решены, чтобы была достигнута цель (п.4.6)?</b><br><br>
            <i>Пункт заполняется по желанию заявителя, если для уточнения цели нужно сформулировать задачи.</i>',
        'introductionPracticesLabel' => '<b>4.7. Результаты распространения и внедрения практики.</b>',
        'introductionPractices' => '<b>4.7.1. Внедрение практики</b>',
        'comment' => '<i>Комментарий к таблицам (если есть):</i>',
        'spreadPracticeApplications' => '<b>4.8. Устойчивость ожидаемых результатов распространения Практики для организаций и специалистов:</b>
            <br><br><i>В чем будет выражаться устойчивость изменений, достигнутых в процессе распространения практики? Какие меры 
            будут приняты для ее достижения? Пожалуйста, для каждой идеи используйте новую строку.</i>',
        'conditionSpreadPractice' => '<b>4.9. Обстоятельства, которые могут воспрепятствовать успешному распространению Практики, и 
            действия, которые предприняты (или будут предприняты) для снижения рисков.</b>',
        'spreadSectionComment' => '<b>4.10. Комментарий к разделу IV</b>'
    ];

    const MONITORING_PRACTICE_DESCRIPTION = [
        'monitoringLabel' => '<i>Одна из задач конкурса «Семейный фарватер» - содействовать развитию доказательной базы эффективности поддержанных практик и 
            повышению потенциала организаций (победителей Конкурса) по теме профилактики социального сиротства и семейного устройства, 
            а также в области мониторинга, измерения и оценки результатов.</i><br>',
        'wishDevelopPractice' => '<b>5.1. Собираетесь ли вы развивать практику (дополнять её новыми элементами, применять 
            к новым целевым группам и пр.) в рамках проекта? Если да, то что именно и когда предполагается сделать? </b>',
        'monitoringProjectPurpose' => '<b>5.5. Цель проекта (в отношении блока мониторинга и оценки): </b>',
        'risePotentialApplications' => '<b>5.7. Устойчивость ожидаемых результатов в сфере мониторинга и оценки: </b>
            <br><br><i>В чем будет выражаться устойчивость изменений, достигнутых в сфере мониторинга и оценки? Какие меры 
            будут приняты для ее достижения? Пожалуйста, для каждой идеи используйте новую строку.</i>',
        'risk' => '<b>5.8. Обстоятельства, которые могут воспрепятствовать успешному повышению организационного потенциала в отношении 
            мониторинга и оценки, и действия, которые предприняты (или будут предприняты) для снижения рисков.</b>',
        'developmentSectionComment' => '<b>5.9. Комментарий к разделу V</b>'
    ];

    const SCHEDULE_DESCRIPTION = [
        'activityLabel' => '6.1. Перечислите <b>основные задачи / мероприятия / действия</b>, которые будут осуществлены <b>в первый год</b> реализации проекта<br><br>',
        'practiceImplementationActivities' => '<b>Реализация Практики</b>',
        'practiceSpreadActivities' => '<b>Распространение и внедрение практики</b>',
        'monitoringResultsActivities' => '<b>Мониторинг и оценка результатов. Развитие практики</b>',
        'activityDescriptionLabel' => '6.2. Кратко опишите основные <b>задачи / мероприятия / действия</b>, которые планируются <b>в течение второго и третьего годов</b> реализации проекта<br><br>',
        'practiceImplementationDescription' => '6.2.1. <b>Реализация практики проекта</b>',
        'practiceSpreadDescription' => '6.2.2. <b>Распространение и внедрение практики проекта</b>',
        'monitoringResultsDescription' => '6.2.3. <b>Мониторинг и оценка результатов. Развитие Практики</b>',
    ];

    const DOCUMENT_DESCRIPTION = [
        'appsLabel' => '<b>При подаче заявки Заявитель должен представить в электронном виде скан-копии (или ссылки, если это 
            оговорено дополнительно) следующих документов, заверенных подписью руководителя и печатью организации, и оформленных
            согласно инструкции: </b><br><br>',
        'regulation' => '<b>1.</b> Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего 
            органа; pdf',
        'projectBudget' => '<b>2.</b> Бюджет проекта – заполненный файл установленного образца; excel',
        'bankLetterExistenceAccount' => '<b>3.</b> Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта 
            (скан-копия); pdf ',
        'processingPersonalDataConsent' => '<b>4.</b> Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке;<br>
            <br><i>Для каждого из лиц заполняется отдельный документ, затем все они сканируются в один файл pdf. Скачать образец согласия 
            на обработку персональных данных (ссылка)</i><br><br><i>При заключении договора, в случае победы в Конкурсе, необходимо будет представить 
            оригиналы (с подписью) Информированного согласия физического лица на обработку персональных данных в рамках организации и проведения 
            Всероссийского конкурса «Семейный фарватер», заполненных отдельно на каждого члена команды проекта.</i>',
        'confirmingAuthorityPersonDocument' => '<b>5.</b> Документ, подтверждающий полномочия лица, которое будет подписывать 
            договор (<b>если</b> договор будет подписывать не руководитель организации); pdf',
        'confirmingLegalStatusEntityDocument' => '<b>6.</b> Документ, подтверждающий статус юридического лица (решение о создании 
            учреждения и т.п.) (<b>только</b> для государственных и муниципальных учреждений);',
        'latestAnnualReport' => '<b>7.</b> Последний годовой отчёт (при наличии) (скан-копия с подписью руководителя и печатью организации); pdf ',
        'latestAnnualReportLink' => 'или ссылка на его версию в сети Интернет',
        'ministryJusticeReport' => '<b>8.</b> Отчёт в Министерство юстиции Российской Федерации /иной регистрирующий орган за предшествующий отчетный период 
            (скан-копия, с подписью руководителя и печатью организации) (<b>за исключением</b> организаций-заявителей – государственных и 
            муниципальных учреждений). pdf',
        'ministryJusticeReportLink' => 'или ссылка на его версию, размещенную на Информационном портале Министерства юстиции Российской 
                Федерации по адресу: http://unro.minjust.ru/ ',
        'authorityHead' => '<b>9.</b> Документ, подтверждающий полномочия руководителя (например, протокол об избрании, приказ). pdf',
    ];

    /**
     * @var EntityManager $em
     */
    private $em;

    public function __construct($code, $class, $baseControllerName, EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->container = $container;
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                "Farvater-2018",
                "Budget"
            ]
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with(self::TABS_DESCRIPTION['tab_3'])
            ->add('projectPurpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['projectPurpose'],
                'attr' => array('maxlength' => 800)
            ))
            ->add('tasksHeadTarget', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['tasksHeadTarget']
            ))
            ->add('projectSocialResultsLabel', CustomTextType::class, [
                'help' => self::HELPS['projectSocialResults']
            ])
            ->add('projectSocialResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('projectSocialIndividualResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['individualResults'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('projectSocialComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['comment'],
                'attr' => array('maxlength' => 1000)
            ))
            ->add('projectDirectResultsLabel', CustomTextType::class, [
                'help' => self::HELPS['projectDirectResults']
            ])
            ->add('projectDirectResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.direct_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('projectDirectIndividualResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['individualResults'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.direct_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('projectDirectComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['comment'],
                'attr' => array('maxlength' => 1000)
            ))
            ->add('realizationPracticeApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['realizationPracticeApplications'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.double_context_application',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('realizationPractices', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['conditionRealizationPractice'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.risk',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('descriptionSectionComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['descriptionSectionComment']
            ))
            ->end()
            ->with(self::TABS_DESCRIPTION['tab_4'])
            ->add('motivationSpreadPractice', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['motivationSpreadPractice']
            ))
            ->add('experienceSpread', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['experienceSpread']
            ))
            ->add('specialistTargetGroupsLabel', CustomTextType::class, [
                'help' => self::HELPS['specialistTargetGroups']
            ])
            ->add('specialistTargetGroups', 'sonata_type_model', [
                'label' => self::DESCRIPTION['specialistTargetGroups'],
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'btn_add' => false,
            ])
            ->add('otherSpecialistTargetGroups', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['other'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('specialistProblemsLabel', CustomTextType::class, [
                'help' => self::HELPS['specialistProblems']
            ])
            ->add('specialistProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.application_2018.problem',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('technologySpread', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['technologySpread']
            ))
            ->add('introductionProjectPurpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['introductionProjectPurpose'],
                'attr' => array('maxlength' => 800)
            ))
            ->add('taskOfTarget', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['taskOfTarget']
            ))

            ->add('introductionPracticesLabel', CustomTextType::class, [
                'help' => self::SPREAD_PRACTICE_DESCRIPTION['introductionPracticesLabel']
            ])
            ->add('introductionPractices', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::SPREAD_PRACTICE_DESCRIPTION['introductionPractices'],
                    'by_reference' => false,
                    'btn_add' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.introduction_index',
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('introductionPracticeComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['comment'],
                'attr' => array('maxlength' => 1000)
            ))
            ->add('practiceSocialResultsLabel', CustomTextType::class, [
                'help' => self::HELPS['practiceSocialResults']
            ])
            ->add('practiceSocialResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('practiceSocialComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['comment'],
                'attr' => array('maxlength' => 1000)
            ))
            ->add('practiceDirectResultsLabel', CustomTextType::class, [
                'help' => self::HELPS['practiceDirectResults']
            ])
            ->add('practiceDirectResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => false,
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.direct_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('practiceDirectIndividualResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['individualResults'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.farvater.application_2018.direct_result',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('practiceDirectComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['comment'],
                'attr' => array('maxlength' => 1000)
            ))
            ->add('spreadPracticeApplications', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['spreadPracticeApplications'],
                'btn_add' => false,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.application_2018.double_context_application',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ))
            ->add('spreadPractices', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::SPREAD_PRACTICE_DESCRIPTION['conditionSpreadPractice'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.risk',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('spreadSectionComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::SPREAD_PRACTICE_DESCRIPTION['spreadSectionComment']
            ))
            ->end()
            ->with(self::TABS_DESCRIPTION['tab_5'])
            ->add('monitoringLabel', CustomTextType::class, [
                'help' => self::MONITORING_PRACTICE_DESCRIPTION['monitoringLabel']
            ])
            ->add('wishDevelopPractice', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::MONITORING_PRACTICE_DESCRIPTION['wishDevelopPractice']
            ))
            ->add('monitoringProjectPurpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::MONITORING_PRACTICE_DESCRIPTION['monitoringProjectPurpose'],
                'attr' => array('maxlength' => 800)
            ))
            ->add('expectedResultsLabel', CustomTextType::class, [
                'help' => self::HELPS['expectedResultsLabel']
            ])
            ->add('experienceItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['experience'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('experienceEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['experienceEtc'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('processItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['process'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('processEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['processEtc'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('qualificationItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['qualification'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('qualificationEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['qualificationEtc'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceBlockItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['resourceBlock'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceBlockEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['resourceBlockEtc'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.farvater.application_2018.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('risePotentialApplications', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::MONITORING_PRACTICE_DESCRIPTION['risePotentialApplications'],
                'btn_add' => false,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.application_2018.double_context_application',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ))
            ->add('risks', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::MONITORING_PRACTICE_DESCRIPTION['risk'],
                    'btn_add' => false,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.risk',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
            ))
            ->add('developmentSectionComment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::MONITORING_PRACTICE_DESCRIPTION['developmentSectionComment']
            ))
            ->end()
            ->with(self::TABS_DESCRIPTION['tab_6'])
            ->add('activityLabel', CustomTextType::class, array(
                    'help' => self::SCHEDULE_DESCRIPTION['activityLabel'],
            ))
            ->add('practiceImplementationActivities', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::SCHEDULE_DESCRIPTION['practiceImplementationActivities'],
                    'by_reference' => false,
                    'btn_add' => false,
                ), array(
                    'admin_code' => 'nko_order.admin.farvater2017.practice_implementation_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('practiceSpreadActivities', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::SCHEDULE_DESCRIPTION['practiceSpreadActivities'],
                    'by_reference' => false,
                    'btn_add' => false,
                ), array(
                    'admin_code' => 'nko_order.admin.farvater2017.practice_spread_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('monitoringResultsActivities', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::SCHEDULE_DESCRIPTION['monitoringResultsActivities'],
                    'by_reference' => false,
                    'btn_add' => false,
                ), array(
                    'admin_code' => 'nko_order.admin.farvater2017.monitoring_results_activity',
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('activityDescriptionLabel', CustomTextType::class, array(
                    'help' => self::SCHEDULE_DESCRIPTION['activityDescriptionLabel'],
            ))
            ->add('practiceImplementationDescription', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => self::SCHEDULE_DESCRIPTION['practiceImplementationDescription']
            ))
            ->add('practiceSpreadDescription', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => self::SCHEDULE_DESCRIPTION['practiceSpreadDescription']
            ))
            ->add('monitoringResultsDescription', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => self::SCHEDULE_DESCRIPTION['monitoringResultsDescription']
            ))
            ->end()
            ->with(self::TABS_DESCRIPTION['tab_7'])
            ->add('projectMembers', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['projectMembers'],
                    'by_reference' => false,
                    'btn_add' => false,
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('projectPartners', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['projectPartners'],
                    'by_reference' => false,
                    'btn_add' => false,
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->add('organizationResources', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['organizationResources'],
                    'by_reference' => false,
                    'btn_add' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
            ))
            ->end()
            ->with(self::TABS_DESCRIPTION['tab_8'])
            ->add('entirePeriodEstimatedFinancing', MoneyType::class, array(
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 5,
                    'label' => self::DESCRIPTION['entirePeriodEstimatedFinancing']
            ))
            ->add('entirePeriodEstimatedCoFinancing', MoneyType::class, array(
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 5,
                    'label' => self::DESCRIPTION['entirePeriodEstimatedCoFinancing']
            ))
            ->add('firstYearEstimatedFinancing', MoneyType::class, array(
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 5,
                    'label' => self::DESCRIPTION['firstYearEstimatedFinancing']
            ))
            ->add('firstYearEstimatedCoFinancing', MoneyType::class, array(
                    'required' => false,
                    'currency' => 'RUB',
                    'scale' => 5,
                    'label' => self::DESCRIPTION['firstYearEstimatedCoFinancing']
            ))
            ->end()
            ->with(self::TABS_DESCRIPTION['tab_9'])
            ->add('appsLabel', CustomTextType::class, array(
                    'help' => self::DOCUMENT_DESCRIPTION['appsLabel'],
            ))
            ->add('agreementTemplate', CustomTextType::class, array(
                'help' => ($agreement = $this->getSubject()->getCompetition()->getAgreement()) ? $agreement : false
            ))
            ->add('regulation', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['regulation']
            ))
            ->add('projectBudget', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['projectBudget'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('bankLetterExistenceAccount', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['bankLetterExistenceAccount'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('processingPersonalDataConsent', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['processingPersonalDataConsent'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('confirmingAuthorityPersonDocument', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['confirmingAuthorityPersonDocument'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('confirmingLegalStatusEntityDocument', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['confirmingLegalStatusEntityDocument'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('latestAnnualReport', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['latestAnnualReport'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('latestAnnualReportLink', UrlType::class, array(
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['latestAnnualReportLink'],
                    'help' => 'URL должен начинаться с http:// или https://'
            ))
            ->add('ministryJusticeReport', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['ministryJusticeReport'],
                    'data_class' => null,
                    'required' => false
            ))
            ->add('ministryJusticeReportLink', UrlType::class, array(
                    'required' => false,
                    'label' => self::DOCUMENT_DESCRIPTION['ministryJusticeReportLink'],
                    'help' => 'URL должен начинаться с http:// или https://'
            ))
            ->add('authorityHead', FilePreviewType::class, array(
                    'label' => self::DOCUMENT_DESCRIPTION['authorityHead'],
                    'data_class' => null,
                    'required' => false
            ))
            ->end()
            ;
    }
    
    public function postUpdate($object)
    {
        $this->updateTargetGroups($object);
        $this->updateSocialResults($object);
        $this->em->flush();

        parent::postUpdate($object);
    }

    public function preUpdate($object)
    {
        $analyticReports = $this->container->get('doctrine')->getRepository(Report::class)
            ->findReportsByApplication($object);

        foreach ($analyticReports as $report) {
            $this->container->get('NKO\OrderBundle\Loader\Loader')
                ->update($report, $object);
        }
    }

    private function updateTargetGroups(Application $application)
    {

        $targetGroups = $this->getArrayByApplication(RelatedTargetGroup::class, $application);

        $targetGroups = $this->modifyToStringArray($application->getSpecialistProblems(), $targetGroups);
        $targetGroups = $this->modifyToStringArray($application->getOtherSpecialistTargetGroups(), $targetGroups);

        foreach ($targetGroups as $value) {
            if (!($value instanceof RelatedTargetGroup)) {
                $this->em->persist($this->createObject(RelatedTargetGroup::class, $value, $application));
            }
        }
    }

    private function updateSocialResults(Application $application)
    {
        $socialResults = $this->getArrayByApplication(RelatedSocialResult::class, $application);

        $problems = $application->getSpecialistProblems();
        foreach ($problems as $problem) {
            $socialResults = $this->modifyToStringArray($problem->getSocialResults(), $socialResults);
        }

        foreach ($problems as $problem) {
            $title = $problem->getCustomSocialResult();
            if (array_search($title, $socialResults) === false && $title && $title != "-") {
                $socialResults[] = $title;
            }
        }

        foreach ($socialResults as $value) {
            if (!($value instanceof RelatedSocialResult)) {
                $this->em->persist($this->createObject(RelatedSocialResult::class, $value, $application));
            }
        }

        return $socialResults;
    }

    private function getArrayByApplication($class, $application)
    {
        $socialResults = $this->em->getRepository($class)->findBy([
            Farvater2018ApplicationUtils::PRACTICE_APPLICATION_LINKED_FIELD_NAME => $application
        ]);

        if (!$socialResults) {
            $socialResults = [];
        }

        return $socialResults;
    }

    private function createObject($class, $value, $application)
    {
        $item = new $class();
        $item->setTitle($value);
        $item->setPracticeApplication($application);
        return $item;
    }
}
