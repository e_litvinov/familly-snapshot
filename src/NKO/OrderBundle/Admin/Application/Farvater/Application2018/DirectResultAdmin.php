<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\Application2018;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedTargetGroup;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class DirectResultAdmin extends AbstractAdmin
{
    const DESCRIPTION = [
        'service'           => 'Наименование услуги, мероприятия и пр.',
        'targetGroup'       => 'Целевая группа ',
        'content'           => 'Непосредственный результат',
        'indicator'         => 'Показатель',
        'targetValue'       => 'Целевое значение на конец I года реализации проекта',
        'approximateValue'  => 'Целевое значение на конец реализации проекта',
    ];

    const CODE = [
        Farvater2018ApplicationUtils::PROJECT_DIRECT_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PROJECT_DIRECT_RESULT_INDICATOR_CODE,
        Farvater2018ApplicationUtils::PRACTICE_DIRECT_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PRACTICE_DIRECT_RESULT_INDICATOR_CODE
    ];

    const FIND_BY = [
        Farvater2018ApplicationUtils::PROJECT_DIRECT_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PROJECT_APPLICATION_FIELD_NAME,
        Farvater2018ApplicationUtils::PROJECT_DIRECT_INDIVIDUAL_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PROJECT_APPLICATION_FIELD_NAME,
        Farvater2018ApplicationUtils::PRACTICE_DIRECT_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PRACTICE_APPLICATION_LINKED_FIELD_NAME,
        Farvater2018ApplicationUtils::PRACTICE_DIRECT_INDIVIDUAL_RESULT_FIELD_NAME => Farvater2018ApplicationUtils::PRACTICE_APPLICATION_LINKED_FIELD_NAME
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $doctrine = $this->getConfigurationPool()->getContainer()->get('doctrine');

        $parentFieldName = $this->getParentFieldDescription()->getFieldName();

        $groups = $doctrine->getRepository(RelatedTargetGroup::class)->findBy([
            self::FIND_BY[$parentFieldName] => $this->getParentFieldDescription()->getAdmin()->getSubject()->getId()
        ]);

        $formMapper
            ->add('service', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['service'],
            ])
            ->add('targetGroup', null, [
                'label' => self::DESCRIPTION['targetGroup'],
                'choices' => $groups
            ])
            ->add('content', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['content'],
                'attr' => [ 'maxlength' => 1000 ]
            ])
        ;

        if (array_key_exists($parentFieldName, self::CODE)) {
            $formMapper
                ->add('indicator', null, [
                    'label' => self::DESCRIPTION['indicator'],
                    'choices' =>  $doctrine->getRepository(Indicator::class)->findBy([
                        'code' => self::CODE[$parentFieldName]
                    ])
                ])
            ;
        }
        else {
            $formMapper
                ->add('customIndicator', TextType::class, [
                    'label' => self::DESCRIPTION['indicator']
                ])
            ;
        }

        $formMapper
            ->add('targetValue', NumberType::class, [
                'label' => self::DESCRIPTION['targetValue']
            ])
            ->add('approximateValue', NumberType::class, [
                'label' => self::DESCRIPTION['approximateValue']
            ])
        ;
    }
}
