<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\BriefApplication2018;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\Application\TargetGroupProblemUtils;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;

class ProblemAdmin extends EmbeddedAdminDecorator
{
    const TYPES_APPLICATION = [
        'otherApplication' => [
            'targetGroup' => 'Целевая группа распространения Практики',
            'content' => 'Основные проблемы целевой группы, которые решает распространение Практики',
            'socialResults' => 'Социальные результаты распространения практики'],

        'briefApplication2018' => [
            'targetGroup' => 'Целевая группа (благополучатели) Практики',
            'content' => 'Основные проблемы целевой группы, на решение которых направлена Практика',
            'socialResults' => 'Социальные результаты применения Практики для целевых групп']
    ];

    const DESCRIPTION = [
        'custom' => 'Свой социальный результат',
        'competition' => 'Отметить, есть ли в данной целевой группе специалисты организаций - участников конкурсов «Семейный Фарватер» и «Курс на семью»'
    ];

    const PROBLEM = [
        'specialistProblems' => ApplicationTypes::CONTINUATION_APPLICATION_2018,
        'beneficiaryProblems' => ApplicationTypes::BRIEF_APPLICATION_2018 ,
        'briefApplication' => 'briefApplication2018',
        'otherApplication' => 'otherApplication'
    ];

    const PLACEHOLDER = [
        'custom' => 'Добавить свои социальные результаты'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $parentFieldName = $this->getParentFieldDescription()->getFieldName();
        $object = $this->getParentFieldDescription()->getAdmin()->getSubject();

        $applicationType = array_key_exists($parentFieldName, self::PROBLEM) ? self::PROBLEM[$parentFieldName] : false;
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();

        if (($parentFieldName == TargetGroupProblemUtils::SPECIALIST_PROBLEMS_FIELD_NAME) ||
            ($parentFieldName == TargetGroupProblemUtils::BENEFICIARY_PROBLEM_FIELD_NAME && !($object instanceof Application))) {
            $application = self::PROBLEM['briefApplication'];
        } else {
            $application = self::PROBLEM['otherApplication'];
        }

        $formMapper
            ->add('targetGroup', TextareaType::class, [
                'required' => false,
                'label' => self::TYPES_APPLICATION[$application]['targetGroup'],
                'attr' => ['readonly' => true]
            ]);
        if ($parentFieldName == TargetGroupProblemUtils::SPECIALIST_PROBLEMS_FIELD_NAME && !($object instanceof Application)) {
            $formMapper
            ->add('specialistCompetitions', 'sonata_type_model', array(
                'btn_add' => false,
                'by_reference' => false,
                'multiple' => true,
                'label' => self::DESCRIPTION['competition'],
                ), array('admin_code' => 'nko_order.admin.indicator'));
        }
        $formMapper
            ->add('content', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::TYPES_APPLICATION[$application]['content']
            ])

            ->add('socialResults', null, [
                'required' => false,
                'by_reference' => false,
                'label' => self::TYPES_APPLICATION[$application]['socialResults'],
                'choices' => $em->getRepository(SocialResult::class)->findCachedByApplicationTypeQuery($applicationType),
                'attr' => array('style' => 'margin-bottom: 10px')
             ])

            ->add('customSocialResult', TextareaType::class, [
                'required' => false,
                'attr' => array(
                    'placeholder' => self::PLACEHOLDER['custom'])
            ])
        ;
    }
}
