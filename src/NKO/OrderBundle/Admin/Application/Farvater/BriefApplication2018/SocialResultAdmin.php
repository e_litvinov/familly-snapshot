<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/12/18
 * Time: 3:26 PM
 */

namespace NKO\OrderBundle\Admin\Application\Farvater\BriefApplication2018;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class SocialResultAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name');
    }
}