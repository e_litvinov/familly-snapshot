<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\BriefApplication2018;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class FinancingSourceAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('type', null, array(
                'label' => false
            ))
            ->add('value', NumberType::class, array(
                'label' => false
            ))
        ;
    }
}
