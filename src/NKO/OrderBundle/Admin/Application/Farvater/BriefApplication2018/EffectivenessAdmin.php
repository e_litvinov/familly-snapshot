<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\BriefApplication2018;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EffectivenessAdmin extends AbstractAdmin
{
    const INDICATOR_ITEMS = [
        'effectivenessItems',
        'effectivenessImplementationItems',
        'effectivenessDisseminationItems'
    ];

    const CUSTOM_INDICATOR_ITEMS = [
        'effectivenessEtcItems',
        'effectivenessImplementationEtcItems',
        'effectivenessDisseminationEtcItems'
    ];

    const DESCRIPTION = [
        'result' => 'Социальные результаты',
        'indicator' => 'Показатели',
        'indicatorValue' => 'Достигнутые значения показателя (2017 год)'
     ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('result', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['result'],
                'attr' => ['maxlength' => 800]
            ])
        ;

        if (in_array($this->getParentFieldDescription()->getFieldName(),self::INDICATOR_ITEMS)) {
            $formMapper
                ->add('indicator', 'sonata_type_admin', array(
                    'label' => self::DESCRIPTION['indicator'],
                ), array(
                    'admin_code' => 'nko_order.admin.indicator'
                ))
            ;
        }

        if (in_array($this->getParentFieldDescription()->getFieldName(), self::CUSTOM_INDICATOR_ITEMS)) {
            $formMapper
                ->add('customIndicator', TextType::class, array(
                    'required' => false,
                    'label' => self::DESCRIPTION['indicator']
                ))
            ;
        }

        $formMapper
            ->add('indicatorValue', NumberType::class, [
                'label' => self::DESCRIPTION['indicatorValue'],
                'required' => false,
                'attr' => [
                    'min' => 0
                ]
             ])
        ;
    }
}
