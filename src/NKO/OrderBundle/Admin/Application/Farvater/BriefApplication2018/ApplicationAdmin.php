<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\BriefApplication2018;

use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;
use NKO\OrderBundle\Entity\DropDownEntity;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use NKO\OrderBundle\Entity\Region;
use NKO\OrderBundle\Form\DropDownListType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\UserBundle\Traits\CurrentUserTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\OrganizationActivityAdmin;

class ApplicationAdmin extends BaseApplicationAdmin
{
    const ORGANIZATION_INFO_DESCRIPTION = [
        'dateRegistrationOfOrganization' => '2.1 <b>Дата регистрации организации</b>',
        'primaryActivity' => '2.2 <b>Основные виды деятельности организации</b>(согласно Уставу, соответствующие направлениям Конкурса и предлагаемому проекту)',
        'mission'=>'2.3 <b>Миссия</b> организации (при наличии)',
        'countEmployeesLabel'=>'2.4 Количество <b>сотрудников и добровольцев</b> организации',
        'projects' => '2.5 <b>Основные реализованные проекты (программы) организации</b> за последние два года (по теме данного Конкурса)',
        'publications'=>'2.6 <b>Публикации об организации за последние 2 года.</b><br><br><i>Укажите не более 5 релевантных материалов</i>',
        'linkToAnnualReport'=> '2.7 Ссылка на последний <b>годовой отчет</b> Организации (при наличии)',
    ];

    const DESCRIPTION = [
        'contactsTab' => 'КОНТАКТНАЯ ИНФОРМАЦИЯ',
        'organizationTab' => '2. ОРГАНИЗАЦИЯ-ЗАЯВИТЕЛЬ',
        'practiceDescriptionTab' => '3. ОПИСАНИЕ ПРАКТИКИ',
        'practiceName' => '3.1 <b>Название Практики</b>',
        'priorityDirection' => '3.2 <b>Приоритетное направление Конкурса</b>, в рамках которого реализуется практика',
        'priorityDirectionEtc' => '<b>Иные эффективные практики</b> в сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, 
            оставшихся без попечения родителей (укажите, какие именно)',
        'otherBeneficiaryGroups' => '<b>Иные группы</b>(укажите)',
        'effectivenessEtc' => '<b>Иные показатели</b><br><br><i>В этой таблице Вы можете добавить свои показатели.</i>',
        'effectivenessPlan' => '<i>3.7.2. Как вы планируете повысить эффективность и доказанность практики в ходе реализации проекта?</i>',
        'introductionPracticeTab' => '4. РАСПРОСТРАНЕНИЕ И ВНЕДРЕНИЕ ПРАКТИКИ',
        'isReady' => '<b>Готовы ли вы проактивно распространять вашу практику:</b>',
        'monitoringTab' => '5. МОНИТОРИНГ И ОЦЕНКА',
        'advantages' => '<b>Сильные стороны</b>',
        'disadvantages' => '<b>Слабые стороны</b>',
        'practiceDevelopment' => '<b>5.2. Каким образом вы планируете повышать эффективность и доказанность вашей Практики, развивать систему мониторинга и оценки?</b>',
        'documentsTab' => '6. ДОКУМЕНТЫ',
        'regulation' => '1. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа',
        'signedAgreement' => '2. Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке',
        'isApproved' => 'Подтверждаю достоверность информации (в том числе документов), представленной в составе заявки на участие во Всероссийском конкурсе «Семейный фарватер»',
        'anotherFinancingSource' => 'Другое (указать, что именно)',
        'isReadyEtc' => '<b>Иное </b><i>(уточните)</i>:'
    ];

    const HELPS = [
        'projectDescriptionAnnotation' => '<i>В данном разделе описывается конкретная Практика вашей организации (модель, технология, методика, услуга и пр.):
            <ul>
            <li>которая направлена на профилактику сиротства или на семейное устройство детей-сирот и детей, оставшихся без попечения родителей;</li>
            <li>которая уже успешно используется вашей организацией (в чем особенно сильна ваша организация, ваша «изюминка»);</li>
            <li>в отношении которой уже есть данные, позволяющие делать вывод об ее эффективности;</li>
            <li>опытом использования которой ваша организация готова делиться с другими специалистами и организациями.</li>
            <li>Необходимо включить краткое описание механизма достижения социальных результатов</li>
            </ul></i>',
        'briefPracticeDescription' => '3.3. <b> Аннотация: краткое описание сути Практики</b>
            <br><i>Кратко опишите, в чем суть представляемой вами на Конкурс Практики; каким образом ваша практика приводит к заявляемым результатам: 
            опишите те шаги (алгоритм действий) вашей практики, которые приводят к значимым результатам (представьте эти результаты). Обратите внимание, 
            что результаты реализации вашей практики должны быть созвучны тем результатам, на достижение которых направлен конкурс.
            Если вы считаете, что ваша практика уникальна (например, узкая целевая группа, с которой не многие организации работают, 
            или есть собственные ноу-хау, или есть узкий фокус работы, направленный на достижение результатов и т.п.), то подчеркните это.</i>',
        'practiceRegions' => '3.4. <b>Территория реализации Практики</b><br>
            <i>В каком регионе (регионах) РФ ваша организация сейчас реализует представленную на Конкурс Практику?</i>',
        'beneficiaryGroups' => '<b>3.5. Проблемы целевых групп, на решение которых направлена Практика; социальные результаты применения Практики для целевых групп<br><br>
            3.5.1. Целевые группы (благополучатели) Практики</b><br><br><i>Конкурс «Семейный фарватер» и Программа «Семья и дети» реализуются, 
            чтобы в жизни конкретных групп людей произошли позитивные изменения. Основные группы благополучателей представлены ниже. 
            Укажите, на какие именно группы благополучателей преимущественно направлена ваша Практика. 
            Вы можете также дополнительно добавить те группы благополучателей, которые отсутствуют в данном списке.</i>',
        'problems' => '<b>3.5.2. Основные проблемы благополучателей и социальные результаты применения практики</b><br><br>
            <i><b>Укажите основные проблемы  целевых групп (благополучателей), на решение которых направлена ваша Практика и изменения (социальные результаты) , 
            которые произойдут  у целевых групп (благополучателей)  благодаря решению этих проблем в результате применения вашей Практики. 
            Например, изменение установок, мотивов, личностных ресурсов, психологического состояния, позиции представителей значимого окружения, 
            развитие новых навыков преодоления проблемной ситуации, адаптации, появление поддерживающих ресурсов, и т.п.</b><br><br>
            <b>Программа «Семья и дети» Фонда Тимченко направлена на достижение следующих социальных результатов:</b>
            <ul>
            <li>Увеличение числа детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства (в том числе подростков, детей с ОВЗ, сиблингов);</li>
            <li>Увеличение числа детей, возвращенных в кровные семьи (в том числе подростков и детей с ОВЗ);</li>
            <li>Уменьшение количества изъятий/отказов детей из кровных семей;</li>
            <li>Уменьшение количества изъятий/отказов детей из замещающих семей;</li>
            <li>Улучшение благополучия детей и семей – участников Программы;</li>
            <li>Рост уровня готовности детей к самостоятельной жизни – они становятся полноценными гражданами, обеспечивающими благополучие общества.</li>
            </ul>
            Вы можете выбрать один или несколько вариантов социальных результатов из списка и указать под ним свои - через точку с запятой.</i>',
        'effectiveness' => '<b>3.6. Результативность практики </b><br><br>
            <i>В рамках реализации Программы «Семья и дети» Фонд Тимченко осуществляет мониторинг по указанным в п 3.5.2. социальным результатам.</i><br><br>
            <b>Укажите сведения о целевых значениях показателей, достигнутых в ходе реализации вашей Практики (не организации!) за 2017 год. </b><br><br>
            <i>Обратите также внимание, что отчётность организаций-победителей Конкурса также преимущественно ориентирована на предоставление сведений 
            о достижении социальных результатов, важных для Фонда.</i><br><br>
            <i>Вы начинаете заполнять таблицу, проставляя значения для приведенных в ней показателей. 
            Затем к ненулевым показателям слева прописываете через точку с запятой один или несколько социальных результатов, указанных Вами в п.3.5.2.</i><br><br>
            <i>В случае, если ваша Практика не работает на тот или иной показатель, поставьте справа от показателя ноль, слева - прочерк. </i><br><br>
            <i>В случае, если у вас нет точных сведений о целевых значениях того или иного показателя, укажите ориентировочное число.</i><br><br>
            <i>Все социальные результаты, указанные в п.3.5.2, должны быть в п.3.6. отражены.</i><br>',
        'effectivenessFact' => '<b>3.7.  Данные об эффективности Практики </b><br><br>
            <i>3.7.1 Как вы определяете достижение обозначенных в п. 3.6 результатов? Какие методы и инструменты для этого используются? 
            Что наиболее убедительно доказывает, что ваша практика эффективна? 
            Если проводились внутренние и внешние оценочные исследования вашей практики (как вашей организацией, так и иными), 
            публикации на профессиональных ресурсах и пр. или мониторинг достигнутых результатов, то укажите, где можно с ними ознакомиться (ссылки и пр.).</i>',
        'introduction' => '<i>4.1. Обратите внимание, что Конкурс «Семейный фарватер» и Программа «Семья и дети» направлены, 
            в том числе, на создание ресурсных центров и стажировочных площадок на базе организаций-победителей. 
            Организации, заинтересованные в распространении Практики, должны быть готовы не только проводить обучающие мероприятия, 
            но и принимать коллег у себя в качестве стажеров, делиться с ними инструментарием и рабочей документацией. 
            А также, что очень важно, быть нацеленными на внедрение своей практики в деятельность заинтересованных партнеров, 
            ощущать свою ответственность за этот социальный результат. <br><br>
            Готовы ли вы проактивно распространять вашу практику:</i>',
        'monitoring' => '<i>Одна из задач конкурса «Семейный фарватер» - содействовать развитию доказательной базы эффективности поддержанных практик 
            и повышению потенциала организаций (победителей   Конкурса) по   теме   профилактики социального сиротства и семейного устройства, 
            а также в области мониторинга, измерения и оценки результатов.</i><br><br>
            <b>5.1. Выделите сильные и слабые стороны мониторинга измерения и оценки результатов вашей практики:</b>',
        'agreement' => 'Согласие на обработку персональных данных заполняется следующими лицами:
            <ol>
            <li> руководителем организации;</li>
            <li> руководителем проекта;</li>
            <li> главным бухгалтером;</li>
            <li> лицом, подающим заявку</li>
            </ol>
            Для каждого из лиц заполняется отдельный документ, если они не совпадают.<br>',
        'isApproved' => '<i>При заключении договора, в случае победы в Конкурсе, необходимо будет представить оригиналы (с подписью) 
            Информированного согласия физического лица на обработку персональных данных в рамках организации и проведения 
            Всероссийского конкурса «Семейный фарватер», заполненных отдельно на каждого члена команды проекта, включая руководителя и бухгалтера организации.</i>',
        'financingSources' => '<b>2.7. Основные источники финансирования на реализацию Практики</b> (в 2017 году)<br>
            <i>Укажите, за счет каких средств сейчас реализуется Практика в вашей организации? Укажите примерную долю в % по каждому источнику.</i>'
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                "BriefApplication-2018",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting'
            ]
        );
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper
            ->with(self::DESCRIPTION['contactsTab']);
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], $em);
        $formMapper
            ->end()
            ->with(self::DESCRIPTION['organizationTab']);
        OrganizationActivityAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], null, self::ORGANIZATION_INFO_DESCRIPTION);
        $formMapper
            ->add('financingSourceLabel', CustomTextType::class, array(
                'help' => self::HELPS['financingSources'],
            ))
            ->add('financingSources', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'by_reference' => false,
                'btn_add' => false,
            ), array(
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('anotherFinancingSource', TextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['anotherFinancingSource'],
                'attr' => ['maxlength' => 500]
            ))
            ->end()
            ->with(self::DESCRIPTION['practiceDescriptionTab'])
            ->add('projectDescriptionAnnotation', CustomTextType::class, array(
                'help' => self::HELPS['projectDescriptionAnnotation'],
            ))
            ->add('projectName')
            ->add('projectName', TextType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['practiceName'],
                'attr' => array('maxlength' => 350),
            ))
            ->add('priorityDirection', 'sonata_type_model', array(
                'choices' => $em->getRepository(PriorityDirection::class)
                    ->findBy([
                        'applicationType' => ['application', 'brief_application']
                    ]),
                'label' => self::DESCRIPTION['priorityDirection'],
                'property' => 'name',
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false
            ))
            ->add('priorityDirectionEtc', TextType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['priorityDirectionEtc'],
                'attr' => array('maxlength' => 255)
            ))
            ->add('briefPracticeDescriptionLabel', CustomTextType::class, array(
                'help' => self::HELPS['briefPracticeDescription']
            ))
            ->add('briefPracticeDescription', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => false,
                'attr' => array('maxlength' => 2500)
            ))
            ->add('practiceRegionLabel', CustomTextType::class, array(
                'help' => self::HELPS['practiceRegions'],
            ))
            ->add('practiceRegions', 'sonata_type_model', array(
                'required' => false,
                'property' => 'name',
                'label' => false,
                'multiple' => true,
                'choices' => $em->getRepository(Region::class)->findBy([], [
                    'name' => 'ASC'
                ])
            ))
            ->add('beneficiaryGroupsLabel', CustomTextType::class, array(
                'help' => self::HELPS['beneficiaryGroups'],
            ))
            ->add('peopleCategories', DropDownListType::class, array(
                'class' => PeopleCategory::class,
                'required' => false,
                'label' => false,
                'multiple' => true
            ))
            ->add('otherBeneficiaryGroups', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['otherBeneficiaryGroups'],
                'btn_add' => false,
                'by_reference' => false
            ), array(
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('problemsLabel', CustomTextType::class, array(
                'help' => self::HELPS['problems'],
            ))
            ->add('beneficiaryProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.problem',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('effectivenessLabel', CustomTextType::class, array(
                'help' => self::HELPS['effectiveness'],
            ))
            ->add('effectivenessItems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => false,
                'by_reference' => false,
                'type_options' => array(
                    'delete' => false
                ),
            ), array(
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('effectivenessEtcItems', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['effectivenessEtc'],
                'btn_add' => false,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('effectivenessFactLabel', CustomTextType::class, array(
                'help' => self::HELPS['effectivenessFact'],
            ))
            ->add('effectivenessFact', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => false,
                'attr' => array('maxlength' => 4000)
            ))
            ->add('effectivenessPlan', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['effectivenessPlan'],
                'attr' => array('maxlength' => 4000)
            ))
            ->end()
            ->with(self::DESCRIPTION['introductionPracticeTab'])
            ->add('introductionLabel', CustomTextType::class, array(
                'help' => self::HELPS['introduction'],
            ))
            ->add('isReady', DropDownListType::class, array(
                'class' => DropDownEntity::class,
                'required' => false,
                'label' => false,
                'multiple' => true
            ))
            ->add('isReadyEtc', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION['isReadyEtc'],
                'required' => false,
                'attr' => array('maxlength' => 1000)
            ))
            ->end()
            ->with(self::DESCRIPTION['monitoringTab'])
            ->add('monitoringLabel', CustomTextType::class, array(
                'help' => self::HELPS['monitoring'],
            ))
            ->add('advantages', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['advantages']
            ))
            ->add('disadvantages', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['disadvantages']
            ))
            ->add('practiceDevelopment', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['practiceDevelopment']
            ))
            ->end()
            ->with(self::DESCRIPTION['documentsTab'])
            ->add('regulation', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DESCRIPTION['regulation']
            ))
            ->add('signedAgreement', FilePreviewType::class, array(
                'data_class' => null,
                'required' => false,
                'label' => self::DESCRIPTION['signedAgreement']
            ))
            ->add('agreementLabel', CustomTextType::class, array(
                'help' => self::HELPS['agreement'],
            ))
            ->add('agreementTemplate', CustomTextType::class, array(
                'help' => ($agreement = $this->getSubject()->getCompetition()->getAgreement()) ? $agreement : false
            ))
            ->add('isApprovedLabel', CustomTextType::class, array(
                'help' => self::HELPS['isApproved'],
            ))
            ->add('isApproved', CheckboxType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['isApproved']
            ))
            ->end()
        ;
    }
}
