<?php

namespace NKO\OrderBundle\Admin\Application\Farvater\BriefApplication2018;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class OrganizationFormAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code', TextType::class)
            ->add('title', TextType::class)
            ->add('applications', 'sonata_type_model', array(
                'multiple' => true,
                'by_reference' => false,
                'btn_add' =>false));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('code')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'delete'=>array(),
                    'edit' => array()
                )));
    }
}
