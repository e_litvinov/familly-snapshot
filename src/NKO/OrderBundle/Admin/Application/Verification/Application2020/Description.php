<?php

namespace NKO\OrderBundle\Admin\Application\Verification\Application2020;

class Description
{
    const CHARACHTER_LIMIT_HELP = 'До 255 символов.';
    const CHARACHTER_LIMIT_HELP_450 = 'До 450 символов.';
    const CHARACHTER_LIMIT_HELP_1000 = 'До 1000 символов.';
    const CHARACHTER_LIMIT_HELP_1500 = 'До 1500 символов.';

    const DESCRIPTIONS = [
        'abbreviation' => '<b>1.2 Сокращенное название </b>организации',
        'firstYearEstimatedFinancing' => "<b>3.3. Ориентировочная сумма запрашиваемого финансирования</b> на весь 
            период реализации проекта (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»)",
        'subjectRegulation' => '1. Заявка – по форме установленного образца в формате docx',
        'regulation' => '2. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой 
            регистрирующего органа в формате pdf',
        'anotherHead' => '3.  Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ) 
            в формате pdf',
        'authorityHead' => '4. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор 
            будет подписывать не руководитель организации) в формате pdf',
        'signedAgreement' => '5.  Информированные согласия на обработку персональных данных, заполненные на всех лиц, 
            упоминаемых в заявке (с личной подписью) в формате pdf',
        'isBankAccountExist' => '6. Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта 
            (скан-копия) в формате pdf',
        'reportForMinistryOfJustice' => 'Отчёт в Министерство юстиции РФ за предшествующий отчётный период или ссылка на его версию, размещённую 
            на Информационном портале Министерства юстиции РФ (за исключением государственных и муниципальных учреждений) в формате pdf',
        'linkToReportForMinistryOfJustice' => false,
        'organizationCreationResolution' => '8. Письма поддержки в формате pdf',
        'projectName' => "<b>1.16. Название практики</b>",
        'isOrganizationHeadEqualProjectHead' => 'Руководителем практики является руководитель организации?',
    ];

    const HELPS = [
        'headOfProjectLabel' => '<div id="_headOfProject">1.12. <b>Руководитель практики (ключевой сотрудник, ответственный за реализацию практики)</b></div>',
        'headOfOrganizationFullName' => self::CHARACHTER_LIMIT_HELP,
        'headOfOrganizationPosition' => self::CHARACHTER_LIMIT_HELP,
        'headOfProjectFullName' => self::CHARACHTER_LIMIT_HELP,
        'headOfProjectPosition' => self::CHARACHTER_LIMIT_HELP,
        'headOfAccountingFullName' => self::CHARACHTER_LIMIT_HELP,
        'oKVED' => self::CHARACHTER_LIMIT_HELP,
        'bankName' => self::CHARACHTER_LIMIT_HELP,
        'bankLocation' => self::CHARACHTER_LIMIT_HELP,
    ];

    const TABS = [
        'budget' => 'ОПИСАНИЕ ПРОЕКТА'
    ];
}
