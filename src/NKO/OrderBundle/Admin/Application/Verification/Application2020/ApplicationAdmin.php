<?php

namespace NKO\OrderBundle\Admin\Application\Verification\Application2020;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Entity\SettingEntity\FederalDistrict;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ApplicationAdmin extends AdminDecorator
{

    const DESCRIPTIONS = [
        'legalFederalDistrict' => 'Федеральный округ',
        'legalLocality' => 'Район',
        'actualFederalDistrict' => 'Федеральный округ',
        'actualLocality' => 'Район',
        'linkToAnnualReport' => '<b>1.14 Ссылка на последний годовой отчёт</b> <i> (при наличии)</i> ',
        'normativeAct' => '<b>1.15 Нормативные акты, которые применяются в деятельности организации</b>',
        'deadLineLabel' => '<b>3.2. Сроки реализации проекта</b>',
        'measurementTools' => '9. Инструмент измерения показателя (сбора данных)',
        'resultConfirmingDocuments' => '10. Документы, подтверждающие достижение результата/показателя',
        'performanceConfirmingDocuments' => '11. Есть ли у вас в наличии другие доказательства, которые могли бы 
            дополнить данные о результативности вашей практики, но не нашли отражения в таблице показателей? Уточните, 
            какие именно доказательства имеются',
        'practiceRegulationDocuments' => '12. Базовые регламенты практики',
        'resultDocuments' => '13. Существует ли разработанная Цепочка социальных результатов (теория изменений/логическая 
            модель/дерево результатов) практики? (добавьте в приложения или дайте ссылки в тексте на эти материалы).',
    ];

    public function __construct($code, $class, $baseControllerName, $admin = null, $em = null)
    {
        parent::__construct($code, $class, $baseControllerName, $admin, $em);

        $this->formOptions = [
            'validation_groups' => [
                "Verification-2020",
                'Address',
                'AdditionalAddress',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                "AdditionBudget",
                "BankDetails",
            ]
        ];
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        // FIRST TAB
        $wrappedFormMapper
            ->remove('competition')
            ->remove('pSRN')
            ->remove('author.nko_name')
        ;
        $this->addAdditionalLocation($wrappedFormMapper);
        $wrappedFormMapper->add('linkToAnnualReport', 'headOfAccountingEmeil', TextType::class, [
            'label' => self::DESCRIPTIONS['linkToAnnualReport'],
            'required' => false,
            'help' => 'URL должен начинаться с http:// или https://',
        ]);

        $wrappedFormMapper
            ->add('normativeActs', 'linkToAnnualReport', EntityType::class, [
                'class' => NormativeAct::class,
                'label' => self::DESCRIPTIONS['normativeAct'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ApplicationTypes::KNS_APPLICATION_3_2018]);
                },
                'multiple' => true,
            ])
        ;

        $wrappedFormMapper
            ->move('projectName', 'normativeActs')
            ->removeTab('budget')
        ;

        //Third tab
        $wrappedFormMapper
            ->remove('deadLineLabel')
            ->remove('deadLineStart')
            ->remove('deadLineFinish')
        ;

        //FOURTH tab
        $wrappedFormMapper
            ->remove('projectDescriptionDocument')
            ->remove('budget')
            ->move('subjectRegulation', 'agreementTemplate')
            ->move('regulation', 'subjectRegulation')
            ->move('anotherHead', 'regulation')
            ->move('authorityHead', 'anotherHead')
            ->move('signedAgreement', 'authorityHead')
            ->move('isBankAccountExist', 'signedAgreement')
            ->move('reportForMinistryOfJustice', 'isBankAccountExist')
            ->move('linkToReportForMinistryOfJustice', 'reportForMinistryOfJustice')
            ->move('organizationCreationResolution', 'linkToReportForMinistryOfJustice')
            ->add('measurementTools', 'organizationCreationResolution', 'sonata_type_collection', [
                'label' => self::DESCRIPTIONS['measurementTools'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.document_with_title',
                'titleLabel' => 'Инструмент',
                'fileLabel' => 'Приложение (в формате pdf, doc или xls)',
            ])
            ->add('resultConfirmingDocuments', 'measurementTools', 'sonata_type_collection', [
                'label' => self::DESCRIPTIONS['resultConfirmingDocuments'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.document_with_fields',
                'firstFieldLabel' => 'Показатель',
                'secondFieldLabel' => 'Документ',
                'fileLabel' => 'Приложение (в формате pdf, doc или xls)',
            ])
            ->add('performanceConfirmingDocuments', 'resultConfirmingDocuments', 'sonata_type_collection', [
                'label' => self::DESCRIPTIONS['performanceConfirmingDocuments'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.document_with_title',
                'titleLabel' => 'Доказательство',
                'fileLabel' => 'Приложение (в формате pdf, doc или xls)',
            ])
            ->add('practiceRegulationDocuments', 'performanceConfirmingDocuments', 'sonata_type_collection', [
                'label' => self::DESCRIPTIONS['practiceRegulationDocuments'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.document_with_title',
                'titleLabel' => 'Базовый регламент практики',
                'fileLabel' => 'Приложение (в формате pdf, doc или xls)',
            ])
            ->add('resultDocuments', 'practiceRegulationDocuments', 'sonata_type_collection', [
                'label' => self::DESCRIPTIONS['resultDocuments'],
                'by_reference' => false,
                'required' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.document_with_title',
                'titleLabel' => 'Цепочка социальных результатов',
                'fileLabel' => 'Приложение (в формате pdf, doc или xls)',
            ])
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
    }

    public function addAdditionalLocation(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->add('legalFederalDistrict', 'legalPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['legalFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('legalLocality', 'legalRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['legalLocality'],
            ])
            ->add('actualFederalDistrict', 'actualPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['actualFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('actualLocality', 'actualRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['actualLocality'],
            ])
        ;
    }
}
