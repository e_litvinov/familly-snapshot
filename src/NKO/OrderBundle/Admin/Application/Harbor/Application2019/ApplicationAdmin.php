<?php

namespace NKO\OrderBundle\Admin\Application\Harbor\Application2019;

use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\AdminConfigurator\BankDetailsAdmin;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationAdmin extends AdminDecorator
{
    const TABS = [
        'contactsTab' => 'ИНФОРМАЦИЯ ОБ ОРГАНИЗАЦИИ',
        'bankDetailsTab' => 'БАНКОВСКИЕ РЕКВИЗИТЫ',
        'budget' => 'БЮДЖЕТ ПРОЕКТА',
        'documentsTab' => 'ДОКУМЕНТЫ',
    ];

    const DOCUMENT_DESCRIPTION =[
        'subjectRegulation' => '1. Форма заявки в формате docx',
        'regulation' => '2 Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
        'authorityHead' => '3. Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.) в формате pdf',
        'anotherHead' => '4. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации) в формате pdf',
        'signedAgreement' => '5. Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью) в формате pdf',
        'budget' => '6. Бюджет проекта (файл в формате Excel по форме установленного образца в формате xlsx',
        'organizationCreationResolution' => '7. Документ, подтверждающий статус юридического лица государственных и муниципальных
            учреждений (решение о создании учреждения и т.п.) – только для государственных и муниципальных учреждений. в формате pdf',
        'isBankAccountExist' => '8. Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта (скан-копия) в формате pdf',
        'reportForMinistryOfJustice' => '9. Отчёт в Министерство юстиции Российской Федерации за предшествующий отчетный период 
            (скан-копия, с подписью руководителя и печатью организации) или ссылка на его версию, размещенную на Информационном 
            портале Министерства юстиции РФ (за исключением организаций-заявителей – государственных и муниципальных учреждений)
            <br><br>Файл в формате pdf:',
    ];

    const DESCRIPTION = [
        'linkToReportForMinistryOfJustice' => "или ссылка:",
        'linkToReportForMinistryOfJusticeHelp' => "URL должен начинаться с http:// или https://",
        'firstYearEstimatedFinancing' => "<b>3.2.</b> Сумма запрашиваемого финансирования 
            <b>на первый год</b> реализации проекта (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»)
            <i>(в тыс. рублей)</i>",
        'projectNameLabel' => "3.1. <b>Название проекта</b>",
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = [
            'validation_groups' => [
                "Harbor-2019",
                'Address',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                "AdditionBudget",
                "BankDetails",
            ]
        ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $subject = $this->getSubject() ?: $this->getRelatedObject();

        $formMapper->with(self::TABS['contactsTab']);
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($subject)], $em);
        $formMapper->end();

        $formMapper->with(self::TABS['bankDetailsTab']);
        BankDetailsAdmin::configureFormFields($formMapper, AdminConfigurator::BANK_DETAILS[get_class($subject)]);
        $formMapper->end();

        $formMapper->with(self::TABS['budget']);
        $formMapper
            ->add('projectName', TextareaType::class, array(
                'label' => self::DESCRIPTION['projectNameLabel'],
                'required' => false,
                'attr' => ['maxlength' => 450]
            ))
            ->add('firstYearEstimatedFinancing', TextType::class, [
                'label' => self::DESCRIPTION['firstYearEstimatedFinancing'],
                'required' => false
            ]);
        $formMapper->end();

        $formMapper->with(self::TABS['documentsTab']);
        $formMapper
            ->add('agreementTemplate', CustomTextType::class, array(
                'help' => ($agreement = $subject->getCompetition()->getAgreement()) ? $agreement : false
            ))
            ->add('subjectRegulation', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['subjectRegulation']
            ])
            ->add('regulation', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['regulation']
            ])
            ->add('authorityHead', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['authorityHead']
            ])
            ->add('anotherHead', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['anotherHead']
            ])
            ->add('signedAgreement', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['signedAgreement']
            ])
            ->add('budget', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['budget']
            ])
            ->add('organizationCreationResolution', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['organizationCreationResolution']
            ])
            ->add('isBankAccountExist', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['isBankAccountExist']
            ])
            ->add('reportForMinistryOfJustice', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DOCUMENT_DESCRIPTION['reportForMinistryOfJustice']
            ])
            ->add('linkToReportForMinistryOfJustice', TextType::class, [
                'required' => false,
                'help' => self::DESCRIPTION['linkToReportForMinistryOfJusticeHelp'],
                'label' => self::DESCRIPTION['linkToReportForMinistryOfJustice'],
                'attr' => [
                    'placeholder' => '-'
                ]
            ])
        ;
        $formMapper->end();
    }
}
