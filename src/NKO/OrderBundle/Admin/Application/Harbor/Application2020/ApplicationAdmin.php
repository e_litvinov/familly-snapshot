<?php

namespace NKO\OrderBundle\Admin\Application\Harbor\Application2020;

use Doctrine\ORM\EntityRepository;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\TrainingGround;
use NKO\OrderBundle\Entity\SettingEntity\FederalDistrict;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationAdmin extends AdminDecorator
{
    public function __construct($code, $class, $baseControllerName, $admin = null, $em = null)
    {
        parent::__construct($code, $class, $baseControllerName, $admin, $em);

        $this->formOptions = [
            'validation_groups' => [
                "Harbor-2020",
                'Address',
                'AdditionalAddress',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                "AdditionBudget",
                "BankDetails",
            ]
        ];
    }

    const DESCRIPTIONS = [
        'legalFederalDistrict' => 'Федеральный округ',
        'legalLocality' => 'Район',
        'actualFederalDistrict' => 'Федеральный округ',
        'actualLocality' => 'Район',
        'normativeAct' => '<b>1.14 Нормативные акты, которые применяются в деятельности организации</b>',
        'updatedAt' => '<b>1.15 Дата отправки</b> заявки',
        'deadLineLabel' => '<b>3.2. Сроки реализации</b> проекта',
        'startDateLabel' => ' 3.3. <b>Дата начала реализации проекта</b> <i>(не ранее 11.05.2020):</i>',
        'finishDateLabel' => ' 3.4. <b>Дата окончания реализации проекта</b> <i>(не позднее 15.11.2021, не менее 12 месяцев):</i>',
        'practiceName' => ' 3.5. <b>Название практики</b>',
        'trainingGround' => '<b>3.1. Приоритетное направление Конкурса </b>(оставьте только один вариант):',
        'projectDescriptionDocument' => '2. Описание практики – по форме установленного образца в формате docx',
    ];

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->remove('competition')
            ->remove('pSRN')
            ->remove('author.nko_name')
        ;

        $wrappedFormMapper
            ->remove('headOfAccountingMobilePhoneLabel')
            ->remove('headOfAccountingMobilePhoneInternationalCode')
            ->remove('headOfAccountingMobilePhoneCode')
            ->remove('headOfAccountingMobilePhone')
        ;
        $wrappedFormMapper->move('linkToReportForMinistryOfJustice', 'headOfAccountingEmeil');
        $this->addAdditionalLocation($wrappedFormMapper);

        $wrappedFormMapper
            ->add('normativeActs', 'linkToReportForMinistryOfJustice', EntityType::class, [
                'class' => NormativeAct::class,
                'label' => self::DESCRIPTIONS['normativeAct'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ApplicationTypes::KNS_APPLICATION_3_2018]);
                },
                'multiple' => true,
            ])
        ;

        $wrappedFormMapper
            ->add('trainingGround', 'projectName', EntityType::class, [
                'class' => TrainingGround::class,
                'label' => self::DESCRIPTIONS['trainingGround'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.code = :name')
                        ->setParameters(['name' => ApplicationTypes::HARBOR_2020]);
                },
                'required' => false,
                'multiple' => false,
                'expanded' => true,
            ], [], true)
            ->move('projectName', 'trainingGround')
            ->add('deadLineStart', 'projectName', 'sonata_type_datetime_picker', [
                'label' => self::DESCRIPTIONS['startDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_min_date' => '11/05/2020',
                'dp_max_date' => '16/11/2020',
                'dp_default_date' => '11/05/2020',
                'dp_pick_time' => false,
                'required' => false,
            ])
            ->add('deadLineFinish', 'deadLineStart', DateTimePickerType::class, [
                'label' => self::DESCRIPTIONS['finishDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_min_date' => '10/05/2021',
                'dp_max_date' => '15/11/2021',
                'dp_default_date' => '10/05/2021',
                'dp_pick_time' => false,
                'required' => false
            ])
            ->add('practiceName', 'deadLineFinish', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTIONS['practiceName'],
                'required' => false,
                'attr' => ['maxlength' => 450]
            ))


            ->move('subjectRegulation', 'agreementTemplate')
            ->add('projectDescriptionDocument', 'subjectRegulation', FilePreviewType::class, [
                'data_class' => null,
                'required' => false,
                'label' => self::DESCRIPTIONS['projectDescriptionDocument']
            ])
            ->move('budget', 'projectDescriptionDocument')
            ->move('signedAgreement', 'budget')
            ->move('regulation', 'signedAgreement')
            ->move('isBankAccountExist', 'regulation')
            ->move('authorityHead', 'isBankAccountExist')
            ->move('anotherHead', 'authorityHead')
            ->move('organizationCreationResolution', 'anotherHead')
            ->remove('reportForMinistryOfJustice')
            ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
    }

    public function addAdditionalLocation(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->add('legalFederalDistrict', 'legalPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['legalFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('legalLocality', 'legalRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['legalLocality'],
            ])
            ->add('actualFederalDistrict', 'actualPostCode', EntityType::class, [
                'label' => self::DESCRIPTIONS['actualFederalDistrict'],
                'required' => false,
                'class' => FederalDistrict::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
            ])
            ->add('actualLocality', 'actualRegion', null, [
                'required' => false,
                'label' => self::DESCRIPTIONS['actualLocality'],
            ])
        ;
    }
}
