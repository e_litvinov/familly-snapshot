<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;

class EffectivenessAdmin extends EmbeddedAdminDecorator
{
    const INDICATOR_ITEMS = [
        'effectivenessImplementationItems',
        'effectivenessDisseminationItems'
    ];

    const CUSTOM_INDICATOR_ITEMS = [
        'effectivenessImplementationEtcItems',
        'effectivenessDisseminationEtcItems'
    ];

    const DESCRIPTION = [
        'result' => 'Социальные результаты',
        'indicator' => 'Показатели',
        'factValue' => 'Факт 2017 г.',
        'planValue' => 'План 2018 г.',
        'measurementMethod' => 'Методы сбора данных'
     ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getEntityManager();

        $formMapper
            ->add('result', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['result'],
                'attr' => ['maxlength' => 800]
            ])
        ;

        if (in_array($this->getParentFieldDescription()->getFieldName(), self::INDICATOR_ITEMS)) {
            $formMapper
                ->add('indicator', 'sonata_type_admin', array(
                    'label' => self::DESCRIPTION['indicator'],
                ), array(
                    'admin_code' => 'nko_order.admin.indicator'
                ))
            ;
        }

        if (in_array($this->getParentFieldDescription()->getFieldName(), self::CUSTOM_INDICATOR_ITEMS)) {
            $formMapper
                ->add('customIndicator', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => self::DESCRIPTION['indicator']
                ))
            ;
        }

        $formMapper
            ->add('factValue', NumberType::class, [
                'label' => self::DESCRIPTION['factValue'],
                'required' => false,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('planValue', NumberType::class, [
                'label' => self::DESCRIPTION['planValue'],
                'required' => false,
                'attr' => [
                    'min' => 0
                ]
            ])
            ->add('methods', null, [
                'required' => false,
                'multiple' => true,
                'label' => self::DESCRIPTION['measurementMethod'],
                'choices' => $em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_APPLICATION_2018)
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => 'Комментарий',
            ])
        ;
    }
}
