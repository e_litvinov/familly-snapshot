<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application as SecondStageApplication;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application2019\Application as SecondStageApplication2019;
use NKO\OrderBundle\Entity\Application\Continuation\ResultDirection;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ExpectedResultAdmin extends EmbeddedAdminDecorator
{
    const CLASS_TABLE_ACCORDING = [
        SecondStageApplication::class => [
            'qualificationItems' => ResultDirection::QUALIFICATION_CONTINUATION_SECOND_STAGE,
        ],
        SecondStageApplication2019::class => [
            'qualificationItems' => ResultDirection::QUALIFICATION_CONTINUATION_SECOND_STAGE,
        ],
        'default'  => [
            'experienceItems' => ResultDirection::EXPERIENCE_TYPE,
            'processItems' => ResultDirection::PROCESS_TYPE,
            'qualificationItems' => ResultDirection::QUALIFICATION_TYPE,
            'resourceBlockItems' => ResultDirection::RESOURCE_TYPE,
        ],
    ];

    const TEXT_FIELD_DESCRIPTION = [
        'experienceEtcItems',
        'processEtcItems',
        'qualificationEtcItems',
        'resourceBlockEtcItems',
    ];

    const DESCRIPTION = [
        'direction' => 'Направления и ориентировочные задачи',
        'indicator' => 'Показатель',
        'measurementMethod' => 'Метод измерения',
        'comment' => 'Комментарий'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $fieldName = $this->getParentFieldDescription()->getFieldName();

        $parentClassName = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());

        if (key_exists($parentClassName, self::CLASS_TABLE_ACCORDING)) {
            $classTable = array_merge(self::CLASS_TABLE_ACCORDING['default'], self::CLASS_TABLE_ACCORDING[$parentClassName]);
        } else {
            $classTable = self::CLASS_TABLE_ACCORDING['default'];
        }

        if (key_exists($fieldName, $classTable)) {
            $data =  $this->getConfigurationPool()
                ->getContainer()
                ->get('doctrine')
                ->getManager()
                ->createQueryBuilder()
                ->select('o')
                ->from(ResultDirection::class, 'o')
                ->where('o.type like :type')
                ->setParameter('type', '%'. (string)$classTable[$fieldName]. '%')
                ->getQuery()
                ->useResultCache(true)
                ->getResult();

            $formMapper
                ->add('direction', null, [
                    'required' => true,
                    'label' => self::DESCRIPTION['direction'],
                    'choices' => $data
                ])
            ;
        } elseif (in_array($fieldName, self::TEXT_FIELD_DESCRIPTION)) {
            $formMapper
                ->add('customDirection', FullscreenTextareaType::class, [
                    'label' => self::DESCRIPTION['direction']
                ])
            ;
        }

        $formMapper
            ->add('indicator', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['indicator'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
            ->add('measurementMethod', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['measurementMethod'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
            ->add('comment', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['comment'],
                'attr' => [ 'maxlength' => 1500 ]
            ])
        ;
    }
}
