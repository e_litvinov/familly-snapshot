<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/13/17
 * Time: 3:20 PM
 */

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;

class ImmediateResultAdmin extends EmbeddedAdminDecorator
{
    const PRACTICE_RESULTS = 'practiceResults';

    const DESCRIPTION = [
        'service' => 'Наименование услуги, мероприятия и пр.',
        'isPublicEvent' => 'Публичное мероприятие',
        'practiceFormat' => 'Формат распространения практики',
        'targetGroup' => 'Целевая группа',
        'startDateFact' => 'Период реализации',
        'locality' => 'Населенный пункт, где будет проводиться мероприятие',
        'serviceValue' => 'Число услуг/мероприятий',
        'beneficiaryValue' => 'Число благополучателей',
        'isFeedback' => 'Планируется ли сбор обратной связи',
        'measurementMethod' => 'Методы сбора данных'
    ];

    const KEYS_TO_FIND_BY = [
        'expectedResults' => 'application',
        'practiceResults' => 'specialistApplication'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $em = $this->getConfigurationPool()->getContainer()
            ->get('doctrine')->getManager();

        $data = $em->getRepository(Problem::class)->getProblem(
            self::KEYS_TO_FIND_BY[$this->getParentFieldDescription()->getFieldName()],
            $this->getParentFieldDescription()->getAdmin()->getSubject()->getId()
        );

        $formMapper
            ->add('service', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['service'],
                'attr' => ['maxlength' => 255]
            ])
            ->add('isPublicEvent', ChoiceType::class, [
                'label' => self::DESCRIPTION['isPublicEvent'],
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]
            ]);
        $parentFieldName = $this->getParentFieldDescription()->getFieldName();
        if ($parentFieldName == self::PRACTICE_RESULTS) {
            $formMapper
                ->add('practiceFormat', null, [
                    'required' => true,
                    'label' => self::DESCRIPTION['practiceFormat']
                ]);
        }
            $formMapper
            ->add('problem', 'sonata_type_model', [
                'label' => self::DESCRIPTION['targetGroup'],
                'choices' => $data,
                'btn_add' => false,
                'property' => 'targetGroup',
                'required' => false,
            ], [
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.problem'
            ])
            ->add('startDateFact', 'sonata_type_datetime_picker', [
                'label' => self::DESCRIPTION['startDateFact'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false
            ])
            ->add('finishDateFact', 'sonata_type_datetime_picker', [
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false
            ])
            ->add('locality', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['locality'],
                'attr' => ['maxlength' => 255]
            ])
            ->add('serviceFactValue', null, [
                'label' => self::DESCRIPTION['serviceValue'],
                'attr' =>['placeholder' => '0']
            ])
            ->add('beneficiaryFactValue', null, [
                'label' => self::DESCRIPTION['beneficiaryValue'],
                'attr' =>['placeholder' => '0']
            ])
            ->add('servicePlanValue', null, [
                'label' => self::DESCRIPTION['serviceValue'],
                'attr' =>['placeholder' => '0']
            ])
            ->add('beneficiaryPlanValue', null, [
                'label' => self::DESCRIPTION['beneficiaryValue'],
                'attr' =>['placeholder' => '0']
            ])
            ->add('isFeedback', ChoiceType::class, [
                'label' => self::DESCRIPTION['isFeedback'],
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]
            ])
            ->add('methods', null, [
                'required' => false,
                'multiple' => true,
                'label' => self::DESCRIPTION['measurementMethod'],
                'choices' => $em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_APPLICATION_2018)
            ])
            ->add('comment', FullscreenTextareaType::class, ['attr' => ['maxlength' => 450]]);
    }
}
