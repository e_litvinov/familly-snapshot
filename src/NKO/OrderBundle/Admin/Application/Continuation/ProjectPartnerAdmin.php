<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProjectPartnerAdmin extends EmbeddedAdminDecorator
{
    const DESCRIPTION = [
        'organizationInfo' => 'Название организации',
        'briefDescription' => 'Краткие сведения об организации',
        'participation' => 'Участие в проекте',
        'isNew' => 'Новый партнер/донор'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('organizationName', FullscreenTextareaType::class, ['label' => self::DESCRIPTION['organizationInfo']])
            ->add('briefDescription', FullscreenTextareaType::class, ['label' => self::DESCRIPTION['briefDescription']])
            ->add('participation', FullscreenTextareaType::class, ['label' => self::DESCRIPTION['participation']])
            ->add('isNew', ChoiceType::class, [
                'label' => self::DESCRIPTION['isNew'],
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]])
        ;
    }
}
