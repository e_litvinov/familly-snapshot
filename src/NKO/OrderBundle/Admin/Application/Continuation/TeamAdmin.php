<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TeamAdmin extends EmbeddedAdminDecorator
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, ['label' => 'ФИО участника проекта'])
            ->add('role', TextType::class, ['label' => 'Роль в проекте'])
            ->add('responsibility', FullscreenTextareaType::class, ['label' => 'Функциональные обязанности'])
            ->add('relationship', 'sonata_type_model', [
                'label' => 'Трудовые отношения с организацией',
                'required' => true,
                'property' => 'name',
                'multiple' => false,
                'btn_add' => false
            ])
            ->add('description', TextareaType::class, ['label' => 'Краткие сведения об участнике'])
            ->add('isNew', ChoiceType::class, [
                'label' => 'Новый член команды',
                'choices' => ['label_type_no' => 0, 'label_type_yes' => 1]])
        ;
    }
}
