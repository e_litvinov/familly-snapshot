<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Entity\Application\Continuation\Application;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;
use NKO\OrderBundle\Entity\Region;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\DropDownListType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\UserBundle\Traits\CurrentUserTrait;
use PhpOffice\Common\Text;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Valid;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use NKO\OrderBundle\EventListener\TargetGroupListener;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;

class ApplicationAdmin extends AdminDecorator
{
    use CurrentUserTrait;
    /** nameOfField => [
            'class' => nameOfClass
            'joins' => [
                alias => nameOfField,
            ]
        ]
    */
    const MANY_TO_MANY = [
        'effectivenessApplication' => [
            'class' => Effectiveness::class,
            'joins' => [
                'm' => 'methods'
            ],
        ],
        'effectivenessDisseminationApplication' => [
            'class' => Effectiveness::class,
            'joins' => [
                'm' => 'methods'
            ],
        ],
        'expectedResultApplication' => [
            'class' => DirectResult::class,
            'joins' => [
                'm' => 'methods',
                'l' => 'linkedFarvaterResult',
            ],
        ],
        'practiceResultApplication' => [
            'class' => DirectResult::class,
            'joins' => [
                'm' => 'methods',
                'l' => 'linkedFarvaterResult',
            ],
        ],
        'application' => [
            'class' => Problem::class,
            'joins' => [
                's' => 'socialResults',
            ],
        ],
    ];

    const TABS = [
        'contactsTab' => 'КОНТАКТНАЯ ИНФОРМАЦИЯ',
        'practiceRealizationTab' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
        'distributionTab' => 'РАСПРОСТРАНЕНИЕ И ВНЕДРЕНИЕ ПРАКТИКИ',
        'monitoringTab' => 'МОНИТОРИНГ И ОЦЕНКА РЕЗУЛЬТАТОВ',
        'resourcesTab' => 'РЕСУРСНОЕ ОБЕСПЕЧЕНИЕ ПРОЕКТА'
    ];

    const DESCRIPTION = [
        'competitionName' => '1.1 <b>Название конкурса</b>',
        'realizationYear' => '1.2 <b>Год реализации</b>',
        'nkoName'=>'1.3 <b>Получатель пожертвования</b>',
        'projectName'=>'1.4 <b>Название проекта</b>',
        'DateRealizationLabel'=>'1.5 <b>Сроки реализации проекта</b>',
        'startDateRealization'=>'<b>Дата начала</b>',
        'finishDateRealization'=>'<b>Дата окончания</b>',
        'headOfOrganization' => '1.6 <b>Руководитель организации</b>',
        'headOfOrganizationFullName' => '<b>ФИО</b>',
        'headOfOrganizationPosition' => '<b>Должность</b>',
        'headOfAccountingFullName' => '1.7 <b>Главный бухгалтер</b>',
        'sustainabilityPractices' => '2.6 <b>Устойчивость ожидаемых результатов практики</b>',
        'sustainabilitySpreadPractices' => '3.4 <b>Устойчивость ожидаемых результатов распространения Практики: </b><br>
            <i>В чем будет выражаться устойчивость изменений, достигнутых в процессе распространения практики? 
            Какие меры будут приняты для ее достижения? Пожалуйста, для каждой идеи используйте новую строку.</i>',
        'sustainabilityResultMonitorings' => '4.4 <b>Устойчивость ожидаемых результатов в сфере мониторинга и оценки:</b><br>
            <i>В чем будет выражаться устойчивость изменений, достигнутых в сфере мониторинга и оценки? 
            Какие меры будут приняты для ее достижения? Пожалуйста, для каждой идеи используйте новую строку.</i>',
        'realizationPractices' => '2.7 <b>Обстоятельства, которые могут воспрепятствовать успешной реализации Практики, 
            и действия, которые предприняты (или будут предприняты) для снижения рисков.</b>',
        'spreadPractices' => '3.5 <b>Обстоятельства, которые могут воспрепятствовать успешному распространению Практики,
            и действия, которые предприняты (или будут предприняты) для снижения рисков.</b>',
        'risePotentials' => '4.5 <b>Обстоятельства, которые могут воспрепятствовать успешному повышению организационного потенциала 
            в отношении мониторинга и оценки, и действия, которые предприняты (или будут предприняты) для снижения рисков.</b>',
        'briefPracticeDescription' => '2.1 <b>Описание сути Практики</b><br><i>Данные можно взять из предыдущей заявки, 
            уточнив их с учетом текущей ситуации.</i>',
        'practiceChanges' => '2.1.1 <b>Какие изменения планируется внести в Практику?</b><br><i>Поле заполняется, 
            если планируете внести такие изменения, иначе ставится прочерк. </i>',
        'beneficiaryGroupsProblems' => '2.2. Проблемы <b>целевых групп, на решение которых направлена Практика; социальные
            результаты применения Практики для целевых групп</b><br><br>
            <i>Данные можно взять из предыдущей заявки, уточнив их с учетом текущей ситуации.</i><br>',
        'projectPurpose' => '2.4 <b>Цель проекта (в отношении реализации Практики):</b>',
        'otherBeneficiaryGroups' => '<b>Иные группы</b> <i>(укажите)</i>',
        'effectivenessEtc' => '<b>Иные показатели</b><br><i>В этой таблице Вы можете добавить свои показатели.</i>',
        'potential' => '<b>4.1. Организационный потенциал вашей организации в области планирования, мониторинга, измерения и оценки социальных программ.</b><br>
            <i>Сюда можно перенести данные из прошлой заявки, но их обязательно нужно пересмотреть с учетом опыта работы в 2017 г.</i>',
        'monitoringProjectPurpose' => '4.2. <b>Цель проекта (в отношении блока мониторинга и оценки): </b>',
        'experienceEtc' => '<b>Иные задачи по блоку “Опыт”</b>',
        'process' => '<b>4.3.2. Процессы</b>',
        'processEtc' => '<b>Иные задачи по блоку “Процессы”</b>',
        'qualification' => '<b>4.3.3. Повышение уровня знаний и навыков сотрудников</b>',
        'qualificationEtc' => '<b>Иные задачи по блоку “Повышение уровня знаний и навыков сотрудников”</b>',
        'resourceBlock' => '<b>4.3.4. Ресурсы</b>',
        'resourceBlockEtc' => '<b>Иные задачи по блоку “Ресурсы”</b>',
        'resourceItems' => '5.3 <b>Имеющиеся у Организации ресурсы, необходимые для реализации Проекта (на период реализации проекта)</b>',
        'resourceEtcItems' => '<b>Иные ресурсы:</b>',
        'team' => '5.1 <b>Команда проекта</b>',
        'projectPartners' => '5.2. <b>Партнеры и доноры Проекта</b>',
        'otherSpecialistTargetGroup' => '<b>Иные группы</b> <i>(укажите)</i>',
        'introductionProjectPurpose' => '3.2 <b>Цель проекта (распространение и внедрение практики):</b>',
        'contract' => '<b>Договор пожертвования N:</b>',
        'priorityDirection' => '<b>Приоритетное направление:</b>',
        'priorityDirectionEtc' => '<b>Иные эффективные практики в сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей (укажите, какие именно):</b>',
        'sumGrant' => '<b>Сумма пожертвования, руб:</b>'
    ];

    const HELPS = [
        'peopleCategories' => '<b>2.2.1. Целевые группы (благополучатели) Практики</b><br><br>
            <i>Конкурс «Семейный фарватер» и Программа «Семья и дети» реализуются, чтобы в жизни конкретных групп людей 
            произошли позитивные изменения. Основные группы благополучателей представлены ниже. 
            Укажите, на какие именно группы благополучателей преимущественно направлена ваша Практика. 
            Вы можете также дополнительно добавить те группы благополучателей, которые отсутствуют в данном списке, <b>но не более 3.</b></i>',
        'problems' => '<b>2.2.2. Основные проблемы благополучателей и социальные результаты применения практики</b><br><br>
            <i><b>Укажите основные проблемы тех целевых групп (благополучателей), на решение которых направлена ваша Практика.</b>
            Что изменяется в жизни благополучателей (целевых групп) благодаря применению вашей Практики? Например, изменение установок, 
            мотивов, личностных ресурсов, психологического состояния, позиции представителей значимого окружения, 
            развитие новых навыков преодоления проблемной ситуации, адаптации, появление поддерживающих ресурсов, и т.п.<br><br>
            <b>Программа «Семья и дети» Фонда Тимченко направлена на достижение следующих социальных результатов:</b>
            <ul>
            <li>Увеличение числа детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства (в том числе подростков, детей с ОВЗ, сиблингов);</li>
            <li>Увеличение числа детей, возвращенных в кровные семьи (в том числе подростков и детей с ОВЗ);</li>
            <li>Уменьшение количества изъятий/отказов детей из кровных семей;</li>
            <li>Уменьшение количества изъятий/отказов детей из замещающих семей;</li>
            <li>Улучшение благополучия детей и семей – участников Программы;</li>
            <li>Рост уровня готовности детей к самостоятельной жизни – они становятся полноценными гражданами, обеспечивающими благополучие общества.</li>
            </ul>
            Вы можете выбрать один или несколько вариантов социальных результатов из списка и указать под ним свои - через точку с запятой.</i>',
        'practiceRegions' => '2.3. <b>Территория реализации Практики</b><br>
            <i>В каком регионе (регионах) РФ ваша организация сейчас реализует представленную на Конкурс Практику?</i>',
        'immediateResults' => '2.5 <b>Ожидаемые результаты проекта</b><br><br>
            2.5.1 <b>Непосредственные результаты</b><br><br>
            <i>Пожалуйста, заполняйте таблицу следующим образом:</i><br><br>
            <ul>
            <li><i>В графе «Публичное мероприятие» указывайте «Да» для мероприятий, на которые можно пригласить внешних участников (например, праздник для приемных семей).</i></li>
            <li><i>Добавляйте в столбец «Целевая группа» целевые группы, указанные вами в п.2.2.1</i></li>
            <li><i>«Период реализации» - указывайте начало и конец периода проведения мероприятий. Например, если это еженедельные лекции, 
            которые проводятся в течение всего периода реализации проекта, указываем даты начала и конца реализации проекта и пр.</i></li>
            <li><i>«Населенный пункт, где будет проводиться мероприятие» – заполните этот столбец, пожалуйста, 
            строго в соответствии с инструкцией, чтобы мероприятие могло быть выведено на Карте мероприятий</i></li>
            <li><i>В «Целевые значения»: «Факт 2017 г.» перенесите данные из годового содержательного отчета.</i></li>
            <li><i>В графе «Планируется ли сбор обратной связи» указывайте «Да», 
            если будете в какой-то момент реализации практики собирать обратную связь от участников по данной услуге / мероприятию.</i></li>
            <li><i>Графа «Метод измерения» заполняется для строк с мероприятиями, по которым вы будете собирать обратную связь. 
            Скопируйте в нее подходящие варианты из списка ниже или поставьте свой.</i></li>
            </ul>
            <i><b style="color: #952a25">ПЕРЕД ЗАПОЛНЕНИЕМ ДАННОЙ ТАБЛИЦЫ ОБЯЗАТЕЛЬНО СОХРАНИТЕ ЗАЯВКУ!</b></i><br><br>',
        'immediatePracticeResults' => '3.3 <b>Результаты распространения практики</b><br><br>
            3.3.1 <b>Непосредственные результаты</b><br><br>
            <i>Пожалуйста, заполняйте таблицу следующим образом:</i><br><br>
            <ul>
            <li><i>В графу «Формат распространения практики» скопируйте подходящие варианты из списка.</i></li>
            <li><i>В графе «Публичное мероприятие» указывайте «Да» для мероприятий, на которые можно пригласить внешних участников.</i></li>
            <li><i>Добавляйте в столбец «Целевая группа» целевые группы, указанные вами в п.3.1.1</i></li>
            <li><i>«Период реализации» - указывайте начало и конец периода проведения мероприятий.</i></li>
            <li><i>«Населенный пункт, где будет проводиться мероприятие» – заполните этот столбец, пожалуйста, 
            строго в соответствии с инструкцией, чтобы мероприятие могло быть выведено на Карте мероприятий.</i></li>
            <li><i>В «Целевые значения»: «Факт 2017 г.» перенесите данные из годового содержательного отчета.</i></li>
            <li><i>В графе «Планируется ли сбор обратной связи» указывайте «Да», 
            если будете в какой-то момент реализации практики собирать обратную связь от участников по данной услуге / мероприятию.</i></li>
            <li><i>Графа «Метод измерения» заполняется для строк с мероприятиями, по которым вы будете собирать обратную связь. 
            Скопируйте в нее подходящие варианты из списка ниже или поставьте свой.</i></li>
            </ul>
            <i><b style="color: #952a25">ПЕРЕД ЗАПОЛНЕНИЕМ ДАННОЙ ТАБЛИЦЫ ОБЯЗАТЕЛЬНО СОХРАНИТЕ ЗАЯВКУ!</b></i><br><br>',
        'effectivenessImplementationItems' => '2.5.2. <b>Социальные результаты реализации Практики</b><br>
            <i>Вы начинаете заполнять таблицу, проставляя значения для приведенных в ней показателей, важных для Фонда Тимченко. 
            Затем к ненулевым показателям слева прописываете через точку с запятой один или несколько социальных результатов, 
            указанных Вами в п.2.2.2. После этого в графу «Метод сбора данных» скопируйте подходящие варианты из списка или добавьте свой.<br><br>
            Все социальные результаты, указанные в п.2.2.2, должны быть в п.2.5.2. отражены.<br><br>
            В «Значения показателя»: «Факт 2017 г.» должны быть перенесены все ненулевые значения из формы мониторингового отчета за 2017 год.</i>',
        'effectivenessDisseminationItems' => '3.3.2. <b>Социальные результаты распространения практики</b><br>
            <i>Вы начинаете заполнять таблицу, проставляя значения для приведенных в ней показателей, важных для Фонда Тимченко. 
            Затем к ненулевым показателям слева прописываете через точку с запятой один или несколько социальных результатов, 
            указанных Вами в п.3.1.2. После этого в графу «Метод сбора данных» скопируйте подходящие варианты из списка или добавьте свой.<br><br>
            Все социальные результаты, указанные в п.3.1.2, должны быть в п.3.3.2. отражены.<br><br>
            В «Значения показателя»: «Факт 2017 г.» должны быть перенесены все ненулевые значения из формы мониторингового отчета за 2017 год.</i>',
        'experience' => '<b>4.3 Ожидаемые результаты проекта по блоку «Мониторинг и оценка результатов, повышение организационного потенциала».</b><br>
            <i>Пожалуйста, выберите варианты из списков по каждому блоку (см. ниже) и/или добавьте свои варианты 
            Одному результату соответствует одна строка.</i><br><br>
            <b>4.3.1. Опыт</b>',
        'targetGroup' => '3.1. <b>Проблемы целевых групп специалистов, на решение которых направлено распространение и внедрение Практики; 
            социальные результаты распространения Практики<br><br>
            3.1.1. Целевые группы специалистов</b>',
        'specialistProblems' => '<b>3.1.2. Основные проблемы специалистов и социальные результаты распространения Практики</b><br>
            <i>В графе «Отметить, есть ли в данной целевой группе специалисты организаций - участников конкурсов «Семейный Фарватер» 
            и «Курс на семью» нужно указать аббревиатуры СФ, КнС, если вы заранее планируете, что в вашей целевой аудитории будут данные специалисты.<br><br>
            <b>Ключевые ожидаемые социальные результаты:</b><br>
            <ul>
            <li><i>Повышены доступность и качество профессиональной помощи семьям и детям в области профилактики социального сиротства и 
            семейного устройства детей-сирот и детей, оставшихся без попечения родителей;</i></li>
            <li><i>Повышен уровень компетенций специалистов сферы защиты детства по темам, приоритетным для достижения долгосрочных социальных результатов</i></li>
            <li><i>Сформированы отраслевые ресурсные центры – стажировочные площадки в области профилактики социального сиротства и семейного устройства, 
            эффективно тиражирующие свои практики</i></li>
            <li><i>Эффективная практика, тиражируемая участниками проекта, внедряется в других организациях</i></li>
            <li><i>Свой вариант</i></li>
            </ul></i>',
        'anotherPracticeRegions' => '<i>Чтобы добавить дополнительный регион, кликните на пустое поле в строке справа от предыдущего региона</i>'
    ];

    const LISTENER_TABLES = [
        'expectedResults' => 'beneficiaryProblems',
        'practiceResults' =>'specialistProblems'
    ];

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => array("ContinuationApplication-2018")
        );
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var EntityManager $em */
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $subject = $this->getSubject() ?: $this->getRelatedObject();
        $this->takeManyToManys($em, $subject);

        $formMapper
            ->with(self::TABS['contactsTab'])
            ->add('competition.name', TextType::class, array(
                'label' => self::DESCRIPTION['competitionName'],
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('competition.realizationYear', TextType::class, array(
                'label' => self::DESCRIPTION ['realizationYear'],
                'data' => $subject ? $subject->getCompetition()->getRealizationYear()->format('Y') : null,
                'mapped' => false,
                'attr' => array(
                    'readonly' => true
                )
            ))
            ->add('author.nko_name', TextType::class, array(
                'label' => self::DESCRIPTION['nkoName'],
                'required' => false,
                'attr' => array(
                    'readonly' => true)
            ))
            ->add('contract', TextType::class, [
                'required' => false,
                'data' => $subject ? $subject->getLinkedApplicationHistory()->getContract() : null,
                'mapped' => false,
                'label' => self::DESCRIPTION['contract'],
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('projectName', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['projectName'],
                'attr' => array('maxlength' => 350),
            ))
            ->add('priorityDirection', 'sonata_type_model', array(
                'choices' => $em->getRepository(PriorityDirection::class)
                    ->findBy([
                        'applicationType' => ['application', 'brief_application']
                    ]),
                'label' => self::DESCRIPTION['priorityDirection'],
                'property' => 'name',
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false
            ))
            ->add('priorityDirectionEtc', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['priorityDirectionEtc'],
                'attr' => array('maxlength' => 255),
            ))
            ->add('dateRealizationLabel', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['DateRealizationLabel'],
                ))
            ->add('deadLineStart', 'sonata_type_datetime_picker', array(
                'label' => 'с',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,
                'dp_use_current' => false,
            ))
            ->add('deadLineFinish', 'sonata_type_datetime_picker', array(
                'label' => 'по',
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,
                'dp_use_current' => false,
            ))
            ->add('sumGrant', TextType::class, [
                'label' => self::DESCRIPTION['sumGrant'],
                'required' => false
            ])
            ->add('headOfOrganization', CustomTextType::class, array(
                    'help' => self::DESCRIPTION['headOfOrganization'],
            ))
            ->add('headOfOrganizationFullName', TextType::class, array(
                'label' => self::DESCRIPTION['headOfOrganizationFullName'],
                'required' => false,
                'attr' => array('maxlength' => 255),
            ))
            ->add('headOfOrganizationPosition', TextType::class, array(
                'label' => self::DESCRIPTION['headOfOrganizationPosition'],
                'required' => false,
                'attr' => array('maxlength' => 255),
            ))
            ->add('headOfAccountingFullName', TextType::class, array(
                'label' => self::DESCRIPTION['headOfAccountingFullName'],
                'required' => false,
                'attr' => array('maxlength' => 255)
            ))
            ->end()
            ->with(self::TABS['practiceRealizationTab'])
            ->add('briefPracticeDescription', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION['briefPracticeDescription'],
                'required' => false,
                'attr' => array('maxlength' => 1800)
            ))
            ->add('practiceChanges', FullscreenTextareaType::class, array(
                'label' => self::DESCRIPTION['practiceChanges'],
                'required' => false,
                'attr' => array('maxlength' => 1500)
            ))
            ->add('beneficiaryGroupsProblems', CustomTextType::class, array(
                'help' => self::DESCRIPTION['beneficiaryGroupsProblems'],
            ))
            ->add('peopleCategoriesLabel', CustomTextType::class, array(
                'help' => self::HELPS['peopleCategories'],
            ))
            ->add('peopleCategories', DropDownListType::class, array(
                'class' => PeopleCategory::class,
                'required' => false,
                'label' => false,
                'multiple' => true
            ))
            ->add('otherBeneficiaryGroups', 'sonata_type_collection', array(
                'required' => false,
                'label' => self::DESCRIPTION['otherBeneficiaryGroups'],
                'btn_add' => "Добавить",
                'by_reference' => false
            ), array(
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('beneficiaryProblemsLabel', CustomTextType::class, array(
                'help' => self::HELPS['problems'],
            ))
            ->add('beneficiaryProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.problem',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('practiceRegionsLabel', CustomTextType::class, array(
                'help' => self::HELPS['practiceRegions'],
            ))
            ->add('practiceRegions', 'sonata_type_model', array(
                'required' => false,
                'property' => 'name',
                'label' => false,
                'help' => self::HELPS['anotherPracticeRegions'],
                'multiple' => true,
                'choices' => $em->getRepository(Region::class)->findBy([], [
                    'name' => 'ASC'
                ])
            ))
            ->add('projectPurpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => self::DESCRIPTION['projectPurpose'],
                'attr' => array('maxlength' => 800)
            ))
            ->add('immediateResults', CustomTextType::class, array(
                'help' => self::HELPS['immediateResults'],
            ))
            ->add('expectedResults', 'sonata_type_collection', [
                'label' => false,
                'required' => false,
                'btn_add' => 'Добавить',
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.immediate_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ])
            ->add('effectivenessImplementationItemsLabel', CustomTextType::class, array(
                'help' => self::HELPS['effectivenessImplementationItems'],
            ))
            ->add('effectivenessImplementationItems', 'sonata_type_collection', array(
                'label' => false,
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
                'type_options' => array(
                    'delete' => false
                )
            ), array(
                'admin_code' => 'nko_order.admin.continuation.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('effectivenessImplementationEtcItems', 'sonata_type_collection', array(
                'label' => self::DESCRIPTION['effectivenessEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.continuation.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('sustainabilityPracticeApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['sustainabilityPractices'],
                    'btn_add' => true,
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('realizationPracticeApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['realizationPractices'],
                    'btn_add' => true,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.double_context_application',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with(self::TABS['distributionTab'])
            ->add('targetGroupLabel', CustomTextType::class, array(
                'help' => self::HELPS['targetGroup'],
            ))
            ->add('specialistTargetGroups', 'sonata_type_model', [
                'label' => false,
                'required' => false,
                'expanded' => true,
                'multiple' => true,
                'btn_add' => false,
            ])
            ->add('otherSpecialistTargetGroups', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['otherSpecialistTargetGroup'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('specialistProblemsLabel', CustomTextType::class, [
                'help' => self::HELPS['specialistProblems']
            ])
            ->add('specialistProblems', 'sonata_type_collection', array(
                'required' => false,
                'label' => false,
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.farvater.brief_application_2018.problem',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('introductionProjectPurpose', FullscreenTextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['introductionProjectPurpose'],
                'attr' => ['maxlength' => 800]
            ])
            ->add('immediatePracticeResults', CustomTextType::class, array(
                'help' => self::HELPS['immediatePracticeResults'],
            ))
            ->add('practiceResults', 'sonata_type_collection', [
                'label' => false,
                'required' => false,
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.continuation.immediate_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('effectivenessDisseminationItemsLabel', CustomTextType::class, array(
                'help' => self::HELPS['effectivenessDisseminationItems'],
            ))
            ->add('effectivenessDisseminationItems', 'sonata_type_collection', array(
                'label'=> false,
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
                'type_options' => array(
                    'delete' => false
                )
            ), array(
                'admin_code' => 'nko_order.admin.continuation.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('effectivenessDisseminationEtcItems', 'sonata_type_collection', array(
                'label' => self::DESCRIPTION['effectivenessEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ), array(
                'admin_code' => 'nko_order.admin.continuation.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
            ))
            ->add('sustainabilitySpreadApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['sustainabilitySpreadPractices'],
                    'btn_add' => true,
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('spreadPracticeApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['spreadPractices'],
                    'btn_add' => true,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.double_context_application',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with(self::TABS['monitoringTab'])
            ->add('potential', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['potential'],
                'required' => false
            ])
            ->add('monitoringProjectPurpose', FullscreenTextareaType::class, [
                'label' => self::DESCRIPTION['monitoringProjectPurpose'],
                'required' => false
            ])
            ->add('experienceItemsLabel', CustomTextType::class, array(
                'help' => self::HELPS['experience'],
            ))
            ->add('experienceItems', 'sonata_type_collection', [
                'label' => false,
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('experienceEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['experienceEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('processItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['process'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('processEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['processEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('qualificationItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['qualification'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('qualificationEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['qualificationEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceBlockItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['resourceBlock'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceBlockEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['resourceBlockEtc'],
                'required' => false,
                'btn_add' => true,
                'by_reference' => false
            ], [
                'admin_code' => 'nko_order.admin.continuation.expected_result',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('sustainabilityMonitoringApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['sustainabilityResultMonitorings'],
                    'btn_add' => true,
                    'by_reference' => false
                ), array(
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->add('risePotentialApplications', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => self::DESCRIPTION['risePotentials'],
                    'btn_add' => true,
                    'by_reference' => false
                ), array(
                    'admin_code' => 'nko_order.admin.double_context_application',
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with(self::TABS['resourcesTab'])
            ->add('projectTeams', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['team'],
                'required' => false,
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table'
            ])
            ->add('projectContPartners', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['projectPartners'],
                'required' => false,
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['resourceItems'],
                'required' => false,
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('resourceEtcItems', 'sonata_type_collection', [
                'label' => self::DESCRIPTION['resourceEtcItems'],
                'required' => false,
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])

            ->end()
            ;

        $formMapper->getFormBuilder()->addEventSubscriber(new TargetGroupListener(self::LISTENER_TABLES));
    }

    public function preUpdate($continuationApplication)
    {
        $reports = $this->getReports($continuationApplication);

        /** @var Report $report */
        foreach ($reports as $report) {
            $this->getConfigurationPool()->getContainer()->get('NKO\OrderBundle\Loader\Loader')
                ->update($report, $continuationApplication);
        }
    }

    private function getReports($continuationApplication)
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')->getManager()
            ->getRepository(Report::class)
            ->findReportsByApplication($continuationApplication);
    }

    public function takeManyToManys(EntityManager $em, $object)
    {
        foreach (self::MANY_TO_MANY as $nameOfField => $params) {
            $this->makeCachedSelect($em, $object, $nameOfField, $params['joins'], $params['class']);
        }
    }

    private function makeCachedSelect(EntityManager $em, $object, $nameOfField, $joins, $class)
    {
        $ids = $em->createQueryBuilder()->select('o.id')
            ->from($class, 'o')
            ->where('o.'. (string)$nameOfField. ' = :application')
            ->setParameter('application', $object)
            ->getQuery()
            ->useResultCache(true)
            ->getResult();

        $qb = $em->createQueryBuilder()
            ->from($class, 'o')
            ->where('o.id in(:ids)')
            ->setParameter('ids', $ids);

        $select = 'o';

        foreach ($joins as $alias => $field) {
            $qb->leftJoin('o.'.(string)$field, $alias);
            $select .= ', '.$alias;
        }
        $qb->select($select)->getQuery()->useResultCache(true)->getResult();
    }
}
