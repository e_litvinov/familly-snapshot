<?php

namespace NKO\OrderBundle\Admin\Application\Continuation\SecondStage;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Form\WrappedFormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Entity\Application\Continuation\TargetGroup;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\EventListener\TargetGroupListener;
use Sonata\AdminBundle\Form\FormMapper;

class ApplicationAdmin extends AdminDecorator
{
    const NOT_CHOSEN = 'Не выбрано';

    const LISTENER_TABLES = [
        'expectedResults' => 'beneficiaryProblems',
        'practiceResults' =>'specialistProblems'
    ];

    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);

        $this->formOptions = [
            'validation_groups' => ["ContinuationApplication-2"]
        ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        parent::configureFormFields($formMapper);
        $formMapper->getFormBuilder()->addEventSubscriber(new TargetGroupListener(self::LISTENER_TABLES));
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $subject = $this->getSubject();
        $grant = $this->em->getRepository(GrantConfig::class)->findGrantByAuthorAndReportForm($subject->getCompetition()->getRelatedReportForm(), $subject->getAuthor());

        $wrappedFormMapper
            ->change('contract', TextType::class, [
                'required' => false,
                'data' => $grant ? $grant->getContract() : null,
                'mapped' => false,
                'label' => $this->description['contract'],
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->change('priorityDirection', 'sonata_type_model', array(
                'placeholder' => self::NOT_CHOSEN
            ))
            ->remove('practiceRegions')
            ->remove('practiceRegionsLabel')
            ->add('territories', 'beneficiaryProblems', 'sonata_type_collection', [
                'label' => $this->description['territories'],
                'required' => false,
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'nko_order.admin.analytic_report_2018.territory',
            ])
            ->add('expectedResultsComment', 'expectedResults', TextareaType::class, [
                    'label' => $this->description['comments'],
                    'required' => false,
            ])
            ->add('effectivenessImplementationItemsComment', 'effectivenessImplementationItems', TextareaType::class, [
                    'label' => $this->description['comments'],
                    'required' => false,
            ])
            ->add('practiceResultsComment', 'practiceResults', TextareaType::class, [
                    'label' => $this->description['comments'],
                    'required' => false,
            ])
            ->add('effectivenessDisseminationItemsComment', 'effectivenessDisseminationItems', TextareaType::class, [
                    'label' => $this->description['comments'],
                    'required' => false,
            ])
            ->change('sumGrant', TextType::class, [
                'label' => $this->description['sumGrant'],
                'required' => false
            ])
            ->add('phoneLabel', 'headOfAccountingFullName', CustomTextType::class, [
                'help' => $this->description['phoneLabel']
            ])
            ->add('phoneLabelHelp', 'phoneLabel', CustomTextType::class, [
                'help' => $this->description['phoneLabelHelp'],
            ])
            ->add('internationalCode', 'phoneLabelHelp', CustomTextType::class, [
                'help' => $this->description['internationalCode'],
            ])
            ->add('phoneCode', 'internationalCode', TextType::class, [
                'required' => false,
                'label' => false,
                'mapped' => false,
                'disabled' => true,
                'data' => $subject ? $subject->getAuthor()->getPhoneCode() : null
            ])
            ->add('phone', 'phoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'mapped' => false,
                'disabled' => true,
                'data' => $subject ? $subject->getAuthor()->getPhone() : null
            ])
            ->add('email', 'phone', TextType::class, [
                'required' => false,
                'label' => $this->description['email'],
                'mapped' => false,
                'disabled' => true,
                'data' => $subject ? $subject->getAuthor()->getEmail() : null
            ])

            //tab2
            ->remove('effectivenessImplementationItems')
            ->remove('effectivenessImplementationItemsLabel')
            ->move('effectivenessImplementationItemsComment', 'effectivenessImplementationEtcItems')

            //tab3
            ->change('specialistTargetGroups', 'sonata_type_model', [
                'query' => $this->em->getRepository(TargetGroup::class)
                    ->createQueryBuilder('t')
                    ->where('t.type like :type')
                    ->setParameter('type', '%' . ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 . '%')
            ])
            ->remove('effectivenessDisseminationItems')
            ->remove('effectivenessDisseminationItemsLabel')
            ->move('effectivenessDisseminationItemsComment', 'effectivenessDisseminationEtcItems')

            //tab4
            ->remove('potential')
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('effectivenessDisseminationEtcItems')
                ->change('factValue', TextType::class, ['attr' => ['placeholder' => 0]])
                ->change('methods', null, ['choices' => $this->em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018)])

            ->with('effectivenessImplementationEtcItems')
                ->change('factValue', TextType::class, ['attr' => ['placeholder' => 0]])
                ->change('planValue', TextType::class, ['attr' => ['placeholder' => 0]])

            ->with('expectedResults')
                ->remove(['isPublicEvent', 'beneficiaryFactValue', 'beneficiaryPlanValue', 'isFeedback'])
                ->move('startDateFact', 'service')
                ->move('finishDateFact', 'startDateFact')
                ->change('serviceFactValue', TextType::class)
                ->change('servicePlanValue', TextType::class)
                ->change('problem', 'sonata_type_model', ['placeholder' => self::NOT_CHOSEN])
                ->change('methods', null, ['choices' => $this->em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018)])

            ->with('practiceResults')
                ->remove(['isPublicEvent', 'beneficiaryFactValue', 'beneficiaryPlanValue', 'isFeedback'])
                ->move('startDateFact', 'practiceFormat')
                ->move('finishDateFact', 'startDateFact')
                ->change('serviceFactValue', TextType::class)
                ->change('servicePlanValue', TextType::class)
                ->change('problem', 'sonata_type_model', ['placeholder' => self::NOT_CHOSEN])
                ->change('practiceFormat', null, ['placeholder' => self::NOT_CHOSEN])
                ->change('methods', null, ['choices' => $this->em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018)])

            ->with('projectContPartners')
                ->change('isNew', ChoiceType::class, ['placeholder' => self::NOT_CHOSEN])

            ->with('projectTeams')
                ->change('isNew', ChoiceType::class, ['placeholder' => self::NOT_CHOSEN])
                ->change('relationship', 'sonata_type_model', ['placeholder' => self::NOT_CHOSEN])
                ->change('role', FullscreenTextareaType::class)
                ->change('name', FullscreenTextareaType::class)
                ->change('description', FullscreenTextareaType::class)

            ->with('beneficiaryProblems')
                ->change('targetGroup', null, ['attr' => ['readonly' => 'readonly']])
                ->change('socialResults', null, [
                    'required' => false,
                    'by_reference' => false,
                    'choices' => $this->em->getRepository(SocialResult::class)->findCachedByApplicationTypeQuery(ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018),
                    'attr' => array('style' => 'margin-bottom: 10px')
                ])
            ->with('specialistProblems')
                ->change('targetGroup', null, ['attr' => ['readonly' => 'readonly']])
            ->with('effectivenessImplementationEtcItems')
                ->change('methods', null, ['choices' => $this->em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018)])
            ->with('effectivenessDisseminationEtcItems')
                ->change('methods', null, ['choices' => $this->em->getRepository(MeasurementMethod::class)->findMethodsByType(ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018)])
        ;
    }

    public function preUpdate($application)
    {
        $this->getRelatedAdmin()->preUpdate($application);
        parent::preUpdate($application);
    }
}
