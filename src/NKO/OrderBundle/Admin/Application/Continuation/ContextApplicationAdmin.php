<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContextApplicationAdmin extends AbstractAdmin
{
    const LABEL_NAMES = [
        'sustainabilityPracticeApplications' => [],
        'sustainabilitySpreadApplications' => ['maxlength' <= 1500],
        'sustainabilityMonitoringApplications' => ['maxlength' <= 1500],
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $attr = key_exists($this->getParentFieldDescription()->getFieldName(), self::LABEL_NAMES) ?
            self::LABEL_NAMES[$this->getParentFieldDescription()->getFieldName()] : false;

        $formMapper
           ->add('context', FullscreenTextareaType::class, [
                'label' => 'Фактор устойчивости – соответствующая мера для ее достижения', 'attr' => $attr
            ])
        ;
    }
}
