<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

class DoubleContextApplicationAdmin extends AbstractAdmin
{
    const LABEL_NAMES = [
        'title' => [
            'label' => 'Ключевые риски',
        ],
        'context' => [
            'label' => 'Действия, нацеленные на снижение риска',
        ]
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', FullscreenTextareaType::class, [
                'label' => self::LABEL_NAMES['title']['label']])
            ->add('context', FullscreenTextareaType::class, [
                'label' => self::LABEL_NAMES['context']['label']]);
    }
}
