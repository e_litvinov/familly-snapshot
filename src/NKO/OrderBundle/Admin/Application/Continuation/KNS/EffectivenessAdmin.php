<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.12.18
 * Time: 12.36
 */

namespace NKO\OrderBundle\Admin\Application\Continuation\KNS;

use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;

class EffectivenessAdmin extends EmbeddedAdminDecorator
{
    const NONE = 'Не выбрано';

    const DESCRIPTION = [
        'indicator' => 'Показатель',
        'factValue' => 'Целевое значение на<br> конец I года реализации<br>проекта (ФАКТ)',
        'planValue' => 'Целевое значение на<br> конец реализации <br>проекта (ПЛАН)',
        'measurementMethod' => 'Способ измерения',
        'comment' => 'Свои способы измерения'
    ];

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->subject) {
            $indicator = $this->subject->getIndicator();
            $indicatorText = $indicator->getIndexValue(). '. '. $indicator->getTitle();
        } else {
            $indicatorText = '';
        }

        $formMapper
            ->add('indicator.title', TextareaType::class, [
                'label' => self::DESCRIPTION['indicator'],
                'attr' => [
                    'readonly' => 'readonly'
                ],
                'data' => $indicatorText,
                'mapped' => false,
                'required' => false,
            ])
            ->add('factValue', NumberType::class, [
                'label' => self::DESCRIPTION['factValue'],
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'placeholder' => 0
                ]
            ])
            ->add('planValue', NumberType::class, [
                'label' => self::DESCRIPTION['planValue'],
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'placeholder' => 0
                ]
            ])
            ->add('methods', null, [
                'label' => self::DESCRIPTION['measurementMethod'],
                'required' => false,
                'by_reference' => false,
                'multiple' => true,
                'attr' => [
                    'placeholder' => self::NONE
                ],
                'choices' => $this->getConfigurationPool()->getContainer()->get('doctrine')
                    ->getRepository(MeasurementMethod::class)
                    ->findMethodsByTypeData(ApplicationTypes::CONTINUATION_KNS)
            ])
            ->add('comment', TextareaType::class, [
                'required' => false,
                'label' => self::DESCRIPTION['comment'],
            ])
        ;
    }
}
