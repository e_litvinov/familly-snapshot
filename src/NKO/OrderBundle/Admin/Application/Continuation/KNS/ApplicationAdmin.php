<?php

namespace NKO\OrderBundle\Admin\Application\Continuation\KNS;

use NKO\OrderBundle\Admin\Decorator\AdminDecorator;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Form\WrappedFormMapper;
use NKO\OrderBundle\Admin\Decorator\FormHandler;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\AdminConfigurator\Application\Continuation\KNS\ProjectCartAdmin;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationAdmin extends AdminDecorator
{
    public function __construct($code, $class, $baseControllerName, $relatedAdmin, $em)
    {
        parent::__construct($code, $class, $baseControllerName, $relatedAdmin, $em);

        $this->formOptions = [
            'validation_groups' => [
                "ContinuationApplicationKNS",
                "AdditionBudget",
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        ];
    }

    public function configureEmbeddedFormFields(WrappedFormMapper $wrappedFormMapper)
    {
        $wrappedFormMapper
            ->addTab("projectCardTab", $this->tabDescription['projectCard'], 'first');
        $this->useConfigurator('projectCardTab', ProjectCartAdmin::class, $this->getFirstTabData());

        $wrappedFormMapper
            ->change('competition', null, [
                'label' => $this->description['competition']
            ])
            ->remove('name')
            ->remove('pSRN')
            ->change('effectivenessLabel', CustomTextType::class, [
                'help' => $this->description['effectivenessLabel']
            ])
            ->change('individualResultLabel', CustomTextType::class, [
                'help' => $this->description['individualResultLabel']
            ])
            ->add('sumGrant', 'organizationResources', TextType::class, [
                'required' => false,
                'label' => $this->description['sumGrant'],
                'data' => number_format($this->getSubject()->getsumGrant(), 2, '.', ''),
            ])
            ->change('deadLineLabel', CustomTextType::class, [
                'help' => $this->description['deadLineLabel']
            ])
            ->change('deadLineStart', DateTimePickerType::class, [
                'dp_min_date' => '15/02/2019',
                'dp_max_date' => '30/11/2019',
            ])
            ->change('deadLineFinish', DateTimePickerType::class, [
                'dp_min_date' => '15/02/2019',
                'dp_max_date' => '30/11/2019',
            ])
            ->change('projectRelevance', FullscreenTextareaType::class)
            ->add('effectivenessKNSItems', 'effectivenessLabel', 'sonata_type_collection', [
                    'label' => false
                ], [
                'admin_code' => 'nko_order.admin.continuation.kns.effectiveness',
                'edit' => 'inline',
                'inline' => 'table'
                ])
            ->remove('effectivenessItems')
            ->change('individualSocialResultsComment', FullscreenTextareaType::class)
            ->change('projectImplementation', FullscreenTextareaType::class)
        ;
    }

    public function changeEmbeddedAdmin(FormHandler $formHandler)
    {
        $formHandler
            ->with('projectMembers')
                ->remove('functionalResponsibilities')

            ->with('beneficiaryProblems')
                ->change('targetGroup', FullscreenTextareaType::class, ['attr' => ['readonly' => false]])
                ->change('socialResults', null, [
                    'required' => false,
                    'by_reference' => false,
                    'choices' => $this->em->getRepository(SocialResult::class)->findCachedByApplicationTypeData(ApplicationTypes::CONTINUATION_KNS),
                    'attr' => array('style' => 'margin-bottom: 10px')
                ])
            ->with('practiceImplementationActivities')
                ->change('expectedResults', null, [
                    'label' => $this->description['expectedResults'],
                ])
                ->change('deadLine', FullscreenTextareaType::class)
            ->with('individualSocialResults')
                ->change('socialResult', FullscreenTextareaType::class)
                ->change('targetGroup', FullscreenTextareaType::class)
                ->change('indicator', FullscreenTextareaType::class)
                ->change('customMeasurementMethod', FullscreenTextareaType::class)
        ;
    }

    private function getFirstTabData()
    {
        $object = $this->getSubject();
        $options['competition'] = $object->getCompetition()->getName();

        $date = $object->getCompetition()->getRealizationYear();
        $options['year'] = $date ? $date->format('Y') : (new \DateTime())->format('Y');

        $grant = $this->em->getRepository(GrantConfig::class)->findGrantByAuthorAndReportForm($object->getCompetition()->getRelatedReportForm(), $object->getAuthor());
        $options['contract'] = $grant ? $grant->getContract() : '';

        return $options;
    }
}
