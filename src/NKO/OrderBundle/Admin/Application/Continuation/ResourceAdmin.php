<?php

namespace NKO\OrderBundle\Admin\Application\Continuation;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ResourceAdmin extends AbstractAdmin
{
    const RESOURCE_ITEMS = 'resourceItems';
    const RESOURCE_ETC_ITEMS = 'resourceEtcItems';

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getParentFieldDescription()->getFieldName() == self::RESOURCE_ITEMS) {
            $formMapper
                ->add('nameResourceType', 'sonata_type_model', array(
                    'label' => 'Наименование ресурса',
                    'btn_add' => false,
                ), array('admin_code' => 'nko_order.admin.indicator'));
        }
        if ($this->getParentFieldDescription()->getFieldName() == self::RESOURCE_ETC_ITEMS) {
            $formMapper
                ->add('nameResource', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => 'Наименование ресурса'
                ));
        }
            $formMapper
            ->add('descriptionResource', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => 'Описание ресурса'
            ));
    }
}
