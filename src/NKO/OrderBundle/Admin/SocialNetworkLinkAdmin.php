<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 10.08.16
 * Time: 12:04
 */

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class SocialNetworkLinkAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('link', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Страницы организации в социальных сетях',
                    'attr' => array('help-bottom' => 'URL должен начинаться с http:// или https://')
                ));
    }
}