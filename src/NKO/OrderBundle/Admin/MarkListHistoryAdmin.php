<?php

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class MarkListHistoryAdmin extends AbstractAdmin
{

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('expert', null, [
                'admin_code' => 'sonata.admin.nko.expert_user'
            ])
            ->add('competition')
            ->add('author', null, [
                'admin_code' => 'sonata.admin.nko.nko_user'
            ])
            ->add('isSend')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('expert', null, [
                'admin_code' => 'sonata.admin.nko.expert_user'
            ])
            ->add('author', null, [
                'admin_code' => 'sonata.admin.nko.nko_user'
            ])
            ->add('competition')
            ->add('isSend')
            ->add("createdAt")
            ->add('_action', null, [
                'actions' => [
                    'download_mark' => ['template' =>  'NKOOrderBundle:CRUD:list_mark_list_download.html.twig'],
                    'revert_mark' =>array( 'template' =>  'NKOOrderBundle:CRUD:list__action_revert_mark.html.twig'),
                ],
            ])
        ;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->select(
            'partial ' .
            $query->getRootAlias() .
            '.{id, expert, author, competition, isSend, createdAt}'
        );

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->add('download_mark', $this->getRouterIdParameter() . '/download_mark')
            ->add('revert_mark', $this->getRouterIdParameter().'/revert_mark')
        ;
    }
}
