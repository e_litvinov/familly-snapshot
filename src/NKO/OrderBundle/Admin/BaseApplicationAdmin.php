<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/24/17
 * Time: 4:13 PM
 */

namespace NKO\OrderBundle\Admin;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Traits\SessionListFilterTrait;
use NKO\UserBundle\Entity\NKOUser;
use NKO\UserBundle\Services\CurrentUserService;
use NKO\UserBundle\Traits\CurrentUserTrait;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Knp\Menu\ItemInterface as MenuItemInterface;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication2018;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplciation2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KnsApplication2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KnsApplication2019;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication2018;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application as ContinuationApplicationSeconStage;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application2019\Application as Continuation2019ApplicationSeconStage;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNS2019Application;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNSSecondStage2018Application;
use NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018\Application as KNSThirdStage2018Application;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as HarborApplication2019;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application as KNSSecondStage2019Application;

class BaseApplicationAdmin extends AbstractAdmin
{
    const ENDING_ENTITY_RESOLVER = '\EntityResolver';

    use CurrentUserTrait;
    use SessionListFilterTrait;

    const LIST_DESCRIPTION = [
        'competitionName' => 'Конкурс',
        'isAccepted' => 'Принята'
    ];

    /**
     * @var ContainerInterface $container
     */
    protected $container;

    private $isHistoryCreated = null;

    /**
     * @var CurrentUserService $currentUserService
     */
    protected $currentUserService;

    protected $description = [];

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
        '_sort_by' => 'createdAt'
    );

    public function setNonGroups()
    {
        $this->setValidationGroups(['None']);
    }


    public function setValidationGroups(array $groups)
    {
        $this->formOptions['validation_groups'] = $groups;
    }

    public function getFilterParameters()
    {
        if (!$this->resetSessionFilters()) {
            $competitionId = $this->getCurrentCompetitionId();
            if (!$competitionId) {
                return parent::getFilterParameters();
            }

            $this->datagridValues = array_merge(
                [
                'competition' => [
                    'value' => $competitionId
                ]
                ],
                $this->datagridValues
            );
        }
        return parent::getFilterParameters();
    }

    public function getCurrentCompetitionId()
    {
        $this->getSubject();
        $user = $this->getCurrentUser();

        if ($user instanceof NKOUser) {
            $competitions = $this->getConfigurationPool()
                ->getContainer()
                ->get('doctrine')
                ->getRepository(Competition::class)
                ->getAvaliableNkoCompetitions($user);
        } else {
            $competitions = $this->getConfigurationPool()
                ->getContainer()
                ->get('doctrine')
                ->getRepository(Competition::class)
                ->findCurrentCompetitions();
        }

        if ($competitions) {
            return reset($competitions)->getId();
        }

        return null;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $currentCompetitions = $this->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getRepository(Competition::class)
            ->findCurrentCompetitions();
        $this->parameters = array('listmapper' => array('currentCompetition' => $currentCompetitions));

        $user = $this->getCurrentUser();
        if ($user->hasRole('ROLE_NKO')) {
            $listMapper
                ->add('competition')
                ->add('readyToSent')
                ->add('isSend')
                ->add('winner', null, array(
                    'editable' => false
                ));
        } else {
            $listMapper
                ->add('author.nko_name')
                ->add('author.psrn')
                ->add('readyToSent')
                ->add('isSend')
                ->add('isAccepted', null, [
                    'label' => self::LIST_DESCRIPTION['isAccepted']
                ])
                ->add('winner', null, array(
                    'editable' => false
                ))
                ->add('competition.name', null, [
                    'label' => self::LIST_DESCRIPTION['competitionName']
                ])
            ;
        }

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $listMapper->add('isAutosaved');
        }

        $listMapper->add('_action', null, array(
            'actions' => array(
                'send' => array('template' => 'NKOOrderBundle:CRUD:send_application.html.twig'),
                'download' => array('template' => 'NKOOrderBundle:CRUD:list__action_download.html.twig'),
                'edit' => array('template' => 'NKOOrderBundle:CRUD:list__action_edit_application.html.twig'),
                'delete' => array()
            )
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('author.psrn')
            ->add('readyToSent')
            ->add('isSend')
            ->add('winner');

        $user = $this->getCurrentUser();

        if ($user->hasRole('ROLE_NKO')) {
            $datagridMapper
                ->add('competition', null, array(), 'entity', [
                    'choices' =>
                        $this->getConfigurationPool()
                            ->getContainer()
                            ->get('doctrine')
                            ->getRepository(Competition::class)
                            ->getAvaliableNkoCompetitions($user)
                ]);
        } else {
            $datagridMapper
                ->add('author.nko_name')
                ->add('author.username')
                ->add('competition');
        }
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $user = $this->getCurrentUser();
        if ($user->hasRole('ROLE_NKO')) {
            $query
                ->andWhere($query->getRootAlias() . '.author = :user')
                ->setParameter('user', $user);
        }
        $user = $this->getCurrentUser();
        if ($user->hasRole('ROLE_NKO')) {
            $query->select('partial ' . $query->getRootAlias() . '.{id, competition, readyToSent, isSend, winner, isAutosaved, isAccepted}');
        } else {
            $query->select(
                'partial ' .
                $query->getRootAlias() .
                '.{id, author, readyToSent, isSend, isAccepted, winner, competition, isAutosaved}'
            );
        }

        return $query;
    }

    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('send', $this->getRouterIdParameter() . '/send')
            ->add('download', $this->getRouterIdParameter() . '/download')
            ->add('contacts')
            ->add('save', $this->getRouterIdParameter() . '/save')
            ->add('clear', $this->getRouterIdParameter() . '/clear')
            ->add('allowAutosave', $this->getRouterIdParameter() . '/allowAutosave')
        ;
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this;
        $em = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getEntityManager();

        $user = $this->getCurrentUser();
        $competitionId = array_key_exists('competition', $this->getFilterParameters()) ? $this->getFilterParameters()['competition']['value'] : null;

        if (!$competitionId) {
            return;
        }

        $competition = $em->getRepository(Competition::class)->find($competitionId);


        $menu->addChild('link_filter_without_values', [
            'uri' => $admin->generateUrl('list', [
                'without_competition' => 1
            ]),
            'label' => 'link_filter_without_values_application'
        ]);
        if ($this->getDatagrid()->getResults() && $competition && $competition->isNeedContacts() && !in_array('ROLE_NKO', $user->getRoles())) {
            $menu->addChild('contacts', [
                'linkAttributes' => ['target' => '_blank'],
                'uri' => $admin->generateUrl('contacts', [
                  'competition' => $this->getFilterParameters()['competition']['value']
                ])
            ]);
        }
    }

    public function postUpdate($object)
    {
        if ($this->isHistoryCreated) {
            return;
        }

        $this->container = $this->getConfigurationPool()->getContainer();
        $em = $this->container->get('doctrine')->getManager();

        $em->detach($object);

        $object = $this->container->get('nko.resolver.proxy_resolver')->resolveProxies($object);
        $object = $this->container->get('nko.resolver.collection_resolver')->fetchCollections($object);
        $object = $this->resolveNestedCollections($object);

        $history = $this->container->get('nko.archiver.order_archiver')->archive($object);

        $em->clear();

        if ($object->getIsSend() && !$object->getIsAutosaved()) {
            $prevHistory = $em->getRepository(ApplicationHistory::class)->findOneBy([
                'isSend' => true,
                'competition' => $object->getCompetition()->getId(),
                'author' => $object->getAuthor()
            ]);

            if ($prevHistory) {
                $prevHistory->setIsSend(false);
                $history->setNumber($prevHistory->getNumber());
            }
        }

        $competition = $em->getRepository(Competition::class)->find($history->getCompetition()->getId());
        $user = $em->getRepository(NKOUser::class)->find($object->getAuthor()->getId());
        $history->setCompetition($competition);
        $history->setAuthor($user);

        $em->persist($history);
        $em->flush();
    }

    /**
     * Get the list of actions that can be accessed directly from the dashboard.
     *
     * @return array
     */
    public function getDashboardActions()
    {
        $actions = array();

        if ($this->hasRoute('create') && $this->isGranted('CREATE')) {
            $actions['create'] = array(
                'label' => 'link_add_application',
                'translation_domain' => 'SonataAdminBundle',
                'template' => $this->getTemplate('action_create'),
                'url' => $this->generateUrl('create'),
                'icon' => 'plus-circle',
            );
        }

        if ($this->hasRoute('list') && $this->isGranted('LIST')) {
            $actions['list'] = array(
                'label' => 'link_list',
                'translation_domain' => 'SonataAdminBundle',
                'url' => $this->generateUrl('list'),
                'icon' => 'list',
            );
        }

        return $actions;
    }

    /**
     * @param string $action
     * @param mixed $object
     *
     * @return array
     */
    public function getActionButtons($action, $object = null)
    {
        $list = $this->configureActionButtons($action, $object);

        foreach ($this->getExtensions() as $extension) {
            // TODO: remove method check in next major release
            if (method_exists($extension, 'configureActionButtons')) {
                $list = $extension->configureActionButtons($this, $list, $action, $object);
            }
        }

        foreach ($list as $key => &$value) {
            if ($key == 'create') {
                foreach ($value as $k => &$v) {
                    if ($k == 'template') {
                        $v = 'NKOOrderBundle:Button:create_button.html.twig';
                    }
                }
            }
        }

        return $list;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig'
            )
        );
    }

    private function resolveNestedCollections($application)
    {
        switch (get_class($application)) {
            case BriefApplication2018::class:
                $this->updateNestedCollection($application->getBeneficiaryProblems());

                foreach ($application->getBeneficiaryProblems() as $problem) {
                    $this->updateNestedCollection($problem);
                }

                $this->updateNestedCollection($application->getFinancingSources());
                $this->updateNestedCollection($application->getEffectivenessItems());
                break;
            case FarvaterApplciation2018::class:
                $this->updateNestedCollection($application->getProjectSocialResults());
                $this->updateNestedCollection($application->getProjectSocialIndividualResults());
                $this->updateNestedCollection($application->getProjectDirectResults());
                $this->updateNestedCollection($application->getProjectDirectIndividualResults());
                $this->updateNestedCollection($application->getPracticeSocialResults());
                $this->updateNestedCollection($application->getPracticeDirectResults());
                $this->updateNestedCollection($application->getPracticeDirectIndividualResults());
                $this->updateNestedCollection($application->getSpecialistProblems());
                break;
            case KnsApplication2018::class:
                $this->updateNestedCollection($application->getProjects());
                $this->updateNestedCollection($application->getProjectResults());
                $this->updateNestedCollection($application->getEmployeeResults());
                $this->updateNestedCollection($application->getBeneficiaryResults());
                break;

            case KnsApplication2019::class:
                $this->updateNestedCollection($application->getProjects());
                $this->updateNestedCollection($application->getProjectResults());
                $this->updateNestedCollection($application->getEmployeeResults());
                $this->updateNestedCollection($application->getBeneficiaryResults());
                break;

            case ContinuationApplication2018::class:
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->removeLinkedHistoryData($application);

                foreach ($application->getBeneficiaryProblems() as $problem) {
                    $this->updateNestedCollection($problem);
                }
                break;

            case ContinuationApplicationSeconStage::class:
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->updateNestedCollection($application->getSpecialistProblems());

                $this->updateNestedCollection($application->getEffectivenessImplementationEtcItems());
                $this->updateNestedCollection($application->getExpectedResults());

                $this->updateNestedCollection($application->getEffectivenessDisseminationEtcItems());
                $this->updateNestedCollection($application->getPracticeResults());

                $this->removeLinkedHistoryData($application);

                break;

            case Continuation2019ApplicationSeconStage::class:
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->updateNestedCollection($application->getSpecialistProblems());

                $this->updateNestedCollection($application->getEffectivenessImplementationEtcItems());
                $this->updateNestedCollection($application->getExpectedResults());

                $this->updateNestedCollection($application->getEffectivenessDisseminationEtcItems());
                $this->updateNestedCollection($application->getPracticeResults());

                $this->removeLinkedHistoryData($application);

                break;

            case ContinuationKNS2019Application::class:
                $this->updateNestedCollection($application->getProjects());
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->updateNestedCollection($application->getEffectivenessKNSItems());
                $this->updateNestedCollection($application->getIndividualSocialResults());

                $this->removeLinkedHistoryData($application);
                break;
            case KNSSecondStage2018Application::class:
                $this->updateNestedCollection($application->getProjects());
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->updateNestedCollection($application->getIndividualSocialResults());
                $this->updateNestedCollection($application->getEffectivenessItems());
                break;
            case KNSSecondStage2019Application::class:
                $this->updateNestedCollection($application->getProjects());
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->updateNestedCollection($application->getIndividualSocialResults());
                $this->updateNestedCollection($application->getEffectivenessItems());
                break;
            case KNSThirdStage2018Application::class:
                $this->updateNestedCollection($application->getProjects());
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $this->updateNestedCollection($application->getIndividualSocialResults());
                $this->updateNestedCollection($application->getEffectivenessKNSItems());
                break;
            case HarborApplication2019::class:
                /** @var $application HarborApplication2019 */
                $this->updateNestedCollection($application->getProjects());
                break;
        }

        return $application;
    }

    /** @var $object ContinuationKNS2019Application */
    private function removeLinkedHistoryData($object)
    {
        $linkedHistory = $object->getLinkedApplicationHistory();

        if ($linkedHistory) {
            $linkedHistory->setData(null);
        }
    }

    private function updateNestedCollection($collection)
    {
        foreach ($collection as $item) {
            $item = $this->container->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->container->get('nko.resolver.collection_resolver')->fetchCollections($item);
        }
    }

    public function setIsHistoryCreated($isHistoryCreated)
    {
        $this->isHistoryCreated = $isHistoryCreated;
    }

    public function preRemove($object)
    {
        $entityResolverService = $this->getConfigurationPool()->getContainer()->get(get_class($object).self::ENDING_ENTITY_RESOLVER);
        $entityResolverService->preDeleteAction($object, null);

        parent::preRemove($object);
    }
}
