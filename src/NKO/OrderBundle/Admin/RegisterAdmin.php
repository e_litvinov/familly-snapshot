<?php

namespace NKO\OrderBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as Report2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as Report2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;

class RegisterAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $subjectClass = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());

        switch ($subjectClass) {
            case Report2018::class:
            case Report2019::class:
            case MixedReport2019::class:
                $adminCode =  'nko_order.admin.finance_report.expense_type';
                break;
            case MixedReport2020::class:
                $adminCode =  'nko_order.admin.finance_report.expense_type';
                break;
            default:
                $adminCode = 'nko_order.admin.expense_type';
                break;
        }

        $formMapper
            ->add('expenseType', 'sonata_type_admin', [
                'label' => false,
            ], ['admin_code' => $adminCode])

            ->add('expenses', 'sonata_type_collection', [
                'label' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.expense',
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('totalExpenseValue', TextType::class, [
                'required' => false,
                'label' => '<b>ИТОГО</b>',
                'attr' => [
                    'readonly' => true
                ]
            ]);
    }
}
