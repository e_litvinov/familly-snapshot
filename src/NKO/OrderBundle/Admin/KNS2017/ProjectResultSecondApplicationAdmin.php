<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 8/28/17
 * Time: 5:09 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ProjectResultSecondApplicationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('resultCriteria', TextareaType::class,
                array(
                    'label' => 'Результат',
                    'disabled' => true,
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('targetValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Целевое значение, на дату окончания проекта (к 15.12.2017)',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('approximateTargetValue', NumberType::class,
                array(
                    'required' => false,
                    'label' => 'Ориентировочное целевое значение (на 01.06.2018)',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('methodMeasurement', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Метод измерения',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));
    }

}