<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/22/17
 * Time: 1:37 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class RiskAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('keyRisk', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ключевые риски',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('actionToReduceRisk', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Действия, нацеленные на снижение рисков',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));
    }

}