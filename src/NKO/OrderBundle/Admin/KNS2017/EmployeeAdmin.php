<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/22/17
 * Time: 12:31 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class EmployeeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('fullName', FullscreenTextareaType::class,
                array(
                    'label' => 'ФИО',
                    'required' => false,
                    'attr' => array(
                        'maxlength' => 255,
                        'placeholder' => '-'
                    )
                ))
            ->add('position', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Должность и выполняемые функции в организации',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('experienceAndEducation', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Опыт и образование',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('educationalActivity', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Обучающие мероприятия',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
        ;
    }
}