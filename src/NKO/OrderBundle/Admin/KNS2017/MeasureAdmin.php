<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/22/17
 * Time: 12:52 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class MeasureAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('action', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Мероприятие',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('deadline', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Сроки',
                    'attr' => array(
                        'placeholder' => '-',
                        'maxlength' => 255,
                    )
                ))
            ->add('actionLocation', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Место проведения',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));

    }

}