<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/22/17
 * Time: 1:15 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNSApplication2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNSApplication2019;
use NKO\OrderBundle\Traits\AdminFieldsTrait;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Application as KNSApplication2020;

class BeneficiaryResultAdmin extends EmbeddedAdminDecorator
{
    use AdminFieldsTrait;
    const DESCRIPTIONS = [
        'KNS2018' => [
            'result' => 'Результат',
            'indicator' => 'Показатель',
            'targetValue' => 'Целевое значение, на дату окончания проекта',
            'approximateTargetValue' => 'Ориентировочное целевое значение (через 6 месяцев после окончания проекта)',
            'linkedMethod' => 'Способ измерения',
            'methodMeasurement' => 'Комментарий',
        ],
        'default' => [
            'result' => 'Результат',
            'indicator' => 'Показатель',
            'targetValue' => 'Целевое значение на 15.11.2017',
            'approximateTargetValue' => 'Ориентировочное целевое значение на 01.06.2018',
            'methodMeasurement' => 'Способ измерения'
        ]
    ];

    const TYPES = [
        'KNS2018' => [
            KNSApplication2018::class,
            KNSApplication2019::class,
            KNSApplication2020::class,
        ]
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $type = $this->generateType(self::TYPES);
        $formMapper
            ->add('result', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['result'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('indicator', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['indicator'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('targetValue', TextType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['targetValue'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('approximateTargetValue', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['approximateTargetValue'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));

        if ($type === 'KNS2018') {
            $formMapper
                ->add('linkedMethod', null, [
                    'label' => self::DESCRIPTIONS[$type]['linkedMethod'],
                    'required' => true,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('o')
                            ->where('o.type like :type')
                            ->setParameter('type', '%' . ApplicationTypes::KNS_APPLICATION_2018 . $this->getParentFieldDescription()->getFieldName() . '%');
                    }
                ]);
        }

        $formMapper
            ->add('methodMeasurement', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['methodMeasurement'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));
    }
}