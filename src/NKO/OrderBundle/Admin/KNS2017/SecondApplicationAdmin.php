<?php

namespace NKO\OrderBundle\Admin\KNS2017;

use Doctrine\ORM\EntityRepository;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Form\RadioButtonTree;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\AdminConfigurator\BankDetailsAdmin;
use NKO\OrderBundle\Utils\Application\AdminConfigurator;
use NKO\OrderBundle\AdminConfigurator\OrganizationInfoAdmin;
use NKO\OrderBundle\AdminConfigurator\OrganizationActivityAdmin;

class SecondApplicationAdmin extends BaseApplicationAdmin
{
    const KNS2017_ASSOCIATION_ENTITY_NAME = 'KNS-2017';
    const COMMON_KNS_ASSOCIATION_ENTITY_NAME  = 'KNS';
    const COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME  = 'application';

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->formOptions = array(
            'validation_groups' => [
                'KNS-2017',
                'Address',
                'GeneralInformation',
                'CountEmployees',
                'HeadOfOrganization',
                'HeadOfProject',
                'HeadOfAccounting',
                'BankDetails',
            ]
        );

        $this->description = $this->description + [
                'deadLineLabel' => 'Сроки реализации проекта (не ранее 1 ноября и не позднее 15 декабря 2017)',
                'primaryActionInfoLabel' => '<b>Информация об основном обучающем мероприятии</b>',
                'employeeLabel' => '<p>3.7.6 <b>Сотрудники организации</b> (члены общественного объединения), которые непосредственно примут участие в обучающих мероприятиях (стажировке).</p>
                    <p>Укажите, ФИО конкретных сотрудников, которые примут участие в стажировке (обучающих мероприятиях), их роль в организации, выполняемые функции, образование и опыт. В графе "Обучающие мероприятия" укажите те мероприятия, в которых непосредственно примет участие сотрудник.</p>',
                'knowledgeImplementationLabel' => '<b>Внедрение полученных знаний и опыта</b><br><br>
                    Как именно в организации будут использоваться знания и опыт, полученные в ходе проекта? Перечислите конкретные действия, которые будут предприняты в период реализации проекта, а также после его окончания.<br><br>
                    В срок до 01.06.2018 Фонд проведет мониторинг проектов, поддержанных в рамках Конкурса. По итогам мониторинга, организации, успешно реализовавшие свои проекты и продемонстрировавшие конкретные результаты внедрения полученных знаний и навыков, смогут принять участие во втором (закрытом) этапе Конкурса и получить дополнительную финансовую поддержку на дальнейшее развитие своего проекта в 2018 - 2019 гг.',
                'actionLabel' => '<b>План-график мероприятий проекта</b>',
                'projectResultLabel' => '<b>Ожидаемые результаты проекта</b>',
                'resultLabel' => '<b>Качественные изменения у благополучателей</b>',
                'documentLabel' => '<b>При подаче заявки необходимо предоставить в электронном виде скан-копии следующих документов, заверенных подписью руководителя и печатью организации, и оформленных согласно инструкции <a href="http://deti.timchenkofoundation.org/informationmaterials/39" >(ссылка)</a></b>'
            ];
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($competition_id = $this->getRequest()->get('competition')) {
            $competition = $this->getConfigurationPool()->getContainer()
                ->get('Doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->find($competition_id)
            ;

            $isCurrentCompetition = $this->getConfigurationPool()->getContainer()
                ->get('Doctrine')
                ->getRepository('NKOOrderBundle:Competition')
                ->isCurrentCompetition($competition);

            if ($isCurrentCompetition) {
                $this->getSubject()->setCompetition($competition);
            }
        }

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $formMapper
            ->with('Application Info');
        OrganizationInfoAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())], $em);
        OrganizationActivityAdmin::configureFormFields($formMapper, AdminConfigurator::ORGANIZATION_ACTIVITY[get_class($this->getSubject())]);
        $formMapper
            ->end()
            ->with('Banking Details');
        BankDetailsAdmin::configureFormFields($formMapper, AdminConfigurator::BANK_DETAILS[get_class($this->getSubject())]);

        if ($this->getSubject()
            && $this->getSubject()->getAuthor()
            && $this->getSubject()->getAuthor()->getStatus()
            && $this->getSubject()->getAuthor()->getStatus()->getSlug() == 'gosudarstviennaia-munitsipal-naia-orghanizatsiia') {
            $formMapper
                ->add('recipientName', TextType::class, array(
                        "label" => "Наименование получателя",
                        "required" => false,
                        'attr' => array('maxlength' => 255),
                    ))
                ->add('oktmo2', null, array(
                    "label" => "ОКТМО получателя",
                    "required" => false,
                    'attr' => array('maxlength' => 8),
                    'help' => 'ОКТМО должен состоять из 8 цифр'
                ))
                ->add('kbk2', TextType::class, array(
                        "label" => "КБК для перечисления пожертвования",
                        "required" => false,
                        'attr' => array('maxlength' => 20),
                        'help' => 'КБК должен состоять из 20 цифр'
                    ))
                ;
        }
        $formMapper
            ->end()
            ->with('Project Description')
            ->add('projectName', TextType::class, array(
                    'required' => false,
                    'label' => '3.1 Название проекта',
                    'attr' => array('maxlength' => 450),
                ))
            ->add('deadLineLabel', CustomTextType::class, array(
                    'help' => '3.2 ' . $this->description['deadLineLabel'],
                ))
            ->add('deadLineStart', DateType::class, array(
                    'required' => false,
                    'label' => 'с',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2017',
                        'data-date-end-date' => '15-12-2017',
                        'maxlength' => 10
                    ]))
            ->add('deadLineFinish', DateType::class, array(
                    'required' => false,
                    'label' => 'по',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2017',
                        'data-date-end-date' => '15-12-2017',
                        'maxlength' => 10
                    ]))
            ->add('projectPurpose', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => '3.3 <b>Цель</b> проекта',
                    'attr' => array('maxlength' => 450),
                ))
            ->add('projectRelevance', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => '3.4 <b>Обоснование актуальности и важности</b> проекта',
                    'attr' => array('maxlength' => 1000),
                ))
            ->add('beneficiaryGroups', 'sonata_type_model', array(
                    'required' => false,
                    'label' => '3.5 Укажите <b>основные целевые группы благополучателей</b>, на которых повлияет реализация проекта',
                    "property" => "groupName",
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('beneficiaryGroupEtc', TextType::class, array(
                    'required' => false,
                    'label' => 'иные группы (укажите, какие именно)',
                ))
            ->add('childrenCategories', 'sonata_type_model', array(
                    'required' => false,
                    'label' => '3.5.1 Укажите, если проект ориентирован <b>преимущественно</b> на детей 
                        «сложных» для семейного устройства категорий:',
                    "property" => "categoryName",
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => $this->getChildrenCategoriesBy(array(
                        self::KNS2017_ASSOCIATION_ENTITY_NAME,
                        self::COMMON_KNS_ASSOCIATION_ENTITY_NAME,
                        self::COMMON_APPLICATION_ASSOCIATION_ENTITY_NAME
                    ))
                ))
            ->add('childrenCategoryName', TextType::class, array(
                    'required' => false,
                    'label' => 'иные группы (укажите)',
                ))
            ->add('projectImplementation', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => '3.6 <b>Механизм реализации проекта</b>',
                    'attr' => array('maxlength' => 1500),
                ))
            ->add('primaryActionInfoLabel', CustomTextType::class, array(
                    'help' => '3.7 ' . $this->description['primaryActionInfoLabel'],
                ))
            ->add('trainingGrounds', RadioButtonTree::class, array(
                    'class' => 'NKOOrderBundle:KNS2017\TrainingGround',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->andWhere('u.competition = :name')
                            ->setParameters(array('name' => 'kns2017-2'));
                    },
                    'required' => false,
                    'label' => '<br>3.7.1 <b>Тематика стажировки</b> (обучающего мероприятия)',
                    'multiple' => true,
                    'help' => 'Простановка системой галочек по умолчанию не означает, что данный пункт имеет приоритет перед другими вариантами'
                ))
            ->add('choosingGroundExplanation', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => '3.7.3 Поясните, <b>почему</b> Вы выбрали именно эту площадку?',
                    'attr' => array('maxlength' => 1500),
                ))
            ->add('dateStartOfInternship', DateType::class, array(
                    'required' => false,
                    'label' => '3.7.4 <b>Дата начала проведения стажировки</b> (обучающего мероприятия) (не ранее 01.11.2017, не позднее 15.12.2017)',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-date-format' => 'DD-MM-YYYY',
                        'data-date-start-date' => '01-11-2017',
                        'data-date-end-date' => '15-12-2017',
                        'maxlength' => 10
                    ]))
            ->add('traineeshipFormats', 'sonata_type_model', array(
                    'required' => false,
                    'label' => '3.7.5 <b>Формат стажировки</b> (обучающего мероприятия)',
                    "property" => "formatName",
                    'expanded' => true,
                    'multiple' => true
                ))
            ->add('traineeshipFormatName', TextType::class, array(
                    'required' => false,
                    'label' => 'другое (укажите)',
                ))
            ->add('employeeLabel', CustomTextType::class, array(
                    'help' => $this->description['employeeLabel'],
                ))
            ->add('employees', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => ' ',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.employee',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('knowledgeImplementationLabel', CustomTextType::class, array(
                    'help' => '3.8 ' . $this->description['knowledgeImplementationLabel'],
                ))
            ->add('knowledgeIntroductionDuringProjectImplementation', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => '3.8.1 В период реализации проекта(до 15.12.2017)',
                    'attr' => array('maxlength' => 1500),
                ))
            ->add('knowledgeIntroductionAfterProjectImplementation', FullscreenTextareaType::class, array(
                    'required' => false,
                    'label' => '3.8.2 После окончания реализации проекта(до 01.06.2018)',
                    'attr' => array('maxlength' => 1500),
                ))
            ->add('actionLabel', CustomTextType::class, array(
                    'help' => '3.9 ' . $this->description['actionLabel'],
                ))
            ->add('traineeships', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '3.9.1 <b>СТАЖИРОВКА (обучающее мероприятие)</b>',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.traineeship',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('measures', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '3.9.2 <b>МЕРОПРИЯТИЯ ПО ВНЕДРЕНИЮ ПОЛУЧЕННЫХ ЗНАНИЙ И ОПЫТА</b>',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.measure',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('projectResultLabel', CustomTextType::class, array(
                    'help' => '3.10 ' . $this->description['projectResultLabel'],
                ))
            ->add('projectResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '3.10.1 Укажите, <b>какие количественные результаты будут получены благодаря реализации проекта, и как вы об этом узнаете.</b>',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.project_result_second_application',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('employeeResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '3.10.2 <b>Качественные результаты для сотрудников организаций / членов общественного объединения</b>',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.employee_result_second_application',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('socialResults', EntityType::class, array(
                    'class' => 'NKOOrderBundle:KNS2017\SocialResult',
                    'required' => false,
                    'label' => '3.10.3 <b>Программа «Семья и дети» Фонда Тимченко направлена на достижение социальных результатов, перечисленных ниже. Выберите из списка, на достижение каких результатов направлена реализация вашего проекта. </b>',
                    'expanded' => true,
                    'multiple' => true,
                ))
            ->add('resultLabel', CustomTextType::class, array(
                    'help' => '3.10.4 ' . $this->description['resultLabel'],
                ))
            ->add('beneficiaryResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '<b>РЕЗУЛЬТАТЫ ДЛЯ БЛАГОПОЛУЧАТЕЛЕЙ</b>',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.beneficiary_result_second_application',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('risks', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => '3.10.5 <b>Риски</b>. Что может препятствовать внедрению результатов проекта?',
                    'btn_add' => "Добавить",
                    'help' => 'Система не позволяет удалять единственную строку таблицы, поэтому, если Вы вначале заполнили одну строку в таблице, а затем решили это удалить, сотрите, пожалуйста, информацию и поставьте прочерк.<br>Система не осуществляет проверку этого пункта на заполнение, Вы должны самостоятельно убедиться в том, что верно заполнили его.',
                    'by_reference' => false
                ), array(
                    'admin_code' => 'sonata.admin.nko.order.kns2017.risk',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('requestedFinancingMoney', TextType::class, array(
                    'required' => false,
                    'label' => '3.11 <b>Сумма запрашиваемого финансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях',
                    'attr' => array('maxlength' => 10)
                ))
            ->add('cofinancingMoney', TextType::class, array(
                    'required' => false,
                    'label' => '3.12 <b>Сумма софинансирования</b> (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»), в рублях.',
                    'attr' => array('maxlength' => 10)
                ))
            ->end()
            ->with('ДОКУМЕНТЫ К ЗАЯВКЕ  ')




            ->add('regulation', FilePreviewType::class, array(
                    'data_class' => null,
                    'required' => false
                ))
            ->add('organizationCreationResolution', FilePreviewType::class, array(
                    'data_class' => null,
                    'required' => false
                ))
            ->add('budget', FilePreviewType::class, array(
                    'data_class' => null,
                    'required' => false
                ))
            ->end();
        ;
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array('NKOOrderBundle:Form:form_admin_fields.html.twig',
                'NKODefaultBundle:form:form_label.html.twig')
        );
    }

    public function getChildrenCategoriesBy($associationEntityNames)
    {
        $directions = $this->getConfigurationPool()
            ->getContainer()
            ->get('Doctrine')
            ->getManager()
            ->getRepository('NKOOrderBundle:ChildrenCategory')
            ->findBy(
                array('type' => $associationEntityNames)
            );
        return $directions;
    }
}
