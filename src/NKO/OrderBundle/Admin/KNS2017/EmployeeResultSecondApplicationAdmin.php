<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 8/28/17
 * Time: 5:14 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class EmployeeResultSecondApplicationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('result', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Результат',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('indicator', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Показатель',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('targetValue', TextType::class,
                array(
                    'required' => false,
                    'label' => 'Целевое значение на 15.12.2017',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('approximateTargetValue', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Ориентировочное целевое значение на 01.06.2018',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('methodMeasurement', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Способ измерения',
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));
    }

}