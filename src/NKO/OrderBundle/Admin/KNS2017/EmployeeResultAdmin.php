<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/22/17
 * Time: 1:28 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Admin\Decorator\EmbeddedAdminDecorator;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNSApplication2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNSApplication2019;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Application as KNSApplication2020;
use NKO\OrderBundle\Traits\AdminFieldsTrait;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;

class EmployeeResultAdmin extends EmbeddedAdminDecorator
{
    use AdminFieldsTrait;

    const DESCRIPTIONS = [
        'KNS2018' => [
            'result' => 'Результат',
            'indicator' => 'Показатель',
            'targetValue' => 'Целевое значение, на дату окончания проекта',
            'approximateTargetValue' => 'Ориентировочное целевое значение (через 6 месяцев после окончания проекта)',
            'linkedMethod' => 'Способ измерения',
            'methodMeasurement' => 'Комментарий',
        ],
        'default' => [
            'result' => 'Результат',
            'indicator' => 'Показатель',
            'targetValue' => 'Целевое значение на 15.11.2017',
            'approximateTargetValue' => 'Ориентировочное целевое значение на 01.06.2018',
            'methodMeasurement' => 'Способ измерения'
        ]
    ];

    const TYPES = [
        'KNS2018' => [
            KNSApplication2018::class,
            KNSApplication2019::class,
            KNSApplication2020::class
        ]
    ];
    
    protected function configureFormFields(FormMapper $formMapper)
    {
        $type = $this->generateType(self::TYPES);
        $formMapper
            ->add('result', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['result'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('indicator', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['indicator'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('targetValue', TextType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['targetValue'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ))
            ->add('approximateTargetValue', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['approximateTargetValue'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));

        if ($type === 'KNS2018') {
            $formMapper
                ->add('linkedMethod', null, [
                    'label' => self::DESCRIPTIONS[$type]['linkedMethod'],
                    'required' => true,
                    'choices' => $this->sortedData['linkedMethod'],
                ]);
        }

        $formMapper
            ->add('methodMeasurement', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => self::DESCRIPTIONS[$type]['methodMeasurement'],
                    'attr' => array(
                        'placeholder' => '-'
                    )
                ));
    }

    protected function sort(array $items = [])
    {
        $class = '';
        $sequence = [];
        $subject = $this->getParentFieldDescription()->getAdmin()->getSubject();

        if ($subject) {
            switch (get_class($subject)) {
                case KNSApplication2020::class:
                    $class = ApplicationTypes::KNS_APPLICATION_2020;
                    $sequence = [2, 3, 4, 1, 17, 5, 7, 19];
                    break;
                default:
                    $class = ApplicationTypes::KNS_APPLICATION_2018;
                    break;
            }
        }

        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();

        parent::sort([
            'linkedMethod' => [
                'query' => $em->getRepository(MeasurementMethod::class)->findMethodsByTypeData($class . $this->getParentFieldDescription()->getFieldName()),
                'sequence' => $sequence
            ]
        ]);
    }

}