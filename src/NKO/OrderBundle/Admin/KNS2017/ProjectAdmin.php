<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/22/17
 * Time: 12:07 PM
 */

namespace NKO\OrderBundle\Admin\KNS2017;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;

class ProjectAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('projectName', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Название проекта',
                ))
            ->add('deadline', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Сроки реализации',
                    'attr' => array('maxlength' => 255)
                ))
            ->add('result', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Результаты',
                ))
            ->add('donorsPartnersAndTheirRole', FullscreenTextareaType::class,
                array(
                    'required' => false,
                    'label' => 'Доноры и партнеры и их роль',
                ))
        ;
    }
}