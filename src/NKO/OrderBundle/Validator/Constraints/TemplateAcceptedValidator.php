<?php

namespace NKO\OrderBundle\Validator\Constraints;

use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class TemplateAcceptedValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!($value instanceof BaseReport)) {
            throw new \Exception("Not correct using of constraint");
        }

        if (!$value->getReportTemplate()->getIsPlanAccepted()) {
            $this->context->buildViolation("У отчёта должны быть приняты плановые показатели")->addViolation();
        }
    }
}
