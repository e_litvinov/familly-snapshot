<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExcelExtension extends Constraint
{
    public $message = 'Please upload a valid excel file';
}