<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CompetitionDateRange extends Constraint
{
    public $messageFailedRange = 'Competition already exists in this date range.';

    public $messageFailedStartDate = 'Start date can not be greater or equal finish date.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}