<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class TemplateAccepted extends Constraint
{
    public $message;

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
