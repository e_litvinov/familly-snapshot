<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback as FeedbackEntity;

class FeedbackValidator extends ConstraintValidator
{
    /** @var PropertyAccessor */
    private $accessor;

    /**
     * @param $constraint Feedback
     * @param $value Collection
     */
    public function validate($value, Constraint $constraint)
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();

        if (!$value && !$value->count()) {
            return;
        }

        $report = $this->context->getObject();
        $countOfFeedbackable = $this->accessor->getValue($report, $constraint->inversedBy)->filter(function ($publication) {
            return $publication->getIsFeedback();
        })->count();

        if (!$countOfFeedbackable) {
            return;
        }

        foreach ($value as $key => $item) {
            $this->validateEntity($item, $constraint, $key);
        }
    }

    /**
     * @param $constraint Feedback
     * @param $value FeedbackEntity
     */
    private function validateEntity($value, Constraint $constraint, $key)
    {
        switch ($constraint->nameOfMethod) {
            case Feedback::DEFAULT_METHOD:
                $this->defaultValidate($value, $constraint, $key);
                break;
            case Feedback::DEFAULT_METHOD_EFFECTIVENESS:
                $this->defaultEffectivenessValidate($value, $constraint, $key);
                break;
        }
    }

    /**
     * @param $constraint Feedback
     * @param $value FeedbackEntity
     */
    private function defaultValidate($value, Constraint $constraint, $key)
    {
        $fields = [
            'comment',
            'practice',
            'administrativeSolution'
        ];

        foreach ($fields as $field) {
            $this->notNullValidate($value, $constraint, $key, $field);
        }
    }

    /**
     * @param $constraint Feedback
     * @param $value FeedbackEntity
     */
    private function defaultEffectivenessValidate($value, Constraint $constraint, $key)
    {
        $fields = [
            'comment',
            'practiceEffectiveness',
            'administrativeSolution'
        ];

        foreach ($fields as $field) {
            $this->notNullValidate($value, $constraint, $key, $field);
        }
    }

    /**
     * @param $constraint Feedback
     * @param $value FeedbackEntity
     */
    private function notNullValidate($value, Constraint $constraint, $key, $field)
    {
        if ($this->accessor->getValue($value, $field)) {
            return;
        }

        $message = key_exists($field, $constraint->messages) ?
            $constraint->messages[$field] :
            $constraint->defaultMessage;

        $subPath = '['. $key. '].'. $field;

        $this->context->buildViolation($message)
            ->atPath($subPath)
            ->addViolation();
    }
}
