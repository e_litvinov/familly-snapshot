<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/9/17
 * Time: 3:38 PM
 */

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PdfExcelExtensionValidator extends ConstraintValidator
{
    const EXT = [
        'xls',
        'xlsx',
        'pdf'
    ];

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return;
        }

        $extension = pathinfo($value->getClientOriginalName())['extension'];

        if (!in_array($extension, self::EXT))
        {
            $this->context->buildViolation($this->container->get('translator')->trans($constraint->message))
                ->addViolation();
        }

    }

}