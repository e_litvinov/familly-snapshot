<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CollectionDecoratorValidation extends Constraint
{
    /**
     * Name of property needed in validation
     * @var string
     */
    public $propertyName;

    /**
     * Name of validator
     * @var string
     */
    public $validatorClass;

    /**
     * Array of options for validator
     * @var array
     */
    public $validatorOptions;

    /**
     * Array of options for validator
     * @var array
     */
    public $isProperty = true;

    public function getRequiredOptions()
    {
        return [
            'propertyName',
            'validatorClass',
        ];
    }

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
