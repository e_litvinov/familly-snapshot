<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PdfExtensionValidator extends ConstraintValidator
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function validate($value, Constraint $constraint)
    {
        if(!$value || is_string($value)){
            return;
        }

        $originalExtension = pathinfo($value->getClientOriginalName())['extension'];
        if (strtolower($originalExtension) != 'pdf') {
            $this
                ->context
                ->buildViolation($this->container->get('translator')->trans($constraint->message))
                ->addViolation();
        }
    }
}