<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PdfExtension extends Constraint
{
    public $message = 'Please upload a valid PDF file';
}