<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 5.7.18
 * Time: 17.52
 */

namespace NKO\OrderBundle\Validator\Constraints;


use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PdfXlsDocExtensionValidator extends ConstraintValidator
{
    const EXT = [
        'application/msword',
        'application/vnd.ms-office',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',

        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

        'application/pdf'
    ];

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$value || is_string($value)) {
            return;
        }

        $extension = $value->getMimeType();

        if (!in_array($extension, self::EXT))
        {
            $this->context->buildViolation($this->container->get('translator')->trans($constraint->message))
                ->addViolation();
        }
    }
}
