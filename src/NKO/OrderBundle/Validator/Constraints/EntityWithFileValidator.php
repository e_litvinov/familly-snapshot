<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use NKO\OrderBundle\Resolver\FileFormatResolver;

class EntityWithFileValidator extends ConstraintValidator
{
    const WEIGHT = [
        'doc' => 1,
        'excel' => 2,
        'pdf' => 3,
    ];

    const MESSAGES = [
        1 => 'Please upload a valid DOC file',
        2 => 'Please upload a valid excel file',
        3 => 'Please upload a valid PDF file',
        30 => 'doc+excel',
        40 => 'Please upload a valid PDF or Doc file',
        50 => 'excel+pdf',
        60 => 'Please upload a valid PDF or Excel or Doc file',
        100 => 'please, upload file'
    ];
    const ONE_DOT_MESSAGES = 'In name of file must be only one dot';

    private $translator;
    private $fileFormatResolver;

    public function __construct($translator, FileFormatResolver $fileFormatResolver)
    {
        $this->translator = $translator;
        $this->fileFormatResolver = $fileFormatResolver;
    }

    public function validate($value, Constraint $constraint)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $relatedObject = null;
        if ($constraint->relatedFields) {
            foreach ($constraint->relatedFields as $field) {
                $relatedObject = $accessor->getValue($value, $field) ?: $relatedObject;
            }

            if (!$relatedObject) {
                return;
            }
        }

        foreach ($constraint->fileNames as $fileName) {
            $file = $accessor->getValue($value, $fileName);
            $fileHelp = $accessor->getValue($value, $fileName.'Help');

            //проверка загружен ли файл
            if ($constraint->notNull && (!$file || $file === "1") && !$fileHelp) {
                 $this->context->buildViolation($this->translator->trans($constraint->notNullMessage))
                    ->atPath($fileName)->addViolation();
                continue;
            }

            //если поле файла необязательное || файл уже был провалидирован || допускаются любые типы файла
            if (!$file || is_string($file) || !$constraint->extensions) {
                continue;
            }

            if (!$file->getClientOriginalName() || substr_count($file->getClientOriginalName(), '.') != 1) {
                $accessor->setValue($value, $fileName, null);
                $this->context->buildViolation($this->translator->trans(self::ONE_DOT_MESSAGES))->atPath($fileName)->addViolation();
                continue;
            }

            if (!$this->fileFormatResolver->check($file, $constraint->extensions)) {
                $message = $constraint->message ?: $this->getMessage($constraint->extensions);

                $accessor->setValue($value, $fileName, null);
                $this->context->buildViolation($this->translator->trans($message))->atPath($fileName)->addViolation();
                continue;
            }
        }
    }

    public function getMessage($extensions)
    {
        $weight = 0;
        if (is_array($extensions)) {
            foreach ($extensions as $extension) {
                if (key_exists($extension, self::WEIGHT)) {
                    $weight += self::WEIGHT[$extension];
                }
            }

            if (count($extensions) > 1) {
                $weight *= 10;
            }
        }

        return key_exists($weight, self::MESSAGES) ? self::MESSAGES[$weight] : self::MESSAGES[100];
    }
}
