<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 14.02.19
 * Time: 12:07
 */

namespace NKO\OrderBundle\Validator\Constraints;

use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MonitoringResultPeriodValidator extends ConstraintValidator
{
    const EXTRA_INDEXES = [
        6 => [
            1,
            2
        ],
        7 => [
            1,
            2
        ],
        11 => [
            2,
            3,
            4,
        ]
    ];

    /** @var Constraint */
    protected $constraint;

    /** @var PropertyAccessor */
    private $accessor;

    /**
     * @param mixed $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        $this->constraint = $constraint;
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $extraIndexes = $constraint->extraIndexes ?? self::EXTRA_INDEXES;

        foreach ($object as $keyRow => $monitoringResult) {
            /** @var MonitoringResult $monitoringResult*/
            $indicator = $monitoringResult->getIndicator();
            $parentIndicator = $indicator->getParent();
            $report = $monitoringResult->getReport();

            if (!$this->extraIndicator($extraIndexes, $indicator)) {
                $parentMonitoringResult = $report->getMonitoringResultByIndicator($parentIndicator);
                $report->getMonitoringResults()->indexOf($parentMonitoringResult);
                $this->compareMonitoringResult($keyRow, $parentMonitoringResult, $monitoringResult);
            }
        }
    }

    /**
     * @param int $keyRow
     * @param MonitoringResult|null $parentResult
     * @param MonitoringResult|null $result
     *
     * @return bool
     */
    protected function compareMonitoringResult($keyRow, MonitoringResult $parentResult = null, MonitoringResult $result = null)
    {
        if (!$parentResult || !$result) {
            return true;
        }

        $allValid = true;
        foreach ($parentResult->getPeriodResults() as $keyColumn => $parentPeriodResult) {
            $allValid = $this->setViolationToResult($keyRow, $keyColumn, $parentPeriodResult, $result->getPeriodResults()->get($keyColumn)) && $allValid;
        }

        return $allValid;
    }

    /**
     * @param $keyRow
     * @param $keyColumn
     * @param PeriodResult|null $parentResult
     * @param PeriodResult|null $result
     *
     * @return bool
     */
    protected function setViolationToResult($keyRow, $keyColumn, PeriodResult $parentResult = null, PeriodResult $result = null)
    {
        if (!$parentResult || !$result) {
            return true;
        }

        $isValid = true;

        if ($parentResult->getPeriod()) {
            $isValid = $isValid && $this->setViolationToAmount($keyRow, $keyColumn, $parentResult, $result, 'totalAmount');
        }
        if ($result->isFinalResult()) {
            $isValid = $isValid && $this->setViolationToAmount($keyRow, $keyColumn, $parentResult, $result, 'totalAmount');
        }
        return $isValid;
    }

    /**
     * @param int $keyRow
     * @param int $keyColumn
     * @param PeriodResult $parentResult
     * @param PeriodResult $result
     * @param string $fieldName
     *
     * @return bool
     */
    private function setViolationToAmount($keyRow, $keyColumn, PeriodResult $parentResult, PeriodResult $result, $fieldName)
    {
        if (!$this->isValidPeriodResult($parentResult, $result, $fieldName)) {
            $this->context
                ->buildViolation('Значение не может быть больше, чем у главного показателя')
                ->atPath($this->generatePath($keyRow, $keyColumn, $fieldName))->addViolation();

            return false;
        }

        return true;
    }

    /**
     * @param string $fieldName
     * @param PeriodResult $parentResult
     * @param PeriodResult $result
     *
     * @return bool
     */
    protected function isValidPeriodResult(PeriodResult $parentResult, PeriodResult $result, $fieldName)
    {
        $accessor = $this->accessor;

        return $accessor->getValue($result, $fieldName) <= $accessor->getValue($parentResult, $fieldName);
    }

    /**
     * @param array $extraIndexes
     * @param Indicator $indicator |null
     *
     * @return bool
     */
    private function extraIndicator(array $extraIndexes, Indicator $indicator = null)
    {
        $parentIndicator = $indicator->getParent();

        $parentIndex = $parentIndicator ? $parentIndicator->getIndexNumber() : null;
        $index = $indicator->getIndexNumber();

        return ($parentIndex && key_exists($parentIndex, $extraIndexes) && in_array($index, $extraIndexes[$parentIndex]));
    }

    /**
     * @param int $keyRow
     * @param int $keyColumn
     * @param string $fieldName
     *
     * @return string
     */
    private function generatePath($keyRow, $keyColumn, $fieldName)
    {
        return sprintf('[%d].periodResults[%d].%s', $keyRow, $keyColumn, $fieldName);
    }
}
