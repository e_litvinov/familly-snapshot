<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MarkRangeValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function validate($object, Constraint $constraint)
    {
        if (!$this->isFirstMarkNull($object)
            && ($object->getMark() > $object->getQuestionMark()->getRangeMark())
        ) {
                $this->context->buildViolation($constraint->messageInvalidMark)
                ->atPath('mark')
                ->addViolation();
        }
        if (!$this->isFirstMarkNull($object) && ( $object->getMark() < 0)) {
            $this->context->buildViolation($constraint->messageNegativeMark)
                ->atPath('mark')
                ->addViolation();
        }
        if (!$this->isFirstMarkNull($object) && $object->getMark() === null) {
            $this->context->buildViolation($constraint->messageNotBlank)
                ->atPath('mark')
                ->addViolation();
        }
        if ((!$this->isFirstMarkNull($object)
            && ($object->getRationale() === null || strlen($object->getRationale()) < 100))
        ) {
            $this->context->buildViolation($constraint->messageRationaleNotBlank)
                ->atPath('rationale')
                ->addViolation();
        }
        if ($this->isFirstMarkNull($object)
            && $object->getRationale() != null
            && strlen($object->getRationale()) < 100
        ) {
                $this->context->buildViolation($constraint->messageRationaleNotBlank)
                    ->atPath('rationale')
                    ->addViolation();
        }
    }

    private function isFirstMarkNull($object)
    {
        $markList = $object->getMarkList();
        if ($markList == null) {
            return false;
        }
        if ($markList->getMarks()[0]->getMark() === 0.0
            && $markList->getApplication()->getCompetition()->getIsMarkListReset()
        ) {
            return true;
        }

        return false;
    }
}
