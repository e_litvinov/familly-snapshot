<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MarkRange extends Constraint
{
    public $messageInvalidMark = 'Mark is higher than the maximum score for this criterion';
    public $messageNotBlank = "Please, set mark";
    public $messageRationaleNotBlank = "Please justify in detail its decision (at least 100 characters) .";
    public $messageNegativeMark = "Value unacceptable";
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}