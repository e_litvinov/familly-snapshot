<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CollectionDecoratorValidationValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /** @var CollectionDecoratorValidation $constraint */
        if ($constraint->isProperty) {
            $this->validateProperties($value, $constraint);
        } else {
            $this->validateEntities($value, $constraint);
        }
    }

    private function validateEntities($value, CollectionDecoratorValidation $constraint)
    {
        foreach ($value as $key => $item) {
            $this->validateCollectionItemProperty(
                new $constraint->validatorClass($constraint->validatorOptions),
                '['. $key. '].'. $constraint->propertyName,
                $item
            );
        }
    }

    private function validateProperties($value, CollectionDecoratorValidation $constraint)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($value as $key => $item) {
            $this->validateCollectionItemProperty(
                new $constraint->validatorClass($constraint->validatorOptions),
                '['. $key. '].'. $constraint->propertyName,
                $accessor->getValue($item, $constraint->propertyName)
            );
        }
    }

    private function validateCollectionItemProperty(Constraint $constraint, $propertyName, $value)
    {
        foreach ($this->context->getValidator()->validate(
            $value,
            $constraint
        ) as $violation) {
            $this->context->buildViolation($violation->getMessage())->atPath($propertyName)->addViolation();
        }
    }
}
