<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/9/17
 * Time: 3:37 PM
 */

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PdfExcelExtension extends Constraint
{
    public $message = 'Please upload a valid PDF or Excel file';
}