<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EntityWithFile extends Constraint
{
    const EXTENSIONS = 'extensions';
    const FILE_NAMES = 'fileNames';
    const MESSAGE = 'message';

    /**
     * List of file extension that can be
     * @var array
     */
    public $extensions;

    /**
     * List of file to check
     * @var array
     */
    public $fileNames;

    /**
     * Message of incorrect extension
     * @var string
     */
    public $message;

    /**
     * Need to check existing of file.
     * @var bool
     */
    public $notNull;

    public $relatedFields;

    /**
     * Message of missing file.
     * Need with notNull==true
     * @var bool
     */
    public $notNullMessage;

    public function __construct($options = null)
    {
        $this->notNullMessage = 'file not found';
        $this->notNull = false;
        parent::__construct($options);
    }

    public function getRequiredOptions()
    {
        return [
            self::FILE_NAMES,
            self::EXTENSIONS,
        ];
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
