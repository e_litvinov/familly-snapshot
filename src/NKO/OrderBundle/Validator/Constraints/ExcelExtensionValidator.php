<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExcelExtensionValidator extends ConstraintValidator
{
    const EXT = [
        'xls',
        'xlsx'
    ];

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function validate($value, Constraint $constraint)
    {
        if(!$value) {
            return;
        }
        $extension = pathinfo($value->getClientOriginalName())['extension'];
        if(!in_array($extension, self::EXT))
        {
            $this->context->buildViolation($this->container->get('translator')->trans($constraint->message))
                ->addViolation();
        }
    }
}