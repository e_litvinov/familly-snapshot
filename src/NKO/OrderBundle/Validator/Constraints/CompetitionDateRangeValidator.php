<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CompetitionDateRangeValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function validate($object, Constraint $constraint)
    {
        if($object->getStartDate() >= $object->getFinishDate()) {
            $this->getViolation('start_date',$constraint->messageFailedStartDate);
        }
    }

    public function getViolation($field_name, $message)
    {
        $this->context->buildViolation($this->container->get('translator')->trans($message))
            ->atPath($field_name)
            ->addViolation();
    }
}