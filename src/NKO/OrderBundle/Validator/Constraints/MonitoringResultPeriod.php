<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 14.02.19
 * Time: 13:06
 */

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MonitoringResultPeriod extends Constraint
{
    public $extraIndexes = null;

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
