<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 5.7.18
 * Time: 18.24
 */

namespace NKO\OrderBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\InvalidOptionsException;

/**
 * @Annotation
 */

class PdfXlsDocExtension extends Constraint
{
    public $message = 'Please upload a valid PDF or Excel or Doc file';
}