<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/9/17
 * Time: 3:38 PM
 */

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;

class FileValidator extends \Symfony\Component\Validator\Constraints\FileValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value instanceof UploadedFile) {
            parent::validate($value, $constraint);
        }
    }

}