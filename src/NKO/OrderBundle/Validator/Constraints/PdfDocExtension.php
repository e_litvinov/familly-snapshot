<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.8.18
 * Time: 11.01
 */

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\InvalidOptionsException;

/**
 * @Annotation
 */

class PdfDocExtension extends Constraint
{
    public $message = 'Please upload a valid PDF or Doc file';
}
