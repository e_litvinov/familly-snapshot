<?php

namespace NKO\OrderBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Feedback extends Constraint
{
    /**
     * example of setting
     *
     *     inversedBy="practiceAnalyticResults",           - Название поля коллекции от которого зависит валидировать
     *                                                       или нет(елемент этой коллекции должен содержать поле"isFeedback")
     *     groups={"AnalyticReport-2019"}                  - groups
     *     nameOfMethod="defaultEffectiveness"             - Параметр от котрого зависит, как именно будет валидироваться Feedback в FeedbackValidator
     *     messages={                                      - Ассоциативный массив сообщений, которые выводятся вместо дефолного значения
     *                                                                                       при невалидном поле "поле" => "сообщение"
     *                 "comment"="nit null",
     *                 "practice"="not null"
     *               }
     *     defaultMessage="This value should be blank."    - Дефолтное сообщение о невалидном поле
     */
    const DEFAULT_METHOD = 'default';
    const DEFAULT_METHOD_EFFECTIVENESS = 'defaultEffectiveness';

    /**
     *  Name of collection's field contain 'isFeedback"
     * @var string
     */
    public $inversedBy;

    /**
     *
     * @var array
     */
    public $messages;

    /**
     *
     * @var string
     */
    public $defaultMessage;

    /**
     *
     * @var string
     */
    public $nameOfMethod;

    public function __construct($options = null)
    {
        $this->defaultMessage = 'This value should not be blank.';
        $this->nameOfMethod = self::DEFAULT_METHOD;
        $this->messages = [];
        parent::__construct($options);
    }

    public function getRequiredOptions()
    {
        return [
            'inversedBy',
        ];
    }

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
