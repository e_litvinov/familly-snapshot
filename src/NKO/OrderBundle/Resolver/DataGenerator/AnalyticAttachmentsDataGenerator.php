<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.03.19
 * Time: 16:17
 */

namespace NKO\OrderBundle\Resolver\DataGenerator;

use Doctrine\ORM\PersistentCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AnalyticAttachmentsDataGenerator implements DataGeneratorInterface
{
    private $parameters;
    private $report;

    /**
     * @return mixed
     */
    public function generateData()
    {
        $attachments = [];

        foreach ($this->parameters as $parameter) {
            $attachments = $this->fillAttachments($this->report, $attachments, $parameter['tableName'], $parameter['collectionName'], $parameter['title'], $parameter['fileField']);
        }

        return $attachments;
    }

    private function fillAttachments($report, $attachments, $tableName, $collectionName, $title, $fileField)
    {
        $attachments[$tableName] = [];
        $accessor = PropertyAccess::createPropertyAccessor();
        $collection = $accessor->getValue($report, $collectionName);
        $i = 0;
        $j = 0;

        if ($collection) {
            foreach ($collection as $item) {
                $attachments[$tableName][$j]['title'] = (string)$accessor->getValue($item, $title);
                $files = $accessor->getValue($item, $fileField);
                if ($files instanceof PersistentCollection) {
                    foreach ($files as $file) {
                        $attachments[$tableName][$j]['files'][$i]['entity'] = get_class($file);
                        $attachments[$tableName][$j]['files'][$i]['object'] = $file;
                        $attachments[$tableName][$j]['files'][$i]['id'] = $file->getId();
                        $attachments[$tableName][$j]['files'][$i++]['field'] = 'file';
                    }
                } else {
                    $attachments[$tableName][$j]['files'][$i]['object'] = $item;
                    $attachments[$tableName][$j]['files'][$i]['entity'] = get_class($item);
                    $attachments[$tableName][$j]['files'][$i]['id'] = $item->getId();
                    $attachments[$tableName][$j]['files'][$i++]['field'] = $fileField;
                }
                $j++;
            }
        }

        return $attachments;
    }

    public function setData($data)
    {
        $this->parameters = $data['parameters'];
        $this->report = $data['report'];
    }
}
