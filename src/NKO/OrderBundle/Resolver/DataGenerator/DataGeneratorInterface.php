<?php

namespace NKO\OrderBundle\Resolver\DataGenerator;

interface DataGeneratorInterface
{
    public function generateData();

    public function setData($data);
}
