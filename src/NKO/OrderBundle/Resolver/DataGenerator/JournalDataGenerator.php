<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 07.02.19
 * Time: 16:05
 */

namespace NKO\OrderBundle\Resolver\DataGenerator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;

class JournalDataGenerator implements DataGeneratorInterface
{
    const HEAD = [
        'Дата подачи',
        'Полное название организации',
        'Название проекта',
        'Регион',
        'ОГРН'
    ];

    /** @var EntityManager */
    private $em;

    private $histories;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getHead()
    {
        return self::HEAD;
    }

    public function setData($histories)
    {
        $this->histories = $histories;
    }

    public function generateData()
    {
        $data = [];

        foreach ($this->histories as $history) {
            $data[] = $this->getDataFromHistory($history);
        }

        return $data;
    }

    private function getDataFromHistory(ApplicationHistory $history)
    {
        /** @var BaseApplication $application */
        $application = unserialize($history->getData());
        $author = $application->getAuthor();

        $newData = [];


        $author = $this->em->find(get_class($author), $author->getId());

        $newData[] = $history->getCreatedAt()->format('d-m-Y');
        $newData[] = $author->getNkoName();
        $newData[] = $application->getProjectName();
        $newData[] = $history->getLegalRegion();
        $newData[] = $author->getPsrn();

        return $newData;
    }
}
