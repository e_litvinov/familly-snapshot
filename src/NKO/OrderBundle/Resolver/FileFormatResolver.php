<?php

namespace NKO\OrderBundle\Resolver;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileFormatResolver
{
    const MIME_TYPES = [
        'excel' => [
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ],
        'doc' => [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ],
        'pdf' => [
            'application/pdf'
        ]
    ];

    const BASE_MIME_TYPES = [
        'application/vnd.ms-office',
        'application/CDFV2-encrypted',
        'application/zip'
    ];

    const EXTENSIONS = [
        'doc' => [
            'doc', 'docx'
        ],
        'excel' => [
            'xls', 'xlsx'
        ]
    ];

    public function check(UploadedFile $file, $formats)
    {
        $currentMimeType = $file->getMimeType();

        if (!$currentMimeType) {
            return null;
        }

        //проверяем mime type, которые точно определены для форматов
        $necessaryMimeTypes = $this->getMimeTypes($formats, self::MIME_TYPES);
        $correctMimeType = in_array($currentMimeType, $necessaryMimeTypes);

        if ($correctMimeType) {
            return true;
        }

        //проверяем mime type, которые общие, с дополнительной проверкой extension, содержащегося в названии файла
        $extension = pathinfo($file->getClientOriginalName())['extension'];

        if (!$extension) {
            return null;
        }
        
        $extension = strtolower($extension);
        $necessaryExtensions = $this->getMimeTypes($formats, self::EXTENSIONS);

        $correctMimeType = in_array($currentMimeType, self::BASE_MIME_TYPES);
        $correctExtension = in_array($extension, $necessaryExtensions);

        if ($correctMimeType && $correctExtension) {
            return true;
        }

        return null;
    }

    /**
     * Подборка форматов
     * @param $formats
     * @param $types
     * @return array
     */
    protected function getMimeTypes($formats, $definedTypes)
    {
        $extensions = [];
        foreach ($formats as $item) {
            if (key_exists($item, $definedTypes)) {
                $extensions = array_merge($extensions, $definedTypes[$item]);
            }
        }

        return $extensions;
    }
}
