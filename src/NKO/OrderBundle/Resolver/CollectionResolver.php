<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 02.03.17
 * Time: 12:05
 */

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class CollectionResolver
{
    private $em;

    private $container;

    private $propertyAccessor;

    private $entity;

    public function __construct(EntityManager $em, ContainerInterface $container, PropertyAccessor $propertyAccessor)
    {
        $this->em = $em;
        $this->container = $container;
        $this->propertyAccessor = $propertyAccessor;
    }

    public function fetchCollections($entity)
    {
        $this->entity = $entity;
        $metadata = $this->em->getClassMetadata(get_class($this->entity));
        $associations = $metadata->getAssociationMappings();

        foreach ($associations as $association) {
            $this->getCollection($association);
        }

        return $entity;
    }

    public function fetchCollectionByFieldName($entity, $fieldName)
    {
        $this->entity = $entity;
        $metadata = $this->em->getClassMetadata(get_class($entity));
        $associations = $metadata->getAssociationMappings();
        $this->getCollection($associations[$fieldName]);
        return $entity;
    }

    private function getCollection($association)
    {
        if ($association['type'] == ClassMetadata::ONE_TO_MANY) {
            $values = $this->getOneToManyCollections($association, $this->entity->getId());
            $this->propertyAccessor->setValue($this->entity, $association['fieldName'], $values);
        }
        elseif ($association['type'] == ClassMetadata::MANY_TO_MANY) {
            $app = $this->getManyToManyCollections($association, $this->entity->getId());
            if ($app) {
                $values = $this->propertyAccessor->getValue($app, $association['fieldName']);
                $this->propertyAccessor->setValue($this->entity, $association['fieldName'], $values);
            }
        }
    }

    private function getOneToManyCollections($assoc, $id)
    {
        return $this->em->createQueryBuilder()
            ->select('o')
            ->from($assoc['targetEntity'], 'o')
            ->leftJoin('o.' . $assoc['mappedBy'], 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    private function getManyToManyCollections($assoc, $id)
    {
        return $this->em->createQueryBuilder()
            ->select(['a', 'o'])
            ->from($assoc['sourceEntity'], 'a')
            ->join('a.'.$assoc['fieldName'], 'o', 'with', 'a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
