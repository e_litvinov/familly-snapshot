<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 02.03.17
 * Time: 12:05
 */

namespace NKO\OrderBundle\Resolver;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use NKO\OrderBundle\Entity\FinanceSpent;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\PropertyAccess\PropertyAccess;

class RelationsResolver
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function updateRelations($object, $dateTime)
    {
        $metadata = $this->em->getClassMetadata(get_class($object));

        $associations = $metadata->getAssociationMappings();

        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($associations as $association) {
            $targetItems= $accessor->getValue($object, $association['fieldName']);
            if ($association['type'] == ClassMetadata::MANY_TO_MANY) {
                if ($dateTime) {
                    $this->removeRelations($association['joinTable']['name'], $object->getId(), array_search('id', $association['relationToSourceKeyColumns']));
                } else {
                    if ($targetItems && $targetItems->first()) {
                        $this->insertRelations($association['joinTable']['name'], $targetItems, $object->getId());
                    }
                }
            } elseif ($association['type'] == ClassMetadata::ONE_TO_MANY) {
                $allItems = $this->em->getRepository($association['targetEntity'])->findBy([
                    $association['mappedBy'] => $object
                ]);

                foreach ($allItems as $item) {
                    foreach ($targetItems as $target) {
                        if ($item->getId() == $target->getId()) {
                            $item->setDeletedAt($dateTime);
                            $this->em->persist($item);
                        }
                    }
                }
            }
        }
        $this->em->flush();
    }

    private function removeRelations($table, $id, $relatedColumn)
    {
        $conn = $this->em->getConnection();
        $query = "DELETE FROM {$table} WHERE {$relatedColumn} = {$id};";
        $conn->prepare($query)->execute();
    }

    private function insertRelations($table, $items, $id)
    {
        $conn = $this->em->getConnection();
        $query = "";
        foreach ($items as $item) {
            $item_id = $item->getId();
            $query .= "INSERT INTO {$table} VALUES({$id}, {$item_id}); ";
        }
        $conn->prepare($query)->execute();
    }
}
