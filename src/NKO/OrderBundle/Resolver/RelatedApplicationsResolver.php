<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 13.04.17
 * Time: 13:41
 */

namespace NKO\OrderBundle\Resolver;

use Symfony\Component\DependencyInjection\ContainerInterface;

class RelatedApplicationsResolver
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getLinkedApplicationHistories($applications, $className)
    {
        $psrns = [];
        $em = $this->container->get('doctrine')->getManager();
        $competition = $em->getRepository('NKOOrderBundle:Competition')->findBy(['applicationClass' => $className], ['finish_date' => 'DESC'], 1);

        if (count($applications) == 1) {
            if (is_array($applications)) {
                $applications = reset($applications);
            }
            
            return $em->getRepository('NKOOrderBundle:ApplicationHistory')->findSpreadedByPsrns($applications->getAuthor()->getPsrn(), reset($competition)->getId());
        }

        foreach ($applications as $application) {
            $psrn = $application->getAuthor()->getPsrn();
            if ($psrn) {
                $psrns[] = $psrn;
            }
        }

        return $em->getRepository('NKOOrderBundle:ApplicationHistory')->findSpreadedByPsrns($psrns, reset($competition)->getId());
    }

    public function getRelatedApplications($applications, $className, $singleResult = null)
    {
        $applicationHistories = $this->getLinkedApplicationHistories($applications, $className);

        if (count($applications) == 1) {
            $value = unserialize(reset($applicationHistories)['data']);
            if ($singleResult) {
                return $value;
            }
            return [$value];
        }

        $psrns = array_column($applicationHistories, 'psrn');
        $briefApplicationsByPsrn = array_combine($psrns, $applicationHistories);

        $briefApplications = [];
        foreach ($applications as $application) {
            if(array_key_exists($application->getAuthor()->getPsrn(), $briefApplicationsByPsrn)) {
                $history = $briefApplicationsByPsrn[$application->getAuthor()->getPsrn()];
                $briefApplications[] = unserialize($history['data']);
            }
        }

        return $briefApplications;
    }
}