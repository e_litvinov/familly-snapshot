<?php

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Entity\Expense;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Register;

class FinanceResolver
{
    private $em;
    private $financeSpentResolver;

    public function __construct(EntityManager $em, FinanceSpentResolver $financeSpentResolver)
    {
        $this->em = $em;
        $this->financeSpentResolver = $financeSpentResolver;
    }

    public function create(BaseReport $object)
    {
        if (!$this->setRegister($object)) {
            return [
                'flash' => [
                    'type' => 'sonata_flash_error',
                    'message' => 'error_expense_type_not_found',
                ], 'redirectTo' => 'list',
            ];
        }

        $this->setFinanceSpent($object, $object->getReportTemplate());
        $this->financeSpentResolver->updateIncrementalCost($object);
    }

    public function isSentReport($object, $request)
    {
        $response = [];
        $reportForm = $object->getReportForm();

        $pastPeriods = $this->em->getRepository(PeriodReport::class)->findPastReportPeriods($object);

        $sentReport = true;
        if ($pastPeriods) {
            $sentReport = $this->em->getRepository(BaseReport::class)->findOneBy([
                'isSend' => true,
                'reportForm' => $reportForm,
                'psrn' => $object->getAuthor()->getPsrn(),
                'period' => array_pop($pastPeriods)
            ]);

            $session = $request->getSession();
            $session->set('sent_report', $sentReport ? true : null);

            if (!$sentReport) {
                $response['flash'] = [
                    'type' => 'sonata_flash_error',
                    'message' => 'Does_not_find_last_report',
                ];
            }
        }

        $response['parameters'] = ['sentReport' => $sentReport];

        return $response;
    }

    private function setRegister($object)
    {
        $reportForm = $object->getReportForm();
        $expenseTypes = $this->em->getRepository(ExpenseType::class)->findExpenseTypesByReportForm($reportForm);

        if (!$expenseTypes) {
            return false;
        }

        foreach ($expenseTypes as $expenseType) {
            $register = new Register();
            $register->addExpense(new Expense());
            $register->setExpenseType($expenseType);
            $object->addRegister($register);
            $register->setReport($object);
        }
        return true;
    }

    private function setFinanceSpent($report, $reportTemplate)
    {
        foreach ($reportTemplate->getFinanceSpent() as $finance) {
            $newFinance = clone $finance;
            $newFinance->setReportTemplate(null);
            $report->addFinanceSpent($newFinance);
        }
    }
}
