<?php

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseReport;
use Doctrine\ORM\Query\Expr\Join;

class FinanceSpentResolver
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function updateIncrementalCost($object)
    {
        $author = $object->getAuthor();
        $period = $object->getPeriod();
        $reportForm = $object->getReportForm();
        $repository = get_class($object);

        $reports = $this->em->createQueryBuilder()
            ->select('r')
            ->from($repository, 'r')
            ->innerJoin('r.period', 'p', Join::WITH, 'r.periodId = p.id')
            ->where('r.reportForm = :reportForm')
            ->andWhere('r.author = :author')
            ->andWhere('p.startDate < :currentPeriodDate')
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $reportForm,
                'author' => $author,
                'currentPeriodDate' => $period->getStartDate()
            ])
            ->getResult()
        ;

        if ($reports) {
            foreach ($reports as $report) {
                $this->fillIncrementalCosts($object, $report);
            }
        }
    }

    private function fillIncrementalCosts(BaseReport $currentReport, BaseReport $lastReport)
    {
        $currentFinances = $currentReport->getFinanceSpent();
        $lastFinances = $lastReport->getFinanceSpent();
        foreach ($currentFinances as $currentFinance) {
            $currentExpenseType = $currentFinance->getExpenseType();
            $finance = $lastFinances->filter(
                function ($object) use ($currentExpenseType) {
                    return ($object->getExpenseType() === $currentExpenseType);
                }
            )->first();

            $incrementalCosts = $currentFinance->getIncrementalCosts() + $finance->getPeriodCosts();
            $currentFinance->setIncrementalCosts(round($incrementalCosts, 2));
        }
    }
}
