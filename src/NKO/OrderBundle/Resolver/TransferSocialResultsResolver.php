<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 18.7.18
 * Time: 22.51
 */

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\TransferResult;
use NKO\OrderBundle\Traits\CollectionHandler;

class TransferSocialResultsResolver
{
    use CollectionHandler;

    private $entityManager;

    private  $relatedObject;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    public function createSocialResults($nameMethods, $object, $relatedObject)
    {
        $this->relatedObject = $relatedObject;
        list($nameCollection, $nameCollectionEtc, $fieldName) = $nameMethods;
        $relatedSocialResults = [];
        $relatedSocialResults = $this->getArraySocialResults ($nameCollection, $fieldName, $relatedSocialResults);
        $relatedSocialResults = $nameCollectionEtc ? $this->getArraySocialResults ($nameCollectionEtc, $fieldName, $relatedSocialResults) : $relatedSocialResults;
        $this->createTransferResult($relatedSocialResults, $nameCollection, $object);
    }

    public function updateSocialResults($nameMethods, $object, $relatedObject)
    {
        $this->relatedObject = $relatedObject;
        list($nameCollection, $nameCollectionEtc, $fieldName) = $nameMethods;
        $relatedSocialResults = [];
        $transferredSocialResults = [];

        $relatedSocialResults = $this->getArraySocialResults ($nameCollection, $fieldName, $relatedSocialResults);
        $relatedSocialResults = $nameCollectionEtc ? $this->getArraySocialResults ($nameCollectionEtc, $fieldName, $relatedSocialResults) : $relatedSocialResults;

        $transferredResultCollection = $this->entityManager->getRepository(TransferResult::class)
            ->findBy(['report' => $object->getId(), 'code' => $nameCollection]);
        $transferredSocialResults = $this->modifyToStringArray($transferredResultCollection, $transferredSocialResults);

        $newSocialResults = array_diff($relatedSocialResults, $transferredSocialResults);
        $removeSocialResults = array_diff($transferredSocialResults, $relatedSocialResults);

        $newSocialResults ? $this->createTransferResult($newSocialResults, $nameCollection, $object) : null;
        $removeSocialResults ? $this->removeTransferResult($transferredResultCollection, $removeSocialResults) : null;
    }

    private function createTransferResult (array $relatedSocialResults, $nameCollection, $object)
    {
        foreach ($relatedSocialResults as $value) {
            $transferResult = new TransferResult();
            $transferResult->setTitle($value);
            $transferResult->setCode($nameCollection);
            $transferResult->setReport($object);
            $this->entityManager->persist($transferResult);
        }
    }

    private function removeTransferResult ($transferredResultCollection, $removeSocialResults)
    {
        foreach ($transferredResultCollection as $result) {
            if (in_array($result->getTitle(), $removeSocialResults) ) {
                $this->entityManager->remove($result);
            }
        }
    }

    private function getArraySocialResults ($nameCollection, $fieldName, array $relatedSocialResults)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($accessor->getValue($this->relatedObject, $nameCollection) as $item) {
            $title = (string) $accessor->getValue($item, $fieldName);
            if (array_search($title, $relatedSocialResults) === false && $title && $title != "-") {
                $relatedSocialResults[] = $title;
            }
        }

        return $relatedSocialResults;
    }
}