<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 12.12.18
 * Time: 17.57
 */

namespace NKO\OrderBundle\Resolver;

use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\OrderBundle\Utils\ConstValues;

class SortedFixtureValuesResolver
{
    public static function sortResultsByType($results, $type, $fieldName)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $keysOrder = [];
        foreach ($results as $key => $value) {
            $keysOrder[$key] = $accessor->getValue($value, $fieldName)[$type];
        }

        if (in_array(ConstValues::AUTO, $keysOrder, true)) {
            return $results;
        }

        $success = asort($keysOrder);
        $keysOrder = array_flip($keysOrder);

        if (!$success) {
            return $results;
        }

        $newResults = [];

        foreach ($keysOrder as $keyResult) {
            $newResults[] = $results[$keyResult];
        }

        return $newResults;
    }
}
