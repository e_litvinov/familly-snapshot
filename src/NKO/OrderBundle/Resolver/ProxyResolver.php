<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 20.02.17
 * Time: 10:56
 */

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Proxy\Proxy;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ProxyResolver
{
    /**
     * @var Container
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    // if smth went wrong, try to clear entity manager before using resolveProxies method
    public function resolveProxies($object)
    {
        /**
         * @var EntityManager $em
         */
        $em = $this->container->get('doctrine')->getManager();

        $className = get_class($object);
        $query = $em->createQuery("select obj from {$className} obj where obj.id = :id")
            ->setParameter('id', $object->getId());
        $metadata = $em->getClassMetadata($className);
        $associations = $metadata->getAssociationMappings();

        /**
         * @var Query $query
         */
        foreach ($associations as $association) {
            $query = $query->setFetchMode($className, $association['fieldName'], ClassMetadata::FETCH_EAGER);
        }

        return $query->getOneOrNullResult();
    }
}