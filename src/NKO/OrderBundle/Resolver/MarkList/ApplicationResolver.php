<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;

class ApplicationResolver extends AbstractMarkListResolver
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:show_mark_application.html.twig';
    }
}
