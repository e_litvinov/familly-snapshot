<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;

class Farvater2018ApplicationResolver extends AbstractMarkListResolver
{
    /** @var RelatedCompetitionResolver  */
    private $resolver;

    public function __construct(EntityManager $em, RelatedCompetitionResolver $resolver)
    {
        $this->resolver = $resolver;

        parent::__construct($em);
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:Application/Farvater/Application2018/show_mark.html.twig';
    }

    public function generateArrayForTwig(MarkList $mark, BaseApplication $application)
    {
        $response = parent::generateArrayForTwig($mark, $application);

        $relatedApplicationHistory = $this->resolver->getApplicationHistory($application);
        $briefApplication = unserialize($relatedApplicationHistory->getData());

        $response['briefApplication'] = $briefApplication;

        return $response;
    }
}
