<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Resolver\RelatedApplicationsResolver;

class Farvater2017ApplicationResolver extends AbstractMarkListResolver
{
    /**
     * @var RelatedApplicationsResolver
     */
    private $resolver;

    public function __construct(EntityManager $em, RelatedApplicationsResolver $resolver)
    {
        $this->resolver = $resolver;

        parent::__construct($em);
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:show_mark_farvater_2017.html.twig';
    }

    public function generateArrayForTwig(MarkList $mark, BaseApplication $application)
    {
        $response = parent::generateArrayForTwig($mark, $application);

        $briefApplication = $this->resolver
            ->getRelatedApplications(
                $application,
                'NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication',
                true
            );

        $response['briefApplication'] = $briefApplication;

        return $response;
    }
}
