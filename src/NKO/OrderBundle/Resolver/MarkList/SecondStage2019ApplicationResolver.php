<?php

namespace NKO\OrderBundle\Resolver\MarkList;

class SecondStage2019ApplicationResolver extends AbstractMarkListResolver
{
    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:Application/KNS/SecondStage2019/show_mark.html.twig';
    }
}
