<?php

namespace NKO\OrderBundle\Resolver\MarkList;

class Verification2020ApplicationResolver extends AbstractMarkListResolver
{
    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:Application/Verification/Application2020/show_mark.html.twig';
    }
}
