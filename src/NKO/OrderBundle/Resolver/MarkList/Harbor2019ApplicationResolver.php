<?php

namespace NKO\OrderBundle\Resolver\MarkList;

class Harbor2019ApplicationResolver extends AbstractMarkListResolver
{
    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:Application/Harbor/Application2019/show_mark.html.twig';
    }
}
