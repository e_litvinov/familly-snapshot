<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;

class FarvaterApplicationResolver extends AbstractMarkListResolver
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:show_mark_sf_2016.html.twig';
    }
}
