<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;

class KNS2019ApplicationResolver extends AbstractMarkListResolver
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:Application/KNS/Application2019/show_mark.html.twig';
    }
}
