<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\FinalDecision;
use NKO\OrderBundle\Entity\MarkList;

abstract class AbstractMarkListResolver
{
    /** @var EntityManager */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    abstract public function getTemplate();

    public function generateArrayForTwig(MarkList $mark, BaseApplication $application)
    {
        $expert = $mark->getExpert();
        $finalDecisionId = $mark->getFinalDecision();
        $finalDecision = $finalDecisionId ?
            $this->em->getRepository(FinalDecision::class)->find($finalDecisionId) :
            null;

        return [
            'mark' => $mark,
            'application' => $application,
            'number' => $mark->getApplication()->getNumber(),
            'expert' => $expert,
            'final_decision' => $finalDecision,
            'action' => 'show'
        ];
    }
}
