<?php

namespace NKO\OrderBundle\Resolver\MarkList;

use Doctrine\ORM\EntityManager;

class BriefApplication2018Resolver extends AbstractMarkListResolver
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:Application/Farvater/BriefApplication2018/show_mark.html.twig';
    }
}
