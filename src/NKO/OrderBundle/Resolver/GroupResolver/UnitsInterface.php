<?php

namespace NKO\OrderBundle\Resolver\GroupResolver;

interface UnitsInterface
{
    public function generate($data = null);
}
