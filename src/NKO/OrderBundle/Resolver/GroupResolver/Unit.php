<?php

namespace NKO\OrderBundle\Resolver\GroupResolver;

use NKO\OrderBundle\Resolver\GroupResolver\UnitsInterface;

class Unit implements UnitsInterface
{
    protected $unit;
    
    public function __construct($object = null)
    {
        $this->unit = $object;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function generate($data = null)
    {
        return $this->unit;
    }
}

