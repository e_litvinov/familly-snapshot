<?php

namespace NKO\OrderBundle\Resolver\GroupResolver\Generators;

use Doctrine\ORM\Query;
use NKO\OrderBundle\Resolver\GroupResolver\Groups\BaseGroup;
use NKO\OrderBundle\Resolver\GroupResolver\UnitsInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

abstract class BaseUnitsGenerator implements UnitsInterface
{
    /**
     * @var string
     * поле по которому формируется структура
     */
    protected $fieldName;

    /**
     * Генерация структуры
     * @return BaseGroup
     */
    public function generate($data = null)
    {
        $this->fieldName = $this->setField();
        $collection = $this->checkResultsQuery($data['query']);
        $parentIds = $this->getParentIds($collection);
        $baseGroup = $this->generateTree($collection, $parentIds, $this->getGroup());

        //применяем изменения порядка элементов к стурктуре
        $this->sort($baseGroup, $data);

        return $baseGroup;
    }

    /**
     * Поиск нужной Group по всему дереву категорий
     * @return null
     */
    public function walkTree($baseGroup, $parent)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $collection = $baseGroup->getUnits();
        foreach ($collection as $item) {

            $unit = $item->getUnit();
            $unit = is_int($parent) ? $accessor->getValue($unit, $this->fieldName) : $unit;
            if ($unit === $parent) {
                return $item;
            }

            if ($item instanceof BaseGroup) {
                $result = $this->walkTree($item, $parent);
                if ($result) {
                    return $result;
                }
            }
        }
        return null;
    }

    /**
     * параметры из админ-класса
     * sequence - одиночная последовательность
     * nestedSequence - вложенная
    */
    public function sort(BaseGroup $baseGroup, $item)
    {
        if (key_exists('sequence', $item)) {
            $baseGroup->sort($item['sequence']);
        }

        if (key_exists('nestedSequence', $item)) {
            foreach ($item['nestedSequence'] as $key => $value) {
                $node = $this->walkTree($baseGroup, $key);

                $node->sort($value);
            }
        }
    }

    public function checkResultsQuery($query)
    {
        if ($query instanceof Query) {
            $query = $query->getResult();
        }

        return $query;
    }

    /**
     * Для каждого типа генератора указывается объект-группа
     */
    abstract protected function getGroup($item = null);

    /**
     * Вернуть название поля, по которому формируется структура
     */
    abstract protected function setField();
}
