<?php

namespace NKO\OrderBundle\Resolver\GroupResolver\Generators;

use NKO\OrderBundle\Resolver\GroupResolver\Unit;
use NKO\OrderBundle\Resolver\GroupResolver\Groups\ExpertMarkGroup;

class ExpertMarkUnitsGenerator extends BaseUnitsGenerator
{
    public function generateTree($collection, $parentIds, $baseGroup)
    {
        foreach ($parentIds as $item) {
            $baseGroup->addUnit($this->getGroup($item));
        }
        
        foreach ($collection as $item) {
            $node = $this->walkTree($baseGroup, $item->getCriteria());
            $node->addUnit(new Unit($item));
        }

        return $baseGroup;
    }

    public function getParentIds($collection)
    {
        $uniqueCriteria = [];
        foreach ($collection as $item) {
            if (!in_array($item->getCriteria(), $uniqueCriteria)) {
                $uniqueCriteria[] = $item->getCriteria();
            }
        }
        return $uniqueCriteria;
    }

    protected function getGroup($item = null)
    {
        return new ExpertMarkGroup($item);
    }

    protected function setField()
    {
        return 'id';
    }
}
