<?php

namespace NKO\OrderBundle\Resolver\GroupResolver\Generators;

use NKO\OrderBundle\Resolver\GroupResolver\Unit;
use NKO\OrderBundle\Resolver\GroupResolver\Groups\TrainingGroundGroup;

class TrainingGroundUnitsGenerator extends BaseUnitsGenerator
{
    public function generateTree($collection, $parentIds, $baseGroup)
    {
        foreach ($collection as $item) {

            //Для обычного списка, без категорий
            if (!$parentIds) {
                $baseGroup->addUnit(new Unit($item));
                continue;
            }

            //Для добавления категорий всех уровней
            if (in_array($item->getStaticId(), $parentIds)) {
                $node = $baseGroup;
                if ($item->getParent()) {
                    $node = $this->walkTree($baseGroup, $item->getParent());
                }
                $node->addUnit($this->getGroup($item));
                continue;
            }

            //Добавляем обычные пункты к категориям
            $node = $this->walkTree($baseGroup, $item->getParent());
            $node->addUnit(new Unit($item));
        }

        return $baseGroup;
    }

    /**
     * получаем id всех РОДИТЕЛЬСКИХ категорий
     * @return array
     */
    public function getParentIds($collection)
    {
        $ids = [];
        $isNested = (new \ReflectionClass(reset($collection)))->hasMethod('getParent');

        if (!$isNested) {
            return [];
        }

        foreach ($collection as $item) {
            $parent = $item->getParent();
            $id = $parent ? $parent->getStaticId() : null;
            if ($id && !in_array($id, $ids)) {
                array_push($ids, $id);
            }
        }
        return $ids;
    }

    protected function getGroup($item = null)
    {
        return new TrainingGroundGroup($item);
    }

    protected function setField()
    {
        return 'staticId';
    }
}
