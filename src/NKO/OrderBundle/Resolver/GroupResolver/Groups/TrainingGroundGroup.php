<?php

namespace NKO\OrderBundle\Resolver\GroupResolver\Groups;

use NKO\OrderBundle\Resolver\GroupResolver\Unit;

class TrainingGroundGroup extends BaseGroup
{
    /**
     * получаем из структуры один список, содержащий объекты в верном порядке
     * @return array|null
     */
    public function generate($data = null)
    {
        $units = [];

        if ($this->unit) {
            $units[] = $this->unit;
        }

        foreach ($this->units as $item) {
            $data = $item->generate();

            if (is_array($data)) {
                $units = array_merge($units, $data);
            } else {
                array_push($units, $data);
            }
        }
        return $units;
    }

    protected function setField()
    {
        return 'staticId';
    }
}
