<?php

namespace NKO\OrderBundle\Resolver\GroupResolver\Groups;

class ExpertMarkGroup extends BaseGroup
{
    public function generate($data = null)
    {
        $units = [];

        if ($this->unit) {
            $units[$this->unit->getId()] = [];
        }

        foreach ($this->units as $item) {
            $data = $item->generate();

            if (is_array($data)) {
                $units += $data;
            } else {
                $key = array_keys($units)[0];
                $units[$key][] = $data->getId();
            }
        }
        return $units;
    }

    protected function setField()
    {
        return 'id';
    }
}
