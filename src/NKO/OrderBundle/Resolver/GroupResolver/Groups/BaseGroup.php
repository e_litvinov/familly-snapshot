<?php

namespace NKO\OrderBundle\Resolver\GroupResolver\Groups;

use NKO\OrderBundle\Resolver\GroupResolver\Unit;
use Symfony\Component\PropertyAccess\PropertyAccess;

abstract class BaseGroup extends Unit
{
    protected $units = [];
    protected $fieldName;

    public function addUnit(Unit $unit)
    {
        $this->fieldName = $this->setField();
        $accessor = PropertyAccess::createPropertyAccessor();
        $id = $accessor->getValue($unit->getUnit(), $this->fieldName);
        $this->units[$id] = $unit;
    }

    // Пока на реальной структуре не протестил, работает или нет? :(
    public function removeUnit(Unit $unit)
    {
        $this->units = array_udiff(
            $this->units,
            [$unit],
            function($a, $b) {
                return ($a === $b) ? 0 : 1;
            })
        ;
    }

    public function getUnits()
    {
        return $this->units?: [];
    }

    /**
     * умная сортировка структуры!
     * указывать:
     *  - либо грани
     *  - либо от начала до грани/от грани до конца
     *  - либо всю последовательность полностью
     */
    public function sort($sequence)
    {
        if (!count($sequence)) {
            return;
        }

        $start = reset($sequence);
        $end = end($sequence);
        $sequenceFlip = array_flip($sequence);

        $currentKeys = array_keys($this->units);
        $currentStart = reset($currentKeys);
        $currentEnd = end($currentKeys);

        //начальная грань
        if (key_exists($currentStart, $sequenceFlip) && $currentStart !== $start) {
            $baseSequence = $sequenceFlip;
        } else {
            $curPosition = array_search($start, $currentKeys);
            $baseSequence = array_slice($this->units, 0, $curPosition, true) + $sequenceFlip;
        }

        //конечная грань
        if ($currentEnd !== $end) {
            $curPosition = array_search($end, $currentKeys);
            $baseSequence = $baseSequence + array_slice($this->units, $curPosition, count($this->units)-$curPosition, true);
        }

        //совокупность
        $this->units = array_replace($baseSequence, $this->units);
    }

    /**
     * Вернуть название поля, по которому формируется структура
     */
    abstract protected function setField();
}
