<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/12/18
 * Time: 2:55 AM
 */

namespace NKO\OrderBundle\Resolver;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as BriefApplication2016;
use NKO\OrderBundle\Entity\Farvater\FarvaterApplication as Farvater2016;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication as BriefApplication2017;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017;
use NKO\OrderBundle\Entity\Application as KNS2016;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class RelatedCompetitionResolver implements ContainerAwareInterface
{
    const PRIORITY_DIRECTION_FIELD_NAME = 'priorityDirection';

    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var PropertyAccessor $propertyAccessor
     */
    private $propertyAccessor;

    private $application;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function setPropertyAccessor(PropertyAccessor $propertyAccessor = null)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    public function getApplicationHistory($followingApplication, $isSpread = null)
    {
        $em = $this->container->get('doctrine')->getManager();

        $this->application = $followingApplication;
        $competitionId = $this->application->getCompetition();
        $relatedCompetition = $em->getRepository(Competition::class)->find($competitionId)->getRelatedCompetition();

        if ($isSpread) {
            $applicationHistory = $em
                ->getRepository(ApplicationHistory::class)
                ->findSpreadByAuthor($this->application->getAuthor(), $relatedCompetition->getId());
        } else {
            $applicationHistory = $em
                ->getRepository(ApplicationHistory::class)
                ->findSendByAuthor($this->application->getAuthor(), $relatedCompetition->getId());
        }

        if (!$applicationHistory) {
            return null;
        }

        return $applicationHistory;
    }

    public function getPriorityDirection(ContinuationApplication $application)
    {
        if (!$application) {
            return false;
        }

        $relatedCompetition = $application->getCompetition()->getRelatedCompetition();

        if ($relatedCompetition->getApplicationClass() != Farvater2017::class) {
            return false;
        }

        $doctrine = $this->container->get('doctrine');
        $competition = $doctrine->getRepository(Competition::class)->findOneBy(['applicationClass' => BriefApplication2017::class]);
        $applicationHistory = $doctrine->getRepository(ApplicationHistory::class)->findSendByAuthor($application->getAuthor(), $competition->getId());
        $unserializedApplication = unserialize($applicationHistory->getData());

        if ($this->propertyAccessor->isReadable($unserializedApplication, self::PRIORITY_DIRECTION_FIELD_NAME)) {
            return $this->propertyAccessor->getValue($unserializedApplication, self::PRIORITY_DIRECTION_FIELD_NAME);
        }
    }
}
