<?php

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\ReportHistory;

class StatisticResolver
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getPeriodsByConfines($reportForm, $start, $finish)
    {
        $periods = $this->em->getRepository(PeriodReport::class)->findReportPeriods($reportForm);
        $currentPeriods = array_filter($periods, function ($period) use ($start, $finish) {
            return $start->getStartDate() <= $period->getStartDate() && $period->getStartDate() <= $finish->getStartDate();
        });

        return $currentPeriods;
    }

    public function checkCorrectConfines($start, $finish)
    {
        return (($start->getStartDate()) <= ($finish->getStartDate()));
    }

    public function getConfines($start, $finish)
    {
        $query = $this->em->getRepository(PeriodReport::class);
        $start = $query->find($start);
        $finish = $query->find($finish);

        return [$start, $finish];
    }

    public function getReportsByReportForm($reportForm)
    {
        return $this->em->getRepository(ReportHistory::class)
            ->findBy(['reportForm' => $reportForm, 'isSend' => true]);
    }
}
