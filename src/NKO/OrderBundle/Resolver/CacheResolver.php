<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 29.10.18
 * Time: 15.01
 */

namespace NKO\OrderBundle\Resolver;

use Doctrine\ORM\EntityManager;
use Psr\Log\LogLevel;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CacheResolver
{
    const DEFAULT_TTL = 1800;

    /** @var RedisAdapter $cache */
    private $cache;

    /** @var EntityManager $cache */
    private $em;

    /** @var Logger $cache */
    private $logger;

    /** @var string $cache */
    private $userName;

    /** @var string $cache */
    private $cacheKey;

    /** @var CacheItem $cachedItem */
    private $cachedItem;

    /** @var int $id*/
    private $id;

    /** @var int $secondsLifeDuration*/
    protected $secondsLifeDuration;

    public function __construct(RedisAdapter $cache, TokenStorageInterface $token, EntityManager $em, Logger $logger)
    {
        $this->cache = $cache;
        $this->userName = $token->getToken()->getUser()->getId();
        $this->em = $em;
        $this->logger = $logger;
        $this->secondsLifeDuration = self::DEFAULT_TTL;
    }
    /**
     * @param AdminInterface $admin        admin class of object
     * @param int            $id           id of object
     * @param int            $userId       id of user
     *
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function generateItem(AdminInterface $admin, $id, $userId = null)
    {
        $this->id = $id;
        $this->cacheKey = implode(' ', [$id, $admin->getCode(), $userId ?: $this->userName]);
        $this->cachedItem = $this->cache->getItem($this->cacheKey);
    }

    /**
     * @param AdminInterface $admin
     *
     * @return Mixed
     */
    private function getObject(AdminInterface $admin)
    {
        $object = null;
        if ($this->cachedItem->isHit()) {
            $newObject = $this->cachedItem->get();
            $object = $newObject;                           //Или это
        }

        return $object;
    }

    public function save($object)
    {
        if (!$this->cachedItem) {
            throw new \Exception("Generate item first");
        }

        $this->cachedItem->set($object);
        $this->cachedItem->expiresAfter($this->secondsLifeDuration);
        $this->cache->save($this->cachedItem);
    }

    public function delete()
    {
        if (!$this->cachedItem) {
            return;
        }

        $this->cache->deleteItem($this->cacheKey);
    }

    /**
     * @param AdminInterface $admin
     * @param int $id
     *
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @return Mixed
     */
    public function getAdminObject(AdminInterface $admin, $id)
    {
        $this->generateItem($admin, $id);

        return $this->getObject($admin);
    }

    public function logError($message = '')
    {

        if ($this->cacheKey) {
            $logMessage = 'CACHED FORM with key: '.  $this->cacheKey. ' — '. $message;
            $this->logger->log(LogLevel::WARNING, $logMessage);
        }
    }

    /**
     * @param $seconds int
     *
     * @return CacheResolver
     */

    public function setLifeDuration($seconds = self::DEFAULT_TTL)
    {
        $this->secondsLifeDuration = $seconds;

        return $this;
    }
}
