<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/15/18
 * Time: 3:45 PM
 */

namespace NKO\OrderBundle\Utils;

use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017Application;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as BriefApplication2016;
use NKO\OrderBundle\Entity\Farvater\FarvaterApplication;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017Application;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication2018;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNSApplication2018;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplication2018;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application as KNSSecondStage2017;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application as ContinuationSecondStage;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application2019\Application as Continuation2019SecondStage;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNS;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNSApplication2019;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNSSecondStage2018;
use NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018\Application as KNSThirdStage2018;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as HarborApplication2019;
use NKO\OrderBundle\Entity\Application\Harbor\Application2020\Application as HarborApplication2020;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Application as KNSApplication2020;
use NKO\OrderBundle\Entity\Application\Verification\Application2020\Application as Verification2020;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application as KNSSecondStage2019;

class ApplicationResolver
{
    const APPLICATION_CREATE_URLS = [
        Application::class => 'admin_nko_order_application_create',
        BriefApplication::class => 'admin_nko_order_briefapplication2017_briefapplication_create',
        KNS2017Application::class => 'admin_nko_order_kns2017_application_create',
        BriefApplication2016::class => 'admin_nko_order_briefapplication2016_application_create',
        FarvaterApplication::class => 'admin_nko_order_farvater_farvaterapplication_create',
        Farvater2017Application::class => 'admin_nko_order_farvater2017_application_create',
        SecondStageApplication::class => 'admin_nko_order_kns2017_secondstageapplication_create',
        BriefApplication2018::class => 'admin_nko_order_application_farvater_briefapplication2018_application_create',
        ContinuationApplication::class => 'admin_nko_order_application_continuation_application_create',
        KNSApplication2018::class => 'admin_nko_order_application_kns_application2018_application_create',
        FarvaterApplication2018::class => 'admin_nko_order_application_farvater_application2018_application_create',
        KNSSecondStage2017::class => 'admin_nko_order_application_kns_secondstage2017_application_create',
        ContinuationSecondStage::class => 'admin_nko_order_application_continuation_secondstage_application_create',
        Continuation2019SecondStage::class => 'admin_nko_order_application_continuation_secondstage_application2019_application_create',
        ContinuationKNS::class => 'admin_nko_order_application_continuation_kns_application_create',
        KNSApplication2019::class => 'admin_nko_order_application_kns_application2019_application_create',
        KNSSecondStage2018::class => 'admin_nko_order_application_kns_secondstage2018_application_create',
        HarborApplication2019::class => 'admin_nko_order_application_harbor_application2019_application_create',
        KNSThirdStage2018::class => 'admin_nko_order_application_kns_thirdstage2018_application_create',
        KNSApplication2020::class => 'admin_nko_order_application_kns_application2020_application_create',
        HarborApplication2020::class => 'admin_nko_order_application_harbor_application2020_application_create',
        Verification2020::class => 'admin_nko_order_application_verification_application2020_application_create',
        KNSSecondStage2019::class => 'admin_nko_order_application_kns_secondstage2019_application_create',
    ];

    const APPLICATION_EDIT_URLS = [
        Application::class => 'admin_nko_order_application_edit',
        BriefApplication::class => 'admin_nko_order_briefapplication2017_briefapplication_edit',
        KNS2017Application::class => 'admin_nko_order_kns2017_application_edit',
        BriefApplication2016::class => 'admin_nko_order_briefapplication2016_application_edit',
        FarvaterApplication::class => 'admin_nko_order_farvater_farvaterapplication_edit',
        Farvater2017Application::class => 'admin_nko_order_farvater2017_application_edit',
        SecondStageApplication::class => 'admin_nko_order_kns2017_secondstageapplication_edit',
        BriefApplication2018::class => 'admin_nko_order_application_farvater_briefapplication2018_application_edit',
        ContinuationApplication::class => 'admin_nko_order_application_continuation_application_edit',
        Continuation2019SecondStage::class => 'admin_nko_order_application_continuation_secondstage_application2019_application_edit',
        KNSApplication2018::class => 'admin_nko_order_application_kns_application2018_application_edit',
        FarvaterApplication2018::class => 'admin_nko_order_application_farvater_application2018_application_edit',
        KNSSecondStage2017::class => 'admin_nko_order_application_kns_secondstage2017_application_edit',
        ContinuationSecondStage::class => 'admin_nko_order_application_continuation_secondstage_application_edit',
        ContinuationKNS::class => 'admin_nko_order_application_continuation_kns_application_edit',
        KNSApplication2019::class => 'admin_nko_order_application_kns_application2019_application_edit',
        KNSSecondStage2018::class => 'admin_nko_order_application_kns_secondstage2018_application_edit',
        HarborApplication2019::class => 'admin_nko_order_application_harbor_application2019_application_edit',
        KNSThirdStage2018::class => 'admin_nko_order_application_kns_thirdstage2018_application_edit',
        KNSApplication2020::class => 'admin_nko_order_application_kns_application2020_application_edit',
        HarborApplication2020::class => 'admin_nko_order_application_harbor_application2020_application_edit',
        Verification2020::class => 'admin_nko_order_application_verification_application2020_application_edit',
        KNSSecondStage2019::class => 'admin_nko_order_application_kns_secondstage2019_application_edit',
    ];

    const APPLICATION_DOCUMENT_LIST_TEMPLATES = [
        Application::class => 'NKOOrderBundle:CRUD:base_list_application_documents.html.twig',
        BriefApplication::class => 'NKOOrderBundle:CRUD:base_list_brief_application_documents.html.twig' ,
        KNS2017Application::class => 'NKOOrderBundle:CRUD:base_list_application_documents.html.twig',
        Farvater2017Application::class => 'NKOOrderBundle:CRUD:farvater2017/base_list_application_documents.html.twig',
        SecondStageApplication::class => 'NKOOrderBundle:CRUD:base_list_application_documents.html.twig',
        BriefApplication2018::class => 'NKOOrderBundle:CRUD:Application/Farvater/BriefApplication2018/base_list_application_documents.html.twig',
        KNSApplication2018::class => 'NKOOrderBundle:CRUD:Application/KNS/Application2018/base_list_application_documents.html.twig',
        FarvaterApplication2018::class => 'NKOOrderBundle:CRUD:Application/Farvater/Application2018/base_list_application_documents.html.twig',
        KNSApplication2019::class => 'NKOOrderBundle:CRUD:Application/KNS/Application2019/base_list_application_documents.html.twig',
        KNSSecondStage2018::class => 'NKOOrderBundle:CRUD:Application/KNS/Application2018/base_list_application_documents.html.twig',
        HarborApplication2019::class => 'NKOOrderBundle:CRUD:Application/base_list_application_documents.html.twig',
        KNSThirdStage2018::class => 'NKOOrderBundle:CRUD:Application/base_list_application_documents.html.twig',
        KNSApplication2020::class => 'NKOOrderBundle:CRUD:Application/KNS/Application2020/base_list_application_documents.html.twig',
        HarborApplication2020::class => 'NKOOrderBundle:CRUD:Application/base_list_application_documents.html.twig',
        Verification2020::class => 'NKOOrderBundle:CRUD:Application/base_list_application_documents.html.twig',
        KNSSecondStage2019::class => 'NKOOrderBundle:CRUD:Application/base_list_application_documents.html.twig',
    ];


    /** Used with NKOOrderBundle:CRUD:Application/base_list_application_documents.html.twig
     *
     */
    const APPLICATION_DOCUMENT_LIST_FILES = [
        HarborApplication2019::class => [
            'single' => [
                'subjectRegulation' => 'Форма заявки.',
                'regulation' => 'Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
                'authorityHead' => 'Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.)',
                'anotherHead' => 'Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
                'signedAgreement' => 'Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью)',
                'budget' => 'Бюджет проекта (файл в формате Excel по форме установленного образца',
                'organizationCreationResolution' => 'Документ, подтверждающий статус юридического лица государственных и муниципальных
                учреждений (решение о создании учреждения и т.п.) – только для государственных и муниципальных учреждений.',
                'isBankAccountExist' => 'Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта (скан-копия)',
                'reportForMinistryOfJustice' => 'Отчёт в Министерство юстиции Российской Федерации за предшествующий отчетный период 
                (скан-копия, с подписью руководителя и печатью организации) или ссылка на его версию, размещенную на Информационном 
                портале Министерства юстиции РФ (за исключением организаций-заявителей – государственных и муниципальных учреждений)',
            ],
            'collections' => [],
        ],
        KNSThirdStage2018::class => [
            'single' => [
                'regulation' => '1 Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
                'authorityHead' => '2. Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.)',
                'anotherHead' => '3. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
                'signedAgreement' => '4. Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью)',
                'budget' => '5. Бюджет заявки в формате excel',
                'subjectRegulation' => '6. Устав субъекта РФ или муниципалитета, положение о профильной службе (департаменте, министерстве и т.п.) 
                – только для органов государственной власти и местного самоуправления',
                'organizationCreationResolution' => '7. Документ, подтверждающий статус юридического лица государственных и муниципальных учреждений 
                (решение о создании учреждения и т.п.) - только для государственных и муниципальных учреждений',
            ],
            'collections' => [],
        ],
        HarborApplication2020::class => [
            'single' => [
                'subjectRegulation' => 'Форма заявки.',
                'projectDescriptionDocument' => 'Описание практики – по форме установленного образца.',
                'regulation' => 'Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего органа в формате pdf',
                'authorityHead' => 'Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ и пр.)',
                'anotherHead' => 'Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор будет подписывать не руководитель организации)',
                'signedAgreement' => 'Информированные согласия на обработку персональных данных, заполненные на всех лиц, упоминаемых в заявке (с личной подписью)',
                'budget' => 'Бюджет проекта (файл в формате Excel по форме установленного образца',
                'organizationCreationResolution' => 'Документ, подтверждающий статус юридического лица государственных и муниципальных
                учреждений (решение о создании учреждения и т.п.) – только для государственных и муниципальных учреждений.',
                'isBankAccountExist' => 'Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта (скан-копия)',
                'reportForMinistryOfJustice' => 'Отчёт в Министерство юстиции Российской Федерации за предшествующий отчетный период 
                (скан-копия, с подписью руководителя и печатью организации) или ссылка на его версию, размещенную на Информационном 
                портале Министерства юстиции РФ (за исключением организаций-заявителей – государственных и муниципальных учреждений)',
            ],
            'collections' => [],
        ],
        Verification2020::class => [
            'single' => [
                'subjectRegulation' => '1. Заявка – по форме установленного образца в формате docx',
                'regulation' => '2. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой 
                    регистрирующего органа в формате pdf',
                'anotherHead' => '3.  Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ) 
                    в формате pdf необязательный документ',
                'authorityHead' => '4. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор 
                    будет подписывать не руководитель организации) в формате pdf',
                'signedAgreement' => '5.  Информированные согласия на обработку персональных данных, заполненные на всех лиц, 
                    упоминаемых в заявке (с личной подписью) в формате pdf',
                'organizationCreationResolution' => '6. Документ, подтверждающий статус юридического лица государственных и муниципальных 
                    учреждений (решение о создании учреждения и т.п.) – только для государственных и муниципальных учреждений. в формате pdf ',
                'isBankAccountExist' => '7. Письмо/справка из банка/территориального органа федерального казначейства о наличии счёта 
                    (скан-копия) в формате pdf',
            ],
            'collections' => [
                'measurementTools' => [
                    'titleLabel' => 'Инструмент измерения показателя (сбора данных)',
                    'files' => [
                            [
                                'fileLabel' => 'Инструмент',
                                'fileField' => 'file',
                            ],
                    ],
                ],
                'resultConfirmingDocuments' => [
                    'titleLabel' => 'Документы, подтверждающие достижение результата/показателя',
                    'files' => [
                            [
                                'fileLabel' => 'Документ',
                                'fileField' => 'file',
                            ],
                    ],
                ],
                'performanceConfirmingDocuments' => [
                    'titleLabel' => 'Есть ли у вас в наличии другие доказательства, которые могли бы 
                        дополнить данные о результативности вашей практики, но не нашли отражения в таблице показателей? Уточните, 
                        какие именно доказательства имеются',
                    'files' => [
                            [
                                'fileLabel' => 'Доказательство',
                                'fileField' => 'file',
                            ],
                    ],
                ],
                'practiceRegulationDocuments' => [
                    'titleLabel' => 'Базовые регламенты практики',
                    'files' => [
                            [
                                'fileLabel' => 'Базовый регламент практики',
                                'fileField' => 'file',
                            ],
                    ],
                ],
                'resultDocuments' => [
                    'titleLabel' => 'Существует ли разработанная Цепочка социальных результатов (теория изменений/логическая 
                        модель/дерево результатов) практики? (добавьте в приложения или дайте ссылки в тексте на эти материалы)',
                    'files' => [
                            [
                                'fileLabel' => 'Цепочка социальных результатов',
                                'fileField' => 'file',
                            ],
                    ],
                ],
            ],
        ],
        KNSSecondStage2019::class => [
            'single' => [        'budget' => '1. Бюджет проекта – заполненный файл установленного образца – в формате excel;',
            'regulation' => '2. Устав организации (со всеми внесенными изменениями и дополнениями), с отметкой регистрирующего 
                органа – только если были изменения и дополнения после заключения договора I этапа – в формате pdf; ',
            'anotherHead' => '3.  Документ, подтверждающий полномочия руководителя (протокол об избрании, приказ) 
                в формате pdf необязательный документ',
            'authorityHead' => '4. Документ, подтверждающий полномочия лица, которое будет подписывать договор (если договор 
                будет подписывать не руководитель организации) в формате pdf',
            'signedAgreement' => '5. Подписанные согласия на обработку персональных данных всех лиц, фигурирующих в заявке',
            'organizationCreationResolution' => '6. Документ, подтверждающий статус юридического лица (решение о создании 
                учреждения и т.п.) – только для государственных и муниципальных учреждений в формате pdf;',
            'reportForMinistryOfJustice' => '7. Отчёт в Министерство юстиции Российской Федерации /иной регистрирующий орган за 
                предшествующий отчётный период (скан-копия, заверенная подписью руководителя и печатью организации) ИЛИ ссылка 
                на его версию, размещённую на Информационном портале Министерства юстиции Российской Федерации по адресу: 
                http://unro.minjust.ru (за исключением организаций-заявителей – государственных и муниципальных учреждений) 
                в формате pdf',
            ],
            'collections' => [],
        ],
    ];
}
