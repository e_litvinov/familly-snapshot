<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/12/18
 * Time: 2:35 PM
 */

namespace NKO\OrderBundle\Utils;

class ApplicationTypes
{
    const BRIEF_APPLICATION_2016 = 'brief_sf-2016';
    const BRIEF_APPLICATION_2017 = 'brief_application_2017';
    const BRIEF_APPLICATION_2018 = 'brief_application_2018';

    const CONTINUATION_APPLICATION_2018 = 'continuation_application_2018';
    const CONTINUATION_APPLICATION_2018_MEATHURE = 'continuation_application_2018_meathure';
    const CONTINUATION_SECOND_STAGE_APPLICATION_2018 = 'continuation_second_stage_2018';
    const CONTINUATION_SECOND_STAGE_APPLICATION_2019 = 'continuation_second_stage_2019';
    const CONTINUATION_KNS = 'continuation_kns_2018';

    const KNS_APPLICATION_2018 = 'kns_application_2018_';
    const FARVATER_APPLICATION_2018 = 'farvater_application_2018';
    const KNS_APPLICATION_2_2017 = 'kns_application_2_2017';
    const KNS_APPLICATION_2019 = 'kns_application_2019';
    const KNS_APPLICATION_2020 = 'kns_application_2020';

    const KNS_APPLICATION_3_2018 = 'kns_application_3_2018';

    const HARBOR_2020 = 'harbor_2020';
}
