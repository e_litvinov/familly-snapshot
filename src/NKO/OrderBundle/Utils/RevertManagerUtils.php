<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/31/18
 * Time: 5:09 PM
 */

namespace NKO\OrderBundle\Utils;

use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as KNSContinuation;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application as KNSSecondStage2017;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticKNSReport2018;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedKNSReport;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application as ContinuationSecondStage;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application2019\Application as Continuation2019SecondStage;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNSSecondStage2018;
use NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018\Application as KNSThirdStage2018;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as HarborApplication2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedKNSReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedKNSReport2020;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application as KNSSecondStage2019;

class RevertManagerUtils
{
    const COLLECTIONS_TO_RESOLVE = [
        BriefApplication2018::class => [
            'beneficiaryProblems',
        ],
        AnalyticReport::class => [
            'practiceFeedbackAttachments',
            'practiceSpreadFeedbackAttachments',
        ],
        Harbor2020MonitoringReport::class => [
            'monitoringResults',
            'documents'
        ],
        MonitoringReport::class => [
            'monitoringResults',
            'documents'
        ],
        FinanceReport::class => [
            'registers',
        ],
        FinanceReport2018::class => [
            'registers',
        ],
        KNSSecondStage2017::class => [
            'beneficiaryProblems',
        ],
        AnalyticReport2018::class => [
            'practiceFeedbackAttachments',
            'practiceSpreadFeedbackAttachments',
        ],
        AnalyticReport2019::class => [
            'practiceFeedbackAttachments',
            'expectedAnalyticResults',
            'implementationSocialResults',
            'practiceAnalyticResults',
            'introductionSocialResults',
            'practiceFeedbackAttachments',
            'practiceSocialFeedbackAttachments',
            'practiceSpreadFeedbackAttachments',
            'practiceSocialSpreadFeedbackAttachments',
        ],
        AnalyticKNSReport2018::class => [
            'practiceFeedbackAttachments',
        ],
        AnalyticHarborReport2019::class => [
            'practiceFeedbackAttachments',
        ],
        MixedKNSReport::class => [
            'registers',
        ],
        MixedKNSReport2019::class => [
            'registers',
        ],
        MixedKNSReport2020::class => [
            'registers',
        ],
        ContinuationSecondStage::class => [
            'beneficiaryProblems',
            'specialistProblems',
            'effectivenessImplementationEtcItems',
            'expectedResults',
            'effectivenessDisseminationEtcItems',
            'practiceResults'
        ],
        Continuation2019SecondStage::class => [
            'beneficiaryProblems',
            'specialistProblems',
            'effectivenessImplementationEtcItems',
            'expectedResults',
            'effectivenessDisseminationEtcItems',
            'practiceResults'
        ],
        KNSContinuation::class => [
            'projects',
            'beneficiaryProblems',
            'effectivenessKNSItems',
            'individualSocialResults',
        ],
        KNSSecondStage2018::class => [
            'beneficiaryProblems',
            'projects',
            'individualSocialResults',
        ],
        KNSThirdStage2018::class => [
            'beneficiaryProblems',
            'EffectivenessKNSItems',
            'projects',
            'individualSocialResults',
        ],
        HarborApplication2019::class => [
            'projects',
        ],
        KNSSecondStage2019::class => [
            'beneficiaryProblems',
            'projects',
            'individualSocialResults',
        ],
    ];
}
