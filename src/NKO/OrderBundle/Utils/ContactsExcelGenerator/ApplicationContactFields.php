<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class ApplicationContactFields extends DefaultFields
{
    const FIELDS = ['projectName', 'requiredMoney'];

    public function getFields()
    {
        return array_merge_recursive(parent::BASE_FIELDS, self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }
}
