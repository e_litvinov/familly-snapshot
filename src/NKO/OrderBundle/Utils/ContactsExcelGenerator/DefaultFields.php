<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class DefaultFields implements ContactExcelFieldsInterface
{
    const MANAGEMENT_CONTACTS = [
        'phone',
        'headOfOrganizationFullName',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPhone',
        'headOfProjectMobilePhone',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingEmeil',
        'siteLinks',
        'socialNetworkLinks',
        'isSend',
    ];


    const BASE_FIELDS = [
        'fullOrganizationName', 'actualRegion', 'legalAddress', 'actualAddress'
    ];

    const TRANSLATION = [
        'phone' => 'Телефон для оперативного контакта',
        'headOfOrganizationFullName' => 'ФИО руководителя организации',
        'headOfOrganizationPhone' => 'Телефон руководителя организации',
        'headOfOrganizationMobilePhone' => 'Мобильный телефон руководителя организации',
        'headOfOrganizationEmeil' => 'Электронная почта руководителя организации',
        'headOfProjectFullName' => 'ФИО руководителя проекта',
        'headOfProjectPhone' => 'Телефон руководителя проекта',
        'headOfProjectMobilePhone' => 'Мобильный телефон руководителя проекта',
        'headOfProjectEmeil' => 'Электронная почта руководителя проекта',
        'headOfAccountingFullName' => 'ФИО главного бухгалтера',
        'headOfAccountingPhone' => 'Телефон главного бухгалтера',
        'headOfAccountingEmeil' => 'Электронная почта главного бухгалтера',
        'siteLinks' => 'Сайт',
        'socialNetworkLinks' => 'Страницы в социальных сетях',
        'isSend' => 'Отправлена',
        'fullOrganizationName' => 'Полное название организации',
        'organizationForm' => 'Организационно-правовая форма',
        'abbreviation' => 'Сокращенное название',
        'actualRegion' => 'Регион',
        'legalAddress' => 'Юридический адрес',
        'actualAddress' => 'Фактический адрес',
        'practiceRegions' => 'Регионы практики',
        'priorityDirection' => 'Приоритетное направление конкурса',
        'priorityDirectionEtc' => 'Другие приоритетное направление конкурса',
        'FirstYearEstimatedFinancing' => 'Сумма запрашиваемого финансирования на первый год реализации проекта',
        'trainingGroundsDirection' => 'Приоритетное направление конкурса',
        'trainingGrounds' => 'Стажировочная площадка',
        'themeInternship' => 'Тематика стажировки',
        'projectName' => 'Название проекта',
        'deadline' => 'Сроки проекта',
        'dateStartOfInternship' => 'Сроки стажировки',
        'requestedFinancingMoney' => 'Запрашиваемая сумма',
        'requiredMoney' => 'Запрашиваемая сумма',
        'psrn' => 'ОГРН',
    ];

    public function getFields()
    {
        return array_merge(self::BASE_FIELDS, self::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return self::TRANSLATION;
    }
}
