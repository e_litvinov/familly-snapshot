<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2020ApplicationContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName', 'organizationForm', 'actualRegion', 'legalAddress', 'actualAddress', 'projectName', 'trainingGrounds', 'deadline', 'dateStartOfInternship', 'requestedFinancingMoney',
    ];

    const MANAGEMENT_CONTACTS = [
        'phone',
        'headOfOrganizationFullName',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPhone',
        'headOfProjectMobilePhone',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingMobilePhone',
        'headOfAccountingEmeil',
        'siteLinks',
        'socialNetworkLinks',
        'isSend',
    ];

    const TRANSLATION = [
        'headOfOrganizationPhone' => 'Городской телефон руководителя организации',
        'headOfProjectPhone' => 'Городской телефон руководителя проекта',
        'headOfAccountingMobilePhone' => 'Мобильный телефон главного бухгалтера'

    ];

    public function getFields()
    {
        return array_merge_recursive(self::FIELDS, self::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
