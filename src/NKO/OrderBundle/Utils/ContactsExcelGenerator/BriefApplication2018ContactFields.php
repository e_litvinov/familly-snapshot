<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class BriefApplication2018ContactFields extends DefaultFields
{
    const FIELDS = ['priorityDirection', 'projectName'];
    const TRANSLATION = ['projectName' => 'Название практики'];

    public function getFields()
    {
        return array_merge_recursive(parent::BASE_FIELDS, self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
