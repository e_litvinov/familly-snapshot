<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

interface ContactExcelFieldsInterface
{
    public function getFields();
    public function getTranslations();
}
