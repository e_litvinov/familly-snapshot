<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class HarborApplication2020ContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName',
        'actualRegion',
        'projectName',
        'deadline',
        'trainingGround',
        'FirstYearEstimatedFinancing',
        'legalAddress',
        'actualAddress',
    ];

    const TRANSLATION = [
        'FirstYearEstimatedFinancing' => 'Сумма запрашиваемого финансирования',
        'trainingGround' => 'Приоритетное направление',
        'deadline' => 'Сроки реализации проекта',
        'headOfOrganizationPhone' => 'Городской телефон руководителя организации',
        'headOfProjectPhone' => 'Городской телефон руководителя проекта',
        'headOfAccountingPhone' => 'Мобильный телефон главного бухгалтера',
    ];

    public function getFields()
    {
        return array_merge(self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
