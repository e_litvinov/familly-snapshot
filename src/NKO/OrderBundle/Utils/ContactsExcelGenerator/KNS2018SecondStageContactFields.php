<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2018SecondStageContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName',
        'actualRegion',
        'trainingGroundsDirection',
        'trainingGrounds',
        'projectName',
        'firstYearEstimatedFinancing',
        'legalAddress',
        'actualAddress'
    ];

    const TRANSLATION = [
        'firstYearEstimatedFinancing' => 'Сумма запрашиваемого финансирования на первый год реализации проекта, тыс. руб.',
    ];

    public function getFields()
    {
        return array_merge(self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
