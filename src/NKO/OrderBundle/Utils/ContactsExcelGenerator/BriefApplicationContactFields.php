<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class BriefApplicationContactFields extends DefaultFields
{
    const FIELDS = [
        'abbreviation', 'priorityDirection', 'priorityDirectionEtc', 'psrn', 'practiceRegions', 'projectName'
    ];
    const TRANSLATION = ['projectName' => 'Название практики'];

    public function getFields()
    {
        return array_merge_recursive(parent::BASE_FIELDS, self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
