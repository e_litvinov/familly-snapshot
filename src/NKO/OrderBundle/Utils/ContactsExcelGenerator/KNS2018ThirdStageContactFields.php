<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2018ThirdStageContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName',
        'actualRegion',
        'trainingGroundsDirection',
        'trainingGrounds',
        'projectName',
        'sumGrant',
        'legalAddress',
        'actualAddress'
    ];

    const TRANSLATION = [
        'sumGrant' => 'Сумма пожертвования, руб',
    ];

    public function getFields()
    {
        return array_merge(self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
