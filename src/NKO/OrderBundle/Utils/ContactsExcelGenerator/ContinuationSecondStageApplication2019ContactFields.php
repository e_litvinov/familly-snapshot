<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class ContinuationSecondStageApplication2019ContactFields extends DefaultFields
{

    const MANAGEMENT_CONTACTS = [
        'authorPhone',
        'authorEmeil',
        'headOfOrganizationFullName',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPhone',
        'headOfProjectMobilePhone',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingEmeil',
        'siteLinks',
        'socialNetworkLinks',
        'isSend',
    ];

    const FIELDS = [
        'projectName', 'FirstYearEstimatedFinancing'
    ];
    const TRANSLATION = [
        'authorPhone' => 'Телефон для оперативного контакта',
        'authorEmeil' => 'Электронная почта для оперативного контакта',
    ];

    public function getFields()
    {
        return array_merge(self::BASE_FIELDS, self::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
