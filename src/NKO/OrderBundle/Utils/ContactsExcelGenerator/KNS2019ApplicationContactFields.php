<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2019ApplicationContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName', 'organizationForm', 'actualRegion', 'legalAddress', 'actualAddress', 'projectName', 'trainingGrounds', 'deadline', 'dateStartOfInternship', 'requestedFinancingMoney'
    ];

    public function getFields()
    {
        return array_merge_recursive(self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }
}
