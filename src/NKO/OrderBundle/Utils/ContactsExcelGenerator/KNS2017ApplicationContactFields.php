<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2017ApplicationContactFields extends DefaultFields
{
    const FIELDS = [
        'trainingGrounds', 'projectName', 'requestedFinancingMoney'
    ];

    public function getFields()
    {
        return array_merge_recursive(parent::BASE_FIELDS, self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }
}
