<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2017SecondStageContactFields extends DefaultFields
{
    const FIELDS = [
        'trainingGrounds', 'trainingGroundsDirection', 'projectName'
    ];

    public function getFields()
    {
        return array_merge_recursive(parent::BASE_FIELDS, self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }
}
