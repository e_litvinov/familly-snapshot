<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class HarborApplication2019ContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName',
        'actualRegion',
        'projectName',
        'FirstYearEstimatedFinancing',
        'legalAddress',
        'actualAddress'
    ];

    const TRANSLATION = [
        'FirstYearEstimatedFinancing' => 'Сумма запрашиваемого финансирования на первый год реализации проекта (тыс.руб.)'
    ];

    public function getFields()
    {
        return array_merge(self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
