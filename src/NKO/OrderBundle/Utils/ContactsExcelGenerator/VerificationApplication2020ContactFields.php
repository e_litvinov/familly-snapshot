<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class VerificationApplication2020ContactFields extends DefaultFields
{
    const FIELDS = [
        'fullOrganizationName',
        'actualRegion',
        'projectName',
        'legalAddress',
        'actualAddress',
    ];

    const TRANSLATION = [
        'siteLinks' => 'Адрес сайта',
        'projectName' => 'Название практики',
        'deadline' => 'Сроки реализации проекта',
        'headOfOrganizationPhone' => 'Городской телефон руководителя организации',
        'headOfProjectFullName' => 'ФИО руководителя практики',
        'headOfProjectPhone' => 'Городской телефон руководителя практики',
        'headOfProjectMobilePhone' => 'Мобильный телефон руководителя практики',
        'headOfProjectEmeil' => 'Электронная почта руководителя практики',
        'headOfAccountingPhone' => 'Мобильный телефон главного бухгалтера',
    ];

    public function getFields()
    {
        return array_merge(self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }

    public function getTranslations()
    {
        return array_merge(parent::TRANSLATION, self::TRANSLATION);
    }
}
