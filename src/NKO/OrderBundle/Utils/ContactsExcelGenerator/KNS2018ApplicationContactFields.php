<?php

namespace NKO\OrderBundle\Utils\ContactsExcelGenerator;

class KNS2018ApplicationContactFields extends DefaultFields
{
    const FIELDS = [
        'organizationForm', 'trainingGrounds', 'projectName', 'deadline', 'dateStartOfInternship', 'requestedFinancingMoney'
    ];

    public function getFields()
    {
        return array_merge_recursive(parent::BASE_FIELDS, self::FIELDS, parent::MANAGEMENT_CONTACTS);
    }
}
