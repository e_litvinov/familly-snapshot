<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 31.01.19
 * Time: 12:12
 */

namespace NKO\OrderBundle\Utils\NameListGenerator;

class BaseGenerator
{
    const ALL_LABEL = 'Все';

    protected static function parseLabels($classes, $labelForAll = self::ALL_LABEL)
    {
        $values = [];
        $values[$labelForAll] = $classes;

        foreach ($classes as $classType) {
            $values[$classType['alias']] = $classType;
            foreach ($classType['classes'] as $alias => $value) {
                $values[$alias] = $value;
            }
        }

        return $values;
    }

    public static function getArrayOfCompetitionsBy($input)
    {
        $classes = [];

        if (is_array($input) && key_exists('classes', $input)) {
            foreach ($input['classes'] as $nextLevel) {
                $classes = array_merge(self::getArrayOfCompetitionsBy($nextLevel), $classes);
            }
        } elseif (is_array($input)) {
            foreach ($input as $nextLevel) {
                $classes = array_merge(self::getArrayOfCompetitionsBy($nextLevel), $classes);
            }
        } else {
            $classes[] = $input;
        }

        return $classes;
    }
}
