<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 20.11.18
 * Time: 12.35
 */

namespace NKO\OrderBundle\Utils\NameListGenerator;

use NKO\OrderBundle\Entity\BriefApplication2016\Application as Farvater2016;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater2018;
use NKO\OrderBundle\Entity\Application as KNS2016;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017_1;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as KNS2017_2;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNS2018;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as Harbor;

class ApplicationNames extends BaseGenerator
{
    const ALL_LABEL = 'Все конкурсы';

    const FARVATER = 'FARVATER';
    const KNS = 'KNS';
    const HARBOR = 'HARBOR';

    const FARVATER_ALIAS = 'СФ';
    const KNS_ALIAS = 'КНС';
    const HARBOR_ALIAS = 'Семейная гавань';

    const FARVATER2016_ALIAS = 'СФ-2016';
    const FARVATER2017_ALIAS = 'СФ-2017';
    const FARVATER2018_ALIAS = 'СФ-2018';

    const KNS2016_ALIAS = 'КНС-2016';
    const KNS2017_1_ALIAS = 'КНС-2017';
    const KNS2017_2_ALIAS = 'КНС-2017-2';
    const KNS2018_ALIAS = 'КНС-2018';

    const MONITORING_APPLICATIONS = [
        self::FARVATER => [
            'classes' => [
                self::FARVATER2016_ALIAS => FARVATER2016::class,
                self::FARVATER2017_ALIAS => FARVATER2017::class,
                self::FARVATER2018_ALIAS => FARVATER2018::class,
            ],
            'alias' => self::FARVATER_ALIAS
        ],
        self::KNS => [
            'classes' => [
                self::KNS2016_ALIAS => KNS2016::class,
                self::KNS2017_1_ALIAS => KNS2017_1::class,
                self::KNS2017_2_ALIAS => KNS2017_2::class,
                self::KNS2018_ALIAS => KNS2018::class,
            ],
            'alias' => self::KNS_ALIAS
        ],
        self::HARBOR => [
            'classes' => [
                self::KNS2016_ALIAS => Harbor::class,
            ],
            'alias' => self::HARBOR_ALIAS
        ],
    ];

    public static function parse($classes)
    {
        return parent::parseLabels($classes, self::ALL_LABEL);
    }
}
