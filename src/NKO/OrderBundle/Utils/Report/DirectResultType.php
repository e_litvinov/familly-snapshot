<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 7.7.18
 * Time: 11.12
 */

namespace NKO\OrderBundle\Utils\Report;


class DirectResultType
{
    const PRACTICE_RESULT = 'practice_result';
    const EXPECTED_RESULT = 'expected_result';

}