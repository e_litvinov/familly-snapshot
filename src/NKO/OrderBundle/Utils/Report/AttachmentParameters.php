<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 16.7.18
 * Time: 17.14
 */

namespace NKO\OrderBundle\Utils\Report;

class AttachmentParameters
{
    const ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT = [
        [
            'tableName' => '6.1.1. Обратная связь. Практика',
            'collectionName' => 'practiceFeedbackAttachments',
            'title' => 'practice',
            'fileField' => 'tools',
        ], [
            'tableName' => '6.1.2. Обратная связь. Распространение практики',
            'collectionName' => 'practiceSpreadFeedbackAttachments',
            'title' => 'spreadPractice',
            'fileField' => 'tools',
        ], [
            'tableName' => '6.2.1. Истории успеха. Реализация практики',
            'collectionName' => 'practiceSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ], [
            'tableName' => '6.2.2. Истории успеха. Распространение практики',
            'collectionName' => 'practiceSpreadSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ], [
            'tableName' => '6.3. Публикации в рамках проекта',
            'collectionName' => 'publicationAttachments',
            'title' => 'name',
            'fileField' => 'file',
        ], [
            'tableName' => '6.4. Издание информационных и методических материалов',
            'collectionName' => 'materialAttachments',
            'title' => 'name',
            'fileField' => 'file',
        ],  [
            'tableName' => '6.5. Факторы, влиявшие на внедрение Практики',
            'collectionName' => 'factorAttachments',
            'title' => 'name',
            'fileField' => 'file',
        ],  [
            'tableName' => '6.6. Разработки по системе мониторинга и оценки организации',
            'collectionName' => 'monitoringAttachments',
            'title' => 'monitoringElement',
            'fileField' => 'file',
        ],
    ];

    const ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT2018 = [
        [
            'tableName' => '6.1.1. Обратная связь. Практика',
            'collectionName' => 'practiceFeedbackAttachments',
            'title' => 'practice',
            'fileField' => 'tools',
        ], [
            'tableName' => '6.1.2. Обратная связь. Распространение практики',
            'collectionName' => 'practiceSpreadFeedbackAttachments',
            'title' => 'practice',
            'fileField' => 'tools',
        ], [
            'tableName' => '6.2.1. Истории успеха. Реализация практики',
            'collectionName' => 'practiceSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ], [
            'tableName' => '6.2.2. Истории успеха. Распространение и внедрение практики',
            'collectionName' => 'practiceSpreadSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ], [
            'tableName' => '6.5. Организации, внедрившие Практику',
            'collectionName' => 'factorAttachments',
            'title' => 'name',
            'fileField' => 'file',
        ], [
            'tableName' => '6.6. Разработки по системе мониторинга и оценки организации',
            'collectionName' => 'monitoringDevelopmentAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ],
    ];
    const ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT2019 = [
        [
                'tableName' => '6.1.1. Реализация практики – непосредственные результаты',
                'collectionName' => 'practiceFeedbackAttachments',
                'title' => 'practice',
                'fileField' => 'tools',
            ], [
                'tableName' => '6.1.2. Реализация практики – социальные результаты',
                'collectionName' => 'practiceSocialFeedbackAttachments',
                'title' => 'practice',
                'fileField' => 'tools',
            ], [
                'tableName' => '6.1.3. Обратная связь. Распространение и внедрение практики',
                'collectionName' => 'practiceSpreadFeedbackAttachments',
                'title' => 'practice',
                'fileField' => 'tools',
            ], [
                'tableName' => '6.1.4. Распространение и внедрение практики – социальные результаты',
                'collectionName' => 'practiceSocialSpreadFeedbackAttachments',
                'title' => 'practice',
                'fileField' => 'tools',
            ], [
                'tableName' => '6.2.1. Истории успеха. Реализация практики',
                'collectionName' => 'practiceSuccessStoryAttachments',
                'title' => 'title',
                'fileField' => 'file',
            ], [
                'tableName' => '6.2.2. Истории успеха. Распространение и внедрение практики',
                'collectionName' => 'practiceSpreadSuccessStoryAttachments',
                'title' => 'title',
                'fileField' => 'file',
            ], [
                'tableName' => '6.3. Публикации в рамках проекта',
                'collectionName' => 'publicationAttachments',
                'title' => 'mediaName',
                'fileField' => 'file',
            ], [
                'tableName' => '6.4. Издание информационных и методических материалов',
                'collectionName' => 'materialAttachments',
                'title' => 'name',
                'fileField' => 'file',
            ], [
                'tableName' => '6.5. Организации, внедрившие Практику',
                'collectionName' => 'factorAttachments',
                'title' => 'name',
                'fileField' => 'file',
            ], [
                'tableName' => '6.6. Разработки по системе мониторинга и оценки организации',
                'collectionName' => 'monitoringDevelopmentAttachments',
                'title' => 'title',
                'fileField' => 'file',
            ],
        ];
    const ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT_KNS2018 = [
        [
            'tableName' => '5.1. Обратная связь',
            'collectionName' => 'practiceFeedbackAttachments',
            'title' => 'practice',
            'fileField' => 'tools',
        ], [
            'tableName' => '5.2. Истории успеха',
            'collectionName' => 'practiceSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ]
    ];

    const ATTACHMENTS_MIXED_REPORT_KNS2019 = [
        [
            'tableName' => 'Приложения к финансовому отчету',
            'collectionName' => 'documents',
            'title' => 'documentName',
            'fileField' => 'file',
        ],
        [
            'tableName' => 'Приложения к содержательному отчету',
            'collectionName' => 'analyticAttachments',
            'title' => 'content',
            'fileField' => 'file',
        ],
    ];


    const ATTACHMENTS_PARAMETERS_ANALYTIC_HARBOR_REPORT2019 = [
        [
            'tableName' => '5.1. Обратная связь',
            'collectionName' => 'practiceFeedbackAttachments',
            'title' => 'practice',
            'fileField' => 'tools',
        ], [
            'tableName' => '5.2. Истории успеха',
            'collectionName' => 'practiceSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ], [
            'tableName' => '5.3. Иные приложения',
            'collectionName' => 'practiceSpreadSuccessStoryAttachments',
            'title' => 'title',
            'fileField' => 'file',
        ]
    ];
}
