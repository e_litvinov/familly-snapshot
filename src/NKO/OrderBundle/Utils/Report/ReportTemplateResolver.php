<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 5/24/18
 * Time: 3:58 PM
 */

namespace NKO\OrderBundle\Utils\Report;

use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReportKNS2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReportKNS2020;

class ReportTemplateResolver
{
    const URL_BY_REPORT_CLASS = [
        FinanceReport::class => 'admin_nko_order_report_financereport_reporttemplate',
        MonitoringReport::class => 'admin_nko_order_report_monitoringreport_reporttemplate',
        Harbor2020MonitoringReport::class => 'admin_nko_order_report_monitoringreport_reporttemplate',
        FinanceReport2018::class => 'admin_nko_order_report_financereport_report2018_reporttemplate',
        FinanceReport2019::class => 'admin_nko_order_report_financereport_report2018_reporttemplate',
        MixedReportKNS::class => 'admin_nko_order_report_financereport_report2018_reporttemplate',
        MixedReportKNS2019::class => 'admin_nko_order_report_financereport_report2018_reporttemplate',
        MixedReportKNS2020::class => 'admin_nko_order_report_financereport_report2018_reporttemplate',
    ];
}
