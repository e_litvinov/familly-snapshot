<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 7/18/18
 * Time: 4:30 PM
 */

namespace NKO\OrderBundle\Utils\Report;


class AttachmentListenerUtils
{
    const PUBLICATION_ATTACHMENTS = 'publicationAttachments';
    const MATERIAL_ATTACHMENTS = 'materialAttachments';

    const PUBLICATION_ATTACHMENTS_DATA = [
        "author" => "",
        "name" => "",
        "date" => "",
        "mediaName" => "",
        "mediaLink" => "",
        "file" => null
    ];

    const MATERIAL_ATTACHMENTS_DATA = [
        "author" => "",
        "name" => "",
        "date" => "",
        "edition" => "",
        "mediaLink" => "",
        "comment" => "",
        "file" => null
    ];
}