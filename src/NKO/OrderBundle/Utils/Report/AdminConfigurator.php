<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.6.18
 * Time: 16.12
 */

namespace NKO\OrderBundle\Utils\Report;

use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReport;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedKNS2019Report;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedKNS2020Report;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticReportKNS2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;

class AdminConfigurator
{
    /**
     * admin_code => [имя поля => админ код]
     * extraFields => [имена полей, которые нужно исключить]
     * parameters => [имя поля => [
     *      параметры,
     *     'selectedType' => 'sonata_type_collection'(если нужно указать тип поля)
     *     ]
     * ]
     */

    const MONITORING_INFO =[
        AnalyticReport2018::class => [
            'admin_code' => [
                'tripleContext' => 'nko_order.admin.analytic_report.triple_context',
            ],
            'extraFields' => [
                'monitoringDevelopments'
            ],
            'parameters' => [
                'monitoringResults' => [
                    'selectedType' => 'sonata_type_collection',
                    'type_options' => ['delete' => true]]
            ]
        ],
        AnalyticReport::class => [
            'admin_code' => [
                'tripleContext' => 'nko_order.admin.analytic_report.triple_context',
            ],
            'parameters' => [
                'monitoringResults' => [
                    'selectedType' => 'sonata_type_collection',
                    'type_options' => ['delete' => false]]
            ]
        ],
    ];

    const RESOURCE_ANALYTIC_REPORT = [
        AnalyticReport2018::class => [
            'extraFields' => [
                'keyRisks', 'commandConclusion', 'partnerConclusion'
            ],
            'admin_code' => [
                'humanResources' => 'nko_order.admin.analytic_report.human_resource',
                'partnerActivities' => 'nko_order.admin.analytic_report.quadruple_context',
            ]
        ],
        AnalyticReport2019::class => [
            'extraFields' => [
                'keyRisks', 'commandConclusion', 'partnerConclusion'
            ],
            'admin_code' => [
                'humanResources' => 'nko_order.admin.analytic_report.human_resource',
                'partnerActivities' => 'nko_order.admin.analytic_report.quadruple_context',
            ]
        ],
        AnalyticReport::class => [
            'extraFields' => [
                'commandConclusion', 'partnerConclusion'
            ],
            'admin_code' => [
                'humanResources' => 'nko_order.admin.analytic_report.human_resource',
                'partnerActivities' => 'nko_order.admin.analytic_report.quadruple_context',
            ],
        ],
        AnalyticReportKNS2018::class => [
            'extraFields' => [
                'keyRisks', 'commandConclusion', 'partnerConclusion'
            ],
            'admin_code' => [
                'partnerActivities' => 'nko_order.admin.report.analytic_report.report2018.kns.quadruple_context',
                'humanResources' => 'nko_order.admin.report.analytic_report.report2018.kns.human_resource',
            ],
        ],
        AnalyticHarborReport2019::class => [
            'extraFields' => [
                'keyRisks'
            ],
            'admin_code' => [
                'partnerActivities' => 'nko_order.admin.report.analytic_report.report2018.kns.quadruple_context',
                'humanResources' => 'nko_order.admin.report.analytic_report.report2018.kns.human_resource',
            ],
        ],
    ];

    const ANALYTIC_ATTACHMENT = [
        AnalyticReport2018::class => [
            'admin_code' => [
                'practiceSuccessStoryAttachments' => 'nko_order.admin.analytic_report2018.attachment',
                'practiceSpreadSuccessStoryAttachments' => 'nko_order.admin.analytic_report2018.attachment',
                'monitoringDevelopmentAttachments' => 'nko_order.admin.analytic_report.attachment',
            ],
            'extraFields' => [
                'practiceSocialFeedbackAttachments',
                'practiceSocialSpreadFeedbackAttachments',
            ],
        ],
        AnalyticReport2019::class => [
            'admin_code' => [
                'practiceSuccessStoryAttachments' => 'nko_order.admin.analytic_report2018.attachment',
                'practiceSpreadSuccessStoryAttachments' => 'nko_order.admin.analytic_report2018.attachment',
                'monitoringDevelopmentAttachments' => 'nko_order.admin.analytic_report.attachment',
            ],
            'extraFields' => [],
        ],
        AnalyticReport::class => [
            'extraFields' => [
                'monitoringDevelopmentAttachments',
                'practiceSocialFeedbackAttachments',
                'practiceSocialSpreadFeedbackAttachments',
            ],
            'admin_code' => [
                'practiceSuccessStoryAttachments' => 'nko_order.admin.analytic_report.attachment',
                'practiceSpreadSuccessStoryAttachments' => 'nko_order.admin.analytic_report.attachment',
                'monitoringDevelopmentAttachments' => 'nko_order.admin.analytic_report.attachment',
            ],
        ],
    ];

    const FINANCE_SPENT = [
        FinanceReport2018::class => [
            'admin_code' => [
                'expenseType' => 'nko_order.admin.finance_report.expense_type',
            ],
        ],
        FinanceReport2019::class => [
            'admin_code' => [
                'expenseType' => 'nko_order.admin.finance_report.expense_type',
            ],
        ],
        MixedReport2019::class => [
            'admin_code' => [
                'expenseType' => 'nko_order.admin.finance_report.expense_type',
            ],
            'extraFields' => [
                'summaryIncrementalCosts'
            ],
        ],
        MixedKNS2020Report::class => [
            'admin_code' => [
                'expenseType' => 'nko_order.admin.finance_report.expense_type',
            ],
            'extraFields' => [
                'summaryIncrementalCosts'
            ],
        ],
        MixedReport::class => [
            'admin_code' => [
                'expenseType' => 'nko_order.admin.finance_report.expense_type',
            ],
            'extraFields' => [
                'summaryIncrementalCosts'
            ],
        ],
        FinanceReport::class => [
            'admin_code' => [
                'expenseType' => 'nko_order.admin.expense_type',
            ],
        ]
    ];

    const PROJECT_DESCRIPTION = [
        MixedReport::class => [
            'extraFields' => [
                'summaryIncrementalCosts', 'accountantLabel', 'accountantName', 'accountantPosition', 'reportForm.year'
            ],
            'parameters' => [
                'projectName' => ['attr' => ['readonly' => true]]
            ]
        ],
        MixedKNS2019Report::class => [
            'extraFields' => [
                'summaryIncrementalCosts', 'accountantLabel', 'accountantName', 'accountantPosition', 'reportForm.year'
            ],
            'parameters' => [
                'projectName' => ['attr' => ['readonly' => true]]
            ]
        ],
        MixedKNS2020Report::class => [
            'extraFields' => [
                'summaryIncrementalCosts', 'accountantLabel', 'accountantName', 'accountantPosition', 'reportForm.year'
            ],
            'parameters' => [
                'projectName' => ['attr' => ['readonly' => true]],
                'startDateProject' => [
                    'selectedType' => 'sonata_type_datetime_picker',
                    'dp_min_date' => '23/03/2020',
                    'dp_max_date' => '30/11/2020',
                    'dp_default_date' => '23/03/2020',
                ],
                'finishDateProject' => [
                    'selectedType' => 'sonata_type_datetime_picker',
                    'dp_min_date' => '23/03/2020',
                    'dp_max_date' => '30/11/2020',
                    'dp_default_date' => '23/03/2020',
                ],
            ]
        ],
        AnalyticReport2018::class => [
            'extraFields' => [
                'reportForm.year', 'trainingGroundsSecond'
            ],
        ],
        AnalyticReport2019::class => [
            'extraFields' => [
                'trainingGroundsSecond', 'accountantPosition'
            ],
        ],
        AnalyticReport::class => [
            'extraFields' => [
                'trainingGroundsSecond'
            ],
        ],
        AnalyticReportKNS2018::class => [
            'extraFields' => [
            ],
        ],
        AnalyticHarborReport2019::class => [
            'extraFields' => [
                'trainingGroundsSecond'
            ],
        ],
    ];
}
