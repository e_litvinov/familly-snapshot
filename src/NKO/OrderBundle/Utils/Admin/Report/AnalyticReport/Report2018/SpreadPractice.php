<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 19.7.18
 * Time: 13.16
 */

namespace NKO\OrderBundle\Utils\Admin\Report\AnalyticReport\Report2018;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;

class SpreadPractice
{

    const SPREAD_PRACTICE_CONFIGURATOR = [
        Application::class =>
            [
                'admin_code' => [
                ],
                'extraFields' => [
                ],
                'type_options' => [
                ],
                'class' => Application::class,
            ]
    ];
}