<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 19.7.18
 * Time: 11.48
 */

namespace NKO\OrderBundle\Utils\Admin\Report\AnalyticReport\Report2018;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;

class PriorityDirection
{
    const PRIORITY_DIRECTION_CONFIGURATOR = [
        Application::class =>
        [
            'admin_code' => [
            ],
            'extraFields' => [
            ],
            'type_options' => [
            ],
            'class' => Application::class,
        ],
    ];
}
