<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 12/26/17
 * Time: 6:05 PM
 */

namespace NKO\OrderBundle\Utils\Admin\Report\AnalyticReport;

class ReportAdminValidation
{
    const DEFAULT_VALIDATION_FIELDS = [
        'practiceAnalyticResults',
        'socialSolvedProblems',
        'socialNotSolvedProblems',
        'socialMeasures',
        'nextSocialMeasures',
        'realizationFactors',
        'practiceFeedbackAttachments',
        'practiceSuccessStoryAttachments',
    ];

    const FARVATER_VALIDATION_FIELDS = [
        'expectedAnalyticResults',
        'analyticPublications',
        'practiceSolvedProblems',
        'practiceNotSolvedProblems',
        'resultStability',
        'introductionFactors',
        'monitoringResults',
        'newMonitoringElements',
        'monitoringChanges',
        'monitoringDevelopments',
        'practiceSpreadSuccessStoryAttachments',
        'monitoringAttachments',
        'factorAttachments',
        'practiceSpreadFeedbackAttachments',
    ];

    const KNS_VALIDATION_FIELDS = [];

}