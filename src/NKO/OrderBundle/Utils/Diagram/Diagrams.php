<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.11.18
 * Time: 14.32
 */

namespace NKO\OrderBundle\Utils\Diagram;

use PHPExcel_Chart_DataSeries;
use PHPExcel_Chart_Legend;

class Diagrams
{
    /**
     * This values used only in addCHart method
     *
     *
     */
    const TYPES = [
        [
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,
            PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED,
            PHPExcel_Chart_DataSeries::DIRECTION_BAR,
            null,
            'Линейчатая диграмма'
        ], [
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,
            PHPExcel_Chart_DataSeries::GROUPING_STANDARD,
            PHPExcel_Chart_DataSeries::DIRECTION_COL,
            null,
            'Гистограмма'

        ], [
            PHPExcel_Chart_DataSeries::TYPE_LINECHART,
            null,
            null,
            null,
            'График'
        ], [
            PHPExcel_Chart_DataSeries::TYPE_PIECHART,
            null,
            null,
            PHPExcel_Chart_Legend::POSITION_TOPRIGHT,
            'Круговая диаграмма'
        ]
    ];
}
