<?php

namespace NKO\OrderBundle\Utils\Application;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application;
use NKO\OrderBundle\Entity\Application\Verification\Application2020\Application as Verification2020Application;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\MarkListHistory;
use NKO\UserBundle\Entity\ExpertUser;
use PHPExcel_Cell;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class SummaryTableConfiguration
{
    const OPTIONS = [
        Verification2020Application::class => [
            [
                'label' => '№ п/п',
                'type' => 'fillMethod',
                'value' => 'getCurrentStringNumner'
            ],
            [
                'label' => '№ заявки',
                'type' => 'history',
                'value' => 'getNumber'
            ],
            [
                'label' => 'Наименование организации',
                'type' => 'application',
                'value' => 'getName'
            ],
            [
                'label' => 'Регион',
                'type' => 'application',
                'value' => 'getLegalRegion'
            ],
            [
                'label' => 'Название практики',
                'type' => 'application',
                'value' => 'getProjectName'
            ],
            [
                'type' => 'experts',
                'value' => [
                    [
                        'label' => 'Итоговое решение ',
                        'type' => 'mark',
                        'value' => 'getFinalDecision',
                    ],
                    [
                        'label' => 'Итоговое решение ',
                        'type' => 'mark',
                        'value' => 'getRationaleFinalDecision',
                    ],
                    [
                        'label' => 'Итоговый балл ',
                        'type' => 'mark',
                        'value' => 'getFinalMark',
                    ],
                ],
            ],
            [
                'label' => 'Cредний итоговый балл',
                'type' => 'fillMethod',
                'value' => 'fillAverageMark'
            ],
            [
                'type' => 'experts',
                'value' => [
                    [
                        'label' => 'Эксперт ',
                        'type' => 'expert',
                        'value' => 'getFullName',
                    ]
                ]
            ],
        ],
    ];

    private $applications;

    private $sheet;

    /** @var BaseApplication */
    private $currentApplication;

    /** @var MarkList */
    private $currentSendMark;

    private $currentExpertNumber = 0;

    private $experts;

    /** @var EntityManager */
    private $em;

    /** @var PropertyAccessor */
    private $accessor;

    /** @var int */
    private $expertsCount;

    /** @var int */
    private $applicationNumber = 0;

    /** @var int */
    private $colNumber = 0;

    public function __call($name, $arguments)
    {
        $this->$name($arguments);
    }

    public function __construct($em, $applications, $sheet, $expertsCount)
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $this->sheet = $sheet;
        $this->em = $em;
        $this->applications = $applications;
        $this->expertsCount = $expertsCount;
    }

    // Uses currentApplication and experts and currentExpertNumber
    private function setCurrentSendMark()
    {
        $expert = $this->experts[$this->currentExpertNumber];
        $markListHistories = $this->em->createQueryBuilder()
            ->select('m')
            ->from(MarkListHistory::class, 'm')
            ->where('m.isSend =:isSend')
            ->andWhere('m.competition =:competition')
            ->andWhere('m.author =:author')
            ->andWhere('m.expert IN (:experts)')
            ->setParameters([
                'isSend'=> true,
                'competition'=> $this->currentApplication->getCompetition()->getId(),
                'author' => $this->currentApplication->getAuthor()->getId(),
                'experts'=> [$expert],
            ])
            ->getQuery()
            ->getResult();


        $sendMarkList = array_filter($markListHistories, function ($item) use ($expert) {
            return $item->getExpert()->getId() === $expert->getId();
        });


        if ($sendMarkList) {
            $sendMarkList = is_array($sendMarkList) ? reset($sendMarkList) : $sendMarkList;

            $unserialized = unserialize($sendMarkList->getData());
            $this->currentSendMark = $unserialized;
        }
    }

    private function setCurrentApplication()
    {
        $this->currentApplication = unserialize($this->getCurrentHistory()->getData());
    }

    public function execute($options)
    {
        foreach ($options as $option) {
            $this->fillHead($option);
        }

        foreach ($this->applications as $i => $applicationHistory) {
            $this->applicationNumber = $i;
            $this->colNumber = 0;
            $this->setCurrentApplication();
            foreach ($options as $option) {
                $this->executeOption($option);
            }
        }
    }

    private function fillHead($option, $value = null)
    {
        switch ($option['type']) {
            case 'experts':
                $this->fillExpertsHead($option['value']);
                break;
            default:
                $this->fillSheetHead($option['label'], $value);
                break;
        }
    }

    private function fillExpertsHead($options)
    {
        for ($i = 0; $i < $this->expertsCount; $i++) {
            foreach ($options as $option) {
                $this->fillHead($option, $i + 1);
            }
        }
    }

    private function executeOption($option)
    {
        switch ($option['type']) {
            case 'fillMethod':
                $methodName = ($option['value']);
                $this->fillSheetByValue($this->$methodName());
                break;
            case 'history':
                $this->fillSheetByValue($this->accessor->getValue($this->getCurrentHistory(), $option['value']));
                break;
            case 'application':
                $this->fillSheetByValue($this->accessor->getValue($this->currentApplication, $option['value']));
                break;
            case 'expert':
                $value = null;
                if ($this->getExpert()) {
                    $value = $this->accessor->getValue($this->getExpert(), $option['value']);
                }
                $this->fillSheetByValue($value);
                break;
            case 'mark':
                $value = null;
                if ($this->currentSendMark) {
                    $value = $this->accessor->getValue($this->currentSendMark, $option['value']);
                }
                $this->fillSheetByValue($value);
                break;
            case 'experts':
                $this->executeExpretsOption($option['value']);
                break;
        }
    }

    private function executeExpretsOption($options)
    {
        $this->resetExperts();
        for ($i = 0; $i < $this->expertsCount; $i++) {
            $this->setCurrentSendMark();
            foreach ($options as $option) {
                $this->executeOption($option);
            }
            $this->incrementExpert();
        }
    }

    private function resetExperts()
    {
        $this->experts = $this->getCurrentHistory()->getExperts();
        $this->currentExpertNumber = 0;
    }

    private function getCurrentHistory()
    {
        return $this->applications[$this->applicationNumber];
    }

    private function incrementExpert()
    {
        $this->currentExpertNumber++;
    }

    private function getExpert()
    {
        return isset($this->experts[$this->currentExpertNumber]) ? $this->experts[$this->currentExpertNumber] : null;
    }

    private function getCurrentString()
    {
        return $this->applicationNumber + 2;
    }

    private function getCurrentStringNumner()
    {
        return $this->applicationNumber + 1;
    }

    private function fillAverageMark()
    {
        return number_format($this->getCurrentHistory()->getAverageMark(), 2, ',', ' ');
    }

    public function setCellStyles($cell, $mergeOptions = [])
    {
        $options = [
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_TOP,
                'wrap' => true
            ]
        ];

        $style = $cell->getStyle();
        $style->applyFromArray(array_merge($mergeOptions, $options));
    }

    private function fillSheetByValue($value)
    {
        $this->setCellStyles($this->sheet->setCellValueByColumnAndRow($this->colNumber++, $this->getCurrentString(), $value, true));
    }

    private function fillSheetHead($value, $additionalValue = null)
    {
        $this->sheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($this->colNumber))->setAutoSize(true);
        $this->setCellStyles($this->sheet->setCellValueByColumnAndRow($this->colNumber++, 1, $value.$additionalValue, true), ['fill' => [
                    'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('argb' => '##8fddf0')
                ]
            ]);
    }
}
