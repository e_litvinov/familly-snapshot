<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/23/18
 * Time: 4:14 PM
 */

namespace NKO\OrderBundle\Utils\Application;


class BriefApplication2018ApplicationUtils
{
    const BENEFICIARY_PROBLEMS_FIELD_NAME = 'beneficiaryProblems';
    const OTHER_BENEFICIARY_GROUPS_FIELD_NAME = 'otherBeneficiaryGroups';
}