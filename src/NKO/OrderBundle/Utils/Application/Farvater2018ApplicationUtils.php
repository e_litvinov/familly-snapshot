<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/23/18
 * Time: 4:14 PM
 */

namespace NKO\OrderBundle\Utils\Application;


class Farvater2018ApplicationUtils
{
    const PROJECT_SOCIAL_RESULT_FIELD_NAME = 'projectSocialResults';
    const PROJECT_SOCIAL_INDIVIDUAL_RESULT_FIELD_NAME = 'projectSocialIndividualResults';
    const PROJECT_DIRECT_RESULT_FIELD_NAME = 'projectDirectResults';
    const PROJECT_DIRECT_INDIVIDUAL_RESULT_FIELD_NAME = 'projectDirectIndividualResults';
    const PRACTICE_SOCIAL_RESULT_FIELD_NAME = 'practiceSocialResults';
    const PRACTICE_DIRECT_RESULT_FIELD_NAME = 'practiceDirectResults';
    const PRACTICE_DIRECT_INDIVIDUAL_RESULT_FIELD_NAME = 'practiceDirectIndividualResults';

    const PROJECT_APPLICATION_FIELD_NAME = 'projectApplication';
    const PRACTICE_APPLICATION_LINKED_FIELD_NAME = 'practiceApplication';

    const INTRODUCTION_INDEX = 'Количество организаций, внедривших практику (элементы практики) в свою деятельность';

    const PROJECT_SOCIAL_RESULT_INDICATOR_CODE = 'project_social_result_indicator';
    const PROJECT_DIRECT_RESULT_INDICATOR_CODE = 'project_direct_result_indicator';
    const PRACTICE_DIRECT_RESULT_INDICATOR_CODE = 'practice_direct_result_indicator';

    const SPREAD_PRACTICE_APPLICATIONS = 'spreadPracticeApplications';

    const QUALIFICATION_ITEMS ='qualificationItems';
    const QUALIFICATION_FARVATER_ITEMS ='qualificationFarvaterItems';
}