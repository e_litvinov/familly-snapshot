<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/11/18
 * Time: 10:42 PM
 */

namespace NKO\OrderBundle\Utils\Application;

class TargetGroupProblemUtils
{
    const SPECIALIST_COMPETITION_CHOICES = [
        'СФ' => 1,
        'КнС' => 2,
        'нет' => 0
    ];

    const SPECIALIST_PROBLEMS_FIELD_NAME = 'specialistProblems';
    const BENEFICIARY_PROBLEM_FIELD_NAME ='beneficiaryProblems';

}