<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/23/18
 * Time: 4:14 PM
 */

namespace NKO\OrderBundle\Utils\Application;

class KNS2018ApplicationUtils
{
    const PROJECT_RESULTS_FIELD_NAME = 'projectResults';
    const EMPLOYEE_RESULTS_FIELD_NAME = 'employeeResults';
    const BENEFICIARY_RESULTS_FIELD_NAME = 'beneficiaryResults';

    const EFFECTIVENESS_INDICATOR_KNS_2018_THIRD_STAGE = 'effectiveness_indicator_kns_2018_third_stage';
}
