<?php

namespace NKO\OrderBundle\Utils\Application;

use NKO\OrderBundle\Entity\Application as Application;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as KNS2017SecondStage;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNS2018;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application as KNS2017SecondStage2018;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNS2018SecondStage2019;
use NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018\Application as KNS2018ThirdStage2019;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication as BriefApplication2017;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication2018;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as BriefApplication2016;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNSApplication;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNS2019;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as HarborApplication2019;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Application as KNS2020;
use NKO\OrderBundle\Entity\Application\Harbor\Application2020\Application as HarborApplication2020;
use NKO\OrderBundle\Entity\Application\Verification\Application2020\Application as Verification2020;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application as KNSSecondStage2019;

class AdminConfigurator
{
    //если добавляется описание нового поля, необходимо исключить его из предыдущих заявок
    const BANK_DETAILS = [
        Application::class => [
            'extraFields' => [
                'nameAddressee', 'personalAccount'
            ]
        ],
        KNS2017::class => [
            'extraFields' => [
                'nameAddressee', 'personalAccount'
            ]
        ],
        KNS2017SecondStage::class => [
            'extraFields' => [
                'nameAddressee', 'oKTMO', 'kBK', 'personalAccount'
            ]
        ],
        KNS2018::class => [
            'extraFields' => []
        ],
        KNS2017SecondStage2018::class => [
            'extraFields' => []
        ],
        KNS2018SecondStage2019::class => [
            'extraFields' => []
        ],
        KNS2018ThirdStage2019::class => [
            'extraFields' => []
        ],
        Farvater2017::class => [
            'extraFields' => [
                'nameAddressee', 'oKTMO', 'kBK', 'personalAccount'
            ]
        ],
        ContinuationKNSApplication::class => [],
        KNS2019::class => [],
        HarborApplication2019::class => [],
        HarborApplication2020::class => [],
        Verification2020::class => [],
        KNS2020::class => [],
        KNSSecondStage2019::class => [],
    ];

    const ORGANIZATION_ACTIVITY = [
        Application::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.social_network_link',
            ],
            'extraFields' => [
                'isOrganizationHeadEqualProjectHead', 'organizationForm', 'author.pSRN', 'author.nko_name'
            ]
        ],
        KNS2017::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.kns2017.project',
                'publications' => 'sonata.admin.nko.order.kns2017.publication',
                'siteLinks' => 'sonata.admin.nko.order.kns2017.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.kns2017.social_network_link',
            ],
            'extraFields' => [
                'isOrganizationHeadEqualProjectHead', 'organizationForm', 'author.pSRN', 'author.nko_name'
            ]
        ],
        KNS2017SecondStage::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.kns2017.project',
                'publications' => 'sonata.admin.nko.order.kns2017.publication',
                'siteLinks' => 'sonata.admin.nko.order.kns2017.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.kns2017.social_network_link',
            ],
            'extraFields' => [
                'isOrganizationHeadEqualProjectHead', 'organizationForm', 'author.pSRN', 'author.nko_name'
            ]
        ],
        KNS2017SecondStage2018::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'pSRN'
            ]
        ],
        KNS2018SecondStage2019::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'pSRN'
            ]
        ],
        KNS2018ThirdStage2019::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'pSRN', 'competition', 'author.nko_name'
            ]
        ],
        KNS2018::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'pSRN', 'author.nko_name'
            ]
        ],
        BriefApplication2017::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.brief_application.project',
                'publications' => 'sonata.admin.nko.order.brief_application.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',

            ],
            'extraFields' => [
                'linkToAnnualReport', 'author.nko_name', 'organizationForm', 'author.pSRN', 'headOfAccountingMobilePhoneLabel',
                'headOfAccountingMobilePhoneInternationalCode', 'headOfAccountingMobilePhoneCode', 'headOfAccountingMobilePhone'
            ]
        ],
        BriefApplication2018::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.brief_application.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'linkToAnnualReport', 'headOfAccountingMobilePhoneLabel', 'headOfAccountingMobilePhoneInternationalCode',
                'headOfAccountingMobilePhoneCode', 'headOfAccountingMobilePhone'
            ]
        ],
        BriefApplication2016::class => [
            'admin_code' => [
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'author.nko_name', 'organizationForm', 'author.pSRN', 'headOfAccountingLabel', 'headOfAccountingFullName',
                'headOfAccountingPhoneLabel', 'headOfAccountingPhoneInternationalCode', 'headOfAccountingPhoneCode',
                'headOfAccountingPhone', 'headOfAccountingMobilePhoneLabel', 'headOfAccountingMobilePhoneInternationalCode',
                'headOfAccountingMobilePhoneCode', 'headOfAccountingMobilePhone', 'headOfAccountingEmeil', 'isOrganizationHeadEqualProjectHead'
            ]
        ],
        ContinuationKNSApplication::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ]
        ],
        KNS2019::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => []
        ],
        HarborApplication2019::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'competition', 'author.nko_name', 'pSRN'
            ]
        ],
        KNS2020::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => []
        ],
        HarborApplication2020::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'competition', 'author.nko_name', 'pSRN', 'organizationForm'
            ]
        ],
        Verification2020::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => [
                'competition', 'author.nko_name', 'pSRN'
            ]
        ],
        KNSSecondStage2019::class => [
            'admin_code' => [
                'projects' => 'sonata.admin.nko.order.project',
                'publications' => 'sonata.admin.nko.order.publication',
                'siteLinks' => 'sonata.admin.nko.order.brief_application.site_link',
                'socialNetworkLinks' => 'sonata.admin.nko.order.brief_application.social_network_link',
            ],
            'extraFields' => []
        ],
    ];
}
