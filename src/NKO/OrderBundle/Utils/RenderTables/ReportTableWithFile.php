<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8.7.18
 * Time: 18.13
 */

namespace NKO\OrderBundle\Utils\RenderTables;

use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticKNSReport2018;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedKNSReport;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedKNSReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedKNSReport2020;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;

class ReportTableWithFile
{
    /**
     * ПАРАМЕТРЫ:
     * 1. класс отчета
     * 1.1. название переменной таблицы в отчете
     * 1.1.1. enabled
     *             number (true/null), нужна колонка "номер"
     *             button (true/null), нужна колонка "действие"
     * 1.1.2 level (1/2) сколько уровней в таблице
     * 1.1.3 fieldDescription (TABLES_DESCRIPTION)
     */

    const TABLES = [
        AnalyticReport2018::class => [
            'practiceFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'practiceSpreadFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'factorAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ],
            'publicationAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => null
                ],
                'fieldDescription' =>self::TABLES_DESCRIPTION['publicationAttachments'],
                'level' => 2
            ],
            'materialAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => null
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['materialAttachments'],
                'level' => 1
            ],

            'practiceSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],

            'practiceSpreadSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
            'monitoringDevelopmentAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
        ],
        AnalyticReport2019::class => [
            'practiceFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'practiceSocialFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'practiceSpreadFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'practiceSocialSpreadFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'factorAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => true
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ],
            'publicationAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => null
                ],
                'fieldDescription' =>self::TABLES_DESCRIPTION['publicationAttachments'],
                'level' => 2
            ],
            'materialAttachments' => [
                'enabled' => [
                    'number' => null,
                    'button' => null
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['materialAttachments'],
                'level' => 2
            ],

            'practiceSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],

            'practiceSpreadSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
            'monitoringDevelopmentAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
        ],
        MonitoringReport::class => [
            'documents' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ]
        ],
        Harbor2020MonitoringReport::class => [
            'documents' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ]
        ],
        AnalyticKNSReport2018::class => [
            'practiceFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'practiceSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
        ],
        AnalyticHarborReport2019::class => [
            'practiceFeedbackAttachments' => [
                'enabled' => [
                    'number' => null,
                ],
                'fieldDescription' => self::TABLES_DESCRIPTION['practiceFeedbackAttachments'],
                'level' => 2
            ],
            'practiceSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
            'practiceSpreadSuccessStoryAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments']
            ],
        ],
        MixedKNSReport::class => [
            'analyticAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ],
            'documents' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ]
        ],
        MixedKNSReport2019::class => [
            'analyticAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ],
            'documents' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ]
        ],
        MixedKNSReport2020::class => [
            'analyticAttachments' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ],
            'documents' => [
                'fieldDescription' => self::TABLES_DESCRIPTION['attachments'],
            ]
        ],

    ];

    /**
     * ПАРАМЕТРЫ:
     * название поля с файлом
     * заголовок первого уровня
     * заголовки второго уровня (вложенная коллекция с файлом) / название переменных, которые нужно объеденить
     * название переменной (вложенная коллекция с файлом)
     */

    const TABLES_DESCRIPTION = [
        'attachments' => [
            'file',
        ],
        'practiceFeedbackAttachments' => [
            null,
            'Инструменты сбора обратной связи (pdf, xls или doc)',
            ['Инструмент 1', 'Инструмент 2', 'Инструмент 3'],
            'tools'
        ],
        'publicationAttachments' => [
            'file',
            'Публикация',
            ['author', 'name', 'date', 'mediaName']
        ],

        'materialAttachments' => [
            'file',
            'Информационный / методический материал',
            ['author', 'name', 'date']
        ],
    ];
}
