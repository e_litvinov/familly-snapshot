<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/12/18
 * Time: 2:35 PM
 */

namespace NKO\OrderBundle\Utils;

class ReportTypes
{
    const ANALYTIC_REPORT_KNS_1 = 'kns_1';
    const FINANCE_REPORT_2018 = 'finance_report_2018';
    const ANALYTIC_REPORT_2018 = 'analytic_report_2018';
    const ANALYTIC_REPORT_2018_MEATHURE = 'analytic_report_2018_meathure';
    const MIXED_KNS_REPORT = 'mixed_kns_report';
    const MIXED_KNS_REPORT_2019 = 'mixed_kns_report_2019';
    const MIXED_KNS_REPORT_2020 = 'mixed_kns_report_2020';
    const HARBOR_REPORT_2019 = 'harbor_analytic_2019';
}
