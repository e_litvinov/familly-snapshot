<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 5.10.18
 * Time: 10.38
 */

namespace NKO\OrderBundle\Utils;

use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNS;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application2019\Application as Continuation2019SecondStage;

class Loader
{
    const LOADER_CLASS_NAME = [
        Report::class => '\\Loader',
        Application::class => '\\ContinuationSecondStageApplication\\Loader',
        ContinuationKNS::class => '\ContinuationKNS\Loader',
        Continuation2019SecondStage::class => '\Continuation2019SecondStage\Loader',
    ];
}
