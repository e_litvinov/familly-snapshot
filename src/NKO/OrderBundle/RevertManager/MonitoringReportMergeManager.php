<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/1/18
 * Time: 11:59 AM
 */

namespace NKO\OrderBundle\RevertManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Resolver\CollectionResolver;
use NKO\OrderBundle\Utils\RevertManagerUtils;

class MonitoringReportMergeManager implements MergeManagerInterface, UpdateManagerInterface
{
    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * @var CollectionResolver $collectionResolver
     */
    private $collectionResolver;

    public function __construct(EntityManager $em, CollectionResolver $collectionResolver)
    {
        $this->em = $em;
        $this->collectionResolver = $collectionResolver;
    }

    /**
     * @param $report Report
     * @param $revertReport Report
     * @return mixed|void
     */
    public function merge($report, $revertReport)
    {
        foreach ($revertReport->getMonitoringResults() as $result) {
            $this->em->getRepository(Indicator::class)->find($result->getIndicator()->getId());
            $this->em->merge($result->getIndicator());
        }

        $this->collectionResolver->fetchCollectionByFieldName($report, 'documents');
        foreach ($revertReport->getDocuments() as $document) {
            $this->em->merge($document);
        }
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function preMerge($object, $revertObject)
    {
        // TODO: Implement preMerge() method.
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function postMerge($object, $revertObject)
    {
        // TODO: Implement postMerge() method.
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function preUpdate($object, $revertObject)
    {
        // TODO: Implement preUpdate() method.
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function postUpdate($object, $revertObject)
    {
        // TODO: Implement postUpdate() method.
    }
}
