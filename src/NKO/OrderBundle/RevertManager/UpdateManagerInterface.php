<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/1/18
 * Time: 12:02 PM
 */

namespace NKO\OrderBundle\RevertManager;

interface UpdateManagerInterface
{
    const PRE_UPDATE = 'preUpdate';
    const POST_UPDATE = 'postUpdate';

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function preUpdate($object, $revertObject);

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function postUpdate($object, $revertObject);
}
