<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/1/18
 * Time: 12:02 PM
 */

namespace NKO\OrderBundle\RevertManager;

interface MergeManagerInterface
{
    const PRE_MERGE = 'preMerge';
    const MERGE = 'merge';
    const POST_MERGE = 'postMerge';

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function preMerge($object, $revertObject);

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function merge($object, $revertObject);

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function postMerge($object, $revertObject);
}
