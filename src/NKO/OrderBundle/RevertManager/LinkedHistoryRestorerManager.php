<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 14.12.18
 * Time: 12.08
 */

namespace NKO\OrderBundle\RevertManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;

class LinkedHistoryRestorerManager
{
    /**
     * @var EntityManager $em
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $object BaseApplication
     */
    public function restore(BaseApplication $object)
    {
        $linkedHistory = $object->getLinkedApplicationHistory();
        if ($linkedHistory) {
            $realHistory = $this->em->getRepository(ApplicationHistory::class)->find($linkedHistory->getId());
            $linkedHistory->setData($realHistory->getData());
        }
    }
}
