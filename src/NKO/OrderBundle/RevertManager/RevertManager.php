<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/31/18
 * Time: 4:24 PM
 */

namespace NKO\OrderBundle\RevertManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\EventListener\FileSubscriber;
use NKO\OrderBundle\Resolver\CollectionResolver;
use NKO\OrderBundle\Resolver\ProxyResolver;
use NKO\OrderBundle\Resolver\RelationsResolver;
use NKO\OrderBundle\Utils\RevertManagerUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\OrderBundle\Entity\BaseApplication;

class RevertManager
{
    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var RelationsResolver $relationsResolver
     */
    private $relationsResolver;

    /**
     * @var CollectionResolver $collectionResolver
     */
    private $collectionResolver;

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * @var MergeManagerInterface|UpdateManagerInterface $mergeManager
     */
    private $manager;

    public function __construct(ContainerInterface $container, RelationsResolver $relationsResolver, CollectionResolver $collectionResolver, ProxyResolver $proxyResolver)
    {
        $this->container = $container;
        $this->relationsResolver = $relationsResolver;
        $this->collectionResolver = $collectionResolver;
        $this->proxyResolver = $proxyResolver;
        $this->manager = null;
    }

    public function revert($object)
    {
        $em = $this->container->get('doctrine')->getManager();
        $this->removeFileSubscriber($em);

        $em->getFilters()->disable('softdeleteable');

        $revertObject = unserialize($object->getData());
        $loadedObject = $this->proxyResolver->resolveProxies($revertObject);
        $className = get_class($revertObject);

        if ($this->container->has($className. '\MergeManager')) {
            $this->manager = $this->container->get($className. '\MergeManager');
        }

        if ($this->container->has($className. '\LinkedHistoryRestorer')) {
            $this->container->get($className. '\LinkedHistoryRestorer')->restore($revertObject);
        }

        if ($loadedObject != null) {
            $this->container->get('doctrine')->getConnection()->beginTransaction();

            $this->executeManagerMethod($loadedObject, $revertObject, MergeManagerInterface::PRE_MERGE);
            $this->executeManagerMethod($loadedObject, $revertObject, MergeManagerInterface::MERGE);


            if ($revertObject instanceof BaseApplication) {
                $revertObject->setIsSend(false);
            }

            $em->merge($revertObject);
            $em->flush();

            $this->executeManagerMethod($loadedObject, $revertObject, MergeManagerInterface::POST_MERGE);
            $this->executeManagerMethod($loadedObject, $revertObject, UpdateManagerInterface::PRE_UPDATE);


            $this->update($loadedObject, $revertObject);

            $this->executeManagerMethod($loadedObject, $revertObject, UpdateManagerInterface::POST_UPDATE);

            $em->getFilters()->enable('softdeleteable');

            $this->container->get('doctrine')->getConnection()->commit();
        }

        return $loadedObject;
    }

    private function executeManagerMethod($loadedObject, $revertObject, $method)
    {
        if ($this->manager && method_exists($this->manager, $method)) {
            $this->manager->$method($loadedObject, $revertObject);
        }
    }

    public function update($object, $revertObject)
    {
        $this->relationsResolver->updateRelations($object, new \DateTime);
        $this->relationsResolver->updateRelations($revertObject, null);

        $className = get_class($object);
        if (key_exists($className, RevertManagerUtils::COLLECTIONS_TO_RESOLVE)) {
            foreach (RevertManagerUtils::COLLECTIONS_TO_RESOLVE[$className] as $collection) {
                $this->resolveNestedCollection($object, $revertObject, $collection);
            }
        }
    }

    public function resolveNestedCollection($object, $revertObject, $fieldName)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $collection = $accessor->getValue($object, $fieldName);

        foreach ($collection as $item) {
            $this->relationsResolver->updateRelations($item, new \DateTime);
        }

        $collection = $accessor->getValue($revertObject, $fieldName);
        foreach ($collection as $item) {
            $this->relationsResolver->updateRelations($item, null);
        }
    }

    private function removeFileSubscriber(EntityManager $em)
    {
        $eventManager = $em->getEventManager();
        foreach ($eventManager->getListeners() as $listeners) {
            foreach ($listeners as $hash => $listener) {
                if ($listener instanceof FileSubscriber) {
                    $eventManager->removeEventSubscriber($listener);
                }
            }
        }
    }
}
