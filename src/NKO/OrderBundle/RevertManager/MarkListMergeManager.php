<?php

namespace NKO\OrderBundle\RevertManager;

class MarkListMergeManager implements UpdateManagerInterface
{
    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function preUpdate($object, $revertObject)
    {
        $revertObject->setIsSend(false);
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function postUpdate($object, $revertObject)
    {
    }
}
