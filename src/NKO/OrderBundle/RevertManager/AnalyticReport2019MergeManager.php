<?php

namespace NKO\OrderBundle\RevertManager;

use Doctrine\ORM\EntityManager;

class AnalyticReport2019MergeManager implements MergeManagerInterface
{
    /**
     * @var EntityManager $relationsResolver
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function merge($object, $revertObject)
    {
        $this->em->merge($revertObject->getPracticeIntroduction());
        $this->em->merge($revertObject->getPracticeImplementation());
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function preMerge($object, $revertObject)
    {
        // TODO: Implement preMerge() method.
    }

    /**
     * @param $object
     * @param $revertObject
     * @return mixed
     */
    public function postMerge($object, $revertObject)
    {
        // TODO: Implement postMerge() method.
    }
}
