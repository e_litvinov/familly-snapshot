<?php

namespace NKO\OrderBundle\RevertManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Resolver\RelationsResolver;

class RegisterMergeManager implements UpdateManagerInterface
{
    /**
     * @var EntityManager $em
     */
    private $em;


    private $relationsResolver;

    public function __construct(EntityManager $em, RelationsResolver $relationsResolver)
    {
        $this->em = $em;
        $this->relationsResolver = $relationsResolver;
    }

    public function preMerge($object, $revertObject)
    {
        $registers = $object->getRegisters();
        $revertRegisters = $revertObject->getRegisters();

        foreach ($registers as $item) {
            $this->relationsResolver->updateRelations($item, new \DateTime);
        }

        foreach ($revertRegisters as $item) {
            $this->relationsResolver->updateRelations($item, null);
        }
    }

    public function postUpdate($object, $revertObject)
    {
    }

    public function preUpdate($object, $revertObject)
    {
    }
}
