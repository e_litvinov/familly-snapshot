<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/1/18
 * Time: 12:09 PM
 */

namespace NKO\OrderBundle\RevertManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;

class AnalyticReportMergeManager implements MergeManagerInterface
{
    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * @var Report $revertReport
     */
    private $revertReport;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function mergeCollections($report, $revertReport)
    {
        $this->revertReport = $revertReport;
        $this->em->getRepository(Attachment::class)->findBy(['reportSpreadPracticeSuccessStory' => $report]);
        foreach ($this->revertReport->getPracticeSpreadSuccessStoryAttachments() as $attachment) {
            $this->em->merge($attachment);
        }
    }
}