<?php

namespace NKO\OrderBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class NKOOrderExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('file_generator_services.yml');
        $loader->load('Application/brief_application2017_services.yml');
        $loader->load('Application/kns2017.yml');
        $loader->load('Application/brief_application2016_services.yml');
        $loader->load('Application/farvater2017_services.yml');
        $loader->load('Application/brief_application_2018_services.yml');
        $loader->load('Application/farvater_services.yml');
        $loader->load('Report/analytic_report_services.yml');
        $loader->load('Report/kns_analytic_report_services.yml');
        $loader->load('Report/finance_report_services.yml');
        $loader->load('Report/monitoring_report_services.yml');
        $loader->load('Application/application_services.yml');
        $loader->load('mark_services.yml');
        $loader->load('Report/report_services.yml');
        $loader->load('Application/continuation_application_services.yml');
        $loader->load('Application/continuation_second_stage_2019.yml');
        $loader->load('entity_resolver_services.yml');
        $loader->load('revert_services.yml');
        $loader->load('Application/kns_2017_second_stage_services.yml');
        $loader->load('Application/kns_2018_second_stage_services.yml');
        $loader->load('Application/farvater_application_2018.yml');
        $loader->load('Report/analytic_report_2018_services.yml');
        $loader->load('Report/analytic_report_2019_services.yml');
        $loader->load('Report/analytic_report_2018_kns_services.yml');
        $loader->load('Report/mixed_report_kns_services.yml');
        $loader->load('statistic.yml');
        $loader->load('factory.yml');
        $loader->load('contacts.yml');
        $loader->load('Application/harbor_application_2019_services.yml');
        $loader->load('Report/analytic_report_2019_harbor.yml');
        $loader->load('Application/kns_2018_third_stage_services.yml');
        $loader->load('Application/harbor_application_2020_services.yml');
        $loader->load('Application/verification_application_2020_services.yml');
        $loader->load('Application/kns_2019_second_stage_services.yml');
        $loader->load('Report/harbor_2020_monitoring_report_services.yml');
    }
}
