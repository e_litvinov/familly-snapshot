<?php

namespace NKO\OrderBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class MarkListHistoryAdminController extends AdminController
{
    public function revertMarkAction(Request $request)
    {
        $object = $this->admin->getSubject();
        $this->get('nko.order.revert_manager')->revert($object);

        $service = $this->container->get('NKO\LoggerBundle\Loggers\MarkListLogger');
        $service->logAction('Восстановление оценки', unserialize($object->getData()));

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
