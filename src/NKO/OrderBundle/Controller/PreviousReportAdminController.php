<?php

namespace NKO\OrderBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\FinanceReport\PrevReport as FinancePreviousReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PrevReport as AnalyticPreviousReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PrevReport as MonitoringPrevReport;

class PreviousReportAdminController extends CRUDController
{
    public function addReportsAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        $reports = [];
        $reports['analytic'] = new AnalyticPreviousReport();
        $reports['finance'] = new FinancePreviousReport();
        $reports['monitoring'] = new MonitoringPrevReport();

        $reportForms = $this->get('doctrine')->getManager()
            ->getRepository('NKOOrderBundle:Report\ReportForm')->findAll();

        return $this->render('NKOOrderBundle:CRUD/report:previous_reports_form.html.twig', [
            'reports' => $reports,
            'reportForms' => $reportForms
        ]);

    }
}
