<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/21/17
 * Time: 2:50 PM
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\MarkList;
use NKO\UserBundle\Entity\ExpertUser;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\HttpFoundation\Response;

class WorkOfExpertsController extends Controller
{
    /**
     * List action.
     *
     * @return Response
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();

        $formView = $datagrid->getForm()->createView();


        if ($datagrid->getValues()
            && array_key_exists('competitions', $datagrid->getValues())
            && $datagrid->getValues()['competitions']
            && array_key_exists('value', $datagrid->getValues()['competitions'])
        ) {
            $competitonId = $datagrid->getValues()['competitions']['value'];
            $em = $this->getDoctrine()->getManager();
            $competition = $em->getRepository("NKOOrderBundle:Competition")->find($competitonId);

            $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
            $service->logAction('Выгрузка светофорика оценок', $competition->getName());

            /** @var ExpertUser $expert */
            foreach ($datagrid->getResults() as $expert) {
                if ($expert->getCompetitions()->contains($competition)) {
                    $expert->setStatus($competition);
                }
            }
        }

        // set the theme for the current Admin Form


        return $this->render($this->admin->getTemplate('list'), array(
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ), null);
    }

    public function cleanExpertActivityAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $em = $this->getDoctrine()->getManager();
        $experts = $em->getRepository(ExpertUser::class)->findExpertsByCompetition($competitionId);
        $markLists = $em->getRepository(MarkList::class)->findMarksByExpert($competitionId, $experts);
        $historyManager = $this->get('mark_list_history_manager');

        foreach ($experts as $expert) {
            $expert->setLastLogin(null);
        }

        foreach ($markLists as $markList) {
            $em->remove($markList);
            $historyManager->removeByMarkList($markList);
        }

        $em->flush();

        $this->addFlash('sonata_flash_success', $this->get('translator')->trans('cleaned_expert_activity_successfully'));
        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
