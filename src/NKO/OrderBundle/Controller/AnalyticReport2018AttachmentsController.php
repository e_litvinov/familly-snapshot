<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 12.7.18
 * Time: 15.36
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Utils\Report\AttachmentParameters;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticReportKNS2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;

class AnalyticReport2018AttachmentsController extends AttachmentController
{
    const OPTIONS =[
        AnalyticReport2018::class => 'ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT2018',
        AnalyticReport2019::class => 'ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT2019',
        AnalyticReportKNS2018::class => 'ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT_KNS2018',
        MixedReport2019::class => 'ATTACHMENTS_MIXED_REPORT_KNS2019',
        MixedReport2020::class => 'ATTACHMENTS_MIXED_REPORT_KNS2019',
        AnalyticHarborReport2019::class => 'ATTACHMENTS_PARAMETERS_ANALYTIC_HARBOR_REPORT2019',
    ];

    public function attachmentsListAction(Request $request)
    {
        $report = null;
        $em = $this->container->get('Doctrine')->getManager();

        if (!$request->get('id')) {
            return $this->redirectToRoute('admin_nko_order_basereport_list');
        }

        $id = $request->get('id');

        if ($request->get('isHistory')) {
            $reportHistory = $em->getRepository(ReportHistory::class)->find($id);
            $report = unserialize($reportHistory->getData());
        } else {
            $report = $em->getRepository(BaseReport::class)->find($id);
        }

        if ($report) {
            return $this->generateResponse($report);
        }

        return $this->redirectToRoute('admin_nko_order_basereport_list');
    }

    private function generateResponse(BaseReport $report)
    {
        $const = (new \ReflectionClass(AttachmentParameters::class))->getConstant(self::OPTIONS[get_class($report)]);
        $attachments = $this->attachmentsList($report, $const);

        return $this->render('@NKOOrder/CRUD/report/analytic_report/attachments_list.html.twig', ['attachments' => $attachments]);
    }
}
