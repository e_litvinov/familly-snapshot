<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 28.03.17
 * Time: 10:31
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\MarkListHistory;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Resolver\MarkList\AbstractMarkListResolver;
use NKO\OrderBundle\Utils\ApplicationResolver;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report as AnalyticKNS1Report;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as Finance2018Report;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as Finance2019Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticReportKNS2018;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedKNSReport;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedKNSReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedKNSReport2020;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as AnalyticHarborReport2019;
use NKO\OrderBundle\Entity\OriginFileName;

class AdminController extends FormCachingController
{
    const ENDING_DOWNLOADING_SERVICE = '\GenerateDoc';
    const ENDING_ENTITY_RESOLVER = '\EntityResolver';
    const ENDING_MARK_LIST_APPLICATION_RESOLVER = '\MarkListApplicationResolver';

    //для этих конкурсов(id) генерация pdf оценок и сводной таблицы осуществляется по MarkList, а не MarkListHistory
    const MARKS_BY_COMPETITION_WITHOUT_HISTORY = [
        3, 6, 7, 8, 10, 13, 14, 15, 16, 17, 18, 22, 23, 30, 31, 35, 36, 37, 38, 45, 46
    ];

    public function downloadAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw $this->createNotFoundException('unable to find the object');
        }

        $applicationHistory = [];

        $nameOfAction = 'Скачивание заявки в Мастере заявки.';

        if ($object instanceof ApplicationHistory) {
            $applicationHistory = $object;
            $object = unserialize($object->getData());
            $nameOfAction = 'Скачивание заявки из истории';
        } elseif ($object->getIsAutosaved()) {
            $em = $this->getDoctrine()->getManager();

            $history = $em->getRepository(ApplicationHistory::class)
                ->findLastManualSubmittedHistory($object);
            if (!$history) {
                $history = $em->getRepository(ApplicationHistory::class)
                    ->findLastManualSubmittedHistory($object, false);
            }
            $object = unserialize($history->getData());
            $nameOfAction = 'Скачивание заявки в Мастере заявки(последней сохранённой вручную версии).';
        }

        $service = $this->container->get('NKO\LoggerBundle\Loggers\ApplicationLogger');
        $service->logAction($nameOfAction, $object);

        $response = $this->get(get_class($object).self::ENDING_DOWNLOADING_SERVICE)->generate($object, $applicationHistory);

        if (!$response) {
            $this->addFlash('sonata_flash_error', 'File generating failed');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        return $response;
    }

    public function showDocumentsListAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw $this->createNotFoundException('unable to find the object');
        }

        $unrealized = unserialize($object->getData());

        $className = get_class($unrealized);
        if (array_key_exists(
            $className,
            ApplicationResolver::APPLICATION_DOCUMENT_LIST_TEMPLATES
        )) {
            $service = $this->container->get('NKO\LoggerBundle\Loggers\ApplicationLogger');
            $service->logAction('Скачивание приложения к заявке', $unrealized);

            $files = key_exists($className, ApplicationResolver::APPLICATION_DOCUMENT_LIST_FILES)
                ? ApplicationResolver::APPLICATION_DOCUMENT_LIST_FILES[$className]
                : [];
            return $this->render(
                ApplicationResolver::APPLICATION_DOCUMENT_LIST_TEMPLATES[$className],
                [
                    'object' => $object,
                    'files' => $files
                ]
            );
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function downloadDocumentAction(Request $request)
    {
        $document = $request->get('document');

        $object = $this->admin->getSubject();

        if (!$object) {
            throw $this->createNotFoundException('unable to find the object');
        }

        if (get_class($object) == ApplicationHistory::class) {
            $object = unserialize($object->getData());

            $resolver = $this->get('itm.file.preview.path.resolver');
            $path = $resolver->getPath($object, $document);

            if ($path != null) {
                $response = new StreamedResponse();
                $response->setCallback(function () use ($path) {
                    echo file_get_contents($path);
                });
                $content_type = 'application/pdf';
                if ($document == 'budget') {
                    $content_type = 'application/vnd.ms-excel';
                }

                $response->headers->set('Content-Type', $content_type);
                $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, basename($path));
                $response->headers->set('Content-Disposition', $contentDisposition);

                return $response;
            }
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function redirectToDocumentsListAction()
    {
        $report = $this->admin->getSubject();
        $isHistory = '0';
        $id = $report->getId();

        $nameOfAction = 'Скачивание приложения к отчету из Мастера';
        if ($report instanceof ReportHistory) {
            $report = unserialize($report->getData());
            $isHistory = '1';
            $nameOfAction = 'Скачивание приложения к отчету из истории';
        }

        $service = $this->container->get('NKO\LoggerBundle\Loggers\ReportLogger');
        $service->logAction($nameOfAction, $report);

        switch (get_class($report)) {
            case MonitoringReport::class:
                return new RedirectResponse($this->generateUrl('admin_nko_order_report_monitoringreport_monitoringdocument_list', ['filter[report][value]' => $report->getId()]));
            case Harbor2020MonitoringReport::class:
                return new RedirectResponse($this->generateUrl('admin_nko_order_report_monitoringreport_monitoringdocument_list', ['filter[harbor2020Report][value]' => $report->getId()]));
            case AnalyticReport::class:
                return new RedirectResponse($this->generateUrl('analytic_report_attachments_path', ["id" => $report->getId()]));
            case AnalyticKNS1Report::class:
                return new RedirectResponse($this->generateUrl('admin_nko_order_report_analyticreport_kns_stage1_attachment_list', ['filter[report][value]' => $report->getId()]));
            case Finance2018Report::class:
                return new RedirectResponse($this->generateUrl('admin_nko_order_report_financereport_report2018_document_list', ['report_id' => $report->getId()]));
            case Finance2019Report::class:
                return new RedirectResponse($this->generateUrl('admin_nko_order_report_financereport_report2019_document_list', ['report_id' => $report->getId()]));
            case AnalyticReport2018::class:
                return new RedirectResponse($this->generateUrl('analytic_report2018_attachments_path', ['id' => $id, 'isHistory' => $isHistory]));
            case AnalyticReport2019::class:
                return new RedirectResponse($this->generateUrl('analytic_report2018_attachments_path', ['id' => $id, 'isHistory' => $isHistory]));
            case AnalyticReportKNS2018::class:
                return new RedirectResponse($this->generateUrl('analytic_report2018_attachments_path', ['id' => $id, 'isHistory' => $isHistory]));
            case MixedKNSReport::class:
                return new RedirectResponse($this->generateUrl('download_mixed_report_attachment_path_edit', ['id' => $report->getId()]));
            case MixedKNSReport2019::class:
                return new RedirectResponse($this->generateUrl('analytic_report2018_attachments_path', ['id' => $id, 'isHistory' => $isHistory]));
            case MixedKNSReport2020::class:
                return new RedirectResponse($this->generateUrl('analytic_report2018_attachments_path', ['id' => $id, 'isHistory' => $isHistory]));
            case AnalyticHarborReport2019::class:
                return new RedirectResponse($this->generateUrl('analytic_report2018_attachments_path', ['id' => $id, 'isHistory' => $isHistory]));
            default:
                return new RedirectResponse($this->generateUrl('admin_nko_order_documentreport_list', ["filter[report][value]" => $report->getId()]));
        }
    }

    public function downloadMarkAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $object = $this->admin->getSubject();
        $class = get_class($object);
        /** @var MarkList $markList */
        $markList = null;
        $mode = null;

        switch ($class) {
            case ApplicationHistory::class:
                $nameOfAction = 'Скачивание оценки из истории заявки.';
                $markId = $request->get('mark_list_id');
                $markList = $em->getRepository(MarkList::class)->find($markId);
                break;
            case MarkListHistory::class:
                $nameOfAction = 'Скачивание оценки из истории оценки.';
                $markList = unserialize($object->getData());

                // Когда сериализуются объекты унаследованые от FOS\UserBundle\Model\User,
                // они теряют все поля, кроме стандарных
                $markList->setExpert($object->getExpert());
                $mode = true;

                break;
            case MarkList::class:
                $nameOfAction = 'Скачивание оценки из объекта.';
                $markList = $object;
                break;
        }
        $service = $this->container->get('NKO\LoggerBundle\Loggers\MarkListLogger');
        $service->logAction($nameOfAction, $markList);

        //генерация pdf и сводной таблицы по markListHistory или markList
        $mode = $mode ?: in_array($markList->getApplication()->getCompetition()->getId(), self::MARKS_BY_COMPETITION_WITHOUT_HISTORY);
        $markList = $mode ? $markList : $this->getSendMarkListHistory($markList);

        $response = $this->downloadMark($markList);

        if (!$response) {
            $this->addFlash('sonata_flash_error', 'PDF generating failed');
            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        return $response;
    }

    protected function downloadMark(MarkList $mark)
    {
        $application = unserialize($mark->getApplication()->getData());
        $expert = $mark->getExpert();

        $application = $this->get('nko.resolver.proxy_resolver')->resolveProxies($application);
        /** @var AbstractMarkListResolver $resolver*/
        $resolver = $this->get(get_class($application).self::ENDING_MARK_LIST_APPLICATION_RESOLVER);

        $html = $this->renderView($resolver->getTemplate(), $resolver->generateArrayForTwig($mark, $application));
        $fileName = $expert->getFullName(). '-'. $mark->getApplication()->getNumber();

        return $this->get('nko_order.downloader.pdf_downloader')->getStreamedResponse($html, $fileName);
    }

    public function getReportHistoryDocumentsAction()
    {
        $reportHistory = $this->admin->getSubject();
        $report = unserialize($reportHistory->getData());

        $author = $this->getDoctrine()->getRepository(NKOUser::class)->find($report->getAuthor());
        $report->setAuthor($author);

        $factory = $this->container->get('nko_order.factory.documents_list_view');

        $service = $this->container->get('NKO\LoggerBundle\Loggers\ReportLogger');
        $service->logAction('Скачивание приложения к отчету из истории', $report);

        try {
            $viewGenerator = $factory->create($report);
        } catch (\Exception $exception) {
            return $this->redirectToDocumentsListAction();
        }

        return $this->render($viewGenerator->getTemplate(), $viewGenerator->getParameters());
    }

    public function getSendMarkListHistory($markList)
    {
        //cкачивание пдф только для последней отправленной оценки
        $em = $this->getDoctrine()->getManager();
        $history = $em->getRepository(MarkListHistory::class)->findSentMarkListHistory($markList);

        $outMarkList = null;
        if ($history) {
            $outMarkList = unserialize($history->getData());
            $outMarkList->setExpert($markList->getExpert());
        }

        if (!$outMarkList) {
            $this->addFlash('sonata_flash_error', 'This mark do not send');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        return $outMarkList;
    }
}
