<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.08.16
 * Time: 14:56
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Resolver\CacheResolver;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ApplicationAdminController extends BaseApplicationAdminController
{
    public function createAction()
    {
        if (!$this->isGranted('ROLE_SONATA_ADMIN_NKO_ORDER_BASE_APPLICATION_CREATE', $this->admin)) {
            $this->addFlash('sonata_flash_error', 'Application with this competition already exist');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $request = $this->getRequest();
        $object = $this->admin->getNewInstance();

        $entityResolverService = $this->container->get(get_class($object).self::ENDING_ENTITY_RESOLVER);
        $created = $entityResolverService->initCreate($object, $request, ['user' => $this->getUser()]);

        if (!$created) {
            $this->addFlash(
                'sonata_flash_error',
                $this->admin->trans(
                    'flash_nko_error',
                    array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                    'SonataAdminBundle'
                )
            );
            return $this->redirectToRoute('admin_nko_order_baseapplication_list');
        }
        $object = $this->admin->create($object);

        $entityResolverService->afterSubmitCreate($object, null);

        $this->saveReferredRoute(sprintf('create_application_%d', $object->getCompetition()->getId()), true);

        return $this->redirectTo($object);
    }

    public function editAction($id = null)
    {

        try {
            $this->admin->checkAccess('edit', $this->admin->getSubject());
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', 'Error_no_edit_application');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';
        $isMethodGet = $request->isMethod('GET');

        $cache = $this->get('nko_order.resolver.cache_resolver');
        $cachedForm = $cache->getAdminObject($this->admin, $id);
        if ($isMethodGet && $cachedForm) {
            $this->admin->setNonGroups();
        }

        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        $this->setFlagsBeforeEdit($object);

        $entityResolverService = $this->container->get(get_class($object).self::ENDING_ENTITY_RESOLVER);
        $entityResolverService->initEdit($object, $request);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $preResponse = $this->preEdit($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);
        $form = $this->admin->getForm();
        $form->setData($object);

        // РЕДИС
        if ($isMethodGet && $cachedForm) {
            $this->setCachedForm($form, $cachedForm);
        }
        //--------

        $form->handleRequest($request);

        if ($form->isSubmitted() && !$isMethodGet) {
            $cache->delete();
            $cachedForm = null;

            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($object);
            }
            $isFormValid = $form->isValid();

            $entityResolverService->afterSubmitEdit($object, $form, ['user' => $this->getUser()]);
            $entityResolverService->logSubmitEdit($object, $form, ['request' => $request]);

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                try {
                    $entityResolverService->afterSubmitValidEdit($object, $form, ['request' => $request]);

                    $this->checkAutosaveVersion($object);

                    $object = $this->admin->update($object);

                    //$this->admin->postUpdateFinaly($object);
                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object),
                            'objectName' => $this->escapeHtml($this->admin->toString($object)),
                        ), 200, array());
                    }

                    $messageId = 'flash_create_application_success';
                    if ($object->getIsFirstTimeSent()) {
                        $messageId = 'flash_edit_and_send_application_success';
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            $messageId,
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->admin->trans('flash_lock_error', array(
                        '%name%' => $this->escapeHtml($this->admin->toString($object)),
                        '%link_start%' => '<a href="' . $this->admin->generateObjectUrl('edit', $object) . '">',
                        '%link_end%' => '</a>',
                    ), 'SonataAdminBundle'));
                }
            } else {
                $this->checkAutosaveVersion($object);
                $object = $this->admin->update($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->admin->trans(
                            'flash_edit_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        } elseif (!$this->isReferredFromCreateRoute(sprintf('create_application_%d', $object->getCompetition()->getId()))) {
            $entityResolverService->submitAlternative($object, $form, ['request' => $request]);
        }

        $view = $form->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ), null);
    }
}
