<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.08.16
 * Time: 14:56
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Mark;
use NKO\OrderBundle\Entity\QuestionMark;
use NKO\UserBundle\Entity\ExpertUser;
use Symfony\Component\Form\Extension\Validator\Constraints\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use NKO\OrderBundle\Entity\MarkListHistory;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNS2018SecondStage;

class MarkListAdminController extends AdminController
{
    protected function preCreate(Request $request, $object)
    {
        $id = $request->get('id');
        $object->setApplicationId($id);
        $app = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(ApplicationHistory::class)
            ->findOneBy(['id' => $id]);

        if ($app) {
            $session = $request->getSession();
            $session->set('competition_id', unserialize($app->getData())->getCompetition()->getId());
            $session->set('coefficient', $this->get('nko.sum_mark_manager')->getCoefficient($app));
        }
    }

    public function createAction()
    {
        $request = $this->getRequest();
        $object = $this->admin->getNewInstance();

        /**
         * @var ExpertUser $current_user
         */
        $current_user = $this->get('security.token_storage')->getToken()->getUser();

        if (get_class($current_user) != 'NKO\UserBundle\Entity\ExpertUser') {
            $this->addFlash(
                'sonata_flash_error',
                $this->admin->trans(
                    'flash_nko_error',
                    array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                    'SonataAdminBundle'
                )
            );

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $preResponse = $this->preCreate($request, $object);

        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        $appHistory_id = $this->getRequest()->get('id');

        $appHistory = $this->getDoctrine()
            ->getRepository(ApplicationHistory::class)
            ->find($appHistory_id);

        $competition = unserialize($appHistory->getData())->getCompetition();

        $markQuestion = $this
            ->getDoctrine()
            ->getRepository(QuestionMark::class)
            ->findByCompetition($competition->getId());

        foreach ($markQuestion as $question) {
            $mark = new Mark();
            $mark->setQuestionMark($question);
            $object->addMark($mark);
            $mark->setMarkList($object);
        }

        $app = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(ApplicationHistory::class)
            ->findOneBy(['id' => $appHistory_id]);

        $object->setApplication($app);
        $app->addMarkList($object);

        if ($this->isFinishEstimate($object)) {
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $object = $this->admin->create($object);

        $session = $request->getSession();
        $session->set('create', true);

        return $this->redirectTo($object);
    }

    /**
     * @param null $id
     * @return null|RedirectResponse|Response
     * @throws \Exception
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function editAction($id = null)
    {
        $request = $this->getRequest();

        $isMethodGet = $request->isMethod('GET');

        $cache = $this->get('nko_order.resolver.cache_resolver');
        $cachedForm = $cache->getAdminObject($this->admin, $id);
        if ($isMethodGet && $cachedForm) {
            $this->admin->setNonGroups();
        }

        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $this->admin->checkAccess('edit', $object);

        $preResponse = $this->preEdit($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        if ($this->isFinishEstimate($object)) {
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        /** @var $form Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        // РЕДИС
        if ($isMethodGet && $cachedForm) {
            $this->setCachedForm($form, $cachedForm);
        }
        //--------

        $form->handleRequest($request);
        $isSend =  $object->getIsSend();
        $object->setIsSend(false);


        $service = $this->container->get('NKO\LoggerBundle\Loggers\MarkListLogger');
        if ($form->isSubmitted()  && !$isMethodGet) {
            $nameOfAction = 'Сохранение оценки';

            $cache->delete();

            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($object);
            }

            $isFormValid = $form->isValid();
            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                try {
                    $object->setIsValid(true);

                    if ($request->get('send') == "sendButton") {
                        $nameOfAction = 'Отправка оценки';
                        $object->setIsSend(true);
                        if ($object->getIsFirstTimeSend() == false) {
                            $object->setIsFirstTimeSend(true);
                        }
                    }

                    $this->checkAutosaveVersion($object);
                    $service->logAction($nameOfAction, $object);
                    $object = $this->admin->update($object);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson([
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object),
                            'objectName' => $this->escapeHtml($this->admin->toString($object)),
                        ], 200, []);
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            'flash_mark_create_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                    if ($request->get('send') == "sendButton") {
                        $this->addFlash(
                            'sonata_flash_success',
                            $this->admin->trans(
                                'flash_mark_send_success',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );
                    }

                    // redirect to edit mode
                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->admin->trans('flash_lock_error', array(
                        '%name%' => $this->escapeHtml($this->admin->toString($object)),
                        '%link_start%' => '<a href="'.$this->admin->generateObjectUrl('edit', $object).'">',
                        '%link_end%' => '</a>',
                    ), 'SonataAdminBundle'));
                }
            }
            // show an error message if the form failed validation
            if (!$isFormValid) {
                $object->setIsValid(false);
                $object->setIsSend(false);
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->admin->trans(
                            'flash_create_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            'flash_create_mark',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                    if ($request->get('send') == "sendButton") {
                        $this->addFlash(
                            'sonata_flash_error',
                            $this->admin->trans(
                                'flash_mark_send_error',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );
                    }
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }


            $this->checkAutosaveVersion($object);
            $service->logAction($nameOfAction, $object);
            $object = $this->admin->update($object);
        } else {
            if ($request->headers->get('referer') != $request->getUri()) {
                $object->setIsSend($isSend);
                $nameOfAction = 'Открытие оценки в редакторе';
                $service->logAction($nameOfAction, $object);
            }
        }



        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ), null);
    }

    public function listAction()
    {
        return new RedirectResponse($this->generateUrl('admin_sonata_nko_order_expert_mark_list'));
    }

    private function isFinishEstimate($object)
    {
        /**
         * @var ExpertUser $current_user
         */
        $current_user = $this->get('security.token_storage')->getToken()->getUser();
        if ($current_user->getCompetitions()->contains($object->getApplication()->getCompetition()) &&
            $object->getApplication()->getCompetition()->getFinishEstimate() < new \DateTime()
        ) {
            $this->addFlash(
                'sonata_flash_error',
                $this->admin->trans(
                    'flash_finish_estimate_error',
                    array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                    'SonataAdminBundle'
                )
            );

            return true;
        }
        return false;
    }

    public function checkAutosaveVersion($object)
    {
        $em = $this->getDoctrine()->getManager();
        $prevHistory = $em->getRepository(MarkListHistory::class)->findSentMarkListHistory($object);

        //если эксперт сам отправил оценку и она валидна. предыдущая история становится неотправленной
        if ($object->getIsSend()) {
            $object->setIsAutosaved(null);
            if ($prevHistory) {
                $prevHistory->setIsSend(false);
            }
            return;
        }

        //автосохранение или просто сохранение.
        //если есть отправленная история, то в MarkList статус отправленности не должен меняться
        if ($prevHistory) {
            $object->setIsAutosaved(true);
            $object->setIsSend(true);
        }
    }
}
