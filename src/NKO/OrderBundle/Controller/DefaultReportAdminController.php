<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 17.9.18
 * Time: 16.29
 */

namespace NKO\OrderBundle\Controller;

use NKO\LoggerBundle\Loggers\ReportLogger;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceReportInterface;
use NKO\OrderBundle\EntityResolver\Report\DefaultReportResolver;
use Sonata\AdminBundle\Exception\LockException;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultReportAdminController extends BaseReportAdminController
{
    private $isSendPreviousVersion = null;

    public function createAction()
    {
        $request = $this->getRequest();

        $class = new \ReflectionClass($this->admin->hasActiveSubClass() ? $this->admin->getActiveSubClass() : $this->admin->getClass());

        if ($class->isAbstract()) {
            return $this->render(
                'SonataAdminBundle:CRUD:select_subclass.html.twig',
                array(
                    'base_template' => $this->getBaseTemplate(),
                    'admin' => $this->admin,
                    'action' => 'create',
                ),
                null,
                $request
            );
        }

        $object = $this->admin->getNewInstance();

        $preResponse = $this->preCreate($request, $object);

        if ($preResponse !== null) {
            return $preResponse;
        }

        $reportFormId = $request->getSession()->get('report_form_id');
        $preResponse = $this->preSetValues($reportFormId, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        //Создание сервиса(Resolver)
        /** @var DefaultReportResolver $service */
        $service = $this->container->get(get_class($object).self::ENDING_ENTITY_RESOLVER);
        $this->admin->setSubject($object);

        $user = $this->getUser();
        $user->addReport($object);
        $object->setAuthor($user);
        $object->setUserBlockedFinanceReport(0);
        $object->setIsBlocked(false);
        $object->setPsrn($user->getPsrn());

        //Выполнение всего, что надо с объектом и, если надо, перенаправление запроса(Redirect) или выведение флашей
        /** @var  array|bool $response */
        $response = $service->initCreate($object, $request, [
            'user' => $this->getUser(),
            'byPeriod' => $request->get('period_id') ? true : null
        ]);

        if ($response) {
            if (key_exists('flash', $response)) {
                $this->addFlash($response['flash']['type'], $response['flash']['message']);
            }
            if (key_exists('redirectTo', $response)) {
                if ($response['redirectTo'] == 'list') {
                    return new RedirectResponse($this->generateUrl(self::BASE_LIST_ROUTE));
                }
                return new RedirectResponse($this->admin->generateUrl($response['redirectTo']));
            }
        }

        $object = $this->admin->create($object);
        $service->afterSubmitCreate($object, null);

        return $this->redirectTo($object);
    }

    public function editAction($id = null)
    {
        $request = $this->getRequest();
        $templateKey = 'edit';
        $isMethodGet = $request->isMethod('GET');
        $service = $this->get('NKO\LoggerBundle\Loggers\ReportLogger');
        /** @var $service ReportLogger */
        $cache = $this->get('nko_order.resolver.cache_resolver');
        $cachedForm = $cache->getAdminObject($this->admin, $id);
        if ($isMethodGet && $cachedForm) {
            $this->admin->setNonGroups();
        }

        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        /**
         * check permission to edit any report
         */
        if (!$this->isGranted('BASE_REPORT_EDIT_VOTER', $object)) {
            $this->addFlash(
                'sonata_flash_error',
                'Отчёт не доступен для редактирования'
            );
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
            return new RedirectResponse($url);
        }

        /**
         * check permission to edit finance report
         */
        if (!$this->isGranted('ROLE_SONATA_ADMIN_NKO_ORDER_FINANCE_REPORT_EDIT', $object)) {
            $this->addFlash(
                'sonata_flash_error',
                'Отчёт не доступен для редактирования'
            );
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
            return new RedirectResponse($url);
        }

        $this->setFlagsBeforeEdit($object);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        /** @var DefaultReportResolver $service */
        $service = $this->container->get(get_class($object).self::ENDING_ENTITY_RESOLVER);

        $this->admin->checkAccess('edit', $object);

        $preResponse = $this->preEdit($request, $object);

        if ($preResponse !== null) {
            return $preResponse;
        }

        $response = $service->initEdit($object, $request);

        if (is_array($response) && key_exists('flash', $response)) {
            $this->addFlash($response['flash']['type'], $response['flash']['message']);
        }

        $this->admin->setSubject($object);

        $metaForm = $this->admin->getForm();
        if ($service->isDoubleSubmit()) {
            // тяжеляк.
            // подгрузка зависимых связанных объектов на форму осуществляется только после перезагрузки объекта в форму.
            // для этого каждый раз получаем клон от чистой формы, который заполняется данными.
            // таким образом при двух сабмитах сабмитятся две независимые формы
            // в результате чего после второго сабмита на форме есть и ошибки валидации и подгруженные связанные сущности.
            $form = clone $metaForm;
        } else {
            $form = $metaForm;
        }

        $form->setData($this->admin->getSubject());
        // РЕДИС
        if ($isMethodGet && $cachedForm) {
            $clonedForm = clone $form;
            try {
                $this->setCachedForm($form, $cachedForm);
            } catch (\Exception $e) {
                $cache->logError($e->getMessage());
                $cache->delete();
                $form = $clonedForm;
            }
        }
        //--------

        $form->handleRequest($request);

        $this->blockReports($object, $this->getUser()->getId(), true);

        if ($form->isSubmitted() && !$isMethodGet) {
            $cache->delete();

            //Это будет вызываться, если нужно какое-то другое действие после сабмита
            $localResponse = $service->skipUpdate($object, $form, ['request' => $request]);
            if ($localResponse) {
                if (key_exists('flash', $localResponse)) {
                    $this->addFlash($localResponse['flash']['type'], $localResponse['flash']['message']);
                }
                if (key_exists('redirectTo', $localResponse)) {
                    return new RedirectResponse($this->admin->generateUrl($localResponse['redirectTo']));
                }
            } else {
                //TODO: remove this check for 4.0
                if (method_exists($this->admin, 'preValidate')) {
                    $this->admin->preValidate($object);
                }

                $isFormValid = $form->isValid();
                $object->setIsSend(false);
                $service->afterSubmitEdit($object, $form, []);
                $service->logSubmitEdit($object, $form, ['request' => $request]);

                if ($request->get('completeEdit') == "completeEditionButton") {
                    $service->setIsSendAfterComplete($object, $this->isSendPreviousVersion);
                    $service->preCompleteEdit($object);

                    $url = $this->generateUrl(self::BASE_LIST_ROUTE);
                    if ($object->getUserBlockedFinanceReport() != $this->getUser()->getId()) {
                        $this->addFlash(
                            'sonata_flash_success',
                            'Отчёт заблокирован другим пользователем'
                        );
                        return new RedirectResponse($url);
                    }

                    $this->blockReports($object, 0, false);
                    $this->addFlash(
                        'sonata_flash_success',
                        $this->getCompleteEditionFlash($object)
                    );
                    $this->admin->update($object);

                    return new RedirectResponse($url);
                }

                if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved()) && $service->isAdditionConditions()) {
                    $object->setIsValid(true);

                    $service->afterSubmitValidEdit($object, $form);

                    $this->checkAutosaveVersion($object);

                    try {
                        if ($request->get('send') == "sendButton") {
                            $object->setIsFirstTimeSent(true);
                            $object->setIsSend(true);
                        }


                        $object = $this->admin->update($object);

                        if ($this->isXmlHttpRequest()) {
                            return $this->renderJson(array(
                                'result' => 'ok',
                                'objectId' => $this->admin->getNormalizedIdentifier($object),
                                'objectName' => $this->escapeHtml($this->admin->toString($object)),
                            ), 200, array());
                        }

                        if ($object->getIsFirstTimeSent()) {
                            $this->addFlash(
                                'sonata_flash_success',
                                $this->admin->trans(
                                    'flash_edit_and_send_report_success',
                                    array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                    'SonataAdminBundle'
                                )
                            );
                        } else {
                            $this->addFlash(
                                'sonata_flash_success',
                                $this->trans(
                                    'flash_edit_success',
                                    array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                    'SonataAdminBundle'
                                )
                            );
                        }

                        // redirect to edit mode
                        return $this->redirectTo($object);
                    } catch (ModelManagerException $e) {
                        $this->handleModelManagerException($e);
                        $isFormValid = false;
                    } catch (LockException $e) {
                        $this->addFlash('sonata_flash_error', $this->trans('flash_lock_error', array(
                            '%name%' => $this->escapeHtml($this->admin->toString($object)),
                            '%link_start%' => '<a href="' . $this->admin->generateObjectUrl('edit', $object) . '">',
                            '%link_end%' => '</a>',
                        ), 'SonataAdminBundle'));
                    }
                }

                //Это isReportValid по дефолту false, однако можно переопределить в ресолвере
                // show an error message if the form failed validation
                if (!$isFormValid || !$service->isReportValid()) {
                    $service->afterSubmitInvalidEdit($object, $form);
                    $this->checkAutosaveVersion($object);

                    $object->setIsValid(false);
                    $object = $this->admin->update($object);
                    if ($service->isDoubleSubmit()) {
                        // для повторной загрузки объекта на форму и сабмита для корретного вывода ошибок
                        static $i = 0;
                        if (!$i++) {
                            $this->admin->setIsHistoryCreated(true);
                            return $this->editAction();
                        }
                    }


                    if (!$this->isXmlHttpRequest()) {
                        $this->addFlash(
                            'sonata_flash_error',
                            $this->trans(
                                'flash_edit_error',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );
                    }
                } elseif ($this->isPreviewRequested()) {
                    // enable the preview template if the form was valid and preview was requested
                    $templateKey = 'preview';
                    $this->admin->getShow();
                }
            }
        } elseif (!strstr($request->headers->get('referer'), 'create')) {
            $service->submitAlternative($object, $form, ['request' => $request]);
        }
        $formView = $form->createView();
        $service->afterFormViewCreateEdit($object, $formView);

        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFormTheme());

        $parameters = [
            'action' => 'edit',
            'form' => $formView,
            'object' => $object,
        ];

        if (is_array($response) && key_exists('parameters', $response)) {
                $parameters = array_merge($parameters, $response['parameters']);
        }

        return $this->render($this->admin->getTemplate($templateKey), $parameters, null);
    }

    public function completeEditionAction($id = null)
    {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);
        $service = $this->container->get(get_class($object).self::ENDING_ENTITY_RESOLVER);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }
        $service->preCompleteEdit($object);

        $url = $this->generateUrl(self::BASE_LIST_ROUTE);

        if ($object->getUserBlockedFinanceReport() != $this->getUser()->getId()) {
            $this->addFlash(
                'sonata_flash_success',
                'Отчёт заблокирован другим пользователем'
            );
            return new RedirectResponse($url);
        }

        $this->blockReports($object, 0, false);
        $this->admin->update($object);

        $this->addFlash(
            'sonata_flash_success',
            $this->getCompleteEditionFlash($object)
        );
        return new RedirectResponse($url);
    }

    /**
     * This function blocks/unblocks all finance reports of this author.
     *
     * @param BaseReport $object
     * @param int $userBlockedFinanceReport
     * @param bool $isBlocked
     */
    protected function blockReports(BaseReport $object, $userBlockedFinanceReport, $isBlocked)
    {
        if ($object instanceof FinanceReportInterface) {
            $object->setUserBlockedFinanceReport($userBlockedFinanceReport);
        } else {
            return null;
        }

        foreach ($object->getAuthor()->getReports() as $report) {
            if ($report instanceof FinanceReportInterface) {
                if ($report->getId() != $object->getId()) {
                    $report->setIsBlocked($isBlocked);
                }
            }
        }
    }

    protected function setFlagsBeforeEdit(BaseReport $object)
    {
        if ($this->isSendPreviousVersion) {
            return;
        }

        $this->isSendPreviousVersion = $object->getIsSend();
    }

    public function checkAutosaveVersion(BaseReport $object)
    {
        if ($this->getRequest()->get('autosaver') != "true") {
            $object->setIsAutosaved(false);
            return;
        }
        $object->setIsSend($this->isSendPreviousVersion);
        $object->setIsAutosaved(true);
    }

    private function getCompleteEditionFlash(BaseReport $report)
    {
        switch (true) {
            case $report instanceof FinanceReportInterface:
                return 'Финансовые отчёты снова доступны для редактирования';
            default:
                return 'Отчёты снова доступны для редактирования';
        }
    }
}
