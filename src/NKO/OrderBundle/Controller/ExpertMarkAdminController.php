<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/10/17
 * Time: 9:42 AM
 */

namespace NKO\OrderBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Entity\Farvater\FarvaterApplication;
use NKO\OrderBundle\Entity\FinalDecision;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Entity\Mark;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;
use NKO\OrderBundle\Utils\Application\SummaryTableConfiguration;
use NKO\UserBundle\Entity\ExpertUser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PHPExcel_Settings;
use PHPExcel_IOFactory;
use PHPExcel_Writer_Excel5;
use PHPExcel_Style_Fill;
use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application as BriefApplication2018;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017Application;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017Application;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as BriefApplication2016;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNS2018Application;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater2018Application;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNS2019Application;
use NKO\OrderBundle\Entity\MarkListHistory;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNS2018SecondStageApplication;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as HarborApplication2019;
use NKO\OrderBundle\Entity\Application\Harbor\Application2020\Application as HarborApplication2020;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Application as KNS2020Application;
use NKO\OrderBundle\Entity\Application\Verification\Application2020\Application as Verification2020Application;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application as KNSSecondStage2019;

class ExpertMarkAdminController extends AdminController
{
    const EXPERT_NUMBER = 3;
    const BORDER_VALUE = 1000000;
    const LIST_ROUTE = 'admin_sonata_nko_order_expert_mark_list';
    const DOWNLOAD_ALL_MARKS_ATTRIBUTE = 'ROLE_SONATA_ADMIN_NKO_ORDER_EXPERT_MARK_DOWNLOAD_ALL_MARKS';

    public function spreadAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        $competition =  $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(Competition::class)
            ->find($competitionId);

        $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Распределение заявок', $competition->getName());

        $experts = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(ExpertUser::class)
            ->createQueryBuilder('e')
            ->innerJoin('e.competitions', 'c')
            ->where(':competitionId MEMBER OF e.competitions')
            ->setParameters(['competitionId' => $competitionId])
            ->getQuery()
            ->getResult();


        if (count($experts) < self::EXPERT_NUMBER) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('not_enough_experts'));

            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        $applications = $this->setApplicationNumber($competitionId);

        if (!$applications) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('no_applications_available'));

            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        shuffle($applications);
        shuffle($experts);

        $rd = 0;
        $i = 0;
        $em = $this->getDoctrine()->getManager();

        foreach ($applications as $application) {

            $em->persist($application);

            loop:

            for (; $i < count($experts); $i++) {
                if (!(++$rd % 4)) {
                    continue(2);
                }
                $experts[$i]->addApplication($application);
                $application->addExpert($experts[$i]);

                $em->persist($experts[$i]);
            }
            $i = 0;
            goto loop;
        }
        $em->flush();


        $this->addFlash('sonata_flash_success', $this->get('translator')->trans('applications_are_spreaded_successfully'));

        return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
    }



    public function spreadForCustomExpertNumberAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        $competition =  $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(Competition::class)
            ->find($competitionId);

        $neededExpertNumbersForCompetition = $competition->getExpertNumber();

        $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction("Распределение заявок с {$neededExpertNumbersForCompetition} експертами", $competition->getName());

        $experts = $this->getDoctrine()->getManager()->getRepository(ExpertUser::class)->findExpertsByCompetition($competitionId);


        if (count($experts) < $neededExpertNumbersForCompetition) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('not_enough_experts__need_number', ['%number%' =>  $neededExpertNumbersForCompetition]));

            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        $applications = $this->setApplicationNumber($competitionId);

        if (!$applications) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('no_applications_available'));

            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        shuffle($applications);
        shuffle($experts);

        $em = $this->getDoctrine()->getManager();

        $currentExpertNumber = 0;
        $expertCount = count($experts);
        foreach ($applications as $key => $application) {
            $expertSpreadedForApplication = 0;
            $em->persist($application);

            while ($expertSpreadedForApplication != $neededExpertNumbersForCompetition) {
                $experts[$currentExpertNumber]->addApplication($application);
                $application->addExpert($experts[$currentExpertNumber]);

                $em->persist($experts[$currentExpertNumber]);

                $expertSpreadedForApplication++;
                $currentExpertNumber = ($currentExpertNumber + 1) % $expertCount;
            }
        }

        $em->flush();

        $this->addFlash('sonata_flash_success', $this->get('translator')->trans('applications_are_spreaded_successfully'));

        return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
    }

    public function spreadWithoutExpertAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $this->setApplicationNumber($competitionId);
        $this->addFlash('sonata_flash_success', $this->get('translator')->trans('applications_are_spreaded_successfully'));
        return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
    }

    public function markListAction(Request $request)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_SONATA_ADMIN_NKO_ORDER_EXPERT_MARK_EDIT_MARK_LIST');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', 'error set grant');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getSubject();
        if ($this->isCurrentUserCreatedMark($object)) {
            $mark_id = $this->getIdCurrentUserMark($object);

            return new RedirectResponse($this->generateUrl('admin_sonata_nko_order_send_application_mark_list_edit', ['id' => $mark_id]));
        }

        return new RedirectResponse($this->generateUrl('admin_sonata_nko_order_send_application_mark_list_create', ['id' => $id]));
    }

    private function isCurrentUserCreatedMark($application)
    {
        $current_user = $this->get('security.token_storage')->getToken()->getUser();

        if (get_class($current_user) == 'NKO\UserBundle\Entity\ExpertUser') {
            $user_marks = $current_user->getMarkLists();
            foreach ($user_marks as $mark) {
                if ($mark->getApplication() == $application) {
                    return true;
                }
            }
        }
        return false;
    }

    private function getIdCurrentUserMark($application)
    {
        $current_user = $this->get('security.token_storage')->getToken()->getUser();
        $user_marks = $current_user->getMarkLists();

        foreach ($user_marks as $mark) {
            if ($mark->getApplication() == $application) {
                return $mark->getId();
            }
        }
    }

    public function editExpertsAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());

        return new RedirectResponse($this->generateUrl('admin_nko_order_applicationhistory_edit', ['id' => $id]));
    }

    public function downloadSummaryTableAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $competition =  $this->getDoctrine()->getManager()->getRepository(Competition::class)->find($competitionId);
        $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Выгрузка таблицы “Cводная таблица оценок”', $competition->getName());

        $applications = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(ApplicationHistory::class)
            ->findSpreadByCompetition($competitionId);

        if (!$applications) {
            $this->addFlash('sonata_flash_error', 'unspreaded_applications');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $applicationClass = $competition->getApplicationClass();
        $expertsCount = $competition->getExpertNumber();

        $finalMark = $this->getFinalMarkField($applicationClass);

        //начиная с NKO-2310 сводная таблица формируется по отправленной истории оценки
        $mode = in_array($competition->getId(), self::MARKS_BY_COMPETITION_WITHOUT_HISTORY);
        $mode ? $this->setAverageMarks($applications, $finalMark) : $this->setAverageMarksByHistory($applications, $finalMark);

        usort($applications, function ($a, $b) {
            if ($a->getAverageMark() == $b->getAverageMark()) {
                return 0;
            }

            return ($a->getAverageMark() < $b->getAverageMark()) ? 1 : -1;
        });

        $filename = null;

        $tempOutFileName = $this->getFileForCompetition($filename, $applicationClass);

        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        $sheet = $excelObj->getActiveSheet();

        $this->fillingSheet($applications, $sheet, $applicationClass, $expertsCount);

        $objWriter = new PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);

        return $this->get('nko_order.downloader.excel_downloader')->getStreamedResponse($tempOutFileName, $this->get('translator')->trans('summary_table'));
    }

    private function getFileForCompetition(&$filename, $applicationClass)
    {
        switch ($applicationClass) {
            case Application::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_application.xls';
                break;
            case BriefApplication::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_brief_application.xls';
                break;
            case Farvater2017Application::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_farvater_2017_application.xls';
                break;
            case KNS2017Application::class:
            case SecondStageApplication::class:
            case KNS2018Application::class:
            case KNS2019Application::class:
            case KNS2020Application::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_kns_2017_application.xls';
                break;
            case KNS2018SecondStageApplication::class:
            case KNSSecondStage2019::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_second_stage_2018_application.xls';
                break;
            case HarborApplication2020::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_harbor_2020_application.xls';
                break;
            case HarborApplication2019::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_harbor_2019_application.xls';
                break;
            case Verification2020Application::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/empty_form_summary.xls';
                break;
            case BriefApplication2018::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_brief_application_2018.xls';
                break;
            case Farvater2018Application::class:
                $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/table_brief_application_2018.xls';
                break;
            default:
                $this->addFlash('sonata_flash_error', 'no_summary_table_template');
                return new RedirectResponse($this->admin->generateUrl('list'));
        }

        return $this->get('nko_order.downloader.excel_downloader')->getTempExcelFilename($filename);
    }

    private function fillingSheet($applications, $sheet, $applicationClass, $expertsCount)
    {
        $pCellCoordinate = 'A1:T';
        $range = range('A', 'T');
        switch ($applicationClass) {
            case Application::class:
                $this->getDataFormApplication($applications, $sheet);
                break;
            case BriefApplication::class:
                $this->getDataFromBriefApplication($applications, $sheet);
                break;
            case Farvater2017Application::class:
                $this->getDataFromFarvaterApplication($applications, $sheet);
                break;
            case KNS2017Application::class:
            case SecondStageApplication::class:

            case KNS2019Application::class:
            case KNS2020Application::class:
                $this->getDataFromKNS2017Application($applications, $sheet);
                $pCellCoordinate = 'A1:U';
                $range = range('A', 'U');
                break;
            case KNS2018SecondStageApplication::class:
            case KNSSecondStage2019::class:
                $this->getDataFromKNS2018SecondStageApplication($applications, $sheet);
                $pCellCoordinate = 'A1:U';
                $range = range('A', 'U');
                break;
            case HarborApplication2019::class:
                $this->getDataFromHarbor2019Application($applications, $sheet);
                $pCellCoordinate = 'A1:V';
                $range = range('A', 'V');
                break;





            case HarborApplication2020::class:
                $this->getDataFromHarbor2020Application($applications, $sheet);
                $pCellCoordinate = 'A1:T';
                $range = range('A', 'T');
                break;
            case BriefApplication2018::class:
                $this->getDataFromBriefApplication($applications, $sheet);
                $pCellCoordinate = 'A1:V';
                $range = range('A', 'V');
                break;
            case Farvater2018Application::class:
                $this->getDataFromFarvater2018Application($applications, $sheet);
                $pCellCoordinate = 'A1:V';
                $range = range('A', 'V');
                break;
            default:
                $this->newFillingSheet($applications, $sheet, $applicationClass, $expertsCount);
                return;
        }

        $sheet->getStyle($pCellCoordinate . (count($applications) + 2))->applyFromArray($this->setTableBorder());
        $this->formatColumn($range, $sheet, $applications);
    }

    private function newFillingSheet($applications, $sheet, $applicationClass, $expertsCount)
    {
        if (!key_exists($applicationClass, SummaryTableConfiguration::OPTIONS)
            || !($options = SummaryTableConfiguration::OPTIONS[$applicationClass])
        ) {
            throw new \Exception('Summary table dosn\'t exist for this competition.');
        }
        $executor = new SummaryTableConfiguration($this->getDoctrine()->getManager(), $applications, $sheet, $expertsCount);

        $executor->execute($options);
    }


    private function formatColumn($range, $sheet, $applications)
    {
        foreach ($range as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
            for ($i = 0; $i < count($applications) + 1; $i++) {
                $sheet->getStyle($columnID . ($i + 1))->getAlignment()->setWrapText(true);
                $sheet->getRowDimension($i + 1)->setRowHeight(-1);
            }
        }
    }

    private function getFinalMarkField($className)
    {
        switch ($className) {
            case Application::class:
                return 'finalMark';
                break;
            case BriefApplication::class:
                return 'markWithCoeff';
                break;
            default:
                return 'finalMark';
        }
    }

    public function setAverageMarks($apps, $finalMark)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($apps as $app) {
            $summary_mark = 0;

            /**
             * @var MarkList $mark
             */
            foreach ($app->getMarkLists() as $mark) {
                $summary_mark += $accessor->getValue($mark, $finalMark);
            }

            $countMarkLists = count($app->getMarkLists());

            if ($countMarkLists != 0) {
                $app->setAverageMark($summary_mark / $countMarkLists);
            } else {
                $app->setAverageMark($summary_mark);
            }

            $this->getDoctrine()->getManager()->persist($app);
            $this->getDoctrine()->getManager()->flush();
        }
    }

    public function setAverageMarksByHistory($applicationHistories, $finalMark)
    {
        $em = $this->getDoctrine()->getManager();
        $competition = unserialize($applicationHistories[0]->getData())->getCompetition();

        $markListHistories = $em->getRepository(MarkListHistory::class)->findBy([
            'isSend' => true,
            'competition' => $competition->getId(),
        ]);

        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($applicationHistories as $applicationHistory) {
            $summaryMark = 0;

            $markListsHistoryByApplication = array_filter($markListHistories, function ($item) use ($applicationHistory) {
                if (!$item->getApplicationHistory()) {
                    return;
                }
                return $item->getApplicationHistory()->getId() === $applicationHistory->getId();
            });


            foreach ($markListsHistoryByApplication as $markListHistory) {
                $markList = unserialize($markListHistory->getData());
                $summaryMark += $accessor->getValue($markList, $finalMark);
            }

            $countMarkLists = count($markListsHistoryByApplication);

            if ($countMarkLists != 0) {
                $applicationHistory->setAverageMark($summaryMark / $countMarkLists);
            } else {
                $applicationHistory->setAverageMark($summaryMark);
            }

            $em->flush();
        }
    }

    public function sendMarkAction(Request $request)
    {
        /** @var ApplicationHistory $applicationHistory */
        $applicationHistory = $this->admin->getSubject();
        if (!$applicationHistory) {
            throw $this->createNotFoundException('unable to find the object');
        }
        $em = $this->getDoctrine()->getManager();
        $mark = $em
            ->getRepository('NKOOrderBundle:MarkList')
            ->find($this->getIdCurrentUserMark($applicationHistory));
        /**
         * @var ExpertUser $current_user
         */
        $current_user = $this->get('security.token_storage')->getToken()->getUser();

        if ($current_user instanceof ExpertUser
            && $current_user->getCompetitions()->contains($applicationHistory->getCompetition())
            && $applicationHistory->getCompetition()->getFinishEstimate() < new \DateTime()) {
            $this->addFlash(
                'sonata_flash_error',
                $this->admin->trans(
                    'flash_finish_estimate_error',
                    array('%name%' => $this->escapeHtml($this->admin->toString($applicationHistory))),
                    'SonataAdminBundle'
                )
            );
            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        if ($mark->getIsValid()) {
            $mark->setIsSend(true);
            /*             $mark->setIsAutosaved(null);


                        $em->detach($mark);
                        $mark = $this->get('nko.resolver.proxy_resolver')->resolveProxies($mark);
                        $mark = $this->get('nko.resolver.collection_resolver')->fetchCollections($mark);

                        foreach ($mark->getMarks as &$a) {
                            $this->get('nko.resolver.proxy_resolver')->resolveProxies($a);
                            $this->get('nko.resolver.collection_resolver')->fetchCollections($a);
                        }



                        $em->clear();


                        $prevHistory = $em->getRepository(MarkListHistory::class)->findOneBy([
                            'competition' => $mark->getApplication()->getCompetition()->getId(),
                            'author' => $mark->getApplication()->getAuthor()
                        ], ['id' => 'DESC']);

                        if (!$prevHistory->getIsSend()) {
                            if (unserialize($prevHistory->getData())->getIsValid()) {
                                $prevHistory->setIsSend(true);
                            }
                        }*/

            $em->flush();
            $this->addFlash('sonata_flash_success', $this->get('translator')->trans('mark_send'));
        } else {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('no_mark_to_send'));
        }

        return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
    }

    private function getDataFormApplication($applications, $sheet)
    {
        $totalSum = 0;
        $countApps = count($applications);
        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, unserialize($applications[$i]->getData())->getProjectName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNkoName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getLegalRegion());
            $id = unserialize($applications[$i]->getData())->getId();
            $app = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository(Application::class)
                ->findOneById($id);

            if ($app && $app->getOrganization()) {
                $sheet->setCellValueByColumnAndRow($j++, $i + 2, $app->getOrganization()->getOrganizationName());
            } else {
                $j++;
            }

            $k = 17;
            foreach ($applications[$i]->getExperts() as $expert) {
                $sheet->setCellValueByColumnAndRow($k++, $i + 2, $expert->getFullName());
                foreach ($expert->getMarkLists() as $mark) {
                    if ($mark->getApplication()->getId() == $applications[$i]->getId()) {
                        $finalDecision =
                            $this
                                ->getDoctrine()
                                ->getManager()
                                ->getRepository('NKOOrderBundle:FinalDecision')
                                ->findOneById($mark->getFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                        $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $mark->getRationaleFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $mark->getFinalMark());
                    }
                }
                $j += 3;
            }
            $j = 15;
            $sheet->setCellValueByColumnAndRow($j, $i + 2, number_format($applications[$i]->getAverageMark(), 2, ',', ' '));
            $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, unserialize($applications[$i]->getData())->getRequiredMoney());

            $totalSum += unserialize($applications[$i]->getData())->getRequiredMoney();
            if ($totalSum <= self::BORDER_VALUE) {
                $this->setRowColor($sheet, $i + 2, range('A', 'T'));
            }
        }
    }

    private function getDataFromBriefApplication($applications, $sheet)
    {
        $countApps = count($applications);
        usort($applications, function ($a, $b) {
            return ($a->getAverageMark() > $b->getAverageMark()) ? -1 : 1;
        });

        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;
            $application = unserialize($applications[$i]->getData());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNumber());

            $secondStageNumber = null;
            if ($application instanceof Farvater2018Application) {
                $secondStageNumber = $applications[$i]->getNumber();
            }
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $secondStageNumber);

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNkoName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getLegalRegion());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getPriorityDirection());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getPriorityDirectionEtc());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getProjectName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, null);

            $k = 15;

            foreach ($applications[$i]->getExperts() as $expert) {
                foreach ($expert->getMarkLists() as $mark) {
                    if ($mark->getApplication()->getId() == $applications[$i]->getId()) {
                        $finalDecision =
                            $this
                                ->getDoctrine()
                                ->getManager()
                                ->getRepository(FinalDecision::class)
                                ->findOneById($mark->getFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                        $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $mark->getRationaleFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $mark->getMarkWithCoeff());
                    }
                }
                $j += 3;
            }
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, number_format($applications[$i]->getAverageMark(), 2, ',', ' '));

            foreach ($applications[$i]->getExperts() as $expert) {
                $sheet->setCellValueByColumnAndRow($j++, $i + 2, $expert->getFullName());
            }
        }
    }

    private function getDataFromFarvater2018Application($applications, $sheet)
    {
        $countApps = count($applications);
        usort($applications, function ($a, $b) {
            return ($a->getAverageMark() > $b->getAverageMark()) ? -1 : 1;
        });

        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;
            $application = unserialize($applications[$i]->getData());
            $relatedApplicationHistory = $this->get('nko_order.resolver.related_competition_resolver')->getApplicationHistory($application);
            $relatedApplication = unserialize($relatedApplicationHistory->getData());

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $relatedApplicationHistory->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNumber());

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNkoName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $relatedApplication->getLegalRegion());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $relatedApplication->getPriorityDirection());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $relatedApplication->getPriorityDirectionEtc());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $relatedApplication->getProjectName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getFirstYearEstimatedFinancing());

            $k = 15;

            foreach ($applications[$i]->getExperts() as $expert) {
                foreach ($expert->getMarkLists() as $mark) {
                    if ($mark->getApplication()->getId() == $applications[$i]->getId()) {
                        $finalDecision =
                            $this
                                ->getDoctrine()
                                ->getManager()
                                ->getRepository(FinalDecision::class)
                                ->findOneById($mark->getFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                        $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $mark->getRationaleFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $mark->getMarkWithCoeff());
                    }
                }
                $j += 3;
            }
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, number_format($applications[$i]->getAverageMark(), 2, ',', ' '));

            foreach ($applications[$i]->getExperts() as $expert) {
                $sheet->setCellValueByColumnAndRow($j++, $i + 2, $expert->getFullName());
            }
        }
    }

    public function setTableBorder()
    {
        return array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'))
            ));
    }

    public function contactsAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $applications = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(ApplicationHistory::class)
            ->findByIsAppropriateCreatedAt($competitionId);

        if (!$applications) {
            $this->addFlash('sonata_flash_error', 'unspreaded_applications');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $className = get_class(unserialize(reset($applications)->getData()));
        $filename = $this->get('nko.order.generator.excel_contacts_generator')->getContactsExcel($applications, $className);

        if (!$filename) {
            $this->addFlash('sonata_flash_error', 'no_contacts_template');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        return $this->get('nko_order.downloader.excel_downloader')->getStreamedResponse($filename, $this->container->get('translator')->trans('contacts_table'));
    }

    public function setRowColor($sheet, $row, $columns)
    {
        foreach ($columns as $column) {
            $sheet->getStyle($column . $row)->applyFromArray(array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'B8FA86')),
            ));
        }
    }

    public function downloadAllMarksAction(Request $request)
    {
        $object = $this->admin->getSubject();
        $em = $this->getDoctrine()->getManager();

        try {
            $this->denyAccessUnlessGranted(self::DOWNLOAD_ALL_MARKS_ATTRIBUTE, $object);
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', 'access_denied');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        if (!$object) {
            throw $this->createNotFoundException('unable to find the object');
        }

        $mode = in_array($object->getCompetition()->getId(), self::MARKS_BY_COMPETITION_WITHOUT_HISTORY);

        if (!$mode) {
            $application = unserialize($object->getData());
            $experts = $object->getExperts();
            $markListHistories = $em->getRepository(MarkListHistory::class)->getSentMarkListHistories($application, $experts);
        }

        $markLists = $object->getMarkLists();
        $markLists->initialize();
        $markLists = $markLists->toArray();


        if ($mode) {
            $marks = array_filter($markLists, function ($mark) {
                return $mark->getIsValid() && $mark->getIsSend();
            });
        } else {
            $correctMarkListhistories = array_filter($markListHistories, function ($item) use ($markLists) {
                return array_filter($markLists, function ($mark) use ($item) {
                    return $item->getExpert()->getId() === $mark->getExpert()->getId();
                });
            });

            $marks = array_map(function ($item) use ($em) {
                $mark = unserialize($item->getData());
                $expert = $mark->getExpert();

                $realExpert = $em->find(get_class($expert), $expert->getId());
                $mark->setExpert($realExpert);

                return $mark;
            }, $correctMarkListhistories);
        }

        return $this->render('NKOOrderBundle:CRUD:base_list_mark_lists.html.twig', array('marks' => $marks));
    }

    public function setNumberAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');

            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $applications = $this->setApplicationNumber($competitionId);

        if (!$applications) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('no_applications_available'));

            return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
        }

        $this->addFlash('sonata_flash_success', $this->get('translator')->trans('applications_get_number_successfully'));

        return $this->redirectToRoute('admin_sonata_nko_order_expert_mark_list');
    }

    private function setApplicationNumber($competitionId)
    {
        $em = $this->getDoctrine()->getManager();
        $applications = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findReadyToSpreadByCompetition($competitionId);

        if (!$applications) {
            return null;
        }

        $i = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findMaxNumber($competitionId);

        foreach ($applications as $application) {
            $application->setNumber(++$i);
            $application->setIsSpread(true);
        }
        $em->flush();

        return $applications;
    }

    private function getDataFromFarvaterApplication($applications, $sheet)
    {
        $countApps = count($applications);

        for ($i = 0; $i < $countApps; $i++) {
            $briefApplications = $this->container->get('nko.resolver.related_applications_resolver')
                ->getLinkedApplicationHistories($applications[$i], 'NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication', true);
            $briefApplication = reset($briefApplications);

            $j = 0;
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $briefApplication['number']);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applications[$i]->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, unserialize($briefApplication['data'])->getProjectName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, unserialize($briefApplication['data'])->getName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $briefApplication['legalRegion']);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, unserialize($briefApplication['data'])->getPriorityDirection());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, unserialize($briefApplication['data'])->getPriorityDirectionEtc());

            $k = 15;
            foreach ($applications[$i]->getExperts() as $expert) {
                /**
                 * @var Mark $mark
                 */
                foreach ($expert->getMarkLists() as $mark) {
                    if ($mark->getApplication()->getId() == $applications[$i]->getId()) {
                        $finalDecision =
                            $this
                                ->getDoctrine()
                                ->getManager()
                                ->getRepository(FinalDecision::class)
                                ->findOneById($mark->getFinalDecision());

                        $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                        $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $mark->getRationaleFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $mark->getMarkWithCoeff());
                    }
                }
                $sheet->setCellValueByColumnAndRow($k++, $i + 2, $expert->getFullName());
                $j += 3;
            }

            $sheet->setCellValueByColumnAndRow(14, $i + 2, number_format($applications[$i]->getAverageMark(), 2, ',', ' '));
        }
    }

    public function getDataFromKNS2017Application($applicationHistories, $sheet)
    {
        $countApps = count($applicationHistories);
        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;
            /**
             * @var \NKO\OrderBundle\Entity\KNS2017\Application $app
             */
            $app = unserialize($applicationHistories[$i]->getData());

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $applicationHistories[$i]->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $app->getName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $app->getLegalRegion());

            $trainingGrounds = null;
            $competitionDirection = null;
            if ($app->getTrainingGrounds()) {
                /** @var TrainingGround $trainingGround */
                foreach ($app->getTrainingGrounds() as $trainingGround) {
                    if ($trainingGround->getParent()) {
                        $trainingGrounds .= $trainingGround->getName() . " ";
                    } else {
                        $competitionDirection .= $trainingGround->getName() . " ";
                    }
                }
            }

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $competitionDirection);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $trainingGrounds);

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $app->getProjectName());

            $k = 18;
            foreach ($applicationHistories[$i]->getExperts() as $expert) {
                $sheet->setCellValueByColumnAndRow($k++, $i + 2, $expert->getFullName());
                foreach ($expert->getMarkLists() as $mark) {
                    if ($mark->getApplication()->getId() == $applicationHistories[$i]->getId()) {
                        $finalDecision =
                            $this
                                ->getDoctrine()
                                ->getManager()
                                ->getRepository('NKOOrderBundle:FinalDecision')
                                ->findOneById($mark->getFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                        $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $mark->getRationaleFinalDecision());
                        $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $mark->getFinalMark());
                    }
                }
                $j += 3;
            }
            $j = 16;
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, number_format($applicationHistories[$i]->getAverageMark(), 2, ',', ' '));
            $sheet->setCellValueByColumnAndRow($j, $i + 2, $app->getRequestedFinancingMoney());
        }
    }


    public function getDataFromKNS2018SecondStageApplication($applicationHistories, $sheet)
    {
        $em = $applications = $this->getDoctrine()->getManager();
        $countApps = count($applicationHistories);
        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;

            $history = $applicationHistories[$i];
            $application = unserialize($history->getData());

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $history->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getLegalRegion());

            $trainingGrounds = null;
            $competitionDirection = null;
            if ($application->getTrainingGrounds()) {
                foreach ($application->getTrainingGrounds() as $trainingGround) {
                    if ($trainingGround->getParent()) {
                        $trainingGrounds .= $trainingGround->getName() . " ";
                    } else {
                        $competitionDirection .= $trainingGround->getName() . " ";
                    }
                }
            }

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $competitionDirection);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $trainingGrounds);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getProjectName());

            $k = 18;
            $experts = $history->getExperts();
            $markListHistories = $em->getRepository(MarkListHistory::class)->getSentMarkListHistories($application, $experts);

            foreach ($experts as $expert) {
                $sheet->setCellValueByColumnAndRow($k++, $i + 2, $expert->getFullName());

                $sendMarkList = array_filter($markListHistories, function ($item) use ($expert) {
                    return $item->getExpert()->getId() === $expert->getId();
                });

                if ($sendMarkList) {
                    $sendMarkList = is_array($sendMarkList) ? reset($sendMarkList) : $sendMarkList;
                    $unserialized = unserialize($sendMarkList->getData());
                    $finalDecision = $unserialized->getFinalDecision();

                    $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                    $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $unserialized->getRationaleFinalDecision());
                    $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $unserialized->getFinalMark());
                }

                $j += 3;
            }

            $j = 16;
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, number_format($history->getAverageMark(), 2, ',', ' '));
            $sheet->setCellValueByColumnAndRow($j, $i + 2, $application->getFirstYearEstimatedFinancing());
        }
    }

    public function getDataFromHarbor2019Application($applicationHistories, $sheet)
    {
        $em = $applications = $this->getDoctrine()->getManager();
        $countApps = count($applicationHistories);
        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;

            $history = $applicationHistories[$i];
            $application = unserialize($history->getData());
            $priorityDirection = null;

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $history->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getLegalRegion());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getProjectName());

            $k = 19;
            $experts = $history->getExperts();

            $markListHistories = $em->createQueryBuilder()
                ->select('m')
                ->from(MarkListHistory::class, 'm')
                ->where('m.isSend =:isSend')
                ->andWhere('m.competition =:competition')
                ->andWhere('m.author =:author')
                ->andWhere('m.expert IN (:experts)')
                ->setParameters([
                    'isSend'=> true,
                    'competition'=> $application->getCompetition()->getId(),
                    'author' => $application->getAuthor()->getId(),
                    'experts'=> $experts,
                ])
                ->getQuery()
                ->getResult();

            foreach ($experts as $expert) {
                $sheet->setCellValueByColumnAndRow($k++, $i + 2, $expert->getFullName());

                $sendMarkList = array_filter($markListHistories, function ($item) use ($expert) {
                    return $item->getExpert()->getId() === $expert->getId();
                });

                if ($sendMarkList) {
                    $sendMarkList = is_array($sendMarkList) ? reset($sendMarkList) : $sendMarkList;

                    $unserialized = unserialize($sendMarkList->getData());
                    $finalDecision = $unserialized->getFinalDecision();

                    $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                    $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $unserialized->getRationaleFinalDecision());
                    $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $unserialized->getFinalMark());
                    $sheet->setCellValueByColumnAndRow($j + 3, $i + 2, $unserialized->getHelp());
                }

                $j += 4;
            }

            $j = 17;
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, number_format($history->getAverageMark(), 2, ',', ' '));
            $sheet->setCellValueByColumnAndRow($j, $i + 2, $application->getFirstYearEstimatedFinancing());
        }
    }


    public function getDataFromHarbor2020Application($applicationHistories, $sheet)
    {
        $em = $applications = $this->getDoctrine()->getManager();
        $countApps = count($applicationHistories);
        for ($i = 0; $i < $countApps; $i++) {
            $j = 0;

            /** @var  $application HarborApplication2020 */
            $history = $applicationHistories[$i];
            $application = unserialize($history->getData());
            $priorityDirection = null;

            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $i + 1);
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $history->getNumber());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getLegalRegion());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getProjectName());
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, $application->getTrainingGround());

            $k = 17;
            $experts = $history->getExperts();

            $markListHistories = $em->createQueryBuilder()
                ->select('m')
                ->from(MarkListHistory::class, 'm')
                ->where('m.isSend =:isSend')
                ->andWhere('m.competition =:competition')
                ->andWhere('m.author =:author')
                ->andWhere('m.expert IN (:experts)')
                ->setParameters([
                    'isSend'=> true,
                    'competition'=> $application->getCompetition()->getId(),
                    'author' => $application->getAuthor()->getId(),
                    'experts'=> $experts,
                ])
                ->getQuery()
                ->getResult();

            foreach ($experts as $expert) {
                $sheet->setCellValueByColumnAndRow($k++, $i + 2, $expert->getFullName());

                $sendMarkList = array_filter($markListHistories, function ($item) use ($expert) {
                    return $item->getExpert()->getId() === $expert->getId();
                });

                if ($sendMarkList) {
                    $sendMarkList = is_array($sendMarkList) ? reset($sendMarkList) : $sendMarkList;

                    $unserialized = unserialize($sendMarkList->getData());
                    $finalDecision = $unserialized->getFinalDecision();

                    $sheet->setCellValueByColumnAndRow($j, $i + 2, $finalDecision);
                    $sheet->setCellValueByColumnAndRow($j + 1, $i + 2, $unserialized->getRationaleFinalDecision());
                    $sheet->setCellValueByColumnAndRow($j + 2, $i + 2, $unserialized->getFinalMark());
                }

                $j += 3;
            }

            $j = 15;
            $sheet->setCellValueByColumnAndRow($j++, $i + 2, number_format($history->getAverageMark(), 2, ',', ' '));
            $sheet->setCellValueByColumnAndRow($j, $i + 2, $application->getFirstYearEstimatedFinancing());
        }
    }

    public function winnerInfoAction(Request $request)
    {
        $competitionId = $request->get('competition');

        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', 'select_competition');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        $em = $applications = $this->getDoctrine()->getManager();
        $competition = $em->getRepository('NKOOrderBundle:Competition')->find($competitionId);
        $applications = $em
            ->getRepository('NKOOrderBundle:ApplicationHistory')
            ->createQueryBuilder('a')
            ->select('a')
            ->where('a.competition = :competition')
            ->andWhere('a.contract IS NOT NULL')
            ->setParameter('competition', $competition)
            ->getQuery()
            ->getResult();


        if (!$applications) {
            $this->addFlash('sonata_flash_error', "winners don't exist");
            return new RedirectResponse($this->admin->generateUrl('list'));
        } else {
            $className = get_class(unserialize(reset($applications)->getData()));
            $filename = $this->get('nko.order.generator.excel_winner_info_generator')
                ->getWinnersTable($applications, $className);
            return $this->get('nko_order.downloader.excel_downloader')->getStreamedResponse($filename, $this->container->get('translator')->trans('winners_table'));
        }
    }

    public function setGrantsAction(Request $request)
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_SONATA_ADMIN_NKO_ORDER_EXPERT_MARK_EDIT_GRANTS');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', 'error set grant');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $em = $this->getDoctrine()->getManager();
        $appHistory = $this->admin->getSubject();
        $competitionId = unserialize($appHistory->getData())->getCompetition()->getId();
        $competition = $em->getRepository(Competition::class)->find($competitionId);

        $reportForms = $competition->getReportForms();

        if ($reportForms->isEmpty()) {
            $this->addFlash('sonata_flash_error', "no_available_report_masters");
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        /**
         * @var ArrayCollection $grants
         */
        $grants = $appHistory->getGrants();

        if ($grants->isEmpty()) {
            foreach ($reportForms as $reportForm) {
                $grant = $this->createGrantConfig($reportForm, $appHistory);
                $em->persist($grant);
            }
            $em->flush();
            return new RedirectResponse($this->generateUrl('admin_nko_order_grantconfig_list', ["filter[applicationHistory][value]" => $appHistory->getId(), 'competition_id' => $competitionId]));
        }

        foreach ($reportForms as $reportForm) {
            $id = $reportForm->getId();
            $grantExists = $grants->filter(
                function ($object) use ($id) {
                    return ($object->getReportForm()->getId() == $id);
                }
            );

            if ($grantExists->isEmpty()) {
                $grant = $this->createGrantConfig($reportForm, $appHistory);
                $em->persist($grant);
            }
        }

        $em->flush();

        return new RedirectResponse($this->generateUrl('admin_nko_order_grantconfig_list', ["filter[applicationHistory][value]" => $appHistory->getId(), 'competition_id' => $competitionId]));
    }

    private function createGrantConfig($reportForm, ApplicationHistory $appHistory)
    {
        $grant = new GrantConfig();
        $grant->setReportForm($reportForm);
        $grant->setApplicationHistory($appHistory);
        $appHistory->addGrant($grant);
        return $grant;
    }
}
