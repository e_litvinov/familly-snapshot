<?php

namespace NKO\OrderBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use NKO\OrderBundle\Entity\Organization;
use NKO\OrderBundle\Form\OrganizationType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Organization controller.
 *
 * @Route("/organization")
 */
class OrganizationController extends Controller
{
    public function getOrganizationsListAction(Request $request) {
        $topicId = $request->getContent();
        $query = $this->getDoctrine()->getRepository('NKOOrderBundle:Organization')
            ->createQueryBuilder('p')
            ->where('p.traineeshipTopicId = :id')
            ->setParameter('id', $topicId)
            ->getQuery();
        $organizations = $query->getResult();

        $organizationsNames = [];
        foreach ($organizations as $organization)
        {
            $organizationsNames[$organization->getId()] = $organization->getOrganizationName();
        }
        $organizationsNames = array_flip($organizationsNames);

        $organization = new Organization();
        $form = $this->createForm('NKO\OrderBundle\Form\OrganizationType', $organization,
            ['organizationsNames' => $organizationsNames]);

        return $this->render('@NKOOrder/layout/organizations.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
