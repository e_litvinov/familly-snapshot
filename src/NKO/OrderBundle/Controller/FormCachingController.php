<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 18.02.19
 * Time: 14:35
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Resolver\CacheResolver;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FormCachingController extends CRUDController
{
    public function saveAction(Request $request)
    {
        $cache = $this->getCacheByRequest($request);
        $cache->save($request->request->all());

        return new Response('ok');
    }

    public function clearAction(Request $request)
    {
        $cache = $this->getCacheByRequest($request);
        $cache->delete();

        return new Response('ok');
    }

    public function allowAutosaveAction(Request $request)
    {

        $object = $this->admin->getSubject();
        $logger = $this->get('NKO\LoggerBundle\Factory\LoggerFactory')->generateLogger($object);
        $status = (bool) ($request->get('status') === 'true');

        $logger->logAction(($status ? 'Включение' : 'Выключение'). ' автосохранения.', $object);

        return new Response('ok');
    }

    protected function getCacheByRequest(Request $request)
    {
        $id = $request->get('id');

        /** @var CacheResolver $cache */
        $cache = $this->get('nko_order.resolver.cache_resolver');

        $cache->generateItem($this->admin, $id);

        return $cache;
    }

    /**
     * @param $form Form
     * @param $cachedForm array|null
     */
    public function setCachedForm($form, $cachedForm = null)
    {
        if (!$cachedForm) {
            return;
        }

        $csrf = $this->container->get('security.csrf.token_manager');
        $token = $csrf->getToken($this->admin->getUniqid());
        $cachedForm = array_pop($cachedForm);
        $cachedForm['_token'] = $token->getValue();

        $this->admin->setNonGroups();

        $form->submit($cachedForm, false);
    }

    public function deleteAction($id)
    {
        $cache = $this->get('nko_order.resolver.cache_resolver');
        $cache->generateItem($this->admin, $id);
        $cache->delete();

        return parent::deleteAction($id);
    }
}
