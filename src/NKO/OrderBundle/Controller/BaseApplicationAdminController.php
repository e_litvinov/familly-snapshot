<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/13/17
 * Time: 6:10 PM
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Resolver\CacheResolver;
use NKO\OrderBundle\Utils\ApplicationResolver;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use NKO\DesignBlocksBundle\Entity\ApplicationHeader;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class BaseApplicationAdminController extends AdminController
{
    const BASE_LIST_ROUTE = 'admin_nko_order_baseapplication_list';
    protected $isReadyToSendPreviousVersion = null;
    protected $isSendPreviousVersion = null;


    public function createAction()
    {
        if (in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles())) {
            return $this->redirectToRoute(self::BASE_LIST_ROUTE);
        }

        $request = $this->getRequest();
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();

        if ($session->get('competition_id')
            && array_key_exists('value', $session->get('competition_id'))
            && $session->get('competition_id')['value']
        ) {
            $competition = $em->getRepository(Competition::class)
                ->find($session->get('competition_id')['value']);

            if ($em->getRepository(ApplicationHeader::class)
                ->findOneByApplicationClass($competition->getApplicationClass())) {
                $session->remove('competition_id');
                if (!$competition->getLink()) {
                    throw new \Exception("Have no link to generate application");
                }
                return $this->redirectToRoute($competition->getLink(), ['competition' => $competition->getId()]);
            } else {
                $this->addFlash('sonata_flash_error', 'customize the title of the application form');
                return new RedirectResponse($this->admin->generateUrl('list'));
            }
        }
        $current_user = $this->get('security.token_storage')->getToken()->getUser();
        $competitions = $em->getRepository(Competition::class)->getAvailiableCompetition($current_user);

        return $this->render(
            'NKOOrderBundle:baseapplication:new.html.twig',
            array('competitions' => $competitions)
        );
    }

    public function editAction($id = null)
    {
        if (array_key_exists(get_class($this->admin->getSubject()), ApplicationResolver::APPLICATION_EDIT_URLS)) {
            $em = $this->getDoctrine()->getManager();
            if ($em->getRepository(ApplicationHeader::class)
                    ->findOneByApplicationClass(get_class($this->admin->getSubject())) !== null) {
                return $this->redirectToRoute(ApplicationResolver::APPLICATION_EDIT_URLS[get_class($this->admin->getSubject())], ['id' => $id]);
            }
            $this->addFlash('sonata_flash_error', 'customize the title of the application form');
        }
        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function sendAction()
    {
        $object = $this->admin->getSubject();
        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to get the object'));
        }
        $service = $this->get('NKO\LoggerBundle\Loggers\ApplicationLogger');
        $service->logAction('Отправка заявки из списка заявок', $object);

        if ($object->getReadyToSent()) {
            $object->setIsSend(true);
            $object->setSendVersion($object->getSendVersion() + 1);
            $object->setIsAutosaved(null);
            $this->admin->update($object);
            $this->addFlash('sonata_flash_success', 'Application is sent');
        } else {
            $object->setIsSend(false);
            $this->admin->update($object);
            $this->addFlash('sonata_flash_error', 'sending the application failed');
        }

        return new RedirectResponse($this->generateUrl(self::BASE_LIST_ROUTE));
    }

    public function revertAction()
    {
        $object = $this->admin->getSubject();
        $this->get('nko.order.revert_manager')->revert($object);


        /** @var BaseApplication $application */
        $application = unserialize($this->admin->getSubject()->getData());

        $service = $this->container->get('NKO\LoggerBundle\Loggers\ApplicationLogger');
        $service->logAction('Восстановление заявки', $application);

        $id = $application->getId();
        $userId = $application->getAuthor()->getId();

        /** @var CacheResolver $cache */
        $cache = $this->get('nko_order.resolver.cache_resolver');

        /** @var AdminInterface $admin */
        $admin = $this->container->get('sonata.admin.pool')->getAdminByClass(get_class($application));

        $cache->generateItem($admin, $id, $userId);
        $cache->delete();

        $cache->generateItem($admin, $id);
        $cache->delete();

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function contactsAction(Request $request)
    {
        $competitionId = $request->get('competition');
        if (!$competitionId) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('First, select(filter) a competition'));
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        $competition =  $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(Competition::class)
            ->find($competitionId);

        $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Выгрузка таблицы “Контактная информация”', $competition->getName());

        $applications = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(BaseApplication::class)
            ->findBy(['competition' => $competitionId]);

        if (!$applications) {
            $this->addFlash('sonata_flash_error', $this->get('translator')->trans('no_competition_applications_available'));
            return new RedirectResponse($this->admin->generateUrl('list'));
        } else {
            $className = get_class(reset($applications));
            $filename = $this->get('nko.order.generator.excel_contacts_generator')->getContactsExcel($applications, $className);
            return $this->get('nko_order.downloader.excel_downloader')->getStreamedResponse($filename, $this->container->get('translator')->trans('contacts_table'));
        }
    }

    protected function redirectTo($object)
    {
        $request = $this->getRequest();

        $url = false;

        if (null !== $request->get('btn_update_and_list')) {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }
        if (null !== $request->get('btn_create_and_list')) {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        if (null !== $request->get('btn_create_and_create')) {
            $params = array();
            if ($this->admin->hasActiveSubClass()) {
                $params['subclass'] = $request->get('subclass');
            }
            $url = $this->admin->generateUrl('create', $params);
        }

        if ($this->getRestMethod() === 'DELETE') {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        if (!$url) {
            foreach (array('edit', 'show') as $route) {
                if ($this->admin->hasRoute($route) && $this->admin->isGranted(strtoupper($route), $object)) {
                    $url = $this->admin->generateObjectUrl($route, $object);
                    break;
                }
            }
        }

        if (!$url) {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        return new RedirectResponse($url);
    }

    /**
     * List action.
     *
     * @return Response
     *
     * @throws AccessDeniedException If access is not granted
     */
    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');
        $session = $request->getSession();

        $preResponse = $this->preList($request);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        if (array_key_exists('competition', $datagrid->getValues())) {
            $session->set('competition_id', $datagrid->getValues()['competition']);
        }

        $formView = $datagrid->getForm()->createView();

        return $this->render($this->admin->getTemplate('list'), array(
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ), null);
    }

    public function deleteAction($id)
    {
        $object = $this->admin->getSubject();
        $em = $this->getDoctrine()->getManager();
        $appHistories = $em->getRepository(ApplicationHistory::class)->findBy(['competition' => $object->getCompetition()->getId(), 'author' => $object->getAuthor()]);
        if ($appHistories) {
            foreach ($appHistories as $appHistory) {
                $appHistory->setDeletedAt(new \DateTime());
            }
        }
        return parent::deleteAction($id);
    }

    /**
     * @param $copyAdmin AdminInterface
     * @param $form Form
     * @param $cachedForm array|null
     */
    public function setCachedForm($form, $cachedForm = null)
    {
        if (!$cachedForm) {
            return;
        }

        $csrf = $this->container->get('security.csrf.token_manager');
        $token = $csrf->getToken($this->admin->getUniqid());
        $formData = array_shift($cachedForm);
        $formData['_token'] = $token->getValue();

        foreach ($cachedForm as $fields) {
            foreach ($fields as $field => $value) {
                $this->changesFiles($field, $formData, $value);
            }
        }

        $form->submit($formData);
    }

    protected function changesFiles($pathName, &$formData, $value)
    {
        $path = explode('_', $pathName);
        $this->recursiveChangeFiles($path, $formData, $value);
    }

    protected function recursiveChangeFiles($path, &$formData, $value)
    {
        $newPath = array_shift($path);

        if (!$path) {
            $formData[$newPath] = $value;
            return;
        }

        $this->recursiveChangeFiles($path, $formData[$newPath], $value);
    }

    protected function setFlagsBeforeEdit(BaseApplication $object)
    {
        if ($this->isReadyToSendPreviousVersion || $this->isSendPreviousVersion) {
            return;
        }

        $this->isReadyToSendPreviousVersion = $object->getReadyToSent();
        $this->isSendPreviousVersion = $object->getIsSend();
    }

    public function checkAutosaveVersion(BaseApplication $object)
    {
        if ($this->getRequest()->get('autosaver') != "true") {
            $object->setIsAutosaved(false);
            return;
        }

        $object->setIsSend($this->isSendPreviousVersion);
        $object->setReadyToSent($this->isReadyToSendPreviousVersion);
        $object->setIsAutosaved(true);
    }

    //Регистрируюем и получаем информацию о роутах внутреннего редиректа
    protected function isReferredFromCreateRoute($route)
    {
        if ($this->getReferredRoute($route)) {
            $this->saveReferredRoute($route, null);
            return true;
        }

        return null;
    }

    protected function saveReferredRoute($route, $value = null)
    {
        $session = $this->getRequest()->getSession();
        $session->set($route, $value);
    }

    protected function getReferredRoute($route)
    {
        $session = $this->getRequest()->getSession();
        return $session->get($route);
    }
}
