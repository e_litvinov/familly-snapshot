<?php

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Utils\Report\ReportTemplateResolver;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019KNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020KNS;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;

class BaseReportTemplateAdminController extends CRUDController
{
    const GRANT_CONFIG_LIST_ROUTE = 'admin_nko_order_grantconfig_list';

    protected function preCreate(Request $request, $object)
    {
        $em = $this->getDoctrine()->getManager();
        $appHistoryId = $request->get('app_history_id');
        $appHistory = $em->getRepository(ApplicationHistory::class)->find($appHistoryId);
        $object->setApplicationHistory($appHistory);
        $appHistory->addReportTemplate($object);

        $author = $em->getRepository(NKOUser::class)->findOneBy(['psrn' => $appHistory->getPsrn()]);
        $object->setAuthor($author);
        $author->addReportTemplate($object);

        $reportFormId = $request->get('report_form_id');
        $reportForm = $em->getRepository(ReportForm::class)->find($reportFormId);
        $object->setReportForm($reportForm);
        $reportForm->addReportTemplate($object);
    }

    public function createAction()
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_NKO_ORDER_ADMIN_REPORT_BASE_REPORT_TEMPLATE_CREATE');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', 'error_no_report_template_create_rights');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $mixedReportKNSLinks = $this->getReportTemplateLinks(MixedReportKNS::class);
        $mixedReport2019KNSLinks = $this->getReportTemplateLinks(MixedReport2019KNS::class);
        $mixedReport2020KNSLinks = $this->getReportTemplateLinks(MixedReport2020KNS::class);
        $financeReport2018Links = $this->getReportTemplateLinks(FinanceReport2018::class);
        $financeReport2019Links = $this->getReportTemplateLinks(FinanceReport2019::class);

        return $this->render('NKOOrderBundle:CRUD/report:new_report_template.html.twig', [
            'financeReportLinks' => $financeReport2018Links,
            'mixedReportKNSLinks' => $mixedReportKNSLinks,
            'mixedReport2019KNSLinks' => $mixedReport2019KNSLinks,
            'mixedReport2020KNSLinks' => $mixedReport2020KNSLinks,
            'financeReport2019Links' => $financeReport2019Links,
        ]);
    }

    protected function logAction($action, BaseReportTemplate $template)
    {
        $this->container->get('NKO\LoggerBundle\Loggers\ReportTemplateLogger')
            ->logAction($action, $template);
    }

    protected function logCreateAction(BaseReportTemplate $template)
    {
        $this->logAction('Добавление показателей', $template);
    }

    private function getReportTemplateLinks($reportClass)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if (!$user->hasRole('ROLE_NKO')) {
            $applicationHistory = $em->getRepository(ApplicationHistory::class)->find($this->getRequest()->get('app_history_id'));
            $user = $applicationHistory->getAuthor();
        }

        $reportForms = $em->getRepository(ReportForm::class)->getCurrentReportFormByReportClass($reportClass, $user);
        $links = [];
        foreach ($reportForms as $reportForm) {
            $sentApplication = $em->getRepository(ApplicationHistory::class)->getSpreadApplicationHistory($reportForm, $user);

            if (!$this->getRelatedSumGrant($reportForm, $sentApplication)) {
                continue;
            }

            $link['title'] = $reportForm->getTitle();
            $link['report_form_id'] = $reportForm->getId();
            $link['app_history_id'] = $sentApplication->getId();
            $links[] = $link;
        }

        return $links;
    }

    protected function redirectTo($object)
    {
        $request = $this->getRequest();

        $url = false;

        if (null !== $request->get('btn_update_and_list')) {
            $url = $this->generateUrl(ExpertMarkAdminController::LIST_ROUTE);
        }
        if (null !== $request->get('btn_create_and_list')) {
            $url = $this->admin->generateUrl('list');
        }

        if (null !== $request->get('btn_create_and_create')) {
            $params = array();
            if ($this->admin->hasActiveSubClass()) {
                $params['subclass'] = $request->get('subclass');
            }
            $url = $this->admin->generateUrl('create', $params);
        }

        if ($this->getRestMethod() === 'DELETE') {
            $url = $this->admin->generateUrl('list');
        }

        if (!$url) {
            foreach (array('edit', 'show') as $route) {
                if ($this->admin->hasRoute($route) && $this->admin->hasAccess($route, $object)) {
                    $url = $this->admin->generateObjectUrl($route, $object);
                    break;
                }
            }
        }

        if (!$url) {
            $url = $this->admin->generateUrl('list');
        }

        return new RedirectResponse($url);
    }

    public function editAction($id = null)
    {
        $template = $this->admin->getSubject();
        $reportClass = $template->getReportForm()->getReportClass();

        if (array_key_exists($reportClass, ReportTemplateResolver::URL_BY_REPORT_CLASS)) {
            return new RedirectResponse($this->generateUrl(ReportTemplateResolver::URL_BY_REPORT_CLASS[$reportClass] . '_edit', ['id' => $template->getId()]));
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    protected function getRelatedSumGrant($reportForm, $sentApplication)
    {
        $em = $this->getDoctrine()->getManager();
        $grant = $em->getRepository(GrantConfig::class)
            ->findOneBy(['reportForm'=>$reportForm, 'applicationHistory'=>$sentApplication]);
        return $grant ? $grant->getSumGrant() : null;
    }

    protected function checkIssetTemplate($object)
    {
        $em = $this->get('doctrine')->getManager();
        return $em->getRepository(BaseReportTemplate::class)->findOneBy([
            'reportForm' => $object->getReportForm(),
            'applicationHistory' => $object->getApplicationHistory(),
        ]);
    }
}
