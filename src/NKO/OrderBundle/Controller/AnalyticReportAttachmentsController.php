<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/14/17
 * Time: 11:59 AM
 */

namespace NKO\OrderBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\OrderBundle\Utils\Report\AttachmentParameters;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AnalyticReportAttachmentsController extends AttachmentController
{
    public function attachmentsListAction(Request $request)
    {
        if ($request->get('id')) {
            $id = $request->get('id');

            $em = $this->container->get('Doctrine')->getManager();
            $report = $em->getRepository(Report::class)->find($id);

            $attachments = $this->attachmentsList($report, AttachmentParameters::ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT);

            return $this->render('@NKOOrder/CRUD/report/analytic_report/attachments_list.html.twig', ['attachments' => $attachments]);
        }

        return $this->redirectToRoute('admin_nko_order_basereport_list');
    }

    public function downloadAnalyticReportAttachmentAction(Request $request)
    {
        $entity = $request->get('entity');
        $id = $request->get('id');
        $field = $request->get('field');

        if($entity && $id && $field) {
            $em = $this->get('Doctrine')->getManager();
            $object = $em->getRepository($entity)->find($id);

            $resolver = $this->get('itm.file.preview.path.resolver');
            $path = $resolver->getPath($object, $field);

            if($path) {
                $response = new StreamedResponse();
                $response->setCallback(function() use($path) {
                    echo file_get_contents($path);
                });
                $content_type = mime_content_type($path);

                $response->headers->set('Content-Type', $content_type);
                $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, basename($path));
                $response->headers->set('Content-Disposition', $contentDisposition);

                return $response;
            }
        }

        return $this->redirectToRoute('admin_nko_order_basereport_list');
    }
}