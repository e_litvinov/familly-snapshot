<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 17.07.17
 * Time: 13:29
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019KNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020KNS;

class GrantConfigAdminController extends CRUDController
{
    const MONITORING_REPORT_TEMPLATE_ROUTE = 'admin_nko_order_report_monitoringreport_reporttemplate';
    const FINANCE_REPORT_TEMPLATE_ROUTE = 'admin_nko_order_report_financereport_reporttemplate';
    const ANALYTIC_REPORT_TEMPLATE_ROUTE = 'admin_nko_order_report_analyticreport_reporttemplate';
    const MONITORING_REPORT_EDIT_ROUTE = 'admin_nko_order_report_monitoringreport_report_edit';
    const FINANCE_REPORT_TEMPLATE_2018_ROUTE = 'admin_nko_order_report_financereport_report2018_reporttemplate';
    const FINANCE_REPORT_TEMPLATE_2019_ROUTE = 'admin_nko_order_report_financereport_report2018_reporttemplate';
    const MIXED_REPORT_TEMPLATE_ROUTE = 'admin_nko_order_report_financereport_report2018_reporttemplate';
    const MIXED_REPORT2019_TEMPLATE_ROUTE = 'admin_nko_order_report_financereport_report2018_reporttemplate';
    const MIXED_REPORT2020_TEMPLATE_ROUTE = 'admin_nko_order_report_financereport_report2018_reporttemplate';

    public function prefillReportAction(Request $request)
    {
        $grant = $this->admin->getSubject();
        switch ($grant->getReportForm()->getReportClass()) {
            case MonitoringReport::class:
                return $this->redirectToReportTemplate($grant, self::MONITORING_REPORT_TEMPLATE_ROUTE);
            case Harbor2020MonitoringReport::class:
                return $this->redirectToReportTemplate($grant, self::MONITORING_REPORT_TEMPLATE_ROUTE);
            case FinanceReport::class:
                return $this->redirectToReportTemplate($grant, self::FINANCE_REPORT_TEMPLATE_ROUTE);
            case AnalyticReport::class:
                return $this->redirectToReportTemplate($grant, self::ANALYTIC_REPORT_TEMPLATE_ROUTE);
            case FinanceReport2018::class:
                return $this->redirectToReportTemplate($grant, self::FINANCE_REPORT_TEMPLATE_2018_ROUTE);
            case FinanceReport2019::class:
                return $this->redirectToReportTemplate($grant, self::FINANCE_REPORT_TEMPLATE_2019_ROUTE);
            case MixedReportKNS::class:
                return $this->redirectToReportTemplate($grant, self::MIXED_REPORT_TEMPLATE_ROUTE);
            case MixedReport2019KNS::class:
                return $this->redirectToReportTemplate($grant, self::MIXED_REPORT2019_TEMPLATE_ROUTE);
            case MixedReport2020KNS::class:
                return $this->redirectToReportTemplate($grant, self::MIXED_REPORT2020_TEMPLATE_ROUTE);
            default:
                $this->addFlash('sonata_flash_error', 'prefill_not_available');
        }

        return new RedirectResponse($request->server->get('HTTP_REFERER'));
    }

    private function redirectToReportTemplate($grant, $route)
    {
        $em = $this->getDoctrine()->getManager();
        $reportForm = $grant->getReportForm();
        $applicationHistory = $grant->getApplicationHistory();
        $isFilledByExpert = $reportForm-> getIsFilledByExpert();

        $template = $em->getRepository(BaseReportTemplate::class)->findOneBy([
            'reportForm' => $reportForm,
            'applicationHistory' => $applicationHistory
        ]);

        if (!$template && !$isFilledByExpert) {
            $this->addFlash('sonata_flash_error', 'organization_does_not_create_template_result');
            return new RedirectResponse($this->generateUrl('admin_nko_order_grantconfig_list', $this->generateListData($applicationHistory)));
        }

        if (!$template) {
            return new RedirectResponse($this->generateUrl($route . '_create', [
                'app_history_id' => $applicationHistory->getId(),
                'report_form_id' => $reportForm->getId()
            ]));
        }
        return new RedirectResponse($this->generateUrl($route . '_edit', ['id' => $template->getId()]));
    }

    public function checkoutValuesAction(Request $request)
    {
        $grant = $this->admin->getSubject();
        $applicationHistory = $grant->getApplicationHistory();

        $em = $this->getDoctrine()->getManager();
        $report = $em->getRepository(MonitoringReport::class)->findOneBy([
            'reportForm' => $grant->getReportForm(),
            'applicationHistory' => $grant->getApplicationHistory()
        ]);

        if ($report) {
            return new RedirectResponse($this->generateUrl('admin_nko_order_admin_report_monitoring_report_accept_plan_edit', ['id' => $report->getId()]));
        }

        $report = $em->getRepository(Harbor2020MonitoringReport::class)->findOneBy([
            'reportForm' => $grant->getReportForm(),
            'applicationHistory' => $grant->getApplicationHistory()
        ]);

        if ($report) {
            return new RedirectResponse($this->generateUrl('admin_nko_order_admin_report_monitoring_report_harbor_2020_accept_plan_edit', ['id' => $report->getId()]));
        }

        if (!$report) {
            $this->addFlash('sonata_flash_error', 'no_object_exists');
            return new RedirectResponse($this->generateUrl('admin_nko_order_grantconfig_list', $this->generateListData($applicationHistory)));
        }
    }


    public function undoAcceptingAction($id = null)
    {
        $grant = $this->admin->getSubject();
        $em = $this->getDoctrine()->getManager();
        $applicationHistory = $grant->getApplicationHistory();

        $report = $em->getRepository(MonitoringReport::class)->findOneBy([
            'reportForm' => $grant->getReportForm(),
            'applicationHistory' => $applicationHistory
        ]);

        if (!$report) {
            $report = $em->getRepository(Harbor2020MonitoringReport::class)->findOneBy([
                'reportForm' => $grant->getReportForm(),
                'applicationHistory' => $applicationHistory
            ]);
        }

        if (!$report) {
            $this->addFlash('sonata_flash_error', 'no_object_exists');
            return new RedirectResponse($this->generateUrl('admin_nko_order_grantconfig_list', $this->generateListData($applicationHistory)));
        }

        $service = $this->get('NKO\LoggerBundle\Loggers\ReportLogger');

        if ($report->getIsAccepted()) {
            $this->addFlash('sonata_flash_error', 'Report is accepted');
        } else {
            $service->logAction('Отмена принятия показателей мониторингового отчета', $report);

            $report->setIsPlanAccepted(false);
            $em->flush();
            $this->addFlash('sonata_flash_success', 'Report plan is not accepted again');
        }

        return new RedirectResponse($this->generateUrl('admin_nko_order_grantconfig_list', $this->generateListData($applicationHistory)));
    }

    public function generateListData($applicationHistory)
    {
        $competitionId = unserialize($applicationHistory->getData())->getCompetition()->getId();
        return [
            "filter[applicationHistory][value]" => $applicationHistory->getId(),
            'competition_id' => $competitionId
        ];
    }
}
