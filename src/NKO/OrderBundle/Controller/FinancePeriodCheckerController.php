<?php

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceReportInterface;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FinancePeriodCheckerController extends Controller
{
    const FINANCEREPORT = 'NKO\OrderBundle\Entity\Report\FinanceReport\Report';
    const FINANCEREPORT2018 = 'NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report';
    const FINANCEREPORT2019 = 'NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report';

    /**
     * Check FinanceReport periods, than redirect to HelperController or return custom button
     *
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function checkFinancePeriodAction(Request $request)
    {
        $field = $request->get('field');
        $code = $request->get('code');
        $objectId = $request->get('objectId');
        $value = $originalValue = $request->get('value');
        $context = $request->get('context');
        /** @var ReportHistory $object */
        $object = $this->getDoctrine()->getRepository(ReportHistory::class)->find($objectId);
        $reportClassName = $object->getReportForm()->getReportClass();

        if ($value && $this->isFinance($reportClassName)) {
            $pastPeriods = $this->getDoctrine()->getRepository(PeriodReport::class)->findPastReportPeriods($object);
            if ($lastPeriod = array_pop($pastPeriods)) {
                $isPreviosHistoryAccepted = $this->getDoctrine()->getRepository(ReportHistory::class)->getIsAcceptedByPsrnAndReportFormAndPeriod($object->getPsrn(), $object->getReportForm(), $lastPeriod);
                if ($isPreviosHistoryAccepted) {
                    return $this->redirectToBaseController($code, $context, $field, $objectId, $value);
                }

                return new JsonResponse($this->templateConstructor($code, $context, $field, $objectId, !$value), 200);
            }
        }

        return $this->redirectToBaseController($code, $context, $field, $objectId, $value);
    }

    /**
     * Redirect to HelperController which will execute the request
     *
     * @param $code
     * @param $context
     * @param $field
     * @param $objectId
     * @param $value
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function redirectToBaseController($code, $context, $field, $objectId, $value)
    {
        return $this->redirectToRoute('sonata_admin_set_object_field_value', [
            'code' => $code,
            'context' => $context,
            'field' => $field,
            'objectId' => $objectId,
            'value' => $value
        ], 307);
    }

    /**
     * Insert data in button template
     *
     * @param $code
     * @param $context
     * @param $field
     * @param $objectId
     * @param $value
     *
     * @return string
     */
    private function templateConstructor($code, $context, $field, $objectId, $value)
    {
        $valueString = $value ? '1' : '0';
        return "<td class=\"sonata-ba-list-field sonata-ba-list-field-boolean\" objectid=\"$objectId\">                      
            <span class=\"x-editable editable editable-click not-last\" data-type=\"select\" data-value=\"$valueString\" data-title=\"Принять\" data-pk=\"$objectId\" data-url=\"/app_dev.php/core/custom-set-object-field-value?context=$context&amp;field=$field&amp;objectId=$objectId&amp;code=$code\" data-source=\"[{value: 0, text: 'нет'},{value: 1, text: 'да'}]\">
                <span class=\"label label-danger\">нет</span>
            </span>
        </td>";
    }

    /**
     * Check whether the financial report
     *
     * @param $reportClassName
     *
     * @return bool
     */
    private function isFinance($reportClassName)
    {
        return is_a($reportClassName, FinanceReportInterface::class, true);
    }
}
