<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/13/17
 * Time: 6:10 PM
 */

namespace NKO\OrderBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\ReportHistory;
use PHPExcel_IOFactory;
use PHPExcel_Settings;
use PHPExcel_Style_Border;
use PHPExcel_Writer_Excel5;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report as KNS2016Report;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report as AnalyticReport2018;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as KNSReport2018;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019KNS;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020KNS;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report as AnalyticReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report as HarborReport2019;

class BaseReportAdminController extends AdminController
{
    const CRITERIA_NUMBER = 3;
    const RESULT_ROWS_NUMBER = 5;
    const ROW_HEIGHT = 20;
    const BASE_LIST_ROUTE = 'admin_nko_order_basereport_list';
    const BASE_CREATE_ROUTE = 'admin_nko_order_basereport_create';

    /**
     * @var EntityManager
     */
    protected $em;

    protected function preCreate(Request $request, $object)
    {
        $periodId = $request->get('period_id');
        $object->setPeriodId($periodId);

        $session = $request->getSession();
        if ($periodId) {
            $period = $this->getDoctrine()->getManager()
                ->getRepository('NKOOrderBundle:PeriodReport')
                ->find($periodId);
            $object->setPeriod($period);
            $session->set('period_id', $periodId);
        }

        $reportFormId = $request->get('report_form_id');
        $object->setReportFormId($reportFormId);

        if ($reportFormId) {
            $session = $request->getSession();
            $session->set('report_form_id', $reportFormId);
        }
    }

    protected function preSetValues($reportFormId, $object)
    {
        $em = $this->getDoctrine()->getManager();
        $reportForm = $em->getRepository(ReportForm::class)->find($reportFormId);
        $object->setReportForm($reportForm);

        $appHistory = $em->getRepository(ApplicationHistory::class)->findOneBy([
            'author' => $this->getUser(),
            'competition' => $reportForm->getCompetition(),
            'isSpread' => true
        ]);
        $object->setApplicationHistory($appHistory);

        $reportTemplate = $em->getRepository(BaseReportTemplate::class)->findOneBy([
            'reportForm' => $reportForm,
            'applicationHistory' => $appHistory
        ]);

        if ($reportTemplate) {
            $object->setReportTemplate($reportTemplate);
        }
    }

    public function createAction()
    {
        try {
            $this->denyAccessUnlessGranted('ROLE_SONATA_ADMIN_NKO_ORDER_BASE_REPORT_CREATE');
        } catch (AccessDeniedException $e) {
            $this->addFlash('sonata_flash_error', 'error_no_report_create_rights');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        $this->em = $this->getDoctrine()->getManager();

        $financeReport2018Links = $this->getPeriodDependentLinks(
            $this->getCurrentReportFormByReportClass(FinanceReport2018::class)
        );
        $financeReport2019Links = $this->getPeriodDependentLinks(
            $this->getCurrentReportFormByReportClass(FinanceReport2019::class)
        );
        $financeReportLinks = $this->getPeriodDependentLinks(
            $this->getCurrentReportFormByReportClass(FinanceReport::class)
        );
        $monitoringReportLinks = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(MonitoringReport::class)
        );
        $harbor2020MonitoringReportLinks = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(Harbor2020MonitoringReport::class)
        );
        $analyticReportLinks = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(AnalyticReport::class)
        );
        $analyticKnsReportLinks = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(KNS2016Report\AnalyticReport\KNS\Stage1\Report::class)
        );
        $analyticReport2018Links = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(AnalyticReport2018::class)
        );
        $analyticReport2019Links = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(AnalyticReport2019::class)
        );
        $analyticKNSReport2018Links = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(KNSReport2018::class)
        );
        $mixedReportKNSLinks = $this->getPeriodDependentLinks(
            $this->getCurrentReportFormByReportClass(MixedReportKNS::class)
        );
        $mixedReport2019KNSLinks = $this->getPeriodDependentLinks(
            $this->getCurrentReportFormByReportClass(MixedReport2019KNS::class)
        );
        $mixedReport2020KNSLinks = $this->getPeriodDependentLinks(
            $this->getCurrentReportFormByReportClass(MixedReport2020KNS::class)
        );
        $analyticHarbor2019Links = $this->getPeriodIndependentLinks(
            $this->getCurrentReportFormByReportClass(HarborReport2019::class)
        );

        return $this->render('NKOOrderBundle:CRUD/report:new.html.twig', [
            'financeReportLinks' => $financeReportLinks,
            'monitoringReportLinks' => $monitoringReportLinks,
            'harbor2020MonitoringReportLinks' => $harbor2020MonitoringReportLinks,
            'analyticReportLinks' => $analyticReportLinks,
            'analyticKnsReportLinks' => $analyticKnsReportLinks,
            'financeReport2018Links' => $financeReport2018Links,
            'financeReport2019Links' => $financeReport2019Links,
            'analyticReport2018Links' => $analyticReport2018Links,
            'analyticReport2019Links' => $analyticReport2019Links,
            'analyticKNSReport2018Links' => $analyticKNSReport2018Links,
            'mixedReportKNSLinks' => $mixedReportKNSLinks,
            'mixedReport2019KNSLinks' => $mixedReport2019KNSLinks,
            'mixedReport2020KNSLinks' => $mixedReport2020KNSLinks,
            'analyticHarbor2019Links' => $analyticHarbor2019Links,
        ]);
    }

    private function getCurrentReportFormByReportClass($reportClass)
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('rf')
            ->from('NKOOrderBundle:Report\ReportForm', 'rf')
            ->where(':current_date between rf.startDate and rf.finishDate')
            ->andWhere('rf.reportClass = :reportClass')
            ->setParameters([
                'current_date' => new \DateTime(),
                'reportClass' => $reportClass,
            ]);

        if ($reportClass == FinanceReport2018::class) {
            $qb
                ->innerJoin('rf.reportTemplates', 'rt')
                ->andWhere('rt.author = :author')
                ->setParameter('author', $this->getUser());
        }

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }

    private function getPeriodDependentLinks($reportForms)
    {
        $links = [];
        foreach ($reportForms as $reportForm) {
            foreach ($reportForm->getReportPeriods() as $period) {
                $link['reportFormId'] = $reportForm->getId();
                $link['linkName'] = $reportForm->getTitle();
                $link['periodId'] = $period->getId();
                $link['periodName'] = $period->getName();
                $links[] = $link;
            }
        }
        return $links;
    }

    private function getPeriodIndependentLinks($reportForms)
    {
        $links = [];
        foreach ($reportForms as $reportForm) {
            $link['reportFormId'] = $reportForm->getId();
            $link['linkName'] = $reportForm->getTitle();
            $links[] = $link;
        }

        return $links;
    }

    public function editAction($id = null)
    {
        switch (get_class($this->admin->getSubject())) {
            case KNS2016Report::class:
                return $this->redirectToRoute('admin_nko_order_report_edit', [
                    'id' => $id
                ]);
            case FinanceReport::class:
                return $this->redirectToRoute('admin_nko_order_report_financereport_report_edit', [
                    'id' => $id
                ]);
            case FinanceReport2018::class:
                return $this->redirectToRoute('admin_nko_order_report_financereport_report2018_report_edit', [
                    'id' => $id
                ]);
            case FinanceReport2019::class:
                return $this->redirectToRoute('admin_nko_order_report_financereport_report2019_report_edit', [
                    'id' => $id
                ]);
            case MonitoringReport::class:
                return $this->redirectToRoute('admin_nko_order_report_monitoringreport_report_edit', [
                    'id' => $id
                ]);
            case Harbor2020MonitoringReport::class:
                return $this->redirectToRoute('admin_nko_order_report_monitoringreport_harbor2020report_report_edit', [
                    'id' => $id
                ]);
            case AnalyticReport::class:
                return $this->redirectToRoute('admin_nko_order_report_analyticreport_report_edit', [
                    'id' => $id
                ]);
            case KNS2016Report\AnalyticReport\KNS\Stage1\Report::class:
                return $this->redirectToRoute('admin_nko_order_report_analyticreport_kns_stage1_report_edit', [
                    'id' => $id
                ]);
            case AnalyticReport2018::class:
                return $this->redirectToRoute('admin_nko_order_report_analyticreport_report2018_report_edit', [
                    'id' => $id
                ]);
            case AnalyticReport2019::class:
                return $this->redirectToRoute('admin_nko_order_report_analyticreport_report2019_report_edit', [
                    'id' => $id
                ]);
            case KNSReport2018::class:
                return $this->redirectToRoute('admin_nko_order_report_analyticreport_report2018_kns_report_edit', [
                    'id' => $id
                ]);
            case MixedReportKNS::class:
                return $this->redirectToRoute('admin_nko_order_report_mixedreport_kns_report_edit', [
                    'id' => $id
                ]);
            case MixedReport2019KNS::class:
                return $this->redirectToRoute('admin_nko_order_report_mixedreport_kns_report2019_report_edit', [
                    'id' => $id
                ]);
            case MixedReport2020KNS::class:
                return $this->redirectToRoute('admin_nko_order_report_mixedreport_kns_report2020_report_edit', [
                    'id' => $id
                ]);
            case HarborReport2019::class:
                return $this->redirectToRoute('admin_nko_order_report_analyticreport_harbor_report2019_report_edit', [
                    'id' => $id
                ]);
            default:
                return new RedirectResponse($this->admin->generateUrl('list'));
        }
    }

    public function completeEditionAction($id = null)
    {
        switch (get_class($this->admin->getSubject())) {
            case FinanceReport::class:
                return $this->redirectToRoute('admin_nko_order_report_financereport_report_complete_edition', [
                    'id' => $id
                ]);
            case FinanceReport2018::class:
                return $this->redirectToRoute('admin_nko_order_report_financereport_report2018_report_complete_edition', [
                    'id' => $id
                ]);
            case FinanceReport2019::class:
                return $this->redirectToRoute('admin_nko_order_report_financereport_report2019_report_complete_edition', [
                    'id' => $id
                ]);
            default:
                return new RedirectResponse($this->admin->generateUrl('list'));
        }
    }

    public function sendReportAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to get the object'));
        }

        if ($object->getIsValid()) {
            $object->setIsSend(true);
            $this->admin->update($object);
            $this->addFlash('sonata_flash_success', 'Report is sent');
        } else {
            $object->setIsSend(false);
            $this->admin->update($object);
            $this->addFlash('sonata_flash_error', 'sending the report failed');
        }

        return new RedirectResponse($this->generateUrl(self::BASE_LIST_ROUTE));
    }

    public function revertReportAction()
    {
        $object = $this->admin->getSubject();
        $this->get('nko.order.revert_manager')->revert($object);

        $service = $this->container->get('NKO\LoggerBundle\Loggers\ReportLogger');
        $service->logAction('Восстановление отчёта', unserialize($object->getData()));

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function downloadReportAction()
    {
        $report = $this->admin->getSubject();

        if (!$report) {
            throw $this->createNotFoundException('unable to find the object');
        }

        $nameOfAction = 'Скачивание отчета из Мастера';
        if ($report instanceof ReportHistory) {
            $report = unserialize($report->getData());
                  $nameOfAction = 'Скачивание отчета из истории';
        } elseif ($report->getIsAutosaved()) {
            $em = $this->getDoctrine()->getManager();

            $history = $em->getRepository(ReportHistory::class)
                ->findLastManualSubmittedHistory($report);
            if (!$history) {
                $history = $em->getRepository(ReportHistory::class)
                    ->findLastManualSubmittedHistory($report, false);
            }
            $report = unserialize($history->getData());
            $nameOfAction = 'Скачивание отчета из объекта(последней сохранённой вручную версии).';
        }

        $service = $this->container->get('NKO\LoggerBundle\Loggers\ReportLogger');
        $service->logAction($nameOfAction, $report);

        $response = $this->get(get_class($report).self::ENDING_DOWNLOADING_SERVICE)->generate($report);

        if (!$response) {
            $this->addFlash('sonata_flash_error', 'File generating failed');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        return $response;
    }

    protected function redirectTo($object)
    {
        $request = $this->getRequest();

        $url = false;

        if (null !== $request->get('btn_update_and_list')) {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        if (null !== $request->get('btn_create_and_list')) {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        if (null !== $request->get('btn_create_and_create')) {
            $params = [];

            if ($this->admin->hasActiveSubClass()) {
                $params['subclass'] = $request->get('subclass');
            }
            $url = $this->generateUrl(self::BASE_CREATE_ROUTE, $params);
        }

        if ($this->getRestMethod() === 'DELETE') {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        if (!$url) {
            foreach (array('edit', 'show') as $route) {
                if ($this->admin->hasRoute($route) && $this->admin->hasAccess($route, $object)) {
                    $url = $this->admin->generateObjectUrl($route, $object);
                    break;
                }
            }
        }

        if (!$url) {
            $url = $this->generateUrl(self::BASE_LIST_ROUTE);
        }

        return new RedirectResponse($url);
    }


    public function resultsAction()
    {
        $criteria = [];
        for ($i = 0; $i < self::CRITERIA_NUMBER; $i++) {
            $criteria[] = $this->getDoctrine()->getManager()
                ->getRepository('NKOOrderBundle:ImmediateResult')->findByCriteriaFromAccepted($i);
        }

        $results = [];
        foreach ($criteria as $criteriaNum => $values) {
            $results[$criteriaNum] = [
                'planValue' => 0,
                'factValue' => 0,
                'expectedValue' => 0
            ];

            foreach ($values as $value) {
                $results[$criteriaNum]['planValue'] += $value['planValue'];
                $results[$criteriaNum]['factValue'] += $value['factValue'];
                $results[$criteriaNum]['expectedValue'] += $value['expectedValue'];
            }
        }

        $filename = $this->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/results.xls';

        $temp_file_name = tempnam(sys_get_temp_dir(), 'Results_');
        $temp_out_file_name = $temp_file_name . '.xls';
        file_put_contents($temp_out_file_name, $filename);

        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        $sheet = $excelObj->getActiveSheet();

        $sheet->setCellValue('B3', $results[0]['planValue']);
        $sheet->setCellValue('C3', $results[0]['factValue']);
        $sheet->setCellValue('D4', $results[1]['expectedValue']);
        $sheet->setCellValue('B5', $results[2]['planValue']);
        $sheet->setCellValue('C5', $results[2]['factValue']);
        $sheet->setCellValue('D5', $results[2]['expectedValue']);

        foreach (range('A', 'D') as $columnID) {
            for ($i = 2; $i <= self::RESULT_ROWS_NUMBER; $i++) {
                $sheet->getRowDimension($i + 1)->setRowHeight(self::ROW_HEIGHT);
            }
        }

        foreach (range('A', 'D') as $columnID) {
            for ($i = 0; $i <= self::RESULT_ROWS_NUMBER; $i++) {
                $sheet->getStyle($columnID.$i)->applyFromArray(
                    array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                'color' => array('rgb' => '000000'))
                        )
                    )
                );
            }
        }

        $objWriter = new PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($temp_out_file_name);

        $response = new StreamedResponse();
        $response->setCallback(function () use ($temp_out_file_name) {
            echo file_get_contents($temp_out_file_name);
            unlink($temp_out_file_name);
        });

        $values = 'attachment; filename="' . $this->get('translator')->trans('results') . '.xls";';
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        $response->headers->set('Content-Disposition', $values);

        return $response;
    }

    public function downloadSummaryReportAction()
    {
        $template = null;
        $report = $this->admin->getSubject();

        if (!$report) {
            throw $this->createNotFoundException('unable to find the object');
        }

        if ($report instanceof ReportHistory) {
            $report = unserialize($report->getData());
        }

        if ($report instanceof FinanceReport) {
            $template = 'NKOOrderBundle:CRUD/report/finance_report:summary_report_template.html.twig';
        } else {
            $this->addFlash('sonata_flash_error', 'summary_report_not_available');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $em = $this->getDoctrine()->getManager();
        $reportForm = $em->getRepository('NKOOrderBundle:Report\ReportForm')->find($report->getReportForm());

        $reports = $em->createQueryBuilder()
            ->select('r')
            ->from('NKOOrderBundle:Report\FinanceReport\Report', 'r')
            ->innerJoin('r.period', 'p', Join::WITH, 'r.periodId = p.id')
            ->where('r.reportForm = :reportForm')
            ->andWhere('r.author = :author')
            ->andWhere('p.startDate <= :currentPeriodDate')
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $reportForm,
                'author' => $report->getAuthor(),
                'currentPeriodDate' => $report->getPeriod()->getStartDate()
            ])
            ->getResult()
        ;

        $expenses = [];
        $totalValues = [];
        foreach ($reports as $report) {
            $registers = $report->getRegisters();
            foreach ($registers as $register) {
                if (!array_key_exists($register->getExpenseType()->getCode(), $totalValues)) {
                    $totalValues[$register->getExpenseType()->getCode()] = $register->getTotalExpenseValue();
                } else {
                    $totalValues[$register->getExpenseType()->getCode()] += $register->getTotalExpenseValue();
                }

                foreach ($register->getExpenses() as $expense) {
                    $expenses[$register->getExpenseType()->getCode()][] = $expense;
                }
            }
        }

        $lastReport = end($reports);
        $expenseTypes = $reportForm->getExpenseTypes();

        $html = $this->renderView($template, [
            'lastReport' => $lastReport,
            'expenseTypes' => $expenseTypes,
            'expenseArrays' => $expenses,
            'totalValues' => $totalValues
        ]);

        $response = $this->get('nko_order.downloader.pdf_downloader')->getStreamedResponse($html, $report->getPsrn());

        if (!$response) {
            $this->addFlash('sonata_flash_error', 'PDF generating failed');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }
        return $response;
    }

    /**
     * @param $copyAdmin AdminInterface
     * @param $form Form
     * @param $cachedForm array|null
     */
    public function setCachedForm($form, $cachedForm = null)
    {
        if (!$cachedForm) {
            return;
        }

        $csrf = $this->container->get('security.csrf.token_manager');
        $token = $csrf->getToken($this->admin->getUniqid());
        $formData = array_shift($cachedForm);
        $formData['_token'] = $token->getValue();

        foreach ($cachedForm as $fields) {
            foreach ($fields as $field => $value) {
                $this->changesFiles($field, $formData, $value);
            }
        }

        $form->submit($formData);
    }

    protected function changesFiles($pathName, &$formData, $value)
    {
        $path = explode('_', $pathName);
        $this->recursiveChangeFiles($path, $formData, $value);
    }

    protected function recursiveChangeFiles($path, &$formData, $value)
    {
        $newPath = array_shift($path);

        if (!$path) {
            $formData[$newPath] = $value;
            return;
        }

        $this->recursiveChangeFiles($path, $formData[$newPath], $value);
    }
}
