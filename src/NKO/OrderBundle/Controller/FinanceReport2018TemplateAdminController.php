<?php

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Entity\ExpenseTypeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Exception\ModelManagerException;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReportKNS2018;

class FinanceReport2018TemplateAdminController extends BaseReportTemplateAdminController
{
    const SALARY = [
        FinanceReport2018::class,
        FinanceReport2019::class
    ];

    public function createAction()
    {
        $request = $this->getRequest();
        $this->admin->checkAccess('create');
        $object = $this->admin->getNewInstance();

        $preResponse = $this->preCreate($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($this->checkIssetTemplate($object)) {
            $this->addFlash('sonata_flash_error', 'error_report_template_is_isset');
            return new RedirectResponse($this->admin->generateUrl('list'));
        }

        $expenseTypes = $this->getExpenseTypes($object->getReportForm());

        if (!$expenseTypes) {
            $this->addFlash('sonata_flash_error', 'error_expense_type_not_found');
            return new RedirectResponse($this->generateUrl(self::GRANT_CONFIG_LIST_ROUTE));
        }

        $this->get('nko_order.finance_report_manager.finance_spent')->createFinanceSpentByExpenseTypes($object, $expenseTypes);

        $this->admin->setSubject($object);
        $object = $this->admin->create($object);
        $this->logCreateAction($object);

        return $this->redirectTo($object);
    }

    public function editAction($id = null)
    {
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $this->admin->checkAccess('edit', $object);

        $preResponse = $this->preEdit($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        /** @var $form Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($object);
            }
            $isFormValid = $form->isValid();

            $grant = $this->getRelatedSumGrant($object->getReportForm(), $object->getApplicationHistory());
            $isEqualSum = $this->checkEqualSum($object, $grant);

            if (!$isEqualSum) {
                $this->addFlash('sonata_flash_error', 'error_different_amounts');
            }

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved()) && $isEqualSum) {
                try {
                    if ($this->getUser()->hasRole('ROLE_NKO')) {
                        if ($object->getIsEqualFinanceSpent()) {
                            $this->clearRequierdSum($object);
                            $this->addFlash('sonata_flash_error', 'Показатели не изменились');
                        } else {
                            $object->setIsChanged(true);
                        }
                    } else {
                        $object->setIsChanged(false);
                    }
                    $object = $this->admin->update($object);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object),
                            'objectName' => $this->escapeHtml($this->admin->toString($object)),
                        ), 200, array());
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->trans(
                            'flash_edit_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    if ($request->get('btn_update_and_list') && $this->getUser()->hasRole('ROLE_NKO')) {
                        return new RedirectResponse($this->generateUrl('admin_nko_order_report_basereporttemplate_list'));
                    }

                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->trans('flash_lock_error', array(
                        '%name%' => $this->escapeHtml($this->admin->toString($object)),
                        '%link_start%' => '<a href="'.$this->admin->generateObjectUrl('edit', $object).'">',
                        '%link_end%' => '</a>',
                    ), 'SonataAdminBundle'));
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->trans(
                            'flash_edit_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        } else {
            if ($request->headers->get('referer') != $request->getUri()) {
                $this->logAction('Открытие показателей в редакторе', $object);
            }
        }

        $formView = $form->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $formView,
            'object' => $object,
        ), null);
    }

    protected function checkEqualSum($object, $sumGrant)
    {
        $requireSum = 0;
        $financeSpents = $object->getFinanceSpent();
        foreach ($financeSpents as $financeSpent) {
            if ($financeSpent->getExpenseType()->getCode() !== 'salary') {
                $requireSum += $financeSpent->getRequiredSum();
            }
        }
        return bccomp($requireSum, $sumGrant, 2) === 0;
    }

    private function checkIfEqual($object)
    {
        foreach ($object->getFinanceSpent() as $finance) {
            if ($finance->getApprovedSum() != $finance->getRequiredSum()) {
                return false;
            }
        }

        return true;
    }

    private function clearRequierdSum($object)
    {
        foreach ($object->getFinanceSpent() as $finance) {
            $finance->setRequiredSum(0);
        }
    }

    private function getExpenseTypes($reportForm)
    {
        $em = $this->getDoctrine()->getManager();
        $expenseTypes = $em->getRepository(ExpenseType::class)->findExpenseTypesByReportForm($reportForm);

        if (in_array($reportForm->getReportClass(), self::SALARY)) {
            $parentType = $this->getDoctrine()->getRepository(ExpenseType::class)->findOneBy([
                'code' => ExpenseTypeInterface::SALARY_CODE
            ]);
            array_unshift($expenseTypes, $parentType);
        }

        return $expenseTypes;
    }
}
