<?php

namespace NKO\OrderBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StatisticAdminController extends CRUDController
{
    public function listAction()
    {
        $resolverName = $this->getSimilarResolver();
        
        if (!$resolverName) {
            $resolverName = $this->getReflection()->getName();
        }

        $resolver = $this->get($resolverName);
        $resolver->getDataFromAdmin($this->admin);

        if ("POST" === $this->admin->getRequest()->getMethod()) {
            $accessDenied = $resolver->checkRequestData($this->getRequest());

            if (!$accessDenied) {
                $generator = $resolver->getExcelService();
                if ($generator) {
                    $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
                    $tableNumber = $this->admin->getTableNumberFromConstant();
                    $service->logAction('Выгрузка статистической таблицы', 'Таблица №'. ($tableNumber ? $tableNumber : 'Номер такблицы не указан'));

                    return $this->get($resolver->getExcelService())->generate($resolver->buildDataStructure($this->getRequest()));
                }
                $this->addFlash('sonata_flash_error', "Excel generator for this table haven't found");
            } else {
                $this->addFlash('sonata_flash_error', $accessDenied);
            }
        }

        return $this->render($this->admin->getTemplate('list'), $resolver->addServiceData($resolver->initForm()), null);
    }

    public function getAjaxDataAction(Request $request)
    {
        $resolver = $this->get((new \ReflectionClass($this->admin))->getName());
        $method = $request->get('method');

        if (method_exists($resolver, $method)) {
            return call_user_func([$resolver, $method], $request);
        }

        return new JsonResponse("Bad request", 400);
    }

    private function getSimilarResolver()
    {
        $reflection = $this->getReflection();
        return $reflection->hasConstant('RESOLVER') ? $reflection->getConstant('RESOLVER') : null;
    }

    private function getReflection()
    {
        return new \ReflectionClass($this->admin);
    }
}
