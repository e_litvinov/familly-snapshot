<?php

namespace NKO\OrderBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\PeriodReport;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ReportController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function getDocumentFolderTypeAction(Request $request)
    {
        $folderId = $request->getContent();

        $query = $this->getDoctrine()->getRepository('NKOOrderBundle:DocumentType')
            ->createQueryBuilder('p')
            ->where('p.folder = :id')
            ->setParameter('id', $folderId)
            ->getQuery();

        $documentTypes = $query->getResult();
        $types = [];
        foreach ($documentTypes as $documentType) {
            $types[$documentType->getId()] = $documentType->getDocumentType();
        }

        return new JsonResponse($types);
    }

    public function getReportPeriodsAction(Request $request)
    {
        $reportFormId = $request->getContent();

        $em = $this->getDoctrine()->getManager();
        $reportForm = $em->getRepository('NKOOrderBundle:Report\ReportForm')->find($reportFormId);
        $user = $this->getUser();
        $appHistory = $em->createQueryBuilder()
            ->select('ah')
            ->from('NKOOrderBundle:ApplicationHistory', 'ah')
            ->where('ah.author = :author')
            ->andWhere('ah.isSend = 1')
            ->andWhere('ah.competition = :competition')
            ->getQuery()
            ->setParameters([
                'author' => $user,
                'competition' => $reportForm->getCompetition()
            ])
            ->getResult();

        if (!$appHistory) {
            return new JsonResponse();
        }

        $periods = $reportForm->getReportPeriods();

        if ($periods->isEmpty()) {
            return new JsonResponse();
        }

        $reports = $em->createQueryBuilder()
            ->select('r')
            ->from('NKOOrderBundle:BaseReport', 'r')
            ->where('r.applicationHistory = :appHistory')
            ->getQuery()
            ->setParameters([
                'appHistory' => $appHistory
            ])
            ->getResult();

        if ($reports) {
            foreach ($reports as $report) {
                foreach ($periods as $period) {
                    if ($report->getPeriod()->getId() == $period->getId()) {
                        $periods->removeElement($period);
                    }
                }
            }
        }

        $values = [];
        foreach ($periods as $period) {
            $values[$period->getId()] = $period->getName();
        }

        return new JsonResponse($values);
    }
}
