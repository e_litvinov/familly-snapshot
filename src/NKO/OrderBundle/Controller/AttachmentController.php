<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.7.18
 * Time: 10.37
 */

namespace NKO\OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AttachmentController extends Controller
{
    public function attachmentsList($report, $parameters)
    {
        $service = $this->get('analytic_kns_2018_attachment_data_generator');
        $service->setData([
            'report' => $report,
           'parameters' => $parameters
        ]);

        return $service->generateData();
    }
}
