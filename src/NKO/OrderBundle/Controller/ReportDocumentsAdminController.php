<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/22/19
 * Time: 2:35 PM
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\ReportHistory;
use NKO\UserBundle\Entity\NKOUser;
use Sonata\AdminBundle\Controller\CRUDController;

class ReportDocumentsAdminController extends CRUDController
{
    public function listAction()
    {
        $request = $this->getRequest();

        $reportHistoryId = $request->get('report_history_id');
        $documents = null;
        if ($reportHistoryId) {
            $documents = $this->getHistoryDocuments($reportHistoryId);
        }

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate('list'), array(
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'documents' => $documents,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ), null);
    }

    public function getHistoryDocuments($historyId)
    {
        $history = $this->getDoctrine()->getRepository(ReportHistory::class)->find($historyId);
        $report = unserialize($history->getData());

        $author = $this->getDoctrine()->getRepository(NKOUser::class)->find($report->getAuthor()->getId());
        $report->setAuthor($author);

        return $report->getDocuments();
    }
}
