<?php

namespace NKO\OrderBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use NKO\OrderBundle\Entity\QuestionMark;
use NKO\OrderBundle\Entity\MarkCriteria;
use NKO\OrderBundle\Entity\MarkListSequence\Section;
use NKO\OrderBundle\Entity\MarkListSequence\SubSection;
use NKO\OrderBundle\Entity\MarkListSequence\Sequence;

class MarkListSequenceAdminController extends CRUDController
{
    public function createAction()
    {
        $this->admin->checkAccess('create');
        $object = $this->admin->getNewInstance();
        $this->admin->setSubject($object);
        $object = $this->admin->create($object);

        return $this->redirectTo($object);
    }

    public function editAction($id = null)
    {
        $request = $this->getRequest();
        $templateKey = 'edit';
        $id = $request->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id : %s', $id));
        }

        $this->admin->checkAccess('edit', $object);
        $this->admin->setSubject($object);

        $form = $this->admin->getForm();
        $form->setData($object);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $isFormValid = $form->isValid();
            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid) {
                try {
                    if (!$object->getIsFill()) {
                        $this->fillData($object);
                    }

                    $object = $this->admin->update($object);

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->trans(
                            'flash_edit_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);
                    $isFormValid = false;
                } catch (LockException $e) {
                    $this->addFlash('sonata_flash_error', $this->trans('flash_lock_error', array(
                        '%name%' => $this->escapeHtml($this->admin->toString($object)),
                        '%link_start%' => '<a href="'.$this->admin->generateObjectUrl('edit', $object).'">',
                        '%link_end%' => '</a>',
                    ), 'SonataAdminBundle'));
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->trans(
                            'flash_edit_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            }
        }

        $formView = $form->createView();
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $formView,
            'object' => $object,
        ), null);
    }

    public function fillData($object)
    {
        $em = $this->getDoctrine();
        $competitionId = $object->getCompetition()->getId();

        $markQuestion = $em->getRepository(QuestionMark::class)->findByCompetition($competitionId);
        $criterias =$em->getRepository(MarkCriteria::class)->findByCompetition($competitionId);

        foreach ($criterias as $criteria) {
            //заполнение поля criteria, где сортируются критерии
            $newObj = new Section();
            $newObj->setCriteria($criteria);
            $object->addSection($newObj);

            //заполнение поля criteriaWithQuestion, где сортируются вопросы
            $newObj = new Section();
            $newObj->setCriteria($criteria);
            $questions = $criteria->getQuestions();
            foreach ($questions as $question) {

                if (!in_array($question, $markQuestion)) {
                    continue;
                }

                $newQuest = new SubSection();
                $newQuest->setQuestion($question);
                $newObj->addSubSection($newQuest);
            }
            $object->addDoubleSection($newObj);
        }

        $object->setIsFill(true);
    }
}