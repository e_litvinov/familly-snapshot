<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 21.11.18
 * Time: 10.49
 */

namespace NKO\OrderBundle\Controller;

use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Utils\Analytic\SummaryMonitoring;
use NKO\OrderBundle\Utils\NameListGenerator\ApplicationNames;
use NKO\OrderBundle\Utils\Diagram\Diagrams;
use NKO\UserBundle\Entity\NKOUser;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as Farvater2016;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as Harbor2019;
use NKO\OrderBundle\Entity\Application as KNS2016;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017_1;

class DiagramAdminController extends CRUDController
{
    const INDICATOR_KEY = 'indicator';
    const COMPETITIONS_KEY = 'competitions';
    const WINNERS_KEY = 'winners';

    const TAMBOW_PSRN = 1026801118981;
    const PODSOLNUH_PSRN = 1097800001804;

    const PODSOLNUH_HARBOR2019_REPORT_FORM = 99;
    const TAMNOW_REPORT_FORM = 12;

    public function listAction()
    {
        $parameters = $this->admin->getRequest()->request->all();

        $em = $this->getDoctrine()->getManager();

        if (key_exists(self::INDICATOR_KEY, $parameters) && $this->admin->isGranted('LIST')) {
            set_time_limit(-1);
            $indicator = $parameters[self::INDICATOR_KEY];
            $competitions = ApplicationNames::parse(ApplicationNames::MONITORING_APPLICATIONS);

            $applications = [];
            $organizations = [];

            if (in_array("ROLE_NKO", $this->getUser()->getRoles()) &&  key_exists(self::COMPETITIONS_KEY, $parameters)) {
                $applications = ApplicationNames::getArrayOfCompetitionsBy($competitions[$parameters[self::COMPETITIONS_KEY]]);
                $organizations[]['psrn']  = $this->getUser()->getPsrn();
            } elseif ($this->admin->getRequest()->request->count() == 3 &&
                key_exists(self::WINNERS_KEY, $parameters) &&
                key_exists(self::COMPETITIONS_KEY, $parameters)) {
                $competitionLabel = $parameters[self::COMPETITIONS_KEY];
                $organizations = $parameters[self::WINNERS_KEY];

                $applications = ApplicationNames::getArrayOfCompetitionsBy($competitions[$competitionLabel]);

                if ($organizations == -1) {
                    $organizations = $em->getRepository(NKOUser::class)
                        ->findWinnersOfCompetitions($applications, Report::class);
                } else {
                    $psrn = $em->getRepository(NKOUser::class)->find($organizations)->getPsrn();
                    $organizations = [];
                    $organizations[]['psrn'] = $psrn;
                }
            }

            $reportHistories = $em->getRepository(ReportHistory::class)
                ->findByAuthorsAndReportFormsSendReportHistories(
                    $applications,
                    array_column($organizations, 'psrn'),
                    Report::class
                );
            if (false !== array_search(Harbor2019::class, $applications)) {
                $reportHistoriesAdditional = $em->getRepository(ReportHistory::class)
                    ->findByPsrnAndReportForm(
                        self::PODSOLNUH_PSRN,
                        self::PODSOLNUH_HARBOR2019_REPORT_FORM
                    );

                $reportHistoriesAdditional = array_filter($reportHistoriesAdditional, function (ReportHistory $reportHistory) use ($reportHistories) {
                    foreach ($reportHistories as $history) {
                        if ($history->getId() == $reportHistory->getId()) {
                            return false;
                        }
                    }

                    return true;
                });

                if ($reportHistoriesAdditional) {
                    $reportHistories += $reportHistoriesAdditional;
                }
            } elseif (false !== array_search(Farvater2016::class, $applications)) {
                $reportHistoriesAdditional = $em->getRepository(ReportHistory::class)
                    ->findByPsrnAndReportForm(
                        self::PODSOLNUH_PSRN,
                        self::PODSOLNUH_HARBOR2019_REPORT_FORM
                    );

                $reportHistories = array_filter($reportHistories, function (ReportHistory $reportHistory) use ($reportHistoriesAdditional) {
                    foreach ($reportHistoriesAdditional as $history) {
                        if ($history->getId() == $reportHistory->getId()) {
                            return false;
                        }
                    }
                    return true;
                });
            }

            if (false !== array_search(KNS2017_1::class, $applications)) {
                $reportHistoriesAdditional = $em->getRepository(ReportHistory::class)
                    ->findByPsrnAndReportForm(
                        self::TAMBOW_PSRN,
                        self::TAMNOW_REPORT_FORM
                    );

                $reportHistoriesAdditional = array_filter($reportHistoriesAdditional, function (ReportHistory $reportHistory) use ($reportHistories) {
                    foreach ($reportHistories as $history) {
                        if ($history->getId() == $reportHistory->getId()) {
                            return false;
                        }
                    }

                    return true;
                });

                if ($reportHistoriesAdditional) {
                    $reportHistories += $reportHistoriesAdditional;
                }
            } elseif (false !== array_search(KNS2016::class, $applications)) {
                $reportHistoriesAdditional = $em->getRepository(ReportHistory::class)
                    ->findByPsrnAndReportForm(
                        self::TAMBOW_PSRN,
                        self::TAMNOW_REPORT_FORM
                    );

                $reportHistories = array_filter($reportHistories, function (ReportHistory $reportHistory) use ($reportHistoriesAdditional) {
                    foreach ($reportHistoriesAdditional as $history) {
                        if ($history->getId() == $reportHistory->getId()) {
                            return false;
                        }
                    }
                    return true;
                });
            }

            $factValues = $this->getFactValuesFromHistories($reportHistories, [$indicator]);
            $compactFactValues = $this->compactFactValue($factValues);
            $results = $this->generateAssociativeArray($compactFactValues);
            $results = $this->removeNullableResults($results, $isNullableExist);

            if (!count($results) || $isNullableExist) {
                $this->addFlash('sonata_flash_error', 'some_organizations_have_no_results');
            }

            if (count($results)) {
                $service = $this->get('NKO\LoggerBundle\Loggers\BaseLogger');
                $indicatorName = $em->getRepository(Indicator::class)->find($indicator)->getTitle();
                $service->logAction('Выгрузка диаграмм', 'Показатель: '. $indicatorName);

                return $this->get('nko_order.diagram.excel')->generate(['results' => $results, 'types' => Diagrams::TYPES]);
            }
        }

        $this->setParameters($em, $winners, $competitions, $indicatorsArray);

        return $this->render($this->admin->getTemplate('list'), [
            'indicator' => $indicatorsArray,
            'competitions' => $competitions,
            'winners' => $winners,
        ]);
    }

    public function getWinnersAction(Request $request)
    {
        $competition = $request->query->get('competition');
        $em = $this->getDoctrine()->getManager();

        if ($request->query->count() == 1 && $competition) {
            $competitions = ApplicationNames::parse(ApplicationNames::MONITORING_APPLICATIONS);

            $organizations = $this->takeOrganizations($competitions, $competition);
            $competitionsArray = ApplicationNames::getArrayOfCompetitionsBy($competitions[$competition]);

            if (false !== array_search(Harbor2019::class, $competitionsArray)) {
                $reportHistories = $em->getRepository(ReportHistory::class)
                    ->findByPsrnAndReportForm(
                        self::PODSOLNUH_PSRN,
                        self::PODSOLNUH_HARBOR2019_REPORT_FORM
                    );

                if ($reportHistories) {
                    $psrn = $reportHistories[0]->getPsrn();
                    $organizationsAdditional = $this->getDoctrine()->getManager()->getRepository(NKOUser::class)
                        ->findWinnersOfCompetitionsByPSRN($psrn);
                    $organizations = array_merge($organizations, array_column($organizationsAdditional, 'id', 'name'));
                }
            }
            if (false !== array_search(KNS2017_1::class, $competitionsArray)) {
                $reportHistories = $em->getRepository(ReportHistory::class)
                    ->findByPsrnAndReportForm(
                        self::TAMBOW_PSRN,
                        self::TAMNOW_REPORT_FORM
                    );

                if ($reportHistories) {
                    $psrn = $reportHistories[0]->getPsrn();
                    $organizationsAdditional = $this->getDoctrine()->getManager()->getRepository(NKOUser::class)
                        ->findWinnersOfCompetitionsByPSRN($psrn);
                    $organizations = array_merge($organizations, array_column($organizationsAdditional, 'id', 'name'));
                }
            }

            return new JsonResponse($organizations, 200);
        }

        return new JsonResponse("Bad request", 400);
    }

    private function takeFirstKey($arr)
    {
        $temp = null;
        foreach ($arr as $key => $item) {
            $temp = $key;
            break;
        }

        return $temp;
    }

    private function takeOrganizations($competitions, $competition)
    {
        $applicationKey = $competitions[$competition];
        $application = ApplicationNames::getArrayOfCompetitionsBy($applicationKey);

        $organizations = $this->getDoctrine()->getManager()->getRepository(NKOUser::class)
            ->findWinnersOfCompetitions($application, Report::class);

        $winners['Все'] = -1;
        $winners = array_merge($winners, array_column($organizations, 'id', 'name'));

        return $winners;
    }

    private function generateAssociativeArray($array)
    {
        $results = [];
        $titles = [];

        foreach ($array as $item) {
            $title = $item['title'];
            $year = $item['year']->format('Y');
            $fact = $item['fact'];

            $result = null;
            $key = null;

            foreach ($results as $index => $temp) {
                if ($temp['title'] === $title) {
                    $result = $temp;
                    $key = $index;
                    break;
                }
                $result = null;
            }

            if (!$result) {
                $result['title'] = $title;
                $result['values'] = [];

                $key = count($results);

                $titles[] = $title;
                $results[] = $result;
            }

            $results[$key]['values'][$year] = $fact;
        }

        return $results;
    }

    private function removeNullableResults($results, &$nullableExist = null)
    {
        $newResults = [];
        foreach ($results as $result) {
            $fact = 0;
            foreach ($result['values'] as $value) {
                $fact += $value;
            }

            if ($fact) {
                $newResults[] = $result;
            } else {
                $nullableExist = true;
            }
        }

        return $newResults;
    }

    private function setParameters($em, &$winners, &$competitions, &$indicatorsArray)
    {
        if (in_array("ROLE_NKO", $this->getUser()->getRoles())) {
            return $this->getUserParameters($em, $winners, $competitions, $indicatorsArray);
        }

        return $this->getParameters($em, $winners, $competitions, $indicatorsArray);
    }

    private function getParameters($em, &$winners, &$competitions, &$indicatorsArray)
    {
        $indicators = $em->getRepository(Indicator::class)->findBYType(SummaryMonitoring::SECOND_TYPE);
        $indicatorsArray = [];
        foreach ($indicators as $item) {
            $indicatorsArray[$item->getTitle()] = $item->getId();
        }

        $competitions = ApplicationNames::parse(ApplicationNames::MONITORING_APPLICATIONS);
        $competition = $this->takeFirstKey($competitions);

        $winners = $this->takeOrganizations($competitions, $competition);
    }

    private function getUserParameters($em, &$winners, &$competitions, &$indicatorsArray)
    {
        $indicators = $em->getRepository(Indicator::class)->findBYType(SummaryMonitoring::SECOND_TYPE);
        $indicatorsArray = [];
        foreach ($indicators as $item) {
            $indicatorsArray[$item->getTitle()] = $item->getId();
        }

        $competitions = ApplicationNames::parse(ApplicationNames::MONITORING_APPLICATIONS);

        $winners = [];
    }

    private function getFactValuesFromHistories(array $reportHistories, array $indicators)
    {
        $results = $this->getMonitoringResults($reportHistories, $indicators);
        $returned = [];
        foreach ($results as $result) {
            $returned[] = $this->generateStructedDataFromPeriodResult($result->first());
        }

        return $returned;
    }

    private function getMonitoringResults(array $reportHistories, array $indicators)
    {
        $results = [];

        foreach ($reportHistories as $history) {
            /** @var Report $report */
            $report = unserialize($history->getData());
            $results[] = $report->getMonitoringResultsByIndicatorsId($indicators);
        }

        return $results;
    }

    private function generateStructedDataFromPeriodResult(MonitoringResult $monitoringResult = null)
    {
        $returned = [];

        $returned['fact'] = $monitoringResult->getFinalPeriodResult()->first()->getNewAmount();
        $returned['title'] = $monitoringResult->getIndicator()->getTitle();
        $returned['year'] = $monitoringResult->getReport()->getReportForm()->getYear();

        return $returned;
    }

    private function compactFactValue(array $factValues)
    {
        $years = [];
        $returned = [];

        foreach ($factValues as $values) {
            if (key_exists($values['year']->format('Y'), $years)) {
                $returned[$years[$values['year']->format('Y')]]['fact'] += $values['fact'];
            } else {
                $key = $values['year']->format('Y') - 2016;
                $returned[$key]['fact'] = $values['fact'];
                $returned[$key]['title'] = $values['title'];
                $returned[$key]['year'] = $values['year'];

                $years[$values['year']->format('Y')] = $key;
            }
        }

        ksort($returned);

        return $returned;
    }
}
