<?php

namespace NKO\OrderBundle\HistoryManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\MarkListHistory;

class MarkListHistoryManager
{
    /** @var EntityManager */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function removeByMarkList(MarkList $markList)
    {
        $histories = $this->em->getRepository(MarkListHistory::class)->findByMarkList($markList);
        foreach ($histories as $history) {
            $this->em->remove($history);
        }
    }
}
