<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\AutosaveTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * MarkList
 *
 * @ORM\Table(name="mark_list")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\MarkListRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MarkList
{
    use AutosaveTrait;

    const APPLY = 0;
    const APPLY_WITH_COMPLETION = 1;
    const NOT_APPLY = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @Assert\Valid
     *@ORM\OneToMany(targetEntity="Mark", mappedBy="markList", cascade={"all"})
     */
    private $marks;

    /**
     * @var string
     *
     * @ORM\Column(name="rationale_final_decision", type="text", nullable=true)
     * @Assert\Length(
     *      min = 50,
     *      minMessage = "")
     * @Assert\NotNull(message = " ")
     */
    private $rationaleFinalDecision;

    /**
     * @var int
     *
     * @ORM\Column(name="final_mark", type="integer", nullable=true)
     */
    private $finalMark;

    /**
     * @ORM\ManyToOne(targetEntity="ApplicationHistory", inversedBy="markLists")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $application;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\ExpertUser", inversedBy="markLists")
     * @ORM\JoinColumn(name="mark_expert_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    private $expert;

    /**
     * @var int
     *
     * @ORM\Column(name="application_id", type="integer", nullable=true)
     */
    private $application_id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_valid", type="boolean", nullable=true)
     */
    private $isValid;

    /**
     * @Assert\NotNull(message = "mark.final_decision")
     * @ORM\ManyToOne(targetEntity="FinalDecision", inversedBy="markLists")
     * @ORM\JoinColumn(name="final_decision_id", referencedColumnName="id", onDelete="cascade")
     */
    private $finalDecision;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $markWithCoeff;

    private $coefficient;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSend;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isFirstTimeSend;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $help;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function getCoefficient()
    {
        return $this->coefficient;
    }

    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marks = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->application->getNkoName();
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add mark
     *
     * @param \NKO\OrderBundle\Entity\Mark $mark
     *
     * @return MarkList
     */
    public function addMark(\NKO\OrderBundle\Entity\Mark $mark)
    {
        $mark->setMarkList($this);
        $this->marks[] = $mark;


        return $this;
    }

    /**
     * Remove mark
     *
     * @param \NKO\OrderBundle\Entity\Mark $mark
     */
    public function removeMark(\NKO\OrderBundle\Entity\Mark $mark)
    {
        $this->marks->removeElement($mark);
    }

    /**
     * Get marks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarks()
    {
        return $this->marks;
    }

    public function setMarks($marks)
    {
        $this->marks = new ArrayCollection();
        foreach ($marks as $mark) {
            $this->addMark($mark);
        }

        return $this;
    }

    /**
     * Set rationaleFinalDecision
     *
     * @param string $rationaleFinalDecision
     *
     * @return MarkList
     */
    public function setRationaleFinalDecision($rationaleFinalDecision)
    {
        $this->rationaleFinalDecision = $rationaleFinalDecision;

        return $this;
    }

    /**
     * Get rationaleFinalDecision
     *
     * @return string
     */
    public function getRationaleFinalDecision()
    {
        return $this->rationaleFinalDecision;
    }

    /**
     * Set finalMark
     *
     * @return MarkList
     */
    public function setFinalMark($finalMark)
    {
        $this->finalMark = $finalMark;

        return $this;
    }



    /**
     * Get finalMark
     *
     * @return integer
     */
    public function getFinalMark()
    {
        return $this->finalMark;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $application
     *
     * @return MarkList
     */
    public function setApplication(\NKO\OrderBundle\Entity\ApplicationHistory $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\ApplicationHistory
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     *
     * @return MarkList
     */
    public function setExpert(\NKO\UserBundle\Entity\ExpertUser $expert = null)
    {
        $this->expert = $expert;

        return $this;
    }

    /**
     * Get expert
     *
     * @return \NKO\UserBundle\Entity\ExpertUser
     */
    public function getExpert()
    {
        return $this->expert;
    }

    /**
     * Set applicationId
     *
     * @param integer $applicationId
     *
     * @return MarkList
     */
    public function setApplicationId($applicationId)
    {
        $this->application_id = $applicationId;

        return $this;
    }

    /**
     * Get applicationId
     *
     * @return integer
     */
    public function getApplicationId()
    {
        return $this->application_id;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return MarkList
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->isValid;
    }




    /**
     * Set markWithCoeff
     *
     * @param float $markWithCoeff
     *
     * @return MarkList
     */
    public function setMarkWithCoeff($markWithCoeff)
    {
        $this->markWithCoeff = $markWithCoeff;

        return $this;
    }

    /**
     * Get markWithCoeff
     *
     * @return float
     */
    public function getMarkWithCoeff()
    {
        return $this->markWithCoeff;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return MarkList
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set finalDecision
     *
     * @param \NKO\OrderBundle\Entity\FinalDecision $finalDecision
     *
     * @return MarkList
     */
    public function setFinalDecision(\NKO\OrderBundle\Entity\FinalDecision $finalDecision = null)
    {
        $this->finalDecision = $finalDecision;

        return $this;
    }

    /**
     * Get finalDecision
     *
     * @return \NKO\OrderBundle\Entity\FinalDecision
     */
    public function getFinalDecision()
    {
        return $this->finalDecision;
    }

    /**
     * Set isFirstTimeSend
     *
     * @param boolean $isFirstTimeSend
     *
     * @return MarkList
     */
    public function setIsFirstTimeSend($isFirstTimeSend)
    {
        $this->isFirstTimeSend = $isFirstTimeSend;

        return $this;
    }

    /**
     * Get isFirstTimeSend
     *
     * @return boolean
     */
    public function getIsFirstTimeSend()
    {
        return $this->isFirstTimeSend;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MarkList
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return string
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * @param string $help
     *
     * @return MarkList
     */
    public function setHelp($help)
    {
        $this->help = $help;

        return $this;
    }
}
