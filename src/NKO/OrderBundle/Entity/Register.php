<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.10.16
 * Time: 10:32
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Costs
 *
 * @ORM\Table(name="register")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Register
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ExpenseType", inversedBy="registers")
     * @ORM\JoinColumn(name="expense_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $expenseType;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Expense", mappedBy="register", cascade={"all"})
     */
    private $expenses;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $report;

    /**
     * @ORM\Column(name="total_expense_value", type="float", nullable=true)
     */
    private $totalExpenseValue;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->expenses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     *
     * @return Register
     */
    public function setExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType = null)
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    /**
     * Get expenseType
     *
     * @return \NKO\OrderBundle\Entity\ExpenseType
     */
    public function getExpenseType()
    {
        return $this->expenseType;
    }

    /**
     * Add expense
     *
     * @param \NKO\OrderBundle\Entity\Expense $expense
     *
     * @return Register
     */
    public function addExpense(\NKO\OrderBundle\Entity\Expense $expense)
    {
        $expense->setRegister($this);
        $this->expenses[] = $expense;

        return $this;
    }

    /**
     * Remove expense
     *
     * @param \NKO\OrderBundle\Entity\Expense $expense
     */
    public function removeExpense(\NKO\OrderBundle\Entity\Expense $expense)
    {
        $this->expenses->removeElement($expense);
        $expense->setRegister(null);
    }

    /**
     * Get expenses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenses()
    {
        return $this->expenses;
    }

    /**
     * Set totalExpenseValue
     *
     * @param float $totalExpenseValue
     *
     * @return Register
     */
    public function setTotalExpenseValue($totalExpenseValue)
    {
        $this->totalExpenseValue = $totalExpenseValue;

        return $this;
    }

    /**
     * Get totalExpenseValue
     *
     * @return float
     */
    public function getTotalExpenseValue()
    {
        return $this->totalExpenseValue;
    }

    /**
     * @param BaseReport|null $report
     * @return $this
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Register
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
