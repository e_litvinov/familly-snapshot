<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Utils\ApplicationResolver;
use NKO\OrderBundle\Validator\Constraints as CompetitionAssert;
use NKO\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Competition
 * @ORM\Entity
 * @ORM\Table(name="competition")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\CompetitionRepository")
 * @CompetitionAssert\CompetitionDateRange
 */

class Competition
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @var string
    * @ORM\Column(name="competition_name", type="text")
    */
    private $name;

    /**
     * @var \DateTime $start_date
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @var \DateTime $finish_date
     * @ORM\Column(type="datetime")
     */
    private $finish_date;

    /**
     * false - (organizations == only them)
     * true - (organizations == all except them)
     *
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $organizationsAccess;

    /**
     * @ORM\OneToMany(targetEntity="BaseApplication", mappedBy="competition")
     */
    private $applications;

    /**
     * @var string
     * @ORM\Column(name="application_class", type="string")
     */
    private $applicationClass;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\MarkCriteria", mappedBy="competitions")
     */
    private $marksCriteria;

    /**
     * @ORM\ManyToMany(targetEntity="FinalDecision", inversedBy="competitions", cascade={"all"})
     * @ORM\JoinTable(name="competition_final_decision")
     */
    private $finalDecisions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\UserBundle\Entity\ExpertUser", mappedBy="competitions")
     */
    private $experts;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\UserBundle\Entity\NKOUser", mappedBy="competitions")
     */
    private $organizations;

    /**
     * @var \DateTime
     * @Assert\NotNull()
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishEstimate;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $helpForRationaleFinalDecision;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", mappedBy="competition")
     */
    private $reportForms;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm")
     * @ORM\JoinColumn(name="related_report_form_id", referencedColumnName="id", nullable=true)
     */
    private $relatedReportForm;
    
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $agreement;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $realizationYear;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNeedContacts;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false, options={"default" : true})
     */
    private $isMarkListReset;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEditableByWinner;

    /**
     * @ORM\ManyToOne(targetEntity="Competition")
     * @ORM\JoinColumn(name="related_competition_id", referencedColumnName="id", nullable=true)
     */
    private $relatedCompetition;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", mappedBy="notAllowedCompetitions", cascade={"all"})
     */
    private $bannedReportForms;

    /**
     * @ORM\Column(type="integer", options={"default" : 3})
     */
    private $expertNumber;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applications = new \Doctrine\Common\Collections\ArrayCollection();
        $this->finalDecisions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->organizations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->expenseTypes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->marksCriteria = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getLink()
    {
        if (array_key_exists($this->getApplicationClass(), ApplicationResolver::APPLICATION_CREATE_URLS)) {
            return ApplicationResolver::APPLICATION_CREATE_URLS[$this->getApplicationClass()];
        }

        return null;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Competition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Competition
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set finishDate
     *
     * @param \DateTime $finishDate
     *
     * @return Competition
     */
    public function setFinishDate($finishDate)
    {
        $this->finish_date = $finishDate;

        return $this;
    }

    /**
     * Get finishDate
     *
     * @return \DateTime
     */
    public function getFinishDate()
    {
        return $this->finish_date;
    }

    /**
     * Add application
     *
     * @param \NKO\OrderBundle\Entity\Application $application
     *
     * @return Competition
     */
    public function addApplication(\NKO\OrderBundle\Entity\BaseApplication $application)
    {
        $this->applications[] = $application;

        return $this;
    }

    /**
     * Remove application
     *
     * @param \NKO\OrderBundle\Entity\Application $application
     */
    public function removeApplication(\NKO\OrderBundle\Entity\BaseApplication $application)
    {
        $this->applications->removeElement($application);
    }

    /**
     * Get applications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Set applicationClass
     *
     * @param string $applicationClass
     *
     * @return Competition
     */
    public function setApplicationClass($applicationClass)
    {
        $this->applicationClass = $applicationClass;

        return $this;
    }

    /**
     * Get applicationClass
     *
     * @return string
     */
    public function getApplicationClass()
    {
        return $this->applicationClass;
    }
    

    /**
     * Add marksCriterium
     *
     * @param \NKO\OrderBundle\Entity\MarkCriteria $marksCriterium
     *
     * @return Competition
     */
    public function addMarksCriterium(\NKO\OrderBundle\Entity\MarkCriteria $marksCriterium)
    {
        $this->marksCriteria[] = $marksCriterium;

        return $this;
    }

    /**
     * Remove marksCriterium
     *
     * @param \NKO\OrderBundle\Entity\MarkCriteria $marksCriterium
     */
    public function removeMarksCriterium(\NKO\OrderBundle\Entity\MarkCriteria $marksCriterium)
    {
        $this->marksCriteria->removeElement($marksCriterium);
    }

    /**
     * Get marksCriteria
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarksCriteria()
    {
        return $this->marksCriteria;
    }

    /**
     * Add finalDecision
     *
     * @param \NKO\OrderBundle\Entity\FinalDecision $finalDecision
     *
     * @return Competition
     */
    public function addFinalDecision(\NKO\OrderBundle\Entity\FinalDecision $finalDecision)
    {
        $this->finalDecisions[] = $finalDecision;

        return $this;
    }

    /**
     * Remove finalDecision
     *
     * @param \NKO\OrderBundle\Entity\FinalDecision $finalDecision
     */
    public function removeFinalDecision(\NKO\OrderBundle\Entity\FinalDecision $finalDecision)
    {
        $this->finalDecisions->removeElement($finalDecision);
    }

    /**
     * Get finalDecisions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinalDecisions()
    {
        return $this->finalDecisions;
    }

    /**
     * Add expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     *
     * @return Competition
     */
    public function addExpert(\NKO\UserBundle\Entity\ExpertUser $expert)
    {
        $this->experts[] = $expert;

        return $this;
    }

    /**
     * Remove expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     */
    public function removeExpert(\NKO\UserBundle\Entity\ExpertUser $expert)
    {
        $this->experts->removeElement($expert);
    }

    /**
     * Get experts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperts()
    {
        return $this->experts;
    }

    /**
     * Set finishEstimate
     *
     * @param \DateTime $finishEstimate
     *
     * @return Competition
     */
    public function setFinishEstimate($finishEstimate)
    {
        $this->finishEstimate = $finishEstimate;

        return $this;
    }

    /**
     * Get finishEstimate
     *
     * @return \DateTime
     */
    public function getFinishEstimate()
    {
        return $this->finishEstimate;
    }

    /**
     * Add organization
     *
     * @param \NKO\UserBundle\Entity\NKOUser $organization
     *
     * @return Competition
     */
    public function addOrganization(\NKO\UserBundle\Entity\NKOUser $organization)
    {
        $this->organizations[] = $organization;
        $organization->addCompetition($this);

        return $this;
    }

    /**
     * Remove organization
     *
     * @param \NKO\UserBundle\Entity\NKOUser $organization
     */
    public function removeOrganization(\NKO\UserBundle\Entity\NKOUser $organization)
    {
        $this->organizations->removeElement($organization);
        $organization->removeCompetition($this);
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizations()
    {
        return $this->organizations;
    }

    /**
     * Set helpForRationaleFinalDecision
     *
     * @param string $helpForRationaleFinalDecision
     *
     * @return Competition
     */
    public function setHelpForRationaleFinalDecision($helpForRationaleFinalDecision)
    {
        $this->helpForRationaleFinalDecision = $helpForRationaleFinalDecision;

        return $this;
    }

    /**
     * Get helpForRationaleFinalDecision
     *
     * @return string
     */
    public function getHelpForRationaleFinalDecision()
    {
        return $this->helpForRationaleFinalDecision;
    }

    /**
     * @param $competition Competition
     *
     * @return bool
     */
    public function getIsCurrentCompetition()
    {
        return ($this->getStartDate() < new \DateTime() && $this->getFinishDate() > new \DateTime());
    }

    /**
     * Add reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return Competition
     */
    public function addReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms[] = $reportForm;

        return $this;
    }

    /**
     * Remove reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     */
    public function removeReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms->removeElement($reportForm);
    }

    /**
     * Get reportForms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportForms()
    {
        return $this->reportForms;
    }



    /**
     * Set relatedReportForm
     *
     * @param ReportForm $reportForm
     *
     * @return Competition
     */
    public function setRelatedReportForm(ReportForm $reportForm = null)
    {
        $this->relatedReportForm = $reportForm;

        return $this;
    }

    /**
     * Get relatedReportForm
     *
     * @return ReportForm
     */
    public function getRelatedReportForm()
    {
        return $this->relatedReportForm;
    }

    /**
     * Add expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     *
     * @return Competition
     */
    public function addExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType)
    {
        $this->expenseTypes[] = $expenseType;

        return $this;
    }

    /**
     * Remove expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     */
    public function removeExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType)
    {
        $this->expenseTypes->removeElement($expenseType);
    }

    /**
     * Get expenseTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenseTypes()
    {
        return $this->expenseTypes;
    }

    /**
     * Set agreement
     *
     * @param string $agreement
     *
     * @return Competition
     */
    public function setAgreement($agreement)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement
     *
     * @return string
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * Set realizationYear
     *
     * @param \DateTime $realizationYear
     *
     * @return Competition
     */
    public function setRealizationYear($realizationYear)
    {
        $this->realizationYear = $realizationYear;

        return $this;
    }

    /**
     * Get realizationYear
     *
     * @return \DateTime
     */
    public function getRealizationYear()
    {
        return $this->realizationYear;
    }

    /**
     * Set relatedCompetition
     *
     * @param \NKO\OrderBundle\Entity\Competition $relatedCompetition
     *
     * @return Competition
     */
    public function setRelatedCompetition(\NKO\OrderBundle\Entity\Competition $relatedCompetition = null)
    {
        $this->relatedCompetition = $relatedCompetition;

        return $this;
    }

    /**
     * Get relatedCompetition
     *
     * @return \NKO\OrderBundle\Entity\Competition
     */
    public function getRelatedCompetition()
    {
        return $this->relatedCompetition;
    }

    /**
     * false - (organizations == only them)
     * true - (organizations == all except them)
     *
     * @return bool
     */
    public function isOrganizationsAccess()
    {
        return $this->organizationsAccess;
    }

    /**
     * @param bool $organizationsAccess
     *
     * @return Competition
     */
    public function setOrganizationsAccess($organizationsAccess)
    {
        $this->organizationsAccess = $organizationsAccess;

        return $this;
    }

    /**
     * @return bool
     */
    public function isNeedContacts()
    {
        return $this->isNeedContacts;
    }

    /**
     * @param bool $isNeedContacts
     *
     * @return Competition
     */
    public function setIsNeedContacts($isNeedContacts)
    {
        $this->isNeedContacts = $isNeedContacts;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditableByWinner()
    {
        return $this->isEditableByWinner;
    }

    /**
     * @param bool $isEditableByWinner
     *
     * @return Competition
     */
    public function setIsEditableByWinner($isEditableByWinner)
    {
        $this->isEditableByWinner = $isEditableByWinner;

        return $this;
    }

    public function isAllowedToUSer(User $user)
    {
        return $this->organizations->isEmpty()
            || (
                ($this->isOrganizationsAccess() != true) && $this->getOrganizations()->contains($user))
                || ($this->isOrganizationsAccess() && !$this->getOrganizations()->contains($user)
            );
    }

    /**
     * Add bannedReportFrom
     *
     * @param ReportForm $reportForm
     *
     * @return Competition
     */
    public function addBannedReportForm(ReportForm $reportForm)
    {
        $this->bannedReportForms[] = $reportForm;
        $reportForm->addNotAllowedCompetitions($this);

        return $this;
    }

    /**
     * Remove organization
     *
     * @param ReportForm $reportForm
     */
    public function removeBannedReportForm(ReportForm $reportForm)
    {
        $this->bannedReportForms->removeElement($reportForm);
        $reportForm->removeNotAllowedCompetitions($this);
    }

    /**
     * Get bannedReportForms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBannedReportForms()
    {
        return $this->bannedReportForms;
    }

    /**
     * @return bool
     */
    public function getIsMarkListReset()
    {
        return $this->isMarkListReset;
    }

    /**
     * @param bool $isMarkListReset
     *
     * @return Competition
     */
    public function setIsMarkListReset($isMarkListReset)
    {
        $this->isMarkListReset = $isMarkListReset;

        return $this;
    }

    public function getExpertNumber()
    {
        return $this->expertNumber;
    }

    public function setExpertNumber($expertNumber)
    {
        $this->expertNumber = $expertNumber;

        return $this;
    }
}
