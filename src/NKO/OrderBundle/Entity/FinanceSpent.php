<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 19.10.16
 * Time: 13:36
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="finance_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "finance_spent" = "FinanceSpent",
 *     "finance_report_finance_spent" = "NKO\OrderBundle\Entity\Report\FinanceReport\FinanceSpent"
 * })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class FinanceSpent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cost_item", type="text", nullable=true)
     */
    private $costItem;

    /**
     * @var float
     * @ORM\Column(name="approved_sum", type="float", nullable=true)
     */
    private $approvedSum;

    /**
     * @var float
     *
     * @ORM\Column(name="period_costs", type="float", nullable=true)
     */
    private $periodCosts;

    /**
     * @var float
     *
     * @ORM\Column(name="balance", type="float", nullable=true)
     */
    private $balance;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ExpenseType", inversedBy="financeSpent")
     * @ORM\JoinColumn(name="expense_type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $expenseType;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $report;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\BaseReportTemplate")
     * @ORM\JoinColumn(name="report_template_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $reportTemplate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    function __toString()
    {
        return (string) $this->approvedSum;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set costItem
     *
     * @param string $costItem
     *
     * @return FinanceSpent
     */
    public function setCostItem($costItem)
    {
        $this->costItem = $costItem;

        return $this;
    }

    /**
     * Get costItem
     *
     * @return string
     */
    public function getCostItem()
    {
        return $this->costItem;
    }

    /**
     * Set approvedSum
     *
     * @param float $approvedSum
     *
     * @return FinanceSpent
     */
    public function setApprovedSum($approvedSum)
    {
        $this->approvedSum = $approvedSum;

        return $this;
    }

    /**
     * Get approvedSum
     *
     * @return float
     */
    public function getApprovedSum()
    {
        return $this->approvedSum;
    }

    /**
     * Set periodCosts
     *
     * @param float $periodCosts
     *
     * @return FinanceSpent
     */
    public function setPeriodCosts($periodCosts)
    {
        $this->periodCosts = $periodCosts;

        return $this;
    }

    /**
     * Get periodCosts
     *
     * @return float
     */
    public function getPeriodCosts()
    {
        return $this->periodCosts;
    }

    /**
     * Set balance
     *
     * @param float $balance
     *
     * @return FinanceSpent
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param BaseReport $report
     * @return $this
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     *
     * @return FinanceSpent
     */
    public function setExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType)
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    /**
     * Get expenseType
     *
     * @return \NKO\OrderBundle\Entity\ExpenseType
     */
    public function getExpenseType()
    {
        return $this->expenseType;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FinanceSpent
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate $reportTemplate
     *
     * @return FinanceSpent
     */
    public function setReportTemplate(\NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate $reportTemplate = null)
    {
        $this->reportTemplate = $reportTemplate;

        return $this;
    }

    /**
     * Get reportTemplate
     *
     * @return \NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate
     */
    public function getReportTemplate()
    {
        return $this->reportTemplate;
    }
}
