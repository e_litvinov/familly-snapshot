<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ProjectResult
 *
 * @ORM\Table(name="project_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ProjectResultRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ProjectResult
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="target_value_on_12_25_2016", type="integer", nullable=true)
     */
    private $targetValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="approximate_target_value_on_01_06_2017", type="integer", nullable=true)
     */
    private $approximateTargetValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="method_measurement", type="text", nullable=true)
     */
    private $methodMeasurement;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018"}
     *     )
     *
     * @ORM\Column(name="result_criteria", type="text", nullable=true)
     *
     */
    private $resultCriteria;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="projectResults")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $application;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication",
     *     cascade={"persist"}, inversedBy="projectResults")
     * @ORM\JoinColumn(name="kns2017_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $KNS2017Application;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinColumn(name="measurement_method_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $linkedMethod;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Application application
     *
     * @return ProjectResult
     */
    public function setApplication(Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get $application
     *
     * @return \NKO\OrderBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set targetValue
     *
     * @param integer $targetValue
     *
     * @return ProjectResult
     */
    public function setTargetValue($targetValue)
    {
        $this->targetValue = $targetValue;

        return $this;
    }

    /**
     * Get targetValue
     *
     * @return int
     */
    public function getTargetValue()
    {
        return $this->targetValue;
    }

    /**
     * Set approximateTargetValues
     *
     * @param integer $approximateTargetValue
     *
     * @return ProjectResult
     */
    public function setApproximateTargetValue($approximateTargetValue)
    {
        $this->approximateTargetValue = $approximateTargetValue;

        return $this;
    }

    /**
     * Get approximateTargetValue
     *
     * @return int
     */
    public function getApproximateTargetValue()
    {
        return $this->approximateTargetValue;
    }

    /**
     * Set methodMeasurement
     *
     * @param string $methodMeasurement
     *
     * @return ProjectResult
     */
    public function setMethodMeasurement($methodMeasurement)
    {
        $this->methodMeasurement = $methodMeasurement;

        return $this;
    }

    /**
     * Get methodMeasurement
     *
     * @return string
     */
    public function getMethodMeasurement()
    {
        return $this->methodMeasurement;
    }


    /**
     * Set resultCriteria
     *
     * @param string $resultCriteria
     *
     * @return ProjectResult
     */
    public function setResultCriteria($resultCriteria)
    {
        $this->resultCriteria = $resultCriteria;

        return $this;
    }

    /**
     * Get resultCriteria
     *
     * @return string
     */
    public function getResultCriteria()
    {
        return $this->resultCriteria;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ProjectResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set expectedValue
     *
     * @param string $expectedValue
     *
     * @return ProjectResult
     */
    public function setExpectedValue($expectedValue)
    {
        $this->expectedValue = $expectedValue;

        return $this;
    }

    /**
     * Get expectedValue
     *
     * @return string
     */
    public function getExpectedValue()
    {
        return $this->expectedValue;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProjectResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set kNS2017Application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $kNS2017Application
     *
     * @return ProjectResult
     */
    public function setKNS2017Application(\NKO\OrderBundle\Entity\BaseApplication $kNS2017Application = null)
    {
        $this->KNS2017Application = $kNS2017Application;

        return $this;
    }

    /**
     * Get kNS2017Application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getKNS2017Application()
    {
        return $this->KNS2017Application;
    }

    /**
     * Set linkedMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $linkedMethod
     *
     * @return ProjectResult
     */
    public function setLinkedMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $linkedMethod = null)
    {
        $this->linkedMethod = $linkedMethod;

        return $this;
    }

    /**
     * Get linkedMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getLinkedMethod()
    {
        return $this->linkedMethod;
    }
}
