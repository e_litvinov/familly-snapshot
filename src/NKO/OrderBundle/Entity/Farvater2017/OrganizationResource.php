<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrganizationResource
 *
 * @ORM\Table(name="farvater2017_organization_resource")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\OrganizationResourceRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable
 */
class OrganizationResource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018", "KNS2017-2", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *         }
     *     )
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="organizationResources",
     *     cascade={"persist"})
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $farvater2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OrganizationResource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return OrganizationResource
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return OrganizationResource
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return OrganizationResource
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\BaseApplication $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }
}
