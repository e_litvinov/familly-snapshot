<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseApplication;

/**
 * PracticeImplementationActivity
 *
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\PracticeImplementationActivityRepository")
 * @Gedmo\Loggable
 */
class PracticeImplementationActivity extends BaseActivity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="practiceImplementationActivities")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")

     * @Gedmo\Versioned
     *
     */
    private $farvater2017Application;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return PracticeImplementationActivity
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\BaseApplication $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }
}
