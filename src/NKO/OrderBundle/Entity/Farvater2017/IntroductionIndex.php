<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * IntroductionIndex
 *
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\IntroductionIndexRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable
 */
class IntroductionIndex extends BaseIndex
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="introductionIndexes",
     *     cascade={"persist"})
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $farvater2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return IntroductionIndex
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return IntroductionIndex
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }
}
