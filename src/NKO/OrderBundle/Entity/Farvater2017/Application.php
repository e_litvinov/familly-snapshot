<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\PurposeInterface;
use NKO\OrderBundle\Entity\Risk;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use NKO\OrderBundle\Traits\ScheduleTrait;
use NKO\OrderBundle\Traits\ResourceTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\DocumentTrait;

/**
 * Application
 *
 * @ORM\Table(name="farvater2017_application")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\ApplicationRepository")
 */
class Application extends BaseApplication implements PurposeInterface
{
    use ScheduleTrait;
    use ResourceTrait;
    use BudgetTrait;
    use DocumentTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="project_purpose", type="text", nullable=true)
     */
    private $projectPurpose;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="practice_motivation", type="text", nullable=true)
     */
    private $practiceMotivation;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Farvater\TargetAudience", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="farvater2017_application_target_audience",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="audience_id", referencedColumnName="id")}
     * )
     */
    private $targetAudiences;

    /**
     * @var string
     *
     * @ORM\Column(name="target_audience_etc", type="text", nullable=true)
     */
    private $targetAudienceEtc;

    /**
     * @ORM\ManyToMany(targetEntity="PracticeSpreadFormat", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="farvater2017_application_practice_spread_format",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="format_id", referencedColumnName="id")}
     * )
     */
    private $practiceSpreadFormats;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_spread_format_etc", type="text", nullable=true)
     */
    private $practiceSpreadFormatEtc;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="practice_spread_technology", type="text", nullable=true)
     */
    private $practiceSpreadTechnology;

    /**
     * @ORM\OneToMany(targetEntity="PracticeActivityIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $practiceActivityIndexes;

    /**
     * @ORM\OneToMany(targetEntity="PublicationIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $publicationIndexes;

    /**
     * @var string
     *
     * @ORM\Column(name="publication_index_comment", type="text", nullable=true)
     */
    private $publicationIndexComment;

    /**
     * @ORM\OneToMany(targetEntity="IntroductionIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $introductionIndexes;

    /**
     * @var string
     *
     * @ORM\Column(name="introduction_index_comment", type="text", nullable=true)
     */
    private $introductionIndexComment;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="spread_results_stability", type="text", nullable=true)
     */
    private $spreadResultsStability;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="introduction_factors", type="text", nullable=true)
     */
    private $introductionFactors;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="organizational_capacity", type="text", nullable=true)
     */
    private $organizationalCapacity;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="expected_results", type="text", nullable=true)
     */
    private $expectedResults;

    /**
     * @ORM\OneToMany(targetEntity="ImmediateResult", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $immediateResults;

    /**
     * @ORM\OneToMany(targetEntity="SocialResultIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $socialResultIndexes;

    /**
     * @ORM\OneToMany(targetEntity="BeneficiaryImprovementIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $beneficiaryImprovementIndexes;

    /**
     * @ORM\OneToMany(targetEntity="OtherBeneficiaryImprovementIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $otherBeneficiaryImprovementIndexes;

    /**
     * @ORM\OneToMany(targetEntity="AdditionalIndicatorIndex", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $additionalIndicatorIndexes;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="expected_results_stability", type="text", nullable=true)
     */
    private $expectedResultsStability;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017"}
     *     )
     *
     * @ORM\Column(name="brief_practice_description", type="text", nullable=true)
     */
    private $briefPracticeDescription;


    public function __construct()
    {
        parent::__construct();

        $this->targetAudiences = new ArrayCollection();
        $this->practiceSpreadFormats = new ArrayCollection();
        $this->practiceActivityIndexes = new ArrayCollection();
        $this->publicationIndexes = new ArrayCollection();
        $this->introductionIndexes = new ArrayCollection();
        $this->practiceImplementationActivities = new ArrayCollection();
        $this->practiceSpreadActivities = new ArrayCollection();
        $this->monitoringResultsActivities = new ArrayCollection();
        $this->projectMembers = new ArrayCollection();
        $this->projectPartners = new ArrayCollection();
        $this->organizationResources = new ArrayCollection();
        $this->immediateResults = new ArrayCollection();
        $this->socialResultIndexes = new ArrayCollection();
        $this->beneficiaryImprovementIndexes = new ArrayCollection();
        $this->otherBeneficiaryImprovementIndexes = new ArrayCollection();
        $this->additionalIndicatorIndexes = new ArrayCollection();

        $this->preCreateInstances();

        $this->preCreatePublicationIndexes();
        $this->preCreateIntroductionIndexes();
        $this->preOrganizationResources();
        $this->preCreateSocialResultIndexes();
        $this->preCreateBeneficiaryImprovementIndexes();
        $this->preCreateAdditionalIndicatorIndexes();
    }

    public function getPurpose()
    {
        return $this->getProjectPurpose();
    }

    private function preCreateInstances()
    {
        $immediateResult = new ImmediateResult();
        $this->addImmediateResult($immediateResult);
        $otherBeneficiaryImprovementIndex = new OtherBeneficiaryImprovementIndex();
        $this->addOtherBeneficiaryImprovementIndex($otherBeneficiaryImprovementIndex);
        $practiceActivityIndex = new PracticeActivityIndex();
        $this->addPracticeActivityIndex($practiceActivityIndex);
        $practiceImplementationActivity = new PracticeImplementationActivity();
        $this->addPracticeImplementationActivity($practiceImplementationActivity);
        $practiceSpreadActivity = new PracticeSpreadActivity();
        $this->addPracticeSpreadActivity($practiceSpreadActivity);
        $monitoringResultsActivity = new MonitoringResultsActivity();
        $this->addMonitoringResultsActivity($monitoringResultsActivity);
        $projectMember = new ProjectMember();
        $this->addProjectMember($projectMember);
        $projectPartner = new ProjectPartner();
        $this->addProjectPartner($projectPartner);
        $risk = new Risk();
        $this->addRisk($risk);
    }

    private function preCreatePublicationIndexes()
    {
        $publicationIndex1 = new PublicationIndex();
        $publicationIndex1->setIndexName('Число сообщений в СМИ, инициированных в рамках проекта');
        $this->addPublicationIndex($publicationIndex1);
        $publicationIndex2 = new PublicationIndex();
        $publicationIndex2->setIndexName('Число изданных информационных / методических материалов');
        $this->addPublicationIndex($publicationIndex2);
        $publicationIndex3 = new PublicationIndex();
        $publicationIndex3->setIndexName('Тираж печатных информационных / методических материалов');
        $this->addPublicationIndex($publicationIndex3);
        $publicationIndex4 = new PublicationIndex();
        $publicationIndex4->setIndexName('Количество распространенных экземпляров информационных / методических материалов по проекту');
        $this->addPublicationIndex($publicationIndex4);
    }

    private function preCreateIntroductionIndexes()
    {
        $introductionIndex = new IntroductionIndex();
        $introductionIndex->setIndexName('Число организаций, внедривших практику в свою деятельность');
        $this->addIntroductionIndex($introductionIndex);
    }

    private function preCreateSocialResultIndexes()
    {
        $socialResultIndex1 = new SocialResultIndex();
        $socialResultIndex1->setIndexName('1. Количество детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства');
        $this->addSocialResultIndex($socialResultIndex1);
        $socialResultIndex2 = new SocialResultIndex();
        $socialResultIndex2->setIndexName('1.1. в том числе подростков');
        $this->addSocialResultIndex($socialResultIndex2);
        $socialResultIndex3 = new SocialResultIndex();
        $socialResultIndex3->setIndexName('1.2. в том числе детей с ОВЗ');
        $this->addSocialResultIndex($socialResultIndex3);
        $socialResultIndex4 = new SocialResultIndex();
        $socialResultIndex4->setIndexName('1.3. в том числе сиблингов');
        $this->addSocialResultIndex($socialResultIndex4);
        $socialResultIndex5 = new SocialResultIndex();
        $socialResultIndex5->setIndexName('2. Количество детей, возвращённых в кровные семьи');
        $this->addSocialResultIndex($socialResultIndex5);
        $socialResultIndex6 = new SocialResultIndex();
        $socialResultIndex6->setIndexName('2.1. в том числе подростков');
        $this->addSocialResultIndex($socialResultIndex6);
        $socialResultIndex7 = new SocialResultIndex();
        $socialResultIndex7->setIndexName('2.2. в том числе детей с ОВЗ');
        $this->addSocialResultIndex($socialResultIndex7);
        $socialResultIndex8 = new SocialResultIndex();
        $socialResultIndex8->setIndexName('3. Количество предотвращённых случаев отобрания (изъятий) / отказов детей из кровных семей');
        $this->addSocialResultIndex($socialResultIndex8);
        $socialResultIndex9 = new SocialResultIndex();
        $socialResultIndex9->setIndexName('4. Количество предотвращённых случаев отобрания (изъятий) / отказов из замещающих семей');
        $this->addSocialResultIndex($socialResultIndex9);
    }

    private function preCreateBeneficiaryImprovementIndexes()
    {
        $beneficiaryImprovementIndex1 = new BeneficiaryImprovementIndex();
        $beneficiaryImprovementIndex1->setIndexName('улучшение психического состояния (снижение уровня тревожности, агрессии, страха и т.п.)');
        $this->addBeneficiaryImprovementIndex($beneficiaryImprovementIndex1);
        $beneficiaryImprovementIndex2 = new BeneficiaryImprovementIndex();
        $beneficiaryImprovementIndex2->setIndexName('улучшение физического состояния');
        $this->addBeneficiaryImprovementIndex($beneficiaryImprovementIndex2);
        $beneficiaryImprovementIndex3 = new BeneficiaryImprovementIndex();
        $beneficiaryImprovementIndex3->setIndexName('повышение уровня развития, навыков');
        $this->addBeneficiaryImprovementIndex($beneficiaryImprovementIndex3);
        $beneficiaryImprovementIndex4 = new BeneficiaryImprovementIndex();
        $beneficiaryImprovementIndex4->setIndexName('улучшение детско-родительских отношений');
        $this->addBeneficiaryImprovementIndex($beneficiaryImprovementIndex4);
        $beneficiaryImprovementIndex5 = new BeneficiaryImprovementIndex();
        $beneficiaryImprovementIndex5->setIndexName('улучшение показателей успеваемости');
        $this->addBeneficiaryImprovementIndex($beneficiaryImprovementIndex5);
    }

    private function preCreateAdditionalIndicatorIndexes()
    {
        $additionalIndicatorIndex1 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex1->setIndexName('Количество детей, повысивших уровень готовности к самостоятельной жизни в обществе');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex1);
        $additionalIndicatorIndex2 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex2->setIndexName('Количество кровных кризисных семей, повысивших родительские / профессиональные компетенции');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex2);
        $additionalIndicatorIndex3 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex3->setIndexName('Количество замещающих семей, повысивших родительские  / профессиональные компетенции');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex3);
        $additionalIndicatorIndex4 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex4->setIndexName('Количество семей, у которых наблюдается улучшение детско-родительских отношений');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex4);
        $additionalIndicatorIndex5 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex5->setIndexName('Количество замещающих семей, в отношении которых повышен уровень поддержки со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex5);
        $additionalIndicatorIndex6 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex6->setIndexName('Количество кризисных кровных семей, в отношении которых  повышен уровень поддержки со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex6);
        $additionalIndicatorIndex7 = new AdditionalIndicatorIndex;
        $additionalIndicatorIndex7->setIndexName('Число новых запросов на услуги (в области профилактики сиротства) со стороны семей целевых групп');
        $this->addAdditionalIndicatorIndex($additionalIndicatorIndex7);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set practiceMotivation
     *
     * @param string $practiceMotivation
     *
     * @return Application
     */
    public function setPracticeMotivation($practiceMotivation)
    {
        $this->practiceMotivation = $practiceMotivation;

        return $this;
    }

    /**
     * Get practiceMotivation
     *
     * @return string
     */
    public function getPracticeMotivation()
    {
        return $this->practiceMotivation;
    }

    /**
     * Set targetAudienceEtc
     *
     * @param string $targetAudienceEtc
     *
     * @return Application
     */
    public function setTargetAudienceEtc($targetAudienceEtc)
    {
        $this->targetAudienceEtc = $targetAudienceEtc;

        return $this;
    }

    /**
     * Get targetAudienceEtc
     *
     * @return string
     */
    public function getTargetAudienceEtc()
    {
        return $this->targetAudienceEtc;
    }

    /**
     * Add targetAudience
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience
     *
     * @return Application
     */
    public function addTargetAudience(\NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience)
    {
        $this->targetAudiences[] = $targetAudience;

        return $this;
    }

    /**
     * Remove targetAudience
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience
     */
    public function removeTargetAudience(\NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience)
    {
        $this->targetAudiences->removeElement($targetAudience);
    }

    /**
     * Get targetAudiences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetAudiences()
    {
        return $this->targetAudiences;
    }

    /**
     * @Assert\Callback(
     *     groups={"Farvater-2017"}
     *     )
     */
    public function isValidTargetAudiences(ExecutionContextInterface $context, $payload)
    {
        if(!$this->targetAudiences->getValues() && !$this->targetAudienceEtc)
            $context->buildViolation('выберите целевую аудиторию')
                ->atPath('targetAudiences')
                ->addViolation();
    }

    /**
     * Set practiceSpreadFormatEtc
     *
     * @param string $practiceSpreadFormatEtc
     *
     * @return Application
     */
    public function setPracticeSpreadFormatEtc($practiceSpreadFormatEtc)
    {
        $this->practiceSpreadFormatEtc = $practiceSpreadFormatEtc;

        return $this;
    }

    /**
     * Get practiceSpreadFormatEtc
     *
     * @return string
     */
    public function getPracticeSpreadFormatEtc()
    {
        return $this->practiceSpreadFormatEtc;
    }

    /**
     * Add practiceSpreadFormat
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadFormat $practiceSpreadFormat
     *
     * @return Application
     */
    public function addPracticeSpreadFormat(\NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadFormat $practiceSpreadFormat)
    {
        $this->practiceSpreadFormats[] = $practiceSpreadFormat;

        return $this;
    }

    /**
     * Remove practiceSpreadFormat
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadFormat $practiceSpreadFormat
     */
    public function removePracticeSpreadFormat(\NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadFormat $practiceSpreadFormat)
    {
        $this->practiceSpreadFormats->removeElement($practiceSpreadFormat);
    }

    /**
     * Get practiceSpreadFormats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSpreadFormats()
    {
        return $this->practiceSpreadFormats;
    }

    /**
     * @Assert\Callback(
     *     groups={"Farvater-2017"}
     *     )
     */
    public function isValidPracticeSpreadFormats(ExecutionContextInterface $context, $payload)
    {
        if(!$this->practiceSpreadFormats->getValues() && !$this->practiceSpreadFormatEtc)
            $context->buildViolation('выберите форматы распространения Практики')
                ->atPath('practiceSpreadFormats')
                ->addViolation();
    }

    /**
     * Set practiceSpreadTechnology
     *
     * @param string $practiceSpreadTechnology
     *
     * @return Application
     */
    public function setPracticeSpreadTechnology($practiceSpreadTechnology)
    {
        $this->practiceSpreadTechnology = $practiceSpreadTechnology;

        return $this;
    }

    /**
     * Get practiceSpreadTechnology
     *
     * @return string
     */
    public function getPracticeSpreadTechnology()
    {
        return $this->practiceSpreadTechnology;
    }

    /**
     * Add practiceActivityIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeActivityIndex $practiceActivityIndex
     *
     * @return Application
     */
    public function addPracticeActivityIndex(\NKO\OrderBundle\Entity\Farvater2017\PracticeActivityIndex $practiceActivityIndex)
    {
        $practiceActivityIndex->setFarvater2017Application($this);
        $this->practiceActivityIndexes[] = $practiceActivityIndex;

        return $this;
    }

    /**
     * Remove practiceActivityIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeActivityIndex $practiceActivityIndex
     */
    public function removePracticeActivityIndex(\NKO\OrderBundle\Entity\Farvater2017\PracticeActivityIndex $practiceActivityIndex)
    {
        $this->practiceActivityIndexes->removeElement($practiceActivityIndex);
    }

    /**
     * Get practiceActivityIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeActivityIndexes()
    {
        return $this->practiceActivityIndexes;
    }

    /**
     * Add publicationIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PublicationIndex $publicationIndex
     *
     * @return Application
     */
    public function addPublicationIndex(\NKO\OrderBundle\Entity\Farvater2017\PublicationIndex $publicationIndex)
    {
        $publicationIndex->setFarvater2017Application($this);
        $this->publicationIndexes[] = $publicationIndex;

        return $this;
    }

    /**
     * Remove publicationIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PublicationIndex $publicationIndex
     */
    public function removePublicationIndex(\NKO\OrderBundle\Entity\Farvater2017\PublicationIndex $publicationIndex)
    {
        $this->publicationIndexes->removeElement($publicationIndex);
    }

    /**
     * Get publicationIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublicationIndexes()
    {
        return $this->publicationIndexes;
    }

    /**
     * Set publicationIndexComment
     *
     * @param string $publicationIndexComment
     *
     * @return Application
     */
    public function setPublicationIndexComment($publicationIndexComment)
    {
        $this->publicationIndexComment = $publicationIndexComment;

        return $this;
    }

    /**
     * Get publicationIndexComment
     *
     * @return string
     */
    public function getPublicationIndexComment()
    {
        return $this->publicationIndexComment;
    }

    /**
     * Add introductionIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\IntroductionIndex $introductionIndex
     *
     * @return Application
     */
    public function addIntroductionIndex(\NKO\OrderBundle\Entity\Farvater2017\IntroductionIndex $introductionIndex)
    {
        $introductionIndex->setFarvater2017Application($this);
        $this->introductionIndexes[] = $introductionIndex;

        return $this;
    }

    /**
     * Remove introductionIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\IntroductionIndex $introductionIndex
     */
    public function removeIntroductionIndex(\NKO\OrderBundle\Entity\Farvater2017\IntroductionIndex $introductionIndex)
    {
        $this->introductionIndexes->removeElement($introductionIndex);
    }

    /**
     * Get introductionIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntroductionIndexes()
    {
        return $this->introductionIndexes;
    }

    /**
     * Set introductionIndexComment
     *
     * @param string $introductionIndexComment
     *
     * @return Application
     */
    public function setIntroductionIndexComment($introductionIndexComment)
    {
        $this->introductionIndexComment = $introductionIndexComment;

        return $this;
    }

    /**
     * Get introductionIndexComment
     *
     * @return string
     */
    public function getIntroductionIndexComment()
    {
        return $this->introductionIndexComment;
    }

    /**
     * Set spreadResultsStability
     *
     * @param string $spreadResultsStability
     *
     * @return Application
     */
    public function setSpreadResultsStability($spreadResultsStability)
    {
        $this->spreadResultsStability = $spreadResultsStability;

        return $this;
    }

    /**
     * Get spreadResultsStability
     *
     * @return string
     */
    public function getSpreadResultsStability()
    {
        return $this->spreadResultsStability;
    }

    /**
     * Set introductionFactors
     *
     * @param string $introductionFactors
     *
     * @return Application
     */
    public function setIntroductionFactors($introductionFactors)
    {
        $this->introductionFactors = $introductionFactors;

        return $this;
    }

    /**
     * Get introductionFactors
     *
     * @return string
     */
    public function getIntroductionFactors()
    {
        return $this->introductionFactors;
    }

    /**
     * Set organizationalCapacity
     *
     * @param string $organizationalCapacity
     *
     * @return Application
     */
    public function setOrganizationalCapacity($organizationalCapacity)
    {
        $this->organizationalCapacity = $organizationalCapacity;

        return $this;
    }

    /**
     * Get organizationalCapacity
     *
     * @return string
     */
    public function getOrganizationalCapacity()
    {
        return $this->organizationalCapacity;
    }

    /**
     * Set expectedResults
     *
     * @param string $expectedResults
     *
     * @return Application
     */
    public function setExpectedResults($expectedResults)
    {
        $this->expectedResults = $expectedResults;

        return $this;
    }

    /**
     * Get expectedResults
     *
     * @return string
     */
    public function getExpectedResults()
    {
        return $this->expectedResults;
    }

    /**
     * Add organizationResource
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource
     *
     * @return Application
     */
    public function addOrganizationResource(\NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource)
    {
        $organizationResource->setFarvater2017Application($this);
        $this->organizationResources[] = $organizationResource;

        return $this;
    }

    /**
     * Remove organizationResource
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource
     */
    public function removeOrganizationResource(\NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource)
    {
        $this->organizationResources->removeElement($organizationResource);
    }

    /**
     * Get organizationResources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizationResources()
    {
        return $this->organizationResources;
    }

    /**
     * Set projectPurpose
     *
     * @param string $projectPurpose
     *
     * @return Application
     */
    public function setProjectPurpose($projectPurpose)
    {
        $this->projectPurpose = $projectPurpose;

        return $this;
    }

    /**
     * Get projectPurpose
     *
     * @return string
     */
    public function getProjectPurpose()
    {
        return $this->projectPurpose;
    }

    /**
     * Add immediateResult
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\ImmediateResult $immediateResult
     *
     * @return Application
     */
    public function addImmediateResult(\NKO\OrderBundle\Entity\Farvater2017\ImmediateResult $immediateResult)
    {
        $immediateResult->setFarvater2017Application($this);
        $this->immediateResults[] = $immediateResult;

        return $this;
    }

    /**
     * Remove immediateResult
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\ImmediateResult $immediateResult
     */
    public function removeImmediateResult(\NKO\OrderBundle\Entity\Farvater2017\ImmediateResult $immediateResult)
    {
        $this->immediateResults->removeElement($immediateResult);
    }

    /**
     * Get immediateResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImmediateResults()
    {
        return $this->immediateResults;
    }

    /**
     * Add socialResultIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\SocialResultIndex $socialResultIndex
     *
     * @return Application
     */
    public function addSocialResultIndex(\NKO\OrderBundle\Entity\Farvater2017\SocialResultIndex $socialResultIndex)
    {
        $socialResultIndex->setFarvater2017Application($this);
        $this->socialResultIndexes[] = $socialResultIndex;

        return $this;
    }

    /**
     * Remove socialResultIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\SocialResultIndex $socialResultIndex
     */
    public function removeSocialResultIndex(\NKO\OrderBundle\Entity\Farvater2017\SocialResultIndex $socialResultIndex)
    {
        $this->socialResultIndexes->removeElement($socialResultIndex);
    }

    /**
     * Get socialResultIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResultIndexes()
    {
        return $this->socialResultIndexes;
    }

    /**
     * Add beneficiaryImprovementIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\BeneficiaryImprovementIndex $beneficiaryImprovementIndex
     *
     * @return Application
     */
    public function addBeneficiaryImprovementIndex(\NKO\OrderBundle\Entity\Farvater2017\BeneficiaryImprovementIndex $beneficiaryImprovementIndex)
    {
        $beneficiaryImprovementIndex->setFarvater2017Application($this);
        $this->beneficiaryImprovementIndexes[] = $beneficiaryImprovementIndex;

        return $this;
    }

    /**
     * Remove beneficiaryImprovementIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\BeneficiaryImprovementIndex $beneficiaryImprovementIndex
     */
    public function removeBeneficiaryImprovementIndex(\NKO\OrderBundle\Entity\Farvater2017\BeneficiaryImprovementIndex $beneficiaryImprovementIndex)
    {
        $this->beneficiaryImprovementIndexes->removeElement($beneficiaryImprovementIndex);
    }

    /**
     * Get beneficiaryImprovementIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryImprovementIndexes()
    {
        return $this->beneficiaryImprovementIndexes;
    }

    /**
     * Add otherBeneficiaryImprovementIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\OtherBeneficiaryImprovementIndex $otherBeneficiaryImprovementIndex
     *
     * @return Application
     */
    public function addOtherBeneficiaryImprovementIndex(\NKO\OrderBundle\Entity\Farvater2017\OtherBeneficiaryImprovementIndex $otherBeneficiaryImprovementIndex)
    {
        $otherBeneficiaryImprovementIndex->setFarvater2017Application($this);
        $this->otherBeneficiaryImprovementIndexes[] = $otherBeneficiaryImprovementIndex;

        return $this;
    }

    /**
     * Remove otherBeneficiaryImprovementIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\OtherBeneficiaryImprovementIndex $otherBeneficiaryImprovementIndex
     */
    public function removeOtherBeneficiaryImprovementIndex(\NKO\OrderBundle\Entity\Farvater2017\OtherBeneficiaryImprovementIndex $otherBeneficiaryImprovementIndex)
    {
        $this->otherBeneficiaryImprovementIndexes->removeElement($otherBeneficiaryImprovementIndex);
    }

    /**
     * Get otherBeneficiaryImprovementIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherBeneficiaryImprovementIndexes()
    {
        return $this->otherBeneficiaryImprovementIndexes;
    }

    /**
     * Add additionalIndicatorIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\AdditionalIndicatorIndex $additionalIndicatorIndex
     *
     * @return Application
     */
    public function addAdditionalIndicatorIndex(\NKO\OrderBundle\Entity\Farvater2017\AdditionalIndicatorIndex $additionalIndicatorIndex)
    {
        $additionalIndicatorIndex->setFarvater2017Application($this);
        $this->additionalIndicatorIndexes[] = $additionalIndicatorIndex;

        return $this;
    }

    /**
     * Remove additionalIndicatorIndex
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\AdditionalIndicatorIndex $additionalIndicatorIndex
     */
    public function removeAdditionalIndicatorIndex(\NKO\OrderBundle\Entity\Farvater2017\AdditionalIndicatorIndex $additionalIndicatorIndex)
    {
        $this->additionalIndicatorIndexes->removeElement($additionalIndicatorIndex);
    }

    /**
     * Get additionalIndicatorIndexes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdditionalIndicatorIndexes()
    {
        return $this->additionalIndicatorIndexes;
    }

    /**
     * Set expectedResultsStability
     *
     * @param integer $expectedResultsStability
     *
     * @return Application
     */
    public function setExpectedResultsStability($expectedResultsStability)
    {
        $this->expectedResultsStability = $expectedResultsStability;

        return $this;
    }

    /**
     * Get expectedResultsStability
     *
     * @return integer
     */
    public function getExpectedResultsStability()
    {
        return $this->expectedResultsStability;
    }

    /**
     * Set briefPracticeDescription
     *
     * @param string $briefPracticeDescription
     *
     * @return Application
     */
    public function setBriefPracticeDescription($briefPracticeDescription)
    {
        $this->briefPracticeDescription = $briefPracticeDescription;

        return $this;
    }

    /**
     * Get briefPracticeDescription
     *
     * @return string
     */
    public function getBriefPracticeDescription()
    {
        return $this->briefPracticeDescription;
    }
}
