<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ImmediateResult
 *
 * @ORM\Table(name="farvater2017_immediate_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\ImmediateResultRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable
 */
class ImmediateResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="string", length=255, nullable=true)
     */
    private $activity;

    /**
     * @var string
     *
     * @ORM\Column(name="target_group", type="string", length=255, nullable=true)
     */
    private $targetGroup;

    /**
     * @var int
     *
     * @ORM\Column(name="first_year_activities_count", type="integer", nullable=true)
     */
    private $firstYearActivitiesCount;

    /**
     * @var int
     *
     * @ORM\Column(name="first_year_beneficiaries_count", type="integer", nullable=true)
     */
    private $firstYearBeneficiariesCount;

    /**
     * @var int
     *
     * @ORM\Column(name="third_year_activities_count", type="integer", nullable=true)
     */
    private $thirdYearActivitiesCount;

    /**
     * @var int
     *
     * @ORM\Column(name="third_year_beneficiaries_count", type="integer", nullable=true)
     */
    private $thirdYearBeneficiariesCount;

    /**
     * @var string
     *
     * @ORM\Column(name="measurement_method", type="text", nullable=true)
     */
    private $measurementMethod;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="immediateResults",
     *     cascade={"persist"})
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $farvater2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return ImmediateResult
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set targetGroup
     *
     * @param string $targetGroup
     *
     * @return ImmediateResult
     */
    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return string
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    /**
     * Set firstYearActivitiesCount
     *
     * @param integer $firstYearActivitiesCount
     *
     * @return ImmediateResult
     */
    public function setFirstYearActivitiesCount($firstYearActivitiesCount)
    {
        $this->firstYearActivitiesCount = $firstYearActivitiesCount;

        return $this;
    }

    /**
     * Get firstYearActivitiesCount
     *
     * @return int
     */
    public function getFirstYearActivitiesCount()
    {
        return $this->firstYearActivitiesCount;
    }

    /**
     * Set firstYearBeneficiariesCount
     *
     * @param integer $firstYearBeneficiariesCount
     *
     * @return ImmediateResult
     */
    public function setFirstYearBeneficiariesCount($firstYearBeneficiariesCount)
    {
        $this->firstYearBeneficiariesCount = $firstYearBeneficiariesCount;

        return $this;
    }

    /**
     * Get firstYearBeneficiariesCount
     *
     * @return int
     */
    public function getFirstYearBeneficiariesCount()
    {
        return $this->firstYearBeneficiariesCount;
    }

    /**
     * Set thirdYearActivitiesCount
     *
     * @param integer $thirdYearActivitiesCount
     *
     * @return ImmediateResult
     */
    public function setThirdYearActivitiesCount($thirdYearActivitiesCount)
    {
        $this->thirdYearActivitiesCount = $thirdYearActivitiesCount;

        return $this;
    }

    /**
     * Get thirdYearActivitiesCount
     *
     * @return int
     */
    public function getThirdYearActivitiesCount()
    {
        return $this->thirdYearActivitiesCount;
    }

    /**
     * Set thirdYearBeneficiariesCount
     *
     * @param integer $thirdYearBeneficiariesCount
     *
     * @return ImmediateResult
     */
    public function setThirdYearBeneficiariesCount($thirdYearBeneficiariesCount)
    {
        $this->thirdYearBeneficiariesCount = $thirdYearBeneficiariesCount;

        return $this;
    }

    /**
     * Get thirdYearBeneficiariesCount
     *
     * @return int
     */
    public function getThirdYearBeneficiariesCount()
    {
        return $this->thirdYearBeneficiariesCount;
    }

    /**
     * Set measurementMethod
     *
     * @param string $measurementMethod
     *
     * @return ImmediateResult
     */
    public function setMeasurementMethod($measurementMethod)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return string
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ImmediateResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return ImmediateResult
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }
}
