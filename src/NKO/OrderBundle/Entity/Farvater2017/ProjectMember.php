<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectMember
 *
 * @ORM\Table(name="farvater2017_project_member")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\ProjectMemberRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable
 */
class ProjectMember
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *          "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="role", type="string", length=255, nullable=true)
     */
    private $role;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Farvater-2018"
     *     }
     * )
     *
     * @ORM\Column(name="functional_responsibilities", type="text", nullable=true)
     */
    private $functionalResponsibilities;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\ManyToOne(targetEntity="EmploymentRelationship", fetch="EAGER")
     * @ORM\JoinColumn(name="relationship_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employmentRelationship;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="brief_informationr", type="text", nullable=true)
     */
    private $briefInformation;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="projectMembers",
     *     cascade={"persist"})
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $farvater2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return ProjectMember
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return ProjectMember
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set functionalResponsibilities
     *
     * @param string $functionalResponsibilities
     *
     * @return ProjectMember
     */
    public function setFunctionalResponsibilities($functionalResponsibilities)
    {
        $this->functionalResponsibilities = $functionalResponsibilities;

        return $this;
    }

    /**
     * Get functionalResponsibilities
     *
     * @return string
     */
    public function getFunctionalResponsibilities()
    {
        return $this->functionalResponsibilities;
    }

    /**
     * Set briefInformation
     *
     * @param string $briefInformation
     *
     * @return ProjectMember
     */
    public function setBriefInformation($briefInformation)
    {
        $this->briefInformation = $briefInformation;

        return $this;
    }

    /**
     * Get briefInformation
     *
     * @return string
     */
    public function getBriefInformation()
    {
        return $this->briefInformation;
    }

    /**
     * Set employmentRelationship
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\EmploymentRelationship $employmentRelationship
     *
     * @return ProjectMember
     */
    public function setEmploymentRelationship(\NKO\OrderBundle\Entity\Farvater2017\EmploymentRelationship $employmentRelationship = null)
    {
        $this->employmentRelationship = $employmentRelationship;

        return $this;
    }

    /**
     * Get employmentRelationship
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\EmploymentRelationship
     */
    public function getEmploymentRelationship()
    {
        return $this->employmentRelationship;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProjectMember
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return ProjectMember
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\BaseApplication $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }
}
