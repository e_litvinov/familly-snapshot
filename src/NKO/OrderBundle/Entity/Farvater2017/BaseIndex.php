<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BaseIndex
 *
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\BaseIndexRepository")
 * @ORM\Table(name="farvater2017_base_index")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="entity_class", type="string")
 */
class BaseIndex
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="index_name", type="text", nullable=true)
     */
    private $indexName;

    /**
     * @var string
     *
     * @ORM\Column(name="first_year_target_value", type="text", nullable=true)
     */
    private $firstYearTargetValue;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="second_year_target_value", type="text", nullable=true)
     */
    private $secondYearTargetValue;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="third_year_target_value", type="text", nullable=true)
     */
    private $thirdYearTargetValue;

    /**
     * @var string
     *
     * @ORM\Column(name="measurement_method", type="text", nullable=true)
     */
    private $measurementMethod;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indexName
     *
     * @param string $indexName
     *
     * @return BaseIndex
     */
    public function setIndexName($indexName)
    {
        $this->indexName = $indexName;

        return $this;
    }

    /**
     * Get indexName
     *
     * @return string
     */
    public function getIndexName()
    {
        return $this->indexName;
    }

    /**
     * Set firstYearTargetValue
     *
     * @param string $firstYearTargetValue
     *
     * @return BaseIndex
     */
    public function setFirstYearTargetValue($firstYearTargetValue)
    {
        $this->firstYearTargetValue = $firstYearTargetValue;

        return $this;
    }

    /**
     * Get firstYearTargetValue
     *
     * @return string
     */
    public function getFirstYearTargetValue()
    {
        return $this->firstYearTargetValue;
    }

    /**
     * Set secondYearTargetValue
     *
     * @param string $secondYearTargetValue
     *
     * @return BaseIndex
     */
    public function setSecondYearTargetValue($secondYearTargetValue)
    {
        $this->secondYearTargetValue = $secondYearTargetValue;

        return $this;
    }

    /**
     * Get secondYearTargetValue
     *
     * @return string
     */
    public function getSecondYearTargetValue()
    {
        return $this->secondYearTargetValue;
    }

    /**
     * Set thirdYearTargetValue
     *
     * @param string $thirdYearTargetValue
     *
     * @return BaseIndex
     */
    public function setThirdYearTargetValue($thirdYearTargetValue)
    {
        $this->thirdYearTargetValue = $thirdYearTargetValue;

        return $this;
    }

    /**
     * Get thirdYearTargetValue
     *
     * @return string
     */
    public function getThirdYearTargetValue()
    {
        return $this->thirdYearTargetValue;
    }

    /**
     * Set measurementMethod
     *
     * @param string $measurementMethod
     *
     * @return BaseIndex
     */
    public function setMeasurementMethod($measurementMethod)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return string
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }
}
