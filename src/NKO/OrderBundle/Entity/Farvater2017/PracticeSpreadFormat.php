<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;

/**
 * PracticeSpreadFormat
 *
 * @ORM\Table(name="farvater2017_practice_spread_format")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\PracticeSpreadFormatRepository")
 */
class PracticeSpreadFormat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PracticeSpreadFormat
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name ?: '';
    }
}
