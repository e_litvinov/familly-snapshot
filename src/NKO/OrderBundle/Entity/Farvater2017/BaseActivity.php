<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BaseActivity
 *
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\BaseActivityRepository")
 * @ORM\Table(name="farvater2017_base_activity")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="entity_class", type="string")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class BaseActivity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *          "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="activity", type="text", nullable=true)
     */
    private $activity;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="dead_line", type="string", length=255, nullable=true)
     */
    private $deadLine;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *          "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="expected_results", type="text", nullable=true)
     */
    private $expectedResults;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    //TODO this is for fix bug of
    public function setStaticId($id)
    {
        $this->id = $id;

        return $this;
    }
    public function getStaticId()
    {
        return $this->id;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return BaseActivity
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set deadLine
     *
     * @param string $deadLine
     *
     * @return BaseActivity
     */
    public function setDeadLine($deadLine)
    {
        $this->deadLine = $deadLine;

        return $this;
    }

    /**
     * Get deadLine
     *
     * @return string
     */
    public function getDeadLine()
    {
        return $this->deadLine;
    }

    /**
     * Set expectedResults
     *
     * @param string $expectedResults
     *
     * @return BaseActivity
     */
    public function setExpectedResults($expectedResults)
    {
        $this->expectedResults = $expectedResults;

        return $this;
    }

    /**
     * Get expectedResults
     *
     * @return string
     */
    public function getExpectedResults()
    {
        return $this->expectedResults;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return BaseActivity
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
