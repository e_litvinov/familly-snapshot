<?php

namespace NKO\OrderBundle\Entity\Farvater2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectPartner
 *
 * @ORM\Table(name="farvater2017_project_partner")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater2017\ProjectPartnerRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Gedmo\Loggable
 */
class ProjectPartner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018", "KNS2018-3", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="organization_name", type="text", nullable=true)
     */
    private $organizationName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018", "KNS2018-3", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="brief_information", type="text", nullable=true)
     */
    private $briefInformation;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018", "KNS2018-3", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="project_participation", type="text", nullable=true)
     */
    private $projectParticipation;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="projectPartners",
     *     cascade={"persist"})
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $farvater2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizationName
     *
     * @param string $organizationName
     *
     * @return ProjectPartner
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    /**
     * Get organizationName
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Set briefInformation
     *
     * @param string $briefInformation
     *
     * @return ProjectPartner
     */
    public function setBriefInformation($briefInformation)
    {
        $this->briefInformation = $briefInformation;

        return $this;
    }

    /**
     * Get briefInformation
     *
     * @return string
     */
    public function getBriefInformation()
    {
        return $this->briefInformation;
    }

    /**
     * Set projectParticipation
     *
     * @param string $projectParticipation
     *
     * @return ProjectPartner
     */
    public function setProjectParticipation($projectParticipation)
    {
        $this->projectParticipation = $projectParticipation;

        return $this;
    }

    /**
     * Get projectParticipation
     *
     * @return string
     */
    public function getProjectParticipation()
    {
        return $this->projectParticipation;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProjectPartner
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return ProjectPartner
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\BaseApplication $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }
}
