<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/1/17
 * Time: 11:27 AM
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Costs
 *
 * @ORM\Table(name="grant_config")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\GrantConfigRepository")
 */
class GrantConfig
{
    /**
     * @var int
     *
     * @ORM\Column( type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Regex("/^\d+(\.\d{1,2})?$/",
     *
     *     message="2 number after digital point")
     *
     * @ORM\Column(name="sum_grant", type="string", nullable=true)
     */
    private $sumGrant;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="text", nullable=true)
     */
    private $contract;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", inversedBy="grants")
     * @ORM\JoinColumn(name="report_form_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reportForm;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", inversedBy="grants")
     * @ORM\JoinColumn(name="application_history_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $applicationHistory;

    public function __toString()
    {
        return (string) $this->contract;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sumGrant
     *
     * @param string $sumGrant
     *
     * @return GrantConfig
     */
    public function setSumGrant($sumGrant)
    {
        $this->sumGrant = $sumGrant;

        return $this;
    }

    /**
     * Get sumGrant
     *
     * @return string
     */
    public function getSumGrant()
    {
        return $this->sumGrant;
    }

    /**
     * Set contract
     *
     * @param string $contract
     *
     * @return GrantConfig
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return GrantConfig
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }

    /**
     * Set applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     *
     * @return GrantConfig
     */
    public function setApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory = null)
    {
        $this->applicationHistory = $applicationHistory;

        return $this;
    }

    /**
     * Get applicationHistory
     *
     * @return \NKO\OrderBundle\Entity\ApplicationHistory
     */
    public function getApplicationHistory()
    {
        return $this->applicationHistory;
    }
}
