<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Traits\ProjectResultsTrait;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ReportRepository")
 */
class Report extends BaseReport
{
    use FinanceReportTrait;
    use ProjectResultsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="recipient", type="text", nullable=true)
     */
    protected $recipient;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     *
     * @ORM\Column(name="solving_problems_ways", type="text", nullable=true)
     */
    protected $solvingProblemsWays;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     *
     * @ORM\Column(name="recommendations", type="text", nullable=true)
     */
    protected $recommendations;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     *
     * @ORM\Column(name="contract", type="text", nullable=true)
     */
    protected $contract;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="project_name", type="text", nullable=true)
     */
    protected $projectName;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TraineeshipTopic")
     *
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     *
     */
    protected $traineeshipTopic;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Organization")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     *
     */
    protected $organization;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     *
     * @ORM\Column(name="deadLineStart", type="datetime", nullable=true)
     */
    protected $deadLineStart;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     *
     * @ORM\Column(name="deadLineFinish", type="datetime", nullable=true)
     */
    protected $deadLineFinish;

    /**
     * @var float
     *
     * @ORM\Column(name="sum_grant", type="float", nullable=true)
     */
    protected $sumGrant;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="$authorized_name", type="text", nullable=true)
     */
    protected $authorizedName;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="$authorized_position", type="text", nullable=true)
     */
    protected $authorizedPosition;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     )
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "phone code must be at least 3 characters long",
     * )
     * @ORM\Column(name="authorized_phone_code", type="string", length=255, nullable=true)
     */
    protected $authorizedPhoneCode;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     )
     * @Assert\Length(
     *      min = 7,
     *      minMessage = "phone must be at least 7 characters long",
     * )
     * @ORM\Column(name="authorized_phone", type="string", length=255, nullable=true)
     */
    protected $authorizedPhone;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     )
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "mobile code must be at least 3 characters long",
     * )
     * @ORM\Column(name="authorized_mobile_phone_code", type="string", length=255, nullable=true)
     */
    protected $authorizedMobilePhoneCode;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     )
     * @Assert\Length(
     *      min = 7,
     *      minMessage = "mobile phone must be at least 7 characters long",
     * )
     * @ORM\Column(name="authorized_mobile_phone", type="string", length=255, nullable=true)
     */
    protected $authorizedMobilePhone;

    /**
     * @var string
     *
     * @Assert\Email(
     *     message = "The email is not valid",
     *     strict = true,
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     *
     * @Assert\Regex(
     *     pattern = "/[а-я]/",
     *     match = false,
     *     message = "not Cyrillic"
     * )
     *
     * @ORM\Column(name="authorized_email", type="string",
     *     length=255, nullable=true)
     */
    protected $authorizedEmail;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="purpose", type="text", nullable=true)
     */
    protected $purpose;

    /**
     * @ORM\OneToMany(targetEntity="TrainingEvent", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $events;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="ImmediateResult", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $immediateResults;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\EmployeeReportResult", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $employeeReportResults;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="BeneficiaryReportResult", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $beneficiaryReportResults;

    /**
     * @ORM\OneToMany(targetEntity="TransportCost", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $transportCosts;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="InvolvedCost", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     */
    protected $involvedCosts;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="EventCost", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     */
    protected $eventCosts;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="OtherCost", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     */
    protected $otherCosts;

    /**
     * @var float
     *
     * @ORM\Column(name="total_transport_cost", type="float", nullable=true)
     */
    protected $totalTransportCost;

    /**
     * @var float
     *
     * @ORM\Column(name="total_involved_cost", type="float", nullable=true)
     */
    protected $totalInvolvedCost;

    /**
     * @var float
     *
     * @ORM\Column(name="total_event_cost", type="float", nullable=true)
     */
    protected $totalEventCost;

    /**
     * @var float
     *
     * @ORM\Column(name="total_other_cost", type="float", nullable=true)
     */
    protected $totalOtherCost;

    /**
     * @Assert\Valid
     * @ORM\ManyToMany(targetEntity="BeneficiaryGroup")
     * @ORM\JoinTable(name="report_beneficiary_group",
     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="beneficiary_group", referencedColumnName="id")}
     *      )
     */
    protected $beneficiaryGroups;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiary_group_name", type="string", length=150, nullable=true)
     */
    protected $beneficiaryGroupName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     *
     * @ORM\Column(name="confirming_document", type="text", nullable=true)
     */
    protected $confirmingDocument;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\DocumentReport", mappedBy="report", orphanRemoval=true)
     */
    protected $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new ArrayCollection();

        $this->beneficiaryReportResults = new ArrayCollection();
        $this->employeeReportResults = new ArrayCollection();
        $this->beneficiaryGroups = new ArrayCollection();

        $this->refreshImmediateResult();
    }

    public function refreshImmediateResult()
    {
        $resultItems = [];
        for ($i = 0; $i < 3; $i++) {
            $resultItems[] = new ImmediateResult();
            $resultItems[$i]->setResultCriteria($i);
        }

        $this->setImmediateResults($resultItems);
    }

    public function __toString()
    {
        return (string)$this->recipient;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipient
     *
     * @param string $recipient
     *
     * @return Report
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set solvingProblemsWays
     *
     * @param string $solvingProblemsWays
     *
     * @return Report
     */
    public function setSolvingProblemsWays($solvingProblemsWays)
    {
        $this->solvingProblemsWays = $solvingProblemsWays;

        return $this;
    }

    /**
     * Get solvingProblemsWays
     *
     * @return string
     */
    public function getSolvingProblemsWays()
    {
        return $this->solvingProblemsWays;
    }

    /**
     * Set recommendations
     *
     * @param string $recommendations
     *
     * @return Report
     */
    public function setRecommendations($recommendations)
    {
        $this->recommendations = $recommendations;

        return $this;
    }

    /**
     * Get recommendations
     *
     * @return string
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set contract
     *
     * @param string $contract
     *
     * @return Report
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     *
     * @return Report
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set deadLineStart
     *
     * @param \DateTime $deadLineStart
     *
     * @return Report
     */
    public function setDeadLineStart($deadLineStart)
    {
        $this->deadLineStart = $deadLineStart;

        return $this;
    }

    /**
     * Get deadLineStart
     *
     * @return \DateTime
     */
    public function getDeadLineStart()
    {
        return $this->deadLineStart;
    }

    /**
     * Set deadLineFinish
     *
     * @param \DateTime $deadLineFinish
     *
     * @return Report
     */
    public function setDeadLineFinish($deadLineFinish)
    {
        $this->deadLineFinish = $deadLineFinish;

        return $this;
    }

    /**
     * Get deadLineFinish
     *
     * @return \DateTime
     */
    public function getDeadLineFinish()
    {
        return $this->deadLineFinish;
    }

    /**
     * Set sumGrant
     *
     * @param float $sumGrant
     *
     * @return Report
     */
    public function setSumGrant($sumGrant)
    {
        $this->sumGrant = $sumGrant;

        return $this;
    }

    /**
     * Get sumGrant
     *
     * @return float
     */
    public function getSumGrant()
    {
        return $this->sumGrant;
    }

    /**
     * Set authorizedName
     *
     * @param string $authorizedName
     *
     * @return Report
     */
    public function setAuthorizedName($authorizedName)
    {
        $this->authorizedName = $authorizedName;

        return $this;
    }

    /**
     * Get authorizedName
     *
     * @return string
     */
    public function getAuthorizedName()
    {
        return $this->authorizedName;
    }

    /**
     * Set authorizedPosition
     *
     * @param string $authorizedPosition
     *
     * @return Report
     */
    public function setAuthorizedPosition($authorizedPosition)
    {
        $this->authorizedPosition = $authorizedPosition;

        return $this;
    }

    /**
     * Get authorizedPosition
     *
     * @return string
     */
    public function getAuthorizedPosition()
    {
        return $this->authorizedPosition;
    }

    /**
     * Set authorizedPhoneCode
     *
     * @param string $authorizedPhoneCode
     *
     * @return Report
     */
    public function setAuthorizedPhoneCode($authorizedPhoneCode)
    {
        $this->authorizedPhoneCode = $authorizedPhoneCode;

        return $this;
    }

    /**
     * Get authorizedPhoneCode
     *
     * @return string
     */
    public function getAuthorizedPhoneCode()
    {
        return $this->authorizedPhoneCode;
    }

    /**
     * Set authorizedPhone
     *
     * @param string $authorizedPhone
     *
     * @return Report
     */
    public function setAuthorizedPhone($authorizedPhone)
    {
        $this->authorizedPhone = $authorizedPhone;

        return $this;
    }

    /**
     * Get authorizedPhone
     *
     * @return string
     */
    public function getAuthorizedPhone()
    {
        return $this->authorizedPhone;
    }

    /**
     * Set authorizedMobilePhoneCode
     *
     * @param string $authorizedMobilePhoneCode
     *
     * @return Report
     */
    public function setAuthorizedMobilePhoneCode($authorizedMobilePhoneCode)
    {
        $this->authorizedMobilePhoneCode = $authorizedMobilePhoneCode;

        return $this;
    }

    /**
     * Get authorizedMobilePhoneCode
     *
     * @return string
     */
    public function getAuthorizedMobilePhoneCode()
    {
        return $this->authorizedMobilePhoneCode;
    }

    /**
     * Set authorizedMobilePhone
     *
     * @param string $authorizedMobilePhone
     *
     * @return Report
     */
    public function setAuthorizedMobilePhone($authorizedMobilePhone)
    {
        $this->authorizedMobilePhone = $authorizedMobilePhone;

        return $this;
    }

    /**
     * Get authorizedMobilePhone
     *
     * @return string
     */
    public function getAuthorizedMobilePhone()
    {
        return $this->authorizedMobilePhone;
    }

    /**
     * Set authorizedEmail
     *
     * @param string $authorizedEmail
     *
     * @return Report
     */
    public function setAuthorizedEmail($authorizedEmail)
    {
        $this->authorizedEmail = $authorizedEmail;

        return $this;
    }

    /**
     * Get authorizedEmail
     *
     * @return string
     */
    public function getAuthorizedEmail()
    {
        return $this->authorizedEmail;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return Report
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set totalTransportCost
     *
     * @param float $totalTransportCost
     *
     * @return Report
     */
    public function setTotalTransportCost($totalTransportCost)
    {
        $this->totalTransportCost = $totalTransportCost;

        return $this;
    }

    /**
     * Get totalTransportCost
     *
     * @return float
     */
    public function getTotalTransportCost()
    {
        return $this->totalTransportCost;
    }

    /**
     * Set totalInvolvedCost
     *
     * @param float $totalInvolvedCost
     *
     * @return Report
     */
    public function setTotalInvolvedCost($totalInvolvedCost)
    {
        $this->totalInvolvedCost = $totalInvolvedCost;

        return $this;
    }

    /**
     * Get totalInvolvedCost
     *
     * @return float
     */
    public function getTotalInvolvedCost()
    {
        return $this->totalInvolvedCost;
    }

    /**
     * Set totalEventCost
     *
     * @param float $totalEventCost
     *
     * @return Report
     */
    public function setTotalEventCost($totalEventCost)
    {
        $this->totalEventCost = $totalEventCost;

        return $this;
    }

    /**
     * Get totalEventCost
     *
     * @return float
     */
    public function getTotalEventCost()
    {
        return $this->totalEventCost;
    }

    /**
     * Set totalOtherCost
     *
     * @param float $totalOtherCost
     *
     * @return Report
     */
    public function setTotalOtherCost($totalOtherCost)
    {
        $this->totalOtherCost = $totalOtherCost;

        return $this;
    }

    /**
     * Get totalOtherCost
     *
     * @return float
     */
    public function getTotalOtherCost()
    {
        return $this->totalOtherCost;
    }

    /**
     * Set traineeshipTopic
     *
     * @param \NKO\OrderBundle\Entity\TraineeshipTopic $traineeshipTopic
     *
     * @return Report
     */
    public function setTraineeshipTopic(\NKO\OrderBundle\Entity\TraineeshipTopic $traineeshipTopic = null)
    {
        $this->traineeshipTopic = $traineeshipTopic;

        return $this;
    }

    /**
     * Get traineeshipTopic
     *
     * @return \NKO\OrderBundle\Entity\TraineeshipTopic
     */
    public function getTraineeshipTopic()
    {
        return $this->traineeshipTopic;
    }

    /**
     * Set organization
     *
     * @param \NKO\OrderBundle\Entity\Organization $organization
     *
     * @return Report
     */
    public function setOrganization(\NKO\OrderBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \NKO\OrderBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Add event
     *
     * @param \NKO\OrderBundle\Entity\TrainingEvent $event
     *
     * @return Report
     */
    public function addEvent(\NKO\OrderBundle\Entity\TrainingEvent $event)
    {
        $event->setReport($this);
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \NKO\OrderBundle\Entity\TrainingEvent $event
     */
    public function removeEvent(\NKO\OrderBundle\Entity\TrainingEvent $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add immediateResult
     *
     * @param \NKO\OrderBundle\Entity\ImmediateResult $immediateResult
     *
     * @return Report
     */
    public function addImmediateResult(\NKO\OrderBundle\Entity\ImmediateResult $immediateResult)
    {
        $immediateResult->setReport($this);
        $this->immediateResults[] = $immediateResult;

        return $this;
    }

    /**
     * Remove immediateResult
     *
     * @param \NKO\OrderBundle\Entity\ImmediateResult $immediateResult
     */
    public function removeImmediateResult(\NKO\OrderBundle\Entity\ImmediateResult $immediateResult)
    {
        $this->immediateResults->removeElement($immediateResult);
    }

    /**
     * Get immediateResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImmediateResults()
    {
        return $this->immediateResults;
    }

    public function setImmediateResults($results)
    {
        $this->immediateResults = new ArrayCollection();
        if (count($results) > 0) {
            foreach ($results as $result) {
                $this->addImmediateResult($result);
            }
        }

        return $this;
    }

    /**
     * Set beneficiaryGroupName
     *
     * @param string $beneficiaryGroupName
     *
     * @return Report
     */
    public function setBeneficiaryGroupName($beneficiaryGroupName)
    {
        $this->beneficiaryGroupName = $beneficiaryGroupName;

        return $this;
    }

    /**
     * Get beneficiaryGroupName
     *
     * @return string
     */
    public function getBeneficiaryGroupName()
    {
        return $this->beneficiaryGroupName;
    }

    /**
     * Add beneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup
     *
     * @return Report
     */
    public function addBeneficiaryGroup(\NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup)
    {
        $this->beneficiaryGroups[] = $beneficiaryGroup;

        return $this;
    }

    /**
     * Remove beneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup
     */
    public function removeBeneficiaryGroup(\NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup)
    {
        $this->beneficiaryGroups->removeElement($beneficiaryGroup);
    }

    public function setBeneficiaryGroups($groups)
    {
        $this->beneficiaryGroups = new ArrayCollection();
        if (count($groups)) {
            foreach ($groups as $group) {
                $this->addBeneficiaryGroup($group);
            }
        }
        return $this;
    }

    /**
     * Get beneficiaryGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryGroups()
    {
        return $this->beneficiaryGroups;
    }

    /**
     * Add transportCost
     *
     * @param \NKO\OrderBundle\Entity\TransportCost $transportCost
     *
     * @return Report
     */
    public function addTransportCost(\NKO\OrderBundle\Entity\TransportCost $transportCost)
    {
        $transportCost->setReport($this);
        $this->transportCosts[] = $transportCost;

        return $this;
    }


    /**
     * Remove transportCost
     *
     * @param \NKO\OrderBundle\Entity\TransportCost $transportCost
     */
    public function removeTransportCost(\NKO\OrderBundle\Entity\TransportCost $transportCost)
    {
        $this->transportCosts->removeElement($transportCost);
    }

    /**
     * Get transportCosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransportCosts()
    {
        return $this->transportCosts;
    }

    /**
     * Add involvedCost
     *
     * @param \NKO\OrderBundle\Entity\InvolvedCost $involvedCost
     *
     * @return Report
     */
    public function addInvolvedCost(\NKO\OrderBundle\Entity\InvolvedCost $involvedCost)
    {
        $involvedCost->setReport($this);
        $this->involvedCosts[] = $involvedCost;

        return $this;
    }

    /**
     * Remove involvedCost
     *
     * @param \NKO\OrderBundle\Entity\InvolvedCost $involvedCost
     */
    public function removeInvolvedCost(\NKO\OrderBundle\Entity\InvolvedCost $involvedCost)
    {
        $this->involvedCosts->removeElement($involvedCost);
    }

    /**
     * Get involvedCosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvolvedCosts()
    {
        return $this->involvedCosts;
    }

    /**
     * Add eventCost
     *
     * @param \NKO\OrderBundle\Entity\EventCost $eventCost
     *
     * @return Report
     */
    public function addEventCost(\NKO\OrderBundle\Entity\EventCost $eventCost)
    {
        $eventCost->setReport($this);
        $this->eventCosts[] = $eventCost;

        return $this;
    }


    /**
     * Remove eventCost
     *
     * @param \NKO\OrderBundle\Entity\EventCost $eventCost
     */
    public function removeEventCost(\NKO\OrderBundle\Entity\EventCost $eventCost)
    {
        $this->eventCosts->removeElement($eventCost);
    }

    /**
     * Get eventCosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventCosts()
    {
        return $this->eventCosts;
    }

    /**
     * Add otherCost
     *
     * @param \NKO\OrderBundle\Entity\OtherCost $otherCost
     *
     * @return Report
     */
    public function addOtherCost(\NKO\OrderBundle\Entity\OtherCost $otherCost)
    {
        $otherCost->setReport($this);
        $this->otherCosts[] = $otherCost;

        return $this;
    }

    /**
     * Remove otherCost
     *
     * @param \NKO\OrderBundle\Entity\OtherCost $otherCost
     */
    public function removeOtherCost(\NKO\OrderBundle\Entity\OtherCost $otherCost)
    {
        $this->otherCosts->removeElement($otherCost);
    }

    /**
     * Get otherCosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherCosts()
    {
        return $this->otherCosts;
    }

    /**
     * Add employeeReportResult
     *
     * @param \NKO\OrderBundle\Entity\EmployeeReportResult $employeeReportResult
     *
     * @return Report
     */
    public function addEmployeeReportResult(\NKO\OrderBundle\Entity\EmployeeReportResult $employeeReportResult)
    {
        $employeeReportResult->setReport($this);
        $this->employeeReportResults[] = $employeeReportResult;

        return $this;
    }


    /**
     * Remove employeeReportResult
     *
     * @param \NKO\OrderBundle\Entity\EmployeeReportResult $employeeReportResult
     */
    public function removeEmployeeReportResult(\NKO\OrderBundle\Entity\EmployeeReportResult $employeeReportResult)
    {
        $this->employeeReportResults->removeElement($employeeReportResult);
    }

    /**
     * Get employeeReportResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployeeReportResults()
    {
        return $this->employeeReportResults;
    }

    /**
     * Add beneficiaryReportResult
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryReportResult $beneficiaryReportResult
     *
     * @return Report
     */
    public function addBeneficiaryReportResult(\NKO\OrderBundle\Entity\BeneficiaryReportResult $beneficiaryReportResult)
    {
        $beneficiaryReportResult->setReport($this);
        $this->beneficiaryReportResults[] = $beneficiaryReportResult;

        return $this;
    }

    /**
     * Remove beneficiaryReportResult
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryReportResult $beneficiaryReportResult
     */
    public function removeBeneficiaryReportResult(\NKO\OrderBundle\Entity\BeneficiaryReportResult $beneficiaryReportResult)
    {
        $this->beneficiaryReportResults->removeElement($beneficiaryReportResult);
    }

    /**
     * Get beneficiaryReportResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryReportResults()
    {
        return $this->beneficiaryReportResults;
    }

    /**
     * @Assert\Callback
     */
    public function isValidBeneficiaryGroup(ExecutionContextInterface $context, $payload)
    {
        if (count($this->getBeneficiaryGroups())==0 && !$this->getBeneficiaryGroupName()) {
            $context->buildViolation('please, choose beneficiary group')
                ->atPath('beneficiaryGroups')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback
     */
    public function isValidDonationBalance(ExecutionContextInterface $context, $payload)
    {
        if ($this->getDonationBalance() < 0) {
            $context->buildViolation('cannot be negative')
                ->atPath('donationBalance')
                ->addViolation();
        }
    }

    public function getCostByType($type)
    {
        switch ($type) {
            case 0:
                return $this->getTransportCosts();
            case 1:
                return $this->getInvolvedCosts();
            case 2:
                return $this->getEventCosts();
            case 3:
                return $this->getOtherCosts();
        }
    }

    /**
     * Set confirmingDocument
     *
     * @param string $confirmingDocument
     *
     * @return Report
     */
    public function setConfirmingDocument($confirmingDocument)
    {
        $this->confirmingDocument = $confirmingDocument;

        return $this;
    }

    /**
     * Get confirmingDocument
     *
     * @return string
     */
    public function getConfirmingDocument()
    {
        return $this->confirmingDocument;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     *
     * @return Report
     */
    public function addDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $document->setReport($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
