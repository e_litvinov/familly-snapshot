<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/2/17
 * Time: 11:28 AM
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as MarkAssert;


/**
 * Mark
 * @ORM\Table(name="mark_criteria")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\MarkCriteriaRepository")
 */
class MarkCriteria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\Column(type="text")
     * */
    private $criterion;

    /**
     *
     *@ORM\OneToMany(targetEntity="QuestionMark", mappedBy="criteria", cascade={"all"})
     */
    private $questions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition", inversedBy="users")
     * @ORM\JoinTable(name="criteria_competitions")
     */
    private $competitions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->competitions = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->criterion;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set criterion
     *
     * @param string $criterion
     *
     * @return MarkCriteria
     */
    public function setCriterion($criterion)
    {
        $this->criterion = $criterion;

        return $this;
    }

    /**
     * Get criterion
     *
     * @return string
     */
    public function getCriterion()
    {
        return $this->criterion;
    }


    /**
     * Add question
     *
     * @param \NKO\OrderBundle\Entity\QuestionMark $question
     *
     * @return MarkCriteria
     */
    public function addQuestion(\NKO\OrderBundle\Entity\QuestionMark $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \NKO\OrderBundle\Entity\QuestionMark $question
     */
    public function removeQuestion(\NKO\OrderBundle\Entity\QuestionMark $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return MarkCriteria
     */
    public function addCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions[] = $competition;

        return $this;
    }

    /**
     * Remove competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     */
    public function removeCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions->removeElement($competition);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }
}
