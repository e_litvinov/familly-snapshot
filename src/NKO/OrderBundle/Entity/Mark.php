<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as MarkAssert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Mark
 * @MarkAssert\MarkRange
 * @ORM\Table(name="mark")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity()
 */
class Mark
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
    * @ORM\Column(name="mark", type="integer", nullable=true)
     */
    private $mark;

    /**
     * @var string
     *
     * @ORM\Column(name="rationale", type="text", nullable=true)
     */
    private $rationale;

    /**
     * @ORM\ManyToOne(targetEntity="MarkList", inversedBy="marks", cascade={"all"})
     * @ORM\JoinColumn(name="mark_list_id", referencedColumnName="id", nullable=true, onDelete="cascade")
    */
    private $markList;

    /**
     * @ORM\ManyToOne(targetEntity="QuestionMark", inversedBy="marks")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id", nullable=TRUE, onDelete="CASCADE")
     */
    private $questionMark;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set mark
     *
     * @param integer $mark
     *
     * @return Mark
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return integer
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set rationale
     *
     * @param string $rationale
     *
     * @return Mark
     */
    public function setRationale($rationale)
    {
        $this->rationale = $rationale;

        return $this;
    }

    /**
     * Get rationale
     *
     * @return string
     */
    public function getRationale()
    {
        return $this->rationale;
    }

    /**
     * Set markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     *
     * @return Mark
     */
    public function setMarkList(\NKO\OrderBundle\Entity\MarkList $markList = null)
    {
        $this->markList = $markList;

        return $this;
    }

    /**
     * Get markList
     *
     * @return \NKO\OrderBundle\Entity\MarkList
     */
    public function getMarkList()
    {
        return $this->markList;
    }

    /**
     * Set questionMark
     *
     * @param \NKO\OrderBundle\Entity\QuestionMark $questionMark
     *
     * @return Mark
     */
    public function setQuestionMark(\NKO\OrderBundle\Entity\QuestionMark $questionMark = null)
    {
        $this->questionMark = $questionMark;

        return $this;
    }

    /**
     * Get questionMark
     *
     * @return \NKO\OrderBundle\Entity\QuestionMark
     */
    public function getQuestionMark()
    {
        return $this->questionMark;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Mark
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
