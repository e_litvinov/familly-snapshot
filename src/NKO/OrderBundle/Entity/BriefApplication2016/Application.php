<?php

namespace NKO\OrderBundle\Entity\BriefApplication2016;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\PurposeInterface;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;

/**
 * Application
 *
 * @ORM\Table(name="brief_application2016")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2016\ApplicationRepository")
 */
class Application extends BaseApplication implements PurposeInterface
{
    use OrganizationInfoTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="NKO\OrderBundle\Entity\Farvater\Grant",
     *     mappedBy="bitrixBriefApplication",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $grants;

    /**
     * @ORM\OneToMany(
     *     targetEntity="NKO\OrderBundle\Entity\Farvater\PublicationMaterial", mappedBy="bitrixBriefApplication",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $publicationMaterials;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Farvater\PriorityDirection", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="brief_application2016_priority_direction",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $priorityDirections;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_direction_etc", type="text", nullable=true)
     */
    private $priorityDirectionEtc;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Farvater\TargetGroup", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="brief_application2016_target_group",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    private $targetGroups;

    /**
     * @var string
     *
     * @ORM\Column(name="target_group_etc", type="text", nullable=true)
     */
    private $targetGroupEtc;

    /**
     * @var string
     *
     * @ORM\Column(name="child_protection_specialists", type="text", nullable=true)
     */
    private $childProtectionSpecialists;

    /**
     * @var string
     *
     * @ORM\Column(name="disabilities_children", type="text", nullable=true)
     */
    private $disabilitiesChildren;

    /**
     * @var string
     *
     * @ORM\Column(name="special_needs_education_children", type="text", nullable=true)
     */
    private $specialNeedsEducationChildren;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Farvater\SocialResult", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="brief_application2016_social_result",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="result_id", referencedColumnName="id")}
     * )
     */
    private $socialResults;

    /**
     * @var string
     * @ORM\Column(name="practice_preview", type="text", nullable=true)
     */
    private $practicePreview;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_evidence", type="text", nullable=true)
     */
    private $practiceEvidence;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_purpose", type="text", nullable=true)
     */
    private $practicePurpose;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_mechanism", type="text", nullable=true)
     */
    private $practiceMechanism;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_results", type="text", nullable=true)
     */
    private $practiceResults;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Region", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="practice_region_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $practiceRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_region_etc", type="string", length=500, nullable=true)
     */
    private $practiceRegionEtc;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Farvater\TargetAudience", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="brief_application2016_target_audience",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="audience_id", referencedColumnName="id")}
     * )
     */
    private $targetAudiences;

    /**
     * @var string
     *
     * @ORM\Column(name="target_audience_etc", type="text", nullable=true)
     */
    private $targetAudienceEtc;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_tech", type="text", nullable=true)
     */
    private $practiceTech;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_expecting_spread_results", type="text", nullable=true)
     */
    private $practiceExpectingSpreadResults;

    /**
     * @var string
     *
     * @ORM\Column(name="organizational_capacity_measure", type="text", nullable=true)
     */
    private $organizationalCapacityMeasure;

    /**
     * @var string
     *
     * @ORM\Column(name="authority_director_document", type="string", length=255, nullable=true)
     */
    private $authorityDirectorDocument;

    private $_authorityDirectorDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="authority_manager_document", type="string", length=255, nullable=true)
     */
    private $authorityManagerDocument;

    private $_authorityManagerDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="social_result_etc", type="text", nullable=true)
     */
    private $socialResultEtc;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_motivation", type="text", nullable=true)
     */
    private $practiceMotivation;

    /**
     * @var string
     *
     * @ORM\Column(name="approximate_requested_money", type="string", nullable=true)
     */
    private $approximateRequestedMoney;

    /**
     * @ORM\OneToMany(
     *     targetEntity="NKO\OrderBundle\Entity\BriefApplication2016\Expenditure",
     *     mappedBy="bitrixBriefApplication",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $expenditures;

    /**
     * @var bool
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;


    public function __construct()
    {
        parent::__construct();

        $this->projects = new ArrayCollection();
        $this->grants = new ArrayCollection();
        $this->priorityDirections = new ArrayCollection();
        $this->targetGroups = new ArrayCollection();
        $this->socialResults = new ArrayCollection();
        $this->targetAudiences = new ArrayCollection();
        $this->publicationMaterials = new ArrayCollection();
        $this->expenditures = new ArrayCollection();
        $this->siteLinks = new ArrayCollection();
        $this->socialNetworkLinks = new ArrayCollection();
        $this->isAddressesEqual = true;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPurpose()
    {
        $this->getPracticePurpose();
    }

    /**
     * Set priorityDirectionEtc
     *
     * @param string $priorityDirectionEtc
     *
     * @return Application
     */
    public function setPriorityDirectionEtc($priorityDirectionEtc)
    {
        $this->priorityDirectionEtc = $priorityDirectionEtc;

        return $this;
    }

    /**
     * Get priorityDirectionEtc
     *
     * @return string
     */
    public function getPriorityDirectionEtc()
    {
        return $this->priorityDirectionEtc;
    }

    /**
     * Set targetGroupEtc
     *
     * @param string $targetGroupEtc
     *
     * @return Application
     */
    public function setTargetGroupEtc($targetGroupEtc)
    {
        $this->targetGroupEtc = $targetGroupEtc;

        return $this;
    }

    /**
     * Get targetGroupEtc
     *
     * @return string
     */
    public function getTargetGroupEtc()
    {
        return $this->targetGroupEtc;
    }

    /**
     * Set childProtectionSpecialists
     *
     * @param string $childProtectionSpecialists
     *
     * @return Application
     */
    public function setChildProtectionSpecialists($childProtectionSpecialists)
    {
        $this->childProtectionSpecialists = $childProtectionSpecialists;

        return $this;
    }

    /**
     * Get childProtectionSpecialists
     *
     * @return string
     */
    public function getChildProtectionSpecialists()
    {
        return $this->childProtectionSpecialists;
    }

    /**
     * Set disabilitiesChildren
     *
     * @param string $disabilitiesChildren
     *
     * @return Application
     */
    public function setDisabilitiesChildren($disabilitiesChildren)
    {
        $this->disabilitiesChildren = $disabilitiesChildren;

        return $this;
    }

    /**
     * Get disabilitiesChildren
     *
     * @return string
     */
    public function getDisabilitiesChildren()
    {
        return $this->disabilitiesChildren;
    }

    /**
     * Set specialNeedsEducationChildren
     *
     * @param string $specialNeedsEducationChildren
     *
     * @return Application
     */
    public function setSpecialNeedsEducationChildren($specialNeedsEducationChildren)
    {
        $this->specialNeedsEducationChildren = $specialNeedsEducationChildren;

        return $this;
    }

    /**
     * Get specialNeedsEducationChildren
     *
     * @return string
     */
    public function getSpecialNeedsEducationChildren()
    {
        return $this->specialNeedsEducationChildren;
    }

    /**
     * Set practicePreview
     *
     * @param string $practicePreview
     *
     * @return Application
     */
    public function setPracticePreview($practicePreview)
    {
        $this->practicePreview = $practicePreview;

        return $this;
    }

    /**
     * Get practicePreview
     *
     * @return string
     */
    public function getPracticePreview()
    {
        return $this->practicePreview;
    }

    /**
     * Set practiceEvidence
     *
     * @param string $practiceEvidence
     *
     * @return Application
     */
    public function setPracticeEvidence($practiceEvidence)
    {
        $this->practiceEvidence = $practiceEvidence;

        return $this;
    }

    /**
     * Get practiceEvidence
     *
     * @return string
     */
    public function getPracticeEvidence()
    {
        return $this->practiceEvidence;
    }

    /**
     * Set practicePurpose
     *
     * @param string $practicePurpose
     *
     * @return Application
     */
    public function setPracticePurpose($practicePurpose)
    {
        $this->practicePurpose = $practicePurpose;

        return $this;
    }

    /**
     * Get practicePurpose
     *
     * @return string
     */
    public function getPracticePurpose()
    {
        return $this->practicePurpose;
    }

    /**
     * Set practiceMechanism
     *
     * @param string $practiceMechanism
     *
     * @return Application
     */
    public function setPracticeMechanism($practiceMechanism)
    {
        $this->practiceMechanism = $practiceMechanism;

        return $this;
    }

    /**
     * Get practiceMechanism
     *
     * @return string
     */
    public function getPracticeMechanism()
    {
        return $this->practiceMechanism;
    }

    /**
     * Set practiceResults
     *
     * @param string $practiceResults
     *
     * @return Application
     */
    public function setPracticeResults($practiceResults)
    {
        $this->practiceResults = $practiceResults;

        return $this;
    }

    /**
     * Get practiceResults
     *
     * @return string
     */
    public function getPracticeResults()
    {
        return $this->practiceResults;
    }

    /**
     * Set practiceRegionEtc
     *
     * @param string $practiceRegionEtc
     *
     * @return Application
     */
    public function setPracticeRegionEtc($practiceRegionEtc)
    {
        $this->practiceRegionEtc = $practiceRegionEtc;

        return $this;
    }

    /**
     * Get practiceRegionEtc
     *
     * @return string
     */
    public function getPracticeRegionEtc()
    {
        return $this->practiceRegionEtc;
    }
    
    /**
     * Set practiceTech
     *
     * @param string $practiceTech
     *
     * @return Application
     */
    public function setPracticeTech($practiceTech)
    {
        $this->practiceTech = $practiceTech;

        return $this;
    }

    /**
     * Get practiceTech
     *
     * @return string
     */
    public function getPracticeTech()
    {
        return $this->practiceTech;
    }

    /**
     * Set practiceExpectingSpreadResults
     *
     * @param string $practiceExpectingSpreadResults
     *
     * @return Application
     */
    public function setPracticeExpectingSpreadResults($practiceExpectingSpreadResults)
    {
        $this->practiceExpectingSpreadResults = $practiceExpectingSpreadResults;

        return $this;
    }

    /**
     * Get practiceExpectingSpreadResults
     *
     * @return string
     */
    public function getPracticeExpectingSpreadResults()
    {
        return $this->practiceExpectingSpreadResults;
    }

    /**
     * Set organizationalCapacityMeasure
     *
     * @param string $organizationalCapacityMeasure
     *
     * @return Application
     */
    public function setOrganizationalCapacityMeasure($organizationalCapacityMeasure)
    {
        $this->organizationalCapacityMeasure = $organizationalCapacityMeasure;

        return $this;
    }

    /**
     * Get organizationalCapacityMeasure
     *
     * @return string
     */
    public function getOrganizationalCapacityMeasure()
    {
        return $this->organizationalCapacityMeasure;
    }

    /**
     * Set authorityDirectorDocument
     *
     * @param string $authorityDirectorDocument
     *
     * @return Application
     */
    public function setAuthorityDirectorDocument($authorityDirectorDocument)
    {
        if(!$this->_authorityDirectorDocument && is_string($this->authorityDirectorDocument)){
            $this->_authorityDirectorDocument = $this->authorityDirectorDocument;
        }

        $this->authorityDirectorDocument = $authorityDirectorDocument;

        return $this;
    }

    /**
     * Get authorityDirectorDocument
     *
     * @return string
     */
    public function getAuthorityDirectorDocument()
    {
        return $this->authorityDirectorDocument;
    }

    /**
     * Set authorityManagerDocument
     *
     * @param string $authorityManagerDocument
     *
     * @return Application
     */
    public function setAuthorityManagerDocument($authorityManagerDocument)
    {
        if(!$this->_authorityManagerDocument && is_string($this->authorityManagerDocument)){
            $this->_authorityManagerDocument = $this->authorityManagerDocument;
        }

        $this->authorityManagerDocument = $authorityManagerDocument;

        return $this;
    }

    /**
     * Get authorityManagerDocument
     *
     * @return string
     */
    public function getAuthorityManagerDocument()
    {
        return $this->authorityManagerDocument;
    }

    /**
     * Set socialResultEtc
     *
     * @param string $socialResultEtc
     *
     * @return Application
     */
    public function setSocialResultEtc($socialResultEtc)
    {
        $this->socialResultEtc = $socialResultEtc;

        return $this;
    }

    /**
     * Get socialResultEtc
     *
     * @return string
     */
    public function getSocialResultEtc()
    {
        return $this->socialResultEtc;
    }

    /**
     * Set practiceMotivation
     *
     * @param string $practiceMotivation
     *
     * @return Application
     */
    public function setPracticeMotivation($practiceMotivation)
    {
        $this->practiceMotivation = $practiceMotivation;

        return $this;
    }

    /**
     * Get practiceMotivation
     *
     * @return string
     */
    public function getPracticeMotivation()
    {
        return $this->practiceMotivation;
    }

    /**
     * Add grant
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Grant $grant
     *
     * @return Application
     */
    public function addGrant(\NKO\OrderBundle\Entity\Farvater\Grant $grant)
    {
        $grant->setBitrixBriefApplication($this);
        $this->grants[] = $grant;

        return $this;
    }

    /**
     * Remove grant
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Grant $grant
     */
    public function removeGrant(\NKO\OrderBundle\Entity\Farvater\Grant $grant)
    {
        $this->grants->removeElement($grant);
    }

    /**
     * Get grants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrants()
    {
        return $this->grants;
    }

    /**
     * Add priorityDirection
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection
     *
     * @return Application
     */
    public function addPriorityDirection(\NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection)
    {
        $this->priorityDirections[] = $priorityDirection;

        return $this;
    }

    /**
     * Remove priorityDirection
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection
     */
    public function removePriorityDirection(\NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection)
    {
        $this->priorityDirections->removeElement($priorityDirection);
    }

    /**
     * Get priorityDirections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriorityDirections()
    {
        return $this->priorityDirections;
    }

    /**
     * Add targetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup
     *
     * @return Application
     */
    public function addTargetGroup(\NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup)
    {
        $this->targetGroups[] = $targetGroup;

        return $this;
    }

    /**
     * Remove targetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup
     */
    public function removeTargetGroup(\NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup)
    {
        $this->targetGroups->removeElement($targetGroup);
    }

    /**
     * Get targetGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetGroups()
    {
        return $this->targetGroups;
    }

    /**
     * Add socialResult
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult
     *
     * @return Application
     */
    public function addSocialResult(\NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult)
    {
        $this->socialResults[] = $socialResult;

        return $this;
    }

    /**
     * Remove socialResult
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult
     */
    public function removeSocialResult(\NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult)
    {
        $this->socialResults->removeElement($socialResult);
    }

    /**
     * Get socialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResults()
    {
        return $this->socialResults;
    }

    /**
     * Set practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     *
     * @return Application
     */
    public function setPracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion = null)
    {
        $this->practiceRegion = $practiceRegion;

        return $this;
    }

    /**
     * Get practiceRegion
     *
     * @return \NKO\OrderBundle\Entity\Region
     */
    public function getPracticeRegion()
    {
        return $this->practiceRegion;
    }

    /**
     * Add targetAudience
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience
     *
     * @return Application
     */
    public function addTargetAudience(\NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience)
    {
        $this->targetAudiences[] = $targetAudience;

        return $this;
    }

    /**
     * Remove targetAudience
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience
     */
    public function removeTargetAudience(\NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience)
    {
        $this->targetAudiences->removeElement($targetAudience);
    }

    /**
     * Get targetAudiences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetAudiences()
    {
        return $this->targetAudiences;
    }

    /**
     * Add publicationMaterial
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial
     *
     * @return Application
     */
    public function addPublicationMaterial(\NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial)
    {
        $publicationMaterial->setBitrixBriefApplication($this);
        $this->publicationMaterials[] = $publicationMaterial;

        return $this;
    }

    /**
     * Remove publicationMaterial
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial
     */
    public function removePublicationMaterial(\NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial)
    {
        $this->publicationMaterials->removeElement($publicationMaterial);
    }

    /**
     * Get publicationMaterials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublicationMaterials()
    {
        return $this->publicationMaterials;
    }

    /**
     * Set targetAudienceEtc
     *
     * @param string $targetAudienceEtc
     *
     * @return Application
     */
    public function setTargetAudienceEtc($targetAudienceEtc)
    {
        $this->targetAudienceEtc = $targetAudienceEtc;

        return $this;
    }

    /**
     * Get targetAudienceEtc
     *
     * @return string
     */
    public function getTargetAudienceEtc()
    {
        return $this->targetAudienceEtc;
    }

    /**
     * Set approximateRequestedMoney
     *
     * @param string $approximateRequestedMoney
     *
     * @return Application
     */
    public function setApproximateRequestedMoney($approximateRequestedMoney)
    {
        $this->approximateRequestedMoney = $approximateRequestedMoney;

        return $this;
    }

    /**
     * Get approximateRequestedMoney
     *
     * @return string
     */
    public function getApproximateRequestedMoney()
    {
        return $this->approximateRequestedMoney;
    }

    /**
     * Add expenditure
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2016\Expenditure $expenditure
     *
     * @return Application
     */
    public function addExpenditure(\NKO\OrderBundle\Entity\BriefApplication2016\Expenditure $expenditure)
    {
        $expenditure->setBitrixBriefApplication($this);
        $this->expenditures[] = $expenditure;

        return $this;
    }

    /**
     * Remove expenditure
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2016\Expenditure $expenditure
     */
    public function removeExpenditure(\NKO\OrderBundle\Entity\BriefApplication2016\Expenditure $expenditure)
    {
        $this->expenditures->removeElement($expenditure);
    }

    /**
     * Get expenditures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenditures()
    {
        return $this->expenditures;
    }

    /**
     * Set oldId
     *
     * @param integer $oldId
     *
     * @return Application
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * Get oldId
     *
     * @return integer
     */
    public function getOldId()
    {
        return $this->oldId;
    }

}
