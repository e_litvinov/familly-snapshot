<?php

namespace NKO\OrderBundle\Entity\BriefApplication2016;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expenditure
 *
 * @ORM\Table(name="brief_application2016_expenditure")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2016\ExpenditureRepository")
 */
class Expenditure
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="requestedMoney", type="string", length=255, nullable=true)
     */
    private $requestedMoney;

    /**
     * @var string
     *
     * @ORM\Column(name="financing", type="string", length=255, nullable=true)
     */
    private $financing;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="NKO\OrderBundle\Entity\BriefApplication2016\Application",
     *     inversedBy="expenditures", cascade={"persist"}
     *     )
     * @ORM\JoinColumn(
     *     name="brief_application_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     *     )
     *
     */
    private $bitrixBriefApplication;

    /**
     * @var string
     *
     * @ORM\Column(name="purpose", type="string", length=255, nullable=true)
     */
    private $purpose;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requestedMoney
     *
     * @param string $requestedMoney
     *
     * @return Expenditure
     */
    public function setRequestedMoney($requestedMoney)
    {
        $this->requestedMoney = $requestedMoney;

        return $this;
    }

    /**
     * Get requestedMoney
     *
     * @return string
     */
    public function getRequestedMoney()
    {
        return $this->requestedMoney;
    }

    /**
     * Set financing
     *
     * @param string $financing
     *
     * @return Expenditure
     */
    public function setFinancing($financing)
    {
        $this->financing = $financing;

        return $this;
    }

    /**
     * Get financing
     *
     * @return string
     */
    public function getFinancing()
    {
        return $this->financing;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Expenditure
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return Expenditure
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set bitrixBriefApplication
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2016\Application $bitrixBriefApplication
     *
     * @return Expenditure
     */
    public function setBitrixBriefApplication(\NKO\OrderBundle\Entity\BriefApplication2016\Application $bitrixBriefApplication = null)
    {
        $this->bitrixBriefApplication = $bitrixBriefApplication;

        return $this;
    }

    /**
     * Get bitrixBriefApplication
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2016\Application
     */
    public function getBitrixBriefApplication()
    {
        return $this->bitrixBriefApplication;
    }
}
