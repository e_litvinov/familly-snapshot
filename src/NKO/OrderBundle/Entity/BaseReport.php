<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\OrderBundle\Traits\AutosaveTrait;
use NKO\OrderBundle\Traits\FieldGetter;
use ReflectionProperty;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BaseReportRepository")
 * @ORM\Table(name="base_report")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @ORM\DiscriminatorMap({
 *    "kns2016" = "Report",
 *    "analytic" = "NKO\OrderBundle\Entity\Report\AnalyticReport\Report",
 *    "finance" = "NKO\OrderBundle\Entity\Report\FinanceReport\Report",
 *    "monitoring" = "NKO\OrderBundle\Entity\Report\MonitoringReport\Report",
 *    "harbor_2020_monitoring_report" = "NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report",
 *    "analytic_kns_1" = "NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report",
 *    "finance_2018" = "NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report",
 *    "analytic_2018" = "NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report",
 *    "analytic_2019" = "NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report",
 *    "analytic_kns_2018" = "NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report",
 *    "mixed_kns_2018" = "NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report",
 *    "mixed_kns_2019" = "NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report",
 *    "mixed_kns_2020" = "NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report",
 *    "finance_2019" = "NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report",
 *    "harbor_analytic_2019" = "NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report",
 *     })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
abstract class BaseReport
{
    use ORMBehaviors\Timestampable\Timestampable;
    use FieldGetter;
    use AutosaveTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", inversedBy="reports")
     * @ORM\JoinColumn(name="application_history_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $applicationHistory;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_accepted", type="boolean", nullable=true, options={"default" : 0})
     */
    protected $isAccepted;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_valid", type="boolean", nullable=true, options={"default" : 0})
     */
    protected $isValid;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="report", cascade={"all"})
     */
    protected $monitoringResults;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_send", type="boolean", nullable=true, options={"default" : 0})
     */
    protected $isSend;

    /**
     * @var string
     *
     * @ORM\Column(name="psrn", type="string", length=13, nullable=true, unique=false)
     */
    protected $psrn;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\PeriodReport")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $period;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm")
     * @ORM\JoinColumn(name="report_form_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reportForm;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $reportFormId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $periodId;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\NKOUser", inversedBy="reports")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\BaseReportTemplate", inversedBy="reports")
     * @ORM\JoinColumn(name="report_template_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reportTemplate;

    /**
     * @var bool
     *
     * @ORM\Column(name="under_consideration", type="boolean", nullable=true)
     */
    protected $underConsideration;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isFirstTimeSent;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastModified;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true,  options={"default" : 0})
     */
    protected $isPlanAccepted;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $userBlockedFinanceReport;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    protected $isBlocked;

    public function __toString()
    {
        if ($this->getAuthor()) {
            return (string) $this->author->getNkoName();
        }

        return (string) $this->getAuthor();
    }


    public function isTwoMonthsNoUpdate()
    {
        if (!$this->isAccepted && ($this instanceof Report
            || $this instanceof \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
            || $this instanceof \NKO\OrderBundle\Entity\Report\MonitoringReport\Report)
        ) {
            $currentDate = new \DateTime();
            $currentDate->modify('-2 month');
            if ($currentDate > $this->getLastModified()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAccepted
     *
     * @param boolean $isAccepted
     *
     * @return BaseReport
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return boolean
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return BaseReport
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return BaseReport
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set psrn
     *
     * @param string $psrn
     *
     * @return BaseReport
     */
    public function setPsrn($psrn)
    {
        $this->psrn = $psrn;

        return $this;
    }

    /**
     * Get psrn
     *
     * @return string
     */
    public function getPsrn()
    {
        return $this->psrn;
    }

    /**
     * Set applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     *
     * @return BaseReport
     */
    public function setApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory = null)
    {
        $this->applicationHistory = $applicationHistory;

        return $this;
    }

    /**
     * Get applicationHistory
     *
     * @return \NKO\OrderBundle\Entity\ApplicationHistory
     */
    public function getApplicationHistory()
    {
        return $this->applicationHistory;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return BaseReport
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set period
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $period
     *
     * @return BaseReport
     */
    public function setPeriod(\NKO\OrderBundle\Entity\PeriodReport $period = null)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \NKO\OrderBundle\Entity\PeriodReport
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return BaseReport
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }

    /**
     * Set reportFormId
     *
     * @param integer $reportFormId
     *
     * @return BaseReport
     */
    public function setReportFormId($reportFormId)
    {
        $this->reportFormId = $reportFormId;

        return $this;
    }

    /**
     * Get reportFormId
     *
     * @return integer
     */
    public function getReportFormId()
    {
        return $this->reportFormId;
    }

    /**
     * Set periodId
     *
     * @param integer $periodId
     *
     * @return BaseReport
     */
    public function setPeriodId($periodId)
    {
        $this->periodId = $periodId;

        return $this;
    }

    /**
     * Get periodId
     *
     * @return integer
     */
    public function getPeriodId()
    {
        return $this->periodId;
    }

    /**
     * Set author
     *
     * @param \NKO\UserBundle\Entity\NKOUser $author
     *
     * @return BaseReport
     */
    public function setAuthor(\NKO\UserBundle\Entity\NKOUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \NKO\UserBundle\Entity\NKOUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set isFirstTimeSent
     *
     * @param boolean $isFirstTimeSent
     *
     * @return BaseReport
     */
    public function setIsFirstTimeSent($isFirstTimeSent)
    {
        $this->isFirstTimeSent = $isFirstTimeSent;

        return $this;
    }

    /**
     * Get isFirstTimeSent
     *
     * @return boolean
     */
    public function getIsFirstTimeSent()
    {
        return $this->isFirstTimeSent;
    }

    /**
     * Set reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     *
     * @return BaseReport
     */
    public function setReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate = null)
    {
        $this->reportTemplate = $reportTemplate;
        $reportTemplate->addReport($this);

        return $this;
    }

    /**
     * Get reportTemplate
     *
     * @return \NKO\OrderBundle\Entity\Report\BaseReportTemplate
     */
    public function getReportTemplate()
    {
        return $this->reportTemplate;
    }

    /**
     * Set underConsideration
     *
     * @param boolean $underConsideration
     *
     * @return BaseReport
     */
    public function setUnderConsideration($underConsideration)
    {
        $this->underConsideration = $underConsideration;

        return $this;
    }

    /**
     * Get underConsideration
     *
     * @return boolean
     */
    public function getUnderConsideration()
    {
        return $this->underConsideration;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     *
     * @return BaseReport
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Get periods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeriods()
    {
        return $this->periods;
    }

    /**
     * Set isPlanAccepted
     *
     * @param boolean $isPlanAccepted
     *
     * @return BaseReport
     */
    public function setIsPlanAccepted($isPlanAccepted)
    {
        $this->isPlanAccepted = $isPlanAccepted;

        return $this;
    }

    /**
     * Get isPlanAccepted
     *
     * @return boolean
     */
    public function getIsPlanAccepted()
    {
        return $this->isPlanAccepted;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }

    /**
     * Set isPlanAccepted
     *
     * @param int $userBlockedFinanceReport
     *
     * @return BaseReport
     */
    public function setUserBlockedFinanceReport($userBlockedFinanceReport)
    {
        $this->userBlockedFinanceReport = $userBlockedFinanceReport;

        return $this;
    }

    /**
     * Get userBlockedFinanceReport
     *
     * @return int
     */
    public function getUserBlockedFinanceReport()
    {
        return $this->userBlockedFinanceReport;
    }

    /**
     * Set isBlocked
     *
     * @param boolean $isBlocked
     *
     * @return BaseReport
     */
    public function setIsBlocked($isBlocked)
    {
        $this->isBlocked = $isBlocked;

        return $this;
    }

    /**
     * Get isBlocked
     *
     * @return boolean
     */
    public function getIsBlocked()
    {
        return $this->isBlocked;
    }

    public function setMonitoringResults($monitoringResults)
    {
        $this->monitoringResults = $monitoringResults;

        return $this;
    }
}
