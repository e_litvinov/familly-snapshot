<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/20/17
 * Time: 4:32 PM
 */

namespace NKO\OrderBundle\Entity;

interface ExpenseTypeInterface
{
    const TRANSPORT_CODE = 'transport';
    const INVOLVED_CODE = 'involved';
    const EVENT_CODE = 'event';
    const OTHER_CODE = 'other';
    const SALARY_CODE = 'salary';
    const STAFF_SALARY_CODE = 'staff_salary';
    const SPECIALIST_SALARY_CODE = 'specialist_salary';
    const MANAGER_SALARY_CODE = 'manager_salary';
    const MATERIAL_COSTS_CODE = 'material_costs';
    const SERVICE_CODE = 'service';
    const RENT_CODE = 'rent';
    const COMMUNICATION_CODE = 'communication';
    const TRANSPORT_CODE_SECOND = 'transport_second';
}
