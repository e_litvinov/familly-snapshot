<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * SocialNetworkLink
 *
 * @ORM\Table(name="social_network_link")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\SocialNetworkLinkRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class SocialNetworkLink
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={
     *         "KNS-2017", "KNS-2016", "BriefApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS",
     *         "KNS-2019", "Harbor-2019", "KNS2018-3", "KNS-2020", "Harbor-2020", "KNS2019-2"
     *     }
     *     )
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="BaseApplication", cascade={"persist"}, inversedBy="socialNetworkLinks")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $application
     *
     * @return SocialNetworkLink
     */
    public function setApplication(BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return SocialNetworkLink
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    public function __toString()
    {
        return (string)$this->getLink();
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return SocialNetworkLink
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
