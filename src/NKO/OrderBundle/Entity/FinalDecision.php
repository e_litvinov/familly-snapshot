<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/6/17
 * Time: 2:30 PM
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Table(name="final_decision")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\FinalDecisionRepository")
 */
class FinalDecision
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Competition", mappedBy="finalDecisions", cascade={"all"})
     */
    private $competitions;

    /**
     * @var int
     * @ORM\Column( type="boolean", nullable=true)
     */
    private $typeDecision;

    /**
     *
     *@ORM\OneToMany(targetEntity="MarkList", mappedBy="finalDecision", cascade={"all"})
     */
    private $markLists;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->competitions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FinalDecision
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return FinalDecision
     */
    public function addCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions[] = $competition;
        $competition->addFinalDecision($this);

        return $this;
    }

    /**
     * Remove competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     */
    public function removeCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions->removeElement($competition);
        $competition->removeFinalDecision($this);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }



    /**
     * Set typeDecision
     *
     * @param boolean $typeDecision
     *
     * @return FinalDecision
     */
    public function setTypeDecision($typeDecision)
    {
        $this->typeDecision = $typeDecision;

        return $this;
    }

    /**
     * Get typeDecision
     *
     * @return boolean
     */
    public function getTypeDecision()
    {
        return $this->typeDecision;
    }

    /**
     * Add markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     *
     * @return FinalDecision
     */
    public function addMarkList(\NKO\OrderBundle\Entity\MarkList $markList)
    {
        $this->markLists[] = $markList;

        return $this;
    }

    /**
     * Remove markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     */
    public function removeMarkList(\NKO\OrderBundle\Entity\MarkList $markList)
    {
        $this->markLists->removeElement($markList);
    }

    /**
     * Get markLists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarkLists()
    {
        return $this->markLists;
    }
}
