<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Organization
 *
 * @ORM\Table(name="organization")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\OrganizationRepository")
 */
class Organization
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="organization_name", type="text", nullable=true)
     */
    private $organizationName;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\TraineeshipTopic")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $traineeshipTopicId;

    public function __toString()
    {
        return (string)$this->getOrganizationName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizationName
     *
     * @param string $organizationName
     *
     * @return Organization
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    /**
     * Get organizationName
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }




    /**
     * Set traineeshipTopicId
     *
     * @param \NKO\OrderBundle\Entity\TraineeshipTopic $traineeshipTopicId
     *
     * @return Organization
     */
    public function setTraineeshipTopicId(\NKO\OrderBundle\Entity\TraineeshipTopic $traineeshipTopicId = null)
    {
        $this->traineeshipTopicId = $traineeshipTopicId;

        return $this;
    }

    /**
     * Get traineeshipTopicId
     *
     * @return \NKO\OrderBundle\Entity\TraineeshipTopic
     */
    public function getTraineeshipTopicId()
    {
        return $this->traineeshipTopicId;
    }
}
