<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.8.18
 * Time: 12.44
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="expense_type_config")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ExpenseTypeConfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ExpenseType", inversedBy="expenseTypeConfig")
     * @ORM\JoinColumn(name="expense_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $expenseType;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $number;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", inversedBy="expenseTypeConfig")
     * @ORM\JoinColumn(name="report_form_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    private $reportForm;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param float $number
     *
     * @return ExpenseTypeConfig
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return float
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ExpenseTypeConfig
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     *
     * @return ExpenseTypeConfig
     */
    public function setExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType = null)
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    /**
     * Get expenseType
     *
     * @return \NKO\OrderBundle\Entity\ExpenseType
     */
    public function getExpenseType()
    {
        return $this->expenseType;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return ExpenseTypeConfig
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }
}
