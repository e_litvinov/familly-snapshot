<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectResult
 *
 * @ORM\Table(name="immediate_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ImmediateResultRepository")
 */
class ImmediateResult
{
    const RESULT_VALUE = array(
      0 => 'Число обученных сотрудников организации / членов общественного объединения (до 15.12.2016)',
      1 => 'Число сотрудников, организации / членов общественного объединения, которые получат новые знания благодаря реализации проекта (на 01.06.2017)',
      2 => 'Число внедренных практик (технологий, услуг, моделей и пр.) в деятельность организации благодаря реализации проекта '
    );
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     *
     * @ORM\Column(name="plan_value", type="integer", nullable=true)
     */
    private $planValue;

    /**
     * @var int
     *
     * @ORM\Column(name="fact_value", type="integer", nullable=true)
     */
    private $factValue;

    /**
     * @var int
     *
     * @ORM\Column(name="expected_value", type="integer", nullable=true)
     */
    private $expectedValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="method_measurement", type="text", nullable=true)
     */
    private $methodMeasurement;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="result_criteria", type="text", nullable=true)
     *
     */
    private $resultCriteria;

    /**
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="immediateResults", cascade={"persist"})
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",
     *     onDelete="CASCADE", )
     */
    private $report;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set planValue
     *
     * @param integer $planValue
     *
     * @return ImmediateResult
     */
    public function setPlanValue($planValue)
    {
        $this->planValue = $planValue;

        return $this;
    }

    /**
     * Get planValue
     *
     * @return integer
     */
    public function getPlanValue()
    {
        return $this->planValue;
    }

    /**
     * Set factValue
     *
     * @param integer $factValue
     *
     * @return ImmediateResult
     */
    public function setFactValue($factValue)
    {
        $this->factValue = $factValue;

        return $this;
    }

    /**
     * Get factValue
     *
     * @return integer
     */
    public function getFactValue()
    {
        return $this->factValue;
    }

    /**
     * Set expectedValue
     *
     * @param integer $expectedValue
     *
     * @return ImmediateResult
     */
    public function setExpectedValue($expectedValue)
    {
        $this->expectedValue = $expectedValue;

        return $this;
    }

    /**
     * Get expectedValue
     *
     * @return integer
     */
    public function getExpectedValue()
    {
        return $this->expectedValue;
    }

    /**
     * Set methodMeasurement
     *
     * @param string $methodMeasurement
     *
     * @return ImmediateResult
     */
    public function setMethodMeasurement($methodMeasurement)
    {
        $this->methodMeasurement = $methodMeasurement;

        return $this;
    }

    /**
     * Get methodMeasurement
     *
     * @return string
     */
    public function getMethodMeasurement()
    {
        return $this->methodMeasurement;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ImmediateResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set resultCriteria
     *
     * @param int $resultCriteria
     *
     * @return ImmediateResult
     */
    public function setResultCriteria($resultCriteria)
    {
        $this->resultCriteria = $resultCriteria;

        return $this;
    }

    /**
     * Get resultCriteria
     *
     * @return int
     */
    public function getResultCriteria()
    {
        return self::RESULT_VALUE[$this->resultCriteria];
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report $report
     *
     * @return ImmediateResult
     */
    public function setReport(\NKO\OrderBundle\Entity\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }
}
