<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Entity\BriefApplication2017\FinancingSourceIndex;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ProjectRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Project
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "BriefApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="project_name", type="text", nullable=true)
     */
    private $projectName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "BriefApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="dead_line", type="text", nullable=true)
     */
    private $deadline;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "BriefApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="donors_partners_and_their_Role", type="text", nullable=true)
     */
    private $donorsPartnersAndTheirRole;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\FinancingSourceIndex", fetch="EAGER")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $index;

    /**
     * @ORM\ManyToOne(targetEntity="BaseApplication")
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType")
     * @ORM\JoinTable(name="kns_application_2018_financing_source_type_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="financing_source_type_id", referencedColumnName="id")}
     *      )
     */
    private $financingSourceTypes;

    public function getHumanizedFinancingSourceTypes()
    {
        return $this->getHumanizedCollections($this->getFinancingSourceTypes());
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $application
     *
     * @return Project
     */
    public function setApplication(\NKO\OrderBundle\Entity\BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    public function __construct()
    {
        $this->indexes = new ArrayCollection();
        $this->financingSourceTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deadline
     *
     * @param string $deadline
     *
     * @return Project
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return string
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Project
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    public function __toString()
    {
        return $this->projectName ? (string)$this->projectName : '';
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     *
     * @return Project
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set donorsPartnersAndTheirRole
     *
     * @param string $donorsPartnersAndTheirRole
     *
     * @return Project
     */
    public function setDonorsPartnersAndTheirRole($donorsPartnersAndTheirRole)
    {
        $this->donorsPartnersAndTheirRole = $donorsPartnersAndTheirRole;

        return $this;
    }

    /**
     * Get donorsPartnersAndTheirRole
     *
     * @return string
     */
    public function getDonorsPartnersAndTheirRole()
    {
        return $this->donorsPartnersAndTheirRole;
    }
    
    /**
     * Set briefApplication
     *
     * @param BriefApplication $briefApplication
     *
     * @return Project
     */
    public function setBriefApplication(BriefApplication $briefApplication = null)
    {
        $this->briefApplication = $briefApplication;

        return $this;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Project
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get briefApplication
     *
     * @return BriefApplication
     */
    public function getBriefApplication()
    {
        return $this->briefApplication;
    }

    /*
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set index
     *
     * @param FinancingSourceIndex $index
     *
     * @return Project
     */
    public function setIndex(FinancingSourceIndex $index = null)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return FinancingSourceIndex
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set kNS2017Application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $kNS2017Application
     *
     * @return Project
     */
    public function setKNS2017Application(\NKO\OrderBundle\Entity\BaseApplication $kNS2017Application = null)
    {
        $this->KNS2017Application = $kNS2017Application;

        return $this;
    }

    /**
     * Get kNS2017Application
     *
     * @return \NKO\OrderBundle\Entity\KNS2017\Application
     */
    public function getKNS2017Application()
    {
        return $this->KNS2017Application;
    }

    /**
     * Set farvater2017Application
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application
     *
     * @return Project
     */
    public function setFarvater2017Application(\NKO\OrderBundle\Entity\Farvater2017\Application $farvater2017Application = null)
    {
        $this->farvater2017Application = $farvater2017Application;

        return $this;
    }

    /**
     * Get farvater2017Application
     *
     * @return \NKO\OrderBundle\Entity\Farvater2017\Application
     */
    public function getFarvater2017Application()
    {
        return $this->farvater2017Application;
    }

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2018", "KNS2017-2", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"}
     *     )
     */
    public function isValidFinancingSourceType(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getDonorsPartnersAndTheirRole() && $this->getFinancingSourceTypes()->isEmpty()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('financingSourceTypes')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016", "BriefApplication-2018"}
     *     )
     */
    public function isValidDonorsPartnersAndTheirRole(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getDonorsPartnersAndTheirRole() && $this->getFinancingSourceTypes()->isEmpty()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('donorsPartnersAndTheirRole')
                ->addViolation();
        }
    }

    /**
     * Add financingSourceType
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType $financingSourceType
     *
     * @return Project
     */
    public function addFinancingSourceType(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType $financingSourceType)
    {
        $this->financingSourceTypes[] = $financingSourceType;

        return $this;
    }

    /**
     * Remove financingSourceType
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType $financingSourceType
     */
    public function removeFinancingSourceType(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType $financingSourceType)
    {
        $this->financingSourceTypes->removeElement($financingSourceType);
    }

    /**
     * Get financingSourceTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinancingSourceTypes()
    {
        return $this->financingSourceTypes;
    }
}
