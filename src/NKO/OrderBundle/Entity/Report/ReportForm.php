<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 14.06.17
 * Time: 19:53
 */

namespace NKO\OrderBundle\Entity\Report;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Report as KNS2016Report;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Table(name="report_form")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ReportFormRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ReportForm
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Competition", inversedBy="reportForms")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    private $competition;

    /**
     * @ORM\OrderBy({"startDate" = "ASC"})
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\PeriodReport", inversedBy="reportForms")
     * @ORM\JoinTable(name="periods_report_forms")
     */
    private $reportPeriods;

    /**
     * @var string
     *
     * @ORM\Column(name="report_class", type="string", nullable=true)
     */
    private $reportClass;

    /**
     * @var \DateTime $start_date
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime $finish_date
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $finishDate;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\ExpenseType", inversedBy="reportForms")
     * @ORM\JoinTable(name="expense_types_report_forms")
     */
    private $expenseTypes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ReportHistory", mappedBy="reportForm")
     */
    private $reportHistories;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\GrantConfig", mappedBy="reportForm", cascade={"all"})
     */
    private $grants;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\BaseReportTemplate", mappedBy="reportForm")
     */
    private $reportTemplates;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator", mappedBy="reportForm", cascade={"all"})
     */
    protected $indicators;

    /**
     * @var \DateTime $year
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $year;

    /**
    * @ORM\ManyToOne(targetEntity="NKO\DesignBlocksBundle\Entity\ReportHeader", inversedBy="reportForms")
    * @ORM\JoinColumn(name="reportHeader_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
    */
    
    private $reportHeader;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_selectable", type="boolean", nullable=true)
     */
    protected $isSelectable;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_transferable", type="boolean", nullable=true, options={"default": true})
     */
    protected $isTransferable = true;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm")
     * @ORM\JoinColumn(name="linked_report_form", referencedColumnName="id", nullable=true)
     */
    protected $linkedForm;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_filled_by_organization", type="boolean", nullable=true)
     */
    protected $isFilledByExpert;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isDataTakesFromActualApplication;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ExpenseTypeConfig", mappedBy="reportForm", cascade={"all"}, orphanRemoval=true)
     */
    private $expenseTypeConfigs;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition", inversedBy="organizations")
     * @ORM\JoinTable(name="banned_report_form_for_competition")
     */
    private $notAllowedCompetitions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\UserBundle\Entity\ExpertUser", mappedBy="reportForms")
     */
    private $experts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reportPeriods = new ArrayCollection();
        $this->reportPeriods;
        $this->reports = new ArrayCollection();
        $this->reportHistories = new ArrayCollection();
        $this->grants = new ArrayCollection();
        $this->reportTemplates = new ArrayCollection();
    }

    public function getLink()
    {
        switch ($this->reportClass) {
            case KNS2016Report::class:
                return 'admin_nko_order_report_create';
            case FinanceReport\Report::class:
                return 'admin_nko_order_report_financereport_report_create';
            case MonitoringReport\Report::class:
                return 'admin_nko_order_report_monitoringreport_report_create';
            case AnalyticReport\Report::class:
                return 'admin_nko_order_report_analyticreport_report_create';
        }

        return null;
    }

    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ReportForm
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set reportClass
     *
     * @param string $reportClass
     *
     * @return ReportForm
     */
    public function setReportClass($reportClass)
    {
        $this->reportClass = $reportClass;

        return $this;
    }

    /**
     * Get reportClass
     *
     * @return string
     */
    public function getReportClass()
    {
        return $this->reportClass;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ReportForm
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set finishDate
     *
     * @param \DateTime $finishDate
     *
     * @return ReportForm
     */
    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;

        return $this;
    }

    /**
     * Get finishDate
     *
     * @return \DateTime
     */
    public function getFinishDate()
    {
        return $this->finishDate;
    }

    /**
     * Set competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return ReportForm
     */
    public function setCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return \NKO\OrderBundle\Entity\Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Add reportPeriod
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $reportPeriod
     *
     * @return ReportForm
     */
    public function addReportPeriod(\NKO\OrderBundle\Entity\PeriodReport $reportPeriod)
    {
        $this->reportPeriods[] = $reportPeriod;

        return $this;
    }

    /**
     * Remove reportPeriod
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $reportPeriod
     */
    public function removeReportPeriod(\NKO\OrderBundle\Entity\PeriodReport $reportPeriod)
    {
        $this->reportPeriods->removeElement($reportPeriod);
    }

    /**
     * Get reportPeriods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportPeriods()
    {
/*        $reportPeriods = $this->reportPeriods;

        if ($reportPeriods) {
            $periods = $this->reportPeriods->getValues();
            usort($periods, function ($a, $b) {
                return ($a->getStartDate() < $b->getStartDate()) ? -1 : 1;
            });
            $reportPeriods = new ArrayCollection($periods);
        }*/

        return $this->reportPeriods;
    }

    /**
     * Add expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     *
     * @return ReportForm
     */
    public function addExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType)
    {
        $this->expenseTypes[] = $expenseType;

        return $this;
    }

    /**
     * Remove expenseType
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $expenseType
     */
    public function removeExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType)
    {
        $this->expenseTypes->removeElement($expenseType);
    }

    /**
     * Get expenseTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenseTypes()
    {
        return $this->expenseTypes;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ReportForm
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add reportHistory
     *
     * @param \NKO\OrderBundle\Entity\ReportHistory $reportHistory
     *
     * @return ReportForm
     */
    public function addReportHistory(\NKO\OrderBundle\Entity\ReportHistory $reportHistory)
    {
        $this->reportHistories[] = $reportHistory;

        return $this;
    }

    /**
     * Remove reportHistory
     *
     * @param \NKO\OrderBundle\Entity\ReportHistory $reportHistory
     */
    public function removeReportHistory(\NKO\OrderBundle\Entity\ReportHistory $reportHistory)
    {
        $this->reportHistories->removeElement($reportHistory);
    }

    /**
     * Get reportHistories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportHistories()
    {
        return $this->reportHistories;
    }

    /**
     * Add grant
     *
     * @param \NKO\OrderBundle\Entity\GrantConfig $grant
     *
     * @return ReportForm
     */
    public function addGrant(\NKO\OrderBundle\Entity\GrantConfig $grant)
    {
        $this->grants[] = $grant;

        return $this;
    }

    /**
     * Remove grant
     *
     * @param \NKO\OrderBundle\Entity\GrantConfig $grant
     */
    public function removeGrant(\NKO\OrderBundle\Entity\GrantConfig $grant)
    {
        $this->grants->removeElement($grant);
        $grant->setReportForm(null);
    }

    /**
     * Get grants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrants()
    {
        return $this->grants;
    }

    /**
     * Add reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     *
     * @return ReportForm
     */
    public function addReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplates[] = $reportTemplate;

        return $this;
    }

    /**
     * Remove reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     */
    public function removeReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplates->removeElement($reportTemplate);
    }

    /**
     * Get reportTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportTemplates()
    {
        return $this->reportTemplates;
    }

    /**
     * Add indicator
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator
     *
     * @return ReportForm
     */
    public function addIndicator(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator)
    {
        $this->indicators[] = $indicator;
        $indicator->setReportForm($this);

        return $this;
    }

    /**
     * Remove indicator
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator
     */
    public function removeIndicator(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator)
    {
        $this->indicators->removeElement($indicator);
        $indicator->setReportForm(null);
    }

    /**
     * Get indicators
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndicators()
    {
        return $this->indicators;
    }

    /**
     * Set year
     *
     * @param \DateTime $year
     *
     * @return ReportForm
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return \DateTime
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set reportHeader
     *
     * @param \NKO\DesignBlocksBundle\Entity\ReportHeader $reportHeader
     *
     * @return ReportForm
     */
    public function setReportHeader(\NKO\DesignBlocksBundle\Entity\ReportHeader $reportHeader = null)
    {
        $this->reportHeader = $reportHeader;

        return $this;
    }

    /**
     * Get reportHeader
     *
     * @return \NKO\DesignBlocksBundle\Entity\ReportHeader
     */
    public function getReportHeader()
    {
        return $this->reportHeader;
    }

    /**
     * Set isSelectable
     *
     * @param boolean $isSelectable
     *
     * @return ReportForm
     */
    public function setIsSelectable($isSelectable)
    {
        $this->isSelectable = $isSelectable;

        return $this;
    }

    /**
     * Get isSelectable
     *
     * @return boolean
     */
    public function getIsSelectable()
    {
        return $this->isSelectable;
    }

    /**
     * Set isTransferable
     *
     * @param boolean isTransferable
     *
     * @return ReportForm
     */
    public function setIsTransferable($isTransferable)
    {
        $this->isTransferable = $isTransferable;

        return $this;
    }

    /**
     * Get isTransferable
     *
     * @return boolean
     */
    public function getIsTransferable()
    {
        return $this->isTransferable;
    }

    /**
     * Set linkedForm
     *
     * @param ReportForm $linkedForm
     *
     * @return ReportForm
     */
    public function setLinkedForm($linkedForm)
    {
        $this->linkedForm = $linkedForm;

        return $this;
    }

    /**
     * Get linkedForm
     *
     * @return ReportForm
     */
    public function getLinkedForm()
    {
        return $this->linkedForm;
    }

    /**
     * Set isFilledByExpert
     *
     * @param boolean $isFilledByExpert
     *
     * @return ReportForm
     */
    public function setIsFilledByExpert($isFilledByExpert)
    {
        $this->isFilledByExpert = $isFilledByExpert;

        return $this;
    }

    /**
     * Get isFilledByExpert
     *
     * @return boolean
     */
    public function getIsFilledByExpert()
    {
        return $this->isFilledByExpert;
    }

    /**
     * Add expenseTypeConfig
     *
     * @param \NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig
     *
     * @return ReportForm
     */
    public function addExpenseTypeConfig(\NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig)
    {
        $this->expenseTypeConfigs[] = $expenseTypeConfig;
        $expenseTypeConfig->setReportForm($this);

        return $this;
    }

    /**
     * Remove expenseTypeConfig
     *
     * @param \NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig
     */
    public function removeExpenseTypeConfig(\NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig)
    {
        $this->expenseTypeConfigs->removeElement($expenseTypeConfig);
    }

    /**
     * Get expenseTypeConfigs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenseTypeConfigs()
    {
        return $this->expenseTypeConfigs;
    }

    /**
     * Add competition
     *
     * @param Competition $competition
     *
     * @return ReportForm
     */
    public function addNotAllowedCompetitions(Competition $competition)
    {
        $this->notAllowedCompetitions[] = $competition;

        return $this;
    }

    /**
     * Remove competition
     *
     * @param Competition $competition
     */
    public function removeNotAllowedCompetitions(Competition $competition)
    {
        $this->notAllowedCompetitions->removeElement($competition);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotAllowedCompetitions()
    {
        return $this->notAllowedCompetitions;
    }

    /**
     * Add notAllowedCompetition
     *
     * @param \NKO\OrderBundle\Entity\Competition $notAllowedCompetition
     *
     * @return ReportForm
     */
    public function addNotAllowedCompetition(\NKO\OrderBundle\Entity\Competition $notAllowedCompetition)
    {
        $this->notAllowedCompetitions[] = $notAllowedCompetition;

        return $this;
    }

    /**
     * Remove notAllowedCompetition
     *
     * @param \NKO\OrderBundle\Entity\Competition $notAllowedCompetition
     */
    public function removeNotAllowedCompetition(\NKO\OrderBundle\Entity\Competition $notAllowedCompetition)
    {
        $this->notAllowedCompetitions->removeElement($notAllowedCompetition);
    }

    /**
     * Add expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     *
     * @return ReportForm
     */
    public function addExpert(\NKO\UserBundle\Entity\ExpertUser $expert)
    {
        $this->experts[] = $expert;

        return $this;
    }

    /**
     * Remove expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     */
    public function removeExpert(\NKO\UserBundle\Entity\ExpertUser $expert)
    {
        $this->experts->removeElement($expert);
    }

    /**
     * Get experts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperts()
    {
        return $this->experts;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenseTypeFromExpenseTypeConfig()
    {
        return  $this->getExpenseTypeConfigs()->map(function ($expenseTypeConfig) {
            return $expenseTypeConfig->getExpenseType();
        });
    }

    public function isDataTakesFromActualApplication()
    {
        return $this->isDataTakesFromActualApplication;
    }

    public function setIsDataTakesFromActualApplication($isDataTakesFromActualApplication)
    {
        $this->isDataTakesFromActualApplication = $isDataTakesFromActualApplication;

        return $this;
    }
}
