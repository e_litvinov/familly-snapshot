<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/12/17
 * Time: 4:50 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="double_context")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class DoubleContext
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     *  @Assert\NotBlank
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     *  @Assert\NotBlank
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $context;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="socialSolvedProblems")
     * @ORM\JoinColumn(name="report_solved_problem_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $socialSolvedProblemReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="practiceNotSolvedProblems")
     * @ORM\JoinColumn(name="practice_not_solves_problem_report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $practiceNotSolvedProblemReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="introductionFactors")
     * @ORM\JoinColumn(name="introduction_factor_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $introductionFactor;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="monitoringDevelopments")
     * @ORM\JoinColumn(name="monitoring_development_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $monitoringDevelopment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="socialNotSolvedProblems")
     * @ORM\JoinColumn(name="report_not_solved_problem_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $socialNotSolvedProblemReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="realizationFactors")
     * @ORM\JoinColumn(name="report_realization_factor_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $realizationFactorReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="practiceSolvedProblems")
     * @ORM\JoinColumn(name="report_practice_solved_problem_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $practiceSolvedProblemReport;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return DoubleContext
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return DoubleContext
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set introductionFactor
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $introductionFactor
     *
     * @return DoubleContext
     */
    public function setIntroductionFactor(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $introductionFactor = null)
    {
        $this->introductionFactor = $introductionFactor;

        return $this;
    }

    /**
     * Get introductionFactor
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getIntroductionFactor()
    {
        return $this->introductionFactor;
    }

    /**
     * Set monitoringDevelopment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $monitoringDevelopment
     *
     * @return DoubleContext
     */
    public function setMonitoringDevelopment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $monitoringDevelopment = null)
    {
        $this->monitoringDevelopment = $monitoringDevelopment;

        return $this;
    }

    /**
     * Get monitoringDevelopment
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getMonitoringDevelopment()
    {
        return $this->monitoringDevelopment;
    }

    /**
     * Set socialNotSolvedProblemReport
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $socialNotSolvedProblemReport
     *
     * @return DoubleContext
     */
    public function setSocialNotSolvedProblemReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $socialNotSolvedProblemReport = null)
    {
        $this->socialNotSolvedProblemReport = $socialNotSolvedProblemReport;

        return $this;
    }

    /**
     * Get socialNotSolvedProblemReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getSocialNotSolvedProblemReport()
    {
        return $this->socialNotSolvedProblemReport;
    }

    /**
     * Set socialSolvedProblemReport
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $socialSolvedProblemReport
     *
     * @return DoubleContext
     */
    public function setSocialSolvedProblemReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $socialSolvedProblemReport = null)
    {
        $this->socialSolvedProblemReport = $socialSolvedProblemReport;

        return $this;
    }

    /**
     * Get socialSolvedProblemReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getSocialSolvedProblemReport()
    {
        return $this->socialSolvedProblemReport;
    }

    /**
     * Set realizationFactorReport
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $realizationFactorReport
     *
     * @return DoubleContext
     */
    public function setRealizationFactorReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $realizationFactorReport = null)
    {
        $this->realizationFactorReport = $realizationFactorReport;

        return $this;
    }

    /**
     * Get realizationFactorReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getRealizationFactorReport()
    {
        return $this->realizationFactorReport;
    }

    /**
     * Set practiceSolvedProblemReport
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $practiceSolvedProblemReport
     *
     * @return DoubleContext
     */
    public function setPracticeSolvedProblemReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $practiceSolvedProblemReport = null)
    {
        $this->practiceSolvedProblemReport = $practiceSolvedProblemReport;

        return $this;
    }

    /**
     * Get practiceSolvedProblemReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getPracticeSolvedProblemReport()
    {
        return $this->practiceSolvedProblemReport;
    }

    /**
     * Set practiceNotSolvedProblemReport
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $practiceNotSolvedProblemReport
     *
     * @return DoubleContext
     */
    public function setPracticeNotSolvedProblemReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $practiceNotSolvedProblemReport = null)
    {
        $this->practiceNotSolvedProblemReport = $practiceNotSolvedProblemReport;

        return $this;
    }

    /**
     * Get practiceNotSolvedProblemReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getPracticeNotSolvedProblemReport()
    {
        return $this->practiceNotSolvedProblemReport;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return DoubleContext
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
