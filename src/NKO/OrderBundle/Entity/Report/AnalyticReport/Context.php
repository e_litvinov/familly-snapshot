<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/12/17
 * Time: 3:27 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="context")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class Context
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "AnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $context;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_social_measure_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $socialMeasure;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_next_social_measure_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $nextSocialMeasure;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_practice_lesson_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $practiceAnalyticLesson;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_stability_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $stabilityAnalyticReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_monitoring_change_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $monitoringChange;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="lessons")
     * @ORM\JoinColumn(name="report_kns_lesson_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $lessonKns;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="successStories")
     * @ORM\JoinColumn(name="report_kns_story_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $successStoryKns;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="feedbackItems")
     * @ORM\JoinColumn(name="report_kns_feedback_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $feedbackKns;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->context;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return Context
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set socialMeasure
     *
     * @param BaseReport $socialMeasure
     *
     * @return Context
     */
    public function setSocialMeasure(BaseReport $socialMeasure = null)
    {
        $this->socialMeasure = $socialMeasure;

        return $this;
    }

    /**
     * Get socialMeasure
     *
     * @return BaseReport
     */
    public function getSocialMeasure()
    {
        return $this->socialMeasure;
    }

    /**
     * Set practiceAnalyticLesson
     *
     * @param BaseReport $practiceAnalyticLesson
     *
     * @return Context
     */
    public function setPracticeAnalyticLesson(BaseReport $practiceAnalyticLesson = null)
    {
        $this->practiceAnalyticLesson = $practiceAnalyticLesson;

        return $this;
    }

    /**
     * Get practiceAnalyticLesson
     *
     * @return BaseReport
     */
    public function getPracticeAnalyticLesson()
    {
        return $this->practiceAnalyticLesson;
    }

    /**
     * Set stabilityAnalyticReport
     *
     * @param BaseReport $stabilityAnalyticReport
     *
     * @return Context
     */
    public function setStabilityAnalyticReport(BaseReport $stabilityAnalyticReport = null)
    {
        $this->stabilityAnalyticReport = $stabilityAnalyticReport;

        return $this;
    }

    /**
     * Get stabilityAnalyticReport
     *
     * @return BaseReport
     */
    public function getStabilityAnalyticReport()
    {
        return $this->stabilityAnalyticReport;
    }

    /**
     * Set monitoringChange
     *
     * @param BaseReport $monitoringChange
     *
     * @return Context
     */
    public function setMonitoringChange(BaseReport $monitoringChange = null)
    {
        $this->monitoringChange = $monitoringChange;

        return $this;
    }

    /**
     * Get monitoringChange
     *
     * @return BaseReport
     */
    public function getMonitoringChange()
    {
        return $this->monitoringChange;
    }

    /**
     * Set nextSocialMeasure
     *
     * @param BaseReport $nextSocialMeasure
     *
     * @return Context
     */
    public function setNextSocialMeasure(BaseReport $nextSocialMeasure = null)
    {
        $this->nextSocialMeasure = $nextSocialMeasure;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->practiceAttachments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get nextSocialMeasure
     *
     * @return Report
     */
    public function getNextSocialMeasure()
    {
        return $this->nextSocialMeasure;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Context
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set lessonKns
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $lessonKns
     *
     * @return Context
     */
    public function setLessonKns(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $lessonKns = null)
    {
        $this->lessonKns = $lessonKns;

        return $this;
    }

    /**
     * Get lessonKns
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getLessonKns()
    {
        return $this->lessonKns;
    }

    /**
     * Set successStoryKns
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $successStoryKns
     *
     * @return Context
     */
    public function setSuccessStoryKns(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $successStoryKns = null)
    {
        $this->successStoryKns = $successStoryKns;

        return $this;
    }

    /**
     * Get successStoryKns
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getSuccessStoryKns()
    {
        return $this->successStoryKns;
    }

    /**
     * Set feedbackKns
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $feedbackKns
     *
     * @return Context
     */
    public function setFeedbackKns(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $feedbackKns = null)
    {
        $this->feedbackKns = $feedbackKns;

        return $this;
    }

    /**
     * Get feedbackKns
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getFeedbackKns()
    {
        return $this->feedbackKns;
    }
}
