<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="analytic_report_2019_effectiveness")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Effectiveness
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    protected $result;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="custom_indicator", type="text", nullable=true)
     */
    protected $customIndicator;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={}
     *     )
     *
     * @ORM\Column(name="indicator_value", type="integer", nullable=true)
     */
    protected $indicatorValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="fact_value", type="integer", nullable=true)
     */
    protected $factValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="plan_value", type="integer", nullable=true)
     */
    protected $planValue;


    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $customMethod;

    /**
     * @Assert\Valid
     * @Assert\Count(min="1", groups={"AnalyticReport-2019"}, minMessage="value is invalid(field must be non empty)")
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinTable(name="report_effectiveness_measurement_method_relationship",
     *      joinColumns={@ORM\JoinColumn(name="effectiveness_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")}
     *      )
     */
    protected $methods;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="implementation_report_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $implementationReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="introduction_report_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $introductionReport;

    /**
     * @var bool
     *
     * @Assert\NotBlank(groups={"AnalyticReport-2019"})
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isFeedback;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function getHumanizedMethods()
    {
        return $this->getHumanizedCollections($this->getMethods());
    }

    public function getId()
    {
        return $this->id;
    }

    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setIndicatorValue($indicatorValue)
    {
        $this->indicatorValue = $indicatorValue;

        return $this;
    }

    public function getIndicatorValue()
    {
        return $this->indicatorValue;
    }

    public function setCustomIndicator($customIndicator)
    {
        $this->customIndicator = $customIndicator;

        return $this;
    }

    public function getCustomIndicator()
    {
        return $this->customIndicator;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setFactValue($factValue)
    {
        $this->factValue = $factValue;

        return $this;
    }

    public function getFactValue()
    {
        return $this->factValue;
    }

    public function setPlanValue($planValue)
    {
        $this->planValue = $planValue;

        return $this;
    }

    public function getPlanValue()
    {
        return $this->planValue;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function __construct()
    {
        $this->methods = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods[] = $method;

        return $this;
    }

    public function removeMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods->removeElement($method);
    }

    public function getMethods()
    {
        return $this->methods;
    }

    public function setIsFeedback($isFeedback)
    {
        $this->isFeedback = $isFeedback;

        return $this;
    }

    public function getIsFeedback()
    {
        return $this->isFeedback;
    }

    public function getCustomMethod()
    {
        return $this->customMethod;
    }

    public function setCustomMethod($customMethod)
    {
        $this->customMethod = $customMethod;

        return $this;
    }

    public function getIntroductionReport()
    {
        return $this->introductionReport;
    }

    public function setIntroductionReport($introductionReport)
    {
        $this->introductionReport = $introductionReport;

        return $this;
    }

    public function getImplementationReport()
    {
        return $this->implementationReport;
    }

    public function setImplementationReport($implementationReport)
    {
        $this->implementationReport = $implementationReport;

        return $this;
    }
}
