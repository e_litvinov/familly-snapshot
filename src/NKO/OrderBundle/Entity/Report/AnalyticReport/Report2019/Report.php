<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019;

use NKO\OrderBundle\Entity\BaseReport;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Traits\AnalyticReport\AttachmentTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ImmediateResultsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\MonitoringTrait;
use NKO\OrderBundle\Traits\AnalyticReport\PriorityDirectionTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\AnalyticReport\PublicationLoaderTrait;
use NKO\OrderBundle\Traits\Report\ExpectedResultsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ResourceTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\AnalyticReport\Report2018\ReportRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_report_2019")
 */
class Report extends BaseReport
{
    use ProjectCardTrait;
    use OfficialsTrait;
    use PriorityDirectionTrait;
    use ImmediateResultsTrait;
    use ExpectedResultsTrait;
    use MonitoringTrait;
    use ResourceTrait;
    use AttachmentTrait;
    use PublicationLoaderTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult")
     * @ORM\JoinColumn(name="practice_result_implementation_id", referencedColumnName="id", nullable=true)
     */
    protected $practiceImplementation;

    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult")
     * @ORM\JoinColumn(name="practice_introduction_implementation_id", referencedColumnName="id", nullable=true)
     */
    protected $practiceIntroduction;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $territories;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="Effectiveness", mappedBy="implementationReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $implementationSocialResults;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="Effectiveness", mappedBy="introductionReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $introductionSocialResults;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Publication", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $analyticPublications;

    /**
     * @var string
     * @Assert\NotBlank(groups={"AnalyticReport-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    private $expectedMonitoringResults;

    /**
     * @Assert\Valid()
     * @CustomAssert\Feedback(
     *     inversedBy="implementationSocialResults",
     *     groups={"AnalyticReport-2019"},
     *     nameOfMethod="defaultEffectiveness"
     * )
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", mappedBy="reportSocial",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceSocialFeedbackAttachments;

    /**
     * @Assert\Valid()
     *
     * @CustomAssert\Feedback(
     *     inversedBy="introductionSocialResults",
     *     groups={"AnalyticReport-2019"},
     *     nameOfMethod="defaultEffectiveness",
     * )
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", mappedBy="reportSpreadSocialAttachment",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceSocialSpreadFeedbackAttachments;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment", mappedBy="reportMonitoring",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringDevelopmentAttachments;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $expectedResultsComment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $implementationSocialResultsComment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $introductionSocialResultsComment;


    /**
     * Add territory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory
     *
     * @return Report
     */
    public function addTerritory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory)
    {
        $this->territories[] = $territory;
        $territory->setReport($this);
        return $this;
    }

    /**
     * Remove territory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory
     */
    public function removeTerritory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory)
    {
        $this->territories->removeElement($territory);
    }

    /**
     * Get territories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerritories()
    {
        return $this->territories;
    }

    /**
     * Set practiceImplementation
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult $practiceImplementation
     *
     * @return Report
     */
    public function setPracticeImplementation(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult $practiceImplementation = null)
    {
        $this->practiceImplementation = $practiceImplementation;

        return $this;
    }

    /**
     * Get practiceImplementation
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult
     */
    public function getPracticeImplementation()
    {
        return $this->practiceImplementation;
    }

    /**
     * Add implementationSocialResult
     *
     * @param Effectiveness $implementationSocialResult
     *
     * @return Report
     */
    public function addImplementationSocialResult(Effectiveness $implementationSocialResult)
    {
        $this->implementationSocialResults[] = $implementationSocialResult;
        $implementationSocialResult->setImplementationReport($this);
        return $this;
    }

    /**
     * Remove implementationSocialResult
     *
     * @param Effectiveness $implementationSocialResult
     */
    public function removeImplementationSocialResult(Effectiveness $implementationSocialResult)
    {
        $this->implementationSocialResults->removeElement($implementationSocialResult);
    }

    /**
     * Get implementationSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImplementationSocialResults()
    {
        return $this->implementationSocialResults;
    }


    /**
     * @param \Doctrine\Common\Collections\Collection
     *
     * @return Report
     */
    public function setImplementationSocialResult($effectivenessCollection)
    {

        foreach ($effectivenessCollection as $effectiveness) {
            $this->addImplementationSocialResult($effectiveness);
        }

        return $this;
    }

    public function addIntroductionSocialResult(Effectiveness $introductionSocialResult)
    {
        $this->introductionSocialResults[] = $introductionSocialResult;
        $introductionSocialResult->setIntroductionReport($this);
        return $this;
    }

    public function removeIntroductionSocialResult(Effectiveness $introductionSocialResult)
    {
        $this->introductionSocialResults->removeElement($introductionSocialResult);
    }

    public function getIntroductionSocialResults()
    {
        return $this->introductionSocialResults;
    }


    public function setIntroductionSocialResult($effectivenessCollection)
    {

        foreach ($effectivenessCollection as $effectiveness) {
            $this->addIntroductionSocialResult($effectiveness);
        }

        return $this;
    }

    public function getPracticeIntroduction()
    {
        return $this->practiceIntroduction;
    }

    public function setPracticeIntroduction($practiceIntroduction)
    {
        $this->practiceIntroduction = $practiceIntroduction;

        return $this;
    }

    public function addAnalyticPublication(\NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $publication)
    {
        $this->analyticPublications[] = $publication;
        $publication->setReport($this);

        return $this;
    }

    public function removeAnalyticPublication(\NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $publication)
    {
        $this->analyticPublications->removeElement($publication);
    }

    public function getAnalyticPublications()
    {
        return $this->analyticPublications;
    }

    public function setExpectedMonitoringResults($expectedMonitoringResults)
    {
        $this->expectedMonitoringResults = $expectedMonitoringResults;

        return $this;
    }

    public function getExpectedMonitoringResults()
    {
        return $this->expectedMonitoringResults;
    }

    public function addPracticeSocialFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceFeedbackAttachment)
    {
        $this->practiceSocialFeedbackAttachments[] = $practiceFeedbackAttachment;
        $practiceFeedbackAttachment->setReportSocial($this);

        return $this;
    }

    public function removePracticeSocialFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceFeedbackAttachment)
    {
        $this->practiceSocialFeedbackAttachments->removeElement($practiceFeedbackAttachment);
    }

    public function getPracticeSocialFeedbackAttachments()
    {
        return $this->practiceSocialFeedbackAttachments;
    }

    public function addPracticeSocialSpreadFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceSpreadFeedbackAttachment)
    {
        $this->practiceSocialSpreadFeedbackAttachments[] = $practiceSpreadFeedbackAttachment;
        $practiceSpreadFeedbackAttachment->setReportSpreadSocialAttachment($this);

        return $this;
    }

    public function removePracticeSocialSpreadFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceSpreadFeedbackAttachment)
    {
        $this->practiceSocialSpreadFeedbackAttachments->removeElement($practiceSpreadFeedbackAttachment);
    }

    public function getPracticeSocialSpreadFeedbackAttachments()
    {
        return $this->practiceSocialSpreadFeedbackAttachments;
    }

    public function addMonitoringDevelopmentAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $monitoringAttachment)
    {
        $this->monitoringDevelopmentAttachments[] = $monitoringAttachment;
        $monitoringAttachment->setReportMonitoring($this);

        return $this;
    }

    public function removeMonitoringDevelopmentAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $monitoringAttachment)
    {
        $this->monitoringDevelopmentAttachments->removeElement($monitoringAttachment);
    }

    public function getMonitoringDevelopmentAttachments()
    {
        return $this->monitoringDevelopmentAttachments;
    }

    public function getExpectedResultsComment()
    {
        return $this->expectedResultsComment;
    }

    public function setExpectedResultsComment($expectedResultsComment)
    {
        $this->expectedResultsComment = $expectedResultsComment;

        return $this;
    }

    public function getImplementationSocialResultsComment()
    {
        return $this->implementationSocialResultsComment;
    }

    public function setImplementationSocialResultsComment($implementationSocialResultsComment)
    {
        $this->implementationSocialResultsComment = $implementationSocialResultsComment;

        return $this;
    }

    public function getIntroductionSocialResultsComment()
    {
        return $this->introductionSocialResultsComment;
    }

    public function setIntroductionSocialResultsComment($introductionSocialResultsComment)
    {
        $this->introductionSocialResultsComment = $introductionSocialResultsComment;

        return $this;
    }
}
