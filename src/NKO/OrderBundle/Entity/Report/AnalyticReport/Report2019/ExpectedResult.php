<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/9/18
 * Time: 3:45 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Application\Continuation\ResultDirection;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="analytic_report2019_expected_result")
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ExpectedResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ResultDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id", nullable=true)
     */
    protected $direction;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $customDirection;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $indicator;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $measurementMethod;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="experience_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $experienceReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="experience_etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $experienceEtcReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="process_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $processReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="process__etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $processEtcReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="qualification_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $qualificationReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="qualification_etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $qualificationEtcReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="resource_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $resourceReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="resource_etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $resourceEtcReport;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDirection(ResultDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function setCustomDirection($customDirection)
    {
        $this->customDirection = $customDirection;

        return $this;
    }

    /**
     * Get customDirection
     *
     * @return string
     */
    public function getCustomDirection()
    {
        return $this->customDirection;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return ExpectedResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set measurementMethod
     *
     * @param string $measurementMethod
     *
     * @return ExpectedResult
     */
    public function setMeasurementMethod($measurementMethod)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return string
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ExpectedResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set experienceReport
     *
     * @param BaseReport $experienceReport
     *
     * @return ExpectedResult
     */
    public function setExperienceReport(BaseReport $experienceReport = null)
    {
        $this->experienceReport = $experienceReport;

        return $this;
    }

    /**
     * Get experienceReport
     *
     * @return BaseReport
     */
    public function getExperienceReport()
    {
        return $this->experienceReport;
    }

    /**
     * Set experienceEtcReport
     *
     * @param BaseReport $experienceEtcReport
     *
     * @return ExpectedResult
     */
    public function setExperienceEtcReport(BaseReport $experienceEtcReport = null)
    {
        $this->experienceEtcReport = $experienceEtcReport;

        return $this;
    }

    /**
     * Get experienceEtcReport
     *
     * @return BaseReport
     */
    public function getExperienceEtcReport()
    {
        return $this->experienceEtcReport;
    }

    /**
     * Set processReport
     *
     * @param BaseReport $processReport
     *
     * @return ExpectedResult
     */
    public function setProcessReport(BaseReport $processReport = null)
    {
        $this->processReport = $processReport;

        return $this;
    }

    /**
     * Get processReport
     *
     * @return BaseReport
     */
    public function getProcessReport()
    {
        return $this->processReport;
    }

    /**
     * Set processEtcReport
     *
     * @param BaseReport $processEtcReport
     *
     * @return ExpectedResult
     */
    public function setProcessEtcReport(BaseReport $processEtcReport = null)
    {
        $this->processEtcReport = $processEtcReport;

        return $this;
    }

    /**
     * Get processEtcReport
     *
     * @return BaseReport
     */
    public function getProcessEtcReport()
    {
        return $this->processEtcReport;
    }

    /**
     * Set qualificationReport
     *
     * @param BaseReport $qualificationReport
     *
     * @return ExpectedResult
     */
    public function setQualificationReport(BaseReport $qualificationReport = null)
    {
        $this->qualificationReport = $qualificationReport;

        return $this;
    }

    /**
     * Get qualificationReport
     *
     * @return BaseReport
     */
    public function getQualificationReport()
    {
        return $this->qualificationReport;
    }

    /**
     * Set qualificationEtcReport
     *
     * @param BaseReport $qualificationEtcReport
     *
     * @return ExpectedResult
     */
    public function setQualificationEtcReport(BaseReport $qualificationEtcReport = null)
    {
        $this->qualificationEtcReport = $qualificationEtcReport;

        return $this;
    }

    /**
     * Get qualificationEtcReport
     *
     * @return BaseReport
     */
    public function getQualificationEtcReport()
    {
        return $this->qualificationEtcReport;
    }

    /**
     * Set resourceReport
     *
     * @param BaseReport $resourceReport
     *
     * @return ExpectedResult
     */
    public function setResourceReport(BaseReport $resourceReport = null)
    {
        $this->resourceReport = $resourceReport;

        return $this;
    }

    /**
     * Get resourceReport
     *
     * @return BaseReport
     */
    public function getResourceReport()
    {
        return $this->resourceReport;
    }

    /**
     * Set resourceEtcReport
     *
     * @param BaseReport $resourceEtcReport
     *
     * @return ExpectedResult
     */
    public function setResourceEtcReport(BaseReport $resourceEtcReport = null)
    {
        $this->resourceEtcReport = $resourceEtcReport;

        return $this;
    }

    /**
     * Get resourceEtcReport
     *
     * @return BaseReport
     */
    public function getResourceEtcReport()
    {
        return $this->resourceEtcReport;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ExpectedResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @Assert\Callback(groups={"AnalyticReport-2019"})
     */
    public function isValidBeneficiaryGroup(ExecutionContextInterface $context, $payload)
    {
        $fields = [
            'comment',
            'measurementMethod',
            'indicator'
        ];

        if (!strpos($context->getPropertyPath(), "Etc")) {
            $accessor = PropertyAccess::createPropertyAccessor();
            foreach ($fields as $field) {
                $value = $accessor->getValue($this, $field);
                if (!$value) {
                    $context->buildViolation('value is invalid(field must be non empty)')
                        ->atPath($field)
                        ->addViolation();
                }
            }
        }
    }
}
