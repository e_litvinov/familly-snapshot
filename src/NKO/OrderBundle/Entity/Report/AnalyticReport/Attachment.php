<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/18/17
 * Time: 10:41 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\FieldGetter;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 *
 * @ORM\Table(name="attachment")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 *
 *  * @CustomAssert\EntityWithFile(
 *     extensions={"pdf"},
 *     fileNames={
 *       "file"
 *     },
 *     relatedFields={
 *         "reportPracticeSuccessStory", "reportSpreadPracticeSuccessStory"
 *     },
 *
 *     notNull=false,
 *     groups={"AnalyticReport-2018", "AnalyticReport-2019"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     extensions={"pdf", "doc", "excel"},
 *     fileNames={
 *       "file"
 *     },
 *     relatedFields={
 *         "reportMonitoring"
 *     },
 *
 *     notNull=false,
 *     groups={"AnalyticReport-2018", "AnalyticReport-2019"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF or Doc file",
 *     extensions={"pdf", "doc"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=false,
 *     groups={"KNS-AnalyticReport-2018"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF or Doc file",
 *     extensions={"pdf", "doc"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=true,
 *     groups={"HarborAnalyticReport-2019"}
 * )
 */

class Attachment
{
    use FieldGetter;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string

     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    /**
     * @var string
     * @Assert\NotBlank(groups={"AnalyticReport-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $text;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     groups={"KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *      )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;

    protected $_file;

    const EXT_1 = [
        'pdf'
    ];

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_practice_success_story_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportPracticeSuccessStory;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_practice_spread_success_story_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportSpreadPracticeSuccessStory;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_monitoring_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportMonitoring;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Attachment
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set reportPracticeSuccessStory
     *
     * @param BaseReport $reportPracticeSuccessStory
     *
     * @return Attachment
     */
    public function setReportPracticeSuccessStory(BaseReport $reportPracticeSuccessStory = null)
    {
        $this->reportPracticeSuccessStory = $reportPracticeSuccessStory;

        return $this;
    }

    /**
     * Get reportPracticeSuccessStory
     *
     * @return BaseReport
     */
    public function getReportPracticeSuccessStory()
    {
        return $this->reportPracticeSuccessStory;
    }

    /**
     * Set reportSpreadPracticeSuccessStory
     *
     * @param BaseReport $reportSpreadPracticeSuccessStory
     *
     * @return Attachment
     */
    public function setReportSpreadPracticeSuccessStory(BaseReport $reportSpreadPracticeSuccessStory = null)
    {
        $this->reportSpreadPracticeSuccessStory = $reportSpreadPracticeSuccessStory;

        return $this;
    }

    /**
     * Get reportSpreadPracticeSuccessStory
     *
     * @return BaseReport
     */
    public function getReportSpreadPracticeSuccessStory()
    {
        return $this->reportSpreadPracticeSuccessStory;
    }

    /**
     * Set reportMonitoring
     *
     * @param BaseReport $reportMonitoring
     *
     * @return Attachment
     */
    public function setReportMonitoring(BaseReport $reportMonitoring = null)
    {
        $this->reportMonitoring = $reportMonitoring;

        return $this;
    }

    /**
     * Get reportMonitoring
     *
     * @return BaseReport
     */
    public function getReportMonitoring()
    {
        return $this->reportMonitoring;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Attachment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Attachment
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @Assert\Callback
     */
    public function isValidAttachments(ExecutionContextInterface $context)
    {
        if (!$this->getFile() && !$this->_file) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('file')
                ->addViolation();
        }
        if ($this->getFile()) {
            $this->validate(self::EXT_1, 'Please upload a valid PDF or Excel file', $context);
        }
    }

    private function validate($arrayExt, $message, $context)
    {
        $extension = pathinfo($this->getFile()->getClientOriginalName())['extension'];
        if (!in_array($extension, $arrayExt)) {
            $context->buildViolation($message)
                ->atPath('file')
                ->addViolation();
        }
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Attachment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
