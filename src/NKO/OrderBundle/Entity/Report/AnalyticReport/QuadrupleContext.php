<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/30/17
 * Time: 10:26 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="quadruple_context")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class QuadrupleContext
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $first;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $second;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $third;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $fours;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="partnerActivities")
     * @ORM\JoinColumn(name="report_partner_activity_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportPartnerActivity;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first
     *
     * @param string $first
     *
     * @return QuadrupleContext
     */
    public function setFirst($first)
    {
        $this->first = $first;

        return $this;
    }

    /**
     * Get first
     *
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Set second
     *
     * @param string $second
     *
     * @return QuadrupleContext
     */
    public function setSecond($second)
    {
        $this->second = $second;

        return $this;
    }

    /**
     * Get second
     *
     * @return string
     */
    public function getSecond()
    {
        return $this->second;
    }

    /**
     * Set third
     *
     * @param string $third
     *
     * @return QuadrupleContext
     */
    public function setThird($third)
    {
        $this->third = $third;

        return $this;
    }

    /**
     * Get third
     *
     * @return string
     */
    public function getThird()
    {
        return $this->third;
    }

    /**
     * Set fours
     *
     * @param string $fours
     *
     * @return QuadrupleContext
     */
    public function setFours($fours)
    {
        $this->fours = $fours;

        return $this;
    }

    /**
     * Get fours
     *
     * @return string
     */
    public function getFours()
    {
        return $this->fours;
    }

    /**
     * Set reportPartnerActivity
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $reportPartnerActivity
     *
     * @return QuadrupleContext
     */
    public function setReportPartnerActivity(\NKO\OrderBundle\Entity\BaseReport $reportPartnerActivity = null)
    {
        $this->reportPartnerActivity = $reportPartnerActivity;

        return $this;
    }

    /**
     * Get reportPartnerActivity
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getReportPartnerActivity()
    {
        return $this->reportPartnerActivity;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return QuadrupleContext
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
