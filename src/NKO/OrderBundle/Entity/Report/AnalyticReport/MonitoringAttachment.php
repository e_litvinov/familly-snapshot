<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/29/17
 * Time: 1:40 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 *
 * @ORM\Table(name="analytic_monitoring_attachment")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class MonitoringAttachment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    /**
     * @Assert\NotNull()
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement", inversedBy="attachments")
     * @ORM\JoinColumn(name="monitoring_element_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $monitoringElement;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $report;

    protected $_file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    const EXT = [
        'xls',
        'xlsx',
        'pdf',
        'doc',
        'docx'
    ];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return MonitoringAttachment
     */
    public function setFile($file)
    {
        if(!$this->_file && is_string($this->file)){
            $this->_file = $this->file;
        }
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
    

    /**
     * Set monitoringElement
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement $monitoringElement
     *
     * @return MonitoringAttachment
     */
    public function setMonitoringElement(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement $monitoringElement = null)
    {
        $this->monitoringElement = $monitoringElement;

        return $this;
    }

    /**
     * Get monitoringElement
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement
     */
    public function getMonitoringElement()
    {
        return $this->monitoringElement;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MonitoringAttachment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getLinkedElementText()
    {
        return $this->getMonitoringElement()->getText();
    }

    /**
     * @Assert\Callback
     */
    public function isValidAttachments(ExecutionContextInterface $context)
    {
        if(!$this->getFile() && !$this->_file) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('file')
                ->addViolation();
        }
        if($this->getFile()) {
            $this->validate(self::EXT, 'Please upload a valid PDF or Excel file', $context);
        }
    }

    private function validate($arrayExt, $message, $context) {
        $extension = pathinfo($this->getFile()->getClientOriginalName())['extension'];
        if(!in_array($extension, $arrayExt)) {
            $context->buildViolation($message)
                ->atPath('file')
                ->addViolation();
        }
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return MonitoringAttachment
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }
}
