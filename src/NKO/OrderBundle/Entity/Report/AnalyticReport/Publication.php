<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/13/17
 * Time: 4:19 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="analytic_publication")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class Publication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $indicator;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $plan;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\NotBlank(groups={"AnalyticReport-2019"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $fact;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="analyticPublications")
     * @ORM\JoinColumn(name="analytic_report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return Publication
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set plan
     *
     * @param integer $plan
     *
     * @return Publication
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return integer
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set fact
     *
     * @param integer $fact
     *
     * @return Publication
     */
    public function setFact($fact)
    {
        $this->fact = $fact;

        return $this;
    }

    /**
     * Get fact
     *
     * @return integer
     */
    public function getFact()
    {
        return $this->fact;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return Publication
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Publication
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
