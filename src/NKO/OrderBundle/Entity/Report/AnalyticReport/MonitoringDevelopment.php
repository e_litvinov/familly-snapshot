<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/8/17
 * Time: 5:16 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="monitoring_development")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class MonitoringDevelopment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Event", inversedBy="monitoringDevelopments")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $event;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $expectedResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="monitoringDevelopments")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->comment;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return MonitoringDevelopment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set expectedResult
     *
     * @param string $expectedResult
     *
     * @return MonitoringDevelopment
     */
    public function setExpectedResult($expectedResult)
    {
        $this->expectedResult = $expectedResult;

        return $this;
    }

    /**
     * Get expectedResult
     *
     * @return string
     */
    public function getExpectedResult()
    {
        return $this->expectedResult;
    }

    /**
     * Set event
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Event $event
     *
     * @return MonitoringDevelopment
     */
    public function setEvent(\NKO\OrderBundle\Entity\Report\AnalyticReport\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $report
     *
     * @return MonitoringDevelopment
     */
    public function setReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attachments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MonitoringDevelopment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
