<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/17/17
 * Time: 10:51 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\FieldGetter;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 *
 * @ORM\Table(name="feedback_tool")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF or Excel or Doc file",
 *     extensions={"doc", "excel", "pdf"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=false,
 *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
 * )
 */
class FeedbackTool
{
    use FieldGetter;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", inversedBy="tools")
     * @ORM\JoinColumn(name="feedback_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $feedback;

    protected $_file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return FeedbackTool
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set feedback
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $feedback
     *
     * @return FeedbackTool
     */
    public function setFeedback(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $feedback = null)
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * Get feedback
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FeedbackTool
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function fileExist()
    {
        if ($this->file || $this->_file) {
            return true;
        }

        return false;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
