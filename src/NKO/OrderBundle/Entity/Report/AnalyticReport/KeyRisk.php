<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/16/17
 * Time: 12:37 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="analytic_report_key_risk")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class KeyRisk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $keyRisk;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isRealized;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $act;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $result;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="keyRisks")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $planRisk;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $planAct;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyRisk
     *
     * @param string $keyRisk
     *
     * @return KeyRisk
     */
    public function setKeyRisk($keyRisk)
    {
        $this->keyRisk = $keyRisk;

        return $this;
    }

    /**
     * Get keyRisk
     *
     * @return string
     */
    public function getKeyRisk()
    {
        return $this->keyRisk;
    }

    /**
     * Set isRealized
     *
     * @param boolean $isRealized
     *
     * @return KeyRisk
     */
    public function setIsRealized($isRealized)
    {
        $this->isRealized = $isRealized;

        return $this;
    }

    /**
     * Get isRealized
     *
     * @return boolean
     */
    public function getIsRealized()
    {
        return $this->isRealized;
    }

    /**
     * Set act
     *
     * @param string $act
     *
     * @return KeyRisk
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }

    /**
     * Get act
     *
     * @return string
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return KeyRisk
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $report
     *
     * @return KeyRisk
     */
    public function setReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set planRisk
     *
     * @param string $planRisk
     *
     * @return KeyRisk
     */
    public function setPlanRisk($planRisk)
    {
        $this->planRisk = $planRisk;

        return $this;
    }

    /**
     * Get planRisk
     *
     * @return string
     */
    public function getPlanRisk()
    {
        return $this->planRisk;
    }

    /**
     * Set planAct
     *
     * @param string $planAct
     *
     * @return KeyRisk
     */
    public function setPlanAct($planAct)
    {
        $this->planAct = $planAct;

        return $this;
    }

    /**
     * Get planAct
     *
     * @return string
     */
    public function getPlanAct()
    {
        return $this->planAct;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return KeyRisk
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
