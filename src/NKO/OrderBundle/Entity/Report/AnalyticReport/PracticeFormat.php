<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/5/17
 * Time: 11:46 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="analytic_report_practice_format")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class PracticeFormat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="practiceFormat",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $directResults;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->directResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PracticeFormat
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PracticeFormat
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return PracticeFormat
     */
    public function addDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults[] = $directResult;

        return $this;
    }

    /**
     * Remove directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     */
    public function removeDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults->removeElement($directResult);
    }

    /**
     * Get directResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectResults()
    {
        return $this->directResults;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return PracticeFormat
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
