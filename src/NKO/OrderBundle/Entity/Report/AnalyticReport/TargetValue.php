<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/7/17
 * Time: 1:36 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="analytic_report_target_value")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class TargetValue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @var int
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $factValue;

    /**
     * @var int
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $planValue;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", inversedBy="targetValues")
     * @ORM\JoinColumn(name="direct_result_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $directResult;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return TargetValue
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set factValue
     *
     * @param integer $factValue
     *
     * @return TargetValue
     */
    public function setFactValue($factValue)
    {
        $this->factValue = $factValue;

        return $this;
    }

    /**
     * Get factValue
     *
     * @return integer
     */
    public function getFactValue()
    {
        return $this->factValue;
    }

    /**
     * Set planValue
     *
     * @param integer $planValue
     *
     * @return TargetValue
     */
    public function setPlanValue($planValue)
    {
        $this->planValue = $planValue;

        return $this;
    }

    /**
     * Get planValue
     *
     * @return integer
     */
    public function getPlanValue()
    {
        return $this->planValue;
    }

    /**
     * Set directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return TargetValue
     */
    public function setDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult = null)
    {
        $this->directResult = $directResult;

        return $this;
    }

    /**
     * Get directResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getDirectResult()
    {
        return $this->directResult;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return TargetValue
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
