<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/7/17
 * Time: 1:58 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PriorityDirection
 *
 * @ORM\Table(name="priority_direction_analytic")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class PriorityDirection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BaseReport", mappedBy="priorityDirection")
     */
    protected $analyticReports;

    /**
     * @var string
     *
     * @ORM\Column( type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->analyticReports = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PriorityDirection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PriorityDirection
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return PriorityDirection
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add analyticReport
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $analyticReport
     *
     * @return PriorityDirection
     */
    public function addAnalyticReport(\NKO\OrderBundle\Entity\BaseReport $analyticReport)
    {
        $this->analyticReports[] = $analyticReport;

        return $this;
    }

    /**
     * Remove analyticReport
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $analyticReport
     */
    public function removeAnalyticReport(\NKO\OrderBundle\Entity\BaseReport $analyticReport)
    {
        $this->analyticReports->removeElement($analyticReport);
    }

    /**
     * Get analyticReports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnalyticReports()
    {
        return $this->analyticReports;
    }
}
