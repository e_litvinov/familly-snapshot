<?php


namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BaseReport;

/**
 * @ORM\Entity()
 * @ORM\Table(name="normative_acts")
 */
class NormativeAct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="string")
     */
    protected $code;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinTable(name="normative_acts_reports")
     */
    protected $reports;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinTable(name="normative_acts_applications")
     */
    protected $applications;

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function addReport(BaseReport $report = null)
    {
        $this->reports[] = $report;

        return $this;
    }

    public function removeReport(BaseReport $report = null)
    {
        $this->reports->removeElement($report);

        return $this;
    }

    public function addApplication(BaseApplication $application = null)
    {
        $this->applications[] = $application;

        return $this;
    }

    public function removeApplication(BaseApplication $application = null)
    {
        $this->applications->removeElement($application);

        return $this;
    }
}
