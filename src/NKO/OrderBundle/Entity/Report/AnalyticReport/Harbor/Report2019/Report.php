<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Publication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Traits\AnalyticReport\AttachmentTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ImmediateResultsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ResourceTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_harbor_report_2019")
 */
class Report extends BaseReport
{
    use ProjectCardTrait;
    use OfficialsTrait;
    use ImmediateResultsTrait;
    use ResourceTrait;
    use AttachmentTrait;

    /**
     * @Assert\Count(
     *     min="1",
     *     minMessage="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct")
     */
    protected $normativeActs;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $practice;

    /**
     * @Assert\Valid()
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\TrainingGround")
     */
    protected $trainingGround;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $beneficiaryGroups;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid()
     */
    protected $territories;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceAnalyticResultCommentYear;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceAnalyticResultComment;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $expectedAnalyticResultCommentYear;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $expectedAnalyticResultComment;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectResume;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $realizationProblem;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $successStories;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $lessons;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Publication", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $publications;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\DirectResult", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $experience;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectExperience;

    /**
     * @var string
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $lastYearReportLink;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $commandConclusion;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $partnerConclusion;


    public function addNormativeAct(NormativeAct $normativeAct = null)
    {
        $this->normativeActs[] = $normativeAct;
        $normativeAct->addReport($this);

        return $this;
    }

    public function removeNormativeAct(NormativeAct $normativeAct = null)
    {
        $this->normativeActs->removeElement($normativeAct);
        $normativeAct->removeReport($this);

        return $this;
    }

    public function getNormativeActs()
    {
        return $this->normativeActs;
    }

    public function getPractice()
    {
        return $this->practice;
    }

    public function setPractice($practice)
    {
        $this->practice = $practice;

        return $this;
    }

    public function getTrainingGround()
    {
        return $this->trainingGround;
    }

    public function setTrainingGround($trainingGround)
    {
        $this->trainingGround = $trainingGround;

        return $this;
    }

    public function getBeneficiaryGroups()
    {
        return $this->beneficiaryGroups;
    }

    public function setBeneficiaryGroups($beneficiaryGroups)
    {
        $this->beneficiaryGroups = $beneficiaryGroups;

        return $this;
    }

    public function addTerritory(Territory $territory)
    {
        $this->territories[] = $territory;
        $territory->setReport($this);
        return $this;
    }

    public function removeTerritory(Territory $territory)
    {
        $this->territories->removeElement($territory);

        return $this;
    }

    public function getTerritories()
    {
        return $this->territories;
    }

    public function getPracticeAnalyticResultCommentYear()
    {
        return $this->practiceAnalyticResultCommentYear;
    }

    public function setPracticeAnalyticResultCommentYear($practiceAnalyticResultCommentYear)
    {
        $this->practiceAnalyticResultCommentYear = $practiceAnalyticResultCommentYear;

        return $this;
    }

    public function getPracticeAnalyticResultComment()
    {
        return $this->practiceAnalyticResultComment;
    }

    public function setPracticeAnalyticResultComment($practiceAnalyticResultComment)
    {
        $this->practiceAnalyticResultComment = $practiceAnalyticResultComment;

        return $this;
    }

    public function getExpectedAnalyticResultCommentYear()
    {
        return $this->expectedAnalyticResultCommentYear;
    }

    public function setExpectedAnalyticResultCommentYear($expectedAnalyticResultCommentYear)
    {
        $this->expectedAnalyticResultCommentYear = $expectedAnalyticResultCommentYear;

        return $this;
    }

    public function getExpectedAnalyticResultComment()
    {
        return $this->expectedAnalyticResultComment;
    }

    public function setExpectedAnalyticResultComment($expectedAnalyticResultComment)
    {
        $this->expectedAnalyticResultComment = $expectedAnalyticResultComment;

        return $this;
    }

    public function getRealizationProblem()
    {
        return $this->realizationProblem;
    }

    public function setRealizationProblem($realizationProblem)
    {
        $this->realizationProblem = $realizationProblem;

        return $this;
    }

    public function getProjectResume()
    {
        return $this->projectResume;
    }

    public function setProjectResume($projectResume)
    {
        $this->projectResume = $projectResume;

        return $this;
    }

    public function getSuccessStories()
    {
        return $this->successStories;
    }

    public function setSuccessStories($successStories)
    {
        $this->successStories = $successStories;

        return $this;
    }

    public function getLessons()
    {
        return $this->lessons;
    }

    public function setLessons($lessons)
    {
        $this->lessons = $lessons;

        return $this;
    }

    public function addPublication(Publication $publication)
    {
        $this->publications[] = $publication;
        $publication->setReport($this);

        return $this;
    }

    public function removePublication(Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    public function getPublications()
    {
        return $this->publications;
    }

    public function getExperience()
    {
        return $this->experience;
    }

    public function addExperience(DirectResult $experience)
    {
        $this->experience[] = $experience;
        $experience->setReport($this);

        return $this;
    }

    public function removeExperience(DirectResult $experience)
    {
        $this->experience->removeElement($experience);

        return $this;
    }

    public function getProjectExperience()
    {
        return $this->projectExperience;
    }

    public function setProjectExperience($projectExperience)
    {
        $this->projectExperience = $projectExperience;

        return $this;
    }

    public function getLastYearReportLink()
    {
        return $this->lastYearReportLink;
    }

    public function setLastYearReportLink($lastYearReportLink)
    {
        $this->lastYearReportLink = $lastYearReportLink;

        return $this;
    }

    public function getCommandConclusion()
    {
        return $this->commandConclusion;
    }

    public function setCommandConclusion($commandConclusion)
    {
        $this->commandConclusion = $commandConclusion;

        return $this;
    }

    public function getPartnerConclusion()
    {
        return $this->partnerConclusion;
    }

    public function setPartnerConclusion($partnerConclusion)
    {
        $this->partnerConclusion = $partnerConclusion;

        return $this;
    }
}
