<?php


namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="direct_result__analytic_harbor_report_2019")
 */
class DirectResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"HarborAnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $action;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"HarborAnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $targetGroup;

    /**
     * @Assert\NotNull(groups={"HarborAnalyticReport-2019"})
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat")
     */
    protected $practiceFormat;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"HarborAnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $result;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     */
    protected $report;

    public function getId()
    {
        return $this->id;
    }

    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    public function setPracticeFormat($practiceFormat)
    {
        $this->practiceFormat = $practiceFormat;

        return $this;
    }

    public function getPracticeFormat()
    {
        return $this->practiceFormat;
    }

    public function setResult($Result)
    {
        $this->result = $Result;

        return $this;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }
}
