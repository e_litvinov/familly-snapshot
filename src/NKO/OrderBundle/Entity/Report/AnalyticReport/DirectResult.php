<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/5/17
 * Time: 11:24 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * PriorityDirection
 *
 * @ORM\Table(name="analytic_report_direct_result")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class DirectResult
{
    use HumanizedProperties;

    const TARGET_VALUE_COUNT = 2;
    const EVENT_DATE_COUNT = 2;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "KNS-AnalyticReport-2018", "ContinuationApplication-2", "AnalyticReport-2019", "ContinuationSF18-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $service;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $targetGroup;

    /**
     * @Assert\NotBlank(groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod", inversedBy="directResults")
     * @ORM\JoinColumn(name="measurement_method_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $measurementMethod;

    /**
     * @Assert\Valid()
     * @Assert\Count(min="1", groups={"AnalyticReport-2019"}, minMessage="value is invalid(field must be non empty)")
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinTable(name="direct_result_measurement_method_relationship",
     *      joinColumns={@ORM\JoinColumn(name="direct_result_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")}
     *      )
     */
    protected $methods;

    /**
     * @ORM\Column(type="string", nullable=true, length=1024)
     */
    protected $customMethods;

    /**
     * @var bool
     *
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isFeedback;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, length=1024)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat", inversedBy="directResults")
     * @ORM\JoinColumn(name="practice_format_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $practiceFormat;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_practice_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportPracticeResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_expected_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $reportExpectedResult;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=1024)
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", mappedBy="practice",
     *     cascade={"persist"})
     */
    protected $practiceAttachments;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", mappedBy="spreadPractice",
     *     cascade={"persist"})
     */
    protected $spreadPracticeAttachments;

    /**
     * @var integer
     */
    protected $rowNumber;
    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startDateFact;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishDatePlan;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startDatePlan;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishDateFact;

    /**
     * @var int
     *
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "KNS-AnalyticReport-2018", "ContinuationApplication-2", "AnalyticReport-2019", "ContinuationSF18-2019", "HarborAnalyticReport-2019"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $serviceFactValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "KNS-AnalyticReport-2018", "ContinuationApplication-2", "AnalyticReport-2019", "ContinuationSF18-2019", "HarborAnalyticReport-2019"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $servicePlanValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(groups={"ContinuationApplication-2018"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $beneficiaryFactValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(groups={"ContinuationApplication-2018"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $beneficiaryPlanValue;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee", inversedBy="directResults")
     * @ORM\JoinColumn(name="grantee_id", referencedColumnName="id")
     */
    protected $grantee;


    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="application_expected_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $expectedResultApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="application_practice_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $practiceResultApplication;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isPublicEvent;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $locality;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem", inversedBy="directResults")
     * @ORM\JoinColumn(name="problem_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $problem;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult")
     * @ORM\JoinColumn(name="linked_result", referencedColumnName="id", nullable=true)
     */
    protected $linkedResult;

    /**
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="directResult")
     */
    private $linkedFarvaterResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report", inversedBy="practiceAnalyticIndividualResults")
     * @ORM\JoinColumn(name="practice_result_analytic_report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $practiceResultAnalyticReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report", inversedBy="expectedAnalyticIndividualResults")
     * @ORM\JoinColumn(name="expected_result_analytic_report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    private $expectedResultAnalyticReport;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"})
     *
     * @ORM\Column(name="indicator", type="text", nullable=true)
     *
     */
    private $indicator;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->methods = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->service;
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidDates(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getStartDateFact() || !$this->getFinishDateFact()) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('finishDateFact')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidMethods(ExecutionContextInterface $context, $payload)
    {
        if ($this->getMethods()->isEmpty()) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('methods')
                ->addViolation();
        }
    }

    public function getHumanizedMethods()
    {
        return $this->getHumanizedCollections($this->getMethods());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set services
     *
     * @param string $service
     *
     * @return DirectResult
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set isFeedback
     *
     * @param boolean $isFeedback
     *
     * @return DirectResult
     */
    public function setIsFeedback($isFeedback)
    {
        $this->isFeedback = $isFeedback;

        return $this;
    }

    /**
     * Get isFeedback
     *
     * @return boolean
     */
    public function getIsFeedback()
    {
        return $this->isFeedback;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return DirectResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     *
     * @return DirectResult
     */
    public function setMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod = null)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set practiceFormat
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat $practiceFormat
     *
     * @return DirectResult
     */
    public function setPracticeFormat(\NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat $practiceFormat = null)
    {
        $this->practiceFormat = $practiceFormat;

        return $this;
    }

    /**
     * Get practiceFormat
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat
     */
    public function getPracticeFormat()
    {
        return $this->practiceFormat;
    }

    /**
     * Set reportPracticeResult
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $reportPracticeResult
     *
     * @return DirectResult
     */
    public function setReportPracticeResult(\NKO\OrderBundle\Entity\BaseReport $reportPracticeResult = null)
    {
        $this->reportPracticeResult = $reportPracticeResult;

        return $this;
    }

    /**
     * Get reportPracticeResult
     *
     * @return \NKO\OrderBundle\Entity\BaseReport
     */
    public function getReportPracticeResult()
    {
        return $this->reportPracticeResult;
    }

    /**
     * Set reportExpectedResult
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $reportExpectedResult
     *
     * @return DirectResult
     */
    public function setReportExpectedResult(\NKO\OrderBundle\Entity\BaseReport $reportExpectedResult = null)
    {
        $this->reportExpectedResult = $reportExpectedResult;

        return $this;
    }

    /**
     * Get reportExpectedResult
     *
     * @return \NKO\OrderBundle\Entity\BaseReport
     */
    public function getReportExpectedResult()
    {
        return $this->reportExpectedResult;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DirectResult
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add practiceAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceAttachment
     *
     * @return DirectResult
     */
    public function addPracticeAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceAttachment)
    {
        $this->practiceAttachments[] = $practiceAttachment;
        $practiceAttachment->setPractice($this);

        return $this;
    }

    /**
     * Remove practiceAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceAttachment
     */
    public function removePracticeAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceAttachment)
    {
        $this->practiceAttachments->removeElement($practiceAttachment);
    }

    /**
     * Get practiceAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeAttachments()
    {
        return $this->practiceAttachments;
    }

    /**
     * Add spreadPracticeAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $spreadPracticeAttachment
     *
     * @return DirectResult
     */
    public function addSpreadPracticeAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $spreadPracticeAttachment)
    {
        $this->spreadPracticeAttachments[] = $spreadPracticeAttachment;
        $spreadPracticeAttachment->setSpreadPractice($this);

        return $this;
    }

    /**
     * Remove spreadPracticeAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $spreadPracticeAttachment
     */
    public function removeSpreadPracticeAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $spreadPracticeAttachment)
    {
        $this->spreadPracticeAttachments->removeElement($spreadPracticeAttachment);
    }

    /**
     * Get spreadPracticeAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpreadPracticeAttachments()
    {
        return $this->spreadPracticeAttachments;
    }

    /**
     * @return int
     */
    public function getRowNumber()
    {
        return $this->rowNumber;
    }

    /**
     * @param int $rowNumber
     */
    public function setRowNumber($rowNumber)
    {
        $this->rowNumber = $rowNumber;
    }


    /**
     * Set grantee
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee $grantee
     *
     * @return Report
     */
    public function setGrantee(\NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee $grantee = null)
    {
        $this->grantee = $grantee;

        return $this;
    }

    /**
     * Get grantee
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee
     */
    public function getGrantee()
    {
        return $this->grantee;
    }

    /**
     * Set startDateFact
     *
     * @param \DateTime $startDateFact
     *
     * @return DirectResult
     */
    public function setStartDateFact($startDateFact)
    {
        $this->startDateFact = $startDateFact;

        return $this;
    }

    /**
     * Get startDateFact
     *
     * @return \DateTime
     */
    public function getStartDateFact()
    {
        return $this->startDateFact;
    }

    /**
     * Set finishDatePlan
     *
     * @param \DateTime $finishDatePlan
     *
     * @return DirectResult
     */
    public function setFinishDatePlan($finishDatePlan)
    {
        $this->finishDatePlan = $finishDatePlan;

        return $this;
    }

    /**
     * Get finishDatePlan
     *
     * @return \DateTime
     */
    public function getFinishDatePlan()
    {
        return $this->finishDatePlan;
    }

    /**
     * Set startDatePlan
     *
     * @param \DateTime $startDatePlan
     *
     * @return DirectResult
     */
    public function setStartDatePlan($startDatePlan)
    {
        $this->startDatePlan = $startDatePlan;

        return $this;
    }

    /**
     * Get startDatePlan
     *
     * @return \DateTime
     */
    public function getStartDatePlan()
    {
        return $this->startDatePlan;
    }

    /**
     * Set finishDateFact
     *
     * @param \DateTime $finishDateFact
     *
     * @return DirectResult
     */
    public function setFinishDateFact($finishDateFact)
    {
        $this->finishDateFact = $finishDateFact;

        return $this;
    }

    /**
     * Get finishDateFact
     *
     * @return \DateTime
     */
    public function getFinishDateFact()
    {
        return $this->finishDateFact;
    }

    /**
     * Set serviceFactValue
     *
     * @param integer $serviceFactValue
     *
     * @return DirectResult
     */
    public function setServiceFactValue($serviceFactValue)
    {
        $this->serviceFactValue = $serviceFactValue;

        return $this;
    }

    /**
     * Get serviceFactValue
     *
     * @return integer
     */
    public function getServiceFactValue()
    {
        return $this->serviceFactValue;
    }

    /**
     * Set servicePlanValue
     *
     * @param integer $servicePlanValue
     *
     * @return DirectResult
     */
    public function setServicePlanValue($servicePlanValue)
    {
        $this->servicePlanValue = $servicePlanValue;

        return $this;
    }

    /**
     * Get servicePlanValue
     *
     * @return integer
     */
    public function getServicePlanValue()
    {
        return $this->servicePlanValue;
    }

    /**
     * Set beneficiaryFactValue
     *
     * @param integer $beneficiaryFactValue
     *
     * @return DirectResult
     */
    public function setBeneficiaryFactValue($beneficiaryFactValue)
    {
        $this->beneficiaryFactValue = $beneficiaryFactValue;

        return $this;
    }

    /**
     * Get beneficiaryFactValue
     *
     * @return integer
     */
    public function getBeneficiaryFactValue()
    {
        return $this->beneficiaryFactValue;
    }

    /**
     * Set beneficiaryPlanValue
     *
     * @param integer $beneficiaryPlanValue
     *
     * @return DirectResult
     */
    public function setBeneficiaryPlanValue($beneficiaryPlanValue)
    {
        $this->beneficiaryPlanValue = $beneficiaryPlanValue;

        return $this;
    }

    /**
     * Get beneficiaryPlanValue
     *
     * @return integer
     */
    public function getBeneficiaryPlanValue()
    {
        return $this->beneficiaryPlanValue;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return DirectResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set isPublicEvent
     *
     * @param boolean $isPublicEvent
     *
     * @return DirectResult
     */
    public function setIsPublicEvent($isPublicEvent)
    {
        $this->isPublicEvent = $isPublicEvent;

        return $this;
    }

    /**
     * Get isPublicEvent
     *
     * @return boolean
     */
    public function getIsPublicEvent()
    {
        return $this->isPublicEvent;
    }

    /**
     * Set locality
     *
     * @param string $locality
     *
     * @return DirectResult
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set expectedResultApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $expectedResultApplication
     *
     * @return DirectResult
     */
    public function setExpectedResultApplication(\NKO\OrderBundle\Entity\BaseApplication $expectedResultApplication = null)
    {
        $this->expectedResultApplication = $expectedResultApplication;

        return $this;
    }

    /**
     * Get expectedResultApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getExpectedResultApplication()
    {
        return $this->expectedResultApplication;
    }

    /**
     * Set practiceResultApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $practiceResultApplication
     *
     * @return DirectResult
     */
    public function setPracticeResultApplication(\NKO\OrderBundle\Entity\BaseApplication $practiceResultApplication = null)
    {
        $this->practiceResultApplication = $practiceResultApplication;

        return $this;
    }

    /**
     * Get practiceResultApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getPracticeResultApplication()
    {
        return $this->practiceResultApplication;
    }

    /**
     * Add method
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method
     *
     * @return DirectResult
     */
    public function addMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods[] = $method;

        return $this;
    }

    /**
     * Remove method
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method
     */
    public function removeMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods->removeElement($method);
    }

    /**
     * Get methods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * Set problem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $problem
     *
     * @return DirectResult
     */
    public function setProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $problem = null)
    {
        $this->problem = $problem;

        return $this;
    }

    /**
     * Get problem
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem
     */
    public function getProblem()
    {
        return $this->problem;
    }

    /**
     * Set targetGroup
     *
     * @param string $targetGroup
     *
     * @return DirectResult
     */
    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return string
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    /**
     * Set linkedResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $linkedResult
     *
     * @return DirectResult
     */
    public function setLinkedResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $linkedResult = null)
    {
        $this->linkedResult = $linkedResult;

        return $this;
    }

    /**
     * Get linkedResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getLinkedResult()
    {
        return $this->linkedResult;
    }

    public function getRelatedService()
    {
        if ($this->getLinkedResult()) {
            return $this->getLinkedResult()->getService();
        } elseif ($this->getLinkedFarvaterResult()) {
            return $this->getLinkedFarvaterResult()->getService();
        } else {
            return $this->getService();
        }
    }

    /**
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getPracticeResultAnalyticReport()
    {
        return $this->practiceResultAnalyticReport;
    }

    /**
     * @param BaseReport $resultAnalyticReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function setPracticeResultAnalyticReport(BaseReport $resultAnalyticReport)
    {
        $this->practiceResultAnalyticReport = $resultAnalyticReport;
        
        return $this;
    }
    
    /**
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getExpectedResultAnalyticReport()
    {
        return $this->expectedResultAnalyticReport;
    }

    /**
     * @param BaseReport $resultAnalyticReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function setExpectedResultAnalyticReport(BaseReport $resultAnalyticReport)
    {
        $this->expectedResultAnalyticReport = $resultAnalyticReport;
        
        return $this;
    }

    /**
     * Set linkedFarvaterResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $linkedFarvaterResult
     *
     * @return DirectResult
     */
    public function setLinkedFarvaterResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $linkedFarvaterResult)
    {
        $this->linkedFarvaterResult = $linkedFarvaterResult;
        $linkedFarvaterResult->setDirectResult($this);

        return $this;
    }

    /**
     * Get linkedFarvaterResult
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result
     */
    public function getLinkedFarvaterResult()
    {
        return $this->linkedFarvaterResult;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return DirectResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidProblem(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getProblem()) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('problem')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2", "AnalyticReport-2019", "ContinuationSF18-2019"})
     */
    public function isValidPracticeFormat(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getPracticeFormat() && !$this->getExpectedResultApplication()) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('practiceFormat')
                ->addViolation();
        }
    }

    public function getCustomMethods()
    {
        return $this->customMethods;
    }

    public function setCustomMethods($customMethods)
    {
        $this->customMethods = $customMethods;

        return $this;
    }
}
