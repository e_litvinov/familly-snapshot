<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 16:15
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Traits\PreviousReportTrait;

/**
 * Report
 *
 * @ORM\Table(name="analytic_prev_report")
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class PrevReport extends Report
{
    use PreviousReportTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Set directorName
     *
     * @param string $directorName
     *
     * @return PrevReport
     */
    public function setDirectorName($directorName)
    {
        $this->directorName = $directorName;

        return $this;
    }

    /**
     * Get directorName
     *
     * @return string
     */
    public function getDirectorName()
    {
        return $this->directorName;
    }

    /**
     * Set accountantName
     *
     * @param string $accountantName
     *
     * @return PrevReport
     */
    public function setAccountantName($accountantName)
    {
        $this->accountantName = $accountantName;

        return $this;
    }

    /**
     * Get accountantName
     *
     * @return string
     */
    public function getAccountantName()
    {
        return $this->accountantName;
    }

    /**
     * Set directorPosition
     *
     * @param string $directorPosition
     *
     * @return PrevReport
     */
    public function setDirectorPosition($directorPosition)
    {
        $this->directorPosition = $directorPosition;

        return $this;
    }

    /**
     * Get directorPosition
     *
     * @return string
     */
    public function getDirectorPosition()
    {
        return $this->directorPosition;
    }

    /**
     * Set accountantPosition
     *
     * @param string $accountantPosition
     *
     * @return PrevReport
     */
    public function setAccountantPosition($accountantPosition)
    {
        $this->accountantPosition = $accountantPosition;

        return $this;
    }

    /**
     * Get accountantPosition
     *
     * @return string
     */
    public function getAccountantPosition()
    {
        return $this->accountantPosition;
    }

    /**
     * Set finishDateProject
     *
     * @param \DateTime $finishDateProject
     *
     * @return PrevReport
     */
    public function setFinishDateProject($finishDateProject)
    {
        $this->finishDateProject = $finishDateProject;

        return $this;
    }

    /**
     * Get finishDateProject
     *
     * @return \DateTime
     */
    public function getFinishDateProject()
    {
        return $this->finishDateProject;
    }
}
