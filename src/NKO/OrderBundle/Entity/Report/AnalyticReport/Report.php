<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/7/17
 * Time: 12:22 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\AnalyticReport\AnalyticReportTrait;
use NKO\OrderBundle\Traits\AnalyticReport\AttachmentTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ImmediateResultsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\MonitoringTrait;
use NKO\OrderBundle\Traits\AnalyticReport\PriorityDirectionTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\PublicationLoaderTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ResourceTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="analytic_report")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "analytic_prev" = "NKO\OrderBundle\Entity\Report\AnalyticReport\PrevReport",
 *     })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Report extends BaseReport
{
    use AnalyticReportTrait;
    use ProjectCardTrait;
    use PriorityDirectionTrait;
    use MonitoringTrait;
    use ResourceTrait;
    use AttachmentTrait;
    use PublicationLoaderTrait;
    use ImmediateResultsTrait;
    use OfficialsTrait;

     /**
      * @var int
      *
      * @ORM\Column(name="id", type="integer")
      * @ORM\Id
      * @ORM\GeneratedValue(strategy="AUTO")
      */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringAttachment", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringAttachments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->targetGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->analyticPublications = new ArrayCollection();
        $this->monitoringDevelopments = new ArrayCollection();
        $this->practiceAnalyticResults = new ArrayCollection();
        $this->socialMeasures = new ArrayCollection();
        $this->nextSocialMeasures = new ArrayCollection();
        $this->expectedAnalyticResults = new ArrayCollection();
        $this->humanResources = new ArrayCollection();
        $this->keyRisks = new ArrayCollection();
        $this->practiceFeedbackAttachments = new ArrayCollection();
        $this->practiceSpreadFeedbackAttachments = new ArrayCollection();
        $this->practiceSuccessStoryAttachments = new ArrayCollection();
        $this->practiceSpreadSuccessStoryAttachments = new ArrayCollection();
        $this->publicationAttachments = new ArrayCollection();
        $this->materialAttachments = new ArrayCollection();
        $this->factorAttachments = new ArrayCollection();
        $this->monitoringAttachments = new ArrayCollection();
    }

    /**
     * Add monitoringAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringAttachment $monitoringAttachment
     *
     * @return BaseReport
     */
    public function addMonitoringAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringAttachment $monitoringAttachment)
    {
        $this->monitoringAttachments[] = $monitoringAttachment;
        $monitoringAttachment->setReport($this);

        return $this;
    }

    /**
     * Remove monitoringAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringAttachment $monitoringAttachment
     */
    public function removeMonitoringAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringAttachment $monitoringAttachment)
    {
        $this->monitoringAttachments->removeElement($monitoringAttachment);
    }

    /**
     * Get monitoringAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringAttachments()
    {
        return $this->monitoringAttachments;
    }
}
