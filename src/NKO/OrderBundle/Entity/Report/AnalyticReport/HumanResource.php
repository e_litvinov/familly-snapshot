<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/16/17
 * Time: 1:34 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="analytic_human_resource")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class HumanResource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * @Assert\NotBlank(message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\GreaterThanOrEqual(
     *     value=0,
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $executorsCount;

    /**
     * @var integer
     * @Assert\NotBlank(message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\GreaterThanOrEqual(
     *     value=0,
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $substitutionsCount;

    /**
     * @var integer
     * @Assert\NotBlank(message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\GreaterThanOrEqual(
     *     value=0,
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $ratesCount;

    /**
     * @var integer
     * @Assert\NotBlank(message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\GreaterThanOrEqual(
     *     value=0,
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $individualJobSpecialistCount;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *      groups={"AnalyticReport-2018", "AnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="humanResources")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set executorsCount
     *
     * @param integer $executorsCount
     *
     * @return HumanResource
     */
    public function setExecutorsCount($executorsCount)
    {
        $this->executorsCount = $executorsCount;

        return $this;
    }

    /**
     * Get executorsCount
     *
     * @return integer
     */
    public function getExecutorsCount()
    {
        return $this->executorsCount;
    }

    /**
     * Set substitutionsCount
     *
     * @param integer $substitutionsCount
     *
     * @return HumanResource
     */
    public function setSubstitutionsCount($substitutionsCount)
    {
        $this->substitutionsCount = $substitutionsCount;

        return $this;
    }

    /**
     * Get substitutionsCount
     *
     * @return integer
     */
    public function getSubstitutionsCount()
    {
        return $this->substitutionsCount;
    }

    /**
     * Set ratesCount
     *
     * @param integer $ratesCount
     *
     * @return HumanResource
     */
    public function setRatesCount($ratesCount)
    {
        $this->ratesCount = $ratesCount;

        return $this;
    }

    /**
     * Get ratesCount
     *
     * @return integer
     */
    public function getRatesCount()
    {
        return $this->ratesCount;
    }

    /**
     * Set individualJobSpecialistCount
     *
     * @param integer $individualJobSpecialistCount
     *
     * @return HumanResource
     */
    public function setIndividualJobSpecialistCount($individualJobSpecialistCount)
    {
        $this->individualJobSpecialistCount = $individualJobSpecialistCount;

        return $this;
    }

    /**
     * Get individualJobSpecialistCount
     *
     * @return integer
     */
    public function getIndividualJobSpecialistCount()
    {
        return $this->individualJobSpecialistCount;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return HumanResource
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $report
     *
     * @return HumanResource
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return HumanResource
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
