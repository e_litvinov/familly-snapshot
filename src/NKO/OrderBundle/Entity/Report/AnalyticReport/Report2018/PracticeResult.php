<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_report_2018_practice_result")
 */
class PracticeResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $purpose;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $unplannedImmediateResults;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceChanges;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $unplannedSocialResults;

    /**
     * @Assert\NotBlank(groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $successStories;

    /**
     * @Assert\NotBlank(groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $difficulties;

    /**
     * @Assert\NotBlank(groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $lessons;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @Assert\Callback(
     *     groups={"AnalyticReport-2018", "AnalyticReport-2019"}
     *     )
     */
    public function isValidPracticeChanges(ExecutionContextInterface $context, $payload)
    {
        if ($context->getPropertyPath() != 'data.practiceIntroduction' && !$this->getPracticeChanges()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('practiceChanges')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return PracticeResult
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set unplannedImmediateResults
     *
     * @param string $unplannedImmediateResults
     *
     * @return PracticeResult
     */
    public function setUnplannedImmediateResults($unplannedImmediateResults)
    {
        $this->unplannedImmediateResults = $unplannedImmediateResults;

        return $this;
    }

    /**
     * Get unplannedImmediateResults
     *
     * @return string
     */
    public function getUnplannedImmediateResults()
    {
        return $this->unplannedImmediateResults;
    }

    /**
     * Set practiceChanges
     *
     * @param string $practiceChanges
     *
     * @return PracticeResult
     */
    public function setPracticeChanges($practiceChanges)
    {
        $this->practiceChanges = $practiceChanges;

        return $this;
    }

    /**
     * Get practiceChanges
     *
     * @return string
     */
    public function getPracticeChanges()
    {
        return $this->practiceChanges;
    }

    /**
     * Set unplannedSocialResults
     *
     * @param string $unplannedSocialResults
     *
     * @return PracticeResult
     */
    public function setUnplannedSocialResults($unplannedSocialResults)
    {
        $this->unplannedSocialResults = $unplannedSocialResults;

        return $this;
    }

    /**
     * Get unplannedSocialResults
     *
     * @return string
     */
    public function getUnplannedSocialResults()
    {
        return $this->unplannedSocialResults;
    }

    /**
     * Set successStories
     *
     * @param string $successStories
     *
     * @return PracticeResult
     */
    public function setSuccessStories($successStories)
    {
        $this->successStories = $successStories;

        return $this;
    }

    /**
     * Get successStories
     *
     * @return string
     */
    public function getSuccessStories()
    {
        return $this->successStories;
    }

    /**
     * Set difficulties
     *
     * @param string $difficulties
     *
     * @return PracticeResult
     */
    public function setDifficulties($difficulties)
    {
        $this->difficulties = $difficulties;

        return $this;
    }

    /**
     * Get difficulties
     *
     * @return string
     */
    public function getDifficulties()
    {
        return $this->difficulties;
    }

    /**
     * Set lessons
     *
     * @param string $lessons
     *
     * @return PracticeResult
     */
    public function setLessons($lessons)
    {
        $this->lessons = $lessons;

        return $this;
    }

    /**
     * Get lessons
     *
     * @return string
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return PracticeResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
