<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Traits\AnalyticReport\AttachmentTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ImmediateResultsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\MonitoringTrait;
use NKO\OrderBundle\Traits\AnalyticReport\PriorityDirectionTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\PublicationLoaderTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Traits\AnalyticReport\ResourceTrait;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\AnalyticReport\Report2018\ReportRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_report_2018")
 */
class Report extends BaseReport
{
    use ProjectCardTrait;
    use PriorityDirectionTrait;
    use ResourceTrait;
    use MonitoringTrait;
    use AttachmentTrait;
    use PublicationLoaderTrait;
    use ImmediateResultsTrait;
    use OfficialsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $territories;

    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult")
     * @ORM\JoinColumn(name="practice_result_implementation_id", referencedColumnName="id", nullable=true)
     */
    protected $practiceImplementation;

    /**
     * @Assert\Valid
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult")
     * @ORM\JoinColumn(name="practice_result_introduction_id", referencedColumnName="id", nullable=true)
     */
    protected $practiceIntroduction;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Publication", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $analyticPublications;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult", mappedBy="reportImplementation",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $implementationSocialResults;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult", mappedBy="reportIntroduction",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $introductionSocialResults;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment", mappedBy="reportMonitoring",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringDevelopmentAttachments;

    /**
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="practiceResultAnalyticReport")
     */
    protected $practiceAnalyticIndividualResults;
    /**
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="expectedResultAnalyticReport")
     */
    protected $expectedAnalyticIndividualResults;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add territory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory
     *
     * @return Report
     */
    public function addTerritory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory)
    {
        $this->territories[] = $territory;
        $territory->setReport($this);
        return $this;
    }

    /**
     * Remove territory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory
     */
    public function removeTerritory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory)
    {
        $this->territories->removeElement($territory);
    }

    /**
     * Get territories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerritories()
    {
        return $this->territories;
    }

    /**
     * Set practiceImplementation
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult $practiceImplementation
     *
     * @return Report
     */
    public function setPracticeImplementation(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult $practiceImplementation = null)
    {
        $this->practiceImplementation = $practiceImplementation;

        return $this;
    }

    /**
     * Get practiceImplementation
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult
     */
    public function getPracticeImplementation()
    {
        return $this->practiceImplementation;
    }

    /**
     * Set practiceIntroduction
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult $practiceIntroduction
     *
     * @return Report
     */
    public function setPracticeIntroduction(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult $practiceIntroduction = null)
    {
        $this->practiceIntroduction = $practiceIntroduction;

        return $this;
    }

    /**
     * Get practiceIntroduction
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult
     */
    public function getPracticeIntroduction()
    {
        return $this->practiceIntroduction;
    }

    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $analyticPublication
     *
     * @return Report
     */
    public function addAnalyticPublication(\NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $publication)
    {
        $this->analyticPublications[] = $publication;
        $publication->setReport($this);

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $analyticPublication
     */
    public function removeAnalyticPublication(\NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $publication)
    {
        $this->analyticPublications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnalyticPublications()
    {
        return $this->analyticPublications;
    }

    /**
     * Add implementationSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $implementationSocialResult
     *
     * @return Report
     */
    public function addImplementationSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $implementationSocialResult)
    {
        $this->implementationSocialResults[] = $implementationSocialResult;
        $implementationSocialResult -> setReportImplementation($this);
        return $this;
    }

    /**
     * Remove implementationSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $implementationSocialResult
     */
    public function removeImplementationSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $implementationSocialResult)
    {
        $this->implementationSocialResults->removeElement($implementationSocialResult);
    }

    /**
     * Get implementationSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImplementationSocialResults()
    {
        return $this->implementationSocialResults;
    }

    /**
     * Add introductionSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $introductionSocialResult
     *
     * @return Report
     */
    public function addIntroductionSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $introductionSocialResult)
    {
        $this->introductionSocialResults[] = $introductionSocialResult;
        $introductionSocialResult -> setReportIntroduction($this);
        return $this;
    }

    /**
     * Remove introductionSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $introductionSocialResult
     */
    public function removeIntroductionSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult $introductionSocialResult)
    {
        $this->introductionSocialResults->removeElement($introductionSocialResult);
    }

    /**
     * Get introductionSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntroductionSocialResults()
    {
        return $this->introductionSocialResults;
    }

    /**
     * Add monitoringAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $monitoringAttachment
     *
     * @return BaseReport
     */
    public function addMonitoringDevelopmentAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $monitoringAttachment)
    {
        $this->monitoringDevelopmentAttachments[] = $monitoringAttachment;
        $monitoringAttachment->setReportMonitoring($this);

        return $this;
    }

    /**
     * Remove monitoringAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $monitoringAttachment
     */
    public function removeMonitoringDevelopmentAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $monitoringAttachment)
    {
        $this->monitoringDevelopmentAttachments->removeElement($monitoringAttachment);
    }

    /**
     * Get monitoringDevelopmentAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringDevelopmentAttachments()
    {
        return $this->monitoringDevelopmentAttachments;
    }

    /**
     * Add practiceAnalyticIndividualResult
     *
     * @param DirectResult $individualResult
     *
     * @return BaseReport
     */
    public function addPracticeAnalyticIndividualResult(DirectResult $individualResult)
    {
        $this->practiceAnalyticIndividualResults[] = $individualResult;
        $individualResult->setPracticeResultAnalyticReport($this);

        return $this;
    }

    /**
     * Remove practiceAnalyticIndividualResult
     *
     * @param DirectResult $individualResult
     */
    public function removePracticeAnalyticIndividualResult(DirectResult $individualResult)
    {
        $this->practiceAnalyticIndividualResults->removeElement($individualResult);
    }

    /**
     * @return mixed
     */
    public function getPracticeAnalyticIndividualResults()
    {
        return $this->practiceAnalyticIndividualResults;
    }

    /**
     * set practiceAnalyticIndividualResults
     *
     * @return Report
     */
    public function setPracticeAnalyticIndividualResults($analyticResults)
    {
        foreach ($analyticResults as $analyticResult) {
            $this->addPracticeAnalyticIndividualResult($analyticResult);
        }
        return $this;
    }

    /**
     * Add expectedAnalyticIndividualResult
     *
     * @param DirectResult $individualResult
     *
     * @return BaseReport
     */
    public function addExpectedAnalyticIndividualResult(DirectResult $individualResult)
    {
        $this->expectedAnalyticIndividualResults[] = $individualResult;
        $individualResult->setExpectedResultAnalyticReport($this);

        return $this;
    }

    /**
     * Remove expectedAnalyticIndividualResult
     *
     * @param DirectResult $individualResult
     */
    public function removeExpectedAnalyticIndividualResult(DirectResult $individualResult)
    {
        $this->expectedAnalyticIndividualResults->removeElement($individualResult);
    }

    /**
     * @return mixed
     */
    public function getExpectedAnalyticIndividualResults()
    {
        return $this->expectedAnalyticIndividualResults;
    }

    /**
     * set expectedAnalyticIndividualResults
     *
     * @return Report
     */
    public function setExpectedAnalyticIndividualResults($analyticResults)
    {
        foreach ($analyticResults as $analyticResult) {
            $this->addExpectedAnalyticIndividualResult($analyticResult);
        }
        return $this;
    }
}
