<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_report_2018_social_result")
 */
class SocialResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\TransferResult")
     * @ORM\JoinColumn(name="transfer_result_id", referencedColumnName="id")
     */
    private $result;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="implementationSocialResults")
     * @ORM\JoinColumn(name="report_implementation_id", referencedColumnName="id")
     */
    private $reportImplementation;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="introductionSocialResults")
     * @ORM\JoinColumn(name="report_introduction_id", referencedColumnName="id")
     */
    private $reportIntroduction;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return SocialResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set result
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\TransferResult $result
     *
     * @return SocialResult
     */
    public function setResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\TransferResult $result = null)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\TransferResult
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set reportImplementation
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report $reportImplementation
     *
     * @return SocialResult
     */
    public function setReportImplementation(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report $reportImplementation = null)
    {
        $this->reportImplementation = $reportImplementation;

        return $this;
    }

    /**
     * Get reportImplementation
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report
     */
    public function getReportImplementation()
    {
        return $this->reportImplementation;
    }

    /**
     * Set reportIntroduction
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report $reportIntroduction
     *
     * @return SocialResult
     */
    public function setReportIntroduction(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report $reportIntroduction = null)
    {
        $this->reportIntroduction = $reportIntroduction;

        return $this;
    }

    /**
     * Get reportIntroduction
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report
     */
    public function getReportIntroduction()
    {
        return $this->reportIntroduction;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return SocialResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
