<?php

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_report_2018_territory")
 */
class Territory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Region")
     * @Assert\NotNull(groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"})
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $region;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $locality;

    /**
     * @Assert\NotNull(groups={"AnalyticReport-2018"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="territories")
     * @ORM\JoinColumn(name="analytic_report_id", referencedColumnName="id")
     */
    private $report;

    /**
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="territories")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;
    
    /**
     * Set region
     *
     * @param \NKO\OrderBundle\Entity\Region $region
     *
     * @return Territory
     */
    public function setRegion(\NKO\OrderBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \NKO\OrderBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locality
     *
     * @param string $locality
     *
     * @return Territory
     */
    public function setLocality($locality)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return Territory
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $report
     *
     * @return Territory
     */
    public function setApplication(\NKO\OrderBundle\Entity\BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Territory
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
