<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 2.8.18
 * Time: 10.47
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Traits\AnalyticReport\AttachmentTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ImmediateResultsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ResourceTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="analytic_report_KNS_2018")
 */
class Report extends BaseReport
{
    use ProjectCardTrait;
    use ResourceTrait;
    use ImmediateResultsTrait;
    use AttachmentTrait;
    use OfficialsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="traineeshipPractice", type="text", nullable=true)
     */
    private $traineeshipPractice;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="practiceIntroduction", type="text", nullable=true)
     */
    private $practiceIntroduction;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="traineeshipSupport", type="text", nullable=true)
     */
    private $traineeshipSupport;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="successStories", type="text", nullable=true)
     */
    private $successStories;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="realizationProblem", type="text", nullable=true)
     */
    private $realizationProblem;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="lessons", type="text", nullable=true)
     */
    private $lessons;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="realizationExperience", type="text", nullable=true)
     */
    private $realizationExperience;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018"}
     *     )
     *
     * @ORM\Column(name="projectExperience", type="text", nullable=true)
     */
    private $projectExperience;

    /**
     * @Assert\Valid()
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns_analytic_report_2018_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="Publication", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $publications;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid()
     */
    protected $territories;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTraineeshipPractice()
    {
        return $this->traineeshipPractice;
    }

    /**
     * @param string $traineeshipPractice
     *
     * @return Report
     */
    public function setTraineeshipPractice($traineeshipPractice)
    {
        $this->traineeshipPractice = $traineeshipPractice;

        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeIntroduction()
    {
        return $this->practiceIntroduction;
    }

    /**
     * @param string $practiceIntroduction
     *
     * @return Report
     */
    public function setPracticeIntroduction($practiceIntroduction)
    {
        $this->practiceIntroduction = $practiceIntroduction;

        return $this;
    }

    /**
     * @return string
     */
    public function getTraineeshipSupport()
    {
        return $this->traineeshipSupport;
    }

    /**
     * @param string $traineeshipSupport
     *
     * @return Report
     */
    public function setTraineeshipSupport($traineeshipSupport)
    {
        $this->traineeshipSupport = $traineeshipSupport;

        return $this;
    }

    /**
     * @return string
     */
    public function getSuccessStories()
    {
        return $this->successStories;
    }

    /**
     * @param string $successStories
     *
     * @return Report
     */
    public function setSuccessStories($successStories)
    {
        $this->successStories = $successStories;

        return $this;
    }

    /**
     * @return string
     */
    public function getRealizationProblem()
    {
        return $this->realizationProblem;
    }

    /**
     * @param string $realizationProblem
     *
     * @return Report
     */
    public function setRealizationProblem($realizationProblem)
    {
        $this->realizationProblem = $realizationProblem;

        return $this;
    }

    /**
     * @return string
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * @param string $lessons
     *
     * @return Report
     */
    public function setLessons($lessons)
    {
        $this->lessons = $lessons;

        return $this;
    }

    /**
     * @return string
     */
    public function getRealizationExperience()
    {
        return $this->realizationExperience;
    }

    /**
     * @param string $realizationExperience
     *
     * @return Report
     */
    public function setRealizationExperience($realizationExperience)
    {
        $this->realizationExperience = $realizationExperience;

        return $this;
    }

    /**
     * @return string
     */
    public function getProjectExperience()
    {
        return $this->projectExperience;
    }

    /**
     * @param string $projectExperience
     *
     * @return Report
     */
    public function setProjectExperience($projectExperience)
    {
        $this->projectExperience = $projectExperience;

        return $this;
    }


    /**
     * Add trainingGround
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround
     *
     * @return Report
     */
    public function addTrainingGround(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround)
    {
        $this->trainingGrounds[] = $trainingGround;

        return $this;
    }

    /**
     * Remove trainingGround
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround
     */
    public function removeTrainingGround(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround)
    {
        $this->trainingGrounds->removeElement($trainingGround);
    }

    /**
     * Get trainingGrounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingGrounds()
    {
        return $this->trainingGrounds;
    }

    /**
     * Add publication
     *
     * @param Publication $publication
     *
     * @return Report
     */
    public function addPublication(Publication $publication)
    {
        $this->publications[] = $publication;
        $publication->setReport($this);

        return $this;
    }

    /**
     * Remove publication
     *
     * @param Publication $publication
     */
    public function removePublication(Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Add territory
     *
     * @param Territory $territory
     *
     * @return Report
     */
    public function addTerritory(Territory $territory)
    {
        $this->territories[] = $territory;
        $territory->setReport($this);
        return $this;
    }

    /**
     * Remove territory
     *
     * @param Territory $territory
     */
    public function removeTerritory(Territory $territory)
    {
        $this->territories->removeElement($territory);
    }

    /**
     * Get territories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerritories()
    {
        return $this->territories;
    }


    /**
     * @Assert\Callback(groups={"KNS-AnalyticReport-2018"})
     */
    public function isValidTrainingGrounds(ExecutionContextInterface $context, $payload)
    {
        $isChoosed = false;
        /**
         * @var TrainingGround $trainingGround
         */
        foreach ($this->trainingGrounds as $trainingGround) {
            if ($trainingGround->getParent()) {
                $isChoosed = true;
                break;
            }
        }
        if (!$isChoosed || count($this->getTrainingGrounds()) < 2) {
            $context->buildViolation('please, choose training grounds category')
                ->atPath('trainingGrounds')
                ->addViolation();
        }
    }
}
