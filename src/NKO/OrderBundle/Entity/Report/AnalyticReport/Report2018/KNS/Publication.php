<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 6.8.18
 * Time: 10.55
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="analytic_publication_kns_2018")
 * @ORM\Entity("")
 */
class Publication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"})
     *
     * @ORM\Column(name="author", type="string", nullable=true)
     *
     */
    protected $author;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"})

     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank()
     * @Assert\NotBlank(groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"})

     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\NotBlank(groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"})

     * @ORM\Column(name="location", type="string", nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     )
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="publications")
     * @ORM\JoinColumn(name="analytic_report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Publication
     */
    public function setAuthor($author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Publication
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Publication
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     *
     * @return Publication
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }
    
    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     *
     * @return Publication
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Set report
     *
     * @param BaseReport $report
     *
     * @return Publication
     */
    public function setReport(BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Publication
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
