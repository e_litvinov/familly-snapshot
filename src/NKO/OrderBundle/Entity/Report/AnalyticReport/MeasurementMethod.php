<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/5/17
 * Time: 11:39 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="analytic_report_measurement_method")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\AnalyticReport\MeasurementMethodRepository")
 */
class MeasurementMethod
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="measurementMethod",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $directResults;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="measurementMethod",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $resultKns;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="measurementMethod",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessItems;

    /**
     * @var int
     *
     * @ORM\Column(name="static_id", type="integer", nullable=false)
     */
    protected $staticId;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->directResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MeasurementMethod
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return MeasurementMethod
     */
    public function addDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults[] = $directResult;
        $directResult->setMeasurementMethod($this);

        return $this;
    }

    /**
     * Remove directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     */
    public function removeDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults->removeElement($directResult);
    }

    /**
     * Get directResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectResults()
    {
        return $this->directResults;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MeasurementMethod
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add resultKn
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $resultKn
     *
     * @return MeasurementMethod
     */
    public function addResultKn(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $resultKn)
    {
        $this->resultKns[] = $resultKn;
        $resultKn->setMeasurementMethod($this);

        return $this;
    }

    /**
     * Remove resultKn
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $resultKn
     */
    public function removeResultKn(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $resultKn)
    {
        $this->resultKns->removeElement($resultKn);
    }

    /**
     * Get resultKns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultKns()
    {
        return $this->resultKns;
    }

    /**
     * Add effectivenessItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem
     *
     * @return MeasurementMethod
     */
    public function addEffectivenessItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem)
    {
        $this->effectivenessItems[] = $effectivenessItem;

        return $this;
    }

    /**
     * Remove effectivenessItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem
     */
    public function removeEffectivenessItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem)
    {
        $this->effectivenessItems->removeElement($effectivenessItem);
    }

    /**
     * Get effectivenessItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessItems()
    {
        return $this->effectivenessItems;
    }

    /**
     * Set type
     *
     *
     * @return MeasurementMethod
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set staticId
     *
     * @param integer $staticId
     *
     * @return MeasurementMethod
     */
    public function setStaticId($staticId)
    {
        $this->staticId = $staticId;

        return $this;
    }

    /**
     * Get staticId
     *
     * @return integer
     */
    public function getStaticId()
    {
        return $this->staticId;
    }
}
