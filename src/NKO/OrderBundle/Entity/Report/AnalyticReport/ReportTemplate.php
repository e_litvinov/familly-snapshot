<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="analytic_report_template")
 */
class ReportTemplate extends BaseReportTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
