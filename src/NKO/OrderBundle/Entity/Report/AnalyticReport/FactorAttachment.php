<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/8/17
 * Time: 3:55 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;
use NKO\OrderBundle\Traits\FieldGetter;

/**
 * @ORM\Table(name="factor_attachment")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=false,
 *     groups={"AnalyticReport-2018", "AnalyticReport-2019"}
 * )
 */
class FactorAttachment
{
    use FieldGetter;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $person;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"AnalyticReport-2018", "AnalyticReport-2019"}
     * )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $link;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\NotBlank(groups={"AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    protected $_file;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FactorAttachment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set person
     *
     * @param string $person
     *
     * @return FactorAttachment
     */
    public function setPerson($person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return string
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return FactorAttachment
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return FactorAttachment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return FactorAttachment
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set report
     *
     * @param BaseReport $report
     *
     * @return FactorAttachment
     */
    public function setReport(BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FactorAttachment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
