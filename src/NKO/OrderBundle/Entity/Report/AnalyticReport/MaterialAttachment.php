<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/8/17
 * Time: 3:02 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;
use NKO\OrderBundle\Traits\FieldGetter;

/**
 *
 * @ORM\Table(name="material_attachment")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=false,
 *     groups={"AnalyticReport-2018"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=true,
 *     groups={"AnalyticReport-2019"}
 * )
 */
class MaterialAttachment
{
    use FieldGetter;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     * @ORM\Column(type="string", nullable=true)
     */
    protected $author;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var integer
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"AnalyticReport-2018", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $edition;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"KNS-2017", "KNS-2016", "BriefApplication-2018", "KNS-2018", "KNS2017-2", "AnalyticReport-2018", "AnalyticReport-2019", "KNS2019-2"}
     * )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $mediaLink;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"farvater", "kns-2016-2", "AnalyticReport-2019"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @var string
     *
     * @DocumentAssert\PdfExtension()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    protected $_file;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="materialAttachments")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return MaterialAttachment
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MaterialAttachment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return MaterialAttachment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set edition
     *
     * @param integer $edition
     *
     * @return MaterialAttachment
     */
    public function setEdition($edition)
    {
        $this->edition = $edition;

        return $this;
    }

    /**
     * Get edition
     *
     * @return integer
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * Set mediaLink
     *
     * @param string $mediaLink
     *
     * @return MaterialAttachment
     */
    public function setMediaLink($mediaLink)
    {
        $this->mediaLink = $mediaLink;

        return $this;
    }

    /**
     * Get mediaLink
     *
     * @return string
     */
    public function getMediaLink()
    {
        return $this->mediaLink;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return MaterialAttachment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return MaterialAttachment
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set report
     *
     * @param BaseReport $report
     *
     * @return MaterialAttachment
     */
    public function setReport(BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @Assert\Callback(groups={"AnalyticReport-2018"})
     */
    public function isValidAttachments(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getMediaLink() && !$this->getFile() && !$this->_file) {
            $context->buildViolation('Please, fill media link or file')
                ->atPath('mediaLink')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback()
     */
    public function isValidLink(ExecutionContextInterface $context, $payload)
    {
        if ($this->getMediaLink()) {
            if (filter_var($this->getMediaLink(), FILTER_VALIDATE_URL) === false) {
                $context->buildViolation('Invalid URL')
                    ->atPath('mediaLink')
                    ->addViolation();
            }
        }
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MaterialAttachment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
