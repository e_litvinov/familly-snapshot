<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/20/17
 * Time: 3:33 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="event_date")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class EventDate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startDateFact;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishDatePlan;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $startDatePlan;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishDateFact;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", inversedBy="eventDates")
     * @ORM\JoinColumn(name="direct_result_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $directResult;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDateFact
     *
     * @param \DateTime $startDateFact
     *
     * @return EventDate
     */
    public function setStartDateFact($startDateFact)
    {
        $this->startDateFact = $startDateFact;

        return $this;
    }

    /**
     * Get startDateFact
     *
     * @return \DateTime
     */
    public function getStartDateFact()
    {
        return $this->startDateFact;
    }

    /**
     * Set finishDatePlan
     *
     * @param \DateTime $finishDatePlan
     *
     * @return EventDate
     */
    public function setFinishDatePlan($finishDatePlan)
    {
        $this->finishDatePlan = $finishDatePlan;

        return $this;
    }

    /**
     * Get finishDatePlan
     *
     * @return \DateTime
     */
    public function getFinishDatePlan()
    {
        return $this->finishDatePlan;
    }

    /**
     * Set startDatePlan
     *
     * @param \DateTime $startDatePlan
     *
     * @return EventDate
     */
    public function setStartDatePlan($startDatePlan)
    {
        $this->startDatePlan = $startDatePlan;

        return $this;
    }

    /**
     * Get startDatePlan
     *
     * @return \DateTime
     */
    public function getStartDatePlan()
    {
        return $this->startDatePlan;
    }

    /**
     * Set finishDateFact
     *
     * @param \DateTime $finishDateFact
     *
     * @return EventDate
     */
    public function setFinishDateFact($finishDateFact)
    {
        $this->finishDateFact = $finishDateFact;

        return $this;
    }

    /**
     * Get finishDateFact
     *
     * @return \DateTime
     */
    public function getFinishDateFact()
    {
        return $this->finishDateFact;
    }

    /**
     * Set directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return EventDate
     */
    public function setDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult = null)
    {
        $this->directResult = $directResult;

        return $this;
    }

    /**
     * Get directResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getDirectResult()
    {
        return $this->directResult;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return EventDate
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
