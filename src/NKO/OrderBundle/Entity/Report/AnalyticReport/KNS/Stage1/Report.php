<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/1/17
 * Time: 1:15 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="analytic_kns_1_report")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Report extends BaseReport
{
    use ProjectCardTrait;
    use OfficialsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^((\+7|7)+([0-9]){10})$/"
     * )
     * @ORM\Column(type="string", nullable=true)
     */
    protected $directorPhone;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^((\+7|7)+([0-9]){10})$/"
     * )
     * @ORM\Column(type="string", nullable=true)
     */
    protected $directorMobilePhone;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $directorEmail;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns_analytic_report_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $expectationsAndReality;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $trainingStart;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $trainingEnd;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\TraineeshipFormat", cascade={"persist"})
     * @ORM\JoinTable(name="kns_analytic_report_traineeship_format",
     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="traineeship_format_id", referencedColumnName="id")}
     *      )
     *
     */
    protected $traineeshipFormats;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $comment;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\TrainingEvent", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $reportTrainingEvents;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectExperience;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $nextExperience;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="lessonKns",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $lessons;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="successStoryKns",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $successStories;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="feedbackKns",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $feedbackItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="directEmployeeResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $directEmployeeResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="qualitativeEmployeeResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $qualitativeEmployeeResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="otherQualitativeEmployeeResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $otherQualitativeEmployeeResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="socialResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $socialReportResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="otherSocialResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $otherSocialResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="beneficiaryResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $beneficiaryResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult", mappedBy="otherBeneficiaryResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $otherBeneficiaryResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment", mappedBy="reportTrainingEvents",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $trainingEventAttachments;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment", mappedBy="reportEtc",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $etcAttachments;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Report
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->trainingGrounds = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reportTrainingEvents = new ArrayCollection();
    }

    /**
     * Set directorPhone
     *
     * @param string $directorPhone
     *
     * @return Report
     */
    public function setDirectorPhone($directorPhone)
    {
        $this->directorPhone = $directorPhone;

        return $this;
    }

    /**
     * Get directorPhone
     *
     * @return string
     */
    public function getDirectorPhone()
    {
        return $this->directorPhone;
    }

    /**
     * Set directorMobilePhone
     *
     * @param string $directorMobilePhone
     *
     * @return Report
     */
    public function setDirectorMobilePhone($directorMobilePhone)
    {
        $this->directorMobilePhone = $directorMobilePhone;

        return $this;
    }

    /**
     * Get directorMobilePhone
     *
     * @return string
     */
    public function getDirectorMobilePhone()
    {
        return $this->directorMobilePhone;
    }

    /**
     * Set directorEmail
     *
     * @param string $directorEmail
     *
     * @return Report
     */
    public function setDirectorEmail($directorEmail)
    {
        $this->directorEmail = $directorEmail;

        return $this;
    }

    /**
     * Get directorEmail
     *
     * @return string
     */
    public function getDirectorEmail()
    {
        return $this->directorEmail;
    }

    /**
     * Add trainingGround
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround
     *
     * @return Report
     */
    public function addTrainingGround(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround)
    {
        $this->trainingGrounds[] = $trainingGround;

        return $this;
    }

    /**
     * Remove trainingGround
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround
     */
    public function removeTrainingGround(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround)
    {
        $this->trainingGrounds->removeElement($trainingGround);
    }

    /**
     * Get trainingGrounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingGrounds()
    {
        return $this->trainingGrounds;
    }

    /**
     * Set expectationsAndReality
     *
     * @param string $expectationsAndReality
     *
     * @return Report
     */
    public function setExpectationsAndReality($expectationsAndReality)
    {
        $this->expectationsAndReality = $expectationsAndReality;

        return $this;
    }

    /**
     * Get expectationsAndReality
     *
     * @return string
     */
    public function getExpectationsAndReality()
    {
        return $this->expectationsAndReality;
    }

    /**
     * Set trainingStart
     *
     * @param \DateTime $trainingStart
     *
     * @return Report
     */
    public function setTrainingStart($trainingStart)
    {
        $this->trainingStart = $trainingStart;

        return $this;
    }

    /**
     * Get trainingStart
     *
     * @return \DateTime
     */
    public function getTrainingStart()
    {
        return $this->trainingStart;
    }

    /**
     * Set trainingEnd
     *
     * @param \DateTime $trainingEnd
     *
     * @return Report
     */
    public function setTrainingEnd($trainingEnd)
    {
        $this->trainingEnd = $trainingEnd;

        return $this;
    }

    /**
     * Get trainingEnd
     *
     * @return \DateTime
     */
    public function getTrainingEnd()
    {
        return $this->trainingEnd;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Report
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add traineeshipFormat
     *
     * @param \NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat
     *
     * @return Report
     */
    public function addTraineeshipFormat(\NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat)
    {
        $this->traineeshipFormats[] = $traineeshipFormat;

        return $this;
    }

    /**
     * Remove traineeshipFormat
     *
     * @param \NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat
     */
    public function removeTraineeshipFormat(\NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat)
    {
        $this->traineeshipFormats->removeElement($traineeshipFormat);
    }

    /**
     * Get traineeshipFormats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTraineeshipFormats()
    {
        return $this->traineeshipFormats;
    }


    /**
     * Add reportTrainingEvent
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\TrainingEvent $reportTrainingEvent
     *
     * @return Report
     */
    public function addReportTrainingEvent(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\TrainingEvent $reportTrainingEvent)
    {
        $this->reportTrainingEvents[] = $reportTrainingEvent;
        $reportTrainingEvent->setReport($this);

        return $this;
    }

    /**
     * Remove reportTrainingEvent
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\TrainingEvent $reportTrainingEvent
     */
    public function removeReportTrainingEvent(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\TrainingEvent $reportTrainingEvent)
    {
        $this->reportTrainingEvents->removeElement($reportTrainingEvent);
    }

    /**
     * Get reportTrainingEvents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportTrainingEvents()
    {
        return $this->reportTrainingEvents;
    }

    /**
     * Add lesson
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $lesson
     *
     * @return Report
     */
    public function addLesson(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $lesson)
    {
        $this->lessons[] = $lesson;
        $lesson->setLessonKns($this);

        return $this;
    }

    /**
     * Remove lesson
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $lesson
     */
    public function removeLesson(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $lesson)
    {
        $this->lessons->removeElement($lesson);
    }

    /**
     * Get lessons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Add successStory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $successStory
     *
     * @return Report
     */
    public function addSuccessStory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $successStory)
    {
        $this->successStories[] = $successStory;
        $successStory->setSuccessStoryKns($this);

        return $this;
    }

    /**
     * Remove successStory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $successStory
     */
    public function removeSuccessStory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $successStory)
    {
        $this->successStories->removeElement($successStory);
    }

    /**
     * Get successStories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSuccessStories()
    {
        return $this->successStories;
    }

    /**
     * Add feedbackItem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $feedbackItem
     *
     * @return Report
     */
    public function addFeedbackItem(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $feedbackItem)
    {
        $this->feedbackItems[] = $feedbackItem;
        $feedbackItem->setFeedbackKns($this);

        return $this;
    }

    /**
     * Remove feedbackItem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $feedbackItem
     */
    public function removeFeedbackItem(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $feedbackItem)
    {
        $this->feedbackItems->removeElement($feedbackItem);
    }

    /**
     * Get feedbackItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFeedbackItems()
    {
        return $this->feedbackItems;
    }

    /**
     * Set projectExperience
     *
     * @param string $projectExperience
     *
     * @return Report
     */
    public function setProjectExperience($projectExperience)
    {
        $this->projectExperience = $projectExperience;

        return $this;
    }

    /**
     * Get projectExperience
     *
     * @return string
     */
    public function getProjectExperience()
    {
        return $this->projectExperience;
    }

    /**
     * Set nextExperience
     *
     * @param string $nextExperience
     *
     * @return Report
     */
    public function setNextExperience($nextExperience)
    {
        $this->nextExperience = $nextExperience;

        return $this;
    }

    /**
     * Get nextExperience
     *
     * @return string
     */
    public function getNextExperience()
    {
        return $this->nextExperience;
    }

    /**
     * Add directEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $directEmployeeResult
     *
     * @return Report
     */
    public function addDirectEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $directEmployeeResult)
    {
        $this->directEmployeeResults[] = $directEmployeeResult;
        $directEmployeeResult->setDirectEmployeeResult($this);

        return $this;
    }

    /**
     * Remove directEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $directEmployeeResult
     */
    public function removeDirectEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $directEmployeeResult)
    {
        $this->directEmployeeResults->removeElement($directEmployeeResult);
    }

    /**
     * Get directEmployeeResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectEmployeeResults()
    {
        return $this->directEmployeeResults;
    }

    /**
     * Add qualitativeEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $qualitativeEmployeeResult
     *
     * @return Report
     */
    public function addQualitativeEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $qualitativeEmployeeResult)
    {
        $this->qualitativeEmployeeResults[] = $qualitativeEmployeeResult;
        $qualitativeEmployeeResult->setQualitativeEmployeeResult($this);

        return $this;
    }

    /**
     * Remove qualitativeEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $qualitativeEmployeeResult
     */
    public function removeQualitativeEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $qualitativeEmployeeResult)
    {
        $this->qualitativeEmployeeResults->removeElement($qualitativeEmployeeResult);
    }

    /**
     * Get qualitativeEmployeeResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQualitativeEmployeeResults()
    {
        return $this->qualitativeEmployeeResults;
    }

    /**
     * Add otherSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherSocialResult
     *
     * @return Report
     */
    public function addOtherSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherSocialResult)
    {
        $this->otherSocialResults[] = $otherSocialResult;
        $otherSocialResult->setOtherSocialResult($this);

        return $this;
    }

    /**
     * Remove otherSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherSocialResult
     */
    public function removeOtherSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherSocialResult)
    {
        $this->otherSocialResults->removeElement($otherSocialResult);
    }

    /**
     * Get otherSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherSocialResults()
    {
        return $this->otherSocialResults;
    }

    /**
     * Add beneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $beneficiaryResult
     *
     * @return Report
     */
    public function addBeneficiaryResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $beneficiaryResult)
    {
        $this->beneficiaryResults[] = $beneficiaryResult;
        $beneficiaryResult->setBeneficiaryResult($this);

        return $this;
    }

    /**
     * Remove beneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $beneficiaryResult
     */
    public function removeBeneficiaryResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $beneficiaryResult)
    {
        $this->beneficiaryResults->removeElement($beneficiaryResult);
    }

    /**
     * Get beneficiaryResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryResults()
    {
        return $this->beneficiaryResults;
    }

    /**
     * Add otherBeneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherBeneficiaryResult
     *
     * @return Report
     */
    public function addOtherBeneficiaryResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherBeneficiaryResult)
    {
        $this->otherBeneficiaryResults[] = $otherBeneficiaryResult;
        $otherBeneficiaryResult->setOtherBeneficiaryResult($this);

        return $this;
    }

    /**
     * Remove otherBeneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherBeneficiaryResult
     */
    public function removeOtherBeneficiaryResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherBeneficiaryResult)
    {
        $this->otherBeneficiaryResults->removeElement($otherBeneficiaryResult);
    }

    /**
     * Get otherBeneficiaryResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherBeneficiaryResults()
    {
        return $this->otherBeneficiaryResults;
    }

    /**
     * Add otherQualitativeEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherQualitativeEmployeeResult
     *
     * @return Report
     */
    public function addOtherQualitativeEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherQualitativeEmployeeResult)
    {
        $this->otherQualitativeEmployeeResults[] = $otherQualitativeEmployeeResult;
        $otherQualitativeEmployeeResult->setOtherQualitativeEmployeeResult($this);

        return $this;
    }

    /**
     * Remove otherQualitativeEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherQualitativeEmployeeResult
     */
    public function removeOtherQualitativeEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $otherQualitativeEmployeeResult)
    {
        $this->otherQualitativeEmployeeResults->removeElement($otherQualitativeEmployeeResult);
    }

    /**
     * Get otherQualitativeEmployeeResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherQualitativeEmployeeResults()
    {
        return $this->otherQualitativeEmployeeResults;
    }

    /**
     * Add socialReportResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $socialReportResult
     *
     * @return Report
     */
    public function addSocialReportResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $socialReportResult)
    {
        $this->socialReportResults[] = $socialReportResult;
        $socialReportResult->setSocialResult($this);

        return $this;
    }

    /**
     * Remove socialReportResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $socialReportResult
     */
    public function removeSocialReportResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult $socialReportResult)
    {
        $this->socialReportResults->removeElement($socialReportResult);
    }

    /**
     * Get socialReportResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialReportResults()
    {
        return $this->socialReportResults;
    }

    /**
     * Add trainingEventAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $trainingEventAttachment
     *
     * @return Report
     */
    public function addTrainingEventAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $trainingEventAttachment)
    {
        $this->trainingEventAttachments[] = $trainingEventAttachment;
        $trainingEventAttachment->setReportTrainingEvents($this);

        return $this;
    }

    /**
     * Remove trainingEventAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $trainingEventAttachment
     */
    public function removeTrainingEventAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $trainingEventAttachment)
    {
        $this->trainingEventAttachments->removeElement($trainingEventAttachment);
    }

    /**
     * Get trainingEventAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingEventAttachments()
    {
        return $this->trainingEventAttachments;
    }

    /**
     * Add etcAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $etcAttachment
     *
     * @return Report
     */
    public function addEtcAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $etcAttachment)
    {
        $this->etcAttachments[] = $etcAttachment;
        $etcAttachment->setReportEtc($this);

        return $this;
    }

    /**
     * Remove etcAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $etcAttachment
     */
    public function removeEtcAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment $etcAttachment)
    {
        $this->etcAttachments->removeElement($etcAttachment);
    }

    /**
     * Get etcAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtcAttachments()
    {
        return $this->etcAttachments;
    }
}
