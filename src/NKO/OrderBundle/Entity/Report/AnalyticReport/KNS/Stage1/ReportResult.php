<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/5/17
 * Time: 10:28 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="analytic_kns_1_result")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ReportResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $result;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $indicator;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $plan;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $fact;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $expected;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod", inversedBy="resultKns")
     * @ORM\JoinColumn(name="measurement_method_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $measurementMethod;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="directEmployeeResults")
     * @ORM\JoinColumn(name="direct_employee_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $directEmployeeResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="qualitativeEmployeeResults")
     * @ORM\JoinColumn(name="qualitative_employee_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $qualitativeEmployeeResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="otherQualitativeEmployeeResults")
     * @ORM\JoinColumn(name="other_qualitative_employee_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $otherQualitativeEmployeeResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="socialReportResults")
     * @ORM\JoinColumn(name="social_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $socialResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="otherSocialResults")
     * @ORM\JoinColumn(name="other_social_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $otherSocialResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="beneficiaryResults")
     * @ORM\JoinColumn(name="beneficiary_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $beneficiaryResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="otherBeneficiaryResults")
     * @ORM\JoinColumn(name="other_beneficiary_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $otherBeneficiaryResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BeneficiaryGroup")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $targetGroup;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $customTargetGroup;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ReportResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return ReportResult
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return ReportResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set plan
     *
     * @param integer $plan
     *
     * @return ReportResult
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return integer
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set fact
     *
     * @param integer $fact
     *
     * @return ReportResult
     */
    public function setFact($fact)
    {
        $this->fact = $fact;

        return $this;
    }

    /**
     * Get fact
     *
     * @return integer
     */
    public function getFact()
    {
        return $this->fact;
    }

    /**
     * Set expected
     *
     * @param integer $expected
     *
     * @return ReportResult
     */
    public function setExpected($expected)
    {
        $this->expected = $expected;

        return $this;
    }

    /**
     * Get expected
     *
     * @return integer
     */
    public function getExpected()
    {
        return $this->expected;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ReportResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     *
     * @return ReportResult
     */
    public function setMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod = null)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set directEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $directEmployeeResult
     *
     * @return ReportResult
     */
    public function setDirectEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $directEmployeeResult = null)
    {
        $this->directEmployeeResult = $directEmployeeResult;

        return $this;
    }

    /**
     * Get directEmployeeResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getDirectEmployeeResult()
    {
        return $this->directEmployeeResult;
    }

    /**
     * Set qualitativeEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $qualitativeEmployeeResult
     *
     * @return ReportResult
     */
    public function setQualitativeEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $qualitativeEmployeeResult = null)
    {
        $this->qualitativeEmployeeResult = $qualitativeEmployeeResult;

        return $this;
    }

    /**
     * Get qualitativeEmployeeResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getQualitativeEmployeeResult()
    {
        return $this->qualitativeEmployeeResult;
    }

    /**
     * Set socialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $socialResult
     *
     * @return ReportResult
     */
    public function setSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $socialResult = null)
    {
        $this->socialResult = $socialResult;

        return $this;
    }

    /**
     * Get socialResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getSocialResult()
    {
        return $this->socialResult;
    }

    /**
     * Set otherSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $otherSocialResult
     *
     * @return ReportResult
     */
    public function setOtherSocialResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $otherSocialResult = null)
    {
        $this->otherSocialResult = $otherSocialResult;

        return $this;
    }

    /**
     * Get otherSocialResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getOtherSocialResult()
    {
        return $this->otherSocialResult;
    }

    /**
     * Set otherBeneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $otherBeneficiaryResult
     *
     * @return ReportResult
     */
    public function setOtherBeneficiaryResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $otherBeneficiaryResult = null)
    {
        $this->otherBeneficiaryResult = $otherBeneficiaryResult;

        return $this;
    }

    /**
     * Get otherBeneficiaryResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getOtherBeneficiaryResult()
    {
        return $this->otherBeneficiaryResult;
    }

    /**
     * Set beneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $beneficiaryResult
     *
     * @return ReportResult
     */
    public function setBeneficiaryResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $beneficiaryResult = null)
    {
        $this->beneficiaryResult = $beneficiaryResult;

        return $this;
    }

    /**
     * Get beneficiaryResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getBeneficiaryResult()
    {
        return $this->beneficiaryResult;
    }

    /**
     * Set otherQualitativeEmployeeResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $otherQualitativeEmployeeResult
     *
     * @return ReportResult
     */
    public function setOtherQualitativeEmployeeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $otherQualitativeEmployeeResult = null)
    {
        $this->otherQualitativeEmployeeResult = $otherQualitativeEmployeeResult;

        return $this;
    }

    /**
     * Get otherQualitativeEmployeeResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getOtherQualitativeEmployeeResult()
    {
        return $this->otherQualitativeEmployeeResult;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Set customTargetGroup
     *
     * @param string $customTargetGroup
     *
     * @return ReportResult
     */
    public function setCustomTargetGroup($customTargetGroup)
    {
        $this->customTargetGroup = $customTargetGroup;

        return $this;
    }

    /**
     * Get customTargetGroup
     *
     * @return string
     */
    public function getCustomTargetGroup()
    {
        return $this->customTargetGroup;
    }


    /**
     * Set targetGroup
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryGroup $targetGroup
     *
     * @return ReportResult
     */
    public function setTargetGroup(\NKO\OrderBundle\Entity\BeneficiaryGroup $targetGroup = null)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return \NKO\OrderBundle\Entity\BeneficiaryGroup
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }
}
