<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/18/17
 * Time: 10:41 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Validator\Constraints as FileAssert;

/**
 *
 * @ORM\Table(name="attachment_kns_stage_1")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class Attachment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @FileAssert\PdfExtension()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    protected $_file;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    const EXT_1 = [
        'pdf'
    ];

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="trainingEventAttachments")
     * @ORM\JoinColumn(name="report_training_event_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportTrainingEvents;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="etcAttachments")
     * @ORM\JoinColumn(name="report_etc_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $reportEtc;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Attachment
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }

        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @Assert\Callback
     */
    public function isValidAttachments(ExecutionContextInterface $context)
    {
        if (!$this->getFile() && !$this->_file) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('file')
                ->addViolation();
        }
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Attachment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Attachment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set reportTrainingEvents
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $reportTrainingEvents
     *
     * @return Attachment
     */
    public function setReportTrainingEvents(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $reportTrainingEvents = null)
    {
        $this->reportTrainingEvents = $reportTrainingEvents;

        return $this;
    }

    /**
     * Get reportTrainingEvents
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getReportTrainingEvents()
    {
        return $this->reportTrainingEvents;
    }

    /**
     * Set reportEtc
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $reportEtc
     *
     * @return Attachment
     */
    public function setReportEtc(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $reportEtc = null)
    {
        $this->reportEtc = $reportEtc;

        return $this;
    }

    /**
     * Get reportEtc
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getReportEtc()
    {
        return $this->reportEtc;
    }

    public function getReport()
    {
        if ($this->reportEtc) {
            return $this->reportEtc;
        }

        return $this->reportTrainingEvents;
    }
}
