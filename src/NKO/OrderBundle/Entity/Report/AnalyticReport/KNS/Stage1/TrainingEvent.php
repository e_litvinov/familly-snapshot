<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/4/17
 * Time: 11:33 AM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="analytic_kns_1_training_event")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class TrainingEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $trainingEvent;

    /**
     *  @Assert\NotBlank()
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $start;

    /**
     *  @Assert\NotBlank()
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $end;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $place;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $participantCount;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report", inversedBy="reportTrainingEvents")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->getTrainingEvent();
    }

    /**
     * Set trainingEvent
     *
     * @param string $trainingEvent
     *
     * @return TrainingEvent
     */
    public function setTrainingEvent($trainingEvent)
    {
        $this->trainingEvent = $trainingEvent;

        return $this;
    }

    /**
     * Get trainingEvent
     *
     * @return string
     */
    public function getTrainingEvent()
    {
        return $this->trainingEvent;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return TrainingEvent
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return TrainingEvent
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return TrainingEvent
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }


    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return TrainingEvent
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $report
     *
     * @return TrainingEvent
     */
    public function setReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set participantCount
     *
     * @param integer $participantCount
     *
     * @return TrainingEvent
     */
    public function setParticipantCount($participantCount)
    {
        $this->participantCount = $participantCount;

        return $this;
    }

    /**
     * Get participantCount
     *
     * @return integer
     */
    public function getParticipantCount()
    {
        return $this->participantCount;
    }
}
