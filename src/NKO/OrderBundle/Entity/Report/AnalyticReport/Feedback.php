<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/16/17
 * Time: 5:05 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Effectiveness;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="feedback")
 * @ORM\Entity("")
 */
class Feedback
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $administrativeSolution;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $service;




    /**
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", inversedBy="practiceAttachments")
     * @ORM\JoinColumn(name="practice_attachment_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $practice;

    /**
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Effectiveness")
     * @ORM\JoinColumn(name="practice_effectiveness_attachment_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $practiceEffectiveness;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", inversedBy="spreadPracticeAttachments")
     * @ORM\JoinColumn(name="spread_practice_attachment_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $spreadPractice;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool", mappedBy="feedback",
     *     cascade={"all"})
     */
    protected $tools;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $report;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_spread_attachment_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $reportSpreadAttachment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_social_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $reportSocial;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_spread_social_attachment_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $reportSpreadSocialAttachment;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    const FILES_COUNT = 3;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Feedback
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Set service
     *
     * @param string $service
     *
     * @return Feedback
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set practice
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practice
     *
     * @return Feedback
     */
    public function setPractice(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practice = null)
    {
        $this->practice = $practice;

        return $this;
    }

    /**
     * Get practice
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getPractice()
    {
        return $this->practice;
    }

    /**
     * Set practice
     *
     * @param Effectiveness $practice
     *
     * @return Feedback
     */
    public function setPracticeEffectiveness(Effectiveness $practice = null)
    {
        $this->practiceEffectiveness = $practice;

        return $this;
    }

    /**
     * Get practice
     *
     * @return Effectiveness
     */
    public function getPracticeEffectiveness()
    {
        return $this->practiceEffectiveness;
    }

    /**
     * Set spreadPractice
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $spreadPractice
     *
     * @return Feedback
     */
    public function setSpreadPractice(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $spreadPractice = null)
    {
        $this->spreadPractice = $spreadPractice;

        return $this;
    }

    /**
     * Get spreadPractice
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getSpreadPractice()
    {
        return $this->spreadPractice;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tools = new ArrayCollection();
        $this->tools = new \Doctrine\Common\Collections\ArrayCollection();
        for ($i = 0; $i < self::FILES_COUNT; $i++) {
            $this->addTool(new FeedbackTool());
        }
    }

    /**
     * Set administrativeSolution
     *
     * @param string $administrativeSolution
     *
     * @return Feedback
     */
    public function setAdministrativeSolution($administrativeSolution)
    {
        $this->administrativeSolution = $administrativeSolution;

        return $this;
    }

    /**
     * Get administrativeSolution
     *
     * @return string
     */
    public function getAdministrativeSolution()
    {
        return $this->administrativeSolution;
    }

    /**
     * Add tool
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool $tool
     *
     * @return Feedback
     */
    public function addTool(\NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool $tool)
    {
        $this->tools[] = $tool;
        $tool->setFeedback($this);

        return $this;
    }

    /**
     * Remove tool
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool $tool
     */
    public function removeTool(\NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool $tool)
    {
        $this->tools->removeElement($tool);
    }

    /**
     * Get tools
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTools()
    {
        return $this->tools;
    }

    /**
     * Set report
     *
     * @param BaseReport $report
     *
     * @return Feedback
     */
    public function setReport(BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set reportSpreadAttachment
     *
     * @param BaseReport $reportSpreadAttachment
     *
     * @return Feedback
     */
    public function setReportSpreadAttachment(BaseReport $reportSpreadAttachment = null)
    {
        $this->reportSpreadAttachment = $reportSpreadAttachment;

        return $this;
    }

    /**
     * Get reportSpreadAttachment
     *
     * @return BaseReport
     */
    public function getReportSpreadAttachment()
    {
        return $this->reportSpreadAttachment;
    }

    /**
     * Set report
     *
     * @param BaseReport $report
     *
     * @return Feedback
     */
    public function setReportSocial(BaseReport $report = null)
    {
        $this->reportSocial = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return BaseReport
     */
    public function getReportSocial()
    {
        return $this->reportSocial;
    }

    /**
     * Set reportSpreadAttachment
     *
     * @param BaseReport $reportSpreadAttachment
     *
     * @return Feedback
     */
    public function setReportSpreadSocialAttachment(BaseReport $reportSpreadAttachment = null)
    {
        $this->reportSpreadSocialAttachment = $reportSpreadAttachment;

        return $this;
    }

    /**
     * Get reportSpreadAttachment
     *
     * @return BaseReport
     */
    public function getReportSpreadSocialAttachment()
    {
        return $this->reportSpreadSocialAttachment;
    }

    /**
     * @Assert\Callback
     */
    public function isValidToolsFile(ExecutionContextInterface $context, $payload)
    {
        foreach ($this->getTools() as $tool) {
            if ($tool->fileExist()) {
                return;
            }
        }
        $context->buildViolation('This value should not be blank.')
            ->atPath('tools[0].file')
            ->addViolation();
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Feedback
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
