<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/8/17
 * Time: 5:18 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="analytic_report_event")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity("")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment", mappedBy="event",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringDevelopments;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->monitoringDevelopments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Event
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add monitoringDevelopment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment
     *
     * @return Event
     */
    public function addMonitoringDevelopment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment)
    {
        $this->monitoringDevelopments[] = $monitoringDevelopment;
        $monitoringDevelopment->setEvent($this);

        return $this;
    }

    /**
     * Remove monitoringDevelopment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment
     */
    public function removeMonitoringDevelopment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment)
    {
        $this->monitoringDevelopments->removeElement($monitoringDevelopment);
    }

    /**
     * Get monitoringDevelopments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringDevelopments()
    {
        return $this->monitoringDevelopments;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Event
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
