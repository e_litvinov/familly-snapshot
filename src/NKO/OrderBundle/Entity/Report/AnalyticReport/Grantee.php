<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/13/17
 * Time: 2:26 PM
 */

namespace NKO\OrderBundle\Entity\Report\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="grantee")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\AnalyticReport\GranteeRepository")
 */
class Grantee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="grantee")
     */
    protected $directResults;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->analyticReports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Grantee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Grantee
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Add directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return Grantee
     */
    public function addDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults[] = $directResult;
        $directResult->setGrantee($this);

        return $this;
    }

    /**
     * Remove directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     */
    public function removeDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults->removeElement($directResult);
    }

    /**
     * Get directResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectResults()
    {
        return $this->directResults;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Grantee
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
