<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 19.10.16
 * Time: 13:36
 */

namespace NKO\OrderBundle\Entity\Report\FinanceReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\FinanceSpent as BaseFinanceSpent;

/**
 * @ORM\Table(name="finance_report_finance_spent")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\FinanceReport\FinanceSpentRepository")
 */
class FinanceSpent extends BaseFinanceSpent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="incremental_costs", type="float", nullable=true, options={"default": 0})
     */
    private $incrementalCosts;

    /**
     * @var float
     *
     * @ORM\Column(name="required_amount", type="float", nullable=true, options={"default": 0})
     */
    private $requiredSum;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set incrementalCosts
     *
     * @param float $incrementalCosts
     *
     * @return FinanceSpent
     */
    public function setIncrementalCosts($incrementalCosts)
    {
        $this->incrementalCosts = $incrementalCosts;

        return $this;
    }

    /**
     * Get incrementalCosts
     *
     * @return float
     */
    public function getIncrementalCosts()
    {
        return $this->incrementalCosts;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FinanceSpent
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set requiredSum
     *
     * @param float $requiredSum
     *
     * @return FinanceSpent
     */
    public function setRequiredSum($requiredSum)
    {
        $this->requiredSum = $requiredSum;

        return $this;
    }

    /**
     * Get requiredSum
     *
     * @return float
     */
    public function getRequiredSum()
    {
        return $this->requiredSum;
    }
}
