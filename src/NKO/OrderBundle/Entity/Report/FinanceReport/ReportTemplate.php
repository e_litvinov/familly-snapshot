<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\FinanceReport;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="finance_report_template")
 */
class ReportTemplate extends BaseReportTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\FinanceSpent", mappedBy="reportTemplate", cascade={"all"}, orphanRemoval=true)
     */
    protected $financeSpent;

    protected $isEqual = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->financeSpent = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add financeSpent
     *
     * @param \NKO\OrderBundle\Entity\FinanceSpent $financeSpent
     *
     * @return ReportTemplate
     */
    public function addFinanceSpent(\NKO\OrderBundle\Entity\FinanceSpent $financeSpent)
    {
        $this->financeSpent[] = $financeSpent;
        $financeSpent->setReportTemplate($this);

        return $this;
    }

    /**
     * Remove financeSpent
     *
     * @param \NKO\OrderBundle\Entity\FinanceSpent $financeSpent
     */
    public function removeFinanceSpent(\NKO\OrderBundle\Entity\FinanceSpent $financeSpent)
    {
        $this->financeSpent->removeElement($financeSpent);
    }

    /**
     * Get financeSpent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinanceSpent()
    {
        return $this->financeSpent;
    }

    public function getSumGrant()
    {
        $applicationHistory = $this->getApplicationHistory();
        $reportForm = $this->getReportForm();
        if ($applicationHistory) {
            $grants = $applicationHistory->getGrants();
            foreach ($grants as $grant) {
                if ($grant->getReportForm() === $reportForm) {
                    return $grant->getSumGrant();
                }
            }
        }
    }

    public function getIsEqualFinanceSpent()
    {
        if ($this->isEqual !== null) {
            return $this->isEqual;
        }

        $this->isEqual = true;
        foreach ($this->financeSpent as $finance) {
            if ($finance->getApprovedSum() != $finance->getRequiredSum()) {
                $this->isEqual = false;
            }
        }

        return $this->isEqual;
    }
}
