<?php

namespace NKO\OrderBundle\Entity\Report\FinanceReport\Report2019;

use NKO\OrderBundle\Traits\FinanceReport\FinanceReportDocumentTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * DocumentReport
 *
 * @ORM\Table(name="finance_document_2019")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={"file"},
 *     groups={"FinanceReport-2019","MixedReportKNS-2019"}
 * )
 */
class Document
{
    use FinanceReportDocumentTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2019", "MixedReportKNS-2019"}
     * )
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $expenseName;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2019", "MixedReportKNS-2019"}
     * )
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $documentName;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getExpenseName()
    {
        return $this->expenseName;
    }

    /**
     * @param string $expenseName
     * @return Document
     */
    public function setExpenseName($expenseName)
    {
        $this->expenseName = $expenseName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentName()
    {
        return $this->documentName;
    }

    /**
     * @param string $documentName
     * @return Document
     */
    public function setDocumentName($documentName)
    {
        $this->documentName = $documentName;

        return $this;
    }
}
