<?php

namespace NKO\OrderBundle\Entity\Report\FinanceReport\Report2019;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceReportInterface;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Document;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="finance_report_2019")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @CustomAssert\TemplateAccepted(groups={"FinanceReport-2019"})
 */
class Report extends BaseReport implements FinanceReportInterface
{
    use FinanceReportTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="incremental_costs", type="float", nullable=true)
     */
    protected $incrementalCosts;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="Document", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->registers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->financeSpent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set incrementalCosts
     *
     * @param float $incrementalCosts
     *
     * @return Report
     */
    public function setIncrementalCosts($incrementalCosts)
    {
        $this->incrementalCosts = $incrementalCosts;

        return $this;
    }

    /**
     * Get incrementalCosts
     *
     * @return float
     */
    public function getIncrementalCosts()
    {
        return $this->incrementalCosts;
    }

    /**
     * @param Document $document
     * @return $this
     */
    public function addDocument(Document $document)
    {
        $this->documents[] = $document;
        $document->setReport($this);

        return $this;
    }

    /**
     * @param Document $document
     * @return Report
     */
    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
