<?php

namespace NKO\OrderBundle\Entity\Report\FinanceReport\Report2018;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceReportInterface;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="finance_report_2018")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Report extends BaseReport implements FinanceReportInterface
{
    use FinanceReportTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="incremental_costs", type="float", nullable=true)
     */
    protected $incrementalCosts;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->registers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->financeSpent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document
     *
     * @return Report
     */
    public function addDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document)
    {
        $this->documents[] = $document;
        $document->setReport($this);

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set incrementalCosts
     *
     * @param float $incrementalCosts
     *
     * @return Report
     */
    public function setIncrementalCosts($incrementalCosts)
    {
        $this->incrementalCosts = $incrementalCosts;

        return $this;
    }

    /**
     * Get incrementalCosts
     *
     * @return float
     */
    public function getIncrementalCosts()
    {
        return $this->incrementalCosts;
    }
}
