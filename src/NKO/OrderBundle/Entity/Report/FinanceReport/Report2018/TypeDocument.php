<?php

namespace NKO\OrderBundle\Entity\Report\FinanceReport\Report2018;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="finance_report_2018_type_document")
 */
class TypeDocument
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="title", nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(type="json_array", nullable=false)
     */
    private $types;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document", mappedBy="documentType", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return TypeDocument
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return TypeDocument
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set types
     *
     * @param array $types
     *
     * @return TypeDocument
     */
    public function setTypes($types)
    {
        $this->types = $types;

        return $this;
    }

    /**
     * Get types
     *
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function __toString()
    {
        return $this->title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document
     *
     * @return TypeDocument
     */
    public function addDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
