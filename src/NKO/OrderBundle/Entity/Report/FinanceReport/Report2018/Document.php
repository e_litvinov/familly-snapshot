<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 5/22/18
 * Time: 1:04 PM
 */

namespace NKO\OrderBundle\Entity\Report\FinanceReport\Report2018;

use NKO\OrderBundle\Traits\FinanceReport\FinanceReportDocumentTrait;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;
use NKO\OrderBundle\Traits\FieldGetter;

/**
 * Document
 *
 * @ORM\Entity()
 * @ORM\Table(name="finance_report_2018_document")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=true,
 *     groups={"MixedReportKNS-2018", "FinanceReport-2018", "FinanceReport-2019"}
 * )
 */
class Document
{
    use FinanceReportDocumentTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2018", "MixedReportKNS-2018"}
     * )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument", inversedBy="documents", fetch="EAGER")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $documentType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentType
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument $documentType
     *
     * @return Document
     */
    public function setDocumentType(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument $documentType = null)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get documentType
     *
     * @return \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }
}
