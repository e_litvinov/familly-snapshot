<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 16:15
 */

namespace NKO\OrderBundle\Entity\Report\FinanceReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTrait;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="finance_report")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "finance_prev" = "NKO\OrderBundle\Entity\Report\FinanceReport\PrevReport",
 *     })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Report extends BaseReport implements FinanceReportInterface
{
    use FinanceReportTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="incremental_costs", type="float", nullable=true)
     */
    protected $incrementalCosts;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\DocumentReport", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->financeSpent = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->registers = new ArrayCollection();
    }

    /**
     * Set incrementalCosts
     *
     * @param float $incrementalCosts
     *
     * @return Report
     */
    public function setIncrementalCosts($incrementalCosts)
    {
        $this->incrementalCosts = $incrementalCosts;

        return $this;
    }

    /**
     * Get incrementalCosts
     *
     * @return float
     */
    public function getIncrementalCosts()
    {
        return $this->incrementalCosts;
    }

    public function addDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents[] = $document;
        $document->setReport($this);

        return $this;
    }

    public function removeDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents->removeElement($document);
    }

    public function getDocuments()
    {
        return $this->documents;
    }
}
