<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 21.8.18
 * Time: 10.59
 */

namespace NKO\OrderBundle\Entity\Report\MixedReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MixedReport\KNS\ReportRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="mixed_result")
 */
class Result
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $indicator;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $result;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $serviceFactValue;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $servicePlanValue;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $serviceExpectedValue;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=1024)
     */
    protected $comment;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     *     )
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinColumn(name="measurement_method_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $measurementMethod;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="mixedEmployeeResults")
     * @ORM\JoinColumn(name="employees_result_mixed_report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $employeesResultMixedReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="mixedBeneficiariesResults")
     * @ORM\JoinColumn(name="beneficiaries_result_mixed_report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $beneficiariesResultMixedReport;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="mixedResults")
     * @ORM\JoinColumn(name="result_mixed_report_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $resultMixedReports;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\ResultType")
     * @ORM\JoinColumn(name="result_type_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $linkedTypes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $customIndex;

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Result
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set result
     *
     * @param string $result
     *
     * @return Result
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return Result
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Result
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * Set serviceFactValue
     *
     * @param integer $serviceFactValue
     *
     * @return Result
     */
    public function setServiceFactValue($serviceFactValue)
    {
        $this->serviceFactValue = $serviceFactValue;

        return $this;
    }

    /**
     * Get serviceFactValue
     *
     * @return integer
     */
    public function getServiceFactValue()
    {
        return $this->serviceFactValue;
    }

    /**
     * Set servicePlanValue
     *
     * @param integer $servicePlanValue
     *
     * @return Result
     */
    public function setServicePlanValue($servicePlanValue)
    {
        $this->servicePlanValue = $servicePlanValue;

        return $this;
    }

    /**
     * Get serviceExpectedValue
     *
     * @return integer
     */
    public function getServiceExpectedValue()
    {
        return $this->serviceExpectedValue;
    }

    /**
     * Set serviceExpectedValue
     *
     * @param integer $serviceExpectedalue
     *
     * @return Result
     */
    public function setServiceExpectedValue($serviceExpectedValue)
    {
        $this->serviceExpectedValue = $serviceExpectedValue;

        return $this;
    }

    /**
     * Get servicePlanValue
     *
     * @return integer
     */
    public function getServicePlanValue()
    {
        return $this->servicePlanValue;
    }

    /**
     * Set measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     *
     * @return Result
     */
    public function setMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod = null)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set employeesResultMixedReport
     *
     * @param BaseReport $employeesResultMixedReport
     *
     * @return Result
     */
    public function setEmployeesResultMixedReport(BaseReport $employeesResultMixedReport = null)
    {
        $this->employeesResultMixedReport = $employeesResultMixedReport;

        return $this;
    }

    /**
     * Get employeesResultMixedReport
     *
     * @return BaseReport
     */
    public function getEmployeesResultMixedReport()
    {
        return $this->employeesResultMixedReport;
    }

    /**
     * Set beneficiariesResultMixedReport
     *
     * @param BaseReport $beneficiariesResultMixedReport
     *
     * @return Result
     */
    public function setBeneficiariesResultMixedReport(BaseReport $beneficiariesResultMixedReport = null)
    {
        $this->beneficiariesResultMixedReport = $beneficiariesResultMixedReport;

        return $this;
    }

    /**
     * Get beneficiariesResultMixedReport
     *
     * @return BaseReport
     */
    public function getBeneficiariesResultMixedReport()
    {
        return $this->beneficiariesResultMixedReport;
    }

    /**
     * @return ResultType
     */
    public function getLinkedTypes()
    {
        return $this->linkedTypes;
    }

    /**
     * @param mixed $linkedTypes
     *
     * @return Result
     */
    public function setLinkedTypes($linkedTypes)
    {
        $this->linkedTypes = $linkedTypes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResultMixedReports()
    {
        return $this->resultMixedReports;
    }

    /**
     * @param mixed $resultMixedReports
     *
     * @return Result
     */
    public function setResultMixedReports(BaseReport  $resultMixedReports = null)
    {
        $this->resultMixedReports = $resultMixedReports;

        return $this;
    }

    /**
     * Set index
     *
     * @param integer $customIndex
     *
     * @return Result
     */
    public function setCustomIndex($customIndex)
    {
        $this->customIndex = $customIndex;

        return $this;
    }

    /**
     * Get index
     *
     * @return integer
     */
    public function getCustomIndex()
    {
        return $this->customIndex;
    }

    /**
    * @Assert\Callback(groups={"MixedReportKNS-2018", "MixedReportKNS-2019"})
    */
    public function isValidCustomIndex(ExecutionContextInterface $context, $payload)
    {
        if (!$this->customIndex) {
            if (!$this->indicator) {
                $this->setValidate($context, 'indicator');
            }

            if (!$this->result) {
                $this->setValidate($context, 'result');
            }
        }

        if (!isset($this->servicePlanValue) && $this->customIndex !== 2) {
            $this->setValidate($context, 'servicePlanValue');
        }

        if (!isset($this->serviceFactValue) && $this->customIndex !== 2) {
            $this->setValidate($context, 'serviceFactValue');
        }

        if (!isset($this->serviceExpectedValue) && $this->customIndex !== 1) {
            $this->setValidate($context, 'serviceExpectedValue');
        }
    }

    public function setValidate(ExecutionContextInterface $context, $field)
    {
        $context->buildViolation('value is invalid(field must be non empty)')
            ->atPath($field)
            ->addViolation();
    }
}
