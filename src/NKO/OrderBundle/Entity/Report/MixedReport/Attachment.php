<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 22.8.18
 * Time: 14.05
 */

namespace NKO\OrderBundle\Entity\Report\MixedReport;

use NKO\OrderBundle\Entity\BaseReport;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Traits\FieldGetter;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 *
 * @ORM\Table(name="attachment_mixed")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF or Excel or Doc file",
 *     extensions={"pdf", "excel", "doc"},
 *     fileNames={"file"},
 *     notNull=true,
 *     groups={"MixedReportKNS-2019", "MixedReportKNS-2018"}
 * )
 * @ORM\Entity("")
 */
class Attachment
{
    use FieldGetter;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var string
     *
     * @DocumentAssert\PdfXlsDocExtension(
     *     groups={"MixedReportKNS-2018"}
     *      )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     *      )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;
    
    /**
     * @var string
     */
    protected $_file;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $report;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Attachment
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Attachment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Attachment
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param BaseReport $report
     *
     * @return Attachment
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
