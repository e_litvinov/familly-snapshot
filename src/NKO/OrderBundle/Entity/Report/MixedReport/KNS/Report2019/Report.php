<?php

namespace NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Document;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTrait;
use NKO\OrderBundle\Traits\MixedReport\AttachmentTrait;
use NKO\OrderBundle\Traits\MixedReport\MixedResultsTrait;
use NKO\OrderBundle\Traits\ProjectResultsTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use NKO\OrderBundle\Traits\Report\ProjectDescriptionTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MixedReport\KNS\ReportRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="mixed_report_kns_2019")
 */
class Report extends BaseReport
{
    use OfficialsTrait;
    use ProjectCardTrait;
    use ProjectDescriptionTrait;
    use ProjectResultsTrait;
    use FinanceReportTrait;
    use AttachmentTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup", mappedBy="report",
     *     orphanRemoval=true, cascade={"all"})
     */
    private $otherBeneficiaryGroups;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="mixed_report_2019_people_category_relationship",
     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $peopleCategories;

    /**
     * @var float
     *
     * @Assert\GreaterThanOrEqual(
     *     0,
     *     groups={"MixedReportKNS-2019"},
     *     message="to_need_positive_value"
     *     )
     *
     * @ORM\Column(name="incremental_costs", type="float", nullable=true)
     */
    protected $incrementalCosts;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="donation_balance_document", type="text", nullable=true)
     */
    private $donationBalanceDocument;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Document", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Result", mappedBy="employeesResultMixedReport", orphanRemoval=true, cascade={"all"})
     */
    protected $mixedEmployeeResults;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Result", mappedBy="beneficiariesResultMixedReport", orphanRemoval=true, cascade={"all"})
     */
    protected $mixedBeneficiariesResults;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Result", mappedBy="resultMixedReports", orphanRemoval=true, cascade={"all"})
     */
    protected $mixedResults;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add otherBeneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup
     *
     * @return Report
     */
    public function addOtherBeneficiaryGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups[] = $otherBeneficiaryGroup;
        $otherBeneficiaryGroup->setReport($this);

        return $this;
    }

    /**
     * Remove otherBeneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup
     */
    public function removeOtherBeneficiaryGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups->removeElement($otherBeneficiaryGroup);
    }

    /**
     * Get otherBeneficiaryGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherBeneficiaryGroups()
    {
        return $this->otherBeneficiaryGroups;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return Report
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;

        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Set incrementalCosts
     *
     * @param float $incrementalCosts
     *
     * @return Report
     */
    public function setIncrementalCosts($incrementalCosts)
    {
        $this->incrementalCosts = $incrementalCosts;

        return $this;
    }

    /**
     * Get incrementalCosts
     *
     * @return float
     */
    public function getIncrementalCosts()
    {
        return $this->incrementalCosts;
    }

    /**
     * Set donationBalanceDocument
     *
     * @param string $donationBalanceDocument
     *
     * @return Report
     */
    public function setDonationBalanceDocument($donationBalanceDocument)
    {
        $this->donationBalanceDocument = $donationBalanceDocument;

        return $this;
    }

    /**
     * Get donationBalanceDocument
     *
     * @return string
     */
    public function getDonationBalanceDocument()
    {
        return $this->donationBalanceDocument;
    }

    public function addDocument(Document $document)
    {
        $this->documents[] = $document;
        $document->setReport($this);

        return $this;
    }

    public function removeDocument(Document $document)
    {
        $this->documents->removeElement($document);

        return $this;
    }

    public function getDocuments()
    {
        return $this->documents;
    }

    public function addMixedEmployeeResult(Result $individualResult)
    {
        $this->mixedEmployeeResults[] = $individualResult;
        $individualResult->setEmployeesResultMixedReport($this);

        return $this;
    }

    public function removeMixedEmployeeResult(Result $individualResult)
    {
        $this->mixedEmployeeResults->removeElement($individualResult);

        return $this;
    }

    public function getMixedEmployeeResults()
    {
        return $this->mixedEmployeeResults;
    }

    public function addMixedBeneficiariesResult(Result $individualResult)
    {
        $this->mixedBeneficiariesResults[] = $individualResult;
        $individualResult->setBeneficiariesResultMixedReport($this);

        return $this;
    }

    public function removeMixedBeneficiariesResult(Result $individualResult)
    {
        $this->mixedBeneficiariesResults->removeElement($individualResult);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMixedBeneficiariesResults()
    {
        return $this->mixedBeneficiariesResults;
    }

    public function addMixedResult(Result $individualResult)
    {
        $this->mixedResults[] = $individualResult;
        $individualResult->setResultMixedReports($this);

        return $this;
    }

    public function removeMixedResult(Result $individualResult)
    {
        $this->mixedResults->removeElement($individualResult);
    }

    public function getMixedResults()
    {
        return $this->mixedResults;
    }
}
