<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.8.18
 * Time: 10.05
 */

namespace NKO\OrderBundle\Entity\Report\MixedReport\KNS;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Traits\MixedReport\AttachmentTrait;
use NKO\OrderBundle\Traits\MixedReport\MixedResultsTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\Report\OfficialsTrait;
use NKO\OrderBundle\Traits\Report\ProjectDescriptionTrait;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTrait;
use NKO\OrderBundle\Traits\ProjectResultsTrait;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MixedReport\KNS\ReportRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(name="mixed_report_kns_2018")
 */
class Report extends BaseReport
{
    use ProjectCardTrait;
    use FinanceReportTrait;
    use ProjectResultsTrait;
    use ProjectDescriptionTrait;
    use OfficialsTrait;
    use AttachmentTrait;
    use MixedResultsTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018"}
     * )
     *
     * @ORM\Column(name="donation_balance_document", type="text", nullable=true)
     */
    private $donationBalanceDocument;

    /**
     * @var float
     *
     * @Assert\GreaterThanOrEqual(
     *     0,
     *     groups={"MixedReportKNS-2018"},
     *     message="to_need_positive_value"
     *     )
     *
     * @ORM\Column(name="incremental_costs", type="float", nullable=true)
     */
    protected $incrementalCosts;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="mixed_report_people_category_relationship",
     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $peopleCategories;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup", mappedBy="report",
     *     orphanRemoval=true, cascade={"all"})
     */
    private $otherBeneficiaryGroups;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->peopleCategories = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document
     *
     * @return Report
     */
    public function addDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document)
    {
        $this->documents[] = $document;
        $document->setReport($this);

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set incrementalCosts
     *
     * @param float $incrementalCosts
     *
     * @return Report
     */
    public function setIncrementalCosts($incrementalCosts)
    {
        $this->incrementalCosts = $incrementalCosts;

        return $this;
    }

    /**
     * Get incrementalCosts
     *
     * @return float
     */
    public function getIncrementalCosts()
    {
        return $this->incrementalCosts;
    }

    /**
     * Set donationBalanceDocument
     *
     * @param string $donationBalanceDocument
     *
     * @return Report
     */
    public function setDonationBalanceDocument($donationBalanceDocument)
    {
        $this->donationBalanceDocument = $donationBalanceDocument;

        return $this;
    }

    /**
     * Get donationBalanceDocument
     *
     * @return string
     */
    public function getDonationBalanceDocument()
    {
        return $this->donationBalanceDocument;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return Report
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;

        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Add otherBeneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup
     *
     * @return Report
     */
    public function addOtherBeneficiaryGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups[] = $otherBeneficiaryGroup;
        $otherBeneficiaryGroup->setReport($this);

        return $this;
    }

    /**
     * Remove otherBeneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup
     */
    public function removeOtherBeneficiaryGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups->removeElement($otherBeneficiaryGroup);
    }

    /**
     * Get otherBeneficiaryGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherBeneficiaryGroups()
    {
        return $this->otherBeneficiaryGroups;
    }

    /**
     * @Assert\Callback(groups={"MixedReportKNS-2018"})
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPeopleCategories();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose people categories')
                ->atPath('peopleCategories')
                ->addViolation();
        }
    }
}
