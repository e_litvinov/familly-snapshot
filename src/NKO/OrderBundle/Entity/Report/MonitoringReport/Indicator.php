<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MonitoringReport\IndicatorRepository")
 * @ORM\Table(name="indicator")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Indicator implements IndicatorInterface
{
    const SEPARATOR = '|';

    public static $section = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $indexNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="indicator", cascade={"all"})
     */
    protected $monitoringResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id",  nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", inversedBy="indicators")
     * @ORM\JoinColumn(name="report_form_id", referencedColumnName="id")
     */
    private $reportForm;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $analyticTypes;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $monitoringType;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->monitoringResults = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getTitle();
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Indicator
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Indicator
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     *
     * @return Indicator
     */
    public function addMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults[] = $monitoringResult;

        return $this;
    }

    /**
     * Remove monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     */
    public function removeMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults->removeElement($monitoringResult);
    }

    /**
     * Get monitoringResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResults()
    {
        return $this->monitoringResults;
    }

    /**
     * Add child
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $child
     *
     * @return Indicator
     */
    public function addChild(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $child
     */
    public function removeChild(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $parent
     *
     * @return Indicator
     */
    public function setParent(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set indexNumber
     *
     * @param integer $indexNumber
     *
     * @return Indicator
     */
    public function setIndexNumber($indexNumber)
    {
        $this->indexNumber = $indexNumber;

        return $this;
    }

    /**
     * Get indexNumber
     *
     * @return integer
     */
    public function getIndexNumber()
    {
        return $this->indexNumber;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return Indicator
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }

    public function getParentCode()
    {
        $parent = $this->getParent();
        if ($parent) {
            return $parent->getCode();
        }
        return $this->code;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Indicator
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Indicator
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set analyticTypes
     *
     * @param array $types
     *
     * @return Indicator
     */
    public function setAnalyticTypes($types)
    {
        $this->analyticTypes = implode(self::SEPARATOR, $types);

        return $this;
    }

    /**
     * Get analyticTypes
     *
     * @return array
     */
    public function getAnalyticTypes()
    {
        return explode(self::SEPARATOR, $this->analyticTypes);
    }

    /**
     * Get analyticTypes
     * @param  $type        string
     * @param  $values      string
     *
     * @return bool
     */
    public static function isNeedAnalytic($values, $type)
    {
        return in_array($type, explode(self::SEPARATOR, $values));
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function getParentId()
    {
        if ($this->parent) {
            return $this->parent->getId();
        }
        return false;
    }

    public function setParentId($id)
    {
        return $this;
    }

    public function getTitleWithNumber()
    {
        if ($this->getParent()->getCode() == 'practice_implementation') {
            return ++self::$section.' '.$this->getTitle();
        }
        return self::$section.'.'.$this->getIndexNumber().' '.$this->getTitle();
    }

    /**
     * @param string $monitoringType
     */
    public function setMonitoringType($monitoringType)
    {
        $this->monitoringType = $monitoringType;

        return $this;
    }
}
