<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 16:15
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Traits\PreviousReportTrait;

/**
 * Report
 *
 * @ORM\Table(name="monitoring_prev_report")
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class PrevReport extends Report
{
    use PreviousReportTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}
