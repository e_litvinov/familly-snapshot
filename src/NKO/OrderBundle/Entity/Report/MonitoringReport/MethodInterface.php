<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

interface MethodInterface
{
    const TEST_CODE = 'test';
    const QUESTIONNAIRE_CODE = 'questioning';
    const INTERVIEW_CODE = 'interview';
    const OBSERVATION_CODE = 'observation';
    const EXPERT_REVIEW_CODE = 'expert_review';
    const FORM_CODE = 'form';
    const LIST_CODE = 'recipient_list';
    const DOCS_CODE = 'docs';
    const CUSTOM_CODE = 'custom';
    const NOT_CHOSEN = 'Не выбран';
}
