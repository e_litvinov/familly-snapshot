<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="monitoring_report_template")
 */
class ReportTemplate extends BaseReportTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="reportTemplate", cascade={"all"})
     */
    protected $monitoringResults;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->monitoringResults = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ReportTemplate
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     *
     * @return ReportTemplate
     */
    public function addMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults[] = $monitoringResult;

        return $this;
    }

    /**
     * Remove monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     */
    public function removeMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults->removeElement($monitoringResult);
    }

    /**
     * Get monitoringResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResults()
    {
        return $this->monitoringResults;
    }

    /**
     * Set applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     *
     * @return ReportTemplate
     */
    public function setApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory = null)
    {
        $this->applicationHistory = $applicationHistory;

        return $this;
    }

    /**
     * Get applicationHistory
     *
     * @return \NKO\OrderBundle\Entity\ApplicationHistory
     */
    public function getApplicationHistory()
    {
        return $this->applicationHistory;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return ReportTemplate
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }

    /**
     * Set author
     *
     * @param \NKO\UserBundle\Entity\NKOUser $author
     *
     * @return ReportTemplate
     */
    public function setAuthor(\NKO\UserBundle\Entity\NKOUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \NKO\UserBundle\Entity\NKOUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return ReportTemplate
     */
    public function addReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     */
    public function removeReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReports()
    {
        return $this->reports;
    }
}
