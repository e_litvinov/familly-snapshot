<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MonitoringReport\ReportRepository")
 * @ORM\Table(name="monitoring_report")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "monitoring_prev" = "NKO\OrderBundle\Entity\Report\MonitoringReport\PrevReport",
 *     })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Report extends BaseReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @CustomAssert\MonitoringResultPeriod
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="report", cascade={"all"})
     */
    protected $monitoringResults;

    /**
     *
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\PeriodReport")
     * @ORM\JoinTable(name="periods_monitoring_report")
     */
    protected $periods;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->monitoringResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     *
     * @return Report
     */
    public function addMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults[] = $monitoringResult;
        $monitoringResult->setReport($this);

        return $this;
    }

    /**
     * Remove monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     */
    public function removeMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults->removeElement($monitoringResult);
    }

    /**
     * Get monitoringResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResults()
    {
        return $this->monitoringResults;
    }

    /**
     * Get monitoringResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResultsByIndicatorsId($ids)
    {
        return $this->monitoringResults->filter(function ($result) use ($ids) {
            return in_array($result->getIndicator()->getId(), $ids);
        });
    }

    /**
     * Set reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     *
     * @return Report
     */
    public function setReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate = null)
    {
        $this->reportTemplate = $reportTemplate;

        return $this;
    }

    /**
     * Get reportTemplate
     *
     * @return \NKO\OrderBundle\Entity\Report\BaseReportTemplate
     */
    public function getReportTemplate()
    {
        return $this->reportTemplate;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document
     *
     * @return Report
     */
    public function addDocument(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document)
    {
        $this->documents[] = $document;
        $document->setReport($this);

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document)
    {
        $this->documents->removeElement($document);
        $document->setReport(null);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @return int
     */
    public function getAmountInMonitoringReport()
    {
        /** @var MonitoringResult $result */
        foreach ($this->getMonitoringResults() as $result) {
            $indicator = $result->getIndicator();

            if ($indicator->getParent() && $indicator->getParent()->getIndexNumber() == 15 && $indicator->getIndexNumber() == 1) {
                /** @var PeriodResult $periodResult */
                foreach ($result->getPeriodResultsWithSorting() as $periodResult) {
                    if ($periodResult->isFinalResult()) {
                        return $periodResult->getNewAmount();
                    }
                }
            }
        }

        return 0;
    }

    /**
     * Add period
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $period
     *
     * @return Report
     */
    public function addPeriod(\NKO\OrderBundle\Entity\PeriodReport $period)
    {
        $this->periods[] = $period;

        return $this;
    }

    /**
     * Remove period
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $period
     */
    public function removePeriod(\NKO\OrderBundle\Entity\PeriodReport $period)
    {
        $this->periods->removeElement($period);
    }

    /**
     * @param Indicator|null $indicator
     *
     * @return MonitoringResult|null
     */
    public function getMonitoringResultByIndicator(Indicator $indicator = null)
    {
        if (!$indicator) {
            return null;
        }

        $searchedResult = null;

        foreach ($this->monitoringResults as $result) {
            if ($result->getIndicator() == $indicator) {
                $searchedResult = $result;
            }
        }

        return $searchedResult;
    }
}
