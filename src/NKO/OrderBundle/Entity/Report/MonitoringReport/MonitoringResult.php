<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MonitoringReport\MonitoringResultRepository")
 * @ORM\Table(name="monitoring_result")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MonitoringResult
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator", inversedBy="monitoringResults", cascade={"persist"})
     * @ORM\JoinColumn(name="indicator_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $indicator;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult", mappedBy="monitoringResult", cascade={"all"})
     */
    protected $periodResults;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $totalValue;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Method", inversedBy="monitoringResults")
     * @ORM\JoinColumn(name="method_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $method;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Method")
     * @ORM\JoinTable(name="monitoring_results_methods",
     *      joinColumns={@ORM\JoinColumn(name="result_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")}
     * )
     */
    private $linkedMethods;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $customMethod;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="monitoringResults", cascade={"persist"})
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\ReportTemplate", inversedBy="monitoringResults")
     * @ORM\JoinColumn(name="report_template_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $reportTemplate;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument", mappedBy="monitoringResult")
     */
    protected $documents;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="practiceMonitoringResults")
     * @ORM\JoinColumn(name="practice_analytic_report_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $analyticReportLinkPractice;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report", inversedBy="introductionMonitoringResults")
     * @ORM\JoinColumn(name="introduction_analytic_report_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $analyticReportLinkIntroduction;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isVisible;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->periodResults = new ArrayCollection();
        $this->linkedMethods = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getIndicator();
    }

    public function getHumanizedMethods()
    {
        return $this->getHumanizedCollections($this->getLinkedMethods());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indicator
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator
     *
     * @return MonitoringResult
     */
    public function setIndicator(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator = null)
    {
        $this->indicator = $indicator;
        $indicator->addMonitoringResult($this);

        return $this;
    }

    /**
     * Get indicator
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Report $report
     *
     * @return MonitoringResult
     */
    public function setReport(BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Add periodResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult
     *
     * @return MonitoringResult
     */
    public function addPeriodResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult)
    {
        $this->periodResults[] = $periodResult;
        $periodResult->setMonitoringResult($this);

        return $this;
    }

    /**
     * Remove periodResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult
     */
    public function removePeriodResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult)
    {
        $this->periodResults->removeElement($periodResult);
    }

    /**
     * Get periodResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeriodResults()
    {
        if ($this->periodResults) {
            $periodResults = $this->periodResults->getValues();
            usort($periodResults, function ($a, $b) {
                $aPeriod = $a->getPeriod();
                $bPeriod = $b->getPeriod();
                if ($aPeriod && $bPeriod) {
                    return ($aPeriod->getStartDate() < $bPeriod->getStartDate()) ? -1 : 1;
                } elseif (!$aPeriod) {
                    return 1;
                }
            });

            $this->periodResults = new ArrayCollection($periodResults);
        }
        return $this->periodResults;
    }

    public function getPeriodResultsWithSorting()
    {
        $sortedPeriodResults = $this->periodResults;

        if ($sortedPeriodResults) {
            $periodResults = $sortedPeriodResults->getValues();
            usort($periodResults, function ($a, $b) {
                $aPeriod = $a->getPeriod();
                $bPeriod = $b->getPeriod();
                if ($aPeriod && $bPeriod) {
                    return ($aPeriod->getStartDate() < $bPeriod->getStartDate()) ? -1 : 1;
                } elseif (!$aPeriod) {
                    return 1;
                }
            });

            $sortedPeriodResults = new ArrayCollection($periodResults);
        }
        return $sortedPeriodResults;
    }

    /**
     * Set reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\ReportTemplate $reportTemplate
     *
     * @return MonitoringResult
     */
    public function setReportTemplate(\NKO\OrderBundle\Entity\Report\MonitoringReport\ReportTemplate $reportTemplate = null)
    {
        $this->reportTemplate = $reportTemplate;
        $reportTemplate->addMonitoringResult($this);

        return $this;
    }

    /**
     * Get reportTemplate
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\ReportTemplate
     */
    public function getReportTemplate()
    {
        return $this->reportTemplate;
    }

    /**
     * Set customMethod
     *
     * @param string $customMethod
     *
     * @return MonitoringResult
     */
    public function setCustomMethod($customMethod)
    {
        $this->customMethod = $customMethod;
        return $this;
    }


    /**
     * Get customMethod
     *
     * @return string
     */
    public function getCustomMethod()
    {
        return $this->customMethod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return MonitoringResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set totalValue
     *
     * @param integer $totalValue
     *
     * @return MonitoringResult
     */
    public function setTotalValue($totalValue)
    {
        $this->totalValue = $totalValue;

        return $this;
    }

    /**
     * Get totalValue
     *
     * @return integer
     */
    public function getTotalValue()
    {
        return $this->totalValue;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document
     *
     * @return MonitoringResult
     */
    public function addDocument(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set method
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Method $method
     *
     * @return MonitoringResult
     */
    public function setMethod(\NKO\OrderBundle\Entity\Report\MonitoringReport\Method $method = null)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\Method
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MonitoringResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set analyticReport
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $analyticReport
     *
     * @return MonitoringResult
     */
    public function setAnalyticReport(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $analyticReport = null)
    {
        $this->analyticReport = $analyticReport;

        return $this;
    }

    /**
     * Get analyticReport
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getAnalyticReport()
    {
        return $this->analyticReport;
    }

    public function getFinalPeriodResult()
    {
        $finalPeriodResult = $this->periodResults;
        if (!$this->periodResults->isEmpty()) {
            $finalPeriodResult = $this->periodResults->filter(
                function ($periodResult) {
                    return $periodResult->isFinalResult();
                }
            );
        }

        return $finalPeriodResult;
    }

    public function setFinalPeriodResult($finalPeriodResult)
    {
        return $this;
    }

    /**
     * Set analyticReportLinkPractice
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $analyticReportLinkPractice
     *
     * @return MonitoringResult
     */
    public function setAnalyticReportLinkPractice(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $analyticReportLinkPractice = null)
    {
        $this->analyticReportLinkPractice = $analyticReportLinkPractice;

        return $this;
    }

    /**
     * Get analyticReportLinkPractice
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getAnalyticReportLinkPractice()
    {
        return $this->analyticReportLinkPractice;
    }

    /**
     * Set analyticReportLinkIntroduction
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report $analyticReportLinkIntroduction
     *
     * @return MonitoringResult
     */
    public function setAnalyticReportLinkIntroduction(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report $analyticReportLinkIntroduction = null)
    {
        $this->analyticReportLinkIntroduction = $analyticReportLinkIntroduction;

        return $this;
    }

    /**
     * Get analyticReportLinkIntroduction
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\Report
     */
    public function getAnalyticReportLinkIntroduction()
    {
        return $this->analyticReportLinkIntroduction;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     *
     * @return MonitoringResult
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * Add linkedMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Method $linkedMethod
     *
     * @return MonitoringResult
     */
    public function addLinkedMethod(\NKO\OrderBundle\Entity\Report\MonitoringReport\Method $linkedMethod)
    {
        $this->linkedMethods[] = $linkedMethod;

        return $this;
    }

    /**
     * Remove linkedMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Method $linkedMethod
     */
    public function removeLinkedMethod(\NKO\OrderBundle\Entity\Report\MonitoringReport\Method $linkedMethod)
    {
        $this->linkedMethods->removeElement($linkedMethod);
    }

    /**
     * Get linkedMethods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinkedMethods()
    {
        return $this->linkedMethods;
    }
}
