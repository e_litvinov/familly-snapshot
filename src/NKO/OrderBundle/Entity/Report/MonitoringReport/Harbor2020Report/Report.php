<?php

namespace NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="harbor_2020_monitoring_report")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Report extends BaseReport
{
    /**
     * @CustomAssert\MonitoringResultPeriod(extraIndexes={7: {1, 2, 11}, 14 : {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} })
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="report", cascade={"all"})
     */
    protected $monitoringResults;

    /**
     *
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument", mappedBy="harbor2020Report", cascade={"all"}, orphanRemoval=true)
     */
    protected $documents;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\PeriodReport")
     * @ORM\JoinTable(name="periods_monitoring_harbor_2020_report")
     */
    protected $periods;


    public function __construct()
    {
        $this->monitoringResults = new ArrayCollection();
    }

    public function addMonitoringResult(MonitoringResult $monitoringResult)
    {
        $this->monitoringResults[] = $monitoringResult;
        $monitoringResult->setReport($this);

        return $this;
    }

    public function removeMonitoringResult(MonitoringResult $monitoringResult)
    {
        $this->monitoringResults->removeElement($monitoringResult);

        return $this;
    }

    public function getMonitoringResults()
    {
        return $this->monitoringResults;
    }

    public function getMonitoringResultByIndicator(Indicator $indicator = null)
    {
        if (!$indicator) {
            return null;
        }

        $searchedResult = null;

        foreach ($this->monitoringResults as $result) {
            if ($result->getIndicator() == $indicator) {
                $searchedResult = $result;
            }
        }

        return $searchedResult;
    }

    public function addDocument(MonitoringDocument $document)
    {
        $this->documents[] = $document;
        $document->setHarbor2020Report($this);

        return $this;
    }

    public function removeDocument(MonitoringDocument $document)
    {
        $this->documents->removeElement($document);
        $document->setHarbor2020Report(null);
    }

    public function getDocuments()
    {
        return $this->documents;
    }

    public function addPeriod(PeriodReport $period)
    {
        $this->periods[] = $period;

        return $this;
    }

    public function removePeriod(PeriodReport $period)
    {
        $this->periods->removeElement($period);
    }
}
