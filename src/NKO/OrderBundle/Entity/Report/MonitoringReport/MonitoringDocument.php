<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 7/24/17
 * Time: 10:45 AM
 */
namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;
use NKO\OrderBundle\Traits\FieldGetter;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="monitoring_document")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @CustomAssert\EntityWithFile(
 *     extensions={"pdf", "excel", "doc"},
 *     fileNames={
 *       "file"
 *     },
 *     notNull=false,
 *     groups={"Default"}
 * )
 */
class MonitoringDocument
{
    use FieldGetter;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @DocumentAssert\File(
     *     maxSize = "50000k",
     * )
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    protected $_file;

    /**
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)")
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", inversedBy="files")
     * @ORM\JoinColumn(name="monitoring_result_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $monitoringResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Report", inversedBy="documents")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $report;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report", inversedBy="documents")
     * @ORM\JoinColumn(name="harbor_2020_report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $harbor2020Report;

    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)")
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\PeriodReport", inversedBy="monitoringDocuments")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $period;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    public function __toString()
    {
        return (string) $this->monitoringResult;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return MonitoringDocument
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }

        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set report
     *
     * @param BaseReport $report
     *
     * @return MonitoringDocument
     */
    public function setReport(BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }
    public function setHarbor2020Report(BaseReport $report = null)
    {
        $this->harbor2020Report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return BaseReport
     */
    public function getHarbor2020Report()
    {
        return $this->harbor2020Report;
    }

    /**
     * Set monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     *
     * @return MonitoringDocument
     */
    public function setMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult = null)
    {
        $this->monitoringResult = $monitoringResult;

        return $this;
    }

    /**
     * Get monitoringResult
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult
     */
    public function getMonitoringResult()
    {
        return $this->monitoringResult;
    }

    /**
     * Set period
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $period
     *
     * @return MonitoringDocument
     */
    public function setPeriod(\NKO\OrderBundle\Entity\PeriodReport $period = null)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \NKO\OrderBundle\Entity\PeriodReport
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MonitoringDocument
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
