<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/20/17
 * Time: 4:32 PM
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

interface IndicatorInterface
{
    const PRACTICE_IMPL_CODE = 'practice_implementation';
    const PRACTICE_DISS_CODE = 'practice_dissemination';
    const INDIVIDUAL_RES_CODE = 'individual_results';
    const TOTAL_CODE = 'total';
    const FAMILY_CODE = 'family';
    const CHILDREN_CODE = 'children';
    const GRADUATES_CODE = 'graduates';
    const PARENT_CODE = 'parent';
    const EXPERT_CODE = 'expert';
    const ORGANIZATION_CODE = 'organization';
    const PRE_CUSTOM_VALUE = 'pre_custom';
    const CUSTOM_VALUE = 'custom';

    const INDIVIDUAL_RES_COUNT = 16;

    const SOCIAL_TYPE = 'social';
    const PROD_TYPE = 'prod';
    const NEWMONITORINGINDICATORTYPE = 'newMonitoringIndicatorType';
}
