<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="data_collection_method")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Method implements MethodInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $code;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="method", cascade={"all"})
     */
    protected $monitoringResults;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->monitoringResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Method
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Method
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     *
     * @return Method
     */
    public function addMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults[] = $monitoringResult;

        return $this;
    }

    /**
     * Remove monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     */
    public function removeMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult)
    {
        $this->monitoringResults->removeElement($monitoringResult);
    }

    /**
     * Get monitoringResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResults()
    {
        return $this->monitoringResults;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Method
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
