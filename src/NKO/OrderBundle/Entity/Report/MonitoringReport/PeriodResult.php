<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report\MonitoringReport;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\MonitoringReport\PeriodResultRepository")
 * @ORM\Table(name="period_result")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class PeriodResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\PeriodReport", inversedBy="periodResults")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $period;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $totalAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $newAmount;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isFinalResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", inversedBy="periodResults")
     * @ORM\JoinColumn(name="monitoring_result_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $monitoringResult;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    public function __toString()
    {
        return (string) $this->totalAmount;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set period
     *
     * @param string $period
     *
     * @return PeriodResult
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set totalAmount
     *
     * @param integer $totalAmount
     *
     * @return PeriodResult
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get totalAmount
     *
     * @return integer
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set newAmount
     *
     * @param integer $newAmount
     *
     * @return PeriodResult
     */
    public function setNewAmount($newAmount)
    {
        $this->newAmount = $newAmount;

        return $this;
    }

    /**
     * Get newAmount
     *
     * @return integer
     */
    public function getNewAmount()
    {
        return $this->newAmount;
    }

    /**
     * Set isFinalResult
     *
     * @param integer $isFinalResult
     *
     * @return PeriodResult
     */
    public function setIsFinalResult($isFinalResult)
    {
        $this->isFinalResult = $isFinalResult;

        return $this;
    }

    /**
     * Get isFinalResult
     *
     * @return integer
     */
    public function getIsFinalResult()
    {
        return $this->isFinalResult;
    }

    /**
     * Set monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult
     *
     * @return PeriodResult
     */
    public function setMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $monitoringResult = null)
    {
        $this->monitoringResult = $monitoringResult;

        return $this;
    }

    /**
     * Get monitoringResult
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult
     */
    public function getMonitoringResult()
    {
        return $this->monitoringResult;
    }

    public function isFinalResult()
    {
        return $this->isFinalResult;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return PeriodResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
