<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.05.17
 * Time: 12:00
 */

namespace NKO\OrderBundle\Entity\Report;

use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Report\BaseReportTemplateRepository")
 * @ORM\Table(name="base_report_template")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="report_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "monitoring_template" = "NKO\OrderBundle\Entity\Report\MonitoringReport\ReportTemplate",
 *     "finance_template" = "NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate",
 *     "analytic_template" = "NKO\OrderBundle\Entity\Report\AnalyticReport\ReportTemplate",
 *     })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
abstract class BaseReportTemplate
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", inversedBy="reportTemplates")
     * @ORM\JoinColumn(name="application_history_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $applicationHistory;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", inversedBy="reportTemplates")
     * @ORM\JoinColumn(name="report_form_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    protected $reportForm;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\NKOUser", inversedBy="reportTemplates")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    protected $author;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BaseReport", mappedBy="reportTemplate")
     */
    protected $reports;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_change", type="boolean", nullable=true,  options={"default" : 0})
     */

    private $isChanged;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_plan_accepted", type="boolean", nullable=true,  options={"default" : 0})
     */

    private $isPlanAccepted;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_plan_declined", type="boolean", nullable=true,  options={"default" : 0})
     */

    private $isPlanDeclined;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    function __toString()
    {
        return (string) $this->getAuthor();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return BaseReportTemplate
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     *
     * @return BaseReportTemplate
     */
    public function setApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory = null)
    {
        $this->applicationHistory = $applicationHistory;

        return $this;
    }

    /**
     * Get applicationHistory
     *
     * @return \NKO\OrderBundle\Entity\ApplicationHistory
     */
    public function getApplicationHistory()
    {
        return $this->applicationHistory;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return BaseReportTemplate
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }

    /**
     * Set author
     *
     * @param \NKO\UserBundle\Entity\NKOUser $author
     *
     * @return BaseReportTemplate
     */
    public function setAuthor(\NKO\UserBundle\Entity\NKOUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \NKO\UserBundle\Entity\NKOUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return BaseReportTemplate
     */
    public function addReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     */
    public function removeReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Set isPlanAccepted
     *
     * @param boolean $isPlanAccepted
     *
     * @return BaseReportTemplate
     */
    public function setIsPlanAccepted($isPlanAccepted)
    {
        $this->isPlanAccepted = $isPlanAccepted;

        return $this;
    }

    /**
     * Get isPlanAccepted
     *
     * @return boolean
     */
    public function getIsPlanAccepted()
    {
        return $this->isPlanAccepted;
    }

    /**
     * Set isChanged
     *
     * @param boolean $isChanged
     *
     * @return BaseReportTemplate
     */
    public function setIsChanged($isChanged)
    {
        $this->isChanged = $isChanged;

        return $this;
    }

    /**
     * Get isChanged
     *
     * @return boolean
     */
    public function getIsChanged()
    {
        return $this->isChanged;
    }

    /**
     * Set isPlanDeclined
     *
     * @param boolean $isPlanDeclined
     *
     * @return BaseReportTemplate
     */
    public function setIsPlanDeclined($isPlanDeclined)
    {
        $this->isPlanDeclined = $isPlanDeclined;

        return $this;
    }

    /**
     * Get isPlanDeclined
     *
     * @return boolean
     */
    public function getIsPlanDeclined()
    {
        return $this->isPlanDeclined;
    }
}
