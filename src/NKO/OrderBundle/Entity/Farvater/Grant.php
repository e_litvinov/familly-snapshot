<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Grant
 *
 * @ORM\Table(name="farvater_grant")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\GrantRepository")
 */
class Grant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="project_name", type="text", nullable=true)
     */
    private $projectName;

    /**
     * @var string
     *
     * @ORM\Column(name="implementation_period", type="string", nullable=true)
     */
    private $implementationPeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="financing_organization_name", type="string", nullable=true)
     */
    private $financingOrganizationName;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="grants", cascade={"persist"})
     *
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="NKO\OrderBundle\Entity\BriefApplication2016\Application",
     *     inversedBy="grants", cascade={"persist"}
     *     )
     * @ORM\JoinColumn(
     *     name="brief_application_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     *     )
     *
     */
    private $bitrixBriefApplication;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     *
     * @return Grant
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set implementationPeriod
     *
     * @param string $implementationPeriod
     *
     * @return Grant
     */
    public function setImplementationPeriod($implementationPeriod)
    {
        $this->implementationPeriod = $implementationPeriod;

        return $this;
    }

    /**
     * Get implementationPeriod
     *
     * @return string
     */
    public function getImplementationPeriod()
    {
        return $this->implementationPeriod;
    }

    /**
     * Set financingOrganizationName
     *
     * @param string $financingOrganizationName
     *
     * @return Grant
     */
    public function setFinancingOrganizationName($financingOrganizationName)
    {
        $this->financingOrganizationName = $financingOrganizationName;

        return $this;
    }

    /**
     * Get financingOrganizationName
     *
     * @return string
     */
    public function getFinancingOrganizationName()
    {
        return $this->financingOrganizationName;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return Grant
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set bitrixBriefApplication
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2016\Application $bitrixBriefApplication
     *
     * @return Grant
     */
    public function setBitrixBriefApplication(\NKO\OrderBundle\Entity\BriefApplication2016\Application $bitrixBriefApplication = null)
    {
        $this->bitrixBriefApplication = $bitrixBriefApplication;

        return $this;
    }

    /**
     * Get bitrixBriefApplication
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2016\Application
     */
    public function getBitrixBriefApplication()
    {
        return $this->bitrixBriefApplication;
    }
}
