<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Plan
 *
 * @ORM\MappedSuperclass()
 */
class Plan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="text", nullable=true)
     */
    private $activity;

    /**
     * @var string
     *
     * @ORM\Column(name="term", type="string", nullable=true)
     */
    private $term;

    /**
     * @var string
     *
     * @ORM\Column(name="expected_result", type="text", nullable=true)
     */
    private $expectedResult;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return Plan
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set term
     *
     * @param string $term
     *
     * @return Plan
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set expectedResult
     *
     * @param string $expectedResult
     *
     * @return PLan
     */
    public function setExpectedResult($expectedResult)
    {
        $this->expectedResult = $expectedResult;

        return $this;
    }

    /**
     * Get expectedResult
     *
     * @return string
     */
    public function getExpectedResult()
    {
        return $this->expectedResult;
    }
}
