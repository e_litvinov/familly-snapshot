<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Introduction
 *
 * @ORM\Table(name="farvater_introduction")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\IntroductionRepository")
 */
class Introduction extends BaseResultMeasure
{
    /**
     * @ORM\ManyToOne(targetEntity="IntroductionIndex", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="index_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $index;

    /**
     * @ORM\Column(name="other_index", type="string", length=500, nullable=true)
     */
    private $otherIndex;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="introductions", cascade={"persist"}
     *     )
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $application;
    
    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set index
     *
     * @param \NKO\OrderBundle\Entity\Farvater\IntroductionIndex $index
     *
     * @return Introduction
     */
    public function setIndex(\NKO\OrderBundle\Entity\Farvater\IntroductionIndex $index = null)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return \NKO\OrderBundle\Entity\Farvater\IntroductionIndex
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return Introduction
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Set otherIndex
     *
     * @param string $otherIndex
     *
     * @return Introduction
     */
    public function setOtherIndex($otherIndex)
    {
        $this->otherIndex = $otherIndex;

        return $this;
    }

    /**
     * Get otherIndex
     *
     * @return string
     */
    public function getOtherIndex()
    {
        return $this->otherIndex;
    }
}
