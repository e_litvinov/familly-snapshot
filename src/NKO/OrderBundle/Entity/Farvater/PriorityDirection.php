<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PriorityDirection
 *
 * @ORM\Table(name="farvater_priority_direction")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\PriorityDirectionRepository")
 */
class PriorityDirection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string",
     *      length=500, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="application_type", type="string",
     *      length=50, nullable=false)
     */
    private $applicationType;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PriorityDirection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set applicationType
     *
     * @param string $applicationType
     *
     * @return PriorityDirection
     */
    public function setApplicationType($applicationType)
    {
        $this->applicationType = $applicationType;

        return $this;
    }

    /**
     * Get applicationType
     *
     * @return string
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }

    public function __toString()
    {
        return $this->name ? (string)$this->name : 'priority direction';
    }
}
