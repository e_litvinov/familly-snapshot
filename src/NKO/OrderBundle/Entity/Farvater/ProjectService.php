<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectService
 *
 * @ORM\Table(name="farvater_project_service")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\ProjectServiceRepository")
 */
class ProjectService extends BaseResultMeasure
{
    /**
     * @var string
     *
     * @ORM\Column(name="service", type="text", nullable=true)
     */
    private $service;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="projectServices", cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Set service
     *
     * @param string $service
     *
     * @return ProjectService
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return ProjectService
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }
}
