<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BaseResultMeasure
 *
 * @ORM\MappedSuperclass
 */
class BaseResultMeasure
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="target_value", type="text", nullable=true)
     */
    private $targetValue;

    /**
     * @var string
     *
     * @ORM\Column(name="approximate_value", type="text", nullable=true)
     */
    private $approximateValue;

    /**
     * @var string
     *
     * @ORM\Column(name="measure_method", type="text", nullable=true)
     */
    private $measureMethod;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set targetValue
     *
     * @param string $targetValue
     *
     * @return BaseResultMeasure
     */
    public function setTargetValue($targetValue)
    {
        $this->targetValue = $targetValue;

        return $this;
    }

    /**
     * Get targetValue
     *
     * @return string
     */
    public function getTargetValue()
    {
        return $this->targetValue;
    }

    /**
     * Set approximateValue
     *
     * @param string $approximateValue
     *
     * @return BaseResultMeasure
     */
    public function setApproximateValue($approximateValue)
    {
        $this->approximateValue = $approximateValue;

        return $this;
    }

    /**
     * Get approximateValue
     *
     * @return string
     */
    public function getApproximateValue()
    {
        return $this->approximateValue;
    }

    /**
     * Set measureMethod
     *
     * @param string $measureMethod
     *
     * @return BaseResultMeasure
     */
    public function setMeasureMethod($measureMethod)
    {
        $this->measureMethod = $measureMethod;

        return $this;
    }

    /**
     * Get measureMethod
     *
     * @return string
     */
    public function getMeasureMethod()
    {
        return $this->measureMethod;
    }
}
