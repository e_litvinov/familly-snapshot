<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Publication
 *
 * @ORM\Table(name="farvater_publication")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\PublicationRepository")
 */
class Publication extends BaseResultMeasure
{
    /**
     * @ORM\ManyToOne(targetEntity="PublicationIndex", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="index_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $index;

    /**
     * @ORM\Column(name="other_index", type="string", length=500, nullable=true)
     */
    private $otherIndex;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="publications", cascade={"persist"}
     *     )
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $application;

    /**
     * Set index
     *
     * @param ParticipationIndex $index
     *
     * @return Publication
     */
    public function setIndex(PublicationIndex $index = null)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return \NKO\OrderBundle\Entity\Farvater\PublicationIndex
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set otherIndex
     *
     * @param string $otherIndex
     *
     * @return Publication
     */
    public function setOtherIndex($otherIndex)
    {
        $this->otherIndex = $otherIndex;

        return $this;
    }

    /**
     * Get otherIndex
     *
     * @return string
     */
    public function getOtherIndex()
    {
        return $this->otherIndex;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return Publication
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }
}
