<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectMember
 *
 * @ORM\Table(name="farvater_project_member")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\ProjectMemberRepository")
 */
class ProjectMember
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", nullable=true)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="functional_responsibilities", type="text", nullable=true)
     */
    private $functionalResponsibilities;

    /**
     * @ORM\ManyToOne(targetEntity="EmploymentRelationship", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="relationship_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $employmentRelationship;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_information", type="text", nullable=true)
     */
    private $personalInformation;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="projectMembers", cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return ProjectMember
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return ProjectMember
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set functionalResponsibilities
     *
     * @param string $functionalResponsibilities
     *
     * @return ProjectMember
     */
    public function setFunctionalResponsibilities($functionalResponsibilities)
    {
        $this->functionalResponsibilities = $functionalResponsibilities;

        return $this;
    }

    /**
     * Get functionalResponsibilities
     *
     * @return string
     */
    public function getFunctionalResponsibilities()
    {
        return $this->functionalResponsibilities;
    }

    /**
     * Set personalInformation
     *
     * @param string $personalInformation
     *
     * @return ProjectMember
     */
    public function setPersonalInformation($personalInformation)
    {
        $this->personalInformation = $personalInformation;

        return $this;
    }

    /**
     * Get personalInformation
     *
     * @return string
     */
    public function getPersonalInformation()
    {
        return $this->personalInformation;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return ProjectMember
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set employmentRelationship
     *
     * @param \NKO\OrderBundle\Entity\Farvater\EmploymentRelationship $employmentRelationship
     *
     * @return ProjectMember
     */
    public function setEmploymentRelationship(\NKO\OrderBundle\Entity\Farvater\EmploymentRelationship $employmentRelationship = null)
    {
        $this->employmentRelationship = $employmentRelationship;

        return $this;
    }

    /**
     * Get employmentRelationship
     *
     * @return \NKO\OrderBundle\Entity\Farvater\EmploymentRelationship
     */
    public function getEmploymentRelationship()
    {
        return $this->employmentRelationship;
    }
}
