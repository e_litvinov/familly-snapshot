<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Region;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;

/**
 * FarvaterApplication
 *
 * @ORM\Table(name="farvater_application")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\ApplicationRepository")
 */
class FarvaterApplication extends BaseApplication
{
    use OrganizationInfoTrait;

    /**-
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Grant", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $grants;

    /**
     * @ORM\OneToMany(
     *     targetEntity="PublicationMaterial", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $publicationMaterials;

    /**
     * @ORM\ManyToMany(targetEntity="PriorityDirection", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="farvater_application_priority_direction",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $priorityDirections;

    /**
     * @var string
     *
     * @ORM\Column(name="priority_direction_etc", type="text", nullable=true)
     */
    private $priorityDirectionEtc;

    /**
     * @ORM\ManyToMany(targetEntity="TargetGroup", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="farvater_application_target_group",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    private $targetGroups;

    /**
     * @var string
     *
     * @ORM\Column(name="target_group_etc", type="text", nullable=true)
     */
    private $targetGroupEtc;

    /**
     * @var string
     *
     * @ORM\Column(name="child_protection_specialists", type="text", nullable=true)
     */
    private $childProtectionSpecialists;

    /**
     * @ORM\ManyToMany(targetEntity="SocialResult", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="farvater_application_social_result",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="result_id", referencedColumnName="id")}
     * )
     */
    private $socialResults;

    /**
     * @var string
     *
     * @ORM\Column(name="social_result_etc", type="text", nullable=true)
     */
    private $socialResultEtc;

    /**
     * @var string
     * @ORM\Column(name="practice_preview", type="text", nullable=true)
     */
    private $practicePreview;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_preview_comment", type="text", nullable=true)
     */
    private $practicePreviewComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_evidence", type="text", nullable=true)
     */
    private $practiceEvidence;

    /**
    * @var string
    *
    * @ORM\Column(name="practice_evidence_comment", type="text", nullable=true)
    */
    private $practiceEvidenceComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_purpose", type="text", nullable=true)
     */
    private $practicePurpose;

    /**
    * @var string
    *
    * @ORM\Column(name="practice_purpose_comment", type="text", nullable=true)
    */
    private $practicePurposeComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_mechanism", type="text", nullable=true)
     */
    private $practiceMechanism;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_mechanism_comment", type="text", nullable=true)
     */
    private $practiceMechanismComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_results", type="text", nullable=true)
     */
    private $practiceResults;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Region", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="practice_region_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $practiceRegion;

    /**
    * @var string
    *
    * @ORM\Column(name="practice_region_etc", type="string", length=500, nullable=true)
    */
    private $practiceRegionEtc;

    /**
     * @ORM\ManyToMany(targetEntity="TargetAudience", fetch="EAGER")
     * @ORM\JoinTable(
     *     name="farvater_application_target_audience",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="audience_id", referencedColumnName="id")}
     * )
     */
    private $targetAudiences;

    /**
     * @var string
     *
     * @ORM\Column(name="target_audience_etc", type="text", nullable=true)
     */
    private $targetAudienceEtc;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_motivation", type="text", nullable=true)
     */
    private $practiceMotivation;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_tech", type="text", nullable=true)
     */
    private $practiceTech;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_tech_comment", type="text", nullable=true)
     */
    private $practiceTechComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_expecting_spread_results", type="text", nullable=true)
     */
    private $practiceExpectingSpreadResults;

    /**
     * @var string
     *
     * @ORM\Column(name="organizational_capacity", type="text", nullable=true)
     */
    private $organizationalCapacity;

    /**
     * @var string
     *
     * @ORM\Column(name="organizational_capacity_comment", type="text", nullable=true)
     */
    private $organizationalCapacityComment;

    /**
     * @var string
     *
     * @ORM\Column(name="monitoring_comment", type="text", nullable=true)
     */
    private $monitoringComment;

    /**
     * @var string
     *
     * @ORM\Column(name="authority_director_document", type="string", length=255, nullable=true)
     */
    private $authorityDirectorDocument;

    private $_authorityDirectorDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="authority_manager_document", type="string", length=255, nullable=true)
     */
    private $authorityManagerDocument;

    private $_authorityManagerDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="first_section_comment", type="text", nullable=true)
     */
    private $firstSectionComment;

    /**
     * @var string
     *
     * @ORM\Column(name="second_section_comment", type="text", nullable=true)
     */
    private $secondSectionComment;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ProjectService", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $projectServices;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ParticipatingTargetGroup", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $participatingTargetGroups;

    /**
     * @var string
     *
     * @ORM\Column(name="immediate_results_comment", type="text", nullable=true)
     */
    private $immediateResultsComment;

    /**
     * @var string
     *
     * @ORM\Column(name="social_results_comment", type="text", nullable=true)
     */
    private $socialResultsComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_expecting_results_stability", type="text", nullable=true)
     */
    private $practiceExpectingResultsStability;

    /**
     * @var string
     *
     * @ORM\Column(name="third_section_comment", type="text", nullable=true)
     */
    private $thirdSectionComment;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_spread_purpose", type="text", nullable=true)
     */
    private $practiceSpreadPurpose;

    /**
     * @ORM\OneToMany(
     *     targetEntity="AwareTargetGroup", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $awareTargetGroups;

    /**
     * @ORM\OneToMany(
     *     targetEntity="SpreadActivity", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $spreadActivities;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Participation", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $participations;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Introduction", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $introductions;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Publication", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $publications;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_spread_comment", type="text", nullable=true)
     */
    private $practiceSpreadComment;

    /**
     * @var string
     *
     * @ORM\Column(name="organization_results", type="text", nullable=true)
     */
    private $organizationResults;

    /**
     * @var string
     *
     * @ORM\Column(name="practice_results_stability", type="text", nullable=true)
     */
    private $practiceResultsStability;

    /**
     * @var string
     *
     * @ORM\Column(name="fourth_section_comment", type="text", nullable=true)
     */
    private $fourthSectionComment;

    /**
     * @var string
     *
     * @ORM\Column(name="monitoring_purpose", type="text", nullable=true)
     */
    private $monitoringPurpose;

    /**
     * @var string
     *
     * @ORM\Column(name="monitoring_review", type="text", nullable=true)
     */
    private $monitoringReview;

    /**
     * @var string
     *
     * @ORM\Column(name="expected_monitoring_results", type="text", nullable=true)
     */
    private $expectedMonitoringResults;

    /**
     * @ORM\OneToMany(
     *     targetEntity="RealizationPlan", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $realizationPlans;

    /**
     * @ORM\OneToMany(
     *     targetEntity="SpreadPlan", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $spreadPlans;

    /**
     * @ORM\OneToMany(
     *     targetEntity="MonitoringPlan", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $monitoringPlans;

    /**
     * @var string
     *
     * @ORM\Column(name="base_activity_comment", type="text", nullable=true)
     */
    private $baseActivityComment;

    /**
     * @var string
     *
     * @ORM\Column(name="task_realization", type="text", nullable=true)
     */
    private $taskRealization;

    /**
     * @var string
     *
     * @ORM\Column(name="task_spread", type="text", nullable=true)
     */
    private $taskSpread;

    /**
     * @var string
     *
     * @ORM\Column(name="task_monitoring", type="text", nullable=true)
     */
    private $taskMonitoring;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ProjectMember", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $projectMembers;

    /**
     * @ORM\OneToMany(
     *     targetEntity="OrganizationResource", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $organizationResources;

    /**
     * @ORM\OneToMany(
     *     targetEntity="ProjectPartner", mappedBy="application",
     *     cascade={"all"}, orphanRemoval=true
     *     )
     */
    private $projectPartners;

    /**
     * @var string
     *
     * @ORM\Column(name="full_budget", type="string", nullable=true)
     */
    private $fullBudget;

    /**
     * @var string
     *
     * @ORM\Column(name="budget_year", type="string", nullable=true)
     */
    private $budgetYear;

    /**
     * @var string
     *
     * @ORM\Column(name="budget_document", type="string", length=255, nullable=true)
     */
    private $budgetDocument;

    private $_budgetDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="ur_status_document", type="string", length=255, nullable=true)
     */
    private $urStatusDocument;

    private $_urStatusDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="last_year_report_document", type="string", length=255, nullable=true)
     */
    private $lastYearReportDocument;

    private $_lastYearReportDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="last_year_report_link", type="text", nullable=true)
     */
    private $lastYearReportLink;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_bank_document", type="string", length=255, nullable=true)
     */
    private $mailBankDocument;

    private $_mailBankDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="mu_report_mail", type="string", length=255, nullable=true)
     */
    private $muReportMail;

    private $_muReportMail;

    /**
     * @var string
     *
     * @ORM\Column(name="mu_report_mail_link", type="text", nullable=true)
     */
    private $muReportMailLink;

    /**
     * @var string
     *
     * @ORM\Column(name="disabilities_children", type="text", nullable=true)
     */
    private $disabilitiesChildren;

    /**
     * @var string
     *
     * @ORM\Column(name="special_needs_education_children", type="text", nullable=true)
     */
    private $specialNeedsEducationChildren;

    /**
     * @var string
     * @ORM\Column(name="sending_application_person_full_name", type="string", length=255, nullable=true)
     */
    private $sendingApplicationPersonFullName;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_correct_info", type="boolean", nullable=true)
     */
    private $isCorrectInfo;

    /**
     * @var bool
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;

    public function __construct() {
        parent::__construct();

        $this->grants = new ArrayCollection();
        $this->priorityDirections = new ArrayCollection();
        $this->targetGroups = new ArrayCollection();
        $this->socialResults = new ArrayCollection();
        $this->targetAudiences = new ArrayCollection();
        $this->projectMembers = new ArrayCollection();
        $this->organizationResources = new ArrayCollection();
        $this->projectPartners = new ArrayCollection();
        $this->projectServices = new ArrayCollection();
        $this->spreadActivities = new ArrayCollection();
        $this->participations = new ArrayCollection();
        $this->introductions = new ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->realizationPlans = new ArrayCollection();
        $this->spreadPlans = new ArrayCollection();
        $this->monitoringPlans = new ArrayCollection();
        $this->awareTargetGroups = new ArrayCollection();
        $this->participatingTargetGroups = new ArrayCollection();
        $this->publicationMaterials = new ArrayCollection();
        $this->isCorrectInfo = true;
        $this->projects = new ArrayCollection();
        $this->siteLinks = new ArrayCollection();
        $this->socialNetworkLinks = new ArrayCollection();
        $this->isAddressesEqual = true;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set priorityDirectionEtc
     *
     * @param string $priorityDirectionEtc
     *
     * @return FarvaterApplication
     */
    public function setPriorityDirectionEtc($priorityDirectionEtc)
    {
        $this->priorityDirectionEtc = $priorityDirectionEtc;

        return $this;
    }

    /**
     * Get priorityDirectionEtc
     *
     * @return string
     */
    public function getPriorityDirectionEtc()
    {
        return $this->priorityDirectionEtc;
    }

    /**
     * Set targetGroupEtc
     *
     * @param string $targetGroupEtc
     *
     * @return FarvaterApplication
     */
    public function setTargetGroupEtc($targetGroupEtc)
    {
        $this->targetGroupEtc = $targetGroupEtc;

        return $this;
    }

    /**
     * Get targetGroupEtc
     *
     * @return string
     */
    public function getTargetGroupEtc()
    {
        return $this->targetGroupEtc;
    }

    /**
     * Set socialResultEtc
     *
     * @param string $socialResultEtc
     *
     * @return FarvaterApplication
     */
    public function setSocialResultEtc($socialResultEtc)
    {
        $this->socialResultEtc = $socialResultEtc;

        return $this;
    }

    /**
     * Get socialResultEtc
     *
     * @return string
     */
    public function getSocialResultEtc()
    {
        return $this->socialResultEtc;
    }

    /**
     * Set practicePreview
     *
     * @param string $practicePreview
     *
     * @return FarvaterApplication
     */
    public function setPracticePreview($practicePreview)
    {
        $this->practicePreview = $practicePreview;

        return $this;
    }

    /**
     * Get practicePreview
     *
     * @return string
     */
    public function getPracticePreview()
    {
        return $this->practicePreview;
    }

    /**
     * Set practicePreviewComment
     *
     * @param string $practicePreviewComment
     *
     * @return FarvaterApplication
     */
    public function setPracticePreviewComment($practicePreviewComment)
    {
        $this->practicePreviewComment = $practicePreviewComment;

        return $this;
    }

    /**
     * Get practicePreviewComment
     *
     * @return string
     */
    public function getPracticePreviewComment()
    {
        return $this->practicePreviewComment;
    }

    /**
     * Set practiceEvidence
     *
     * @param string $practiceEvidence
     *
     * @return FarvaterApplication
     */
    public function setPracticeEvidence($practiceEvidence)
    {
        $this->practiceEvidence = $practiceEvidence;

        return $this;
    }

    /**
     * Get practiceEvidence
     *
     * @return string
     */
    public function getPracticeEvidence()
    {
        return $this->practiceEvidence;
    }

    /**
     * Set practiceEvidenceComment
     *
     * @param string $practiceEvidenceComment
     *
     * @return FarvaterApplication
     */
    public function setPracticeEvidenceComment($practiceEvidenceComment)
    {
        $this->practiceEvidenceComment = $practiceEvidenceComment;

        return $this;
    }

    /**
     * Get practiceEvidenceComment
     *
     * @return string
     */
    public function getPracticeEvidenceComment()
    {
        return $this->practiceEvidenceComment;
    }

    /**
     * Set practicePurpose
     *
     * @param string $practicePurpose
     *
     * @return FarvaterApplication
     */
    public function setPracticePurpose($practicePurpose)
    {
        $this->practicePurpose = $practicePurpose;

        return $this;
    }

    /**
     * Get practicePurpose
     *
     * @return string
     */
    public function getPracticePurpose()
    {
        return $this->practicePurpose;
    }

    /**
     * Set practicePurposeComment
     *
     * @param string $practicePurposeComment
     *
     * @return FarvaterApplication
     */
    public function setPracticePurposeComment($practicePurposeComment)
    {
        $this->practicePurposeComment = $practicePurposeComment;

        return $this;
    }

    /**
     * Get practicePurposeComment
     *
     * @return string
     */
    public function getPracticePurposeComment()
    {
        return $this->practicePurposeComment;
    }

    /**
     * Set practiceMechanism
     *
     * @param string $practiceMechanism
     *
     * @return FarvaterApplication
     */
    public function setPracticeMechanism($practiceMechanism)
    {
        $this->practiceMechanism = $practiceMechanism;

        return $this;
    }

    /**
     * Get practiceMechanism
     *
     * @return string
     */
    public function getPracticeMechanism()
    {
        return $this->practiceMechanism;
    }

    /**
     * Set practiceMechanismComment
     *
     * @param string $practiceMechanismComment
     *
     * @return FarvaterApplication
     */
    public function setPracticeMechanismComment($practiceMechanismComment)
    {
        $this->practiceMechanismComment = $practiceMechanismComment;

        return $this;
    }

    /**
     * Get practiceMechanismComment
     *
     * @return string
     */
    public function getPracticeMechanismComment()
    {
        return $this->practiceMechanismComment;
    }

    /**
     * Set practiceResults
     *
     * @param string $practiceResults
     *
     * @return FarvaterApplication
     */
    public function setPracticeResults($practiceResults)
    {
        $this->practiceResults = $practiceResults;

        return $this;
    }

    /**
     * Get practiceResults
     *
     * @return string
     */
    public function getPracticeResults()
    {
        return $this->practiceResults;
    }

    /**
     * Set practiceRegionEtc
     *
     * @param string $practiceRegionEtc
     *
     * @return FarvaterApplication
     */
    public function setPracticeRegionEtc($practiceRegionEtc)
    {
        $this->practiceRegionEtc = $practiceRegionEtc;

        return $this;
    }

    /**
     * Get practiceRegionEtc
     *
     * @return string
     */
    public function getPracticeRegionEtc()
    {
        return $this->practiceRegionEtc;
    }

    /**
     * Set targetAudienceEtc
     *
     * @param string $targetAudienceEtc
     *
     * @return FarvaterApplication
     */
    public function setTargetAudienceEtc($targetAudienceEtc)
    {
        $this->targetAudienceEtc = $targetAudienceEtc;

        return $this;
    }

    /**
     * Get targetAudienceEtc
     *
     * @return string
     */
    public function getTargetAudienceEtc()
    {
        return $this->targetAudienceEtc;
    }

    /**
     * Set practiceMotivation
     *
     * @param string $practiceMotivation
     *
     * @return FarvaterApplication
     */
    public function setPracticeMotivation($practiceMotivation)
    {
        $this->practiceMotivation = $practiceMotivation;

        return $this;
    }

    /**
     * Get practiceMotivation
     *
     * @return string
     */
    public function getPracticeMotivation()
    {
        return $this->practiceMotivation;
    }

    /**
     * Set practiceTech
     *
     * @param string $practiceTech
     *
     * @return FarvaterApplication
     */
    public function setPracticeTech($practiceTech)
    {
        $this->practiceTech = $practiceTech;

        return $this;
    }

    /**
     * Get practiceTech
     *
     * @return string
     */
    public function getPracticeTech()
    {
        return $this->practiceTech;
    }

    /**
     * Set practiceTechComment
     *
     * @param string $practiceTechComment
     *
     * @return FarvaterApplication
     */
    public function setPracticeTechComment($practiceTechComment)
    {
        $this->practiceTechComment = $practiceTechComment;

        return $this;
    }

    /**
     * Get practiceTechComment
     *
     * @return string
     */
    public function getPracticeTechComment()
    {
        return $this->practiceTechComment;
    }

    /**
     * Set organizationalCapacity
     *
     * @param string $organizationalCapacity
     *
     * @return FarvaterApplication
     */
    public function setOrganizationalCapacity($organizationalCapacity)
    {
        $this->organizationalCapacity = $organizationalCapacity;

        return $this;
    }

    /**
     * Get organizationalCapacity
     *
     * @return string
     */
    public function getOrganizationalCapacity()
    {
        return $this->organizationalCapacity;
    }

    /**
     * Set firstSectionComment
     *
     * @param string $firstSectionComment
     *
     * @return FarvaterApplication
     */
    public function setFirstSectionComment($firstSectionComment)
    {
        $this->firstSectionComment = $firstSectionComment;

        return $this;
    }

    /**
     * Get firstSectionComment
     *
     * @return string
     */
    public function getFirstSectionComment()
    {
        return $this->firstSectionComment;
    }

    /**
     * Set secondSectionComment
     *
     * @param string $secondSectionComment
     *
     * @return FarvaterApplication
     */
    public function setSecondSectionComment($secondSectionComment)
    {
        $this->secondSectionComment = $secondSectionComment;

        return $this;
    }

    /**
     * Get secondSectionComment
     *
     * @return string
     */
    public function getSecondSectionComment()
    {
        return $this->secondSectionComment;
    }

    /**
     * Set immediateResultsComment
     *
     * @param string $immediateResultsComment
     *
     * @return FarvaterApplication
     */
    public function setImmediateResultsComment($immediateResultsComment)
    {
        $this->immediateResultsComment = $immediateResultsComment;

        return $this;
    }

    /**
     * Get immediateResultsComment
     *
     * @return string
     */
    public function getImmediateResultsComment()
    {
        return $this->immediateResultsComment;
    }

    /**
     * Set socialResultsComment
     *
     * @param string $socialResultsComment
     *
     * @return FarvaterApplication
     */
    public function setSocialResultsComment($socialResultsComment)
    {
        $this->socialResultsComment = $socialResultsComment;

        return $this;
    }

    /**
     * Get socialResultsComment
     *
     * @return string
     */
    public function getSocialResultsComment()
    {
        return $this->socialResultsComment;
    }

    /**
     * Set thirdSectionComment
     *
     * @param string $thirdSectionComment
     *
     * @return FarvaterApplication
     */
    public function setThirdSectionComment($thirdSectionComment)
    {
        $this->thirdSectionComment = $thirdSectionComment;

        return $this;
    }

    /**
     * Get thirdSectionComment
     *
     * @return string
     */
    public function getThirdSectionComment()
    {
        return $this->thirdSectionComment;
    }

    /**
     * Set practiceSpreadPurpose
     *
     * @param string $practiceSpreadPurpose
     *
     * @return FarvaterApplication
     */
    public function setPracticeSpreadPurpose($practiceSpreadPurpose)
    {
        $this->practiceSpreadPurpose = $practiceSpreadPurpose;

        return $this;
    }

    /**
     * Get practiceSpreadPurpose
     *
     * @return string
     */
    public function getPracticeSpreadPurpose()
    {
        return $this->practiceSpreadPurpose;
    }

    /**
     * Set practiceSpreadComment
     *
     * @param string $practiceSpreadComment
     *
     * @return FarvaterApplication
     */
    public function setPracticeSpreadComment($practiceSpreadComment)
    {
        $this->practiceSpreadComment = $practiceSpreadComment;

        return $this;
    }

    /**
     * Get practiceSpreadComment
     *
     * @return string
     */
    public function getPracticeSpreadComment()
    {
        return $this->practiceSpreadComment;
    }

    /**
     * Set organizationResults
     *
     * @param string $organizationResults
     *
     * @return FarvaterApplication
     */
    public function setOrganizationResults($organizationResults)
    {
        $this->organizationResults = $organizationResults;

        return $this;
    }

    /**
     * Get organizationResults
     *
     * @return string
     */
    public function getOrganizationResults()
    {
        return $this->organizationResults;
    }

    /**
     * Set practiceResultsStability
     *
     * @param string $practiceResultsStability
     *
     * @return FarvaterApplication
     */
    public function setPracticeResultsStability($practiceResultsStability)
    {
        $this->practiceResultsStability = $practiceResultsStability;

        return $this;
    }

    /**
     * Get practiceResultsStability
     *
     * @return string
     */
    public function getPracticeResultsStability()
    {
        return $this->practiceResultsStability;
    }

    /**
     * Set fourthSectionComment
     *
     * @param string $fourthSectionComment
     *
     * @return FarvaterApplication
     */
    public function setFourthSectionComment($fourthSectionComment)
    {
        $this->fourthSectionComment = $fourthSectionComment;

        return $this;
    }

    /**
     * Get fourthSectionComment
     *
     * @return string
     */
    public function getFourthSectionComment()
    {
        return $this->fourthSectionComment;
    }

    /**
     * Set monitoringPurpose
     *
     * @param string $monitoringPurpose
     *
     * @return FarvaterApplication
     */
    public function setMonitoringPurpose($monitoringPurpose)
    {
        $this->monitoringPurpose = $monitoringPurpose;

        return $this;
    }

    /**
     * Get monitoringPurpose
     *
     * @return string
     */
    public function getMonitoringPurpose()
    {
        return $this->monitoringPurpose;
    }

    /**
     * Set monitoringReview
     *
     * @param string $monitoringReview
     *
     * @return FarvaterApplication
     */
    public function setMonitoringReview($monitoringReview)
    {
        $this->monitoringReview = $monitoringReview;

        return $this;
    }

    /**
     * Get monitoringReview
     *
     * @return string
     */
    public function getMonitoringReview()
    {
        return $this->monitoringReview;
    }

    /**
     * Set expectedMonitoringResults
     *
     * @param string $expectedMonitoringResults
     *
     * @return FarvaterApplication
     */
    public function setExpectedMonitoringResults($expectedMonitoringResults)
    {
        $this->expectedMonitoringResults = $expectedMonitoringResults;

        return $this;
    }

    /**
     * Get expectedMonitoringResults
     *
     * @return string
     */
    public function getExpectedMonitoringResults()
    {
        return $this->expectedMonitoringResults;
    }

    /**
     * Set baseActivityComment
     *
     * @param string $baseActivityComment
     *
     * @return FarvaterApplication
     */
    public function setBaseActivityComment($baseActivityComment)
    {
        $this->baseActivityComment = $baseActivityComment;

        return $this;
    }

    /**
     * Get baseActivityComment
     *
     * @return string
     */
    public function getBaseActivityComment()
    {
        return $this->baseActivityComment;
    }

    /**
     * Set taskRealization
     *
     * @param string $taskRealization
     *
     * @return FarvaterApplication
     */
    public function setTaskRealization($taskRealization)
    {
        $this->taskRealization = $taskRealization;

        return $this;
    }

    /**
     * Get taskRealization
     *
     * @return string
     */
    public function getTaskRealization()
    {
        return $this->taskRealization;
    }

    /**
     * Set taskSpread
     *
     * @param string $taskSpread
     *
     * @return FarvaterApplication
     */
    public function setTaskSpread($taskSpread)
    {
        $this->taskSpread = $taskSpread;

        return $this;
    }

    /**
     * Get taskSpread
     *
     * @return string
     */
    public function getTaskSpread()
    {
        return $this->taskSpread;
    }

    /**
     * Set taskMonitoring
     *
     * @param string $taskMonitoring
     *
     * @return FarvaterApplication
     */
    public function setTaskMonitoring($taskMonitoring)
    {
        $this->taskMonitoring = $taskMonitoring;

        return $this;
    }

    /**
     * Get taskMonitoring
     *
     * @return string
     */
    public function getTaskMonitoring()
    {
        return $this->taskMonitoring;
    }

    /**
     * Set fullBudget
     *
     * @param string $fullBudget
     *
     * @return FarvaterApplication
     */
    public function setFullBudget($fullBudget)
    {
        $this->fullBudget = $fullBudget;

        return $this;
    }

    /**
     * Get fullBudget
     *
     * @return string
     */
    public function getFullBudget()
    {
        return $this->fullBudget;
    }

    /**
     * Set budgetYear
     *
     * @param string $budgetYear
     *
     * @return FarvaterApplication
     */
    public function setBudgetYear($budgetYear)
    {
        $this->budgetYear = $budgetYear;

        return $this;
    }

    /**
     * Get budgetYear
     *
     * @return string
     */
    public function getBudgetYear()
    {
        return $this->budgetYear;
    }

    /**
     * Set authorityDirectorDocument
     *
     * @param string $authorityDirectorDocument
     *
     * @return FarvaterApplication
     */
    public function setAuthorityDirectorDocument($authorityDirectorDocument)
    {
        if(!$this->_authorityDirectorDocument && is_string($this->authorityDirectorDocument)){
            $this->_authorityDirectorDocument = $this->authorityDirectorDocument;
        }

        $this->authorityDirectorDocument = $authorityDirectorDocument;

        return $this;
    }

    /**
     * Get authorityDirectorDocument
     *
     * @return string
     */
    public function getAuthorityDirectorDocument()
    {
        return $this->authorityDirectorDocument;
    }

    /**
     * Set authorityManagerDocument
     *
     * @param string $authorityManagerDocument
     *
     * @return FarvaterApplication
     */
    public function setAuthorityManagerDocument($authorityManagerDocument)
    {
        if(!$this->_authorityManagerDocument && is_string($this->authorityManagerDocument)){
            $this->_authorityManagerDocument = $this->authorityManagerDocument;
        }

        $this->authorityManagerDocument = $authorityManagerDocument;

        return $this;
    }

    /**
     * Get authorityManagerDocument
     *
     * @return string
     */
    public function getAuthorityManagerDocument()
    {
        return $this->authorityManagerDocument;
    }

    /**
     * Set budgetDocument
     *
     * @param string $budgetDocument
     *
     * @return FarvaterApplication
     */
    public function setBudgetDocument($budgetDocument)
    {
        if(!$this->_budgetDocument && is_string($this->budgetDocument)){
            $this->_budgetDocument = $this->budgetDocument;
        }

        $this->budgetDocument = $budgetDocument;

        return $this;
    }

    /**
     * Get budgetDocument
     *
     * @return string
     */
    public function getBudgetDocument()
    {
        return $this->budgetDocument;
    }

    /**
     * Set urStatusDocument
     *
     * @param string $urStatusDocument
     *
     * @return FarvaterApplication
     */
    public function setUrStatusDocument($urStatusDocument)
    {
        if(!$this->_urStatusDocument && is_string($this->urStatusDocument)){
            $this->_urStatusDocument = $this->urStatusDocument;
        }

        $this->urStatusDocument = $urStatusDocument;

        return $this;
    }

    /**
     * Get urStatusDocument
     *
     * @return string
     */
    public function getUrStatusDocument()
    {
        return $this->urStatusDocument;
    }

    /**
     * Set lastYearReportDocument
     *
     * @param string $lastYearReportDocument
     *
     * @return FarvaterApplication
     */
    public function setLastYearReportDocument($lastYearReportDocument)
    {
        if(!$this->_lastYearReportDocument && is_string($this->lastYearReportDocument)){
            $this->_lastYearReportDocument = $this->lastYearReportDocument;
        }

        $this->lastYearReportDocument = $lastYearReportDocument;

        return $this;
    }

    /**
     * Get lastYearReportDocument
     *
     * @return string
     */
    public function getLastYearReportDocument()
    {
        return $this->lastYearReportDocument;
    }

    /**
     * Set muReportMail
     *
     * @param string $muReportMail
     *
     * @return FarvaterApplication
     */
    public function setMuReportMail($muReportMail)
    {
        if(!$this->_muReportMail && is_string($this->muReportMail)){
            $this->_muReportMail = $this->muReportMail;
        }

        $this->muReportMail = $muReportMail;

        return $this;
    }

    /**
     * Get muReportMail
     *
     * @return string
     */
    public function getMuReportMail()
    {
        return $this->muReportMail;
    }

    /**
     * Set mailBankDocument
     *
     * @param string $mailBankDocument
     *
     * @return FarvaterApplication
     */
    public function setMailBankDocument($mailBankDocument)
    {
        if(!$this->_mailBankDocument && is_string($this->mailBankDocument)){
            $this->_mailBankDocument = $this->mailBankDocument;
        }

        $this->mailBankDocument = $mailBankDocument;

        return $this;
    }

    /**
     * Get mailBankDocument
     *
     * @return string
     */
    public function getMailBankDocument()
    {
        return $this->mailBankDocument;
    }

    /**
     * Set lastYearReportLink
     *
     * @param string $lastYearReportLink
     *
     * @return FarvaterApplication
     */
    public function setLastYearReportLink($lastYearReportLink)
    {
        $this->lastYearReportLink = $lastYearReportLink;

        return $this;
    }

    /**
     * Get lastYearReportLink
     *
     * @return string
     */
    public function getLastYearReportLink()
    {
        return $this->lastYearReportLink;
    }

    /**
     * Set muReportMailLink
     *
     * @param string $muReportMailLink
     *
     * @return FarvaterApplication
     */
    public function setMuReportMailLink($muReportMailLink)
    {
        $this->muReportMailLink = $muReportMailLink;

        return $this;
    }

    /**
     * Get muReportMailLink
     *
     * @return string
     */
    public function getMuReportMailLink()
    {
        return $this->muReportMailLink;
    }

    /**
     * Set childProtectionSpecialists
     *
     * @param string $childProtectionSpecialists
     *
     * @return FarvaterApplication
     */
    public function setChildProtectionSpecialists($childProtectionSpecialists)
    {
        $this->childProtectionSpecialists = $childProtectionSpecialists;

        return $this;
    }

    /**
     * Get childProtectionSpecialists
     *
     * @return string
     */
    public function getChildProtectionSpecialists()
    {
        return $this->childProtectionSpecialists;
    }

    /**
     * Set practiceRegion
     *
     * @param Region $practiceRegion
     *
     * @return FarvaterApplication
     */
    public function setPracticeRegion(Region $practiceRegion = null)
    {
        $this->practiceRegion = $practiceRegion;

        return $this;
    }

    /**
     * Get practiceRegion
     *
     * @return \NKO\OrderBundle\Entity\Region
     */
    public function getPracticeRegion()
    {
        return $this->practiceRegion;
    }

    /**
     * Set practiceExpectingSpreadResults
     *
     * @param string $practiceExpectingSpreadResults
     *
     * @return FarvaterApplication
     */
    public function setPracticeExpectingSpreadResults($practiceExpectingSpreadResults)
    {
        $this->practiceExpectingSpreadResults = $practiceExpectingSpreadResults;

        return $this;
    }

    /**
     * Get practiceExpectingSpreadResults
     *
     * @return string
     */
    public function getPracticeExpectingSpreadResults()
    {
        return $this->practiceExpectingSpreadResults;
    }

    /**
     * Set practiceExpectingResultsStability
     *
     * @param string $practiceExpectingResultsStability
     *
     * @return FarvaterApplication
     */
    public function setPracticeExpectingResultsStability($practiceExpectingResultsStability)
    {
        $this->practiceExpectingResultsStability = $practiceExpectingResultsStability;

        return $this;
    }

    /**
     * Get practiceExpectingResultsStability
     *
     * @return string
     */
    public function getPracticeExpectingResultsStability()
    {
        return $this->practiceExpectingResultsStability;
    }

    /**
     * Set monitoringComment
     *
     * @param string $monitoringComment
     *
     * @return FarvaterApplication
     */
    public function setMonitoringComment($monitoringComment)
    {
        $this->monitoringComment = $monitoringComment;

        return $this;
    }

    /**
     * Get monitoringComment
     *
     * @return string
     */
    public function getMonitoringComment()
    {
        return $this->monitoringComment;
    }

    /**
     * Set disabilitiesChildren
     *
     * @param string $disabilitiesChildren
     *
     * @return FarvaterApplication
     */
    public function setDisabilitiesChildren($disabilitiesChildren)
    {
        $this->disabilitiesChildren = $disabilitiesChildren;

        return $this;
    }

    /**
     * Get disabilitiesChildren
     *
     * @return string
     */
    public function getDisabilitiesChildren()
    {
        return $this->disabilitiesChildren;
    }

    /**
     * Set specialNeedsEducationChildren
     *
     * @param string $specialNeedsEducationChildren
     *
     * @return FarvaterApplication
     */
    public function setSpecialNeedsEducationChildren($specialNeedsEducationChildren)
    {
        $this->specialNeedsEducationChildren = $specialNeedsEducationChildren;

        return $this;
    }

    /**
     * Get specialNeedsEducationChildren
     *
     * @return string
     */
    public function getSpecialNeedsEducationChildren()
    {
        return $this->specialNeedsEducationChildren;
    }

    /**
     * Set organizationalCapacityComment
     *
     * @param string $organizationalCapacityComment
     *
     * @return FarvaterApplication
     */
    public function setOrganizationalCapacityComment($organizationalCapacityComment)
    {
        $this->organizationalCapacityComment = $organizationalCapacityComment;

        return $this;
    }

    /**
     * Get organizationalCapacityComment
     *
     * @return string
     */
    public function getOrganizationalCapacityComment()
    {
        return $this->organizationalCapacityComment;
    }

    /**
     * Set sendingApplicationPersonFullName
     *
     * @param string $sendingApplicationPersonFullName
     *
     * @return FarvaterApplication
     */
    public function setSendingApplicationPersonFullName($sendingApplicationPersonFullName)
    {
        $this->sendingApplicationPersonFullName = $sendingApplicationPersonFullName;

        return $this;
    }

    /**
     * Get sendingApplicationPersonFullName
     *
     * @return string
     */
    public function getSendingApplicationPersonFullName()
    {
        return $this->sendingApplicationPersonFullName;
    }

    /**
     * Set isCorrectInfo
     *
     * @param boolean $isCorrectInfo
     *
     * @return FarvaterApplication
     */
    public function setIsCorrectInfo($isCorrectInfo)
    {
        $this->isCorrectInfo = $isCorrectInfo;

        return $this;
    }

    /**
     * Get isCorrectInfo
     *
     * @return boolean
     */
    public function getIsCorrectInfo()
    {
        return $this->isCorrectInfo;
    }

    /**
     * @Assert\Callback
     */
    public function isIsCorrectInfo(ExecutionContextInterface $context, $payload)
    {
        if(!$this->isCorrectInfo)
            $context->buildViolation('please, confirm if correct data')
                ->atPath('isCorrectInfo')
                ->addViolation();
    }

    /**
     * Add grant
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Grant $grant
     *
     * @return FarvaterApplication
     */
    public function addGrant(\NKO\OrderBundle\Entity\Farvater\Grant $grant)
    {
        $this->grants[] = $grant;

        return $this;
    }

    /**
     * Remove grant
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Grant $grant
     */
    public function removeGrant(\NKO\OrderBundle\Entity\Farvater\Grant $grant)
    {
        $this->grants->removeElement($grant);
    }

    /**
     * Get grants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrants()
    {
        return $this->grants;
    }

    /**
     * Add publicationMaterial
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial
     *
     * @return FarvaterApplication
     */
    public function addPublicationMaterial(\NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial)
    {
        $this->publicationMaterials[] = $publicationMaterial;

        return $this;
    }

    /**
     * Remove publicationMaterial
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial
     */
    public function removePublicationMaterial(\NKO\OrderBundle\Entity\Farvater\PublicationMaterial $publicationMaterial)
    {
        $this->publicationMaterials->removeElement($publicationMaterial);
    }

    /**
     * Get publicationMaterials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublicationMaterials()
    {
        return $this->publicationMaterials;
    }

    /**
     * Add priorityDirection
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection
     *
     * @return FarvaterApplication
     */
    public function addPriorityDirection(\NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection)
    {
        $this->priorityDirections[] = $priorityDirection;

        return $this;
    }

    /**
     * Remove priorityDirection
     *
     * @param \NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection
     */
    public function removePriorityDirection(\NKO\OrderBundle\Entity\Farvater\PriorityDirection $priorityDirection)
    {
        $this->priorityDirections->removeElement($priorityDirection);
    }

    /**
     * Get priorityDirections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriorityDirections()
    {
        return $this->priorityDirections;
    }

    /**
     * Add targetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup
     *
     * @return FarvaterApplication
     */
    public function addTargetGroup(\NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup)
    {
        $this->targetGroups[] = $targetGroup;

        return $this;
    }

    /**
     * Remove targetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup
     */
    public function removeTargetGroup(\NKO\OrderBundle\Entity\Farvater\TargetGroup $targetGroup)
    {
        $this->targetGroups->removeElement($targetGroup);
    }

    /**
     * Get targetGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetGroups()
    {
        return $this->targetGroups;
    }

    /**
     * Add socialResult
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult
     *
     * @return FarvaterApplication
     */
    public function addSocialResult(\NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult)
    {
        $this->socialResults[] = $socialResult;

        return $this;
    }

    /**
     * Remove socialResult
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult
     */
    public function removeSocialResult(\NKO\OrderBundle\Entity\Farvater\SocialResult $socialResult)
    {
        $this->socialResults->removeElement($socialResult);
    }

    /**
     * Get socialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResults()
    {
        return $this->socialResults;
    }

    /**
     * Add targetAudience
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience
     *
     * @return FarvaterApplication
     */
    public function addTargetAudience(\NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience)
    {
        $this->targetAudiences[] = $targetAudience;

        return $this;
    }

    /**
     * Remove targetAudience
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience
     */
    public function removeTargetAudience(\NKO\OrderBundle\Entity\Farvater\TargetAudience $targetAudience)
    {
        $this->targetAudiences->removeElement($targetAudience);
    }

    /**
     * Get targetAudiences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetAudiences()
    {
        return $this->targetAudiences;
    }

    /**
     * Add projectService
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ProjectService $projectService
     *
     * @return FarvaterApplication
     */
    public function addProjectService(\NKO\OrderBundle\Entity\Farvater\ProjectService $projectService)
    {
        $this->projectServices[] = $projectService;

        return $this;
    }

    /**
     * Remove projectService
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ProjectService $projectService
     */
    public function removeProjectService(\NKO\OrderBundle\Entity\Farvater\ProjectService $projectService)
    {
        $this->projectServices->removeElement($projectService);
    }

    /**
     * Get projectServices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectServices()
    {
        return $this->projectServices;
    }

    /**
     * Add participatingTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ParticipatingTargetGroup $participatingTargetGroup
     *
     * @return FarvaterApplication
     */
    public function addParticipatingTargetGroup(\NKO\OrderBundle\Entity\Farvater\ParticipatingTargetGroup $participatingTargetGroup)
    {
        $this->participatingTargetGroups[] = $participatingTargetGroup;

        return $this;
    }

    /**
     * Remove participatingTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ParticipatingTargetGroup $participatingTargetGroup
     */
    public function removeParticipatingTargetGroup(\NKO\OrderBundle\Entity\Farvater\ParticipatingTargetGroup $participatingTargetGroup)
    {
        $this->participatingTargetGroups->removeElement($participatingTargetGroup);
    }

    /**
     * Get participatingTargetGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipatingTargetGroups()
    {
        return $this->participatingTargetGroups;
    }

    /**
     * Add awareTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\AwareTargetGroup $awareTargetGroup
     *
     * @return FarvaterApplication
     */
    public function addAwareTargetGroup(\NKO\OrderBundle\Entity\Farvater\AwareTargetGroup $awareTargetGroup)
    {
        $this->awareTargetGroups[] = $awareTargetGroup;

        return $this;
    }

    /**
     * Remove awareTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\Farvater\AwareTargetGroup $awareTargetGroup
     */
    public function removeAwareTargetGroup(\NKO\OrderBundle\Entity\Farvater\AwareTargetGroup $awareTargetGroup)
    {
        $this->awareTargetGroups->removeElement($awareTargetGroup);
    }

    /**
     * Get awareTargetGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAwareTargetGroups()
    {
        return $this->awareTargetGroups;
    }

    /**
     * Add spreadActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SpreadActivity $spreadActivity
     *
     * @return FarvaterApplication
     */
    public function addSpreadActivity(\NKO\OrderBundle\Entity\Farvater\SpreadActivity $spreadActivity)
    {
        $this->spreadActivities[] = $spreadActivity;

        return $this;
    }

    /**
     * Remove spreadActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SpreadActivity $spreadActivity
     */
    public function removeSpreadActivity(\NKO\OrderBundle\Entity\Farvater\SpreadActivity $spreadActivity)
    {
        $this->spreadActivities->removeElement($spreadActivity);
    }

    /**
     * Get spreadActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpreadActivities()
    {
        return $this->spreadActivities;
    }

    /**
     * Add participation
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Participation $participation
     *
     * @return FarvaterApplication
     */
    public function addParticipation(\NKO\OrderBundle\Entity\Farvater\Participation $participation)
    {
        $this->participations[] = $participation;

        return $this;
    }

    /**
     * Remove participation
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Participation $participation
     */
    public function removeParticipation(\NKO\OrderBundle\Entity\Farvater\Participation $participation)
    {
        $this->participations->removeElement($participation);
    }

    /**
     * Get participations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipations()
    {
        return $this->participations;
    }

    /**
     * Add introduction
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Introduction $introduction
     *
     * @return FarvaterApplication
     */
    public function addIntroduction(\NKO\OrderBundle\Entity\Farvater\Introduction $introduction)
    {
        $this->introductions[] = $introduction;

        return $this;
    }

    /**
     * Remove introduction
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Introduction $introduction
     */
    public function removeIntroduction(\NKO\OrderBundle\Entity\Farvater\Introduction $introduction)
    {
        $this->introductions->removeElement($introduction);
    }

    /**
     * Get introductions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntroductions()
    {
        return $this->introductions;
    }

    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Publication $publication
     *
     * @return FarvaterApplication
     */
    public function addPublication(\NKO\OrderBundle\Entity\Farvater\Publication $publication)
    {
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Farvater\Publication $publication
     */
    public function removePublication(\NKO\OrderBundle\Entity\Farvater\Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Add realizationPlan
     *
     * @param \NKO\OrderBundle\Entity\Farvater\RealizationPlan $realizationPlan
     *
     * @return FarvaterApplication
     */
    public function addRealizationPlan(\NKO\OrderBundle\Entity\Farvater\RealizationPlan $realizationPlan)
    {
        $this->realizationPlans[] = $realizationPlan;

        return $this;
    }

    /**
     * Remove realizationPlan
     *
     * @param \NKO\OrderBundle\Entity\Farvater\RealizationPlan $realizationPlan
     */
    public function removeRealizationPlan(\NKO\OrderBundle\Entity\Farvater\RealizationPlan $realizationPlan)
    {
        $this->realizationPlans->removeElement($realizationPlan);
    }

    /**
     * Get realizationPlans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealizationPlans()
    {
        return $this->realizationPlans;
    }

    /**
     * Add spreadPlan
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SpreadPlan $spreadPlan
     *
     * @return FarvaterApplication
     */
    public function addSpreadPlan(\NKO\OrderBundle\Entity\Farvater\SpreadPlan $spreadPlan)
    {
        $this->spreadPlans[] = $spreadPlan;

        return $this;
    }

    /**
     * Remove spreadPlan
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SpreadPlan $spreadPlan
     */
    public function removeSpreadPlan(\NKO\OrderBundle\Entity\Farvater\SpreadPlan $spreadPlan)
    {
        $this->spreadPlans->removeElement($spreadPlan);
    }

    /**
     * Get spreadPlans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpreadPlans()
    {
        return $this->spreadPlans;
    }

    /**
     * Add monitoringPlan
     *
     * @param \NKO\OrderBundle\Entity\Farvater\MonitoringPlan $monitoringPlan
     *
     * @return FarvaterApplication
     */
    public function addMonitoringPlan(\NKO\OrderBundle\Entity\Farvater\MonitoringPlan $monitoringPlan)
    {
        $this->monitoringPlans[] = $monitoringPlan;

        return $this;
    }

    /**
     * Remove monitoringPlan
     *
     * @param \NKO\OrderBundle\Entity\Farvater\MonitoringPlan $monitoringPlan
     */
    public function removeMonitoringPlan(\NKO\OrderBundle\Entity\Farvater\MonitoringPlan $monitoringPlan)
    {
        $this->monitoringPlans->removeElement($monitoringPlan);
    }

    /**
     * Get monitoringPlans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringPlans()
    {
        return $this->monitoringPlans;
    }

    /**
     * Add projectMember
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ProjectMember $projectMember
     *
     * @return FarvaterApplication
     */
    public function addProjectMember(\NKO\OrderBundle\Entity\Farvater\ProjectMember $projectMember)
    {
        $this->projectMembers[] = $projectMember;

        return $this;
    }

    /**
     * Remove projectMember
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ProjectMember $projectMember
     */
    public function removeProjectMember(\NKO\OrderBundle\Entity\Farvater\ProjectMember $projectMember)
    {
        $this->projectMembers->removeElement($projectMember);
    }

    /**
     * Get projectMembers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectMembers()
    {
        return $this->projectMembers;
    }

    /**
     * Add organizationResource
     *
     * @param \NKO\OrderBundle\Entity\Farvater\OrganizationResource $organizationResource
     *
     * @return FarvaterApplication
     */
    public function addOrganizationResource(\NKO\OrderBundle\Entity\Farvater\OrganizationResource $organizationResource)
    {
        $this->organizationResources[] = $organizationResource;

        return $this;
    }

    /**
     * Remove organizationResource
     *
     * @param \NKO\OrderBundle\Entity\Farvater\OrganizationResource $organizationResource
     */
    public function removeOrganizationResource(\NKO\OrderBundle\Entity\Farvater\OrganizationResource $organizationResource)
    {
        $this->organizationResources->removeElement($organizationResource);
    }

    /**
     * Get organizationResources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizationResources()
    {
        return $this->organizationResources;
    }

    /**
     * Add projectPartner
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ProjectPartner $projectPartner
     *
     * @return FarvaterApplication
     */
    public function addProjectPartner(\NKO\OrderBundle\Entity\Farvater\ProjectPartner $projectPartner)
    {
        $this->projectPartners[] = $projectPartner;

        return $this;
    }

    /**
     * Remove projectPartner
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ProjectPartner $projectPartner
     */
    public function removeProjectPartner(\NKO\OrderBundle\Entity\Farvater\ProjectPartner $projectPartner)
    {
        $this->projectPartners->removeElement($projectPartner);
    }

    /**
     * Get projectPartners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectPartners()
    {
        return $this->projectPartners;
    }

    /**
     * Set oldId
     *
     * @param integer $oldId
     *
     * @return FarvaterApplication
     */
    public function setOldId($oldId)
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * Get oldId
     *
     * @return integer
     */
    public function getOldId()
    {
        return $this->oldId;
    }
}
