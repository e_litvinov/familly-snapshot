<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealizationPlan
 *
 * @ORM\Table(name="farvater_realization_plan")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\RealizationPlanRepository")
 */
class RealizationPlan extends Plan
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="realizationPlans", cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return RealizationPlan
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }
}
