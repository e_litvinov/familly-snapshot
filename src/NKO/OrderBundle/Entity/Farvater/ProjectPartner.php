<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectPartner
 *
 * @ORM\Table(name="farvater_project_partner")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\ProjectPartnerRepository")
 */
class ProjectPartner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="organization_name", type="text", nullable=true)
     */
    private $organizationName;

    /**
     * @var string
     *
     * @ORM\Column(name="organization_information", type="text", nullable=true)
     */
    private $organizationInformation;

    /**
     * @var string
     *
     * @ORM\Column(name="project_participation", type="text", nullable=true)
     */
    private $projectParticipation;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="projectPartners", cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizationName
     *
     * @param string $organizationName
     *
     * @return ProjectPartner
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    /**
     * Get organizationName
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Set organizationInformation
     *
     * @param string $organizationInformation
     *
     * @return ProjectPartner
     */
    public function setOrganizationInformation($organizationInformation)
    {
        $this->organizationInformation = $organizationInformation;

        return $this;
    }

    /**
     * Get organizationInformation
     *
     * @return string
     */
    public function getOrganizationInformation()
    {
        return $this->organizationInformation;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return ProjectPartner
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set projectParticipation
     *
     * @param string $projectParticipation
     *
     * @return ProjectPartner
     */
    public function setProjectParticipation($projectParticipation)
    {
        $this->projectParticipation = $projectParticipation;

        return $this;
    }

    /**
     * Get projectParticipation
     *
     * @return string
     */
    public function getProjectParticipation()
    {
        return $this->projectParticipation;
    }
}
