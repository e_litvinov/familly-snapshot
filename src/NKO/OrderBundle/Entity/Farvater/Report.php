<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/7/17
 * Time: 12:37 PM
 */

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="farvater_2016_report")
 * @ORM\Entity("")
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    //Organization Information

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $accountingPeriod;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $amountOfDonation;

    //Project practice


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountingPeriod
     *
     * @param string $accountingPeriod
     *
     * @return Report
     */
    public function setAccountingPeriod($accountingPeriod)
    {
        $this->accountingPeriod = $accountingPeriod;

        return $this;
    }

    /**
     * Get accountingPeriod
     *
     * @return string
     */
    public function getAccountingPeriod()
    {
        return $this->accountingPeriod;
    }

    /**
     * Set amountOfDonation
     *
     * @param float $amountOfDonation
     *
     * @return Report
     */
    public function setAmountOfDonation($amountOfDonation)
    {
        $this->amountOfDonation = $amountOfDonation;

        return $this;
    }

    /**
     * Get amountOfDonation
     *
     * @return float
     */
    public function getAmountOfDonation()
    {
        return $this->amountOfDonation;
    }
}
