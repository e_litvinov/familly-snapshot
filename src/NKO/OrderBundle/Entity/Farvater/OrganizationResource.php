<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrganizationResource
 *
 * @ORM\Table(name="farvater_organization_resource")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\OrganizationResourceRepository")
 */
class OrganizationResource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ResourceType", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="resource_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $resourceType;

    /**
     * @var string
     *
     * @ORM\Column(name="resource_description", type="text", nullable=true)
     */
    private $resourceDescription;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="organizationResources", cascade={"persist"}
     * )
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resourceDescription
     *
     * @param string $resourceDescription
     *
     * @return OrganizationResource
     */
    public function setResourceDescription($resourceDescription)
    {
        $this->resourceDescription = $resourceDescription;

        return $this;
    }

    /**
     * Get resourceDescription
     *
     * @return string
     */
    public function getResourceDescription()
    {
        return $this->resourceDescription;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return OrganizationResource
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set resourceType
     *
     * @param \NKO\OrderBundle\Entity\Farvater\ResourceType $resourceType
     *
     * @return OrganizationResource
     */
    public function setResourceType(\NKO\OrderBundle\Entity\Farvater\ResourceType $resourceType = null)
    {
        $this->resourceType = $resourceType;

        return $this;
    }

    /**
     * Get resourceType
     *
     * @return \NKO\OrderBundle\Entity\Farvater\ResourceType
     */
    public function getResourceType()
    {
        return $this->resourceType;
    }
}
