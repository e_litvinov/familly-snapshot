<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicationMaterial
 *
 * @ORM\Table(name="farvater_publication_material")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\PublicationMaterialRepository")
 */
class PublicationMaterial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="publicationMaterials", cascade={"persist"}
     *     )
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $application;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="NKO\OrderBundle\Entity\BriefApplication2016\Application",
     *     inversedBy="publicationMaterials", cascade={"persist"}
     *     )
     * @ORM\JoinColumn(
     *     name="brief_application_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     *     )
     *
     */
    private $bitrixBriefApplication;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PublicationMaterial
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return PublicationMaterial
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set bitrixBriefApplication
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2016\Application $bitrixBriefApplication
     *
     * @return PublicationMaterial
     */
    public function setBitrixBriefApplication(\NKO\OrderBundle\Entity\BriefApplication2016\Application $bitrixBriefApplication = null)
    {
        $this->bitrixBriefApplication = $bitrixBriefApplication;

        return $this;
    }

    /**
     * Get bitrixBriefApplication
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2016\Application
     */
    public function getBitrixBriefApplication()
    {
        return $this->bitrixBriefApplication;
    }
}
