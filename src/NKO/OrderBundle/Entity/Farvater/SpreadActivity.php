<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SpreadActivity
 *
 * @ORM\Table(name="farvater_spread_activity")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\SpreadActivityRepository")
 */
class SpreadActivity extends BaseResultMeasure
{
    /**
     * @ORM\ManyToOne(targetEntity="SpreadActivityIndex", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="index_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $index;

    /**
     * @ORM\Column(name="other_index", type="string", length=500, nullable=true)
     */
    private $otherIndex;


    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="spreadActivities", cascade={"persist"}
     * )
     *
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return SpreadActivity
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set index
     *
     * @param \NKO\OrderBundle\Entity\Farvater\SpreadActivityIndex $index
     *
     * @return SpreadActivity
     */
    public function setIndex(\NKO\OrderBundle\Entity\Farvater\SpreadActivityIndex $index = null)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return \NKO\OrderBundle\Entity\Farvater\SpreadActivityIndex
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set otherIndex
     *
     * @param string $otherIndex
     *
     * @return SpreadActivity
     */
    public function setOtherIndex($otherIndex)
    {
        $this->otherIndex = $otherIndex;

        return $this;
    }

    /**
     * Get otherIndex
     *
     * @return string
     */
    public function getOtherIndex()
    {
        return $this->otherIndex;
    }
}
