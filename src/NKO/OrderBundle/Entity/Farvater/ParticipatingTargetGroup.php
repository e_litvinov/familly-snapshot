<?php

namespace NKO\OrderBundle\Entity\Farvater;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ParticipatingTargetGroup
 *
 * @ORM\Table(name="farvater_participating_target_group")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Farvater\ParticipatingTargetGroupRepository")
 */
class ParticipatingTargetGroup extends BaseResultMeasure
{
    /**
     * @ORM\ManyToOne(targetEntity="TargetGroupIndex", fetch="EAGER")
     *
     * @ORM\JoinColumn(name="index_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $index;

    /**
     * @ORM\Column(name="other_index", type="string", length=500, nullable=true)
     */
    private $otherIndex;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="FarvaterApplication", inversedBy="participatingTargetGroups", cascade={"persist"}
     * )
     *
     * @ORM\JoinColumn(
     *     name="application_id", referencedColumnName="id", onDelete="CASCADE"
     * )
     *
     */
    private $application;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application
     *
     * @return ParticipatingTargetGroup
     */
    public function setApplication(\NKO\OrderBundle\Entity\Farvater\FarvaterApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Farvater\FarvaterApplication
     */
    public function getApplication()
    {
        return $this->application;
    }
    
    /**
     * Set otherIndex
     *
     * @param string $otherIndex
     *
     * @return ParticipatingTargetGroup
     */
    public function setOtherIndex($otherIndex)
    {
        $this->otherIndex = $otherIndex;

        return $this;
    }

    /**
     * Get otherIndex
     *
     * @return string
     */
    public function getOtherIndex()
    {
        return $this->otherIndex;
    }

    /**
     * Set index
     *
     * @param \NKO\OrderBundle\Entity\Farvater\TargetGroupIndex $index
     *
     * @return ParticipatingTargetGroup
     */
    public function setIndex(\NKO\OrderBundle\Entity\Farvater\TargetGroupIndex $index = null)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * Get index
     *
     * @return \NKO\OrderBundle\Entity\Farvater\TargetGroupIndex
     */
    public function getIndex()
    {
        return $this->index;
    }
}
