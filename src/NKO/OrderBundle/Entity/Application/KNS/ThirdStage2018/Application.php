<?php

namespace NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Effectiveness;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Traits\AdditionalOrganizationInfoTrait;
use NKO\OrderBundle\Traits\Application\Continuation\KNS\Kns2018ApplicationTrait;
use NKO\OrderBundle\Traits\Application\KNS\Application2018\DocumentsTrait;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\ResourceTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="kns_2018_application_third_stage")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "regulation",
 *       "authorityHead",
 *       "signedAgreement"
 *     },
 *     notNull=true,
 *     groups={"KNS2018-3"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid excel file",
 *     extensions={"excel"},
 *     fileNames={
 *       "budget",
 *     },
 *     notNull=true,
 *     groups={"KNS2018-3"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "organizationCreationResolution",
 *       "subjectRegulation",
 *       "anotherHead"
 *     },
 *     groups={"KNS2018-3"}
 * )
 */
class Application extends BaseApplication
{
    use BudgetAdditionalTrait;
    use Kns2018ApplicationTrait;

    use OrganizationInfoTrait;
    use AdditionalOrganizationInfoTrait;
    use KnsApplicationTrait;

    use ResourceTrait;
    use PracticeDescriptionTrait;
    use BudgetTrait;
    use DocumentsTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="kns2018_third_stage_2_people_category",
     * joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     * inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $peopleCategories;

    /**
     * @Assert\Count(min="1", groups={"KNS2018-3"})
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="kns2018_third_stage2_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     * @Assert\NotBlank(groups={"KNS2018-3"})
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns2018_third_stage2_application_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\KNS\Effectiveness", mappedBy="effectivenessApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessKNSItems;

    /**
     * @var float
     *  @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS2018-3"}
     *     )
     * @ORM\Column(name="sum_grant", type="float", nullable=true)
     */
    protected $sumGrant;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="string", nullable=true)
     */
    private $contract;


    public function __construct()
    {
        parent::__construct();
        $this->isAddressesEqual = true;
        $this->practiceRegions = new ArrayCollection();
        $this->effectivenessKNSItems = new ArrayCollection();
        $this->preOrganizationResources();
    }

    public function addEffectivenessKNSItem(Effectiveness $effectivenessItem)
    {

        $this->effectivenessKNSItems[] = $effectivenessItem;
        $effectivenessItem->setEffectivenessApplication($this);
        return $this;
    }

    public function removeEffectivenessKNSItem(Effectiveness $effectivenessItem)
    {
        $this->effectivenessKNSItems->removeElement($effectivenessItem);
    }

    public function getEffectivenessKNSItems()
    {
        return $this->effectivenessKNSItems;
    }

    public function setSumGrant($sumGrant)
    {
        $this->sumGrant = $sumGrant;

        return $this;
    }

    public function getSumGrant()
    {
        return $this->sumGrant;
    }

    public function getContract()
    {
        return $this->contract;
    }

    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }
}
