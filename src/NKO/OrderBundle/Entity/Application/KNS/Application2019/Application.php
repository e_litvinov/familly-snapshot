<?php

namespace NKO\OrderBundle\Entity\Application\KNS\Application2019;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use NKO\OrderBundle\Traits\Application\KNS\Application2018\Kns2018ApplicationTrait;
use NKO\OrderBundle\Traits\Application\KNS\Application2018\DocumentsTrait;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="kns_2019_application")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "regulation", "signedAgreement"
 *     },
 *     notNull=true,
 *     groups={"KNS-2019", "KNS-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "organizationCreationResolution"
 *     },
 *     groups={"KNS-2019", "KNS-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid excel file",
 *     extensions={"excel"},
 *     fileNames={
 *       "budget",
 *     },
 *     notNull=true,
 *     groups={"KNS-2019", "KNS-2020"}
 * )
 *
 */
class Application extends BaseApplication
{
    use OrganizationInfoTrait;
    use KnsApplicationTrait;
    use Kns2018ApplicationTrait;
    use DocumentsTrait;
    use BudgetAdditionalTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(groups={"KNS-2019", "KNS-2020"})
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\TraineeshipFormat", cascade={"persist"})
     * @ORM\JoinTable(name="kns2019_application_traineeship_format",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="traineeship_format_id", referencedColumnName="id")})
     */
    protected $traineeshipFormats;

    /**
     * @Assert\NotBlank(groups={"KNS-2019"})
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns2019_application_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")})
     */
    protected $trainingGrounds;

    /**
     * @Assert\NotBlank(groups={"KNS-2019"})
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\SocialResult",  cascade={"persist"})
     * @ORM\JoinTable(name="kns2019_application_social_result_relationship",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="social_result_id", referencedColumnName="id")})
     */
    protected $socialResults;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="kns2019_people_category_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $peopleCategories;

    public function __construct()
    {
        parent::__construct();
        $this->isAddressesEqual = true;
        $this->socialResults = new ArrayCollection();
        $this->preCreateProjectResults();
    }
}
