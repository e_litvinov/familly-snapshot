<?php

namespace NKO\OrderBundle\Entity\Application\KNS\SecondStage2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Traits\HumanizedProperties;

/**
 * Result
 * @ORM\Entity()
 * @ORM\Table(name="kns_2017_result_second_stage")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Result
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\Column(name="socialResult", type="text", nullable=true)
     *
     */
    private $socialResult;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\Column(name="targetGroup", type="text", nullable=true)
     */
    private $targetGroup;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\Column(name="indicator", type="text", nullable=true)
     */
    private $indicator;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\Column(name="firstYearTargetValue", type="integer", nullable=true)
     */
    private $firstYearTargetValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\Column(name="targetValue", type="integer", nullable=true)
     */
    private $targetValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\Column(name="customMeasurementMethod", type="text", nullable=true)
     */
    private $customMeasurementMethod;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $application;

    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinTable(name="result_measurement_method_relationship",
     *      joinColumns={@ORM\JoinColumn(name="result_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")}
     *      )
     */
    private $measurementMethods;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     *
     * @Assert\Callback(groups={"KNS2017-2", "KNS2018-3", "KNS2019-2"})
     */
    public function isValidMeasurementMethods(ExecutionContextInterface $context, $payload)
    {
        if ($this->measurementMethods->isEmpty()) {
            $context->buildViolation('please, choose anything')
                ->atPath('measurementMethods')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set socialResult
     *
     * @param string $socialResult
     *
     * @return Result
     */
    public function setSocialResult($socialResult)
    {
        $this->socialResult = $socialResult;

        return $this;
    }

    /**
     * Get socialResult
     *
     * @return string
     */
    public function getSocialResult()
    {
        return $this->socialResult;
    }

    /**
     * Set targetGroup
     *
     * @param string $targetGroup
     *
     * @return Result
     */
    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return string
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return Result
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set firstYearTargetValue
     *
     * @param integer $firstYearTargetValue
     *
     * @return Result
     */
    public function setFirstYearTargetValue($firstYearTargetValue)
    {
        $this->firstYearTargetValue = $firstYearTargetValue;

        return $this;
    }

    /**
     * Get firstYearTargetValue
     *
     * @return int
     */
    public function getFirstYearTargetValue()
    {
        return $this->firstYearTargetValue;
    }

    /**
     * Set targetValue
     *
     * @param integer $targetValue
     *
     * @return Result
     */
    public function setTargetValue($targetValue)
    {
        $this->targetValue = $targetValue;

        return $this;
    }

    /**
     * Get targetValue
     *
     * @return int
     */
    public function getTargetValue()
    {
        return $this->targetValue;
    }

    /**
     * Set customMeasurementMethod
     *
     * @param string $customMeasurementMethod
     *
     * @return Result
     */
    public function setCustomMeasurementMethod($customMeasurementMethod)
    {
        $this->customMeasurementMethod = $customMeasurementMethod;

        return $this;
    }

    /**
     * Get customMeasurementMethod
     *
     * @return string
     */
    public function getCustomMeasurementMethod()
    {
        return $this->customMeasurementMethod;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication  $application
     *
     * @return Result
     */
    public function setApplication(\NKO\OrderBundle\Entity\BaseApplication  $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\KNS2017\Application
     */
    public function getApplication()
    {
        return $this->application;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->measurementMethods = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     *
     * @return Result
     */
    public function addMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod)
    {
        $this->measurementMethods[] = $measurementMethod;

        return $this;
    }

    /**
     * Remove measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     */
    public function removeMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod)
    {
        $this->measurementMethods->removeElement($measurementMethod);
    }

    /**
     * Get measurementMethods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMeasurementMethods()
    {
        return $this->measurementMethods;
    }

    public function getHumanizedMeasurementMethod()
    {
        return $this->getHumanizedCollections($this->getMeasurementMethods());
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
}
