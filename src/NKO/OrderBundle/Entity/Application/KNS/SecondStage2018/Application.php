<?php

namespace NKO\OrderBundle\Entity\Application\KNS\SecondStage2018;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\ResourceTrait;
use NKO\OrderBundle\Traits\Application\Continuation\KNS\Kns2018ApplicationTrait;
use NKO\OrderBundle\Traits\Application\KNS\Application2018\DocumentsTrait;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="kns_2018_application_second_stage")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "regulation",
 *       "authorityHead",
 *     },
 *     notNull=true,
 *     groups={"KNS2018-2"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid excel file",
 *     extensions={"excel"},
 *     fileNames={
 *       "budget",
 *     },
 *     notNull=true,
 *     groups={"KNS2018-2"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "organizationCreationResolution",
 *     },
 *     groups={"KNS2018-2"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="",
 *     extensions={},
 *     fileNames={
 *       "signedAgreement",
 *     },
 *     notNull=true,
 *     groups={"KNS2018-2"}
 * )
 */
class Application extends BaseApplication
{
    use OrganizationInfoTrait;
    use KnsApplicationTrait;
    use ResourceTrait;
    use PracticeDescriptionTrait;
    use Kns2018ApplicationTrait;
    use BudgetTrait;
    use DocumentsTrait;
    use BudgetAdditionalTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="kns2018_second_stage_2_people_category",
     * joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     * inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $peopleCategories;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="kns2018_second_stage2_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns2018_second_stage2_application_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    public function __construct()
    {
        parent::__construct();
        $this->isAddressesEqual = true;
        $this->practiceRegions = new ArrayCollection();
        $this->preOrganizationResources();
    }
}
