<?php

namespace NKO\OrderBundle\Entity\Application\KNS\SecondStage2019;

use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\BaseApplication;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\AdditionalOrganizationInfoTrait;
use NKO\OrderBundle\Traits\Application\Continuation\KNS\Kns2018ApplicationTrait;
use NKO\OrderBundle\Traits\Application\KNS\Application2018\DocumentsTrait as FirstDocuments;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\ResourceTrait;
use NKO\OrderBundle\Traits\Application\Harbor\Application2019\DocumentsTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="kns_2019_application_second_stage")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "authorityHead",
 *       "organizationCreationResolution",
 *       "reportForMinistryOfJustice",
 *     },
 *     notNull=false,
 *     groups={"KNS2019-2"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid excel file",
 *     extensions={"excel"},
 *     fileNames={
 *       "budget",
 *     },
 *     notNull=true,
 *     groups={"KNS2019-2"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "anotherHead",
 *       "regulation",
 *       "signedAgreement",
 *     },
 *     notNull=true,
 *     groups={"KNS2019-2"}
 * )
 */
class Application extends BaseApplication
{

    use OrganizationInfoTrait;
    use KnsApplicationTrait;
    use ResourceTrait;
    use PracticeDescriptionTrait;
    use Kns2018ApplicationTrait;
    use BudgetTrait;
    use FirstDocuments;
    use DocumentsTrait;
    use BudgetAdditionalTrait;
    use AdditionalOrganizationInfoTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="kns2019_second_stage_people_category",
     * joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     * inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $peopleCategories;

    /**
     * @Assert\Count(min="1", minMessage="Пожалуйста, выберите хотя бы один из приведенных вариантов", groups={"KNS2019-2"})
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="kns2019_second_stage_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns2019_second_stage_application_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"KNS2019-2"}
     * )
     *
     * @ORM\Column(name="link_to_report_for_ministry_of_justice", type="text", nullable=true)
     */
    private $linkToReportForMinistryOfJustice;

    public function __construct()
    {
        parent::__construct();
        $this->isAddressesEqual = true;
        $this->practiceRegions = new ArrayCollection();
        $this->preOrganizationResources();
    }

    public function setLinkToReportForMinistryOfJustice($linkToReportForMinistryOfJustice)
    {
        $this->linkToReportForMinistryOfJustice = $linkToReportForMinistryOfJustice;

        return $this;
    }

    public function getLinkToReportForMinistryOfJustice()
    {
        return $this->linkToReportForMinistryOfJustice;
    }
}
