<?php

namespace NKO\OrderBundle\Entity\Application\Harbor\Application2019;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\Application\AllDocumentsTrait;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\Application\Harbor\Application2019\DocumentsTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="harbor_application_2019")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid DOC file",
 *     extensions={"doc"},
 *     fileNames={
 *       "subjectRegulation"
 *     },
 *     notNull=true,
 *     groups={"Harbor-2019"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "regulation",
 *       "authorityHead",
 *       "isBankAccountExist",
 *       "signedAgreement",
 *     },
 *     notNull=true,
 *     groups={"Harbor-2019"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid excel file",
 *     extensions={"excel"},
 *     fileNames={
 *       "budget",
 *     },
 *     notNull=true,
 *     groups={"Harbor-2019"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "anotherHead",
 *       "organizationCreationResolution",
 *       "reportForMinistryOfJustice",
 *     },
 *     groups={"Harbor-2019"}
 * )
 */
class Application extends BaseApplication
{
    use BudgetTrait;
    use BudgetAdditionalTrait;
    use OrganizationInfoTrait;
    use AllDocumentsTrait;
    use DocumentsTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"Harbor-2019"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm")
     * @ORM\JoinColumn(name="organizationForm_id", referencedColumnName="id", nullable=true)
     */
    private $organizationForm;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Harbor-2019"}
     *     )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $personalAccount;

    /**
     * @var bool
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    protected $isOrganizationHeadEqualProjectHead;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameAddressee;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"Harbor-2019"}
     * )
     *
     * @ORM\Column(name="link_to_report_for_ministry_of_justice", type="text", nullable=true)
     */
    private $linkToReportForMinistryOfJustice;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={}
     * )
     *
     * @ORM\Column(name="link_to_annual_report", type="text", nullable=true)
     */
    private $linkToAnnualReport;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Publication", mappedBy="KNS2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     */
    private $publications;

    /**
     * Set organizationForm
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm
     *
     * @return Application
     */
    public function setOrganizationForm(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm = null)
    {
        $this->organizationForm = $organizationForm;

        return $this;
    }

    /**
     * Get organizationForm
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm
     */
    public function getOrganizationForm()
    {
        return $this->organizationForm;
    }

    /**
     * Set personalAccount
     *
     * @param string $personalAccount
     *
     * @return Application
     */
    public function setPersonalAccount($personalAccount)
    {
        $this->personalAccount = $personalAccount;

        return $this;
    }

    /**
     * Get personalAccount
     *
     * @return string
     */
    public function getPersonalAccount()
    {
        return $this->personalAccount;
    }

    /**
     * Set isOrganizationHeadEqualProjectHead
     *
     * @param boolean $isOrganizationHeadEqualProjectHead
     *
     * @return Application
     */
    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    /**
     * Get isOrganizationHeadEqualProjectHead
     *
     * @return boolean
     */
    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    /**
     * Set nameAddressee
     *
     * @param string $nameAddressee
     *
     * @return Application
     */
    public function setNameAddressee($nameAddressee)
    {
        $this->nameAddressee = $nameAddressee;

        return $this;
    }

    /**
     * Get nameAddressee
     *
     * @return string
     */
    public function getNameAddressee()
    {
        return $this->nameAddressee;
    }

    /**
     * @return string
     */
    public function getLinkToReportForMinistryOfJustice()
    {
        return $this->linkToReportForMinistryOfJustice;
    }

    /**
     * @param string $linkToReportForMinistryOfJustice
     * @return Application
     */
    public function setLinkToReportForMinistryOfJustice($linkToReportForMinistryOfJustice)
    {
        $this->linkToReportForMinistryOfJustice = $linkToReportForMinistryOfJustice;

        return $this;
    }

    /**
     * Set linkToAnnualReport
     *
     * @param string $linkToAnnualReport
     *
     * @return BaseApplication
     */
    public function setLinkToAnnualReport($linkToAnnualReport)
    {
        $this->linkToAnnualReport = $linkToAnnualReport;

        return $this;
    }

    /**
     * Get linkToAnnualReport
     *
     * @return string
     */
    public function getLinkToAnnualReport()
    {
        return $this->linkToAnnualReport;
    }

    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     *
     * @return BaseApplication
     */
    public function addPublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $publication->setKNS2017Application($this);
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     */
    public function removePublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }
}
