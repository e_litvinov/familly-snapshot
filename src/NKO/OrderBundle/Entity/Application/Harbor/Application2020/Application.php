<?php

namespace NKO\OrderBundle\Entity\Application\Harbor\Application2020;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\AdditionalOrganizationInfoTrait;
use NKO\OrderBundle\Traits\AnalyticReport\ProjectCardTrait;
use NKO\OrderBundle\Traits\Application\AllDocumentsTrait;
use NKO\OrderBundle\Traits\Application\Harbor\Application2019\DocumentsTrait;
use NKO\OrderBundle\Traits\BankDetailsTrait;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="harbor_application_2020")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid DOC file",
 *     extensions={"doc"},
 *     fileNames={
 *       "subjectRegulation",
 *       "projectDescriptionDocument"
 *     },
 *     notNull=true,
 *     groups={"Harbor-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "regulation",
 *       "isBankAccountExist",
 *       "signedAgreement"
 *     },
 *     notNull=true,
 *     groups={"Harbor-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid excel file",
 *     extensions={"excel"},
 *     fileNames={
 *       "budget",
 *     },
 *     notNull=true,
 *     groups={"Harbor-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "authorityHead",
 *       "anotherHead",
 *       "organizationCreationResolution",
 *     },
 *     groups={"Harbor-2020"}
 * )
 */

class Application extends BaseApplication
{
    use OrganizationInfoTrait;
    use BankDetailsTrait;
    use BudgetTrait;
    use BudgetAdditionalTrait;
    use AllDocumentsTrait;
    use DocumentsTrait;
    use AdditionalOrganizationInfoTrait;
    use ProjectCardTrait;

    /**
     * @Assert\NotNull(groups={"Harbor-2020"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\TrainingGround")
     */
    protected $trainingGround;

    /**
     * @CustomAssert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"Harbor-2020"}
     * )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $linkToReportForMinistryOfJustice;

    /**
     * @ORM\Column(name="priority_direction_etc", type="text", nullable=true)
     */
    protected $priorityDirectionEtc;

    /**
     * @var bool
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    protected $isOrganizationHeadEqualProjectHead;
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Harbor-2020"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $projectDescriptionDocument;

    protected $_projectDescriptionDocument;

    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"Harbor-2019"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm")
     * @ORM\JoinColumn(name="organizationForm_id", referencedColumnName="id", nullable=true)
     */
    private $organizationForm;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Harbor-2020"}
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *            }
     *     )
     * @Assert\Length(
     *     min = 20,
     *     minMessage = "paymentAccount must be at least 20 characters long",
     *     groups={
     *
     *            }
     *     )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $personalAccount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameAddressee;

    public function getTrainingGround()
    {
        return $this->trainingGround;
    }

    public function setTrainingGround($trainingGround)
    {
        $this->trainingGround = $trainingGround;

        return $this;
    }

    public function getLinkToReportForMinistryOfJustice()
    {
        return $this->linkToReportForMinistryOfJustice;
    }

    public function setLinkToReportForMinistryOfJustice($linkToReportForMinistryOfJustice)
    {
        $this->linkToReportForMinistryOfJustice = $linkToReportForMinistryOfJustice;

        return $this;
    }

    public function setPriorityDirectionEtc($priorityDirectionEtc)
    {
        $this->priorityDirectionEtc = $priorityDirectionEtc;

        return $this;
    }

    public function getPriorityDirectionEtc()
    {
        return $this->priorityDirectionEtc;
    }

    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    public function getPracticeName()
    {
        return $this->practiceName;
    }

    public function setPracticeName($practiceName)
    {
        $this->practiceName = $practiceName;

        return $this;
    }

    public function setProjectDescriptionDocument($file)
    {
        if (!$this->_projectDescriptionDocument && is_string($this->projectDescriptionDocument)) {
            $this->_projectDescriptionDocument = $this->projectDescriptionDocument;
        }
        $this->projectDescriptionDocument = $file;

        return $this;
    }

    public function getProjectDescriptionDocument()
    {
        return $this->projectDescriptionDocument;
    }

    public function setOrganizationForm(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm = null)
    {
        $this->organizationForm = $organizationForm;

        return $this;
    }

    public function getOrganizationForm()
    {
        return $this->organizationForm;
    }

    public function setPersonalAccount($personalAccount)
    {
        $this->personalAccount = $personalAccount;

        return $this;
    }

    public function getPersonalAccount()
    {
        return $this->personalAccount;
    }

    public function setNameAddressee($nameAddressee)
    {
        $this->nameAddressee = $nameAddressee;

        return $this;
    }

    public function getNameAddressee()
    {
        return $this->nameAddressee;
    }
}
