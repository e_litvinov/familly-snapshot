<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/9/18
 * Time: 1:24 PM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="continuation_application_labor_relationship")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class LaborRelationship
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\Team", mappedBy="relationship",
     *     cascade={"all"})
     */
    protected $teams;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LaborRelationship
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add team
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Team $team
     *
     * @return LaborRelationship
     */
    public function addTeam(\NKO\OrderBundle\Entity\Application\Continuation\Team $team)
    {
        $this->teams[] = $team;
        $team->setRelationship($this);

        return $this;
    }

    /**
     * Remove team
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Team $team
     */
    public function removeTeam(\NKO\OrderBundle\Entity\Application\Continuation\Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return LaborRelationship
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
