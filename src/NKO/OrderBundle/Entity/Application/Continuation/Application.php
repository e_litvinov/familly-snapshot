<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/1/18
 * Time: 4:34 PM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\Application\Continuation\ApplicationTrait;
use NKO\OrderBundle\Traits\ExpectedResultsTrait;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\SpecialistTargetGroupTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="continuation_application_2018")
 * @ORM\Entity()
 */
class Application extends BaseApplication
{
    use PracticeDescriptionTrait;
    use SpecialistTargetGroupTrait;
    use ExpectedResultsTrait;
    use ApplicationTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="continuation_application_people_category_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    protected $peopleCategories;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\TargetGroup")
     * @ORM\JoinTable(name="continuation_application_specialist_target_group_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="target_group_id", referencedColumnName="id")}
     *      )
     */
    protected $specialistTargetGroups;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="continuation_application_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessImplementationItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessDisseminationApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessDisseminationItems;

    public function __construct()
    {
        parent::__construct();
        $this->sustainabilityPracticeApplications = new ArrayCollection();
        $this->sustainabilitySpreadApplications = new ArrayCollection();
        $this->sustainabilityMonitoringApplications = new ArrayCollection();
        $this->realizationPracticeApplications = new ArrayCollection();
        $this->spreadPracticeApplications = new ArrayCollection();
        $this->risePotentialApplications = new ArrayCollection();
        $this->peopleCategories = new ArrayCollection();
        $this->beneficiaryProblems = new ArrayCollection();
        $this->practiceRegions = new ArrayCollection();
        $this->expectedResults = new ArrayCollection();
        $this->practiceResults = new ArrayCollection();
        $this->resourceItems = new ArrayCollection();
        $this->resourceEtcItems = new ArrayCollection();
        $this->effectivenessImplementationItems = new ArrayCollection();
        $this->effectivenessImplementationEtcItems = new ArrayCollection();
        $this->effectivenessDisseminationItems = new ArrayCollection();
        $this->effectivenessDisseminationEtcItems = new ArrayCollection();
        $this->experienceItems = new ArrayCollection();
        $this->experienceEtcItems = new ArrayCollection();
        $this->processItems = new ArrayCollection();
        $this->processEtcItems = new ArrayCollection();
        $this->qualificationItems = new ArrayCollection();
        $this->qualificationEtcItems = new ArrayCollection();
        $this->resourceBlockItems = new ArrayCollection();
        $this->resourceBlockEtcItems = new ArrayCollection();
        $this->projectTeams = new ArrayCollection();
        $this->projectContPartners = new ArrayCollection();
        $this->specialistTargetGroups = new ArrayCollection();
        $this->otherSpecialistTargetGroups = new ArrayCollection();
    }

    /**
     * Add effectivenessImplementationItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationItem
     *
     * @return Application
     */
    public function addEffectivenessImplementationItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationItem)
    {
        $effectivenessImplementationItem->setEffectivenessApplication($this);
        $this->effectivenessImplementationItems[] = $effectivenessImplementationItem;

        return $this;
    }

    /**
     * Remove effectivenessImplementationItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationItem
     */
    public function removeEffectivenessImplementationItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationItem)
    {
        $this->effectivenessImplementationItems->removeElement($effectivenessImplementationItem);
    }

    /**
     * Get effectivenessImplementationItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessImplementationItems()
    {
        return $this->effectivenessImplementationItems;
    }

    /**
     * Add effectivenessDisseminationItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationItem
     *
     * @return Application
     */
    public function addEffectivenessDisseminationItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationItem)
    {
        $effectivenessDisseminationItem->setEffectivenessDisseminationApplication($this);
        $this->effectivenessDisseminationItems[] = $effectivenessDisseminationItem;

        return $this;
    }

    /**
     * Remove effectivenessDisseminationItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationItem
     */
    public function removeEffectivenessDisseminationItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationItem)
    {
        $this->effectivenessDisseminationItems->removeElement($effectivenessDisseminationItem);
    }

    /**
     * Get effectivenessDisseminationItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessDisseminationItems()
    {
        return $this->effectivenessDisseminationItems;
    }
}
