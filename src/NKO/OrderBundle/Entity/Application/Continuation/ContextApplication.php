<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/12/17
 * Time: 3:27 PM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="continuation_application_context")
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ContextApplication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $context;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="sustainabilityPracticeApplications")
     * @ORM\JoinColumn(name="sustainability_practice_application_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $sustainabilityPracticeApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="sustainabilitySpreadApplications")
     * @ORM\JoinColumn(name="sustainability_spread_application_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $sustainabilitySpreadApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="sustainabilityMonitoringApplications")
     * @ORM\JoinColumn(name="sustainability_monitoring_application_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $sustainabilityMonitoringApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->context;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return Context
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }


    /**
     * Set sustainabilityPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $sustainabilityPracticeApplication
     *
     * @return ContextApplication
     */
    public function setSustainabilityPracticeApplication(\NKO\OrderBundle\Entity\BaseApplication $sustainabilityPracticeApplication = null)
    {
        $this->sustainabilityPracticeApplication = $sustainabilityPracticeApplication;

        return $this;
    }

    /**
     * Get sustainabilityPracticeApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getSustainabilityPracticeApplication()
    {
        return $this->sustainabilityPracticeApplication;
    }

    /**
     * Set sustainabilitySpreadApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $sustainabilitySpreadApplication
     *
     * @return ContextApplication
     */
    public function setSustainabilitySpreadApplication(\NKO\OrderBundle\Entity\BaseApplication $sustainabilitySpreadApplication = null)
    {
        $this->sustainabilitySpreadApplication = $sustainabilitySpreadApplication;

        return $this;
    }

    /**
     * Get sustainabilitySpreadApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getSustainabilitySpreadApplication()
    {
        return $this->sustainabilitySpreadApplication;
    }

    /**
     * Set sustainabilityMonitoringApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $sustainabilityMonitoringApplication
     *
     * @return ContextApplication
     */
    public function setSustainabilityMonitoringApplication(\NKO\OrderBundle\Entity\BaseApplication $sustainabilityMonitoringApplication = null)
    {
        $this->sustainabilityMonitoringApplication = $sustainabilityMonitoringApplication;

        return $this;
    }

    /**
     * Get sustainabilityMonitoringApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getSustainabilityMonitoringApplication()
    {
        return $this->sustainabilityMonitoringApplication;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ContextApplication
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
