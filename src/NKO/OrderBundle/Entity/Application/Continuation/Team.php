<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/9/18
 * Time: 1:14 PM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="continuation_application_team")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Team
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $role;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $responsibility;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\LaborRelationship", inversedBy="teams")
     * @ORM\JoinColumn(name="relation_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $relationship;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isNew;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="projectTeams")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        if (!$this->relationship) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('relationship')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Team
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set responsibility
     *
     * @param string $responsibility
     *
     * @return Team
     */
    public function setResponsibility($responsibility)
    {
        $this->responsibility = $responsibility;

        return $this;
    }

    /**
     * Get responsibility
     *
     * @return string
     */
    public function getResponsibility()
    {
        return $this->responsibility;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Team
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isNew
     *
     * @param boolean $isNew
     *
     * @return Team
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;

        return $this;
    }

    /**
     * Get isNew
     *
     * @return boolean
     */
    public function getIsNew()
    {
        return $this->isNew;
    }

    /**
     * Set relationship
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\LaborRelationship $relationship
     *
     * @return Team
     */
    public function setRelationship(\NKO\OrderBundle\Entity\Application\Continuation\LaborRelationship $relationship = null)
    {
        $this->relationship = $relationship;

        return $this;
    }

    /**
     * Get relationship
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\LaborRelationship
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $application
     *
     * @return Team
     */
    public function setApplication(\NKO\OrderBundle\Entity\BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Team
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
