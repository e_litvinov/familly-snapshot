<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/9/18
 * Time: 3:45 PM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="continuation_application_expected_result")
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ExpectedResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ResultDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id", nullable=true)
     */
    protected $direction;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $customDirection;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $indicator;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $measurementMethod;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="experience_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $experienceApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="experience_etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $experienceEtcApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="process_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $processApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="process__etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $processEtcApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="qualification_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $qualificationApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="qualification_etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $qualificationEtcApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="resource_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $resourceApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="resource_etc_application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $resourceEtcApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customDirection
     *
     * @param string $customDirection
     *
     * @return ExpectedResult
     */
    public function setCustomDirection($customDirection)
    {
        $this->customDirection = $customDirection;

        return $this;
    }

    /**
     * Get customDirection
     *
     * @return string
     */
    public function getCustomDirection()
    {
        return $this->customDirection;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return ExpectedResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set measurementMethod
     *
     * @param string $measurementMethod
     *
     * @return ExpectedResult
     */
    public function setMeasurementMethod($measurementMethod)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return string
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ExpectedResult
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set direction
     *
     * @param ResultDirection $direction
     *
     * @return ExpectedResult
     */
    public function setDirection(ResultDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return ResultDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set experienceApplication
     *
     * @param BaseApplication $experienceApplication
     *
     * @return ExpectedResult
     */
    public function setExperienceApplication(BaseApplication $experienceApplication = null)
    {
        $this->experienceApplication = $experienceApplication;

        return $this;
    }

    /**
     * Get experienceApplication
     *
     * @return BaseApplication
     */
    public function getExperienceApplication()
    {
        return $this->experienceApplication;
    }

    /**
     * Set experienceEtcApplication
     *
     * @param BaseApplication $experienceEtcApplication
     *
     * @return ExpectedResult
     */
    public function setExperienceEtcApplication(BaseApplication $experienceEtcApplication = null)
    {
        $this->experienceEtcApplication = $experienceEtcApplication;

        return $this;
    }

    /**
     * Get experienceEtcApplication
     *
     * @return BaseApplication
     */
    public function getExperienceEtcApplication()
    {
        return $this->experienceEtcApplication;
    }

    /**
     * Set processApplication
     *
     * @param BaseApplication $processApplication
     *
     * @return ExpectedResult
     */
    public function setProcessApplication(BaseApplication $processApplication = null)
    {
        $this->processApplication = $processApplication;

        return $this;
    }

    /**
     * Get processApplication
     *
     * @return BaseApplication
     */
    public function getProcessApplication()
    {
        return $this->processApplication;
    }

    /**
     * Set processEtcApplication
     *
     * @param BaseApplication $processEtcApplication
     *
     * @return ExpectedResult
     */
    public function setProcessEtcApplication(BaseApplication $processEtcApplication = null)
    {
        $this->processEtcApplication = $processEtcApplication;

        return $this;
    }

    /**
     * Get processEtcApplication
     *
     * @return BaseApplication
     */
    public function getProcessEtcApplication()
    {
        return $this->processEtcApplication;
    }

    /**
     * Set qualificationApplication
     *
     * @param BaseApplication $qualificationApplication
     *
     * @return ExpectedResult
     */
    public function setQualificationApplication(BaseApplication $qualificationApplication = null)
    {
        $this->qualificationApplication = $qualificationApplication;

        return $this;
    }

    /**
     * Get qualificationApplication
     *
     * @return BaseApplication
     */
    public function getQualificationApplication()
    {
        return $this->qualificationApplication;
    }

    /**
     * Set qualificationEtcApplication
     *
     * @param BaseApplication $qualificationEtcApplication
     *
     * @return ExpectedResult
     */
    public function setQualificationEtcApplication(BaseApplication $qualificationEtcApplication = null)
    {
        $this->qualificationEtcApplication = $qualificationEtcApplication;

        return $this;
    }

    /**
     * Get qualificationEtcApplication
     *
     * @return BaseApplication
     */
    public function getQualificationEtcApplication()
    {
        return $this->qualificationEtcApplication;
    }

    /**
     * Set resourceApplication
     *
     * @param BaseApplication $resourceApplication
     *
     * @return ExpectedResult
     */
    public function setResourceApplication(BaseApplication $resourceApplication = null)
    {
        $this->resourceApplication = $resourceApplication;

        return $this;
    }

    /**
     * Get resourceApplication
     *
     * @return BaseApplication
     */
    public function getResourceApplication()
    {
        return $this->resourceApplication;
    }

    /**
     * Set resourceEtcApplication
     *
     * @param BaseApplication $resourceEtcApplication
     *
     * @return ExpectedResult
     */
    public function setResourceEtcApplication(BaseApplication $resourceEtcApplication = null)
    {
        $this->resourceEtcApplication = $resourceEtcApplication;

        return $this;
    }

    /**
     * Get resourceEtcApplication
     *
     * @return BaseApplication
     */
    public function getResourceEtcApplication()
    {
        return $this->resourceEtcApplication;
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018", "Farvater-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidNameResource(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getDirection() && !$this->getCustomDirection()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('customDirection')
                ->addViolation();
        }
    }



    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ExpectedResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
