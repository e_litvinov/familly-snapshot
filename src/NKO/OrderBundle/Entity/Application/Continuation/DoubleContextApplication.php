<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/12/17
 * Time: 4:50 PM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="continuation_application_double_context")
 * @ORM\Entity("")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class DoubleContextApplication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     *  @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "Farvater-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     *  @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "Farvater-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $context;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="realizationPracticeApplications")
     * @ORM\JoinColumn(name="realization_practice_application_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $realizationPracticeApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="spreadPracticeApplications")
     * @ORM\JoinColumn(name="spread_practice_application_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $spreadPracticeApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="risePotentialApplications")
     * @ORM\JoinColumn(name="rise_potential_application_id", referencedColumnName="id",  onDelete="CASCADE")
     */
    protected $risePotentialApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return DoubleContext
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set context
     *
     * @param string $context
     *
     * @return DoubleContext
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set realizationPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $realizationPracticeApplication
     *
     * @return DoubleContextApplication
     */
    public function setRealizationPracticeApplication(\NKO\OrderBundle\Entity\BaseApplication $realizationPracticeApplication = null)
    {
        $this->realizationPracticeApplication = $realizationPracticeApplication;

        return $this;
    }

    /**
     * Get realizationPracticeApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getRealizationPracticeApplication()
    {
        return $this->realizationPracticeApplication;
    }

    /**
     * Set spreadPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $spreadPracticeApplication
     *
     * @return DoubleContextApplication
     */
    public function setSpreadPracticeApplication(\NKO\OrderBundle\Entity\BaseApplication $spreadPracticeApplication = null)
    {
        $this->spreadPracticeApplication = $spreadPracticeApplication;

        return $this;
    }

    /**
     * Get spreadPracticeApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getSpreadPracticeApplication()
    {
        return $this->spreadPracticeApplication;
    }

    /**
     * Set risePotentialApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $risePotentialApplication
     *
     * @return DoubleContextApplication
     */
    public function setRisePotentialApplication(\NKO\OrderBundle\Entity\BaseApplication $risePotentialApplication = null)
    {
        $this->risePotentialApplication = $risePotentialApplication;

        return $this;
    }

    /**
     * Get risePotentialApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getRisePotentialApplication()
    {
        return $this->risePotentialApplication;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return DoubleContextApplication
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
