<?php

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="continuation_application_resource")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Resource
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @ORM\Column(name="name_resource", nullable=true)
     */
    private $nameResource;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(name="description_resource", nullable=true, type="text")
     */
    private $descriptionResource;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ResourceType")
     * @ORM\JoinColumn(name="name_resource_type_id", referencedColumnName="id")
     */
    private $nameResourceType;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="resource_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $resourceApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="resource_etc_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $resourceEtcApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameResource
     *
     * @param string $nameResource
     *
     * @return Resource
     */
    public function setNameResource($nameResource)
    {
        $this->nameResource = $nameResource;

        return $this;
    }

    /**
     * Get nameResource
     *
     * @return string
     */
    public function getNameResource()
    {
        return $this->nameResource;
    }

    /**
     * Set descriptionResource
     *
     * @param string $descriptionResource
     *
     * @return Resource
     */
    public function setDescriptionResource($descriptionResource)
    {
        $this->descriptionResource = $descriptionResource;

        return $this;
    }

    /**
     * Get descriptionResource
     *
     * @return string
     */
    public function getDescriptionResource()
    {
        return $this->descriptionResource;
    }

    /**
     * Set nameResourceType
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ResourceType $nameResourceType
     *
     * @return Resource
     */
    public function setNameResourceType(\NKO\OrderBundle\Entity\Application\Continuation\ResourceType $nameResourceType = null)
    {
        $this->nameResourceType = $nameResourceType;

        return $this;
    }

    /**
     * Get nameResourceType
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\ResourceType
     */
    public function getNameResourceType()
    {
        return $this->nameResourceType;
    }

    /**
     * Set resourceApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $resourceApplication
     *
     * @return Resource
     */
    public function setResourceApplication(\NKO\OrderBundle\Entity\BaseApplication $resourceApplication = null)
    {
        $this->resourceApplication = $resourceApplication;

        return $this;
    }

    /**
     * Get resourceApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getResourceApplication()
    {
        return $this->resourceApplication;
    }

    /**
     * Set resourceEtcApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $resourceEtcApplication
     *
     * @return Resource
     */
    public function setResourceEtcApplication(\NKO\OrderBundle\Entity\BaseApplication $resourceEtcApplication = null)
    {
        $this->resourceEtcApplication = $resourceEtcApplication;

        return $this;
    }

    /**
     * Get resourceEtcApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getResourceEtcApplication()
    {
        return $this->resourceEtcApplication;
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidNameResource(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getNameResourceType() && !$this->getNameResource()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('nameResource')
                ->addViolation();
        }
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Resource
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
