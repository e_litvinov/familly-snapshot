<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.9.18
 * Time: 16.09
 */

namespace NKO\OrderBundle\Entity\Application\Continuation\SecondStage;

use NKO\OrderBundle\Entity\BaseApplication;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\Application\Continuation\ApplicationTrait;
use NKO\OrderBundle\Traits\ExpectedResultsTrait;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\SpecialistTargetGroupTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="continuation_application_second_stage_2018")
 * @ORM\Entity()
 */
class Application extends BaseApplication
{
    use PracticeDescriptionTrait;
    use SpecialistTargetGroupTrait;
    use ExpectedResultsTrait;
    use ApplicationTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $expectedResultsComment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $effectivenessImplementationItemsComment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceResultsComment;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $effectivenessDisseminationItemsComment;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="continuation_application_people_category_second_stage",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    protected $peopleCategories;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\TargetGroup")
     * @ORM\JoinTable(name="continuation_application_specialist_target_group_second_stage",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="target_group_id", referencedColumnName="id")}
     *      )
     */
    protected $specialistTargetGroups;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="continuation_application_practice_region_second_stage",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(
     *     targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory",
     *      mappedBy="application", orphanRemoval=true, cascade={"all"}
     *     )
     */
    protected $territories;

    public function __construct()
    {
        $this->projectTeams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projectContPartners = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add territory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory
     *
     * @return Application
     */
    public function addTerritory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory)
    {
        $this->territories[] = $territory;
        $territory->setApplication($this);
        return $this;
    }

    /**
     * Remove territory
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory
     *
     * @return Application
     */
    public function removeTerritory(\NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory $territory)
    {
        $this->territories->removeElement($territory);

        return $this;
    }

    /**
     * Get territories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerritories()
    {
        return $this->territories;
    }

    /**
     * @return string
     */
    public function getExpectedResultsComment()
    {
        return $this->expectedResultsComment;
    }

    /**
     * @param string $expectedResultsComment
     *
     * @return Application
     */
    public function setExpectedResultsComment($expectedResultsComment)
    {
        $this->expectedResultsComment = $expectedResultsComment;

        return $this;
    }

    /**
     * @return string
     */
    public function getEffectivenessImplementationItemsComment()
    {
        return $this->effectivenessImplementationItemsComment;
    }

    /**
     * @param string $effectivenessImplementationItemsComment
     *
     * @return Application
     */
    public function setEffectivenessImplementationItemsComment($effectivenessImplementationItemsComment)
    {
        $this->effectivenessImplementationItemsComment = $effectivenessImplementationItemsComment;

        return $this;
    }

    /**
     * @return string
     */
    public function getPracticeResultsComment()
    {
        return $this->practiceResultsComment;
    }

    /**
     * @param string $practiceResultsComment
     *
     * @return Application
     */
    public function setPracticeResultsComment($practiceResultsComment)
    {
        $this->practiceResultsComment = $practiceResultsComment;

        return $this;
    }

    /**
     * @return string
     */
    public function getEffectivenessDisseminationItemsComment()
    {
        return $this->effectivenessDisseminationItemsComment;
    }

    /**
     * @param string $effectivenessDisseminationItemsComment
     *
     * @return Application
     */
    public function setEffectivenessDisseminationItemsComment($effectivenessDisseminationItemsComment)
    {
        $this->effectivenessDisseminationItemsComment = $effectivenessDisseminationItemsComment;

        return $this;
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPeopleCategories();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose people categories')
                ->atPath('peopleCategories')
                ->addViolation();
        }
    }
}
