<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/10/18
 * Time: 5:48 AM
 */

namespace NKO\OrderBundle\Entity\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="continuation_application_project_partner")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ProjectPartner
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $organizationName;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $briefDescription;

    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $participation;

    /**
     * @var bool
     * @Assert\NotBlank(groups={"ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isNew;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="projectContPartners")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organizationName
     *
     * @param string $organizationName
     *
     * @return ProjectPartner
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    /**
     * Get organizationName
     *
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * Set briefDescription
     *
     * @param string $briefDescription
     *
     * @return ProjectPartner
     */
    public function setBriefDescription($briefDescription)
    {
        $this->briefDescription = $briefDescription;

        return $this;
    }

    /**
     * Get briefDescription
     *
     * @return string
     */
    public function getBriefDescription()
    {
        return $this->briefDescription;
    }

    /**
     * Set participation
     *
     * @param string $participation
     *
     * @return ProjectPartner
     */
    public function setParticipation($participation)
    {
        $this->participation = $participation;

        return $this;
    }

    /**
     * Get participation
     *
     * @return string
     */
    public function getParticipation()
    {
        return $this->participation;
    }

    /**
     * Set isNew
     *
     * @param boolean $isNew
     *
     * @return ProjectPartner
     */
    public function setIsNew($isNew)
    {
        $this->isNew = $isNew;

        return $this;
    }

    /**
     * Get isNew
     *
     * @return boolean
     */
    public function getIsNew()
    {
        return $this->isNew;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $application
     *
     * @return ProjectPartner
     */
    public function setApplication(\NKO\OrderBundle\Entity\BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ProjectPartner
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
