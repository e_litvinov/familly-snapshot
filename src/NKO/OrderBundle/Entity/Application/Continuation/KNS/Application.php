<?php

namespace NKO\OrderBundle\Entity\Application\Continuation\KNS;

use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\ResourceTrait;
use NKO\OrderBundle\Traits\Application\Continuation\KNS\Kns2018ApplicationTrait;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Effectiveness;

/**
 * @ORM\Table(name="continuation_kns_2019")
 * @ORM\Entity()
 */
class Application extends BaseApplication
{
    use OrganizationInfoTrait;
    use KnsApplicationTrait;
    use ResourceTrait;
    use PracticeDescriptionTrait;
    use Kns2018ApplicationTrait;
    use BudgetAdditionalTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="continuation_kns_people_category_relationship",
     * joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     * inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $peopleCategories;

    /**
     * @Assert\Count(
     *     min="1",
     *     minMessage="value is invalid(field must be non empty)",
     *     groups={
     *         "ContinuationApplicationKNS"
     *     }
     * )
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="continuation_kns_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="continuation_kns_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", inversedBy="followingApplications", cascade={"detach"})
     * @ORM\JoinColumn(name="linked_application_history_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $linkedApplicationHistory;

    /**
     * @var float
     *  @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplicationKNS"}
     *     )
     * @ORM\Column(name="sum_grant", type="float", nullable=true)
     */
    protected $sumGrant;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\KNS\Effectiveness", mappedBy="effectivenessApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessKNSItems;

    /**
     * Set linkedApplicationHistory
     *
     * @param ApplicationHistory $linkedApplicationHistory
     *
     * @return Application
     */
    public function setLinkedApplicationHistory(ApplicationHistory $linkedApplicationHistory = null)
    {
        $this->linkedApplicationHistory = $linkedApplicationHistory;

        return $this;
    }

    /**
     * Get linkedApplicationHistory
     *
     * @return ApplicationHistory
     */
    public function getLinkedApplicationHistory()
    {
        return $this->linkedApplicationHistory;
    }

    /**
     * Set sumGrant
     *
     * @param float $sumGrant
     *
     * @return Application
     */
    public function setSumGrant($sumGrant)
    {
        $this->sumGrant = $sumGrant;

        return $this;
    }

    /**
     * Get sumGrant
     *
     * @return float
     */
    public function getSumGrant()
    {
        return $this->sumGrant;
    }

    public function __construct()
    {
        parent::__construct();
        $this->isAddressesEqual = true;
        $this->practiceRegions = new ArrayCollection();
    }

    /**
     * Add effectivenessItem
     *
     * @param Effectiveness $effectivenessItem
     *
     * @return Application
     */
    public function addEffectivenessKNSItem(Effectiveness $effectivenessItem)
    {
        $this->effectivenessKNSItems[] = $effectivenessItem;
        $effectivenessItem->setEffectivenessApplication($this);
        return $this;
    }

    /**
     * Remove effectivenessItem
     *
     * @param Effectiveness $effectivenessItem
     */
    public function removeEffectivenessKNSItem(Effectiveness $effectivenessItem)
    {
        $this->effectivenessKNSItems->removeElement($effectivenessItem);
    }

    /**
     * Get effectivenessItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessKNSItems()
    {
        return $this->effectivenessKNSItems;
    }
}
