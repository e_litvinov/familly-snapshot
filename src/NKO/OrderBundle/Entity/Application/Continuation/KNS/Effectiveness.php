<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.12.18
 * Time: 11.41
 */

namespace NKO\OrderBundle\Entity\Application\Continuation\KNS;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator;

/**
 * @ORM\Table(name="continuation_kns2018_effectiveness")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Effectiveness
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *     }
     * )
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    protected $result;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator", fetch="EAGER")
     * @ORM\JoinColumn(name="indicator_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */

    protected $indicator;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *     }
     * )
     *
     * @ORM\Column(name="custom_indicator", type="text", nullable=true)
     */
    protected $customIndicator;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *     }
     *  )
     *
     * @ORM\Column(name="indicator_value", type="integer", nullable=true)
     */
    protected $indicatorValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *        "KNS2018-3"
     *     }
     * )
     *
     * @ORM\Column(name="fact_value", type="integer", nullable=true)
     */
    protected $factValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *        "KNS2018-3"
     *     }
     * )
     *
     * @ORM\Column(name="plan_value", type="integer", nullable=true)
     */
    protected $planValue;

    /**
     * @Assert\Valid
     * @Assert\Count(
     *     min="1",
     *     minMessage="You should choice methods.",
     *     groups={
     *         "KNS2018-3"
     *     }
     * )
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinTable(name="kns_effectiveness_measurement_method_relationship",
     *      joinColumns={@ORM\JoinColumn(name="effectiveness_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")}
     *      )
     */
    protected $methods;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *     }
     * )
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="effectiveness_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $effectivenessApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Effectiveness
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicatorValue
     *
     * @param integer $indicatorValue
     *
     * @return Effectiveness
     */
    public function setIndicatorValue($indicatorValue)
    {
        $this->indicatorValue = $indicatorValue;

        return $this;
    }

    /**
     * Get indicatorValue
     *
     */
    public function getIndicatorValue()
    {
        return $this->indicatorValue;
    }

    /**
     * Set effectivenessApplication
     *
     * @param BaseApplication $effectivenessApplication
     *
     * @return Effectiveness
     */
    public function setEffectivenessApplication(BaseApplication $effectivenessApplication = null)
    {
        $this->effectivenessApplication = $effectivenessApplication;

        return $this;
    }

    /**
     * Get effectivenessApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application
     */
    public function getEffectivenessApplication()
    {
        return $this->effectivenessApplication;
    }

    /**
     * Set indicator
     *
     * @param Indicator $indicator
     *
     * @return Effectiveness
     */
    public function setIndicator(Indicator $indicator = null)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return Indicator
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set customIndicator
     *
     * @param string $customIndicator
     *
     * @return Effectiveness
     */
    public function setCustomIndicator($customIndicator)
    {
        $this->customIndicator = $customIndicator;

        return $this;
    }

    /**
     * Get customIndicator
     *
     * @return string
     */
    public function getCustomIndicator()
    {
        return $this->customIndicator;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Effectiveness
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set factValue
     *
     * @param integer $factValue
     *
     * @return Effectiveness
     */
    public function setFactValue($factValue)
    {
        $this->factValue = $factValue;

        return $this;
    }

    /**
     * Get factValue
     *
     * @return integer
     */
    public function getFactValue()
    {
        return $this->factValue;
    }

    /**
     * Set planValue
     *
     * @param integer $planValue
     *
     * @return Effectiveness
     */
    public function setPlanValue($planValue)
    {
        $this->planValue = $planValue;

        return $this;
    }

    /**
     * Get planValue
     *
     * @return integer
     */
    public function getPlanValue()
    {
        return $this->planValue;
    }

    /**
     * Set measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     *
     * @return Effectiveness
     */
    public function setMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod = null)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Effectiveness
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add method
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method
     *
     * @return Effectiveness
     */
    public function addMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods[] = $method;

        return $this;
    }

    /**
     * Remove method
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method
     */
    public function removeMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods->removeElement($method);
    }

    /**
     * Get methods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMethods()
    {
        return $this->methods;
    }
}
