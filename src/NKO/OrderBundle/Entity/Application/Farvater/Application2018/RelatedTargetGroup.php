<?php

namespace NKO\OrderBundle\Entity\Application\Farvater\Application2018;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Indicator
 *
 * @ORM\Table(name="farvater_application_2018_related_target_group")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Application\Farvater\Application2018\IndicatorRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class RelatedTargetGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="project_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $projectApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="practice_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $practiceApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;    

    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indexNumber
     *
     * @param integer $indexNumber
     *
     * @return Indicator
     */
    public function setIndexNumber($indexNumber)
    {
        $this->indexNumber = $indexNumber;

        return $this;
    }

    /**
     * Get indexNumber
     *
     * @return int
     */
    public function getIndexNumber()
    {
        return $this->indexNumber;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Indicator
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Indicator
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->results = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set projectApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $projectApplication
     *
     * @return RelatedTargetGroup
     */
    public function setProjectApplication(\NKO\OrderBundle\Entity\BaseApplication $projectApplication = null)
    {
        $this->projectApplication = $projectApplication;

        return $this;
    }

    /**
     * Get projectApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getProjectApplication()
    {
        return $this->projectApplication;
    }

    /**
     * Set practiceApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $practiceApplication
     *
     * @return RelatedTargetGroup
     */
    public function setPracticeApplication(\NKO\OrderBundle\Entity\BaseApplication $practiceApplication = null)
    {
        $this->practiceApplication = $practiceApplication;

        return $this;
    }

    /**
     * Get practiceApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getPracticeApplication()
    {
        return $this->practiceApplication;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return RelatedTargetGroup
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
