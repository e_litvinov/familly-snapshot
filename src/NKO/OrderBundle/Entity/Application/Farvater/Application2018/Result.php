<?php

namespace NKO\OrderBundle\Entity\Application\Farvater\Application2018;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Result
 *
 * @ORM\Table(name="farvater_application_2018_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Application\Farvater\Application2018\ResultRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Result
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult")
     * @ORM\JoinColumn(name="social_result_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $socialResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedTargetGroup")
     * @ORM\JoinColumn(name="target_group_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $targetGroup;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"Farvater-2018"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $targetValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"Farvater-2018"})
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $approximateValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"Farvater-2018"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $service;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator")
     * @ORM\JoinColumn(name="indicator_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $indicator;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="projectSocialResults")
     * @ORM\JoinColumn(name="project_social_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $projectSocialResultApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="projectSocialIndividualResults")
     * @ORM\JoinColumn(name="project_social_individual_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $projectSocialIndividualResultApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="projectDirectResults")
     * @ORM\JoinColumn(name="project_direct_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $projectDirectResultApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="projectDirectIndividualResults")
     * @ORM\JoinColumn(name="project_direct_individual_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $projectDirectIndividualResultApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="practiceSocialResults")
     * @ORM\JoinColumn(name="practice_social_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $practiceSocialResultApplication;


    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="practiceDirectResults")
     * @ORM\JoinColumn(name="practice_direct_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $practiceDirectResultApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="practiceDirectIndividualResults")
     * @ORM\JoinColumn(name="practice_direct_individual_result_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $practiceDirectIndividualResultApplication;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $customIndicator;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", inversedBy="linkedFarvaterResult")
     * @ORM\JoinColumn(name="direct_result", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $directResult;

    /**
     * @Assert\Callback(groups={"Farvater-2018"})
     */
    public function isValidIndicator(ExecutionContextInterface $context, $payload)
    {
        if (!$this->indicator && ($this->projectDirectResultApplication || $this->projectSocialResultApplication)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('indicator')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"Farvater-2018"})
     */
    public function isValidCustomIndicator(ExecutionContextInterface $context, $payload)
    {
        if (!$this->customIndicator && ($this->projectDirectIndividualResultApplication || $this->projectSocialIndividualResultApplication)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('customIndicator')
                ->addViolation();
        }
    }


    /**
     * @Assert\Callback(groups={"Farvater-2018"})
     */
    public function isValidContent(ExecutionContextInterface $context, $payload)
    {
        if (!$this->content && ($this->projectDirectIndividualResultApplication || $this->projectDirectResultApplication)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('content')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set targetGroup
     *
     * @param string $targetGroup
     *
     * @return Result
     */
    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return string
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    /**
     * Set targetValue
     *
     * @param integer $targetValue
     *
     * @return Result
     */
    public function setTargetValue($targetValue)
    {
        $this->targetValue = $targetValue;

        return $this;
    }

    /**
     * Get targetValue
     *
     * @return int
     */
    public function getTargetValue()
    {
        return $this->targetValue;
    }

    /**
     * Set approximateValue
     *
     * @param integer $approximateValue
     *
     * @return Result
     */
    public function setApproximateValue($approximateValue)
    {
        $this->approximateValue = $approximateValue;

        return $this;
    }

    /**
     * Get approximateValue
     *
     * @return int
     */
    public function getApproximateValue()
    {
        return $this->approximateValue;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return Result
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set indicator
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator $indicator
     *
     * @return Result
     */
    public function setIndicator(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator $indicator = null)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Indicator
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set customIndicator
     *
     * @param string $customIndicator
     *
     * @return Result
     */
    public function setCustomIndicator($customIndicator)
    {
        $this->customIndicator = $customIndicator;

        return $this;
    }

    /**
     * Get customIndicator
     *
     * @return string
     */
    public function getCustomIndicator()
    {
        return $this->customIndicator;
    }

    /**
     * Set projectSocialResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectSocialResultApplication
     *
     * @return Result
     */
    public function setProjectSocialResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectSocialResultApplication = null)
    {
        $this->projectSocialResultApplication = $projectSocialResultApplication;

        return $this;
    }

    /**
     * Get projectSocialResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getProjectSocialResultApplication()
    {
        return $this->projectSocialResultApplication;
    }

    /**
     * Set projectDirectResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectDirectResultApplication
     *
     * @return Result
     */
    public function setProjectDirectResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectDirectResultApplication = null)
    {
        $this->projectDirectResultApplication = $projectDirectResultApplication;

        return $this;
    }

    /**
     * Get projectDirectResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getProjectDirectResultApplication()
    {
        return $this->projectDirectResultApplication;
    }

    /**
     * Set projectSocialIndividualResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectSocialIndividualResultApplication
     *
     * @return Result
     */
    public function setProjectSocialIndividualResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectSocialIndividualResultApplication = null)
    {
        $this->projectSocialIndividualResultApplication = $projectSocialIndividualResultApplication;

        return $this;
    }

    /**
     * Get projectSocialIndividualResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getProjectSocialIndividualResultApplication()
    {
        return $this->projectSocialIndividualResultApplication;
    }

    /**
     * Set projectDirectIndividualResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectDirectIndividualResultApplication
     *
     * @return Result
     */
    public function setProjectDirectIndividualResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $projectDirectIndividualResultApplication = null)
    {
        $this->projectDirectIndividualResultApplication = $projectDirectIndividualResultApplication;

        return $this;
    }

    /**
     * Get projectDirectIndividualResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getProjectDirectIndividualResultApplication()
    {
        return $this->projectDirectIndividualResultApplication;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Result
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set socialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult $socialResult
     *
     * @return Result
     */
    public function setSocialResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult $socialResult = null)
    {
        $this->socialResult = $socialResult;

        return $this;
    }

    /**
     * Get socialResult
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedSocialResult
     */
    public function getSocialResult()
    {
        return $this->socialResult;
    }

    /**
     * Set practiceSocialResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $practiceSocialResultApplication
     *
     * @return Result
     */
    public function setPracticeSocialResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $practiceSocialResultApplication = null)
    {
        $this->practiceSocialResultApplication = $practiceSocialResultApplication;

        return $this;
    }

    /**
     * Get practiceSocialResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getPracticeSocialResultApplication()
    {
        return $this->practiceSocialResultApplication;
    }

    /**
     * Set practiceDirectResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $practiceDirectResultApplication
     *
     * @return Result
     */
    public function setPracticeDirectResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $practiceDirectResultApplication = null)
    {
        $this->practiceDirectResultApplication = $practiceDirectResultApplication;

        return $this;
    }

    /**
     * Get practiceDirectResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getPracticeDirectResultApplication()
    {
        return $this->practiceDirectResultApplication;
    }

    /**
     * Set practiceDirectIndividualResultApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $practiceDirectIndividualResultApplication
     *
     * @return Result
     */
    public function setPracticeDirectIndividualResultApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $practiceDirectIndividualResultApplication = null)
    {
        $this->practiceDirectIndividualResultApplication = $practiceDirectIndividualResultApplication;

        return $this;
    }

    /**
     * Get practiceDirectIndividualResultApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getPracticeDirectIndividualResultApplication()
    {
        return $this->practiceDirectIndividualResultApplication;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Result
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return Result
     */
    public function setDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult = null)
    {
        $this->directResult = $directResult;

        return $this;
    }

    /**
     * Get directResult
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult
     */
    public function getDirectResult()
    {
        return $this->directResult;
    }
}
