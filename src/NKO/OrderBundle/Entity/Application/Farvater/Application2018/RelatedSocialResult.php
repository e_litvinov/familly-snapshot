<?php

namespace NKO\OrderBundle\Entity\Application\Farvater\Application2018;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * RelatedSocialResult
 *
 * @ORM\Table(name="farvater_application_2018_related_social_result")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class RelatedSocialResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="project_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $projectApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="practice_application_id", referencedColumnName="id", onDelete="cascade")
     */
    private $practiceApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;    

    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return RelatedSocialResult
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return RelatedSocialResult
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set projectApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $projectApplication
     *
     * @return RelatedSocialResult
     */
    public function setProjectApplication(\NKO\OrderBundle\Entity\BaseApplication $projectApplication = null)
    {
        $this->projectApplication = $projectApplication;

        return $this;
    }

    /**
     * Get projectApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getProjectApplication()
    {
        return $this->projectApplication;
    }

    /**
     * Set practiceApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $practiceApplication
     *
     * @return RelatedSocialResult
     */
    public function setPracticeApplication(\NKO\OrderBundle\Entity\BaseApplication $practiceApplication = null)
    {
        $this->practiceApplication = $practiceApplication;

        return $this;
    }

    /**
     * Get practiceApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getPracticeApplication()
    {
        return $this->practiceApplication;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return RelatedSocialResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
