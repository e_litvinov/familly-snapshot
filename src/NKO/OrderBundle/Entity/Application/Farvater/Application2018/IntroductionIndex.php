<?php

namespace NKO\OrderBundle\Entity\Application\Farvater\Application2018;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="farvater_application_2018_introduction_index")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class IntroductionIndex
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="index_name", type="text", nullable=true)
     */
    private $indexName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(name="first_year_target_value", type="text", nullable=true)
     */
    private $firstYearTargetValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(name="second_year_target_value", type="text", nullable=true)
     */
    private $secondYearTargetValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(name="third_year_target_value", type="text", nullable=true)
     */
    private $thirdYearTargetValue;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="introductionPractices")
     * @ORM\JoinColumn(name="application_farvater_2018_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set indexName
     *
     * @param string $indexName
     *
     * @return IntroductionIndex
     */
    public function setIndexName($indexName)
    {
        $this->indexName = $indexName;

        return $this;
    }

    /**
     * Get indexName
     *
     * @return string
     */
    public function getIndexName()
    {
        return $this->indexName;
    }

    /**
     * Set firstYearTargetValue
     *
     * @param string $firstYearTargetValue
     *
     * @return IntroductionIndex
     */
    public function setFirstYearTargetValue($firstYearTargetValue)
    {
        $this->firstYearTargetValue = $firstYearTargetValue;

        return $this;
    }

    /**
     * Get firstYearTargetValue
     *
     * @return string
     */
    public function getFirstYearTargetValue()
    {
        return $this->firstYearTargetValue;
    }

    /**
     * Set secondYearTargetValue
     *
     * @param string $secondYearTargetValue
     *
     * @return IntroductionIndex
     */
    public function setSecondYearTargetValue($secondYearTargetValue)
    {
        $this->secondYearTargetValue = $secondYearTargetValue;

        return $this;
    }

    /**
     * Get secondYearTargetValue
     *
     * @return string
     */
    public function getSecondYearTargetValue()
    {
        return $this->secondYearTargetValue;
    }

    /**
     * Set thirdYearTargetValue
     *
     * @param string $thirdYearTargetValue
     *
     * @return IntroductionIndex
     */
    public function setThirdYearTargetValue($thirdYearTargetValue)
    {
        $this->thirdYearTargetValue = $thirdYearTargetValue;

        return $this;
    }

    /**
     * Get thirdYearTargetValue
     *
     * @return string
     */
    public function getThirdYearTargetValue()
    {
        return $this->thirdYearTargetValue;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $application
     *
     * @return IntroductionIndex
     */
    public function setApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return IntroductionIndex
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
