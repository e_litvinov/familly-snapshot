<?php

namespace NKO\OrderBundle\Entity\Application\Farvater\Application2018;

use NKO\OrderBundle\Entity\BaseApplication;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\ExpectedResultsTrait;
use NKO\OrderBundle\Traits\SpecialistTargetGroupTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Traits\ScheduleTrait;
use NKO\OrderBundle\Traits\ResourceTrait;
use NKO\OrderBundle\Traits\BudgetTrait;
use NKO\OrderBundle\Traits\DocumentTrait;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="farvater_application_2018")
 * @ORM\Entity()
 */
class Application extends BaseApplication
{
    use SpecialistTargetGroupTrait;
    use ExpectedResultsTrait;
    use ScheduleTrait;
    use ResourceTrait;
    use BudgetTrait;
    use DocumentTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\TargetGroup")
     * @ORM\JoinTable(name="farvater_application_2018_specialist_target_group_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="target_group_id", referencedColumnName="id")}
     *      )
     */
    protected $specialistTargetGroups;

    /**
     * @var string
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication", mappedBy="realizationPracticeApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $realizationPracticeApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication", mappedBy="spreadPracticeApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $spreadPracticeApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication", mappedBy="risePotentialApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $risePotentialApplications;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */

    protected $projectPurpose;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $tasksHeadTarget;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $motivationSpreadPractice;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $experienceSpread;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $technologySpread;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $introductionProjectPurpose;

    /**
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $taskOfTarget;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $wishDevelopPractice;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $monitoringProjectPurpose;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Risk", mappedBy="conditionRealizationApplication",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $realizationPractices;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Risk", mappedBy="conditionSpreadApplication",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $spreadPractices;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="projectSocialResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $projectSocialResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="projectSocialIndividualResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $projectSocialIndividualResults;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectSocialComment;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="projectDirectResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $projectDirectResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="projectDirectIndividualResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $projectDirectIndividualResults;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectDirectComment;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="practiceSocialResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $practiceSocialResults;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceSocialComment;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="practiceDirectResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $practiceDirectResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result", mappedBy="practiceDirectIndividualResultApplication", cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     */
    protected $practiceDirectIndividualResults;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $practiceDirectComment;

    /**
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="authority_head", type="text", nullable=true)
     */
    protected $authorityHead;

    protected $_authorityHead;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $descriptionSectionComment;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $spreadSectionComment;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $developmentSectionComment;

    /**
     * @ORM\OneToMany(targetEntity="IntroductionIndex", mappedBy="application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $introductionPractices;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $introductionPracticeComment;

    public function __construct()
    {
        parent::__construct();
        $this->preOrganizationResources();
    }

    /**
     * Set projectPurpose
     *
     * @param string $projectPurpose
     *
     * @return Application
     */
    public function setProjectPurpose($projectPurpose)
    {
        $this->projectPurpose = $projectPurpose;

        return $this;
    }

    /**
     * Get projectPurpose
     *
     * @return string
     */
    public function getProjectPurpose()
    {
        return $this->projectPurpose;
    }

    /**
     * Set tasksHeadTarget
     *
     * @param string $tasksHeadTarget
     *
     * @return Application
     */
    public function setTasksHeadTarget($tasksHeadTarget)
    {
        $this->tasksHeadTarget = $tasksHeadTarget;

        return $this;
    }

    /**
     * Get tasksHeadTarget
     *
     * @return string
     */
    public function getTasksHeadTarget()
    {
        return $this->tasksHeadTarget;
    }

    /**
     * Set motivationSpreadPractice
     *
     * @param string $motivationSpreadPractice
     *
     * @return Application
     */
    public function setMotivationSpreadPractice($motivationSpreadPractice)
    {
        $this->motivationSpreadPractice = $motivationSpreadPractice;

        return $this;
    }

    /**
     * Get motivationSpreadPractice
     *
     * @return string
     */
    public function getMotivationSpreadPractice()
    {
        return $this->motivationSpreadPractice;
    }

    /**
     * Set experienceSpread
     *
     * @param string $experienceSpread
     *
     * @return Application
     */
    public function setExperienceSpread($experienceSpread)
    {
        $this->experienceSpread = $experienceSpread;

        return $this;
    }

    /**
     * Get experienceSpread
     *
     * @return string
     */
    public function getExperienceSpread()
    {
        return $this->experienceSpread;
    }

    /**
     * Set technologySpread
     *
     * @param string $technologySpread
     *
     * @return Application
     */
    public function setTechnologySpread($technologySpread)
    {
        $this->technologySpread = $technologySpread;

        return $this;
    }

    /**
     * Get technologySpread
     *
     * @return string
     */
    public function getTechnologySpread()
    {
        return $this->technologySpread;
    }

    /**
     * Set introductionProjectPurpose
     *
     * @param string $introductionProjectPurpose
     *
     * @return Application
     */
    public function setIntroductionProjectPurpose($introductionProjectPurpose)
    {
        $this->introductionProjectPurpose = $introductionProjectPurpose;

        return $this;
    }

    /**
     * Get introductionProjectPurpose
     *
     * @return string
     */
    public function getIntroductionProjectPurpose()
    {
        return $this->introductionProjectPurpose;
    }

    /**
     * Set taskOfTarget
     *
     * @param string $taskOfTarget
     *
     * @return Application
     */
    public function setTaskOfTarget($taskOfTarget)
    {
        $this->taskOfTarget = $taskOfTarget;

        return $this;
    }

    /**
     * Get taskOfTarget
     *
     * @return string
     */
    public function getTaskOfTarget()
    {
        return $this->taskOfTarget;
    }

    /**
     * Set wishDevelopPractice
     *
     * @param string $wishDevelopPractice
     *
     * @return Application
     */
    public function setWishDevelopPractice($wishDevelopPractice)
    {
        $this->wishDevelopPractice = $wishDevelopPractice;

        return $this;
    }

    /**
     * Get wishDevelopPractice
     *
     * @return string
     */
    public function getWishDevelopPractice()
    {
        return $this->wishDevelopPractice;
    }

    /**
     * Set monitoringProjectPurpose
     *
     * @param string $monitoringProjectPurpose
     *
     * @return Application
     */
    public function setMonitoringProjectPurpose($monitoringProjectPurpose)
    {
        $this->monitoringProjectPurpose = $monitoringProjectPurpose;

        return $this;
    }

    /**
     * Get monitoringProjectPurpose
     *
     * @return string
     */
    public function getMonitoringProjectPurpose()
    {
        return $this->monitoringProjectPurpose;
    }

    /**
     * Add realizationPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication
     *
     * @return Application
     */
    public function addRealizationPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication)
    {
        $this->realizationPracticeApplications[] = $realizationPracticeApplication;
        $realizationPracticeApplication->setRealizationPracticeApplication($this);
        return $this;
    }

    /**
     * Remove realizationPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication
     */
    public function removeRealizationPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication)
    {
        $this->realizationPracticeApplications->removeElement($realizationPracticeApplication);
    }

    /**
     * Get realizationPracticeApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealizationPracticeApplications()
    {
        return $this->realizationPracticeApplications;
    }

    /**
     * Add spreadPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication
     *
     * @return Application
     */
    public function addSpreadPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication)
    {
        $this->spreadPracticeApplications[] = $spreadPracticeApplication;
        $spreadPracticeApplication->setSpreadPracticeApplication($this);
        return $this;
    }

    /**
     * Remove spreadPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication
     */
    public function removeSpreadPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication)
    {
        $this->spreadPracticeApplications->removeElement($spreadPracticeApplication);
    }

    /**
     * Get spreadPracticeApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpreadPracticeApplications()
    {
        return $this->spreadPracticeApplications;
    }

    /**
     * Add risePotentialApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication
     *
     * @return Application
     */
    public function addRisePotentialApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication)
    {
        $this->risePotentialApplications[] = $risePotentialApplication;
        $risePotentialApplication->setRisePotentialApplication($this);
        return $this;
    }

    /**
     * Remove risePotentialApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication
     */
    public function removeRisePotentialApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication)
    {
        $this->risePotentialApplications->removeElement($risePotentialApplication);
    }

    /**
     * Get risePotentialApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRisePotentialApplications()
    {
        return $this->risePotentialApplications;
    }

    /**
     * Add realizationPractice
     *
     * @param \NKO\OrderBundle\Entity\Risk $realizationPractice
     *
     * @return Application
     */
    public function addRealizationPractice(\NKO\OrderBundle\Entity\Risk $realizationPractice)
    {
        $this->realizationPractices[] = $realizationPractice;
        $realizationPractice -> setConditionRealizationApplication($this);
        return $this;
    }

    /**
     * Remove realizationPractice
     *
     * @param \NKO\OrderBundle\Entity\Risk $realizationPractice
     */
    public function removeRealizationPractice(\NKO\OrderBundle\Entity\Risk $realizationPractice)
    {
        $this->realizationPractices->removeElement($realizationPractice);
    }

    /**
     * Get realizationPractices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealizationPractices()
    {
        return $this->realizationPractices;
    }

    /**
     * Add spreadPractice
     *
     * @param \NKO\OrderBundle\Entity\Risk $spreadPractice
     *
     * @return Application
     */
    public function addSpreadPractice(\NKO\OrderBundle\Entity\Risk $spreadPractice)
    {
        $this->spreadPractices[] = $spreadPractice;
        $spreadPractice -> setConditionSpreadApplication($this);
        return $this;
    }

    /**
     * Remove spreadPractice
     *
     * @param \NKO\OrderBundle\Entity\Risk $spreadPractice
     */
    public function removeSpreadPractice(\NKO\OrderBundle\Entity\Risk $spreadPractice)
    {
        $this->spreadPractices->removeElement($spreadPractice);
    }

    /**
     * Get spreadPractices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpreadPractices()
    {
        return $this->spreadPractices;
    }

    /**
     * Add projectSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialResult
     *
     * @return Application
     */
    public function addProjectSocialResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialResult)
    {
        $projectSocialResult->setProjectSocialResultApplication($this);
        $this->projectSocialResults[] = $projectSocialResult;

        return $this;
    }

    /**
     * Set authorityHead
     *
     * @param string $authorityHead
     *
     * @return Application
     */
    public function setAuthorityHead($authorityHead)
    {
        if (!$this->_authorityHead && is_string($this->authorityHead)) {
            $this->_authorityHead = $this->authorityHead;
        }
        $this->authorityHead = $authorityHead;

        return $this;
    }

    /**
     * Remove projectSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialResult
     */
    public function removeProjectSocialResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialResult)
    {
        $this->projectSocialResults->removeElement($projectSocialResult);
    }

    /**
     * Get projectSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectSocialResults()
    {
        return $this->projectSocialResults;
    }

    /**
     * Add projectDirectResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectResult
     *
     * @return Application
     */
    public function addProjectDirectResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectResult)
    {
        $projectDirectResult->setProjectDirectResultApplication($this);
        $this->projectDirectResults[] = $projectDirectResult;

        return $this;
    }

    /**
     * Remove projectDirectResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectResult
     */
    public function removeProjectDirectResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectResult)
    {
        $this->projectDirectResults->removeElement($projectDirectResult);
    }

    /**
     * Get projectDirectResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectDirectResults()
    {
        return $this->projectDirectResults;
    }

    /**
     * Add projectSocialIndividualResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialIndividualResult
     *
     * @return Application
     */
    public function addProjectSocialIndividualResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialIndividualResult)
    {
        $projectSocialIndividualResult->setProjectSocialIndividualResultApplication($this);
        $this->projectSocialIndividualResults[] = $projectSocialIndividualResult;

        return $this;
    }

    /**
     * Remove projectSocialIndividualResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialIndividualResult
     */
    public function removeProjectSocialIndividualResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectSocialIndividualResult)
    {
        $this->projectSocialIndividualResults->removeElement($projectSocialIndividualResult);
    }

    /**
     * Get projectSocialIndividualResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectSocialIndividualResults()
    {
        return $this->projectSocialIndividualResults;
    }

    /**
     * Add projectDirectIndividualResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectIndividualResult
     *
     * @return Application
     */
    public function addProjectDirectIndividualResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectIndividualResult)
    {
        $projectDirectIndividualResult->setProjectDirectIndividualResultApplication($this);
        $this->projectDirectIndividualResults[] = $projectDirectIndividualResult;

        return $this;
    }

    /**
     * Set descriptionSectionComment
     *
     * @param string $descriptionSectionComment
     *
     * @return Application
     */
    public function setDescriptionSectionComment($descriptionSectionComment)
    {
        $this->descriptionSectionComment = $descriptionSectionComment;
        return $this;
    }

    /**
     * Get authorityHead
     *
     * @return string
     */
    public function getAuthorityHead()
    {
        return $this->authorityHead;
    }

    /**
     * @Assert\Callback(
     *     groups={"Farvater-2018"}
     *     )
     */
    public function isValidAuthorityHead(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getAuthorityHead() && !$this->_authorityHead) {
            $context->buildViolation('please, upload regulation')
                ->atPath('authorityHead')
                ->addViolation();
        }
    }

     /**
     * Get descriptionSectionComment
     *
     * @return string
     */
    public function getDescriptionSectionComment()
    {
        return $this->descriptionSectionComment;
    }

    /**
     * Set spreadSectionComment
     *
     * @param string $spreadSectionComment
     *
     * @return Application
     */
    public function setSpreadSectionComment($spreadSectionComment)
    {
        $this->spreadSectionComment = $spreadSectionComment;

        return $this;
    }

    /**
     * Get spreadSectionComment
     *
     * @return string
     */
    public function getSpreadSectionComment()
    {
        return $this->spreadSectionComment;
    }

    /**
     * Set developmentSectionComment
     *
     * @param string $developmentSectionComment
     *
     * @return Application
     */
    public function setDevelopmentSectionComment($developmentSectionComment)
    {
        $this->developmentSectionComment = $developmentSectionComment;

        return $this;
    }

    /**
     * Get developmentSectionComment
     *
     * @return string
     */
    public function getDevelopmentSectionComment()
    {
        return $this->developmentSectionComment;
    }

    /**
     * Add introductionPractice
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\IntroductionIndex $introductionPractice
     *
     * @return Application
     */
    public function addIntroductionPractice(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\IntroductionIndex $introductionPractice)
    {
        $this->introductionPractices[] = $introductionPractice;
        $introductionPractice->setApplication($this);

        return $this;
    }

    /**
     * Remove projectDirectIndividualResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectIndividualResult
     */
    public function removeProjectDirectIndividualResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $projectDirectIndividualResult)
    {
        $this->projectDirectIndividualResults->removeElement($projectDirectIndividualResult);
    }

    /**
     * Get projectDirectIndividualResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectDirectIndividualResults()
    {
        return $this->projectDirectIndividualResults;
    }

    /**
     * Remove introductionPractice
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\IntroductionIndex $introductionPractice
     */
    public function removeIntroductionPractice(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\IntroductionIndex $introductionPractice)
    {
        $this->introductionPractices->removeElement($introductionPractice);
    }

    /**
     * Get introductionPractices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntroductionPractices()
    {
        return $this->introductionPractices;
    }

    /**
     * Add practiceSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceSocialResult
     *
     * @return Application
     */
    public function addPracticeSocialResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceSocialResult)
    {
        $practiceSocialResult->setPracticeSocialResultApplication($this);
        $this->practiceSocialResults[] = $practiceSocialResult;

        return $this;
    }

    /**
     * Remove practiceSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceSocialResult
     */
    public function removePracticeSocialResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceSocialResult)
    {
        $this->practiceSocialResults->removeElement($practiceSocialResult);
    }

    /**
     * Get practiceSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSocialResults()
    {
        return $this->practiceSocialResults;
    }

    /**
     * Add practiceDirectResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectResult
     *
     * @return Application
     */
    public function addPracticeDirectResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectResult)
    {
        $practiceDirectResult->setPracticeDirectResultApplication($this);
        $this->practiceDirectResults[] = $practiceDirectResult;

        return $this;
    }

    /**
     * Remove practiceDirectResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectResult
     */
    public function removePracticeDirectResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectResult)
    {
        $this->practiceDirectResults->removeElement($practiceDirectResult);
    }

    /**
     * Get practiceDirectResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeDirectResults()
    {
        return $this->practiceDirectResults;
    }

    /**
     * Add practiceDirectIndividualResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectIndividualResult
     *
     * @return Application
     */
    public function addPracticeDirectIndividualResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectIndividualResult)
    {
        $practiceDirectIndividualResult->setPracticeDirectIndividualResultApplication($this);
        $this->practiceDirectIndividualResults[] = $practiceDirectIndividualResult;

        return $this;
    }

    /**
     * Remove practiceDirectIndividualResult
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectIndividualResult
     */
    public function removePracticeDirectIndividualResult(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result $practiceDirectIndividualResult)
    {
        $this->practiceDirectIndividualResults->removeElement($practiceDirectIndividualResult);
    }

    /**
     * Get practiceDirectIndividualResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeDirectIndividualResults()
    {
        return $this->practiceDirectIndividualResults;
    }

    /**
     * Set projectSocialComment
     *
     * @param string $projectSocialComment
     *
     * @return Application
     */
    public function setProjectSocialComment($projectSocialComment)
    {
        $this->projectSocialComment = $projectSocialComment;

        return $this;
    }

    /**
     * Get projectSocialComment
     *
     * @return string
     */
    public function getProjectSocialComment()
    {
        return $this->projectSocialComment;
    }

    /**
     * Set projectDirectComment
     *
     * @param string $projectDirectComment
     *
     * @return Application
     */
    public function setProjectDirectComment($projectDirectComment)
    {
        $this->projectDirectComment = $projectDirectComment;

        return $this;
    }

    /**
     * Get projectDirectComment
     *
     * @return string
     */
    public function getProjectDirectComment()
    {
        return $this->projectDirectComment;
    }

    /**
     * Set practiceSocialComment
     *
     * @param string $practiceSocialComment
     *
     * @return Application
     */
    public function setPracticeSocialComment($practiceSocialComment)
    {
        $this->practiceSocialComment = $practiceSocialComment;

        return $this;
    }

    /**
     * Get practiceSocialComment
     *
     * @return string
     */
    public function getPracticeSocialComment()
    {
        return $this->practiceSocialComment;
    }

    /**
     * Set practiceDirectComment
     *
     * @param string $practiceDirectComment
     *
     * @return Application
     */
    public function setPracticeDirectComment($practiceDirectComment)
    {
        $this->practiceDirectComment = $practiceDirectComment;

        return $this;
    }

    /**
     * Get practiceDirectComment
     *
     * @return string
     */
    public function getPracticeDirectComment()
    {
        return $this->practiceDirectComment;
    }

    /**
     * Set introductionPracticeComment
     *
     * @param string $introductionPracticeComment
     *
     * @return Application
     */
    public function setIntroductionPracticeComment($introductionPracticeComment)
    {
        $this->introductionPracticeComment = $introductionPracticeComment;

        return $this;
    }

    /**
     * Get introductionPracticeComment
     *
     * @return string
     */
    public function getIntroductionPracticeComment()
    {
        return $this->introductionPracticeComment;
    }
}
