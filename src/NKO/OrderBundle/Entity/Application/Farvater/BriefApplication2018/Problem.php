<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/12/18
 * Time: 12:56 PM
 */

namespace NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="brief_application_2018_problem")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\Application\Farvater\BriefApplication2018\ProblemRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Problem
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "ContinuationApplicationKNS", "KNS2018-3"
     *     }
     * )
     * @ORM\Column(name="target_group", type="text", nullable=true)
     */
    private $targetGroup;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "ContinuationApplication-2018", "Farvater-2018", "KNS2017-2", "ContinuationApplication-2",
     *         "ContinuationApplicationKNS", "ContinuationSF18-2019", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\SocialResult")
     * @ORM\JoinTable(name="problems_social_results",
     *      joinColumns={@ORM\JoinColumn(name="problem_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="social_result_id", referencedColumnName="id")}
     *      )
     */
    private $socialResults;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_social_result", type="text", nullable=true)
     */
    private $customSocialResult;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="specialist_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $specialistApplication;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\SpecialistCompetition")
     * @ORM\JoinTable(name="application_specialist_competition",
     *      joinColumns={@ORM\JoinColumn(name="problem_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="specialist_competition_id", referencedColumnName="id")}
     *      )
     */
    private $specialistCompetitions;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="problem",
     *     cascade={"persist"})
     */
    protected $directResults;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __construct()
    {
        $this->socialResults = new ArrayCollection();
        $this->specialistCompetitions = new ArrayCollection();
        $this->directResults = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->targetGroup;
    }

    /**
     * @Assert\Callback(groups={
     *     "KNS2017-2", "ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019", "KNS2019-2"
     * })
     */
    public function isValidSocialResults(ExecutionContextInterface $context, $payload)
    {

        if ($this->socialResults->isEmpty()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('socialResults')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={
     *     "KNS2018-3"
     * })
     */
    public function isValidSocialResultsWithCustom(ExecutionContextInterface $context, $payload)
    {
        if ($this->socialResults->isEmpty() || !$this->getCustomSocialResult()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('socialResults')
                ->addViolation();
        }
    }

    public function getHumanizedSocialResults()
    {
        return $this->getHumanizedCollections($this->getSocialResults());
    }

    public function getHumanizedSpecialistCompetitions()
    {
        return $this->getHumanizedCollections($this->getSpecialistCompetitions());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Problem
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Problem
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set customSocialResult
     *
     * @param string $customSocialResult
     *
     * @return Problem
     */
    public function setCustomSocialResult($customSocialResult)
    {
        $this->customSocialResult = $customSocialResult;

        return $this;
    }

    /**
     * Get customSocialResult
     *
     * @return string
     */
    public function getCustomSocialResult()
    {
        return $this->customSocialResult;
    }

    /**
     * Set application
     *
     * @param BaseApplication $application
     *
     * @return Problem
     */
    public function setApplication(BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return BaseApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set targetGroup
     *
     * @param string $targetGroup
     *
     * @return Problem
     */
    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return string
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    /**
     * Add socialResult
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult
     *
     * @return Problem
     */
    public function addSocialResult(\NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult)
    {
        $this->socialResults[] = $socialResult;

        return $this;
    }

    /**
     * Remove socialResult
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult
     */
    public function removeSocialResult(\NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult)
    {
        $this->socialResults->removeElement($socialResult);
    }

    /**
     * Get socialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResults()
    {
        return $this->socialResults;
    }

    /**
     * Set specialistApplication
     *
     * @param BaseApplication $specialistApplication
     *
     * @return Problem
     */
    public function setSpecialistApplication(BaseApplication $specialistApplication = null)
    {
        $this->specialistApplication = $specialistApplication;

        return $this;
    }

    /**
     * Get specialistApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getSpecialistApplication()
    {
        return $this->specialistApplication;
    }



    /**
     * Add specialistCompetition
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\SpecialistCompetition $specialistCompetition
     *
     * @return Problem
     */
    public function addSpecialistCompetition(\NKO\OrderBundle\Entity\Application\Continuation\SpecialistCompetition $specialistCompetition)
    {
        $this->specialistCompetitions[] = $specialistCompetition;

        return $this;
    }

    /**
     * Remove specialistCompetition
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\SpecialistCompetition $specialistCompetition
     */
    public function removeSpecialistCompetition(\NKO\OrderBundle\Entity\Application\Continuation\SpecialistCompetition $specialistCompetition)
    {
        $this->specialistCompetitions->removeElement($specialistCompetition);
    }

    /**
     * Get specialistCompetitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialistCompetitions()
    {
        return $this->specialistCompetitions;
    }

    /**
     * @Assert\Callback(
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     */
    public function isValidSpecialistCompetitions(ExecutionContextInterface $context, $payload)
    {
        if ($this->getSpecialistCompetitions()->isEmpty() && !$this->getApplication()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('specialistCompetitions')
                ->addViolation();
        }
    }


    /**
     * Add directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     *
     * @return Problem
     */
    public function addDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $directResult->setProblem($this);
        $this->directResults[] = $directResult;

        return $this;
    }

    /**
     * Remove directResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult
     */
    public function removeDirectResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $directResult)
    {
        $this->directResults->removeElement($directResult);
    }

    /**
     * Get directResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectResults()
    {
        return $this->directResults;
    }
}
