<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/18/18
 * Time: 12:12 PM
 */

namespace NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="brief_application_2018_financing_source")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class FinancingSource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2018"}
     *     )
     *
     * @Assert\LessThanOrEqual(
     *     value = 100,
     *     groups={"BriefApplication-2018"}
     * )
     *
     * @Assert\GreaterThanOrEqual(
     *     value = 0,
     *     groups={"BriefApplication-2018"}
     * )
     *
     * @Assert\Regex("/^\d+(\.\d{1,2})?$/",
     *
     *     message="2 number after digital point")
     *
     * @ORM\Column(name="sum", type="decimal", scale=2, nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application", inversedBy="financingSources")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return FinancingSource
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set type
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType $type
     *
     * @return FinancingSource
     */
    public function setType(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application $application
     *
     * @return FinancingSource
     */
    public function setApplication(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FinancingSource
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
