<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/12/18
 * Time: 5:14 PM
 */

namespace NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\HumanizedProperties;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="brief_application_2018_effectiveness")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Effectiveness
{
    use HumanizedProperties;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    protected $result;

     /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator", fetch="EAGER")
     * @ORM\JoinColumn(name="indicator_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */

    protected $indicator;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(name="custom_indicator", type="text", nullable=true)
     */
    protected $customIndicator;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2018"}
     *     )
     *
     * @ORM\Column(name="indicator_value", type="integer", nullable=true)
     */
    protected $indicatorValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS2017-2", "ContinuationApplication-2", "ContinuationSF18-2019", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="fact_value", type="integer", nullable=true)
     */
    protected $factValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS2017-2", "ContinuationApplication-2", "ContinuationSF18-2019", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="plan_value", type="integer", nullable=true)
     */
    protected $planValue;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod", inversedBy="effectivenessItems")
     * @ORM\JoinColumn(name="measurement_method_id", referencedColumnName="id",  onDelete="SET NULL")
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS2018-2", "KNS2019-2"}
     * )
     */
    protected $measurementMethod;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinTable(name="effectiveness_measurement_method_relationship",
     *      joinColumns={@ORM\JoinColumn(name="effectiveness_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="method_id", referencedColumnName="id")}
     *      )
     */
    protected $methods;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS2017-2", "KNS2019-2"}
     *     )
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="effectiveness_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $effectivenessApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication")
     * @ORM\JoinColumn(name="effectiveness_etc_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $effectivenessEtcApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="effectivenessDisseminationItems")
     * @ORM\JoinColumn(name="effectiveness_diss_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $effectivenessDisseminationApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", inversedBy="effectivenessDisseminationEtcItems")
     * @ORM\JoinColumn(name="effectiveness_diss_etc_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $effectivenessDisseminationEtcApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Assert\Callback(groups={"BriefApplication-2018", "ContinuationApplication-2018"})
     */
    public function isValidCustomIndicator(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getIndicator() && !$this->getCustomIndicator()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('customIndicator')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"BriefApplication-2018"})
     */
    public function isValidResult(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getResult() && ($this->getIndicatorValue() > 0)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('result')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018"})
     */
    public function isValidEffectivenessResult(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getResult() && ($this->getFactValue() > 0 || $this->getPlanValue() > 0)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('result')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"BriefApplication-2018", "ContinuationApplication-2018"})
     */
    public function isValidCustomResult(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getIndicator() && !$this->getCustomIndicator()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('result')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"", "ContinuationApplication-2018"})
     */
    public function isValidMethods(ExecutionContextInterface $context, $payload)
    {
        if ($this->getMethods()->isEmpty() && ($this->getFactValue() > 0 || $this->getPlanValue() > 0)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('methods')
                ->addViolation();
        } elseif ($this->getResult() && $this->getMethods()->isEmpty()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('methods')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018"})
     */
    public function isValidFactValue(ExecutionContextInterface $context, $payload)
    {

        if ($this->getResult() && ($this->getFactValue()=== null)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('factValue')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018"})
     */
    public function isValidPlanValue(ExecutionContextInterface $context, $payload)
    {
        if ($this->getResult() && ($this->getPlanValue() === null)) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('planValue')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidMethod(ExecutionContextInterface $context, $payload)
    {

        if ($this->getMethods()->isEmpty()) {
            $context->buildViolation('value is invalid(field must be non empty)')
                ->atPath('methods')
                ->addViolation();
        }
    }

    public function getHumanizedMethods()
    {
        return $this->getHumanizedCollections($this->getMethods());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Effectiveness
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicatorValue
     *
     * @param integer $indicatorValue
     *
     * @return Effectiveness
     */
    public function setIndicatorValue($indicatorValue)
    {
        $this->indicatorValue = $indicatorValue;

        return $this;
    }

    /**
     * Get indicatorValue
     *
     */
    public function getIndicatorValue()
    {
        return $this->indicatorValue;
    }

    /**
     * Set effectivenessApplication
     *
     * @param BaseApplication $effectivenessApplication
     *
     * @return Effectiveness
     */
    public function setEffectivenessApplication(BaseApplication $effectivenessApplication = null)
    {
        $this->effectivenessApplication = $effectivenessApplication;

        return $this;
    }

    /**
     * Get effectivenessApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application
     */
    public function getEffectivenessApplication()
    {
        return $this->effectivenessApplication;
    }

    /**
     * Set effectivenessEtcApplication
     *
     * @param BaseApplication $effectivenessEtcApplication
     *
     * @return Effectiveness
     */
    public function setEffectivenessEtcApplication(BaseApplication $effectivenessEtcApplication = null)
    {
        $this->effectivenessEtcApplication = $effectivenessEtcApplication;

        return $this;
    }

    /**
     * Get effectivenessEtcApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application
     */
    public function getEffectivenessEtcApplication()
    {
        return $this->effectivenessEtcApplication;
    }

    /**
     * Set indicator
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator
     *
     * @return Effectiveness
     */
    public function setIndicator(\NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator $indicator = null)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return \NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set customIndicator
     *
     * @param string $customIndicator
     *
     * @return Effectiveness
     */
    public function setCustomIndicator($customIndicator)
    {
        $this->customIndicator = $customIndicator;

        return $this;
    }

    /**
     * Get customIndicator
     *
     * @return string
     */
    public function getCustomIndicator()
    {
        return $this->customIndicator;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Effectiveness
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set effectivenessDisseminationApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $effectivenessDisseminationApplication
     *
     * @return Effectiveness
     */
    public function setEffectivenessDisseminationApplication(\NKO\OrderBundle\Entity\BaseApplication $effectivenessDisseminationApplication = null)
    {
        $this->effectivenessDisseminationApplication = $effectivenessDisseminationApplication;

        return $this;
    }

    /**
     * Get effectivenessDisseminationApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getEffectivenessDisseminationApplication()
    {
        return $this->effectivenessDisseminationApplication;
    }

    /**
     * Set effectivenessDisseminationEtcApplication
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $effectivenessDisseminationEtcApplication
     *
     * @return Effectiveness
     */
    public function setEffectivenessDisseminationEtcApplication(\NKO\OrderBundle\Entity\BaseApplication $effectivenessDisseminationEtcApplication = null)
    {
        $this->effectivenessDisseminationEtcApplication = $effectivenessDisseminationEtcApplication;

        return $this;
    }

    /**
     * Get effectivenessDisseminationEtcApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Continuation\Application
     */
    public function getEffectivenessDisseminationEtcApplication()
    {
        return $this->effectivenessDisseminationEtcApplication;
    }

    /**
     * Set factValue
     *
     * @param integer $factValue
     *
     * @return Effectiveness
     */
    public function setFactValue($factValue)
    {
        $this->factValue = $factValue;

        return $this;
    }

    /**
     * Get factValue
     *
     * @return integer
     */
    public function getFactValue()
    {
        return $this->factValue;
    }

    /**
     * Set planValue
     *
     * @param integer $planValue
     *
     * @return Effectiveness
     */
    public function setPlanValue($planValue)
    {
        $this->planValue = $planValue;

        return $this;
    }

    /**
     * Get planValue
     *
     * @return integer
     */
    public function getPlanValue()
    {
        return $this->planValue;
    }

    /**
     * Set measurementMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod
     *
     * @return Effectiveness
     */
    public function setMeasurementMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $measurementMethod = null)
    {
        $this->measurementMethod = $measurementMethod;

        return $this;
    }

    /**
     * Get measurementMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getMeasurementMethod()
    {
        return $this->measurementMethod;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Effectiveness
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->methods = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add method
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method
     *
     * @return Effectiveness
     */
    public function addMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods[] = $method;

        return $this;
    }

    /**
     * Remove method
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method
     */
    public function removeMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $method)
    {
        $this->methods->removeElement($method);
    }

    /**
     * Get methods
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMethods()
    {
        return $this->methods;
    }
}
