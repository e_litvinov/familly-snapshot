<?php

namespace NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Table(name="brief_application_2018")
 * @ORM\Entity()
 */
class Application extends BaseApplication
{
    use PracticeDescriptionTrait;
    use OrganizationInfoTrait;

    const NAME_APPLICATION = 'Краткая заявка СФ-2018';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Valid
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="brief_application_2018_people_category_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    protected $peopleCategories;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="brief_application_2018_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    protected $practiceRegions;

    /**
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $beneficiaryProblems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessEtcItems;

    /**
     *
     * @Assert\NotBlank(
     * message="value is invalid(field must be non empty)",
     * groups={"BriefApplication-2018"}
     * )
     *
     * @ORM\Column(name="effectiveness_fact", type="text", nullable=true)
     */
    protected $effectivenessFact;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2018"}
     *     )
     *
     * @ORM\Column(name="effectiveness_plan", type="text", nullable=true)
     */
    protected $effectivenessPlan;

    /**
     * @var bool
     *
     *
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    protected $isOrganizationHeadEqualProjectHead;

    /**
     *
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\DropDownEntity")
     * @ORM\JoinTable(name="brief_application_2018_is_ready_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="drop_down_list_id", referencedColumnName="id")}
     *      )
     */
    private $isReady;

    /**
     * @ORM\Column(name="advantages", type="text", nullable=true)
     *
     * @Assert\NotBlank(
     * message="value is invalid(field must be non empty)",
     * groups={"KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018"}
     * )
     */
    protected $advantages;

    /**
     * @Assert\NotBlank(
     * message="value is invalid(field must be non empty)",
     * groups={"KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018"}
     * )
     *
     * @ORM\Column(name="disadvantages", type="text", nullable=true)
     */
    protected $disadvantages;

    /**
     * @Assert\NotBlank(
     * message="value is invalid(field must be non empty)",
     * groups={"KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018"}
     * )
     *
     * @ORM\Column(name="practice_development", type="text", nullable=true)
     */
    protected $practiceDevelopment;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"KNS-2017", "KNS-2016", "Farvater-2017", "BriefApplication-2018"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found"
     *     )
     * @ORM\Column(name="signed_agreement", type="string", length=255, nullable=true)
     */
    protected $signedAgreement;

    protected $_signedAgreement;

    /**
     * @Assert\NotBlank(
     * message="value is invalid(field must be non empty)",
     * groups={"BriefApplication-2018"}
     * )
     *
     * @ORM\Column(name="is_approved", type="boolean", nullable=true)
     */
    protected $isApproved;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Publication", mappedBy="briefApplication2018",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $publications;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2018"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm", inversedBy="applications")
     * @ORM\JoinColumn(name="organizationForm_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $organizationForm;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSource", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $financingSources;

    /**
     *     @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2018"}
     *     )
     *
     * @ORM\Column(name="another_financing_source", type="text", nullable=true)
     */
    protected $anotherFinancingSource;

    /**
     * @ORM\Column(name="is_ready_etc", type="text", nullable=true)
     */
    protected $isReadyEtc;

    public function __construct()
    {
        parent::__construct();
        $this->isApproved = true;
        $this->peopleCategories = new ArrayCollection();
        $this->practiceRegions = new ArrayCollection();
        $this->effectivenessItems = new ArrayCollection();
        $this->effectivenessEtcItems = new ArrayCollection();
        $this->siteLinks = new ArrayCollection();
        $this->socialNetworkLinks = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->isAddressesEqual = true;
        $this->publications = new ArrayCollection();
        $this->financingSources = new ArrayCollection();
        $this->isReady = new ArrayCollection();
    }

    public function __toString()
    {
        return self::NAME_APPLICATION;
    }

    /**
     * @Assert\Callback(groups={"BriefApplication-2018"})
     */
    public function isValidIsApproved(ExecutionContextInterface $context, $payload)
    {
        if (!$this->isApproved) {
            $context->buildViolation('please, confirm if correct data')
                ->atPath('isApproved')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"BriefApplication-2018"})
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPeopleCategories();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose people categories')
                ->atPath('peopleCategories')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"BriefApplication-2018"})
     */
    public function isValidIsReady(ExecutionContextInterface $context, $payload)
    {
        $readyItems = $this->getIsReady();
        if ($readyItems->isEmpty()) {
            $context->buildViolation('please, choose anything')
                ->atPath('isReady')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"BriefApplication-2018"})
     */
    public function isValidSignedAgreement(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getSignedAgreement() && !$this->_signedAgreement) {
            $context->buildViolation('please, upload signed agreement')
                ->atPath('signedAgreement')
                ->addViolation();
        }
    }

    /**
     * Set effectivenessFact
     *
     * @param string $effectivenessFact
     *
     * @return Application
     */
    public function setEffectivenessFact($effectivenessFact)
    {
        $this->effectivenessFact = $effectivenessFact;

        return $this;
    }

    /**
     * Get effectivenessFact
     *
     * @return string
     */
    public function getEffectivenessFact()
    {
        return $this->effectivenessFact;
    }

    /**
     * Set effectivenessPlan
     *
     * @param string $effectivenessPlan
     *
     * @return Application
     */
    public function setEffectivenessPlan($effectivenessPlan)
    {
        $this->effectivenessPlan = $effectivenessPlan;

        return $this;
    }

    /**
     * Get effectivenessPlan
     *
     * @return string
     */
    public function getEffectivenessPlan()
    {
        return $this->effectivenessPlan;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return Application
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;

        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Add practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     *
     * @return Application
     */
    public function addPracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions[] = $practiceRegion;

        return $this;
    }

    /**
     * Remove practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     */
    public function removePracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions->removeElement($practiceRegion);
    }

    /**
     * Get practiceRegions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeRegions()
    {
        return $this->practiceRegions;
    }

    /**
     * Add beneficiaryProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem
     *
     * @return Application
     */
    public function addBeneficiaryProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem)
    {
        $beneficiaryProblem->setApplication($this);
        $this->beneficiaryProblems[] = $beneficiaryProblem;

        return $this;
    }

    /**
     * Remove beneficiaryProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem
     */
    public function removeBeneficiaryProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem)
    {
        $this->beneficiaryProblems->removeElement($beneficiaryProblem);
    }

    /**
     * Get beneficiaryProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryProblems()
    {
        return $this->beneficiaryProblems;
    }

    /**
     * Add effectivenessItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem
     *
     * @return Application
     */
    public function addEffectivenessItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem)
    {
        $this->effectivenessItems[] = $effectivenessItem;
        $effectivenessItem->setEffectivenessApplication($this);

        return $this;
    }

    /**
     * Remove effectivenessItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem
     */
    public function removeEffectivenessItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem)
    {
        $this->effectivenessItems->removeElement($effectivenessItem);
    }

    /**
     * Get effectivenessItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessItems()
    {
        return $this->effectivenessItems;
    }

    /**
     * Add effectivenessEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessEtcItem
     *
     * @return Application
     */
    public function addEffectivenessEtcItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessEtcItem)
    {
        $effectivenessEtcItem->setEffectivenessEtcApplication($this);

        return $this;
    }

    /**
     * Remove effectivenessEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessEtcItem
     */
    public function removeEffectivenessEtcItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessEtcItem)
    {
        $this->effectivenessEtcItems->removeElement($effectivenessEtcItem);
    }


    /**
     * Set isOrganizationHeadEqualProjectHead
     *
     * @param boolean $isOrganizationHeadEqualProjectHead
     *
     * @return Application
     */
    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    /**
     * Get isOrganizationHeadEqualProjectHead
     *
     * @return boolean
     */
    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    /**
     * Get effectivenessEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessEtcItems()
    {
        return $this->effectivenessEtcItems;
    }

    /**
     * Set advantages
     *
     * @param string $advantages
     *
     * @return Application
     */
    public function setAdvantages($advantages)
    {
        $this->advantages = $advantages;

        return $this;
    }

    /**
     * Get advantages
     *
     * @return string
     */
    public function getAdvantages()
    {
        return $this->advantages;
    }

    /**
     * Set disadvantages
     *
     * @param string $disadvantages
     *
     * @return Application
     */
    public function setDisadvantages($disadvantages)
    {
        $this->disadvantages = $disadvantages;

        return $this;
    }

    /**
     * Get disadvantages
     *
     * @return string
     */
    public function getDisadvantages()
    {
        return $this->disadvantages;
    }

    /**
     * Set practiceDevelopment
     *
     * @param string $practiceDevelopment
     *
     * @return Application
     */
    public function setPracticeDevelopment($practiceDevelopment)
    {
        $this->practiceDevelopment = $practiceDevelopment;

        return $this;
    }

    /**
     * Get practiceDevelopment
     *
     * @return string
     */
    public function getPracticeDevelopment()
    {
        return $this->practiceDevelopment;
    }

    /**
     * Set signedAgreement
     *
     * @param string $signedAgreement
     *
     * @return Application
     */
    public function setSignedAgreement($signedAgreement)
    {
        if (!$this->_signedAgreement && is_string($this->signedAgreement)) {
            $this->_signedAgreement = $this->signedAgreement;
        }

        $this->signedAgreement = $signedAgreement;
        return $this;
    }


    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     *
     * @return Application
     */
    public function addPublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $publication->setBriefApplication2018($this);
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Get signedAgreement
     *
     * @return string
     */
    public function getSignedAgreement()
    {
        return $this->signedAgreement;
    }

    /**
     * Set isApproved
     *
     * @param boolean $isApproved
     *
     * @return Application
     */
    public function setIsApproved($isApproved)
    {
        $this->isApproved = $isApproved;

        return $this;
    }

    /**
     * Get isApproved
     *
     * @return boolean
     */
    public function getIsApproved()
    {
        return $this->isApproved;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     */
    public function removePublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Set organizationForm
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm
     *
     * @return Application
     */
    public function setOrganizationForm(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm = null)
    {
        $this->organizationForm = $organizationForm;
    }

    /*
    * Get organizationForm
    *
    * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm
    */
    public function getOrganizationForm()
    {
        return $this->organizationForm;
    }


     /*
     * Add financingSource
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSource $financingSource
     *
     * @return Application
     */
    public function addFinancingSource(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSource $financingSource)
    {
        $financingSource->setApplication($this);
        $this->financingSources[] = $financingSource;

        return $this;
    }


    /**
     * Remove financingSource
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSource $financingSource
     */
    public function removeFinancingSource(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSource $financingSource)
    {
        $this->financingSources->removeElement($financingSource);
    }

    /**
     * Get financingSources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinancingSources()
    {
        return $this->financingSources;
    }

    /**
     * Set anotherFinancingSource
     *
     * @param string $anotherFinancingSource
     *
     * @return Application
     */
    public function setAnotherFinancingSource($anotherFinancingSource)
    {
        $this->anotherFinancingSource = $anotherFinancingSource;

        return $this;
    }

    /**
     * Get anotherFinancingSource
     *
     * @return string
     */
    public function getAnotherFinancingSource()
    {
        return $this->anotherFinancingSource;
    }

    /**
     * Add isReady
     *
     * @param \NKO\OrderBundle\Entity\DropDownEntity $isReady
     *
     * @return Application
     */
    public function addIsReady(\NKO\OrderBundle\Entity\DropDownEntity $isReady)
    {
        $this->isReady[] = $isReady;

        return $this;
    }

    /**
     * Remove isReady
     *
     * @param \NKO\OrderBundle\Entity\DropDownEntity $isReady
     */
    public function removeIsReady(\NKO\OrderBundle\Entity\DropDownEntity $isReady)
    {
        $this->isReady->removeElement($isReady);
    }

    /**
     * Get isReady
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIsReady()
    {
        return $this->isReady;
    }

    /**
     * Set isReadyEtc
     *
     * @param string $isReadyEtc
     *
     * @return Application
     */
    public function setIsReadyEtc($isReadyEtc)
    {
        $this->isReadyEtc = $isReadyEtc;

        return $this;
    }

    /**
     * Get isReadyEtc
     *
     * @return string
     */
    public function getIsReadyEtc()
    {
        return $this->isReadyEtc;
    }

     /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2018"}
     *     )
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getPracticeRegions()->isEmpty()) {
            $context->buildViolation('Wrong choose!')
                ->atPath('practiceRegions')
                ->addViolation();
        }

        if (!$this->getPriorityDirection()) {
            $context->buildViolation('Wrong choose!')
                ->atPath('priorityDirection')
                ->addViolation();
        }

        if ($this->getPeopleCategories()->isEmpty()) {
            $context->buildViolation('Wrong choose!')
                ->atPath('peopleCategories')
                ->addViolation();
        }

        if ($this->getIsReady()->isEmpty()) {
            $context->buildViolation('Wrong choose!')
                ->atPath('isReadyEtc')
                ->addViolation();
        }
    }
}
