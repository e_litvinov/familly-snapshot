<?php

namespace NKO\OrderBundle\Entity\Application\Verification\Application2020;

use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use NKO\OrderBundle\Entity\BaseApplication;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Document\FileWithFields;
use NKO\OrderBundle\Entity\Document\FileWithTitle;
use NKO\OrderBundle\Traits\AdditionalOrganizationInfoTrait;
use NKO\OrderBundle\Traits\Application\AllDocumentsTrait;
use NKO\OrderBundle\Traits\Application\Harbor\Application2019\DocumentsTrait;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Table(name="verification_application_2020")
 * @ORM\Entity()
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid DOC file",
 *     extensions={"doc"},
 *     fileNames={
 *       "subjectRegulation"
 *     },
 *     notNull=true,
 *     groups={"Verification-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "anotherHead",
 *       "regulation",
 *       "signedAgreement",
 *       "isBankAccountExist",
 *     },
 *     notNull=true,
 *     groups={"Verification-2020"}
 * )
 *
 * @CustomAssert\EntityWithFile(
 *     message="Please upload a valid PDF file",
 *     extensions={"pdf"},
 *     fileNames={
 *       "authorityHead",
 *       "organizationCreationResolution",
 *     },
 *     groups={"Verification-2020"}
 * )
 *
 **/
class Application extends BaseApplication
{
    use OrganizationInfoTrait;
    use AdditionalOrganizationInfoTrait;
    use BudgetAdditionalTrait;
    use AllDocumentsTrait;
    use DocumentsTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"Verification-2020"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm")
     * @ORM\JoinColumn(name="organizationForm_id", referencedColumnName="id", nullable=true)
     */
    private $organizationForm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $personalAccount;

    /**
     * @var bool
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    protected $isOrganizationHeadEqualProjectHead;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameAddressee;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"Verification-2020"}
     * )
     *
     * @ORM\Column(name="link_to_report_for_ministry_of_justice", type="text", nullable=true)
     */
    private $linkToReportForMinistryOfJustice;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"Verification-2020"}
     * )
     *
     * @ORM\Column(name="link_to_annual_report", type="text", nullable=true)
     */
    private $linkToAnnualReport;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Publication", mappedBy="KNS2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     */
    private $publications;

    /**
     * @Assert\Valid
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="title",
     *     validatorClass="Symfony\Component\Validator\Constraints\NotBlank"
     * )
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="file",
     *     isProperty=false,
     *     validatorClass="NKO\OrderBundle\Validator\Constraints\EntityWithFile",
     *     validatorOptions={
     *         "extensions"={"pdf", "excel", "doc"},
     *         "notNull"=true,
     *         "fileNames"={
     *           "file"
     *         },
     *     }
     * )
     *
     * @ManyToMany(targetEntity="NKO\OrderBundle\Entity\Document\FileWithTitle", cascade={"persist"})
     * @JoinTable(name="measure_tools_application",
     *      joinColumns={@JoinColumn(name="application", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="document", referencedColumnName="id", unique=true)}
     *      )
     */
    private $measurementTools;

    /**
     * @Assert\Valid
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="firstField",
     *     validatorClass="Symfony\Component\Validator\Constraints\NotBlank"
     * )
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="secondField",
     *     validatorClass="Symfony\Component\Validator\Constraints\NotBlank"
     * )
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="file",
     *     isProperty=false,
     *     validatorClass="NKO\OrderBundle\Validator\Constraints\EntityWithFile",
     *     validatorOptions={
     *         "extensions"={"pdf", "excel", "doc"},
     *         "notNull"=true,
     *         "fileNames"={
     *           "file"
     *         },
     *     }
     * )
     *
     * @ManyToMany(targetEntity="NKO\OrderBundle\Entity\Document\FileWithFields", cascade={"persist"})
     * @JoinTable(name="result_confirming_application",
     *      joinColumns={@JoinColumn(name="application", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="document", referencedColumnName="id", unique=true)}
     *      )
     */
    private $resultConfirmingDocuments;

    /**
     * @Assert\Valid
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="file",
     *     isProperty=false,
     *     validatorClass="NKO\OrderBundle\Validator\Constraints\EntityWithFile",
     *     validatorOptions={
     *         "extensions"={"pdf", "excel", "doc"},
     *         "fileNames"={
     *           "file"
     *         },
     *     }
     * )
     *
     * @ManyToMany(targetEntity="NKO\OrderBundle\Entity\Document\FileWithTitle", cascade={"persist"})
     * @JoinTable(name="perfomance_confirming_application",
     *      joinColumns={@JoinColumn(name="application", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="document", referencedColumnName="id", unique=true)}
     *      )
     */
    private $performanceConfirmingDocuments;

    /**
     * @Assert\Valid
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="file",
     *     isProperty=false,
     *     validatorClass="NKO\OrderBundle\Validator\Constraints\EntityWithFile",
     *     validatorOptions={
     *         "extensions"={"pdf", "excel", "doc"},
     *         "fileNames"={
     *           "file"
     *         },
     *     }
     * )
     *
     * @ManyToMany(targetEntity="NKO\OrderBundle\Entity\Document\FileWithTitle", cascade={"persist"})
     * @JoinTable(name="practice_regulation_application",
     *      joinColumns={@JoinColumn(name="application", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="document", referencedColumnName="id", unique=true)}
     *      )
     */
    private $practiceRegulationDocuments;

    /**
     * @Assert\Valid
     * @CustomAssert\CollectionDecoratorValidation(
     *     groups={"Verification-2020"},
     *     propertyName="file",
     *     isProperty=false,
     *     validatorClass="NKO\OrderBundle\Validator\Constraints\EntityWithFile",
     *     validatorOptions={
     *         "extensions"={"pdf", "excel", "doc"},
     *         "fileNames"={
     *           "file"
     *         },
     *     }
     * )
     *
     * @ManyToMany(targetEntity="NKO\OrderBundle\Entity\Document\FileWithTitle", cascade={"persist"})
     * @JoinTable(name="result_document_application",
     *      joinColumns={@JoinColumn(name="application", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="document", referencedColumnName="id", unique=true)}
     *      )
     */
    private $resultDocuments;

    public function setOrganizationForm(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm = null)
    {
        $this->organizationForm = $organizationForm;

        return $this;
    }

    public function getOrganizationForm()
    {
        return $this->organizationForm;
    }

    public function setPersonalAccount($personalAccount)
    {
        $this->personalAccount = $personalAccount;

        return $this;
    }

    public function getPersonalAccount()
    {
        return $this->personalAccount;
    }

    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    public function setNameAddressee($nameAddressee)
    {
        $this->nameAddressee = $nameAddressee;

        return $this;
    }

    public function getNameAddressee()
    {
        return $this->nameAddressee;
    }

    public function getLinkToReportForMinistryOfJustice()
    {
        return $this->linkToReportForMinistryOfJustice;
    }

    public function setLinkToReportForMinistryOfJustice($linkToReportForMinistryOfJustice)
    {
        $this->linkToReportForMinistryOfJustice = $linkToReportForMinistryOfJustice;

        return $this;
    }

    public function setLinkToAnnualReport($linkToAnnualReport)
    {
        $this->linkToAnnualReport = $linkToAnnualReport;

        return $this;
    }

    public function getLinkToAnnualReport()
    {
        return $this->linkToAnnualReport;
    }

    public function addPublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $publication->setKNS2017Application($this);
        $this->publications[] = $publication;

        return $this;
    }

    public function removePublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $this->publications->removeElement($publication);

        return $this;
    }

    public function getPublications()
    {
        return $this->publications;
    }

    public function getMeasurementTools()
    {
        return $this->measurementTools;
    }

    public function addMeasurementTool(FileWithTitle $element)
    {
        $this->measurementTools[] = $element;

        return $this;
    }

    public function removeMeasurementTool(FileWithTitle $element)
    {
        $this->measurementTools->removeElement($element);

        return $this;
    }

    public function getResultConfirmingDocuments()
    {
        return $this->resultConfirmingDocuments;
    }

    public function addResultConfirmingDocument(FileWithFields $element)
    {
        $this->resultConfirmingDocuments[] = $element;

        return $this;
    }

    public function removeResultConfirmingDocument(FileWithFields $element)
    {
        $this->resultConfirmingDocuments->removeElement($element);

        return $this;
    }

    public function getPerformanceConfirmingDocuments()
    {
        return $this->performanceConfirmingDocuments;
    }

    public function addPerformanceConfirmingDocument(FileWithTitle $element)
    {
        $this->performanceConfirmingDocuments[] = $element;

        return $this;
    }

    public function removePerformanceConfirmingDocument(FileWithTitle $element)
    {
        $this->performanceConfirmingDocuments->removeElement($element);

        return $this;
    }

    public function getPracticeRegulationDocuments()
    {
        return $this->practiceRegulationDocuments;
    }

    public function addPracticeRegulationDocument(FileWithTitle $element)
    {
        $this->practiceRegulationDocuments[] = $element;

        return $this;
    }

    public function removePracticeRegulationDocument(FileWithTitle $element)
    {
        $this->practiceRegulationDocuments->removeElement($element);

        return $this;
    }

    public function getResultDocuments()
    {
        return $this->resultDocuments;
    }

    public function addResultDocument(FileWithTitle $element)
    {
        $this->resultDocuments[] = $element;

        return $this;
    }

    public function removeResultDocument(FileWithTitle $element)
    {
        $this->resultDocuments->removeElement($element);

        return $this;
    }
}
