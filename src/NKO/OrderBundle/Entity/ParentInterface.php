<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/20/17
 * Time: 4:32 PM
 */

namespace NKO\OrderBundle\Entity;

interface ParentInterface
{
    public function getParent();
}