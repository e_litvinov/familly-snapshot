<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProjectResult
 *
 * @ORM\Table(name="beneficiary_report_result")
 * @ORM\Entity()
 */
class BeneficiaryReportResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     *
     * @ORM\Column(name="plan_value", type="integer", nullable=true)
     */
    private $planValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="fact_value", type="integer", nullable=true)
     */
    private $factValue;

    /**
     * @var int
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="expected_value", type="integer", nullable=true)
     */
    private $expectedValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="method_measurement", type="text", nullable=true)
     */
    private $methodMeasurement;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="comments", type="text", nullable=true)
     */
    private $comments;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="result_criteria", type="text", nullable=true)
     *
     */
    private $result;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)"
     * )
     * @ORM\Column(name="indicator", type="text", nullable=true)
     *
     */
    private $indicator;

    /**
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="beneficiaryReportResults", cascade={"persist"})
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $report;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set planValue
     *
     * @param integer $planValue
     *
     * @return BeneficiaryReportResult
     */
    public function setPlanValue($planValue)
    {
        $this->planValue = $planValue;

        return $this;
    }

    /**
     * Get planValue
     *
     * @return integer
     */
    public function getPlanValue()
    {
        return $this->planValue;
    }

    /**
     * Set factValue
     *
     * @param integer $factValue
     *
     * @return BeneficiaryReportResult
     */
    public function setFactValue($factValue)
    {
        $this->factValue = $factValue;

        return $this;
    }

    /**
     * Get factValue
     *
     * @return integer
     */
    public function getFactValue()
    {
        return $this->factValue;
    }

    /**
     * Set expectedValue
     *
     * @param integer $expectedValue
     *
     * @return BeneficiaryReportResult
     */
    public function setExpectedValue($expectedValue)
    {
        $this->expectedValue = $expectedValue;

        return $this;
    }

    /**
     * Get expectedValue
     *
     * @return integer
     */
    public function getExpectedValue()
    {
        return $this->expectedValue;
    }

    /**
     * Set methodMeasurement
     *
     * @param string $methodMeasurement
     *
     * @return BeneficiaryReportResult
     */
    public function setMethodMeasurement($methodMeasurement)
    {
        $this->methodMeasurement = $methodMeasurement;

        return $this;
    }

    /**
     * Get methodMeasurement
     *
     * @return string
     */
    public function getMethodMeasurement()
    {
        return $this->methodMeasurement;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return BeneficiaryReportResult
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return BeneficiaryReportResult
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return BeneficiaryReportResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report $report
     *
     * @return BeneficiaryReportResult
     */
    public function setReport(\NKO\OrderBundle\Entity\Report $report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }
}
