<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="mark_list_history")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\MarkListHistoryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class MarkListHistory
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\ExpertUser", inversedBy="markListHistories")
     * @ORM\JoinColumn(name="expert_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $expert;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Competition")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $competition;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory")
     * @ORM\JoinColumn(name="application_history_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $applicationHistory;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\NKOUser")
     * @ORM\JoinColumn(name="nko_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $author;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_send", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isSend;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return MarkListHistory
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return MarkListHistory
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     *
     * @return MarkListHistory
     */
    public function setExpert(\NKO\UserBundle\Entity\ExpertUser $expert = null)
    {
        $this->expert = $expert;

        return $this;
    }

    /**
     * Get expert
     *
     * @return \NKO\UserBundle\Entity\ExpertUser
     */
    public function getExpert()
    {
        return $this->expert;
    }

    /**
     * Set competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return MarkListHistory
     */
    public function setCompetition(\NKO\OrderBundle\Entity\Competition $competition = null)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return \NKO\OrderBundle\Entity\Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Set applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     *
     * @return MarkListHistory
     */
    public function setApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory = null)
    {
        $this->applicationHistory = $applicationHistory;

        return $this;
    }

    /**
     * Get applicationHistory
     *
     * @return \NKO\OrderBundle\Entity\ApplicationHistory
     */
    public function getApplicationHistory()
    {
        return $this->applicationHistory;
    }

    /**
     * Set author
     *
     * @param \NKO\UserBundle\Entity\NKOUser $author
     *
     * @return MarkListHistory
     */
    public function setAuthor(\NKO\UserBundle\Entity\NKOUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \NKO\UserBundle\Entity\NKOUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return MarkListHistory
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }
}
