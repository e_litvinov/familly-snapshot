<?php


namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Folder
 *
 * @ORM\Table(name="folder")
 * @ORM\Entity()
 */
class Folder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="folder", type="text", nullable=true)
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\DocumentReport", mappedBy="folder", cascade={"persist"})
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="DocumentType", mappedBy="folder", cascade={"persist"})
     */
    private $document_types;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Folder", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Folder", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="report_class", type="string", nullable=true)
     */
    private $reportClass;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->document_types = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        if ($this->getParent()) {
            return (string) ($this->getParent()->getFolder() . '. ' . $this->getFolder());
        }
        return (string) $this->getFolder();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set folder
     *
     * @param string $folder
     *
     * @return Folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return string
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     *
     * @return Folder
     */
    public function addDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $document->setFolder($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set reportClass
     *
     * @param string $reportClass
     *
     * @return Folder
     */
    public function setReportClass($reportClass)
    {
        $this->reportClass = $reportClass;

        return $this;
    }

    /**
     * Get reportClass
     *
     * @return string
     */
    public function getReportClass()
    {
        return $this->reportClass;
    }

    /**
     * Add documentType
     *
     * @param \NKO\OrderBundle\Entity\DocumentType $documentType
     *
     * @return Folder
     */
    public function addDocumentType(\NKO\OrderBundle\Entity\DocumentType $documentType)
    {
        $this->document_types[] = $documentType;

        return $this;
    }

    /**
     * Remove documentType
     *
     * @param \NKO\OrderBundle\Entity\DocumentType $documentType
     */
    public function removeDocumentType(\NKO\OrderBundle\Entity\DocumentType $documentType)
    {
        $this->document_types->removeElement($documentType);
    }

    /**
     * Get documentTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocumentTypes()
    {
        return $this->document_types;
    }

    /**
     * Add child
     *
     * @param \NKO\OrderBundle\Entity\Folder $child
     *
     * @return Folder
     */
    public function addChild(\NKO\OrderBundle\Entity\Folder $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \NKO\OrderBundle\Entity\Folder $child
     */
    public function removeChild(\NKO\OrderBundle\Entity\Folder $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \NKO\OrderBundle\Entity\Folder $parent
     *
     * @return Folder
     */
    public function setParent(\NKO\OrderBundle\Entity\Folder $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \NKO\OrderBundle\Entity\Folder
     */
    public function getParent()
    {
        return $this->parent;
    }
}
