<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 05.01.17
 * Time: 14:09
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Traits\AutosaveTrait;
use NKO\OrderBundle\Traits\FieldGetter;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use NKO\OrderBundle\Traits\BankDetailsTrait;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="base_application")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="competition_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "course_for_a_family" = "Application",
 *     "farvater" = "NKO\OrderBundle\Entity\Farvater\FarvaterApplication",
 *     "brief_application2017" = "NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication",
 *     "kns2017" = "NKO\OrderBundle\Entity\KNS2017\Application",
 *     "brief_application2016" = "NKO\OrderBundle\Entity\BriefApplication2016\Application",
 *     "farvater2017" = "NKO\OrderBundle\Entity\Farvater2017\Application",
 *     "kns2017-2" = "NKO\OrderBundle\Entity\KNS2017\SecondStageApplication",
 *     "brief_application_2018" = "NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application",
 *     "continuation_of_work" = "NKO\OrderBundle\Entity\Application\Continuation\Application",
 *     "kns2018" = "NKO\OrderBundle\Entity\Application\KNS\Application2018\Application",
 *     "farvater2018" = "NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application",
 *     "kns2017second_stage_2" = "NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application",
 *     "continuation_second_stage" = "NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application",
 *     "continuation_second_stage_2019" = "NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application2019\Application",
 *     "continuation_kns" = "NKO\OrderBundle\Entity\Application\Continuation\KNS\Application",
 *     "kns-2019" = "NKO\OrderBundle\Entity\Application\KNS\Application2019\Application",
 *     "kns2018second_stage_2" = "NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application",
 *     "kns2018third_stage_2" = "NKO\OrderBundle\Entity\Application\KNS\ThirdStage2018\Application",
 *     "harbor_application_2019" = "NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application",
 *     "harbor_application_2020" = "NKO\OrderBundle\Entity\Application\Harbor\Application2020\Application",
 *     "kns-2020" = "NKO\OrderBundle\Entity\Application\KNS\Application2020\Application",
 *     "verification_application_2020" = "NKO\OrderBundle\Entity\Application\Verification\Application2020\Application",
 *     "kns2019second_stage" = "NKO\OrderBundle\Entity\Application\KNS\SecondStage2019\Application",
 *     })
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
abstract class BaseApplication
{
    use ORMBehaviors\Timestampable\Timestampable;
    use BankDetailsTrait;
    use FieldGetter;
    use AutosaveTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *          "HeadOfOrganization", "ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"
     *      }
     *     )
     *
     * @ORM\Column(name="head_of_organization_full_name", type="string",
     *      length=255, nullable=true)
     */
    protected $headOfOrganizationFullName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "HeadOfOrganization", "ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"
     *      }
     *     )
     *
     * @ORM\Column(name="head_of_organization_position", type="string",
     *      length=255, nullable=true)
     */
    protected $headOfOrganizationPosition;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfOrganization"
     *          }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfOrganization"
     *          }
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "phone code must be at least 3 characters long",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_organization_phone_code", type="string", length=255, nullable=true)
     */
    protected $headOfOrganizationPhoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "phone must be at least 7 characters long",
     *     groups={
     *              "KNS-2017", "BriefApplication-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS-2020", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_organization_phone", type="string", length=255, nullable=true)
     */
    protected $headOfOrganizationPhone;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "mobile code must be at least 3 characters long",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_organization_mobile_phone_code", type="string", length=255, nullable=true)
     */
    protected $headOfOrganizationMobilePhoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "mobile phone must be at least 7 characters long",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_organization_mobile_phone", type="string", length=255, nullable=true)
     */
    protected $headOfOrganizationMobilePhone;

    /**
     * @var string
     *
     * @Assert\Email(
     *     message = "The email is not valid",
     *     strict = true,
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern = "/[а-я]/",
     *     match = false,
     *     message = "not Cyrillic",
     *     groups={
     *              "HeadOfOrganization"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_organization_emeil", type="string", length=255, nullable=true)
     */
    protected $headOfOrganizationEmeil;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018", "KNS-2018",
     *              "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "Harbor-2020", "KNS2018-3", "KNS-2020",
     *              "Verification-2020", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_full_name", type="string",
     *      length=255, nullable=true)
     */
    protected $headOfProjectFullName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018", "KNS-2018",
     *              "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "Harbor-2020", "KNS2018-3", "KNS-2020",
     *              "Verification-2020", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_position", type="string",
     *      length=255, nullable=true)
     */
    protected $headOfProjectPosition;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "phone code must be at least 3 characters long",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_phone_code", type="string", length=255, nullable=true)
     */
    protected $headOfProjectPhoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "phone must be at least 7 characters long",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_phone", type="string", length=255, nullable=true)
     */
    protected $headOfProjectPhone;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "mobile code must be at least 3 characters long",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_mobile_phone_code", type="string", length=255, nullable=true)
     */
    protected $headOfProjectMobilePhoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "mobile phone must be at least 7 characters long",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_mobile_phone", type="string", length=255, nullable=true)
     */
    protected $headOfProjectMobilePhone;

    /**
     * @var string
     *
     * @Assert\Email(
     *     message = "The email is not valid",
     *     strict = true,
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern = "/[а-я]/",
     *     match = false,
     *     message = "not Cyrillic",
     *     groups={
     *              "HeadOfProject"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_project_emeil", type="string",
     *     length=255, nullable=true)
     */
    protected $headOfProjectEmeil;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfAccounting", "ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_accounting_full_name", type="string",
     *      length=255, nullable=true)
     */
    protected $headOfAccountingFullName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "phone code must be at least 3 characters long",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_accounting_phone_code", type="string", length=255, nullable=true)
     */
    protected $headOfAccountingPhoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "phone must be at least 7 characters long",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_accounting_phone", type="string", length=255, nullable=true)
     */
    protected $headOfAccountingPhone;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "KNS-2020", "Verification-2020"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "KNS-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "KNS-2020", "Verification-2020"
     *            }
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "mobile code must be at least 3 characters long",
     *     groups={
     *              "KNS-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "KNS-2020", "Verification-2020"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_accounting_mobile_phone_code", type="string", length=255, nullable=true)
     */
    protected $headOfAccountingMobilePhoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "KNS-2020", "Verification-2020"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "KNS-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "KNS-2020", "Verification-2020"
     *            }
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "mobile phone must be at least 7 characters long",
     *     groups={
     *              "KNS-2017", "KNS-2016", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "KNS-2020", "Verification-2020"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_accounting_mobile_phone", type="string", length=255, nullable=true)
     */
    protected $headOfAccountingMobilePhone;

    /**
     * @var string
     *
     * @Assert\Email(
     *     message = "The email is not valid",
     *     strict = true,
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     *
     * @Assert\Regex(
     *     pattern = "/[а-я]/",
     *     match = false,
     *     message = "not Cyrillic",
     *     groups={
     *              "HeadOfAccounting"
     *            }
     *     )
     *
     * @ORM\Column(name="head_of_accounting_emeil", type="string",
     *     length=255, nullable=true)
     */
    protected $headOfAccountingEmeil;

    /**
     * @ORM\OneToMany(targetEntity="Risk", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $risks;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018", "ContinuationApplication-2018", "KNS-2018", "KNS2017-2",
     *              "ContinuationApplication-2", "ContinuationApplicationKNS",  "KNS-2019", "Harbor-2019", "Harbor-2020", "ContinuationSF18-2019", "KNS2018-3", "KNS-2020",
     *              "Verification-2020", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(name="project_name", type="text", nullable=true)
     */
    protected $projectName;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2016", "KNS-2017", "ContinuationApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplication-2", "ContinuationApplicationKNS", "KNS-2019",
     *              "ContinuationSF18-2019", "KNS2018-3", "KNS-2020", "Harbor-2020", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(name="deadLineStart", type="datetime", nullable=true)
     */
    protected $deadLineStart;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2016", "KNS-2017", "ContinuationApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplication-2", "ContinuationApplicationKNS", "KNS-2019",
     *              "ContinuationSF18-2019", "KNS2018-3", "KNS-2020", "Harbor-2020", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(name="deadLineFinish", type="datetime", nullable=true)
     */
    protected $deadLineFinish;

    /**
     * @ORM\OneToMany(targetEntity="BeneficiaryResult", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $beneficiaryResults;

    /**
     * @ORM\OneToMany(targetEntity="EmployeeResult", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $employeeResults;

    /**
     * @ORM\ManyToMany(targetEntity="ChildrenCategory")
     * @ORM\JoinTable(name="application_children_category",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="children_category_id", referencedColumnName="id")}
     *      )
     */
    protected $childrenCategories;

    /**
     * @var bool
     *
     * @ORM\Column(name="ready_to_sent", type="boolean", nullable=true)
     */
    protected $readyToSent;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_send", type="boolean", nullable=true)
     */
    protected $isSend;

    /**
     * @var string
     *
     * @ORM\Column(name="children_category_name", type="string", length=500, nullable=true)
     */
    protected $childrenCategoryName;

    /**
     * @ORM\ManyToOne(targetEntity="Competition", inversedBy="applications")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    protected $competition;

    /**
     *
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\NKOUser")
     * @ORM\JoinColumn(name="author", referencedColumnName="id", nullable=true, onDelete="cascade")
     *
     */
    protected $author;

    /**
     * @var string
     *
     * @ORM\Column(name="sendVersion", type="string", nullable=true, options={"default" : "0"})
     */
    protected $sendVersion;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={
     *         "KNS-2017", "KNS-2016", "Farvater-2017", "BriefApplication-2018", "KNS-2018", "Farvater-2018"
     *     }
     * )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={
     *         "KNS-2017", "KNS-2016", "Farvater-2017", "BriefApplication-2018", "KNS-2018", "Farvater-2017"
     *     },
     * )
     * @ORM\Column(name="regulation", type="string", length=255, nullable=true)
     */
    protected $regulation;

    protected $_regulation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=200, nullable=true, options={"default" : "0"})
     */
    protected $version;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isFirstTimeSent;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_accepted", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isAccepted;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true,  options={"default" : 0})
     */
    private $winner;

    /**
     * @Assert\Count(
     *     min="1",
     *     minMessage="value is invalid(field must be non empty)",
     *     groups={"HarborAnalyticReport-2019", "ContinuationSF18-2019", "Harbor-2020", "Verification-2020", "KNS2019-2"}
     *     )
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct")
     * @ORM\JoinTable(name="application_normative_act",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *         inverseJoinColumns={@ORM\JoinColumn(name="normative_act_id", referencedColumnName="id")}
     * )
     */
    protected $normativeActs;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->risks = new ArrayCollection();
        $this->beneficiaryResults = new ArrayCollection();
        $this->employeeResults = new ArrayCollection();
        $this->childrenCategories = new ArrayCollection();
        $this->normativeActs = new ArrayCollection();
        $this->readyToSent = false;
        $this->isSend = false;
    }

    /**
     * @Assert\Callback(
     *     groups={
     *         "KNS-2017", "KNS-2016", "Farvater-2017", "BriefApplication-2018", "KNS-2018", "Farvater-2018"
     *     }
     * )
     */
    public function isValidRegulation(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getRegulation() && !$this->_regulation) {
            $context->buildViolation('file not found')
                ->atPath('regulation')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016", "KNS-2017"}
     *     )
     */
    public function isValidDateFinish(ExecutionContextInterface $context, $payload)
    {
        if ($this->getDeadLineFinish()==null || $this->getDeadLineStart()==null) {
            ;
        } elseif ($this->getDeadLineFinish()->getTimestamp() <= $this->getDeadLineStart()->getTimestamp()) {
                $context->buildViolation('the date of end of the project should not be before the start date')
                    ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016", "KNS-2017"}
     *     )
     */
    public function isValidOKPOLength(ExecutionContextInterface $context, $payload)
    {
        if (strlen($this->oKPO) == 9) {
            $context->buildViolation('OKPO can not be 9 characters long')
                ->atPath('oKPO')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016", "KNS-2017"}
     *     )
     */
    public function isValidChildrenCategory(ExecutionContextInterface $context, $payload)
    {
        if (count($this->childrenCategories)==0 && !$this->childrenCategoryName) {
            $context->buildViolation('please, choose children category')
                ->atPath('childrenCategories')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set headOfOrganizationFullName
     *
     * @param string $headOfOrganizationFullName
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationFullName($headOfOrganizationFullName)
    {
        $this->headOfOrganizationFullName = $headOfOrganizationFullName;

        return $this;
    }

    /**
     * Get headOfOrganizationFullName
     *
     * @return string
     */
    public function getHeadOfOrganizationFullName()
    {
        return $this->headOfOrganizationFullName;
    }

    /**
     * Set headOfOrganizationPosition
     *
     * @param string $headOfOrganizationPosition
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationPosition($headOfOrganizationPosition)
    {
        $this->headOfOrganizationPosition = $headOfOrganizationPosition;

        return $this;
    }

    /**
     * Get headOfOrganizationPosition
     *
     * @return string
     */
    public function getHeadOfOrganizationPosition()
    {
        return $this->headOfOrganizationPosition;
    }

    /**
     * Set headOfOrganizationPhoneCode
     *
     * @param string $headOfOrganizationPhoneCode
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationPhoneCode($headOfOrganizationPhoneCode)
    {
        $this->headOfOrganizationPhoneCode = $headOfOrganizationPhoneCode;

        return $this;
    }

    /**
     * Get headOfOrganizationPhoneCode
     *
     * @return string
     */
    public function getHeadOfOrganizationPhoneCode()
    {
        return $this->headOfOrganizationPhoneCode;
    }

    /**
     * Set headOfOrganizationPhone
     *
     * @param string $headOfOrganizationPhone
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationPhone($headOfOrganizationPhone)
    {
        $this->headOfOrganizationPhone = $headOfOrganizationPhone;

        return $this;
    }

    /**
     * Get headOfOrganizationPhone
     *
     * @return string
     */
    public function getHeadOfOrganizationPhone()
    {
        return $this->headOfOrganizationPhone;
    }

    /**
     * Set headOfOrganizationMobilePhoneCode
     *
     * @param string $headOfOrganizationMobilePhoneCode
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationMobilePhoneCode($headOfOrganizationMobilePhoneCode)
    {
        $this->headOfOrganizationMobilePhoneCode = $headOfOrganizationMobilePhoneCode;

        return $this;
    }

    /**
     * Get headOfOrganizationMobilePhoneCode
     *
     * @return string
     */
    public function getHeadOfOrganizationMobilePhoneCode()
    {
        return $this->headOfOrganizationMobilePhoneCode;
    }

    /**
     * Set headOfOrganizationMobilePhone
     *
     * @param string $headOfOrganizationMobilePhone
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationMobilePhone($headOfOrganizationMobilePhone)
    {
        $this->headOfOrganizationMobilePhone = $headOfOrganizationMobilePhone;

        return $this;
    }

    /**
     * Get headOfOrganizationMobilePhone
     *
     * @return string
     */
    public function getHeadOfOrganizationMobilePhone()
    {
        return $this->headOfOrganizationMobilePhone;
    }

    /**
     * Set headOfOrganizationEmeil
     *
     * @param string $headOfOrganizationEmeil
     *
     * @return BaseApplication
     */
    public function setHeadOfOrganizationEmeil($headOfOrganizationEmeil)
    {
        $this->headOfOrganizationEmeil = $headOfOrganizationEmeil;

        return $this;
    }

    /**
     * Get headOfOrganizationEmeil
     *
     * @return string
     */
    public function getHeadOfOrganizationEmeil()
    {
        return $this->headOfOrganizationEmeil;
    }

    /**
     * Set headOfProjectFullName
     *
     * @param string $headOfProjectFullName
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectFullName($headOfProjectFullName)
    {
        $this->headOfProjectFullName = $headOfProjectFullName;

        return $this;
    }

    /**
     * Get headOfProjectFullName
     *
     * @return string
     */
    public function getHeadOfProjectFullName()
    {
        return $this->headOfProjectFullName;
    }

    /**
     * Set headOfProjectPhoneCode
     *
     * @param string $headOfProjectPhoneCode
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectPhoneCode($headOfProjectPhoneCode)
    {
        $this->headOfProjectPhoneCode = $headOfProjectPhoneCode;

        return $this;
    }

    /**
     * Get headOfProjectPhoneCode
     *
     * @return string
     */
    public function getHeadOfProjectPhoneCode()
    {
        return $this->headOfProjectPhoneCode;
    }

    /**
     * Set headOfProjectPhone
     *
     * @param string $headOfProjectPhone
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectPhone($headOfProjectPhone)
    {
        $this->headOfProjectPhone = $headOfProjectPhone;

        return $this;
    }

    /**
     * Get headOfProjectPhone
     *
     * @return string
     */
    public function getHeadOfProjectPhone()
    {
        return $this->headOfProjectPhone;
    }

    /**
     * Set headOfProjectMobilePhoneCode
     *
     * @param string $headOfProjectMobilePhoneCode
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectMobilePhoneCode($headOfProjectMobilePhoneCode)
    {
        $this->headOfProjectMobilePhoneCode = $headOfProjectMobilePhoneCode;

        return $this;
    }

    /**
     * Get headOfProjectMobilePhoneCode
     *
     * @return string
     */
    public function getHeadOfProjectMobilePhoneCode()
    {
        return $this->headOfProjectMobilePhoneCode;
    }

    /**
     * Set headOfProjectMobilePhone
     *
     * @param string $headOfProjectMobilePhone
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectMobilePhone($headOfProjectMobilePhone)
    {
        $this->headOfProjectMobilePhone = $headOfProjectMobilePhone;

        return $this;
    }

    /**
     * Get headOfProjectMobilePhone
     *
     * @return string
     */
    public function getHeadOfProjectMobilePhone()
    {
        return $this->headOfProjectMobilePhone;
    }

    /**
     * Set headOfProjectEmeil
     *
     * @param string $headOfProjectEmeil
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectEmeil($headOfProjectEmeil)
    {
        $this->headOfProjectEmeil = $headOfProjectEmeil;

        return $this;
    }

    /**
     * Get headOfProjectEmeil
     *
     * @return string
     */
    public function getHeadOfProjectEmeil()
    {
        return $this->headOfProjectEmeil;
    }

    /**
     * Set headOfAccountingFullName
     *
     * @param string $headOfAccountingFullName
     *
     * @return BaseApplication
     */
    public function setHeadOfAccountingFullName($headOfAccountingFullName)
    {
        $this->headOfAccountingFullName = $headOfAccountingFullName;

        return $this;
    }

    /**
     * Get headOfAccountingFullName
     *
     * @return string
     */
    public function getHeadOfAccountingFullName()
    {
        return $this->headOfAccountingFullName;
    }

    /**
     * Set headOfAccountingPhoneCode
     *
     * @param string $headOfAccountingPhoneCode
     *
     * @return BaseApplication
     */
    public function setHeadOfAccountingPhoneCode($headOfAccountingPhoneCode)
    {
        $this->headOfAccountingPhoneCode = $headOfAccountingPhoneCode;

        return $this;
    }

    /**
     * Get headOfAccountingPhoneCode
     *
     * @return string
     */
    public function getHeadOfAccountingPhoneCode()
    {
        return $this->headOfAccountingPhoneCode;
    }

    /**
     * Set headOfAccountingPhone
     *
     * @param string $headOfAccountingPhone
     *
     * @return BaseApplication
     */
    public function setHeadOfAccountingPhone($headOfAccountingPhone)
    {
        $this->headOfAccountingPhone = $headOfAccountingPhone;

        return $this;
    }

    /**
     * Get headOfAccountingPhone
     *
     * @return string
     */
    public function getHeadOfAccountingPhone()
    {
        return $this->headOfAccountingPhone;
    }

    /**
     * Set headOfAccountingMobilePhoneCode
     *
     * @param string $headOfAccountingMobilePhoneCode
     *
     * @return BaseApplication
     */
    public function setHeadOfAccountingMobilePhoneCode($headOfAccountingMobilePhoneCode)
    {
        $this->headOfAccountingMobilePhoneCode = $headOfAccountingMobilePhoneCode;

        return $this;
    }

    /**
     * Get headOfAccountingMobilePhoneCode
     *
     * @return string
     */
    public function getHeadOfAccountingMobilePhoneCode()
    {
        return $this->headOfAccountingMobilePhoneCode;
    }

    /**
     * Set headOfAccountingMobilePhone
     *
     * @param string $headOfAccountingMobilePhone
     *
     * @return BaseApplication
     */
    public function setHeadOfAccountingMobilePhone($headOfAccountingMobilePhone)
    {
        $this->headOfAccountingMobilePhone = $headOfAccountingMobilePhone;

        return $this;
    }

    /**
     * Get headOfAccountingMobilePhone
     *
     * @return string
     */
    public function getHeadOfAccountingMobilePhone()
    {
        return $this->headOfAccountingMobilePhone;
    }

    /**
     * Set headOfAccountingEmeil
     *
     * @param string $headOfAccountingEmeil
     *
     * @return BaseApplication
     */
    public function setHeadOfAccountingEmeil($headOfAccountingEmeil)
    {
        $this->headOfAccountingEmeil = $headOfAccountingEmeil;

        return $this;
    }

    /**
     * Get headOfAccountingEmeil
     *
     * @return string
     */
    public function getHeadOfAccountingEmeil()
    {
        return $this->headOfAccountingEmeil;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     *
     * @return BaseApplication
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set deadLineStart
     *
     * @param \DateTime $deadLineStart
     *
     * @return BaseApplication
     */
    public function setDeadLineStart($deadLineStart)
    {
        $this->deadLineStart = $deadLineStart;

        return $this;
    }

    /**
     * Get deadLineStart
     *
     * @return \DateTime
     */
    public function getDeadLineStart()
    {
        return $this->deadLineStart;
    }

    /**
     * Set deadLineFinish
     *
     * @param \DateTime $deadLineFinish
     *
     * @return BaseApplication
     */
    public function setDeadLineFinish($deadLineFinish)
    {
        $this->deadLineFinish = $deadLineFinish;

        return $this;
    }

    /**
     * Get deadLineFinish
     *
     * @return \DateTime
     */
    public function getDeadLineFinish()
    {
        return $this->deadLineFinish;
    }

    /**
     * Set headOfProjectPosition
     *
     * @param string $headOfProjectPosition
     *
     * @return BaseApplication
     */
    public function setHeadOfProjectPosition($headOfProjectPosition)
    {
        $this->headOfProjectPosition = $headOfProjectPosition;

        return $this;
    }

    /**
     * Get headOfProjectPosition
     *
     * @return string
     */
    public function getHeadOfProjectPosition()
    {
        return $this->headOfProjectPosition;
    }

    /**
     * Set readyToSent
     *
     * @param boolean $readyToSent
     *
     * @return BaseApplication
     */
    public function setReadyToSent($readyToSent)
    {
        $this->readyToSent = $readyToSent;

        return $this;
    }

    /**
     * Get readyToSent
     *
     * @return boolean
     */
    public function getReadyToSent()
    {
        return $this->readyToSent;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return BaseApplication
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set childrenCategoryName
     *
     * @param string $childrenCategoryName
     *
     * @return BaseApplication
     */
    public function setChildrenCategoryName($childrenCategoryName)
    {
        $this->childrenCategoryName = $childrenCategoryName;

        return $this;
    }

    /**
     * Get childrenCategoryName
     *
     * @return string
     */
    public function getChildrenCategoryName()
    {
        return $this->childrenCategoryName;
    }

    /**
     * Add childrenCategory
     *
     * @param \NKO\OrderBundle\Entity\ChildrenCategory $childrenCategory
     *
     * @return BaseApplication
     */
    public function addChildrenCategory(\NKO\OrderBundle\Entity\ChildrenCategory $childrenCategory)
    {
        $this->childrenCategories->add($childrenCategory);

        return $this;
    }

    /**
     * Remove childrenCategory
     *
     * @param \NKO\OrderBundle\Entity\ChildrenCategory $childrenCategory
     */
    public function removeChildrenCategory(\NKO\OrderBundle\Entity\ChildrenCategory $childrenCategory)
    {
        $this->childrenCategories->removeElement($childrenCategory);
    }

    /**
     * Get childrenCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrenCategories()
    {
        return $this->childrenCategories;
    }

    public function setChildrenCategories($childrenCategories)
    {
        if (count($childrenCategories) > 0) {
            foreach ($childrenCategories as $childrenCategory) {
                $this->addChildrenCategory($childrenCategory);
            }
        }
        return $this;
    }

    /**
     * Set competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return BaseApplication
     */
    public function setCompetition(\NKO\OrderBundle\Entity\Competition $competition = null)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return \NKO\OrderBundle\Entity\Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Set author
     *
     * @param \NKO\UserBundle\Entity\NKOUser $author
     *
     * @return BaseApplication
     */
    public function setAuthor(\NKO\UserBundle\Entity\NKOUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \NKO\UserBundle\Entity\NKOUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set sendVersion
     *
     * @param string $sendVersion
     *
     * @return BaseApplication
     */
    public function setSendVersion($sendVersion)
    {
        $this->sendVersion = $sendVersion;

        return $this;
    }

    /**
     * Get sendVersion
     *
     * @return string
     */
    public function getSendVersion()
    {
        return $this->sendVersion;
    }

    /**
     * Set regulation
     *
     * @param string $regulation
     *
     * @return Application
     */
    public function setRegulation($regulation)
    {
        if (!$this->_regulation && is_string($this->regulation)) {
            $this->_regulation = $this->regulation;
        }

        $this->regulation = $regulation;

        return $this;
    }

    /**
     * Get regulation
     *
     * @return string
     */
    public function getRegulation()
    {
        return $this->regulation;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return BaseApplication
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }


    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return BaseApplication
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
    
    /**
     * Add risk
     *
     * @param \NKO\OrderBundle\Entity\Risk $risk
     *
     * @return BaseApplication
     */
    public function addRisk(\NKO\OrderBundle\Entity\Risk $risk)
    {
        $risk->setApplication($this);
        $this->risks[] = $risk;

        return $this;
    }

    /**
     * Remove risk
     *
     * @param \NKO\OrderBundle\Entity\Risk $risk
     */
    public function removeRisk(\NKO\OrderBundle\Entity\Risk $risk)
    {
        $this->risks->removeElement($risk);
    }

    /**
     * Get risks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRisks()
    {
        return $this->risks;
    }

    /**
     * Add beneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryResult $beneficiaryResult
     *
     * @return BaseApplication
     */
    public function addBeneficiaryResult(\NKO\OrderBundle\Entity\BeneficiaryResult $beneficiaryResult)
    {
        $beneficiaryResult->setApplication($this);
        $this->beneficiaryResults[] = $beneficiaryResult;

        return $this;
    }

    /**
     * Remove beneficiaryResult
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryResult $beneficiaryResult
     */
    public function removeBeneficiaryResult(\NKO\OrderBundle\Entity\BeneficiaryResult $beneficiaryResult)
    {
        $this->beneficiaryResults->removeElement($beneficiaryResult);
    }

    /**
     * Get beneficiaryResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryResults()
    {
        return $this->beneficiaryResults;
    }
    
    /**
     * Add employeeResult
     *
     * @param \NKO\OrderBundle\Entity\EmployeeResult $employeeResult
     *
     * @return BaseApplication
     */
    public function addEmployeeResult(\NKO\OrderBundle\Entity\EmployeeResult $employeeResult)
    {
        $employeeResult->setApplication($this);
        $this->employeeResults[] = $employeeResult;

        return $this;
    }

    /**
     * Remove employeeResult
     *
     * @param \NKO\OrderBundle\Entity\EmployeeResult $employeeResult
     */
    public function removeEmployeeResult(\NKO\OrderBundle\Entity\EmployeeResult $employeeResult)
    {
        $this->employeeResults->removeElement($employeeResult);
    }

    /**
     * Get employeeResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployeeResults()
    {
        return $this->employeeResults;
    }

 
  
    /**
     * Set isFirstTimeSent
     *
     * @param boolean $isFirstTimeSent
     *
     * @return BaseApplication
     */
    public function setIsFirstTimeSent($isFirstTimeSent)
    {
        $this->isFirstTimeSent = $isFirstTimeSent;

        return $this;
    }

    /**
     * Get isFirstTimeSent
     *
     * @return boolean
     */
    public function getIsFirstTimeSent()
    {
        return $this->isFirstTimeSent;
    }

    /**
     * Set isAccepted
     *
     * @param boolean $isAccepted
     *
     * @return BaseApplication
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return boolean
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set winner
     *
     * @param boolean $winner
     *
     * @return BaseApplication
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return boolean
     */
    public function getWinner()
    {
        return $this->winner;
    }

    public function removeNormativeAct(NormativeAct $normativeAct = null)
    {
        $this->normativeActs->removeElement($normativeAct);
        $normativeAct->removeApplication($this);

        return $this;
    }

    public function addNormativeAct(NormativeAct $normativeAct = null)
    {
        $this->normativeActs[] = $normativeAct;
        $normativeAct->addApplication($this);

        return $this;
    }

    public function getNormativeActs()
    {
        return $this->normativeActs;
    }

    public function getRegulationHelp()
    {
        return $this->getField('_regulation');
    }

    public function getBudgetHelp()
    {
        return $this->getField('_budget');
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }

    public function getAuthorityDirectorDocumentHelp()
    {
        return $this->getField('_authorityDirectorDocument');
    }

    public function getBudgetDocumentHelp()
    {
        return $this->getField('_budgetDocument');
    }

    public function getUrStatusDocumentHelp()
    {
        return $this->getField('_urStatusDocumentHelp');
    }

    public function getLastYearReportDocumentHelp()
    {
        return $this->getField('_lastYearReportDocument');
    }

    public function getMailBankDocumentHelp()
    {
        return $this->getField('_mailBankDocument');
    }

    public function getMuReportMailHelp()
    {
        return $this->getField('_muReportMail');
    }

    public function getAuthorityHeadHelp()
    {
        return $this->getField('_authorityHead');
    }

    public function getSignedAgreementHelp()
    {
        return $this->getField('_signedAgreement');
    }

    public function getOrganizationHeadConsentHelp()
    {
        return $this->getField('_organizationHeadConsent');
    }

    public function getProjectHeadConsentHelp()
    {
        return $this->getField('_projectHeadConsent');
    }

    public function getAccountantHeadConsentHelp()
    {
        return $this->getField('_accountantHeadConsent');
    }

    public function getSendingApplicationPersonConsentHelp()
    {
        return $this->getField('_sendingApplicationPersonConsent');
    }

    public function getProjectBudgetHelp()
    {
        return $this->getField('_projectBudget');
    }

    public function getBankLetterExistenceAccountHelp()
    {
        return $this->getField('_bankLetterExistenceAccount');
    }

    public function getProcessingPersonalDataConsentHelp()
    {
        return $this->getField('_processingPersonalDataConsent');
    }

    public function getSubjectRegulationHelp()
    {
        return $this->getField('_subjectRegulation');
    }

    public function getAnotherHeadHelp()
    {
        return $this->getField('_anotherHead');
    }

    public function getOrganizationCreationResolutionHelp()
    {
        return $this->getField('_organizationCreationResolution');
    }

    public function getIsBankAccountExistHelp()
    {
        return $this->getField('_isBankAccountExist');
    }

    public function getReportForMinistryOfJusticeHelp()
    {
        return $this->getField('_reportForMinistryOfJustice');
    }

    public function getProjectDescriptionDocumentHelp()
    {
        return $this->getField('_projectDescriptionDocument');
    }
}
