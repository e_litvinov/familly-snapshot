<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpenseType
 *
 * @ORM\Table(name="expense_type")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ExpenseTypeRepository")
 */
class ExpenseType implements ExpenseTypeInterface
{
    public static $reportForm;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="helpProofProductExpenseDocument", type="text", nullable=true)
     */
    private $helpProofProductExpenseDocument;

    /**
     * @var string
     *
     * @ORM\Column(name="helpProofPaidGoodsDocument", type="text", nullable=true)
     */
    private $helpProofPaidGoodsDocument;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Register", mappedBy="expenseType")
     */
    private $registers;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\DocumentReport", mappedBy="expenseType")
     */
    private $documents;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ExpenseType", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ExpenseType", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\FinanceSpent", mappedBy="expenseType")
     */
    private $financeSpent;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition", inversedBy="expenseType", cascade={"all"})
     * @ORM\JoinTable(name="expense_type_competition")
     */
    private $competitions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report", mappedBy="expenseTypes")
     */
    private $reportForms;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $indexNumber;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ExpenseTypeConfig", mappedBy="expenseType")
     */
    private $expenseTypeConfig;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ExpenseType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add child
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $child
     *
     * @return ExpenseType
     */
    public function addChild(\NKO\OrderBundle\Entity\ExpenseType $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $child
     */
    public function removeChild(\NKO\OrderBundle\Entity\ExpenseType $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \NKO\OrderBundle\Entity\ExpenseType $parent
     *
     * @return ExpenseType
     */
    public function setParent(\NKO\OrderBundle\Entity\ExpenseType $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \NKO\OrderBundle\Entity\ExpenseType
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ExpenseType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return  $this->title;
    }

    public function getTitleWithNumber()
    {
        if (self::$reportForm) {
            if (!$this->expenseTypeConfig->isEmpty()) {
                $reportForm = self::$reportForm;
                $config = $this->expenseTypeConfig->filter(function ($object) use ($reportForm) {
                    return $object->getReportForm() === $reportForm;
                })->first();
                if ($config) {
                    return $config->getNumber().'. '.$this->getTitle();
                }
            }
        }

        if ($this->getCode() === ExpenseTypeInterface::SALARY_CODE) {
            return '1. '. $this->getTitle();
        }

        return $this->title;
    }

    public function setTitleWithNumber($title)
    {
        return $this;
    }

    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Add register
     *
     * @param \NKO\OrderBundle\Entity\Register $register
     *
     * @return ExpenseType
     */
    public function addRegister(\NKO\OrderBundle\Entity\Register $register)
    {
        $register->setExpenseType($this);
        $this->registers[] = $register;

        return $this;
    }

    /**
     * Remove register
     *
     * @param \NKO\OrderBundle\Entity\Register $register
     */
    public function removeRegister(\NKO\OrderBundle\Entity\Register $register)
    {
        $this->registers->removeElement($register);
    }

    /**
     * Get registers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegisters()
    {
        return $this->registers;
    }

    /**
     * Add financeSpent
     *
     * @param \NKO\OrderBundle\Entity\FinanceSpent $financeSpent
     *
     * @return ExpenseType
     */
    public function addFinanceSpent(\NKO\OrderBundle\Entity\FinanceSpent $financeSpent)
    {
        $this->financeSpent[] = $financeSpent;

        return $this;
    }

    /**
     * Remove financeSpent
     *
     * @param \NKO\OrderBundle\Entity\FinanceSpent $financeSpent
     */
    public function removeFinanceSpent(\NKO\OrderBundle\Entity\FinanceSpent $financeSpent)
    {
        $this->financeSpent->removeElement($financeSpent);
    }

    /**
     * Get financeSpent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinanceSpent()
    {
        return $this->financeSpent;
    }

    /**
     * Add competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return ExpenseType
     */
    public function addCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions[] = $competition;

        return $this;
    }

    /**
     * Remove competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     */
    public function removeCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions->removeElement($competition);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     *
     * @return ExpenseType
     */
    public function addDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    public function getParentCode()
    {
        $parent = $this->getParent();
        if ($parent) {
            return $parent->getCode();
        }
        return $this->code;
    }

    /**
     * Add reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return ExpenseType
     */
    public function addReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms[] = $reportForm;

        return $this;
    }

    /**
     * Remove reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     */
    public function removeReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms->removeElement($reportForm);
    }

    /**
     * Get reportForms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportForms()
    {
        return $this->reportForms;
    }

    public function getParentTitle()
    {
        $parent = $this->getParent();
        if (!$parent) {
            return $this->title;
        }
        return $parent->getTitle();
    }

    /**
     * Set indexNumber
     *
     * @param integer $indexNumber
     *
     * @return ExpenseType
     */
    public function setIndexNumber($indexNumber)
    {
        $this->indexNumber = $indexNumber;

        return $this;
    }

    /**
     * Get indexNumber
     *
     * @return integer
     */
    public function getIndexNumber()
    {
        return $this->indexNumber;
    }

    /**
     * Add expenseTypeConfig
     *
     * @param \NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig
     *
     * @return ExpenseType
     */
    public function addExpenseTypeConfig(\NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig)
    {
        $this->expenseTypeConfig[] = $expenseTypeConfig;
        $expenseTypeConfig->setExpenseType($this);

        return $this;
    }

    /**
     * Remove expenseTypeConfig
     *
     * @param \NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig
     */
    public function removeExpenseTypeConfig(\NKO\OrderBundle\Entity\ExpenseTypeConfig $expenseTypeConfig)
    {
        $this->expenseTypeConfig->removeElement($expenseTypeConfig);
    }

    /**
     * Get expenseTypeConfig
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpenseTypeConfig()
    {
        return $this->expenseTypeConfig;
    }

    /**
     * @return string
     */
    public function getHelpProofProductExpenseDocument()
    {
        return unserialize($this->helpProofProductExpenseDocument);
    }

    /**
     * @param array $help
     * @return ExpenseType
     */
    public function setHelpProofProductExpenseDocument($help)
    {
        $this->helpProofProductExpenseDocument = serialize($help);

        return $this;
    }

    /**
     * @return string
     */
    public function getHelpProofPaidGoodsDocument()
    {
        return unserialize($this->helpProofPaidGoodsDocument);
    }

    /**
     * @param array $helpProofPaidGoodsDocument
     * @return ExpenseType
     */
    public function setHelpProofPaidGoodsDocument($helpProofPaidGoodsDocument)
    {
        $this->helpProofPaidGoodsDocument = serialize($helpProofPaidGoodsDocument);

        return $this;
    }
}
