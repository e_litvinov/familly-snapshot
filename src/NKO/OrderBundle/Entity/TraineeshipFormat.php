<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * TraineeshipFormat
 *
 * @ORM\Table(name="traineeship_format")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\TraineeshipFormatRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class TraineeshipFormat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="format_name", type="string", length=255, nullable=true)
     */
    private $formatName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->formatName;
    }

    /**
     * Set formatName
     *
     * @param string $formatName
     *
     * @return TraineeshipFormat
     */
    public function setFormatName($formatName)
    {
        $this->formatName = $formatName;

        return $this;
    }

    /**
     * Get formatName
     *
     * @return string
     */
    public function getFormatName()
    {
        return $this->formatName;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return TraineeshipFormat
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
