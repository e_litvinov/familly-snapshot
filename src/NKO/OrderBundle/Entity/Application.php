<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;

/**
 * Application
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ApplicationRepository")
 */
class Application extends BaseApplication implements PurposeInterface
{
    use OrganizationInfoTrait;
    use BudgetAdditionalTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={"KNS-2016"}
     * )
     *
     * @ORM\Column(name="link_to_annual_report", type="text", nullable=true)
     */
    private $linkToAnnualReport;

    /**
     * @ORM\OneToMany(targetEntity="Publication", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $publications;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="projectPurpose", type="text", nullable=true)
     */
    private $projectPurpose;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="projectRelevance", type="text", nullable=true)
     */
    private $projectRelevance;

    /**
     * @ORM\ManyToMany(targetEntity="BeneficiaryGroup",  cascade={"persist"})
     * @ORM\JoinTable(name="application_beneficiary_group",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="beneficiary_group", referencedColumnName="id")}
     *      )
     */
    private $beneficiaryGroups;

    /**
     * @var string
     *
     * @ORM\Column(name="beneficiary_group_name", type="string", length=150, nullable=true)
     */
    private $beneficiaryGroupName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="projectImplementation", type="text", nullable=true)
     */
    private $projectImplementation;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="TraineeshipTopic", cascade={"persist"})
     *
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     *
     */
    private $traineeshipTopic;

    /**
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="Organization", cascade={"persist"})
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     *
     */
    private $organization;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="dateStartOfInternship", type="datetime", nullable=true)
     */
    private $dateStartOfInternship;

    /**
     * @ORM\ManyToMany(targetEntity="TraineeshipFormat", cascade={"persist"})
     * @ORM\JoinTable(name="application_traineeship_format",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="traineeship_format_id", referencedColumnName="id")}
     *      )
     */
    private $traineeshipFormats;


    /**
     * @var string
     *
     * @ORM\Column(name="traineeship_format_name", type="string", length=200, nullable=true)
     */
    private $traineeshipFormatName;

    /**
     * @ORM\OneToMany(targetEntity="Employee", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $employees;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="knowledge_introduction_during_project_implementation", type="text", nullable=true)
     */
    private $knowledgeIntroductionDuringProjectImplementation;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="knowledge_introduction_after_project_implementation", type="text", nullable=true)
     */
    private $knowledgeIntroductionAfterProjectImplementation;

    /**
     * @ORM\OneToMany(targetEntity="Traineeship", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $traineeships;

    /**
     * @ORM\OneToMany(targetEntity="Measure", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $measures;

    /**
     * @ORM\OneToMany(targetEntity="ProjectResult", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $projectResults;

    /**
     * @var float
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     * @Assert\Range(
     *      min = 10000,
     *      max = 100000,
     *      minMessage = "minRequiredMoneyMessage",
     *      maxMessage = "maxRequiredMoneyMessage"
     *     )
     *
     * @ORM\Column(name="required_money", type="float", nullable=true)
     */
    private $requiredMoney;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"KNS-2016"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="organization_creation_resolution", type="string", nullable=true)
     */
    private $organizationCreationResolution;

    /**
     * @var string
     *
     * @ApplicationAssert\ExcelExtension(
     *     groups={"KNS-2016"}
     *     )
     * @Assert\File(
     *     maxSize = "20480k",
     *     mimeTypesMessage = "Please upload a valid excel file",
     *     maxSizeMessage = "file must not exceed 20MB",
     *     notFoundMessage = "file not found",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="budget", type="string", nullable=true)
     */
    private $budget;

    private $_budget;

    public function __construct()
    {
        parent::__construct();

        $this->isAddressesEqual = true;
        $this->refreshProjectResults();
    }

    public function refreshProjectResults()
    {
        $projectResult1 = new ProjectResult();
        $projectResult1->setResultCriteria('Число обученных сотрудников организации / членов общественного объединения (до 15.12.2016)');
        $projectResult2 = new ProjectResult();
        $projectResult2->setResultCriteria('Число сотрудников, организации / членов общественного объединения, которые получат новые знания благодаря реализации проекта (на 01.06.2017)');
        $projectResult3 = new ProjectResult();
        $projectResult3->setResultCriteria('Число внедренных практик (технологий, услуг, моделей и пр.) в деятельность организации благодаря реализации проекта');
        $this->setProjectResults(array($projectResult1, $projectResult2, $projectResult3));
    }

    public function getPurpose()
    {
        return $this->getProjectPurpose();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function addProject(Project $project)
    {
        $project->setApplication($this);
        $this->projects->add($project);
    }

    public function setProjects($projects)
    {
        $this->projects = new ArrayCollection();
        if (count($projects) > 0) {
            foreach ($projects as $project) {
                $this->addProject($project);
            }
        }

        return $this;
    }

    public function setSiteLinks($siteLinks)
    {
        $this->siteLinks = new ArrayCollection();
        if (count($siteLinks) > 0) {
            foreach ($siteLinks as $siteLink) {
                $this->addSiteLink($siteLink);
            }
        }

        return $this;
    }

    public function setSocialNetworkLinks($socialNetworkLinks)
    {
        $this->socialNetworkLinks = new ArrayCollection();
        if (count($socialNetworkLinks) > 0) {
            foreach ($socialNetworkLinks as $socialNetworkLink) {
                $this->addSocialNetworkLink($socialNetworkLink);
            }
        }

        return $this;
    }

    public function setBeneficiaryResult($beneficiaryResults)
    {
        $this->beneficiaryResults = new ArrayCollection();
        if (count($beneficiaryResults) > 0) {
            foreach ($beneficiaryResults as $beneficiaryResult) {
                $this->addBeneficiaryResult($beneficiaryResult);
            }
        }

        return $this;
    }

    public function setEmployeeResults($employeeResults)
    {
        $this->employeeResults = new ArrayCollection();
        if (count($employeeResults) > 0) {
            foreach ($employeeResults as $employeeResult) {
                $this->addEmployeeResult($employeeResult);
            }
        }

        return $this;
    }

    public function setRisks($risks)
    {
        $this->risks = new ArrayCollection();
        if (count($risks) > 0) {
            foreach ($risks as $risk) {
                $this->addRisk($risk);
            }
        }

        return $this;
    }

    /**
     * Set linkToAnnualReport
     *
     * @param string $linkToAnnualReport
     *
     * @return Application
     */
    public function setLinkToAnnualReport($linkToAnnualReport)
    {
        $this->linkToAnnualReport = $linkToAnnualReport;

        return $this;
    }

    /**
     * Get linkToAnnualReport
     *
     * @return string
     */
    public function getLinkToAnnualReport()
    {
        return $this->linkToAnnualReport;
    }

    public function addPublication(Publication $publication)
    {
        $publication->setApplication($this);
        $this->publications->add($publication);
    }

    public function removePublication(Publication $publication)
    {
        $this->publications->removeElement($publication);
    }


    public function getPublications()
    {
        return $this->publications;
    }

    public function setPublications($publications)
    {
        $this->publications = new ArrayCollection();
        if (count($publications) > 0) {
            foreach ($publications as $publication) {
                $this->addPublication($publication);
            }
        }
        return $this;
    }

    /**
     * Set projectPurpose
     *
     * @param string $projectPurpose
     *
     * @return Application
     */
    public function setProjectPurpose($projectPurpose)
    {
        $this->projectPurpose = $projectPurpose;

        return $this;
    }

    /**
     * Get projectPurpose
     *
     * @return string
     */
    public function getProjectPurpose()
    {
        return $this->projectPurpose;
    }

    /**
     * Set projectRelevance
     *
     * @param string $projectRelevance
     *
     * @return Application
     */
    public function setProjectRelevance($projectRelevance)
    {
        $this->projectRelevance = $projectRelevance;

        return $this;
    }

    /**
     * Get projectRelevance
     *
     * @return string
     */
    public function getProjectRelevance()
    {
        return $this->projectRelevance;
    }

    public function addBeneficiaryGroup(BeneficiaryGroup $beneficiaryGroup)
    {
        $this->beneficiaryGroups->add($beneficiaryGroup);
    }

    public function removeBeneficiaryGroup(BeneficiaryGroup $beneficiaryGroup)
    {
        $this->beneficiaryGroups->removeElement($beneficiaryGroup);
    }

    public function getBeneficiaryGroups()
    {
        return $this->beneficiaryGroups;
    }

    public function setBeneficiaryGroups($beneficiaryGroups)
    {
        if (count($beneficiaryGroups) > 0) {
            foreach ($beneficiaryGroups as $beneficiaryGroup) {
                $this->addBeneficiaryGroup($beneficiaryGroup);
            }
        }
        return $this;
    }

    /**
     * Set projectImplementation
     *
     * @param string $projectImplementation
     *
     * @return Application
     */
    public function setProjectImplementation($projectImplementation)
    {
        $this->projectImplementation = $projectImplementation;

        return $this;
    }

    /**
     * Get projectImplementation
     *
     * @return string
     */
    public function getProjectImplementation()
    {
        return $this->projectImplementation;
    }

    /**
     * @param \NKO\OrderBundle\Entity\TraineeshipTopic $traineeshipTopic
     * @return $this
     */
    public function setTraineeshipTopic($traineeshipTopic)
    {
        $this->traineeshipTopic= $traineeshipTopic;

        return $this;
    }

    public function getTraineeshipTopic()
    {
        return $this->traineeshipTopic;
    }

    public function addEmployee(Employee $employee)
    {
        $employee->setApplication($this);
        $this->employees->add($employee);
    }

    public function removeEmployee(Employee $employee)
    {
        $this->employees->removeElement($employee);
    }

    public function getEmployees()
    {
        return $this->employees;
    }

    public function setEmployees($employees)
    {
        $this->employees = new ArrayCollection();
        if (count($employees) > 0) {
            foreach ($employees as $employee) {
                $this->addEmployee($employee);
            }
        }
        return $this;
    }

    /**
     * Set organization
     *
     * @param \NKO\OrderBundle\Entity\Organization $organization
     *
     * @return Application
     */
    public function setOrganization(Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \NKO\OrderBundle\Entity\Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set dateStartOfInternship
     *
     * @param \DateTime $dateStartOfInternship
     *
     * @return Application
     */
    public function setDateStartOfInternship($dateStartOfInternship)
    {
        $this->dateStartOfInternship = $dateStartOfInternship;

        return $this;
    }

    /**
     * Get dateStartOfInternship
     *
     * @return \DateTime
     */
    public function getDateStartOfInternship()
    {
        return $this->dateStartOfInternship;
    }

    public function addTraineeshipFormat(TraineeshipFormat $format)
    {
        $this->traineeshipFormats->add($format);
    }

    public function removeTraineeshipFormat(TraineeshipFormat $format)
    {
        $this->traineeshipFormats->removeElement($format);
    }

    public function getTraineeshipFormats()
    {
        return $this->traineeshipFormats;
    }

    public function setTraineeshipFormats($formats)
    {
        if (count($formats) > 0) {
            foreach ($formats as $format) {
                $this->addTraineeshipFormat($format);
            }
        }
        return $this;
    }

    /**
     * Set knowledgeIntroductionDuringProjectImplementation
     *
     * @param string $knowledgeIntroductionDuringProjectImplementation
     *
     * @return Application
     */
    public function setKnowledgeIntroductionDuringProjectImplementation($knowledgeIntroductionDuringProjectImplementation)
    {
        $this->knowledgeIntroductionDuringProjectImplementation = $knowledgeIntroductionDuringProjectImplementation;

        return $this;
    }

    /**
     * Get knowledgeIntroductionDuringProjectImplementation
     *
     * @return string
     */
    public function getKnowledgeIntroductionDuringProjectImplementation()
    {
        return $this->knowledgeIntroductionDuringProjectImplementation;
    }

    /**
     * Set knowledgeIntroductionAfterProjectImplementation
     *
     * @param string $knowledgeIntroductionAfterProjectImplementation
     *
     * @return Application
     */
    public function setKnowledgeIntroductionAfterProjectImplementation($knowledgeIntroductionAfterProjectImplementation)
    {
        $this->knowledgeIntroductionAfterProjectImplementation = $knowledgeIntroductionAfterProjectImplementation;

        return $this;
    }

    /**
     * Get knowledgeIntroductionAfterProjectImplementation
     *
     * @return string
     */
    public function getKnowledgeIntroductionAfterProjectImplementation()
    {
        return $this->knowledgeIntroductionAfterProjectImplementation;
    }

    public function addTraineeship(Traineeship $traineeship)
    {
        $traineeship->setApplication($this);
        $this->traineeships->add($traineeship);
    }

    public function removeTraineeship(Traineeship $traineeship)
    {
        $this->traineeships->removeElement($traineeship);
    }


    public function getTraineeships()
    {
        return $this->traineeships;
    }

    public function setTraineeships($traineeships)
    {
        $this->traineeships = new ArrayCollection();
        if (count($traineeships) > 0) {
            foreach ($traineeships as $traineeship) {
                $this->addTraineeship($traineeship);
            }
        }
        return $this;
    }

    public function addMeasure(Measure $measure)
    {
        $measure->setApplication($this);
        $this->measures->add($measure);
    }

    public function removeMeasure(Measure $measure)
    {
        $this->measures->removeElement($measure);
    }


    public function getMeasures()
    {
        return $this->measures;
    }

    public function setMeasures($measures)
    {
        $this->measures = new ArrayCollection();
        if (count($measures) > 0) {
            foreach ($measures as $measure) {
                $this->addMeasure($measure);
            }
        }
        return $this;
    }

    public function addProjectResult(ProjectResult $projectResult)
    {
        $projectResult->setApplication($this);
        $this->projectResults->add($projectResult);
    }

    public function removeProjectResult(ProjectResult $projectResult)
    {
        $this->projectResults->removeElement($projectResult);
    }

    public function getProjectResults()
    {
        return $this->projectResults;
    }

    public function setProjectResults($projectResults)
    {
        $this->projectResults = new ArrayCollection();
        if (count($projectResults) > 0) {
            foreach ($projectResults as $projectResult) {
                $this->addProjectResult($projectResult);
            }
        }
        return $this;
    }

    public function __toString()
    {
        if (!$this->getName()) {
            return 'Корректирование заявки';
        }

        return $this->getName();
    }

    /**
     * Set beneficiaryGroupName
     *
     * @param string $beneficiaryGroupName
     *
     * @return Application
     */
    public function setBeneficiaryGroupName($beneficiaryGroupName)
    {
        $this->beneficiaryGroupName = $beneficiaryGroupName;

        return $this;
    }

    /**
     * Get beneficiaryGroupName
     *
     * @return string
     */
    public function getBeneficiaryGroupName()
    {
        return $this->beneficiaryGroupName;
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016"}
     *     )
     */
    public function isValidBeneficiaryGroup(ExecutionContextInterface $context, $payload)
    {
        if (count($this->getBeneficiaryGroups())==0 && !$this->getBeneficiaryGroupName()) {
            $context->buildViolation('please, choose beneficiary group')
                ->atPath('beneficiaryGroups')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016"}
     *     )
     */
    public function isValidTraineeship(ExecutionContextInterface $context, $payload)
    {
        if (count($this->getTraineeshipFormats())==0 && !$this->getTraineeshipFormatName()) {
            $context->buildViolation('please, choose traineeship format')
                ->atPath('traineeshipFormats')
                ->addViolation();
        }
    }

    /**
     * Set traineeshipFormatName
     *
     * @param string $traineeshipFormatName
     *
     * @return Application
     */
    public function setTraineeshipFormatName($traineeshipFormatName)
    {
        $this->traineeshipFormatName = $traineeshipFormatName;

        return $this;
    }

    /**
     * Get traineeshipFormatName
     *
     * @return string
     */
    public function getTraineeshipFormatName()
    {
        return $this->traineeshipFormatName;
    }


    /**
     * Set organizationCreationResolution
     *
     * @param string $organizationCreationResolution
     *
     * @return Application
     */
    public function setOrganizationCreationResolution($organizationCreationResolution)
    {
        $this->organizationCreationResolution = $organizationCreationResolution;

        return $this;
    }

    /**
     * Get organizationCreationResolution
     *
     * @return string
     */
    public function getOrganizationCreationResolution()
    {
        return $this->organizationCreationResolution;
    }

    /**
     * Set budget
     *
     * @param string $budget
     *
     * @return Application
     */
    public function setBudget($budget)
    {
        if (!$this->_budget && is_string($this->budget)) {
            $this->_budget = $this->budget;
        }
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016"}
     *     )
     */
    public function isValidBudget(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getBudget() && !$this->_budget) {
            $context->buildViolation('please, upload budget')
                ->atPath('budget')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2016"}
     *     )
     */
    public function isValidOKTMOLength(ExecutionContextInterface $context, $payload)
    {
        if (strlen($this->oKTMO) == 9 || strlen($this->oKTMO) == 10) {
            $context->buildViolation('OKTMO can not be 9 or 10 characters long')
                ->atPath('oKTMO')
                ->addViolation();
        }
    }
    

    /**
     * Set requiredMoney
     *
     * @param float $requiredMoney
     *
     * @return Application
     */
    public function setRequiredMoney($requiredMoney)
    {
        $this->requiredMoney = $requiredMoney;

        return $this;
    }

    /**
     * Get requiredMoney
     *
     * @return float
     */
    public function getRequiredMoney()
    {
        return $this->requiredMoney;
    }
}
