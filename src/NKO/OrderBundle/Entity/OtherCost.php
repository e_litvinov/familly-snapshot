<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 31.10.16
 * Time: 17:55
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Costs
 *
 * @ORM\Table(name="other_cost")
 * @ORM\Entity()
 */
class OtherCost
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     * )
     * @ORM\Column(name="name", nullable=true)
     */
    private $name;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     * )
     *
     * @Assert\Regex("/^\d+(\.\d{1,2})?$/",
     *
     *     message="2 number after digital point")
     *
     * @ORM\Column(name="sum", type="decimal", scale=2, nullable=true)
     */
    private $sum;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     * )
     * @ORM\Column(name="proof_product_cost_document", type="text", nullable=true)
     */
    private $proofProductCostDocument;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     * )
     * @ORM\Column(name="proof_paid_goods_document", type="text", nullable=true)
     */
    private $proofPaidGoodsDocument;

    /**
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="otherCosts")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $report;

    /**
     * @ORM\Column(name="cost_key", type="string", nullable=true)
     */
    private $cost_key;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OtherCost
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sum
     *
     * @param float $sum
     *
     * @return OtherCost
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set proofProductCostDocument
     *
     * @param string $proofProductCostDocument
     *
     * @return OtherCost
     */
    public function setProofProductCostDocument($proofProductCostDocument)
    {
        $this->proofProductCostDocument = $proofProductCostDocument;

        return $this;
    }

    /**
     * Get proofProductCostDocument
     *
     * @return string
     */
    public function getProofProductCostDocument()
    {
        return $this->proofProductCostDocument;
    }

    /**
     * Set proofPaidGoodsDocument
     *
     * @param string $proofPaidGoodsDocument
     *
     * @return OtherCost
     */
    public function setProofPaidGoodsDocument($proofPaidGoodsDocument)
    {
        $this->proofPaidGoodsDocument = $proofPaidGoodsDocument;

        return $this;
    }

    /**
     * Get proofPaidGoodsDocument
     *
     * @return string
     */
    public function getProofPaidGoodsDocument()
    {
        return $this->proofPaidGoodsDocument;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\Report $report
     *
     * @return OtherCost
     */
    public function setReport(\NKO\OrderBundle\Entity\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set costKey
     *
     * @param string $costKey
     *
     * @return OtherCost
     */
    public function setCostKey($costKey)
    {
        $this->cost_key = $costKey;

        return $this;
    }

    /**
     * Get costKey
     *
     * @return string
     */
    public function getCostKey()
    {
        return $this->cost_key;
    }
}
