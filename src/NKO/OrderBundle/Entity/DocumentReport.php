<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportDocumentTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DocumentReport
 *
 * @ORM\Table(name="document_report")
 * @ORM\Entity()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class DocumentReport
{
    use FinanceReportDocumentTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="documents", fetch="EAGER")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    protected $folder;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="DocumentType", inversedBy="documents", fetch="EAGER")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $document_type;

    /**
     * @var int
     * @ORM\Column(name="table_type", type="integer", nullable=true)
     */
    protected $tableType;

    /**
     * @var int
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport"}
     *     )
     * @ORM\Column(name="row_value", type="integer", nullable=true)
     */
    protected $row;

    public function __toString()
    {
        return (string) $this->report;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tableType
     *
     * @param integer $tableType
     *
     * @return DocumentReport
     */
    public function setTableType($tableType)
    {
        $this->tableType = $tableType;

        return $this;
    }

    /**
     * Get tableType
     *
     * @return integer
     */
    public function getTableType()
    {
        return $this->tableType;
    }

    /**
     * Set row
     *
     * @param integer $row
     *
     * @return DocumentReport
     */
    public function setRow($row)
    {
        $this->row = $row;

        return $this;
    }

    /**
     * Get row
     *
     * @return integer
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set folder
     *
     * @param \NKO\OrderBundle\Entity\Folder $folder
     *
     * @return DocumentReport
     */
    public function setFolder(\NKO\OrderBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return \NKO\OrderBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Set documentType
     *
     * @param \NKO\OrderBundle\Entity\DocumentType $documentType
     *
     * @return DocumentReport
     */
    public function setDocumentType(\NKO\OrderBundle\Entity\DocumentType $documentType = null)
    {
        $this->document_type = $documentType;

        return $this;
    }

    /**
     * Get documentType
     *
     * @return \NKO\OrderBundle\Entity\DocumentType
     */
    public function getDocumentType()
    {
        return $this->document_type;
    }
}
