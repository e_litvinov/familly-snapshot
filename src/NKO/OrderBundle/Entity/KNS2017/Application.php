<?php

namespace NKO\OrderBundle\Entity\KNS2017;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;

/**
 * Application
 *
 * @ORM\Table(name="kns2017_application")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\KNS2017\ApplicationRepository")
 */
class Application extends BaseApplication
{
    use KnsApplicationTrait;
    use OrganizationInfoTrait;
    use BudgetAdditionalTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BeneficiaryGroup",  cascade={"persist"})
     * @ORM\JoinTable(
     *     name="kns2017_application_beneficiary_group",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="beneficiary_group", referencedColumnName="id")}
     *     )
     */
    protected $beneficiaryGroups;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\TraineeshipFormat", cascade={"persist"})
     * @ORM\JoinTable(name="kns2017_application_traineeship_format",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="traineeship_format_id", referencedColumnName="id")}
     *      )
     *
     */
    protected $traineeshipFormats;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns2017_application_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\SocialResult",  cascade={"persist"})
     * @ORM\JoinTable(
     *     name="kns2017_application_social_result_relationship",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="social_result_id", referencedColumnName="id")}
     *     )
     *
     */
    protected $socialResults;


    public function __construct()
    {
        parent::__construct();

        $this->isAddressesEqual = true;
        $this->preCreateProjectResults();
        $this->preCreateInstances();
    }
}
