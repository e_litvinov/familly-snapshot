<?php

namespace NKO\OrderBundle\Entity\KNS2017;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\ParentInterface;

/**
 * TrainingGround
 *
 * @ORM\Table(name="kns2017_training_ground")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\KNS2017\TrainingGroundRepository")
 */
class TrainingGround implements ParentInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=500, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingGround", fetch="EAGER")
     * @ORM\JoinColumn(fieldName="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $competition;

    /**
     * @var int
     *
     * @ORM\Column(name="static_id", type="integer", nullable=false)
     */
    protected $staticId;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TrainingGround
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set parent
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $parent
     *
     * @return TrainingGround
     */
    public function setParent(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \NKO\OrderBundle\Entity\KNS2017\TrainingGround
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function __toString()
    {
        return $this->name ? (string)$this->name : '';
    }

    /**
     * Set competition
     *
     * @param string $competition
     *
     * @return TrainingGround
     */
    public function setCompetition($competition)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return string
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Set staticId
     *
     * @param integer $staticId
     *
     * @return TrainingGround
     */
    public function setStaticId($staticId)
    {
        $this->staticId = $staticId;

        return $this;
    }

    /**
     * Get staticId
     *
     * @return integer
     */
    public function getStaticId()
    {
        return $this->staticId;
    }
}
