<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 8/11/17
 * Time: 4:29 PM
 */

namespace NKO\OrderBundle\Entity\KNS2017;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Traits\BudgetAdditionalTrait;
use NKO\OrderBundle\Traits\KnsApplicationTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;

/**
 * Application
 *
 * @ORM\Table(name="kns2017_2_application")
 * @ORM\Entity(repositoryClass="")
 */
class SecondStageApplication extends BaseApplication
{
    use KnsApplicationTrait;
    use OrganizationInfoTrait;
    use BudgetAdditionalTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BeneficiaryGroup",  cascade={"persist"})
     * @ORM\JoinTable(
     *     name="kns2017_2_application_beneficiary_group",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="beneficiary_group", referencedColumnName="id")}
     *     )
     */
    protected $beneficiaryGroups;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\TraineeshipFormat", cascade={"persist"})
     * @ORM\JoinTable(name="kns2017_2_application_traineeship_format",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="traineeship_format_id", referencedColumnName="id")}
     *      )
     *
     */
    protected $traineeshipFormats;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\TrainingGround")
     * @ORM\JoinTable(name="kns2017_2_application_training_ground_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_ground_id", referencedColumnName="id")}
     *      )
     */
    protected $trainingGrounds;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\KNS2017\SocialResult",  cascade={"persist"})
     * @ORM\JoinTable(
     *     name="kns2017_2_application_social_result_relationship",
     *     joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="social_result_id", referencedColumnName="id")}
     *     )
     *
     */
    protected $socialResults;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={"KNS-2017"}
     *     )
     * @Assert\Length(
     *     min = 8,
     *     minMessage = "OKTMO must be at least 8 characters long",
     *     groups={"KNS-2017"}
     *     )
     *
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    protected $oktmo2;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={"KNS-2017"}
     *     )
     * @Assert\Length(
     *     min = 20,
     *     minMessage = "KBK must be at least 20 characters long",
     *     groups={"KNS-2017"}
     *     )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $kbk2;

    /**
     * @var string
     *
     * @Assert\Length(
     *     max = 255,
     *     minMessage = "Name must be less than 255 characters",
     *     groups={"KNS-2017"}
     *     )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $recipientName;


    public function __construct()
    {
        parent::__construct();

        $this->projects = new ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->beneficiaryGroups = new ArrayCollection();
        $this->traineeships = new ArrayCollection();
        $this->measures = new ArrayCollection();
        $this->projectResults = new ArrayCollection();
        $this->traineeshipFormats = new ArrayCollection();
        $this->trainingGrounds = new ArrayCollection();
        $this->socialResults = new ArrayCollection();
        $this->siteLinks = new ArrayCollection();
        $this->socialNetworkLinks = new ArrayCollection();
        $this->isAddressesEqual = true;
        $this->preCreateProjectResults();
        $this->preCreateInstances();
    }

    public function __toString()
    {
        return (string)$this->getCompetition()->getName();
    }

    /**
     * Set oktmo2
     *
     * @param string $oktmo2
     *
     * @return SecondStageApplication
     */
    public function setOktmo2($oktmo2)
    {
        $this->oktmo2 = $oktmo2;

        return $this;
    }

    /**
     * Get oktmo2
     *
     * @return string
     */
    public function getOktmo2()
    {
        return $this->oktmo2;
    }

    /**
     * Set kBK
     *
     * @param string $kbk2
     *
     * @return SecondStageApplication
     */
    public function setKbk2($kbk2)
    {
        $this->kbk2 = $kbk2;

        return $this;
    }

    /**
     * Get kbk
     *
     * @return string
     */
    public function getKbk2()
    {
        return $this->kbk2;
    }

    /**
     * @param $recipientName
     * @return $this
     */
    public function setRecipientName($recipientName)
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    /**
     * @return string
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }
}
