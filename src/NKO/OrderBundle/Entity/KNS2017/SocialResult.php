<?php

namespace NKO\OrderBundle\Entity\KNS2017;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocialResult
 *
 * @ORM\Table(name="kns2017_social_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\KNS2017\SocialResultRepository")
 */
class SocialResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="application_type", type="json_array", nullable=false)
     */
    private $applicationType;

    /**
     * @var int
     *
     * @ORM\Column(name="static_id", type="integer", nullable=false)
     */
    protected $staticId;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SocialResult
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name ? (string)$this->name : '';
    }

    /**
     * Set applicationType
     *
     * @param string $applicationType
     *
     * @return SocialResult
     */
    public function setApplicationType($applicationType)
    {
        $this->applicationType = $applicationType;

        return $this;
    }

    /**
     * Get applicationType
     *
     * @return string
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }

    /**
     * Set staticId
     *
     * @param integer $staticId
     *
     * @return SocialResult
     */
    public function setStaticId($staticId)
    {
        $this->staticId = $staticId;

        return $this;
    }

    /**
     * Get staticId
     *
     * @return integer
     */
    public function getStaticId()
    {
        return $this->staticId;
    }
}
