<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Employee
 *
 * @ORM\Table(name="employee")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\EmployeeRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Employee
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="position", type="text", nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="experience_and_education", type="text", nullable=true)
     */
    private $experienceAndEducation;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="educational_activity", type="text", nullable=true)
     */
    private $educationalActivity;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="employees", cascade={"persist"})
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication",
     *     cascade={"persist"}, inversedBy="employees")
     * @ORM\JoinColumn(name="kns2017_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $KNS2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set $application
     *
     * @param \NKO\OrderBundle\Entity\Application $application
     *
     * @return Employee
     */
    public function setApplication(Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get $application
     *
     * @return \NKO\OrderBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     *
     * @return Employee
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Employee
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set experienceAndEducation
     *
     * @param string $experienceAndEducation
     *
     * @return Employee
     */
    public function setExperienceAndEducation($experienceAndEducation)
    {
        $this->experienceAndEducation = $experienceAndEducation;

        return $this;
    }

    /**
     * Get experienceAndEducation
     *
     * @return string
     */
    public function getExperienceAndEducation()
    {
        return $this->experienceAndEducation;
    }

    /**
     * Set educationalActivity
     *
     * @param string $educationalActivity
     *
     * @return Employee
     */
    public function setEducationalActivity($educationalActivity)
    {
        $this->educationalActivity = $educationalActivity;

        return $this;
    }

    /**
     * Get educationalActivity
     *
     * @return string
     */
    public function getEducationalActivity()
    {
        return $this->educationalActivity;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Employee
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set kNS2017Application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $kNS2017Application
     *
     * @return Employee
     */
    public function setKNS2017Application(\NKO\OrderBundle\Entity\BaseApplication $kNS2017Application = null)
    {
        $this->KNS2017Application = $kNS2017Application;

        return $this;
    }

    /**
     * Get kNS2017Application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getKNS2017Application()
    {
        return $this->KNS2017Application;
    }
}
