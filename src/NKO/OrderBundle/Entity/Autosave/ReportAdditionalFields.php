<?php

namespace NKO\OrderBundle\Entity\Autosave;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="report_additional_fields")
 */
class ReportAdditionalFields
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\ReportHistory")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $reportHistory;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_autosaved", type="boolean", nullable=true)
     */
    protected $isAutosaved;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAutosaved
     *
     * @param boolean $isAutosaved
     *
     * @return ReportAdditionalFields
     */
    public function setIsAutosaved($isAutosaved)
    {
        $this->isAutosaved = $isAutosaved;

        return $this;
    }

    /**
     * Get isAutosaved
     *
     * @return boolean
     */
    public function getIsAutosaved()
    {
        return $this->isAutosaved;
    }

    /**
     * Set reportHistory
     *
     * @param \NKO\OrderBundle\Entity\ReportHistory $reportHistory
     *
     * @return ReportAdditionalFields
     */
    public function setReportHistory(\NKO\OrderBundle\Entity\ReportHistory $reportHistory = null)
    {
        $this->reportHistory = $reportHistory;

        return $this;
    }

    /**
     * Get reportHistory
     *
     * @return \NKO\OrderBundle\Entity\ReportHistory
     */
    public function getReportHistory()
    {
        return $this->reportHistory;
    }
}
