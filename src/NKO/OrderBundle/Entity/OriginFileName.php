<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\OriginFileNameRepository")
 */
class OriginFileName
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $originName;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $originExtension;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $autoName;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $autoExtension;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getOriginName(): ?string
    {
        return $this->originName;
    }

    /**
     * @param null|string $originName
     */
    public function setOriginName(?string $originName)
    {
        $this->originName = $originName;
    }

    /**
     * @return null|string
     */
    public function getOriginExtension(): ?string
    {
        return $this->originExtension;
    }

    /**
     * @param null|string $originExtension
     */
    public function setOriginExtension(?string $originExtension)
    {
        $this->originExtension = $originExtension;
    }

    /**
     * @return null|string
     */
    public function getAutoName(): ?string
    {
        return $this->autoName;
    }

    /**
     * @param null|string $autoName
     */
    public function setAutoName(?string $autoName)
    {
        $this->autoName = $autoName;
    }

    /**
     * @return null|string
     */
    public function getAutoExtension(): ?string
    {
        return $this->autoExtension;
    }

    /**
     * @param null|string $autoExtension
     */
    public function setAutoExtension(?string $autoExtension)
    {
        $this->autoExtension = $autoExtension;
    }
}
