<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Traineeship
 *
 * @ORM\Table(name="traineeship")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\TraineeshipRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Traineeship
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="action", type="text", nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="dead_line", type="string", length=255, nullable=true)
     */
    private $deadline;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @ORM\Column(name="action_location", type="text", nullable=true)
     */
    private $actionLocation;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="traineeships")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication",
     *     cascade={"persist"}, inversedBy="traineeships")
     * @ORM\JoinColumn(name="kns2017_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $KNS2017Application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Application application
     *
     * @return Traineeship
     */
    public function setApplication(Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get $application
     *
     * @return \NKO\OrderBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return Traineeship
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set deadline
     *
     * @param string $deadline
     *
     * @return Traineeship
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get deadline
     *
     * @return string
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set actionLocation
     *
     * @param string $actionLocation
     *
     * @return Traineeship
     */
    public function setActionLocation($actionLocation)
    {
        $this->actionLocation = $actionLocation;

        return $this;
    }

    /**
     * Get actionLocation
     *
     * @return string
     */
    public function getActionLocation()
    {
        return $this->actionLocation;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Traineeship
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set kNS2017Application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $kNS2017Application
     *
     * @return Traineeship
     */
    public function setKNS2017Application(\NKO\OrderBundle\Entity\BaseApplication $kNS2017Application = null)
    {
        $this->KNS2017Application = $kNS2017Application;

        return $this;
    }

    /**
     * Get kNS2017Application
     *
     * @return \NKO\OrderBundle\Entity\KNS2017\Application
     */
    public function getKNS2017Application()
    {
        return $this->KNS2017Application;
    }
}
