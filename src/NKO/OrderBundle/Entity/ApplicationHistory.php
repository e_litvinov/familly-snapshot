<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\AutosaveTrait;
use NKO\OrderBundle\Traits\MarkListGetterTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Application
 *
 * @ORM\Table(name="application_history")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ApplicationHistoryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(indexes={
 *     @ORM\Index(name="is_send_index", columns={"is_send"}),
 *     @ORM\Index(name="competition_index", columns={"competition"}),
 * })
 */
class ApplicationHistory
{
    use ORMBehaviors\Timestampable\Timestampable;
    use MarkListGetterTrait;
    use AutosaveTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="text")
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="nko_name", type="text", nullable=true)
     */
    private $nko_name;


    /**
     * @var string
     *
     * @ORM\Column(name="legal_region", type="text", nullable=true)
     */
    private $legalRegion;

    /**
     * @var string
     *
     * @ORM\Column(name="psrn", type="text")
     */
    private $psrn;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_send", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isSend;

    /**
     * @ORM\ManyToOne(targetEntity="Competition")
     * @ORM\JoinColumn(name="competition", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $competition;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_appropriate", type="boolean", nullable=true, options={"default" : 1})
     */
    private $isAppropriate;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     *
     * @ORM\ManyToMany(targetEntity="NKO\UserBundle\Entity\ExpertUser", inversedBy="applications"
     *     )
     * @ORM\JoinColumn(name="experts", nullable=true)
     *
     */
    private $experts;

    /**
     * @ORM\OneToMany(targetEntity="MarkList", mappedBy="application")
     * @ORM\JoinColumn(name="application", nullable=true, onDelete="cascade")
     */
    private $markLists;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_spread", type="boolean", nullable=true, options={"default" : 0})
     */
    private $isSpread;

    /**
     * @var float
     *
     * @ORM\Column(name="average_mark", type="float", nullable=true)
     */
    private $averageMark;

    /**
     * @var string
     *
     * @Assert\Regex("/^\d+(\.\d{1,2})?$/",
     *
     *     message="2 number after digital point")
     *
     * @ORM\Column(name="sum_grant", type="string", nullable=true)
     */
    private $sumGrant;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="text", nullable=true)
     */
    private $contract;

    /**
     * @var string
     *
     * @ORM\Column( type="string", nullable=true)
     */
    private $projectName;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BaseReport", mappedBy="applicationHistory")
     */
    private $reports;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\GrantConfig", mappedBy="applicationHistory")
     */
    private $grants;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\BaseReportTemplate", mappedBy="applicationHistory")
     */
    private $reportTemplates;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BaseApplication", mappedBy="linkedApplicationHistory")
     */
    private $followingApplications;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_accepted", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isAccepted;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true,  options={"default" : 0})
     */
    private $winner;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\NKOUser", inversedBy="applicationHistories")
     * @ORM\JoinColumn(name="nko_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $author;

    public function __construct()
    {
        $this->experts = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->reportTemplates = new ArrayCollection();
    }

    public function getFinalMark()
    {
        $mark = $this->getMarkListByExpert($this->experts->first());
        return $mark ? $mark->getMarkWithCoeff() : null;
    }

    public function getFinalDecision()
    {
        $mark = $this->getMarkListByExpert($this->experts->first());
        return $mark ? $mark->getFinalDecision() : null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return ApplicationHistory
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return ApplicationHistory
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set nkoName
     *
     * @param string $nkoName
     *
     * @return ApplicationHistory
     */
    public function setNkoName($nkoName)
    {
        $this->nko_name = $nkoName;

        return $this;
    }

    /**
     * Get nkoName
     *
     * @return string
     */
    public function getNkoName()
    {
        return $this->nko_name;
    }


    /**
     * Set psrn
     *
     * @param string $psrn
     *
     * @return ApplicationHistory
     */
    public function setPsrn($psrn)
    {
        $this->psrn = $psrn;

        return $this;
    }

    /**
     * Get psrn
     *
     * @return string
     */
    public function getPsrn()
    {
        return $this->psrn;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return ApplicationHistory
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return ApplicationHistory
     */
    public function setCompetition(\NKO\OrderBundle\Entity\Competition $competition = null)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return \NKO\OrderBundle\Entity\Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Set isAppropriate
     *
     * @param boolean $isAppropriate
     *
     * @return ApplicationHistory
     */
    public function setIsAppropriate($isAppropriate)
    {
        $this->isAppropriate = $isAppropriate;

        return $this;
    }

    /**
     * Get isAppropriate
     *
     * @return boolean
     */
    public function getIsAppropriate()
    {
        return $this->isAppropriate;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return ApplicationHistory
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     *
     * @return ApplicationHistory
     */
    public function addExpert(\NKO\UserBundle\Entity\ExpertUser $expert)
    {
        $this->experts[] = $expert;

        return $this;
    }

    /**
     * Remove expert
     *
     * @param \NKO\UserBundle\Entity\ExpertUser $expert
     */
    public function removeExpert(\NKO\UserBundle\Entity\ExpertUser $expert)
    {
        $this->experts->removeElement($expert);
    }

    /**
     * Get experts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperts()
    {
        return $this->experts;
    }


    /**
     * Add markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     *
     * @return ApplicationHistory
     */
    public function addMarkList(\NKO\OrderBundle\Entity\MarkList $markList)
    {
        $this->markLists[] = $markList;

        return $this;
    }

    /**
     * Remove markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     */
    public function removeMarkList(\NKO\OrderBundle\Entity\MarkList $markList)
    {
        $this->markLists->removeElement($markList);
    }

    /**
     * Get markLists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarkLists()
    {
        return $this->markLists;
    }

    public function __toString()
    {
        return (string)$this->getNkoName();
    }

    /**
     * Set isSpread
     *
     * @param boolean $isSpread
     *
     * @return ApplicationHistory
     */
    public function setIsSpread($isSpread)
    {
        $this->isSpread = $isSpread;

        return $this;
    }

    /**
     * Get isSpread
     *
     * @return boolean
     */
    public function getIsSpread()
    {
        return $this->isSpread;
    }

    /**
     * Set legalRegion
     *
     * @param string $legalRegion
     *
     * @return ApplicationHistory
     */
    public function setLegalRegion($legalRegion)
    {
        $this->legalRegion = $legalRegion;
        return $this;
    }

    /**
     * Get legalRegion
     *
     * @return string
     */
    public function getLegalRegion()
    {
        return $this->legalRegion;
    }

    /**
     * Set averageMark
     *
     * @param float $averageMark
     *
     * @return ApplicationHistory
     */
    public function setAverageMark($averageMark)
    {
        $this->averageMark = $averageMark;
    }

    /**
     * Get averageMark
     *
     * @return float
     */
    public function getAverageMark()
    {
        return $this->averageMark;
    }

    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    public function getProjectName()
    {
        $projectName = unserialize($this->data)->getProjectName();
        if (!$projectName) {
            return $this->projectName;
        }
        return $projectName;
    }

    /**
     * Set contract
     *
     * @param string $contract
     *
     * @return ApplicationHistory
     */
    public function setContract($contract)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    

    /**
     * Set sumGrant
     *
     * @param string $sumGrant
     *
     * @return ApplicationHistory
     */
    public function setSumGrant($sumGrant)
    {
        $this->sumGrant = $sumGrant;

        return $this;
    }

    /**
     * Get sumGrant
     *
     * @return string
     */
    public function getSumGrant()
    {
        return $this->sumGrant;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ApplicationHistory
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return ApplicationHistory
     */
    public function addReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     */
    public function removeReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Add grant
     *
     * @param \NKO\OrderBundle\Entity\GrantConfig $grant
     *
     * @return ApplicationHistory
     */
    public function addGrant(\NKO\OrderBundle\Entity\GrantConfig $grant)
    {
        $this->grants[] = $grant;

        return $this;
    }

    /**
     * Remove grant
     *
     * @param \NKO\OrderBundle\Entity\GrantConfig $grant
     */
    public function removeGrant(\NKO\OrderBundle\Entity\GrantConfig $grant)
    {
        $this->grants->removeElement($grant);
    }

    /**
     * Get grants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrants()
    {
        return $this->grants;
    }

    /**
     * Add reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     *
     * @return ApplicationHistory
     */
    public function addReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplates[] = $reportTemplate;

        return $this;
    }

    /**
     * Remove reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     */
    public function removeReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplates->removeElement($reportTemplate);
    }

    /**
     * Get reportTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportTemplates()
    {
        return $this->reportTemplates;
    }

    /**
     * Add followingApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Application $followingApplication
     *
     * @return ApplicationHistory
     */
    public function addFollowingApplication(\NKO\OrderBundle\Entity\BaseApplication $followingApplication)
    {
        $this->followingApplications[] = $followingApplication;

        return $this;
    }

    /**
     * Remove followingApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Application $followingApplication
     */
    public function removeFollowingApplication(\NKO\OrderBundle\Entity\BaseApplication $followingApplication)
    {
        $this->followingApplications->removeElement($followingApplication);
    }

    /**
     * Get followingApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowingApplications()
    {
        return $this->followingApplications;
    }

    /**
     * Set isAccepted
     *
     * @param boolean $isAccepted
     *
     * @return ApplicationHistory
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return boolean
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set winner
     *
     * @param boolean $winner
     *
     * @return ApplicationHistory
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return boolean
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * Set author
     *
     * @param \NKO\UserBundle\Entity\NKOUser $author
     *
     * @return ApplicationHistory
     */
    public function setAuthor(\NKO\UserBundle\Entity\NKOUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \NKO\UserBundle\Entity\NKOUser
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
