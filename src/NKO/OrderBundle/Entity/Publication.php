<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Publication
 *
 * @ORM\Table(name="publication")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\PublicationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Publication
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2016", "BriefApplication-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="publication_name", type="text", nullable=true)
     */
    private $publicationName;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication",
     *     inversedBy="publications",
     *     cascade={"persist"})
     * @ORM\JoinColumn(name="brief_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $briefApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application",
     *     inversedBy="publications",
     *     cascade={"persist"})
     * @ORM\JoinColumn(name="brief_application2018_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $briefApplication2018;


    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\KNS\Application2018\Application",
     *     inversedBy="publications",
     *     cascade={"persist"})
     * @ORM\JoinColumn(name="kns_application2018_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $KNS2018Application;


    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication",
     *     cascade={"persist"}, inversedBy="publications")
     * @ORM\JoinColumn(name="kns2017_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $KNS2017Application;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *          "KNS-2017", "KNS-2016", "Farvater-2017", "KNS-2018", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2016", "ContinuationApplicationKNS", "KNS2019-2"
     *     }
     * )
     * @Assert\Length(
     *     min = 4,
     *     minMessage = "Your year must be at least 4 characters long",
     *     groups={
     *          "KNS-2017", "KNS-2016", "Farvater-2017", "KNS-2018", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *     }
     * )
     * @Assert\Range(
     *     min = 1960,
     *     max = 2017,
     *     minMessage = "year must be at least 1960",
     *     maxMessage = "year cannot be taller than 2017",
     *     groups={
     *         "KNS-2017", "KNS-2016", "Farvater-2017"
     *     }
     * )
     * )
     * @Assert\Range(
     *     min = 1960,
     *     minMessage = "year must be at least 1960",
     *     groups={
     *         "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="year_publication", type="string", length=4, nullable=true)
     */
    private $yearPublication;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016"}
     *     )
     *
     * @Assert\Url(
     *     message = "The url is not valid",
     *     protocols = {"http", "https"},
     *     groups={
     *     "KNS-2016", "Farvater-2017", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="link", type="text", nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="Application", inversedBy="publications")
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\Application $application
     *
     * @return Publication
     */
    public function setApplication(\NKO\OrderBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set yearPublication
     *
     * @param string $yearPublication
     *
     * @return Publication
     */
    public function setYearPublication($yearPublication)
    {
        $this->yearPublication = $yearPublication;

        return $this;
    }

    /**
     * Get yearPublication
     *
     * @return string
     */
    public function getYearPublication()
    {
        return $this->yearPublication;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Publication
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    public function __toString()
    {
        return (string)$this->getPublicationName();
    }

    /**
     * Set publicationName
     *
     * @param string $publicationName
     *
     * @return Publication
     */
    public function setPublicationName($publicationName)
    {
        $this->publicationName = $publicationName;

        return $this;
    }

    /**
     * Get publicationName
     *
     * @return string
     */
    public function getPublicationName()
    {
        return $this->publicationName;
    }

    /**
     * Set briefApplication
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $briefApplication
     *
     * @return Publication
     */
    public function setBriefApplication(\NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $briefApplication = null)
    {
        $this->briefApplication = $briefApplication;
    }

    /*
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Publication
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get briefApplication
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication
     */
    public function getBriefApplication()
    {
        return $this->briefApplication;
    }

    /*
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set kNS2017Application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $kNS2017Application
     *
     * @return Publication
     */
    public function setKNS2017Application(\NKO\OrderBundle\Entity\BaseApplication $kNS2017Application = null)
    {
        $this->KNS2017Application = $kNS2017Application;

        return $this;
    }

    /**
     * Get kNS2017Application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getKNS2017Application()
    {
        return $this->KNS2017Application;
    }

    /**
     * Set briefApplication2018
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application $briefApplication2018
     *
     * @return Publication
     */
    public function setBriefApplication2018(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application $briefApplication2018 = null)
    {
        $this->briefApplication2018 = $briefApplication2018;

        return $this;
    }

    /**
     * Get briefApplication2018
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application
     */
    public function getBriefApplication2018()
    {
        return $this->briefApplication2018;
    }

    /**
     * Set kNS2018Application
     *
     * @param \NKO\OrderBundle\Entity\Application\KNS\Application2018\Application $kNS2018Application
     *
     * @return Publication
     */
    public function setKNS2018Application(\NKO\OrderBundle\Entity\Application\KNS\Application2018\Application $kNS2018Application = null)
    {
        $this->KNS2018Application = $kNS2018Application;

        return $this;
    }

    /**
     * Get kNS2018Application
     *
     * @return \NKO\OrderBundle\Entity\Application\KNS\Application2018\Application
     */
    public function getKNS2018Application()
    {
        return $this->KNS2018Application;
    }
}
