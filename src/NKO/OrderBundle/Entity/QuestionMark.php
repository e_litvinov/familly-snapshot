<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/21/17
 * Time: 12:17 PM
 */
namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as MarkAssert;


/**
 * Mark
 * @ORM\Table(name="question_mark")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\QuestionMarkRepository")
 */
class QuestionMark
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MarkCriteria", inversedBy="questions")
     * @ORM\JoinColumn(name="criteria_id", referencedColumnName="id", nullable=FALSE, onDelete="cascade")
     */
    private $criteria;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition")
     * @ORM\JoinTable(name="questions_competitions",
     *      joinColumns={@ORM\JoinColumn(name="question_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="competition_id", referencedColumnName="id")}
     * )
     */
    private $competitions;

    /**
     * @var string
     *
     * @ORM\Column(name="questions", type="text")
     */
    private $question;

    /**
     * @var int
     *
     * @ORM\Column(name="range_mark", type="integer")
     */
    private $rangeMark;


    /**
     *
     *@ORM\OneToMany(targetEntity="Mark", mappedBy="questionMark")
     */
    private $marks;

    /**
     * @var string
     *
     * @ORM\Column( type="text", nullable=true)
     */
    private $helpField;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marks = new ArrayCollection();
        $this->competitions = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->question;

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return QuestionMark
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set rangeMark
     *
     * @param integer $rangeMark
     *
     * @return QuestionMark
     */
    public function setRangeMark($rangeMark)
    {
        $this->rangeMark = $rangeMark;

        return $this;
    }

    /**
     * Get rangeMark
     *
     * @return integer
     */
    public function getRangeMark()
    {
        return $this->rangeMark;
    }

    /**
     * Set criteria
     *
     * @param \NKO\OrderBundle\Entity\MarkCriteria $criteria
     *
     * @return QuestionMark
     */
    public function setCriteria(\NKO\OrderBundle\Entity\MarkCriteria $criteria)
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * Get criteria
     *
     * @return \NKO\OrderBundle\Entity\MarkCriteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Add mark
     *
     * @param \NKO\OrderBundle\Entity\Mark $mark
     *
     * @return QuestionMark
     */
    public function addMark(\NKO\OrderBundle\Entity\Mark $mark)
    {
        $this->marks[] = $mark;

        return $this;
    }

    /**
     * Remove mark
     *
     * @param \NKO\OrderBundle\Entity\Mark $mark
     */
    public function removeMark(\NKO\OrderBundle\Entity\Mark $mark)
    {
        $this->marks->removeElement($mark);
    }

    /**
     * Get marks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarks()
    {
        return $this->marks;
    }

    /**
     * Set helpField
     *
     * @param string $helpField
     *
     * @return QuestionMark
     */
    public function setHelpField($helpField)
    {
        $this->helpField = $helpField;

        return $this;
    }

    /**
     * Get helpField
     *
     * @return string
     */
    public function getHelpField()
    {
        return $this->helpField;
    }

    /**
     * Add competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return QuestionMark
     */
    public function addCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions[] = $competition;

        return $this;
    }

    /**
     * Remove competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     */
    public function removeCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions->removeElement($competition);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }
}
