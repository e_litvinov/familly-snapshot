<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.2.19
 * Time: 12.40
 */

namespace NKO\OrderBundle\Entity\MarkListSequence;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\MarkListSequence\SequenceRepository")
 * @ORM\Table(name="mark_list_sequence")
 */
class Sequence
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     * )
     *
     * @ORM\OneToOne(targetEntity="NKO\OrderBundle\Entity\Competition")
     * @ORM\JoinColumn(name="competition_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $competition;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition")
     * @ORM\JoinTable(name="mark_list_sequence_competition_relationship",
     *      joinColumns={@ORM\JoinColumn(name="related_competition_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="competition_id", referencedColumnName="id")}
     *      )
     */
    private $relatedCompetitions;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\MarkListSequence\Section" , cascade={"all"}, mappedBy="sequence", orphanRemoval=true)
     */
    protected $sections;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\MarkListSequence\Section" , cascade={"all"}, mappedBy="nestedSequence", orphanRemoval=true)
     */
    protected $doubleSections;

    /**
     * @ORM\Column(name="is_fill", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isFill;

    public function __construct()
    {
        $this->sections = new ArrayCollection();
        $this->doubleSections = new ArrayCollection();
    }

    public function getUsesCompetitions()
    {
        $title = $this->getCompetition();
        $relatedCompetitions = $this->getRelatedCompetitions();
        foreach ($relatedCompetitions as $item) {
            $title .= " | ". $item;
        }

        return $title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isFill
     *
     * @param boolean $isFill
     *
     * @return Sequence
     */
    public function setIsFill($isFill)
    {
        $this->isFill = $isFill;

        return $this;
    }

    /**
     * Get isFill
     *
     * @return boolean
     */
    public function getIsFill()
    {
        return $this->isFill;
    }
    

    /**
     * Add relatedCompetition
     *
     * @param \NKO\OrderBundle\Entity\Competition $relatedCompetition
     *
     * @return Sequence
     */
    public function addRelatedCompetition(\NKO\OrderBundle\Entity\Competition $relatedCompetition)
    {
        $this->relatedCompetitions[] = $relatedCompetition;

        return $this;
    }

    /**
     * Remove relatedCompetition
     *
     * @param \NKO\OrderBundle\Entity\Competition $relatedCompetition
     */
    public function removeRelatedCompetition(\NKO\OrderBundle\Entity\Competition $relatedCompetition)
    {
        $this->relatedCompetitions->removeElement($relatedCompetition);
    }

    /**
     * Get relatedCompetitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelatedCompetitions()
    {
        return $this->relatedCompetitions;
    }

    /**
     * Add section
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Section $section
     *
     * @return Sequence
     */
    public function addSection(\NKO\OrderBundle\Entity\MarkListSequence\Section $section)
    {
        $this->sections[] = $section;
        $section->setSequence($this);

        return $this;
    }

    /**
     * Remove section
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Section $section
     */
    public function removeSection(\NKO\OrderBundle\Entity\MarkListSequence\Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Add doubleSection
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Section $doubleSection
     *
     * @return Sequence
     */
    public function addDoubleSection(\NKO\OrderBundle\Entity\MarkListSequence\Section $doubleSection)
    {
        $this->doubleSections[] = $doubleSection;
        $doubleSection->setNestedSequence($this);

        return $this;
    }

    /**
     * Remove doubleSection
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Section $doubleSection
     */
    public function removeDoubleSection(\NKO\OrderBundle\Entity\MarkListSequence\Section $doubleSection)
    {
        $this->doubleSections->removeElement($doubleSection);
    }

    /**
     * Get doubleSections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDoubleSections()
    {
        return $this->doubleSections;
    }

    /**
     * Set competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return Sequence
     */
    public function setCompetition(\NKO\OrderBundle\Entity\Competition $competition = null)
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * Get competition
     *
     * @return \NKO\OrderBundle\Entity\Competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }
}
