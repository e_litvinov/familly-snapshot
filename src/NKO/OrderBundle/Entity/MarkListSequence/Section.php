<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.2.19
 * Time: 12.40
 */

namespace NKO\OrderBundle\Entity\MarkListSequence;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="mark_list_sequence_section")
 */
class Section
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\MarkCriteria")
     * @ORM\JoinColumn(name="criteria", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $criteria;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\MarkListSequence\Sequence", inversedBy="sections")
     * @ORM\JoinColumn(name="section", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $sequence;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\MarkListSequence\Sequence", inversedBy="doubleSections")
     * @ORM\JoinColumn(name="double_section", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $nestedSequence;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\MarkListSequence\SubSection", cascade={"all"}, mappedBy="section", orphanRemoval=true)
     */
    protected $subSections;

    public function __construct()
    {
        $this->subSections = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set criteria
     *
     * @param \NKO\OrderBundle\Entity\MarkCriteria $criteria
     *
     * @return Section
     */
    public function setCriteria(\NKO\OrderBundle\Entity\MarkCriteria $criteria = null)
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * Get criteria
     *
     * @return \NKO\OrderBundle\Entity\MarkCriteria
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * Set sequence
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Sequence $sequence
     *
     * @return Section
     */
    public function setSequence(\NKO\OrderBundle\Entity\MarkListSequence\Sequence $sequence = null)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return \NKO\OrderBundle\Entity\MarkListSequence\Sequence
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Add subSection
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\SubSection $subSection
     *
     * @return Section
     */
    public function addSubSection(\NKO\OrderBundle\Entity\MarkListSequence\SubSection $subSection)
    {
        $this->subSections[] = $subSection;
        $subSection->setSection($this);

        return $this;
    }

    /**
     * Remove subSection
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\SubSection $subSection
     */
    public function removeSubSection(\NKO\OrderBundle\Entity\MarkListSequence\SubSection $subSection)
    {
        $this->subSections->removeElement($subSection);
    }

    /**
     * Get subSections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubSections()
    {
        return $this->subSections;
    }

    /**
     * Set nestedSequence
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Sequence $nestedSequence
     *
     * @return Section
     */
    public function setNestedSequence(\NKO\OrderBundle\Entity\MarkListSequence\Sequence $nestedSequence = null)
    {
        $this->nestedSequence = $nestedSequence;

        return $this;
    }

    /**
     * Get nestedSequence
     *
     * @return \NKO\OrderBundle\Entity\MarkListSequence\Sequence
     */
    public function getNestedSequence()
    {
        return $this->nestedSequence;
    }
}
