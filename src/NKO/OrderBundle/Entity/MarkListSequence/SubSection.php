<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.2.19
 * Time: 12.40
 */

namespace NKO\OrderBundle\Entity\MarkListSequence;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity("")
 * @ORM\Table(name="mark_list_sequence_subsection")
 */
class SubSection
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\MarkListSequence\Section", inversedBy="subSections")
     * @ORM\JoinColumn(name="section", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $section;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\QuestionMark")
     * @ORM\JoinColumn(name="question", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $question;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set section
     *
     * @param \NKO\OrderBundle\Entity\MarkListSequence\Section $section
     *
     * @return SubSection
     */
    public function setSection(\NKO\OrderBundle\Entity\MarkListSequence\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \NKO\OrderBundle\Entity\MarkListSequence\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set question
     *
     * @param \NKO\OrderBundle\Entity\QuestionMark $question
     *
     * @return SubSection
     */
    public function setQuestion(\NKO\OrderBundle\Entity\QuestionMark $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \NKO\OrderBundle\Entity\QuestionMark
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
