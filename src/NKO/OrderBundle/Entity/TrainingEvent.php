<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/17/16
 * Time: 10:58 AM
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * TrainingEvent
 *
 * @ORM\Table(name="training_event")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity()
 */
class TrainingEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     * @ORM\Column(name="event_name", type="text", nullable=true)
     */
    private $eventName;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     * )
     * @ORM\Column(name="format", type="text", nullable=true)
     */
    private $format;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     * @ORM\Column(name="time", type="text", nullable=true)
     */
    private $time;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     * @ORM\Column(name="place", type="text", nullable=true)
     */
    private $place;

    /**
     * @var int
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="count_member", type="integer", nullable=true)
     */
    private $count_member;

    /**
     * @ORM\ManyToOne(targetEntity="BaseReport", inversedBy="events")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    private $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventName
     *
     * @param string $eventName
     *
     * @return TrainingEvent
     */
    public function setEventName($eventName)
    {
        $this->eventName = $eventName;

        return $this;
    }

    /**
     * Get eventName
     *
     * @return string
     */
    public function getEventName()
    {
        return $this->eventName;
    }

    /**
     * Set format
     *
     * @param string $format
     *
     * @return TrainingEvent
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set time
     *
     * @param string $time
     *
     * @return TrainingEvent
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return TrainingEvent
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set countMember
     *
     * @param integer $countMember
     *
     * @return TrainingEvent
     */
    public function setCountMember($countMember)
    {
        $this->count_member = $countMember;

        return $this;
    }

    /**
     * Get countMember
     *
     * @return integer
     */
    public function getCountMember()
    {
        return $this->count_member;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return TrainingEvent
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
