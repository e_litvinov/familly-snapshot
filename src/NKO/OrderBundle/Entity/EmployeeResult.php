<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EmployeeResult
 *
 * @ORM\Table(name="employee_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\EmployeeResultRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class EmployeeResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="indicator", type="text", nullable=true)
     */
    private $indicator;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value this field must be integer type",
     *     groups={"KNS-2020"}
     *     )
     *
     * @ORM\Column(name="target_value", type="text", nullable=true)
     */
    private $targetValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @Assert\Type(
     *     type="integer",
     *     message="The value this field must be integer type",
     *     groups={"KNS-2020"}
     *     )
     *
     * @ORM\Column(name="approximate_target_value", type="text", nullable=true)
     */
    private $approximateTargetValue;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2016", "KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\Column(name="method_measurement", type="text", nullable=true)
     */
    private $methodMeasurement;

    /**
     * @ORM\ManyToOne(targetEntity="BaseApplication", inversedBy="employeeResults",
     *     cascade={"persist"} )
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2019", "KNS-2020"}
     *     )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod")
     * @ORM\JoinColumn(name="measurement_method_id", referencedColumnName="id",  onDelete="SET NULL")
     */
    protected $linkedMethod;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication application
     *
     * @return EmployeeResult
     */
    public function setApplication(BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get $application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return EmployeeResult
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set indicator
     *
     * @param string $indicator
     *
     * @return EmployeeResult
     */
    public function setIndicator($indicator)
    {
        $this->indicator = $indicator;

        return $this;
    }

    /**
     * Get indicator
     *
     * @return string
     */
    public function getIndicator()
    {
        return $this->indicator;
    }

    /**
     * Set targetValue
     *
     * @param string $targetValue
     *
     * @return EmployeeResult
     */
    public function setTargetValue($targetValue)
    {
        $this->targetValue = $targetValue;

        return $this;
    }

    /**
     * Get targetValue
     *
     * @return string
     */
    public function getTargetValue()
    {
        return $this->targetValue;
    }

    /**
     * Set approximateTargetValue
     *
     * @param string $approximateTargetValue
     *
     * @return EmployeeResult
     */
    public function setApproximateTargetValue($approximateTargetValue)
    {
        $this->approximateTargetValue = $approximateTargetValue;

        return $this;
    }

    /**
     * Get approximateTargetValue
     *
     * @return string
     */
    public function getApproximateTargetValue()
    {
        return $this->approximateTargetValue;
    }

    /**
     * Set methodMeasurement
     *
     * @param string $methodMeasurement
     *
     * @return EmployeeResult
     */
    public function setMethodMeasurement($methodMeasurement)
    {
        $this->methodMeasurement = $methodMeasurement;

        return $this;
    }

    /**
     * Get methodMeasurement
     *
     * @return string
     */
    public function getMethodMeasurement()
    {
        return $this->methodMeasurement;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return EmployeeResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set linkedMethod
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $linkedMethod
     *
     * @return EmployeeResult
     */
    public function setLinkedMethod(\NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod $linkedMethod = null)
    {
        $this->linkedMethod = $linkedMethod;

        return $this;
    }

    /**
     * Get linkedMethod
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod
     */
    public function getLinkedMethod()
    {
        return $this->linkedMethod;
    }
}
