<?php

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Risk
 *
 * @ORM\Table(name="risk")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\RiskRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Risk
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2016", "KNS-2018", "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *      }
     *     )
     *
     * @ORM\Column(name="key_risk", type="text", nullable=true)
     */
    private $keyRisk;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2016", "KNS-2018", "Farvater-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *     }
     *     )
     *
     * @ORM\Column(name="action_to_reduce_risk", type="text", nullable=true)
     */
    private $actionToReduceRisk;

    /**
     * @ORM\ManyToOne(targetEntity="BaseApplication", inversedBy="risks", cascade={"persist"})
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="realizationPractices", cascade={"persist"})
     * @ORM\JoinColumn(name="condition_realization_practice_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $conditionRealizationApplication;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application", inversedBy="spreadPractices", cascade={"persist"})
     * @ORM\JoinColumn(name="condition_spread_practice_application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $conditionSpreadApplication;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication application
     *
     * @return Risk
     */
    public function setApplication(BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get $application
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyRisk
     *
     * @param string $keyRisk
     *
     * @return Risk
     */
    public function setKeyRisk($keyRisk)
    {
        $this->keyRisk = $keyRisk;

        return $this;
    }

    /**
     * Get keyRisk
     *
     * @return string
     */
    public function getKeyRisk()
    {
        return $this->keyRisk;
    }

    /**
     * Set actionToReduceRisk
     *
     * @param string $actionToReduceRisk
     *
     * @return Risk
     */
    public function setActionToReduceRisk($actionToReduceRisk)
    {
        $this->actionToReduceRisk = $actionToReduceRisk;

        return $this;
    }

    /**
     * Get actionToReduceRisk
     *
     * @return string
     */
    public function getActionToReduceRisk()
    {
        return $this->actionToReduceRisk;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * Set conditionRealizationApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $conditionRealizationApplication
     *
     * @return Risk
     */
    public function setConditionRealizationApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $conditionRealizationApplication = null)
    {
        $this->conditionRealizationApplication = $conditionRealizationApplication;

        return $this;
    }

    /**
     * Get conditionRealizationApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getConditionRealizationApplication()
    {
        return $this->conditionRealizationApplication;
    }

    /**
     * Set conditionSpreadApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $conditionSpreadApplication
     *
     * @return Risk
     */
    public function setConditionSpreadApplication(\NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application $conditionSpreadApplication = null)
    {
        $this->conditionSpreadApplication = $conditionSpreadApplication;

        return $this;
    }

    /**
     * Get conditionSpreadApplication
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application
     */
    public function getConditionSpreadApplication()
    {
        return $this->conditionSpreadApplication;
    }
}
