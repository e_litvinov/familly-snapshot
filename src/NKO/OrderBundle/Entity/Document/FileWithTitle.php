<?php

namespace NKO\OrderBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="document_with_title")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity()
 */
class FileWithTitle
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    protected $_file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }

        $this->file = $file;

        return $this;
    }

    //For Asserting
    public function getFileHelp()
    {
        return $this->_file;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
