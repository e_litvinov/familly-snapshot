<?php

namespace NKO\OrderBundle\Entity\Document;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @ORM\Table(name="document_with_fields")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Entity()
 *
 */

class FileWithFields
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $firstField;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $secondField;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $file;

    protected $_file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;


    public function getId()
    {
        return $this->id;
    }

    public function getFirstField()
    {
        return $this->firstField;
    }

    public function setFirstField($firstField)
    {
        $this->firstField = $firstField;
    }


    public function getSecondField()
    {
        return $this->secondField;
    }

    public function setSecondField($secondField)
    {
        $this->secondField = $secondField;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }

        $this->file = $file;

        return $this;
    }

    //For Asserting
    public function getFileHelp()
    {
        return $this->_file;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
