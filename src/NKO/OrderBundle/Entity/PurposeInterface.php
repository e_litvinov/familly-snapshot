<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 12/26/17
 * Time: 1:16 PM
 */

namespace NKO\OrderBundle\Entity;


interface PurposeInterface
{
    public function getPurpose();

}