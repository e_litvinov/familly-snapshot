<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/31/16
 * Time: 8:57 AM
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Traits\AutosaveTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ReportHistory
 *
 * @ORM\Table(name="report_history")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ReportHistoryRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ReportHistory
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="recipient", type="text", nullable=true)
     */
    private $recipient;

    /**
     * @var string
     *
     * @ORM\Column(name="psrn", type="text")
     */
    private $psrn;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text")
     */
    private $data;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_accepted", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isAccepted;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_send", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isSend;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_valid", type="boolean", nullable=true,  options={"default" : 0})
     */
    private $isValid;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", inversedBy="reportHistories")
     * @ORM\JoinColumn(name="report_form_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $reportForm;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\PeriodReport", inversedBy="reportHistories")
     * @ORM\JoinColumn(name="period_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $period;

    /**
     * @var string
     *
     * @ORM\Column(name="nko_name", type="string", nullable=true)
     */
    protected $nkoName;

    /**
     * @var bool
     *
     * @ORM\Column(name="under_consideration", type="boolean", nullable=true)
     */
    protected $underConsideration;

    /**
     * @var string
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipient
     *
     * @param string $recipient
     *
     * @return ReportHistory
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set psrn
     *
     * @param string $psrn
     *
     * @return ReportHistory
     */
    public function setPsrn($psrn)
    {
        $this->psrn = $psrn;

        return $this;
    }

    /**
     * Get psrn
     *
     * @return string
     */
    public function getPsrn()
    {
        return $this->psrn;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return ReportHistory
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set isAccepted
     *
     * @param boolean $isAccepted
     *
     * @return ReportHistory
     */
    public function setIsAccepted($isAccepted)
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * Get isAccepted
     *
     * @return boolean
     */
    public function getIsAccepted()
    {
        return $this->isAccepted;
    }

    /**
     * Set isSend
     *
     * @param boolean $isSend
     *
     * @return ReportHistory
     */
    public function setIsSend($isSend)
    {
        $this->isSend = $isSend;

        return $this;
    }

    /**
     * Get isSend
     *
     * @return boolean
     */
    public function getIsSend()
    {
        return $this->isSend;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return ReportHistory
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return boolean
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return ReportHistory
     */
    public function setReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm = null)
    {
        $this->reportForm = $reportForm;

        return $this;
    }

    /**
     * Get reportForm
     *
     * @return \NKO\OrderBundle\Entity\Report\ReportForm
     */
    public function getReportForm()
    {
        return $this->reportForm;
    }

    /**
     * Set nkoName
     *
     * @param string $nkoName
     *
     * @return ReportHistory
     */
    public function setNkoName($nkoName)
    {
        $this->nkoName = $nkoName;

        return $this;
    }

    /**
     * Get nkoName
     *
     * @return string
     */
    public function getNkoName()
    {
        return $this->nkoName;
    }

    /**
     * Set period
     *
     * @param \NKO\OrderBundle\Entity\PeriodReport $period
     *
     * @return ReportHistory
     */
    public function setPeriod(\NKO\OrderBundle\Entity\PeriodReport $period = null)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \NKO\OrderBundle\Entity\PeriodReport
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * Set underConsideration
     *
     * @param boolean $underConsideration
     *
     * @return ReportHistory
     */
    public function setUnderConsideration($underConsideration)
    {
        $this->underConsideration = $underConsideration;

        return $this;
    }

    /**
     * Get underConsideration
     *
     * @return boolean
     */
    public function getUnderConsideration()
    {
        return $this->underConsideration;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return ReportHistory
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
