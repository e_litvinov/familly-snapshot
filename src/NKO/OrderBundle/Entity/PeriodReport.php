<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/1/17
 * Time: 11:27 AM
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\Report\ReportForm;

/**
 * Costs
 *
 * @ORM\Table(name="period_report")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\PeriodReportRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class PeriodReport
{
    /**
     * @var int
     *
     * @ORM\Column( type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $name;

    /**
     * @var \DateTime $start_date
     * @ORM\Column(type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime $finish_date
     * @ORM\Column(type="datetime")
     */
    private $finishDate;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", mappedBy="reportPeriods")
     */
    private $reportForms;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ReportHistory", mappedBy="period")
     */
    private $reportHistories;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult", mappedBy="period")
     */
    private $periodResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument", mappedBy="period")
     */
    protected $monitoringDocuments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->competitions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->periodResults = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PeriodReport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return PeriodReport
     */
    public function addReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     */
    public function removeReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Add reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report $reportForm
     *
     * @return PeriodReport
     */
    public function addReportForm(ReportForm $reportForm)
    {
        $this->reportForms[] = $reportForm;

        return $this;
    }

    /**
     * Remove reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report $reportForm
     */
    public function removeReportForm(ReportForm $reportForm)
    {
        $this->reportForms->removeElement($reportForm);
    }

    /**
     * Get reportForms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportForms()
    {
        return $this->reportForms;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return PeriodReport
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set finishDate
     *
     * @param \DateTime $finishDate
     *
     * @return PeriodReport
     */
    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;

        return $this;
    }

    /**
     * Get finishDate
     *
     * @return \DateTime
     */
    public function getFinishDate()
    {
        return $this->finishDate;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return PeriodReport
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add reportHistory
     *
     * @param \NKO\OrderBundle\Entity\ReportHistory $reportHistory
     *
     * @return PeriodReport
     */
    public function addReportHistory(\NKO\OrderBundle\Entity\ReportHistory $reportHistory)
    {
        $this->reportHistories[] = $reportHistory;

        return $this;
    }

    /**
     * Remove reportHistory
     *
     * @param \NKO\OrderBundle\Entity\ReportHistory $reportHistory
     */
    public function removeReportHistory(\NKO\OrderBundle\Entity\ReportHistory $reportHistory)
    {
        $this->reportHistories->removeElement($reportHistory);
    }

    /**
     * Get reportHistories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportHistories()
    {
        return $this->reportHistories;
    }

    /**
     * Add periodResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult
     *
     * @return PeriodReport
     */
    public function addPeriodResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult)
    {
        $this->periodResults[] = $periodResult;

        return $this;
    }

    /**
     * Remove periodResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult
     */
    public function removePeriodResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult $periodResult)
    {
        $this->periodResults->removeElement($periodResult);
    }

    /**
     * Get periodResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeriodResults()
    {
        return $this->periodResults;
    }

    /**
     * Add monitoringDocument
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $monitoringDocument
     *
     * @return PeriodReport
     */
    public function addMonitoringDocument(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $monitoringDocument)
    {
        $this->monitoringDocuments[] = $monitoringDocument;

        return $this;
    }

    /**
     * Remove monitoringDocument
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $monitoringDocument
     */
    public function removeMonitoringDocument(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument $monitoringDocument)
    {
        $this->monitoringDocuments->removeElement($monitoringDocument);
    }

    /**
     * Get monitoringDocuments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringDocuments()
    {
        return $this->monitoringDocuments;
    }
}
