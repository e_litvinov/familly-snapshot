<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 18.10.16
 * Time: 10:32
 */

namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Costs
 *
 * @ORM\Table(name="expense")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\ExpenseRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Expense
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2018", "FinanceReport", "MixedReportKNS-2018", "FinanceReport-2019", "MixedReportKNS-2019"}
     *     )
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2018", "FinanceReport", "MixedReportKNS-2018", "FinanceReport-2019", "MixedReportKNS-2019"}
     *     )
     *
     * @Assert\Regex("/^-?\d+(\.\d{1,2})?$/",
     *      groups={"FinanceReport-2018", "FinanceReport", "MixedReportKNS-2018", "FinanceReport-2019", "MixedReportKNS-2019"},
     *     message="2 number after digital point")
     *
     * @ORM\Column(name="sum", type="decimal", scale=2, nullable=true)
     */
    private $sum;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport", "MixedReportKNS-2018", "FinanceReport-2019", "MixedReportKNS-2019"}
     *     )
     *
     * @ORM\Column(name="proof_product_expense_document", type="text", nullable=true)
     */
    private $proofProductExpenseDocument;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport", "FinanceReport-2019", "MixedReportKNS-2019"}
     *     )
     * @ORM\Column(name="proof_paid_goods_document", type="text", nullable=true)
     */
    private $proofPaidGoodsDocument;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Register", inversedBy="expenses")
     * @ORM\JoinColumn(name="register_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $register;

    /**
     * @Assert\NotNull(groups={"FinanceReport-2018"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument")
     * @ORM\JoinColumn(name="type_document_id", referencedColumnName="id", onDelete="CASCADE")
     */

    private $typeDocument;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Expense
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sum
     *
     * @param string $sum
     *
     * @return Expense
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * Get sum
     *
     * @return string
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set proofProductExpenseDocument
     *
     * @param string $proofProductExpenseDocument
     *
     * @return Expense
     */
    public function setProofProductExpenseDocument($proofProductExpenseDocument)
    {
        $this->proofProductExpenseDocument = $proofProductExpenseDocument;

        return $this;
    }

    /**
     * Get proofProductExpenseDocument
     *
     * @return string
     */
    public function getProofProductExpenseDocument()
    {
        return $this->proofProductExpenseDocument;
    }

    /**
     * Set proofPaidGoodsDocument
     *
     * @param string $proofPaidGoodsDocument
     *
     * @return Expense
     */
    public function setProofPaidGoodsDocument($proofPaidGoodsDocument)
    {
        $this->proofPaidGoodsDocument = $proofPaidGoodsDocument;

        return $this;
    }

    /**
     * Get proofPaidGoodsDocument
     *
     * @return string
     */
    public function getProofPaidGoodsDocument()
    {
        return $this->proofPaidGoodsDocument;
    }

    /**
     * Set register
     *
     * @param \NKO\OrderBundle\Entity\Register $register
     *
     * @return Expense
     */
    public function setRegister(\NKO\OrderBundle\Entity\Register $register = null)
    {
        $this->register = $register;

        return $this;
    }

    /**
     * Get register
     *
     * @return \NKO\OrderBundle\Entity\Register
     */
    public function getRegister()
    {
        return $this->register;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set typeDocument
     *
     * @param \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument $typeDocument
     *
     * @return Expense
     */
    public function setTypeDocument(\NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument $typeDocument = null)
    {
        $this->typeDocument = $typeDocument;

        return $this;
    }

    /**
     * Get typeDocument
     *
     * @return \NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\TypeDocument
     */
    public function getTypeDocument()
    {
        return $this->typeDocument;
    }
}
