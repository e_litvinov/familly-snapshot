<?php


namespace NKO\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * DocumentType
 *
 * @ORM\Table(name="document_type")
 * @ORM\Entity()
 */
class DocumentType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="document_type", type="text", nullable=true)
     */
    private $documentType;

    /**
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="document_types")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    private $folder;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\DocumentReport", mappedBy="document_type", cascade={"persist"})
     */
    private $documents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return (string)$this->getDocumentType();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentType
     *
     * @param string $documentType
     *
     * @return DocumentType
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;

        return $this;
    }

    /**
     * Get documentType
     *
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * Set folder
     *
     * @param \NKO\OrderBundle\Entity\Folder $folder
     *
     * @return DocumentType
     */
    public function setFolder(\NKO\OrderBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return \NKO\OrderBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Add document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     *
     * @return DocumentType
     */
    public function addDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $document->setDocumentType($this);
        $this->documents[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \NKO\OrderBundle\Entity\DocumentReport $document
     */
    public function removeDocument(\NKO\OrderBundle\Entity\DocumentReport $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
