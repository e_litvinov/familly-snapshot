<?php

namespace NKO\OrderBundle\Entity\BriefApplication2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Problem
 *
 * @ORM\Table(name="brief_application2017_problem")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2017\ProblemRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Problem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="targetGroup", type="string", length=255, nullable=true)
     */
    private $targetGroup;

    /**
     * @var string
     *
     * @ORM\Column(name="mainProblems", type="text", nullable=true)
     */
    private $mainProblems;

    /**
     * @var string
     *
     * @ORM\Column(name="expectedResults", type="text", nullable=true)
     */
    private $expectedResults;

    /**
     * @ORM\ManyToOne(targetEntity="BriefApplication", inversedBy="problems",
     *     cascade={"persist"})
     *
     * @Gedmo\Versioned
     *
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set targetGroup
     *
     * @param string $targetGroup
     *
     * @return Problem
     */
    public function setTargetGroup($targetGroup)
    {
        $this->targetGroup = $targetGroup;

        return $this;
    }

    /**
     * Get targetGroup
     *
     * @return string
     */
    public function getTargetGroup()
    {
        return $this->targetGroup;
    }

    /**
     * Set mainProblems
     *
     * @param string $mainProblems
     *
     * @return Problem
     */
    public function setMainProblems($mainProblems)
    {
        $this->mainProblems = $mainProblems;

        return $this;
    }

    /**
     * Get mainProblems
     *
     * @return string
     */
    public function getMainProblems()
    {
        return $this->mainProblems;
    }

    /**
     * Set expectedResults
     *
     * @param string $expectedResults
     *
     * @return Problem
     */
    public function setExpectedResults($expectedResults)
    {
        $this->expectedResults = $expectedResults;

        return $this;
    }

    /**
     * Get expectedResults
     *
     * @return string
     */
    public function getExpectedResults()
    {
        return $this->expectedResults;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $application
     *
     * @return Problem
     */
    public function setApplication(\NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Problem
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
