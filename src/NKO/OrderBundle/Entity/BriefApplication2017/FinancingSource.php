<?php

namespace NKO\OrderBundle\Entity\BriefApplication2017;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * FinancingSource
 *
 * @ORM\Table(name="brief_application2017_financing_source")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2017\FinancingSourceRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class FinancingSource
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sourceName", type="string", length=255, nullable=true)
     */
    private $sourceName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\Range(
     *     min="0",
     *     max="100",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="percent", type="string", length=255, nullable=true)
     */
    private $percent;

    /**
     * @ORM\ManyToOne(targetEntity="BriefApplication", inversedBy="financingSources",
     *     cascade={"persist"})
     * @Gedmo\Versioned
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id",
     *     onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sourceName
     *
     * @param string $sourceName
     *
     * @return FinancingSource
     */
    public function setSourceName($sourceName)
    {
        $this->sourceName = $sourceName;

        return $this;
    }

    /**
     * Get sourceName
     *
     * @return string
     */
    public function getSourceName()
    {
        return $this->sourceName;
    }

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return FinancingSource
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $application
     *
     * @return FinancingSource
     */
    public function setApplication(\NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return FinancingSource
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
