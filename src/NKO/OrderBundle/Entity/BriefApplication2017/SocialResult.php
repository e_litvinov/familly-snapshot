<?php

namespace NKO\OrderBundle\Entity\BriefApplication2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * SocialResult
 *
 * @ORM\Table(name="brief_application2017_social_result")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2017\SocialResultRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class SocialResult
{
    const TITLE = 'Social result';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="achieved_value", type="string", length=1000, nullable=true)
     */
    private $achievedValue;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="BriefApplication", inversedBy="socialResults", cascade={"persist"})
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $application;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->problems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SocialResult
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name ? (string)$this->name : self::TITLE;
    }

    /**
     * Set achievedValue
     *
     * @param string $achievedValue
     *
     * @return SocialResult
     */
    public function setAchievedValue($achievedValue)
    {
        $this->achievedValue = $achievedValue;

        return $this;
    }

    /**
     * Get achievedValue
     *
     * @return string
     */
    public function getAchievedValue()
    {
        return $this->achievedValue;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $application
     *
     * @return SocialResult
     */
    public function setApplication(\NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return SocialResult
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
