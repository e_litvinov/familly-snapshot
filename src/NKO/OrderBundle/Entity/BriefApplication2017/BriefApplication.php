<?php

namespace NKO\OrderBundle\Entity\BriefApplication2017;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\Publication;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Traits\PracticeDescriptionTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Traits\OrganizationInfoTrait;

/**
 * BriefApplication
 *
 * @ORM\Table(name="brief_application2017")
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2017\BriefApplicationRepository")
 */
class BriefApplication extends BaseApplication
{
    use PracticeDescriptionTrait;
    use OrganizationInfoTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    private $isOrganizationHeadEqualProjectHead;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_organization_head_equal_head_of_accounting", type="boolean", nullable=true)
     */
    private $isOrganizationHeadEqualHeadOfAccounting;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="achievement_result_mechanizm", type="text", nullable=true)
     */
    private $achievementResultMechanizm;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="effectiveness_practice_data", type="text", nullable=true)
     */
    private $effectivenessPracticeData;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="financing_source_etc", type="text", nullable=true)
     */
    private $financingSourceEtc;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="authority_director_document", type="text", nullable=true)
     */
    private $authorityDirectorDocument;

    private $_authorityDirectorDocument;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="sending_application_person_full_name", type="string", length=255, nullable=true)
     */
    private $sendingApplicationPersonFullName;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_correct_info", type="boolean", nullable=true)
     */
    private $isCorrectInfo;

    /**
     * @ORM\OneToMany(targetEntity="SocialResult", mappedBy="application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $socialResults;

    /**
     * @ORM\OneToMany(targetEntity="Problem", mappedBy="application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $problems;

    /**
     * @ORM\OneToMany(targetEntity="FinancingSource", mappedBy="application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $financingSources;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"BriefApplication-2017"}
     * )
     *
     * @ORM\Column(name="organization_head_consent", type="text", nullable=true)
     */
    private $organizationHeadConsent;

    private $_organizationHeadConsent;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="project_head_consent", type="text", nullable=true)
     */
    private $projectHeadConsent;

    private $_projectHeadConsent;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="accountant_head_consent", type="text", nullable=true)
     */
    private $accountantHeadConsent;

    private $_accountantHeadConsent;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="sending_application_person_consent", type="text", nullable=true)
     */
    private $sendingApplicationPersonConsent;

    private $_sendingApplicationPersonConsent;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory")
     * @ORM\JoinTable(name="brief_application2017_people_category_relationship",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     *      )
     */
    private $peopleCategories;


    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinTable(name="brief_application2017_practice_region",
     *      joinColumns={@ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="practice_region_id", referencedColumnName="id")}
     *      )
     */
    private $practiceRegions;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Publication", mappedBy="briefApplication",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $publications;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The value must be integer type",
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="count_practice_staff_employees", type="string", nullable=true)
     */
    private $countPracticeStaffEmployees;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The value must be integer type",
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="count_practice_involving_employees", type="string", nullable=true)
     */
    private $countPracticeInvolvingEmployees;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The value must be integer type",
     *     groups={"BriefApplication-2017"}
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"BriefApplication-2017"}
     *     )
     *
     * @ORM\Column(name="count_practice_volunteer_employees", type="string", nullable=true)
     */
    private $countPracticeVolunteerEmployees;

    public function __construct()
    {
        parent::__construct();
        $this->isCorrectInfo = true;
        $this->otherBeneficiaryGroups = new ArrayCollection();
        $this->socialResults = new ArrayCollection();
        $this->problems = new ArrayCollection();
        $this->financingSources = new ArrayCollection();
        $this->preSetFinancingSources();
        $this->projects = new ArrayCollection();
        $this->publications = new ArrayCollection();
        $this->peopleCategories = new ArrayCollection();
        $this->preCreateInstance();
        $this->preSetSocialResults();
        $this->practiceRegions = new ArrayCollection();
        $this->siteLinks = new ArrayCollection();
        $this->socialNetworkLinks = new ArrayCollection();
        $this->isAddressesEqual = true;
        $this->projects = new ArrayCollection();
    }

    private function preSetSocialResults()
    {
        $result1 = new SocialResult();
        $result1->setName('Количество детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства');
        $this->addSocialResult($result1);
        $result2 = new SocialResult();
        $result2->setName('в том числе подростков');
        $this->addSocialResult($result2);
        $result3 = new SocialResult();
        $result3->setName('в том числе детей с ОВЗ');
        $this->addSocialResult($result3);
        $result4 = new SocialResult();
        $result4->setName('в том числе сиблингов');
        $this->addSocialResult($result4);
        $result5 = new SocialResult();
        $result5->setName('Количество детей, возвращённых в кровные семьи');
        $this->addSocialResult($result5);
        $result6 = new SocialResult();
        $result6->setName('в том числе подростков');
        $this->addSocialResult($result6);
        $result7 = new SocialResult();
        $result7->setName('в том числе детей с ОВЗ');
        $this->addSocialResult($result7);
        $result8 = new SocialResult();
        $result8->setName('Количество предотвращённых случаев отобрания (изъятий) / отказов детей из кровных семей');
        $this->addSocialResult($result8);
        $result9 = new SocialResult();
        $result9->setName('Количество предотвращённых случаев отобрания (изъятий) / отказов детей из замещающих семей');
        $this->addSocialResult($result9);
        $result10 = new SocialResult();
        $result10->setName('Количество детей, улучшивших своё благополучие');
        $this->addSocialResult($result10);
        $result11 = new SocialResult();
        $result11->setName('Количество детей, повысивших уровень готовности к самостоятельной жизни в обществе');
        $this->addSocialResult($result11);
    }

    private function preCreateInstance()
    {
        $siteLink = new SiteLink();
        $this->addSiteLink($siteLink);
        $project = new Project();
        $this->addProject($project);
        $publication = new Publication();
        $this->addPublication($publication);
        $problem = new Problem();
        $this->addProblem($problem);
        $otherBeneficiaryGroup = new OtherBeneficiaryGroup();
        $this->addOtherBeneficiaryGroup($otherBeneficiaryGroup);
    }

    private function preSetFinancingSources()
    {
        $source1 = new FinancingSource();
        $source1->setSourceName('Собственные средства организации');
        $this->addFinancingSource($source1);
        $source2 = new FinancingSource();
        $source2->setSourceName('Поступления от других российских НКО');
        $this->addFinancingSource($source2);
        $source3 = new FinancingSource();
        $source3->setSourceName('Поступления от коммерческих организаций');
        $this->addFinancingSource($source3);
        $source4 = new FinancingSource();
        $source4->setSourceName('Пожертвования частных лиц');
        $this->addFinancingSource($source4);
        $source5 = new FinancingSource();
        $source5->setSourceName('Финансирование из федерального бюджета');
        $this->addFinancingSource($source5);
        $source6 = new FinancingSource();
        $source6->setSourceName('Финансирование из бюджета субъекта РФ');
        $this->addFinancingSource($source6);
        $source7 = new FinancingSource();
        $source7->setSourceName('Финансирование из местного (муниципального) бюджета');
        $this->addFinancingSource($source7);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     *     )
     */
    public function isValidPracticeRegions(ExecutionContextInterface $context, $payload)
    {
        if (!count($this->getPracticeRegions())) {
            $context->buildViolation('please, choose region')
                ->atPath('practiceRegions')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     *     )
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPeopleCategories();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose people categories')
                ->atPath('peopleCategories')
                ->addViolation();
        }
    }

    /**
     * Set authorityDirectorDocument
     * @param string $authorityDirectorDocument
     * @return BriefApplication
     */
    public function setAuthorityDirectorDocument($authorityDirectorDocument)
    {
        if (!$this->_authorityDirectorDocument && is_string($this->authorityDirectorDocument)) {
            $this->_authorityDirectorDocument = $this->authorityDirectorDocument;
        }
        $this->authorityDirectorDocument = $authorityDirectorDocument;

        return $this;
    }

    /**
     * Get authorityDirectorDocument
     * @return string
     */
    public function getAuthorityDirectorDocument()
    {
        return $this->authorityDirectorDocument;
    }

    /**
     * Set isOrganizationHeadEqualProjectHead
     *
     * @param boolean $isOrganizationHeadEqualProjectHead
     *
     * @return BriefApplication
     */
    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    /**
     * Get isOrganizationHeadEqualProjectHead
     *
     * @return boolean
     */
    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    /**
     * Set achievementResultMechanizm
     *
     * @param string $achievementResultMechanizm
     *
     * @return BriefApplication
     */
    public function setAchievementResultMechanizm($achievementResultMechanizm)
    {
        $this->achievementResultMechanizm = $achievementResultMechanizm;

        return $this;
    }

    /**
     * Get achievementResultMechanizm
     *
     * @return string
     */
    public function getAchievementResultMechanizm()
    {
        return $this->achievementResultMechanizm;
    }

    /**
     * Set effectivenessPracticeData
     *
     * @param string $effectivenessPracticeData
     *
     * @return BriefApplication
     */
    public function setEffectivenessPracticeData($effectivenessPracticeData)
    {
        $this->effectivenessPracticeData = $effectivenessPracticeData;

        return $this;
    }

    /**
     * Get effectivenessPracticeData
     *
     * @return string
     */
    public function getEffectivenessPracticeData()
    {
        return $this->effectivenessPracticeData;
    }

    /**
     * Set financingSourceEtc
     *
     * @param string $financingSourceEtc
     *
     * @return BriefApplication
     */
    public function setFinancingSourceEtc($financingSourceEtc)
    {
        $this->financingSourceEtc = $financingSourceEtc;

        return $this;
    }

    /**
     * Get financingSourceEtc
     *
     * @return string
     */
    public function getFinancingSourceEtc()
    {
        return $this->financingSourceEtc;
    }

    /**
     * Set sendingApplicationPersonFullName
     *
     * @param string $sendingApplicationPersonFullName
     *
     * @return BriefApplication
     */
    public function setSendingApplicationPersonFullName($sendingApplicationPersonFullName)
    {
        $this->sendingApplicationPersonFullName = $sendingApplicationPersonFullName;

        return $this;
    }

    /**
     * Get sendingApplicationPersonFullName
     *
     * @return string
     */
    public function getSendingApplicationPersonFullName()
    {
        return $this->sendingApplicationPersonFullName;
    }

    /**
     * Set isCorrectInfo
     *
     * @param boolean $isCorrectInfo
     *
     * @return BriefApplication
     */
    public function setIsCorrectInfo($isCorrectInfo)
    {
        $this->isCorrectInfo = $isCorrectInfo;

        return $this;
    }

    /**
     * Get isCorrectInfo
     *
     * @return boolean
     */
    public function getIsCorrectInfo()
    {
        return $this->isCorrectInfo;
    }

    /**
     * Set organizationHeadConsent
     * @param string $organizationHeadConsent
     * @return BriefApplication
     */
    public function setOrganizationHeadConsent($organizationHeadConsent)
    {
        if (!$this->_organizationHeadConsent && is_string($this->organizationHeadConsent)) {
            $this->_organizationHeadConsent = $this->organizationHeadConsent;
        }
        $this->organizationHeadConsent = $organizationHeadConsent;

        return $this;
    }

    /**
     * Get organizationHeadConsent
     * @return string
     */
    public function getOrganizationHeadConsent()
    {
        return $this->organizationHeadConsent;
    }

    /**
     * Set projectHeadConsent
     * @param string $projectHeadConsent
     * @return BriefApplication
     */
    public function setProjectHeadConsent($projectHeadConsent)
    {
        if (!$this->_projectHeadConsent && is_string($this->projectHeadConsent)) {
            $this->_projectHeadConsent = $this->projectHeadConsent;
        }
        $this->projectHeadConsent = $projectHeadConsent;

        return $this;
    }

    /**
     * Get projectHeadConsent
     * @return string
     */
    public function getProjectHeadConsent()
    {
        return $this->projectHeadConsent;
    }

    /**
     * Set accountantHeadConsent
     * @param string $accountantHeadConsent
     * @return BriefApplication
     */
    public function setAccountantHeadConsent($accountantHeadConsent)
    {
        if (!$this->_accountantHeadConsent && is_string($this->accountantHeadConsent)) {
            $this->_accountantHeadConsent = $this->accountantHeadConsent;
        }
        $this->accountantHeadConsent = $accountantHeadConsent;
        return $this;
    }

    /**
     * Get accountantHeadConsent
     * @return string
     */
    public function getAccountantHeadConsent()
    {
        return $this->accountantHeadConsent;
    }

    /**
     * Set sendingApplicationPersonConsent
     * @param string $sendingApplicationPersonConsent
     * @return BriefApplication
     */
    public function setSendingApplicationPersonConsent($sendingApplicationPersonConsent)
    {
        if (!$this->_sendingApplicationPersonConsent && is_string($this->sendingApplicationPersonConsent)) {
            $this->_sendingApplicationPersonConsent = $this->sendingApplicationPersonConsent;
        }
        $this->sendingApplicationPersonConsent = $sendingApplicationPersonConsent;
        return $this;
    }

    /**
     * Get sendingApplicationPersonConsent
     * @return string
     */
    public function getSendingApplicationPersonConsent()
    {
        return $this->sendingApplicationPersonConsent;
    }

    /**
     * Set countPracticeStaffEmployees
     *
     * @param string $countPracticeStaffEmployees
     *
     * @return BriefApplication
     */
    public function setCountPracticeStaffEmployees($countPracticeStaffEmployees)
    {
        $this->countPracticeStaffEmployees = $countPracticeStaffEmployees;

        return $this;
    }

    /**
     * Get countPracticeStaffEmployees
     *
     * @return string
     */
    public function getCountPracticeStaffEmployees()
    {
        return $this->countPracticeStaffEmployees;
    }

    /**
     * Set countPracticeInvolvingEmployees
     *
     * @param string $countPracticeInvolvingEmployees
     *
     * @return BriefApplication
     */
    public function setCountPracticeInvolvingEmployees($countPracticeInvolvingEmployees)
    {
        $this->countPracticeInvolvingEmployees = $countPracticeInvolvingEmployees;
        return $this;
    }

    /**
     * Get countPracticeInvolvingEmployees
     *
     * @return string
     */
    public function getCountPracticeInvolvingEmployees()
    {
        return $this->countPracticeInvolvingEmployees;
    }

    /**
     * Set countPracticeVolunteerEmployees
     *
     * @param string $countPracticeVolunteerEmployees
     *
     * @return BriefApplication
     */
    public function setCountPracticeVolunteerEmployees($countPracticeVolunteerEmployees)
    {
        $this->countPracticeVolunteerEmployees = $countPracticeVolunteerEmployees;

        return $this;
    }

    /**
     * Get countPracticeVolunteerEmployees
     *
     * @return string
     */
    public function getCountPracticeVolunteerEmployees()
    {
        return $this->countPracticeVolunteerEmployees;
    }

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     *     )
     */
    public function isValidFinancingSources(ExecutionContextInterface $context, $payload)
    {
        $percentSum = 0;
        /**
         * @var FinancingSource $source
         */
        foreach ($this->getFinancingSources() as $source) {
            $percentSum += ($source->getPercent());
        }

        if ($percentSum > 100) {
            $context->buildViolation('sum percent can not be more than 100%')
                ->atPath('financingSources')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     *     )
     */
    public function isValidAuthorityDirectorDocument(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getAuthorityDirectorDocument() && !$this->_authorityDirectorDocument) {
            $context->buildViolation('please, upload authority director document')
                ->atPath('authorityDirectorDocument')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     *     )
     */
    public function isValidOrganizationHeadConsent(ExecutionContextInterface $context, $payload)
    {
        if (!$this->organizationHeadConsent && !$this->_organizationHeadConsent) {
            $this->createViolation($context, 'please, upload organization head consent', 'organizationHeadConsent');
        }
    }

    private function createViolation(ExecutionContextInterface $context, $message, $path)
    {
        $context->buildViolation($message)
            ->atPath($path)
            ->addViolation();
    }

    /**
     * Set isOrganizationHeadEqualHeadOfAccounting
     *
     * @param boolean $isOrganizationHeadEqualHeadOfAccounting
     *
     * @return BriefApplication
     */
    public function setIsOrganizationHeadEqualHeadOfAccounting($isOrganizationHeadEqualHeadOfAccounting)
    {
        $this->isOrganizationHeadEqualHeadOfAccounting = $isOrganizationHeadEqualHeadOfAccounting;

        return $this;
    }

    /**
     * Get isOrganizationHeadEqualHeadOfAccounting
     *
     * @return boolean
     */
    public function getIsOrganizationHeadEqualHeadOfAccounting()
    {
        return $this->isOrganizationHeadEqualHeadOfAccounting;
    }

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     *     )
     */
    public function isIsCorrectInfo(ExecutionContextInterface $context, $payload)
    {
        if (!$this->isCorrectInfo) {
            $context->buildViolation('please, confirm if correct data')
                ->atPath('isCorrectInfo')
                ->addViolation();
        }
    }

    /**
     * Add socialResult
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\SocialResult $socialResult
     *
     * @return BriefApplication
     */
    public function addSocialResult(\NKO\OrderBundle\Entity\BriefApplication2017\SocialResult $socialResult)
    {
        $socialResult->setApplication($this);
        $this->socialResults[] = $socialResult;

        return $this;
    }

    /**
     * Remove socialResult
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\SocialResult $socialResult
     */
    public function removeSocialResult(\NKO\OrderBundle\Entity\BriefApplication2017\SocialResult $socialResult)
    {
        $this->socialResults->removeElement($socialResult);
    }

    /**
     * Get socialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResults()
    {
        return $this->socialResults;
    }

    /**
     * Add problem
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\Problem $problem
     *
     * @return BriefApplication
     */
    public function addProblem(\NKO\OrderBundle\Entity\BriefApplication2017\Problem $problem)
    {
        $problem->setApplication($this);
        $this->problems[] = $problem;

        return $this;
    }

    /**
     * Remove problem
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\Problem $problem
     */
    public function removeProblem(\NKO\OrderBundle\Entity\BriefApplication2017\Problem $problem)
    {
        $this->problems->removeElement($problem);
    }

    /**
     * Get problems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProblems()
    {
        return $this->problems;
    }

    /**
     * Add financingSource
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\FinancingSource $financingSource
     *
     * @return BriefApplication
     */
    public function addFinancingSource(\NKO\OrderBundle\Entity\BriefApplication2017\FinancingSource $financingSource)
    {
        $financingSource->setApplication($this);
        $this->financingSources[] = $financingSource;

        return $this;
    }

    /**
     * Remove financingSource
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\FinancingSource $financingSource
     */
    public function removeFinancingSource(\NKO\OrderBundle\Entity\BriefApplication2017\FinancingSource $financingSource)
    {
        $this->financingSources->removeElement($financingSource);
    }

    /**
     * Get financingSources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFinancingSources()
    {
        return $this->financingSources;
    }


    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     *
     * @return BriefApplication
     */
    public function addPublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $publication->setBriefApplication($this);
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     */
    public function removePublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return BriefApplication
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;

        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Add practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     *
     * @return BriefApplication
     */
    public function addPracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions[] = $practiceRegion;

        return $this;
    }

    /**
     * Remove practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     */
    public function removePracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions->removeElement($practiceRegion);
    }

    /**
     * Get practiceRegions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeRegions()
    {
        return $this->practiceRegions;
    }

    public function __toString()
    {
        return (string) $this->getAuthor()->getNkoName();
    }
}
