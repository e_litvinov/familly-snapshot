<?php

namespace NKO\OrderBundle\Entity\BriefApplication2017;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OtherBeneficiaryGroup
 *
 * @ORM\Table(name="brief_application2017_other_beneficiary_group")
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="NKO\OrderBundle\Repository\BriefApplication2017\OtherBeneficiaryGroupRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class OtherBeneficiaryGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"KNS2018-3"})
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", cascade={"persist"})
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $application;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseApplication", cascade={"persist"})
     * @ORM\JoinColumn(name="specialist_application_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $specialistApplication;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport", inversedBy="otherBeneficiaryGroups")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OtherBeneficiaryGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set application
     *
     * @param \NKO\OrderBundle\Entity\BaseApplication $application
     *
     * @return OtherBeneficiaryGroup
     */
    public function setApplication(\NKO\OrderBundle\Entity\BaseApplication $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return OtherBeneficiaryGroup
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set specialistApplication
     *
     * @param BaseApplication $specialistApplication
     *
     * @return OtherBeneficiaryGroup
     */
    public function setSpecialistApplication(BaseApplication $specialistApplication = null)
    {
        $this->specialistApplication = $specialistApplication;

        return $this;
    }

    /**
     * Get specialistApplication
     *
     * @return \NKO\OrderBundle\Entity\BaseApplication
     */
    public function getSpecialistApplication()
    {
        return $this->specialistApplication;
    }

    /**
     * Set report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return OtherBeneficiaryGroup
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \NKO\OrderBundle\Entity\BaseReport
     */
    public function getReport()
    {
        return $this->report;
    }
}
