<?php

namespace NKO\OrderBundle;

use NKO\OrderBundle\Factory\AdminListView\DocumentsGeneratorPass;
use NKO\OrderBundle\ListApplication\ListApplicationPass;
use NKO\OrderBundle\ReportTypeManager\ReportTypePass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class NKOOrderBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ListApplicationPass());
        $container->addCompilerPass(new ReportTypePass());
        $container->addCompilerPass(new DocumentsGeneratorPass());
    }
}
