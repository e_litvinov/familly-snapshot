<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/11/18
 * Time: 3:29 PM
 */

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait PracticeDescriptionTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Farvater\PriorityDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $priorityDirection;

    /**
     * @var string
     * @ORM\Column(name="priority_direction_etc", type="string", length=255, nullable=true)
     */
    private $priorityDirectionEtc;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "BriefApplication-2017", "BriefApplication-2018", "ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"
     *         }
     *     )
     *
     * @ORM\Column(name="brief_practice_description", type="text", nullable=true)
     */
    private $briefPracticeDescription;

    /**
     * @Assert\Valid()
     * @ORM\OrderBy({"id" = "ASC"})
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup", mappedBy="application",
     *     orphanRemoval=true, cascade={"all"})
     */
    private $otherBeneficiaryGroups;

    /**
     * @Assert\Callback(
     *     groups={"BriefApplication-2017"}
     * )
     */
    public function isValidPriorityDirections(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getPriorityDirection() && !$this->getPriorityDirectionEtc()) {
            $context->buildViolation('please, choose priority direction')
                ->atPath('priorityDirection')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"ContinuationApplication-2", "ContinuationSF18-2019"}
     * )
     */
    public function isValidOnlyPriorityDirections(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getPriorityDirection()) {
            $context->buildViolation('please, choose priority direction')
                ->atPath('priorityDirection')
                ->addViolation();
        }
    }

    public function setPriorityDirectionEtc($priorityDirectionEtc)
    {
        $this->priorityDirectionEtc = $priorityDirectionEtc;

        return $this;
    }

    public function getPriorityDirectionEtc()
    {
        return $this->priorityDirectionEtc;
    }

    public function setBriefPracticeDescription($briefPracticeDescription)
    {
        $this->briefPracticeDescription = $briefPracticeDescription;

        return $this;
    }

    public function getBriefPracticeDescription()
    {
        return $this->briefPracticeDescription;
    }

    public function setPriorityDirection(PriorityDirection $priorityDirection = null)
    {
        $this->priorityDirection = $priorityDirection;

        return $this;
    }

    public function getPriorityDirection()
    {
        return $this->priorityDirection;
    }

    public function addOtherBeneficiaryGroup(OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $otherBeneficiaryGroup->setApplication($this);
        $this->otherBeneficiaryGroups[] = $otherBeneficiaryGroup;

        return $this;
    }

    public function removeOtherBeneficiaryGroup(OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups->removeElement($otherBeneficiaryGroup);
    }

    public function getOtherBeneficiaryGroups()
    {
        return $this->otherBeneficiaryGroups;
    }
}
