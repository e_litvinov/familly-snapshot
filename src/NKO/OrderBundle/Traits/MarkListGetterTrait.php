<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 17:38
 */

namespace NKO\OrderBundle\Traits;

trait MarkListGetterTrait
{
    public function getMarkListByExpert($expert)
    {
        $mark = $this->getMarkLists()->filter(function ($markList) use ($expert) {
            return $markList->getExpert()->getId() == $expert->getId();
        })->first();

        return $mark;
    }
}