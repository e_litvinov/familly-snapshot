<?php

namespace NKO\OrderBundle\Traits;

trait AdminConfiguratorTrait
{
    public static function getCompletedDescription($customDescription, $newDescription)
    {
        return $newDescription ? array_merge($customDescription, $newDescription) : $customDescription;
    }

    public static function removeExtraFields($formMapper, array $options)
    {

        if (self::issetConfig($options, 'extraFields')) {
            foreach ($options['extraFields'] as $field) {
                $formMapper->remove($field);
            }
        }
    }

    public static function changeOptions($formMapper, array $options)
    {
        if (self::issetConfig($options, 'parameters')) {
            foreach ($options['parameters'] as $key => $field) {
                $type = null;
                $beginOptions = $formMapper->get($key)->getOptions();

                if (self::issetConfig($field, 'selectedType')) {
                    $type = $field['selectedType'];
                    unset($field['selectedType']);
                }

                $finalOptions = array_merge($beginOptions, $field);

                if (in_array($type, ['sonata_type_collection', 'sonata_type_model'], true)) {
                    self::addSonataType($formMapper, $type, $options, $key, $finalOptions);
                    continue;
                }

                $formMapper->add($key, $type, $finalOptions);
            }
        }
    }

    public static function addSonataType($formMapper, $type, $options, $key, $finalOptions)
    {
        $adminOptions = ['admin_code' => isset($options['admin_code'][$key]) ? $options['admin_code'][$key] : null];
        $adminOptions = ($type !== 'sonata_type_collection') ? $adminOptions : (array_merge($adminOptions, ['edit' => 'inline', 'inline' => 'table']));
        $formMapper->add($key, $type, $finalOptions, $adminOptions);
    }

    public static function issetConfig($options, $field)
    {
        return $options && array_key_exists($field, $options) ? true : null;
    }
}
