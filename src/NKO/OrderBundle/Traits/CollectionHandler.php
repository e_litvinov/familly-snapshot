<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/21/18
 * Time: 7:54 PM
 */

namespace NKO\OrderBundle\Traits;


trait CollectionHandler
{
    public function modifyToStringArray($collection, $values = null)
    {
        if (!$values) {
            $values = [];
        }

        foreach ($collection as $item) {
            $title = (string) $item;
            if (array_search($title, $values) === false && $title && $title != "-") {
                $values[] = $title;
            }
        }

        return $values;
    }
}