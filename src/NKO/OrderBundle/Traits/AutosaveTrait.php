<?php

namespace NKO\OrderBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait AutosaveTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(name="is_autosaved", type="boolean", nullable=true)
     */
    protected $isAutosaved;

    public function setIsAutosaved($isAutosaved)
    {
        $this->isAutosaved = $isAutosaved;

        return $this;
    }

    public function getIsAutosaved()
    {
        return $this->isAutosaved;
    }
}
