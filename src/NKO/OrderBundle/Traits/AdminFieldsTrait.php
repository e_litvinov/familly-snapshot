<?php

namespace NKO\OrderBundle\Traits;

trait AdminFieldsTrait
{
    public function generateType($types)
    {
        $type = 'default';
        $class = get_class($this->getParentFieldDescription()->getAdmin()->getSubject());
        foreach ($types as $key => $typeValue) {
            $type = in_array($class, $typeValue) ? $key : $type;
        }
        
        return $type;
    }
}