<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

trait OrganizationInfoTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS-2017", "BriefApplication-2017", "KNS-2016", "KNS-2018",
     *              "KNS2017-2_2", "KNS-2019", "KNS2018-3", "KNS-2020", "Harbor-2020", "Verification-2020", "KNS2019-2"
     *         }
     *     )
     *
     * @ORM\Column(name="organization_name", type="text", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2017", "BriefApplication-2017", "KNS-2016", "BriefApplication-2018", "KNS-2018",
     *         "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "Harbor-2019", "Harbor-2020", "KNS2018-3", "KNS-2020",
     *         "Verification-2020", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="organization_abbreviation", type="string", length=255, nullable=true)
     */
    protected $abbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="organization_PSRN", type="string", length=13, nullable=true, unique=false)
     */
    protected $pSRN;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *         "Address"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "index must be at least 6 characters long",
     *     groups={
     *         "Address"
     *     }
     *     )
     *
     * @ORM\Column(name="legal_post_code", type="string", length=6, nullable=true)
     */
    protected $legalPostCode;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Region")
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\JoinColumn(name="legal_region_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    protected $legalRegion;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\Column(name="legal_city", type="string", length=255, nullable=true)
     */
    protected $legalCity;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\Column(name="legal_street", type="text", nullable=true)
     */
    protected $legalStreet;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_addresses_equal", type="boolean", nullable=true)
     */
    protected $isAddressesEqual;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *         "Address"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     * @Assert\Length(
     *     min = 6,
     *     minMessage = "index must be at least 6 characters long",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\Column(name="actual_post_code", type="string", length=6, nullable=true)
     */
    protected $actualPostCode;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Region")
     * @ORM\JoinColumn(name="actual_region_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $actualRegion;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\Column(name="actual_city", type="string", length=255, nullable=true)
     */
    protected $actualCity;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "Address"
     *     }
     * )
     *
     * @ORM\Column(name="actual_street", type="text", nullable=true)
     */
    protected $actualStreet;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\SiteLink", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $siteLinks;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\SocialNetworkLink", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    protected $socialNetworkLinks;

     /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Project", mappedBy="application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $projects;

    /**
     * @var string
     *
     * @Assert\Email(
     *     message = "The email is not valid",
     *     strict = true,
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     * @Assert\Regex(
     *     pattern = "/[а-я]/",
     *     match = false,
     *     message = "not Cyrillic",
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "phone code must be at least 3 characters long",
     *     groups={
     *          "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     *
     * @ORM\Column(name="phone_code", type="string", length=255, nullable=true)
     */
    protected $phoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *          "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "phone must be at least 7 characters long",
     *     groups={
     *         "GeneralInformation", "Harbor-2019", "Harbor-2020", "Verification-2020"
     *     }
     * )
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "GeneralInformation"
     *     }
     * )
     *
     * @ORM\Column(name="date_registration_of_organization", type="datetime", nullable=true)
     */
    protected $dateRegistrationOfOrganization;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "GeneralInformation"
     *     }
     * )
     *
     * @ORM\Column(name="primaryActivity", type="text", nullable=true)
     */
    protected $primaryActivity;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "GeneralInformation"
     *     }
     * )
     *
     * @ORM\Column(name="mission", type="text", nullable=true)
     */
    protected $mission;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The value must be integer type",
     *     groups={
     *         "CountEmployees"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *          "CountEmployees"
     *     }
     * )
     *
     * @ORM\Column(name="countStaffEmployees", type="string", nullable=true)
     */
    protected $countStaffEmployees;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The value must be integer type",
     *     groups={
     *          "CountEmployees"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "CountEmployees"
     *     }
     * )
     *
     * @ORM\Column(name="countInvolvingEmployees", type="string", nullable=true)
     */
    protected $countInvolvingEmployees;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The value must be integer type",
     *     groups={
     *         "BriefApplication-2017", "BriefApplication-2018", "KNS-2018", "KNS-2020", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS2019-2"
     *     }
     * )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "BriefApplication-2017", "BriefApplication-2018", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *     }
     * )
     *
     * @ORM\Column(name="count_volunteer_employees", type="string", nullable=true)
     */
    protected $countVolunteerEmployees;

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $abbreviation
     * @return $this
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set pSRN
     *
     * @param string $pSRN
     *
     */
    public function setPSRN($pSRN)
    {
        $this->pSRN = $pSRN;

        return $this;
    }

    /**
     * Get pSRN
     *
     * @return string
     */
    public function getPSRN()
    {
        return $this->pSRN;
    }

    /**
     * Set legalPostCode
     *
     * @param string $legalPostCode
     */
    public function setLegalPostCode($legalPostCode)
    {
        $this->legalPostCode = $legalPostCode;

        return $this;
    }

    /**
     * Get legalPostCode
     *
     * @return string
     */
    public function getLegalPostCode()
    {
        return $this->legalPostCode;
    }

    /**
     * Set legalRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $legalRegion
     *
     */
    public function setLegalRegion(\NKO\OrderBundle\Entity\Region $legalRegion = null)
    {
        $this->legalRegion = $legalRegion;

        return $this;
    }

    /**
     * Get legalRegion
     *
     * @return \NKO\OrderBundle\Entity\Region
     */
    public function getLegalRegion()
    {
        return $this->legalRegion;
    }

    /**
     * Set legalCity
     *
     * @param string $legalCity
     *
     */
    public function setLegalCity($legalCity)
    {
        $this->legalCity = $legalCity;

        return $this;
    }

    /**
     * Get legalCity
     *
     * @return string
     */
    public function getLegalCity()
    {
        return $this->legalCity;
    }

    /**
     * Set legalStreet
     *
     * @param string $legalStreet
     *
     */
    public function setLegalStreet($legalStreet)
    {
        $this->legalStreet = $legalStreet;

        return $this;
    }

    /**
     * Get legalStreet
     *
     * @return string
     */
    public function getLegalStreet()
    {
        return $this->legalStreet;
    }

    /**
     * Set isAddressesEqual
     *
     * @param boolean $isAddressesEqual
     *
     */
    public function setIsAddressesEqual($isAddressesEqual)
    {
        $this->isAddressesEqual = $isAddressesEqual;

        return $this;
    }

    /**
     * Get isAddressesEqual
     *
     * @return boolean
     */
    public function getIsAddressesEqual()
    {
        return $this->isAddressesEqual;
    }

    /**
     * Set actualPostCode
     *
     * @param string $actualPostCode
     *
     */
    public function setActualPostCode($actualPostCode)
    {
        $this->actualPostCode = $actualPostCode;

        return $this;
    }

    /**
     * Get actualPostCode
     *
     * @return string
     */
    public function getActualPostCode()
    {
        return $this->actualPostCode;
    }

    /**
     * Set actualRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $actualRegion
     *
     */
    public function setActualRegion(\NKO\OrderBundle\Entity\Region $actualRegion = null)
    {
        $this->actualRegion = $actualRegion;

        return $this;
    }

    /**
     * Get actualRegion
     *
     * @return \NKO\OrderBundle\Entity\Region
     */
    public function getActualRegion()
    {
        return $this->actualRegion;
    }

    /**
     * Set actualCity
     *
     * @param string $actualCity
     *
     */
    public function setActualCity($actualCity)
    {
        $this->actualCity = $actualCity;

        return $this;
    }

    /**
     * Get actualCity
     *
     * @return string
     */
    public function getActualCity()
    {
        return $this->actualCity;
    }

    /**
     * Set actualStreet
     *
     * @param string $actualStreet
     *
     */
    public function setActualStreet($actualStreet)
    {
        $this->actualStreet = $actualStreet;

        return $this;
    }

    /**
     * Get actualStreet
     *
     * @return string
     */
    public function getActualStreet()
    {
        return $this->actualStreet;
    }

    /**
     * Add siteLink
     *
     * @param \NKO\OrderBundle\Entity\SiteLink $siteLink
     *
     */
    public function addSiteLink(\NKO\OrderBundle\Entity\SiteLink $siteLink)
    {
        $siteLink->setApplication($this);
        $this->siteLinks[] = $siteLink;

        return $this;
    }

    /**
     * Remove siteLink
     *
     * @param \NKO\OrderBundle\Entity\SiteLink $siteLink
     */
    public function removeSiteLink(\NKO\OrderBundle\Entity\SiteLink $siteLink)
    {
        $this->siteLinks->removeElement($siteLink);
    }

    /**
     * Get siteLinks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSiteLinks()
    {
        return $this->siteLinks;
    }

    /**
     * Add socialNetworkLink
     *
     * @param \NKO\OrderBundle\Entity\SocialNetworkLink $socialNetworkLink
     *
     */
    public function addSocialNetworkLink(\NKO\OrderBundle\Entity\SocialNetworkLink $socialNetworkLink)
    {
        $socialNetworkLink->setApplication($this);
        $this->socialNetworkLinks[] = $socialNetworkLink;

        return $this;
    }

    /**
     * Remove socialNetworkLink
     *
     * @param \NKO\OrderBundle\Entity\SocialNetworkLink $socialNetworkLink
     */
    public function removeSocialNetworkLink(\NKO\OrderBundle\Entity\SocialNetworkLink $socialNetworkLink)
    {
        $this->socialNetworkLinks->removeElement($socialNetworkLink);
    }

    /**
     * Get socialNetworkLinks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialNetworkLinks()
    {
        return $this->socialNetworkLinks;
    }
    /**
     * Set email
     *
     * @param string $email
     *
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phoneCode
     *
     * @param string $phoneCode
     *
     */
    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;

        return $this;
    }

    /**
     * Get phoneCode
     *
     * @return string
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set dateRegistrationOfOrganization
     *
     * @param \DateTime $dateRegistrationOfOrganization
     *
     * @return BaseApplication
     */
    public function setDateRegistrationOfOrganization($dateRegistrationOfOrganization)
    {
        $this->dateRegistrationOfOrganization = $dateRegistrationOfOrganization;

        return $this;
    }

    /**
     * Get dateRegistrationOfOrganization
     *
     * @return \DateTime
     */
    public function getDateRegistrationOfOrganization()
    {
        return $this->dateRegistrationOfOrganization;
    }

    /**
     * Set primaryActivity
     *
     * @param string $primaryActivity
     *
     * @return BaseApplication
     */
    public function setPrimaryActivity($primaryActivity)
    {
        $this->primaryActivity = $primaryActivity;

        return $this;
    }

    /**
     * Get primaryActivity
     *
     * @return string
     */
    public function getPrimaryActivity()
    {
        return $this->primaryActivity;
    }

    /**
     * Set mission
     *
     * @param string $mission
     *
     * @return BaseApplication
     */
    public function setMission($mission)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * Get mission
     *
     * @return string
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * Set countStaffEmployees
     *
     * @param string $countStaffEmployees
     *
     * @return BaseApplication
     */
    public function setCountStaffEmployees($countStaffEmployees)
    {
        $this->countStaffEmployees = $countStaffEmployees;

        return $this;
    }

    /**
     * Get countStaffEmployees
     *
     * @return string
     */
    public function getCountStaffEmployees()
    {
        return $this->countStaffEmployees;
    }

    /**
     * Set countInvolvingEmployees
     *
     * @param string $countInvolvingEmployees
     *
     * @return BaseApplication
     */
    public function setCountInvolvingEmployees($countInvolvingEmployees)
    {
        $this->countInvolvingEmployees = $countInvolvingEmployees;

        return $this;
    }

    /**
     * Get countInvolvingEmployees
     *
     * @return string
     */
    public function getCountInvolvingEmployees()
    {
        return $this->countInvolvingEmployees;
    }

    public function setCountVolunteerEmployees($countVolunteerEmployees)
    {
        $this->countVolunteerEmployees = $countVolunteerEmployees;

        return $this;
    }

    /**
     * Get countVolunteerEmployees
     *
     * @return string
     */
    public function getCountVolunteerEmployees()
    {
        return $this->countVolunteerEmployees;
    }

    public function addProject(\NKO\OrderBundle\Entity\Project $project)
    {
        $project->setApplication($this);
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \NKO\OrderBundle\Entity\Project $project
     */
    public function removeProject(\NKO\OrderBundle\Entity\Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }
}
