<?php

namespace NKO\OrderBundle\Traits\Report;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult;

trait ExpectedResultsTrait
{
    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="experienceReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $experienceItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="experienceEtcReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $experienceEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="processReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $processItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="processEtcReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $processEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="qualificationReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $qualificationItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="qualificationEtcReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $qualificationEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="resourceReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $resourceBlockItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult", mappedBy="resourceEtcReport", cascade={"all"}, orphanRemoval=true)
     */
    protected $resourceBlockEtcItems;


    public function addExperienceItem(ExpectedResult $experienceItem)
    {
        $experienceItem->setExperienceReport($this);
        $this->experienceItems[] = $experienceItem;

        return $this;
    }

    public function removeExperienceItem(ExpectedResult $experienceItem)
    {
        $this->experienceItems->removeElement($experienceItem);
    }

    public function getExperienceItems()
    {
        return $this->experienceItems;
    }

    public function addExperienceEtcItem(ExpectedResult $experienceEtcItem)
    {
        $experienceEtcItem->setExperienceEtcReport($this);
        $this->experienceEtcItems[] = $experienceEtcItem;

        return $this;
    }

    public function removeExperienceEtcItem(ExpectedResult $experienceEtcItem)
    {
        $this->experienceEtcItems->removeElement($experienceEtcItem);
    }

    public function getExperienceEtcItems()
    {
        return $this->experienceEtcItems;
    }

    public function addProcessItem(ExpectedResult $processItem)
    {
        $processItem->setProcessReport($this);
        $this->processItems[] = $processItem;

        return $this;
    }

    public function removeProcessItem(ExpectedResult $processItem)
    {
        $this->processItems->removeElement($processItem);
    }

    public function getProcessItems()
    {
        return $this->processItems;
    }

    public function addProcessEtcItem(ExpectedResult $processEtcItem)
    {
        $processEtcItem->setProcessEtcReport($this);
        $this->processEtcItems[] = $processEtcItem;

        return $this;
    }

    public function removeProcessEtcItem(ExpectedResult $processEtcItem)
    {
        $this->processEtcItems->removeElement($processEtcItem);
    }

    public function getProcessEtcItems()
    {
        return $this->processEtcItems;
    }

    public function addQualificationItem(ExpectedResult $qualificationItem)
    {
        $qualificationItem->setQualificationReport($this);
        $this->qualificationItems[] = $qualificationItem;

        return $this;
    }

    public function removeQualificationItem(ExpectedResult $qualificationItem)
    {
        $this->qualificationItems->removeElement($qualificationItem);
    }

    public function getQualificationItems()
    {
        return $this->qualificationItems;
    }

    public function addQualificationEtcItem(ExpectedResult $qualificationEtcItem)
    {
        $qualificationEtcItem->setQualificationEtcReport($this);
        $this->qualificationEtcItems[] = $qualificationEtcItem;
        return $this;
    }

    public function removeQualificationEtcItem(ExpectedResult $qualificationEtcItem)
    {
        $this->qualificationEtcItems->removeElement($qualificationEtcItem);
    }

    public function getQualificationEtcItems()
    {
        return $this->qualificationEtcItems;
    }

    public function addResourceBlockItem(ExpectedResult $resourceBlockItem)
    {
        $resourceBlockItem->setResourceReport($this);
        $this->resourceBlockItems[] = $resourceBlockItem;

        return $this;
    }

    public function removeResourceBlockItem(ExpectedResult $resourceBlockItem)
    {
        $this->resourceBlockItems->removeElement($resourceBlockItem);
    }

    public function getResourceBlockItems()
    {
        return $this->resourceBlockItems;
    }

    public function addResourceBlockEtcItem(ExpectedResult $resourceBlockEtcItem)
    {
        $resourceBlockEtcItem->setResourceEtcReport($this);
        $this->resourceBlockEtcItems[] = $resourceBlockEtcItem;

        return $this;
    }

    public function removeResourceBlockEtcItem(ExpectedResult $resourceBlockEtcItem)
    {
        $this->resourceBlockEtcItems->removeElement($resourceBlockEtcItem);
    }

    public function getResourceBlockEtcItems()
    {
        return $this->resourceBlockEtcItems;
    }
}
