<?php

namespace NKO\OrderBundle\Traits\Report;

trait OfficialsTrait
{

    /**
     * @Assert\Email(
     *     message = "The email is not valid",
     *     strict = true,
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\Regex(
     *     pattern = "/[а-я]/",
     *     match = false,
     *     message = "not Cyrillic",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\Length(
     *     min = 3,
     *     minMessage = "phone code must be at least 3 characters long",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="phone_code", type="string", length=255, nullable=true)
     */
    protected $phoneCode;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     * @Assert\Length(
     *     min = 7,
     *     minMessage = "phone must be at least 7 characters long",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "MixedReportKNS-2018", "MixedReportKNS-2019", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $directorName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "MixedReportKNS-2018", "MixedReportKNS-2019", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $directorPosition;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $accountantName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $accountantPosition;


    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;

        return $this;
    }


    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setDirectorName($directorName)
    {
        $this->directorName = $directorName;

        return $this;
    }

    public function getDirectorName()
    {
        return $this->directorName;
    }

    public function setDirectorPosition($directorPosition)
    {
        $this->directorPosition = $directorPosition;

        return $this;
    }

    public function getDirectorPosition()
    {
        return $this->directorPosition;
    }

    public function setAccountantName($accountantName)
    {
        $this->accountantName = $accountantName;

        return $this;
    }

    public function getAccountantName()
    {
        return $this->accountantName;
    }

    public function setAccountantPosition($accountantPosition)
    {
        $this->accountantPosition = $accountantPosition;

        return $this;
    }

    public function getAccountantPosition()
    {
        return $this->accountantPosition;
    }
}
