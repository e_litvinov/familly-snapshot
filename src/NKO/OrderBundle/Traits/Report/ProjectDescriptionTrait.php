<?php

namespace NKO\OrderBundle\Traits\Report;

use NKO\OrderBundle\Entity\BaseReport;

trait ProjectDescriptionTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="purpose", type="text", nullable=true)
     */
    private $purpose;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\TrainingEvent", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid
     */
    protected $events;

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return BaseReport
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Add event
     *
     * @param \NKO\OrderBundle\Entity\TrainingEvent $event
     *
     * @return BaseReport
     */
    public function addEvent(\NKO\OrderBundle\Entity\TrainingEvent $event)
    {
        $event->setReport($this);
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \NKO\OrderBundle\Entity\TrainingEvent $event
     *
     * @return BaseReport
     */
    public function removeEvent(\NKO\OrderBundle\Entity\TrainingEvent $event)
    {
        $this->events->removeElement($event);

        return $this;
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
