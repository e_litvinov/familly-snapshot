<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity;
use NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity;
use NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity;

trait ScheduleTrait
{
    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $practiceImplementationActivities;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $practiceSpreadActivities;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $monitoringResultsActivities;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     *
     * @ORM\Column(name="practice_implementation_description", type="text", nullable=true)
     */
    private $practiceImplementationDescription;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     *
     * @ORM\Column(name="practice_spread_description", type="text", nullable=true)
     */
    private $practiceSpreadDescription;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     *
     * @ORM\Column(name="monitoring_results_description", type="text", nullable=true)
     */
    private $monitoringResultsDescription;

    /**
     * Add practiceImplementationActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity
     */
    public function addPracticeImplementationActivity(\NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity)
    {
        $practiceImplementationActivity->setFarvater2017Application($this);
        $this->practiceImplementationActivities[] = $practiceImplementationActivity;

        return $this;
    }

    /**
     * Remove practiceImplementationActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity
     */
    public function removePracticeImplementationActivity(\NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity)
    {
        $this->practiceImplementationActivities->removeElement($practiceImplementationActivity);
    }

    /**
     * Get practiceImplementationActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeImplementationActivities()
    {
        return $this->practiceImplementationActivities;
    }

    /**
     * Add practiceSpreadActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity $practiceSpreadActivity
     */
    public function addPracticeSpreadActivity(\NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity $practiceSpreadActivity)
    {
        $practiceSpreadActivity->setFarvater2017Application($this);
        $this->practiceSpreadActivities[] = $practiceSpreadActivity;

        return $this;
    }

    /**
     * Remove practiceSpreadActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity $practiceSpreadActivity
     */
    public function removePracticeSpreadActivity(\NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity $practiceSpreadActivity)
    {
        $this->practiceSpreadActivities->removeElement($practiceSpreadActivity);
    }

    /**
     * Get practiceSpreadActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSpreadActivities()
    {
        return $this->practiceSpreadActivities;
    }

    /**
     * Add monitoringResultsActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity $monitoringResultsActivity
     */
    public function addMonitoringResultsActivity(\NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity $monitoringResultsActivity)
    {
        $monitoringResultsActivity->setFarvater2017Application($this);
        $this->monitoringResultsActivities[] = $monitoringResultsActivity;

        return $this;
    }

    /**
     * Remove monitoringResultsActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity $monitoringResultsActivity
     */
    public function removeMonitoringResultsActivity(\NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity $monitoringResultsActivity)
    {
        $this->monitoringResultsActivities->removeElement($monitoringResultsActivity);
    }

    /**
     * Get monitoringResultsActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResultsActivities()
    {
        return $this->monitoringResultsActivities;
    }

    /**
     * Set practiceImplementationDescription
     *
     * @param string $practiceImplementationDescription
     */
    public function setPracticeImplementationDescription($practiceImplementationDescription)
    {
        $this->practiceImplementationDescription = $practiceImplementationDescription;

        return $this;
    }

    /**
     * Get practiceImplementationDescription
     *
     * @return string
     */
    public function getPracticeImplementationDescription()
    {
        return $this->practiceImplementationDescription;
    }

    /**
     * Set practiceSpreadDescription
     *
     * @param string $practiceSpreadDescription
     */
    public function setPracticeSpreadDescription($practiceSpreadDescription)
    {
        $this->practiceSpreadDescription = $practiceSpreadDescription;

        return $this;
    }

    /**
     * Get practiceSpreadDescription
     *
     * @return string
     */
    public function getPracticeSpreadDescription()
    {
        return $this->practiceSpreadDescription;
    }

    /**
     * Set monitoringResultsDescription
     *
     * @param string $monitoringResultsDescription
     */
    public function setMonitoringResultsDescription($monitoringResultsDescription)
    {
        $this->monitoringResultsDescription = $monitoringResultsDescription;

        return $this;
    }

    /**
     * Get monitoringResultsDescription
     *
     * @return string
     */
    public function getMonitoringResultsDescription()
    {
        return $this->monitoringResultsDescription;
    }
}