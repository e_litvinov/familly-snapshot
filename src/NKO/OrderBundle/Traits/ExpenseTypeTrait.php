<?php

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\ExpenseType;

trait ExpenseTypeTrait
{
    public static function addReportForm($object)
    {
        if ($object && $object->getReportForm()) {

            ExpenseType::$reportForm = ExpenseType::$reportForm !== $object->getReportForm() ? $object->getReportForm() : ExpenseType::$reportForm;
        }
    }
}
