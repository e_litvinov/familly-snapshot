<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 8/11/17
 * Time: 3:56 PM
 */

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BeneficiaryResult;
use NKO\OrderBundle\Entity\Employee;
use NKO\OrderBundle\Entity\EmployeeResult;
use NKO\OrderBundle\Entity\Measure;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\ProjectResult;
use NKO\OrderBundle\Entity\Publication;
use NKO\OrderBundle\Entity\Risk;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;
use NKO\OrderBundle\Entity\Traineeship;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;

trait KnsApplicationTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2018", "KNS-2019", "KNS-2020"
     * })
     *
     * @ORM\Column(name="choosing_ground_explanation", type="text", nullable=true, length=4294967295)
     */
    protected $choosingGroundExplanation;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2017", "KNS-2018", "KNS-2019", "KNS-2020"
     * })
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The money must be integer type",
     *     groups={
     *         "KNS-2017", "KNS-2018", "KNS-2019"
     * })
     *
     * @ORM\Column(name="requested_financing_money", type="string", length=10, nullable=true)
     */
    protected $requestedFinancingMoney;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2017", "KNS-2018", "KNS-2019", "KNS-2020"
     *     }
     * )
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="The money must be integer type",
     *     groups={
     *          "KNS-2017", "KNS-2018", "KNS-2019"
     *     }
     * )
     *
     * @ORM\Column(name="cofinancing_money", type="string", length=10, nullable=true)
     */
    protected $cofinancingMoney;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^(http|https):\/\//",
     *     message="The url is not valid",
     *     groups={
     *             "KNS-2017", "KNS-2018", "KNS-2019", "KNS2018-3", "KNS-2020"
     *         }
     *     )
     * @ORM\Column(name="link_to_annual_report", type="text", nullable=true)
     */
    protected $linkToAnnualReport;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Publication", mappedBy="KNS2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Count(
     *     max="5",
     *     maxMessage="Max count publications is 5",
     *     groups={
     *         "KNS2018-3"
     *     }
     * )
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $publications;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "KNS-2018", "KNS-2017", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *         }
     *     )
     *
     * @ORM\Column(name="projectPurpose", type="text", nullable=true)
     */
    protected $projectPurpose;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "KNS-2017", "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *         }
     *     )
     *
     * @ORM\Column(name="projectRelevance", type="text", nullable=true)
     */
    protected $projectRelevance;
    
    /**
     * @var string
     *
     * @ORM\Column(name="beneficiary_group_name", type="string", length=150, nullable=true)
     */
    private $beneficiaryGroupEtc;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *        "KNS-2017", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *      }
     *     )
     *
     * @ORM\Column(name="projectImplementation", type="text", nullable=true)
     */
    protected $projectImplementation;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "KNS-2017", "KNS-2018", "KNS-2019", "KNS-2020"
     * })
     *
     * @ORM\Column(name="dateStartOfInternship", type="datetime", nullable=true)
     */
    protected $dateStartOfInternship;

    /**
     * @var string
     *
     * @ORM\Column(name="traineeship_format_name", type="text", nullable=true)
     */
    protected $traineeshipFormatName;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Employee", mappedBy="KNS2017Application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $employees;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "KNS-2017", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *         }
     *     )
     *
     * @ORM\Column(name="knowledge_introduction_during_project_implementation", type="text", nullable=true)
     */
    protected $knowledgeIntroductionDuringProjectImplementation;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "KNS-2017", "KNS-2018", "KNS2017-2", "ContinuationApplicationKNS", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"
     *         }
     *     )
     *
     * @ORM\Column(name="knowledge_introduction_after_project_implementation", type="text", nullable=true)
     */
    protected $knowledgeIntroductionAfterProjectImplementation;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Traineeship", mappedBy="KNS2017Application",
     *      cascade={"all"}, orphanRemoval=true)
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $traineeships;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Measure", mappedBy="KNS2017Application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $measures;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ProjectResult", mappedBy="KNS2017Application",
     *      cascade={"all"}, orphanRemoval=true )
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $projectResults;

    /**
     * @var string
     *
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={
     *         "KNS-2017", "KNS-2018"
     *     }
     * )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={
     *          "KNS-2017", "KNS-2018"
     *     }
     * )
     * @ORM\Column(name="organization_creation_resolution", type="string", nullable=true)
     */
    protected $organizationCreationResolution;

    protected $_organizationCreationResolution;

    /**
     * @var string
     *
     * @ApplicationAssert\ExcelExtension(
     *     groups={
     *          "KNS-2017", "KNS-2018"
     *     }
     * )
     * @Assert\File(
     *     maxSize = "20480k",
     *     mimeTypesMessage = "Please upload a valid excel file",
     *     maxSizeMessage = "file must not exceed 20MB",
     *     notFoundMessage = "file not found",
     *     groups={
     *         "KNS-2017", "KNS-2018"
     *     }
     * )
     * @ORM\Column(name="budget", type="string", nullable=true)
     */
    protected $budget;

    protected $_budget;

    protected function preCreateInstances()
    {
        $siteLink = new SiteLink();
        $this->addSiteLink($siteLink);
        $socialNetworkLink = new SocialNetworkLink();
        $this->addSocialNetworkLink($socialNetworkLink);
        $project = new Project();
        $this->addProject($project);
        $publication = new Publication();
        $this->addPublication($publication);
        $employee = new Employee();
        $this->addEmployee($employee);
        $traineeship = new Traineeship();
        $this->addTraineeship($traineeship);
        $measure = new Measure();
        $this->addMeasure($measure);
        $beneficiaryResult = new BeneficiaryResult();
        $this->addBeneficiaryResult($beneficiaryResult);
        $employeeResult = new EmployeeResult();
        $this->addEmployeeResult($employeeResult);
        $risk = new Risk();
        $this->addRisk($risk);
    }

    public function preCreateProjectResults()
    {
        $projectResult1 = new ProjectResult();
        $projectResult1->setResultCriteria('Число обученных сотрудников организации (членов общественного объединения)');
        $this->addProjectResult($projectResult1);
        $projectResult2 = new ProjectResult();
        $projectResult2->setResultCriteria('Число сотрудников, организации (членов общественного объединения), которые получат новые знания благодаря реализации проекта');
        $this->addProjectResult($projectResult2);
        $projectResult3 = new ProjectResult();
        $projectResult3->setResultCriteria('Число внедренных практик (технологий, услуг, моделей и пр.) в деятельность организации, благодаря реализации проекта (уточните также, что именно)');
        $this->addProjectResult($projectResult3);
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2017"}
     *     )
     */
    public function isValidBeneficiaryGroup(ExecutionContextInterface $context, $payload)
    {
        if (count($this->beneficiaryGroups)==0 && !$this->beneficiaryGroupEtc) {
            $context->buildViolation('please, choose beneficiary group')
                ->atPath('beneficiaryGroups')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"KNS-2017", "KNS-2018", "KNS2017-2", "KNS-2019", "KNS2018-3", "KNS-2020", "KNS2019-2"})
     */
    public function isValidTrainingGrounds(ExecutionContextInterface $context, $payload)
    {
        $isChoosed = false;
        /**
         * @var TrainingGround $trainingGround
         */
        foreach ($this->trainingGrounds as $trainingGround) {
            if ($trainingGround->getParent()) {
                $isChoosed = true;
                break;
            }
        }
        if (!$isChoosed || count($this->getTrainingGrounds()) < 2) {
            $context->buildViolation('please, choose training grounds category')
                ->atPath('trainingGrounds')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2017", "KNS-2018"}
     *     )
     */
    public function isValidBudget(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getBudget() && !$this->_budget) {
            $context->buildViolation('file not found')
                ->atPath('budget')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"KNS-2017", "KNS-2018", "KNS-2019", "KNS-2020"})
     */
    public function isValidTraineeshipFormats(ExecutionContextInterface $context, $payload)
    {
        if (count($this->traineeshipFormats)==0 && !$this->traineeshipFormatName) {
            $context->buildViolation('please, choose traineeship format')
                ->atPath('traineeshipFormats')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(groups={"KNS-2017", "KNS-2018", "KNS-2019", "KNS-2020"})
     */
    public function isValidSocialResults(ExecutionContextInterface $context, $payload)
    {
        if ($this->socialResults->isEmpty()) {
            $context->buildViolation('please, choose social result')
                ->atPath('socialResults')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requestedFinancingMoney
     *
     * @param string $requestedFinancingMoney
     *
     * @return BaseApplication
     */
    public function setRequestedFinancingMoney($requestedFinancingMoney)
    {
        $this->requestedFinancingMoney = $requestedFinancingMoney;

        return $this;
    }

    /**
     * Get requestedFinancingMoney
     *
     * @return string
     */
    public function getRequestedFinancingMoney()
    {
        return $this->requestedFinancingMoney;
    }

    /**
     * Set cofinancingMoney
     *
     * @param string $cofinancingMoney
     *
     * @return BaseApplication
     */
    public function setCofinancingMoney($cofinancingMoney)
    {
        $this->cofinancingMoney = $cofinancingMoney;

        return $this;
    }

    /**
     * Get cofinancingMoney
     *
     * @return string
     */
    public function getCofinancingMoney()
    {
        return $this->cofinancingMoney;
    }


    /**
     * Set linkToAnnualReport
     *
     * @param string $linkToAnnualReport
     *
     * @return BaseApplication
     */
    public function setLinkToAnnualReport($linkToAnnualReport)
    {
        $this->linkToAnnualReport = $linkToAnnualReport;

        return $this;
    }

    /**
     * Get linkToAnnualReport
     *
     * @return string
     */
    public function getLinkToAnnualReport()
    {
        return $this->linkToAnnualReport;
    }

    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     *
     * @return BaseApplication
     */
    public function addPublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $publication->setKNS2017Application($this);
        $this->publications[] = $publication;

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Publication $publication
     */
    public function removePublication(\NKO\OrderBundle\Entity\Publication $publication)
    {
        $this->publications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublications()
    {
        return $this->publications;
    }

    /**
     * Set projectPurpose
     *
     * @param string $projectPurpose
     *
     * @return BaseApplication
     */
    public function setProjectPurpose($projectPurpose)
    {
        $this->projectPurpose = $projectPurpose;

        return $this;
    }

    /**
     * Get projectPurpose
     *
     * @return string
     */
    public function getProjectPurpose()
    {
        return $this->projectPurpose;
    }

    /**
     * Set projectRelevance
     *
     * @param string $projectRelevance
     *
     * @return BaseApplication
     */
    public function setProjectRelevance($projectRelevance)
    {
        $this->projectRelevance = $projectRelevance;

        return $this;
    }

    /**
     * Get projectRelevance
     *
     * @return string
     */
    public function getProjectRelevance()
    {
        return $this->projectRelevance;
    }


    /**
     * Set beneficiaryGroupEtc
     *
     * @param string $beneficiaryGroupEtc
     *
     * @return BaseApplication
     */
    public function setBeneficiaryGroupEtc($beneficiaryGroupEtc)
    {
        $this->beneficiaryGroupEtc = $beneficiaryGroupEtc;

        return $this;
    }

    /**
     * Get beneficiaryGroupEtc
     *
     * @return string
     */
    public function getBeneficiaryGroupEtc()
    {
        return $this->beneficiaryGroupEtc;
    }

    /**
     * Set projectImplementation
     *
     * @param string $projectImplementation
     *
     * @return BaseApplication
     */
    public function setProjectImplementation($projectImplementation)
    {
        $this->projectImplementation = $projectImplementation;

        return $this;
    }

    /**
     * Get projectImplementation
     *
     * @return string
     */
    public function getProjectImplementation()
    {
        return $this->projectImplementation;
    }


    /**
     * Set dateStartOfInternship
     *
     * @param \DateTime $dateStartOfInternship
     *
     * @return BaseApplication
     */
    public function setDateStartOfInternship($dateStartOfInternship)
    {
        $this->dateStartOfInternship = $dateStartOfInternship;

        return $this;
    }

    /**
     * Get dateStartOfInternship
     *
     * @return \DateTime
     */
    public function getDateStartOfInternship()
    {
        return $this->dateStartOfInternship;
    }


    /**
     * Set traineeshipFormatName
     *
     * @param string $traineeshipFormatName
     *
     * @return BaseApplication
     */
    public function setTraineeshipFormatName($traineeshipFormatName)
    {
        $this->traineeshipFormatName = $traineeshipFormatName;

        return $this;
    }

    /**
     * Get traineeshipFormatName
     *
     * @return string
     */
    public function getTraineeshipFormatName()
    {
        return $this->traineeshipFormatName;
    }


    /**
     * Add employee
     *
     * @param \NKO\OrderBundle\Entity\Employee $employee
     *
     * @return BaseApplication
     */
    public function addEmployee(\NKO\OrderBundle\Entity\Employee $employee)
    {
        $employee->setKNS2017Application($this);
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \NKO\OrderBundle\Entity\Employee $employee
     */
    public function removeEmployee(\NKO\OrderBundle\Entity\Employee $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Set knowledgeIntroductionDuringProjectImplementation
     *
     * @param string $knowledgeIntroductionDuringProjectImplementation
     *
     * @return BaseApplication
     */
    public function setKnowledgeIntroductionDuringProjectImplementation($knowledgeIntroductionDuringProjectImplementation)
    {
        $this->knowledgeIntroductionDuringProjectImplementation = $knowledgeIntroductionDuringProjectImplementation;

        return $this;
    }

    /**
     * Get knowledgeIntroductionDuringProjectImplementation
     *
     * @return string
     */
    public function getKnowledgeIntroductionDuringProjectImplementation()
    {
        return $this->knowledgeIntroductionDuringProjectImplementation;
    }

    /**
     * Set knowledgeIntroductionAfterProjectImplementation
     *
     * @param string $knowledgeIntroductionAfterProjectImplementation
     *
     * @return BaseApplication
     */
    public function setKnowledgeIntroductionAfterProjectImplementation($knowledgeIntroductionAfterProjectImplementation)
    {
        $this->knowledgeIntroductionAfterProjectImplementation = $knowledgeIntroductionAfterProjectImplementation;

        return $this;
    }

    /**
     * Get knowledgeIntroductionAfterProjectImplementation
     *
     * @return string
     */
    public function getKnowledgeIntroductionAfterProjectImplementation()
    {
        return $this->knowledgeIntroductionAfterProjectImplementation;
    }

    /**
     * Add traineeship
     *
     * @param \NKO\OrderBundle\Entity\Traineeship $traineeship
     *
     * @return BaseApplication
     */
    public function addTraineeship(\NKO\OrderBundle\Entity\Traineeship $traineeship)
    {
        $traineeship->setKNS2017Application($this);
        $this->traineeships[] = $traineeship;

        return $this;
    }

    /**
     * Remove traineeship
     *
     * @param \NKO\OrderBundle\Entity\Traineeship $traineeship
     */
    public function removeTraineeship(\NKO\OrderBundle\Entity\Traineeship $traineeship)
    {
        $this->traineeships->removeElement($traineeship);
    }

    /**
     * Get traineeships
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTraineeships()
    {
        return $this->traineeships;
    }

    /**
     * Add measure
     *
     * @param \NKO\OrderBundle\Entity\Measure $measure
     *
     * @return BaseApplication
     */
    public function addMeasure(\NKO\OrderBundle\Entity\Measure $measure)
    {
        $measure->setKNS2017Application($this);
        $this->measures[] = $measure;

        return $this;
    }

    /**
     * Remove measure
     *
     * @param \NKO\OrderBundle\Entity\Measure $measure
     */
    public function removeMeasure(\NKO\OrderBundle\Entity\Measure $measure)
    {
        $this->measures->removeElement($measure);
    }

    /**
     * Get measures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    /**
     * Add projectResult
     *
     * @param \NKO\OrderBundle\Entity\ProjectResult $projectResult
     *
     * @return BaseApplication
     */
    public function addProjectResult(\NKO\OrderBundle\Entity\ProjectResult $projectResult)
    {
        $projectResult->setKNS2017Application($this);
        $this->projectResults[] = $projectResult;

        return $this;
    }

    /**
     * Remove projectResult
     *
     * @param \NKO\OrderBundle\Entity\ProjectResult $projectResult
     */
    public function removeProjectResult(\NKO\OrderBundle\Entity\ProjectResult $projectResult)
    {
        $this->projectResults->removeElement($projectResult);
    }

    /**
     * Get projectResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectResults()
    {
        return $this->projectResults;
    }

    /**
     * Set budget
     *
     * @param string $budget
     *
     * @return BaseApplication
     */
    public function setBudget($budget)
    {
        if (!$this->_budget && is_string($this->budget)) {
            $this->_budget = $this->budget;
        }
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }


    /**
     * Set organizationCreationResolution
     *
     * @param string $organizationCreationResolution
     *
     * @return BaseApplication
     */
    public function setOrganizationCreationResolution($organizationCreationResolution)
    {
        if (!$this->_organizationCreationResolution && is_string($this->organizationCreationResolution)) {
            $this->_organizationCreationResolution = $this->organizationCreationResolution;
        }
        $this->organizationCreationResolution = $organizationCreationResolution;

        return $this;
    }

    /**
     * Get organizationCreationResolution
     *
     * @return string
     */
    public function getOrganizationCreationResolution()
    {
        return $this->organizationCreationResolution;
    }

    /**
     * Set choosingGroundExplanation
     *
     * @param string $choosingGroundExplanation
     *
     * @return BaseApplication
     */
    public function setChoosingGroundExplanation($choosingGroundExplanation)
    {
        $this->choosingGroundExplanation = $choosingGroundExplanation;

        return $this;
    }

    /**
     * Get choosingGroundExplanation
     *
     * @return string
     */
    public function getChoosingGroundExplanation()
    {
        return $this->choosingGroundExplanation;
    }

    /**
     * Add beneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup
     *
     * @return BaseApplication
     */
    public function addBeneficiaryGroup(\NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup)
    {
        $this->beneficiaryGroups[] = $beneficiaryGroup;

        return $this;
    }

    /**
     * Remove beneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup
     */
    public function removeBeneficiaryGroup(\NKO\OrderBundle\Entity\BeneficiaryGroup $beneficiaryGroup)
    {
        $this->beneficiaryGroups->removeElement($beneficiaryGroup);
    }

    /**
     * Get beneficiaryGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryGroups()
    {
        return $this->beneficiaryGroups;
    }

    /**
     * Add traineeshipFormat
     *
     * @param \NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat
     *
     * @return BaseApplication
     */
    public function addTraineeshipFormat(\NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat)
    {
        $this->traineeshipFormats[] = $traineeshipFormat;

        return $this;
    }

    /**
     * Remove traineeshipFormat
     *
     * @param \NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat
     */
    public function removeTraineeshipFormat(\NKO\OrderBundle\Entity\TraineeshipFormat $traineeshipFormat)
    {
        $this->traineeshipFormats->removeElement($traineeshipFormat);
    }

    /**
     * Get traineeshipFormats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTraineeshipFormats()
    {
        return $this->traineeshipFormats;
    }

    /**
     * Add trainingGround
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround
     *
     * @return BaseApplication
     */
    public function addTrainingGround(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround)
    {
        $this->trainingGrounds[] = $trainingGround;

        return $this;
    }

    /**
     * Remove trainingGround
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround
     */
    public function removeTrainingGround(\NKO\OrderBundle\Entity\KNS2017\TrainingGround $trainingGround)
    {
        $this->trainingGrounds->removeElement($trainingGround);
    }

    /**
     * Get trainingGrounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingGrounds()
    {
        return $this->trainingGrounds;
    }

    /**
     * Add socialResult
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult
     *
     * @return BaseApplication
     */
    public function addSocialResult(\NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult)
    {
        $this->socialResults[] = $socialResult;

        return $this;
    }

    /**
     * Remove socialResult
     *
     * @param \NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult
     */
    public function removeSocialResult(\NKO\OrderBundle\Entity\KNS2017\SocialResult $socialResult)
    {
        $this->socialResults->removeElement($socialResult);
    }

    /**
     * Get socialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialResults()
    {
        return $this->socialResults;
    }
}
