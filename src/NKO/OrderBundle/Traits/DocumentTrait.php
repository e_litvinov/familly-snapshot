<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Entity\Farvater2017\OrganizationResource;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;

trait DocumentTrait
{
    /**
     * @var string
     *
     * @ApplicationAssert\ExcelExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid Excel file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="project_budget", type="text", nullable=true)
     */
    protected $projectBudget;

    protected $_projectBudget;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="bank_letter_existence_account", type="text", nullable=true)
     */
    protected $bankLetterExistenceAccount;

    protected $_bankLetterExistenceAccount;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="processing_personal_data_consent", type="text", nullable=true)
     */
    protected $processingPersonalDataConsent;

    protected $_processingPersonalDataConsent;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     * )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="confirming_authority_person_document", type="text", nullable=true)
     */
    protected $confirmingAuthorityPersonDocument;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     * )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="confirming_legal_status_entity_document", type="text", nullable=true)
     */
    protected $confirmingLegalStatusEntityDocument;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     * )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="latest_annual_report", type="text", nullable=true)
     */
    protected $latestAnnualReport;

    /**
     * @var string
     *
     * @ORM\Column(name="latest_annual_report_link", type="string", length=500, nullable=true)
     */
    private $latestAnnualReportLink;

    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"Farvater-2017", "Farvater-2018"}
     * )
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     * @ORM\Column(name="ministry_justice_report", type="text", nullable=true)
     */
    protected $ministryJusticeReport;

    /**
     * @var string
     *
     * @ORM\Column(name="ministry_justice_report_link", type="string", length=500, nullable=true)
     */
    private $ministryJusticeReportLink;

    /**
     * Set projectBudget
     *
     * @param string $projectBudget
     *
     * @return Application
     */
    public function setProjectBudget($projectBudget)
    {
        if(!$this->_projectBudget && is_string($this->projectBudget)){
            $this->_projectBudget = $this->projectBudget;
        }

        $this->projectBudget = $projectBudget;

        return $this;
    }

    /**
     * Get projectBudget
     *
     * @return string
     */
    public function getProjectBudget()
    {
        return $this->projectBudget;
    }

    /**
     * Set bankLetterExistenceAccount
     *
     * @param string $bankLetterExistenceAccount
     *
     * @return Application
     */
    public function setBankLetterExistenceAccount($bankLetterExistenceAccount)
    {
        if(!$this->_bankLetterExistenceAccount && is_string($this->bankLetterExistenceAccount)){
            $this->_bankLetterExistenceAccount = $this->bankLetterExistenceAccount;
        }
        $this->bankLetterExistenceAccount = $bankLetterExistenceAccount;

        return $this;
    }

    /**
     * Get bankLetterExistenceAccount
     *
     * @return string
     */
    public function getBankLetterExistenceAccount()
    {
        return $this->bankLetterExistenceAccount;
    }

    /**
     * Set processingPersonalDataConsent
     *
     * @param string $processingPersonalDataConsent
     *
     * @return Application
     */
    public function setProcessingPersonalDataConsent($processingPersonalDataConsent)
    {
        if(!$this->_processingPersonalDataConsent && is_string($this->processingPersonalDataConsent)){
            $this->_processingPersonalDataConsent = $this->processingPersonalDataConsent;
        }

        $this->processingPersonalDataConsent = $processingPersonalDataConsent;

        return $this;
    }

    /**
     * Get processingPersonalDataConsent
     *
     * @return string
     */
    public function getProcessingPersonalDataConsent()
    {
        return $this->processingPersonalDataConsent;
    }

    /**
     * Set confirmingAuthorityPersonDocument
     *
     * @param string $confirmingAuthorityPersonDocument
     *
     * @return Application
     */
    public function setConfirmingAuthorityPersonDocument($confirmingAuthorityPersonDocument)
    {
        $this->confirmingAuthorityPersonDocument = $confirmingAuthorityPersonDocument;

        return $this;
    }

    /**
     * Get confirmingAuthorityPersonDocument
     *
     * @return string
     */
    public function getConfirmingAuthorityPersonDocument()
    {
        return $this->confirmingAuthorityPersonDocument;
    }

    /**
     * Set confirmingLegalStatusEntityDocument
     *
     * @param string $confirmingLegalStatusEntityDocument
     *
     * @return Application
     */
    public function setConfirmingLegalStatusEntityDocument($confirmingLegalStatusEntityDocument)
    {
        $this->confirmingLegalStatusEntityDocument = $confirmingLegalStatusEntityDocument;

        return $this;
    }

    /**
     * Get confirmingLegalStatusEntityDocument
     *
     * @return string
     */
    public function getConfirmingLegalStatusEntityDocument()
    {
        return $this->confirmingLegalStatusEntityDocument;
    }

    /**
     * Set latestAnnualReport
     *
     * @param string $latestAnnualReport
     *
     * @return Application
     */
    public function setLatestAnnualReport($latestAnnualReport)
    {
        $this->latestAnnualReport = $latestAnnualReport;

        return $this;
    }

    /**
     * Get latestAnnualReport
     *
     * @return string
     */
    public function getLatestAnnualReport()
    {
        return $this->latestAnnualReport;
    }

    /**
     * Set latestAnnualReportLink
     *
     * @param string $latestAnnualReportLink
     *
     * @return Application
     */
    public function setLatestAnnualReportLink($latestAnnualReportLink)
    {
        $this->latestAnnualReportLink = $latestAnnualReportLink;

        return $this;
    }

    /**
     * Get latestAnnualReportLink
     *
     * @return string
     */
    public function getLatestAnnualReportLink()
    {
        return $this->latestAnnualReportLink;
    }

    /**
     * Set ministryJusticeReport
     *
     * @param string $ministryJusticeReport
     *
     * @return Application
     */
    public function setMinistryJusticeReport($ministryJusticeReport)
    {
        $this->ministryJusticeReport = $ministryJusticeReport;

        return $this;
    }

    /**
     * Get ministryJusticeReport
     *
     * @return string
     */
    public function getMinistryJusticeReport()
    {
        return $this->ministryJusticeReport;
    }

    /**
     * Set ministryJusticeReportLink
     *
     * @param string $ministryJusticeReportLink
     *
     * @return Application
     */
    public function setMinistryJusticeReportLink($ministryJusticeReportLink)
    {
        $this->ministryJusticeReportLink = $ministryJusticeReportLink;

        return $this;
    }

    /**
     * Get ministryJusticeReportLink
     *
     * @return string
     */
    public function getMinistryJusticeReportLink()
    {
        return $this->ministryJusticeReportLink;
    }

    /**
     * @Assert\Callback(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     */
    public function isValidProjectBudget(ExecutionContextInterface $context, $payload)
    {
        if(!$this->projectBudget && !$this->_projectBudget)
            $context->buildViolation('Необходимо загрузить excel-файл')
                ->atPath('projectBudget')
                ->addViolation();
    }

    /**
     * @Assert\Callback(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     */
    public function isValidBankLetterExistenceAccount(ExecutionContextInterface $context, $payload)
    {
        if(!$this->bankLetterExistenceAccount && !$this->_bankLetterExistenceAccount)
            $context->buildViolation('please, upload file')
                ->atPath('bankLetterExistenceAccount')
                ->addViolation();
    }

    /**
     * @Assert\Callback(
     *     groups={"Farvater-2017", "Farvater-2018"}
     *     )
     */
    public function isValidProcessingPersonalDataConsent(ExecutionContextInterface $context, $payload)
    {
        if(!$this->processingPersonalDataConsent && !$this->_processingPersonalDataConsent)
            $context->buildViolation('please, upload file')
                ->atPath('processingPersonalDataConsent')
                ->addViolation();
    }
}