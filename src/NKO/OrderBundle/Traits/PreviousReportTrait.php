<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 17:38
 */

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Validator\Constraints\PdfExtension;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

trait PreviousReportTrait
{
    /**
     * @var string
     *
     * @PdfExtension()
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     notFoundMessage = "file not found",
     *     )
     *
     * @ORM\Column(name="generated_pdf", type="string", nullable=true)
     */
    private $generatedPdf;

    public function setGeneratedPdf($generatedPdf)
    {
        $this->generatedPdf = $generatedPdf;

        return $this;
    }

    public function getGeneratedPdf()
    {
        return $this->generatedPdf;
    }
}