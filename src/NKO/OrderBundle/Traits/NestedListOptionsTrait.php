<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 17:38
 */

namespace NKO\OrderBundle\Traits;

use Symfony\Component\PropertyAccess\PropertyAccess;

trait NestedListOptionsTrait
{
    public function getOptionsByParent($objects, $property)
    {
        if (!$objects) {
            return array();
        }
        $choices = [];
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($objects as $object) {
            $children = $object->getChildren();
            $title = $accessor->getValue($object, $property);
            if ($children->isEmpty() && !$object->getParent()) {
                $choices[$title] = $object;
            }
            elseif (!$children->isEmpty()) {
                foreach ($children as $child) {
                    $choices[$title][] = $child;
                }
            }
        }
        return $choices;
    }

    public function getOptionsByChildren($objects, $property)
    {
        if (!$objects) {
            return array();
        }
        $choices = [];
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($objects as $object) {
            $title = $accessor->getValue($object, $property);
            $parent = $object->getParent();
            if ($parent) {
                $title = $accessor->getValue($parent, $property);
                $choices[$title][] = $object;
            }
            else {
                $choices[$title] = $object;
            }
        }
        return $choices;
    }

    public function getChildOptions($objects, $property)
    {
        if (!$objects) {
            return array();
        }
        $choices = [];
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($objects as $object) {
            $children = $object->getChildren();
            $title = $accessor->getValue($object, $property);
            if ($children->isEmpty() && !$object->getParent()) {
                $choices[$title] = $object;
            }
            elseif (!$children->isEmpty()) {
                foreach ($children as $child) {
                    $choices[$title][] = $child;
                }
            }
        }
        return $choices;
    }
}