<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Entity\Farvater2017\OrganizationResource;

trait ResourceTrait
{
    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\ProjectMember", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $projectMembers;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\ProjectPartner", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $projectPartners;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\OrganizationResource", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     *
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $organizationResources;

    /**
     * Add projectMember
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\ProjectMember $projectMember
     *
     * @return Application
     */
    public function addProjectMember(\NKO\OrderBundle\Entity\Farvater2017\ProjectMember $projectMember)
    {
        $projectMember->setFarvater2017Application($this);
        $this->projectMembers[] = $projectMember;

        return $this;
    }

    /**
     * Remove projectMember
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\ProjectMember $projectMember
     */
    public function removeProjectMember(\NKO\OrderBundle\Entity\Farvater2017\ProjectMember $projectMember)
    {
        $this->projectMembers->removeElement($projectMember);
    }

    /**
     * Get projectMembers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectMembers()
    {
        return $this->projectMembers;
    }

    /**
     * Add projectPartner
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\ProjectPartner $projectPartner
     *
     * @return Application
     */
    public function addProjectPartner(\NKO\OrderBundle\Entity\Farvater2017\ProjectPartner $projectPartner)
    {
        $projectPartner->setFarvater2017Application($this);
        $this->projectPartners[] = $projectPartner;

        return $this;
    }

    /**
     * Remove projectPartner
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\ProjectPartner $projectPartner
     */
    public function removeProjectPartner(\NKO\OrderBundle\Entity\Farvater2017\ProjectPartner $projectPartner)
    {
        $this->projectPartners->removeElement($projectPartner);
    }

    /**
     * Get projectPartners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectPartners()
    {
        return $this->projectPartners;
    }

    /**
     * Add organizationResource
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource
     */
    public function addOrganizationResource(\NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource)
    {
        $organizationResource->setFarvater2017Application($this);
        $this->organizationResources[] = $organizationResource;

        return $this;
    }

    /**
     * Remove organizationResource
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource
     */
    public function removeOrganizationResource(\NKO\OrderBundle\Entity\Farvater2017\OrganizationResource $organizationResource)
    {
        $this->organizationResources->removeElement($organizationResource);
    }

    /**
     * Get organizationResources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizationResources()
    {
        return $this->organizationResources;
    }

    public function preOrganizationResources()
    {
        $organizationResource1 = new OrganizationResource();
        $organizationResource1->setName('1. Помещение');
        $this->addOrganizationResource($organizationResource1);
        $organizationResource2 = new OrganizationResource();
        $organizationResource2->setName('2. Оборудование');
        $this->addOrganizationResource($organizationResource2);
        $organizationResource3 = new OrganizationResource();
        $organizationResource3->setName('3. Финансовые средства');
        $this->addOrganizationResource($organizationResource3);
        $organizationResource4 = new OrganizationResource();
        $organizationResource4->setName('4. Труд добровольцев');
        $this->addOrganizationResource($organizationResource4);
        $organizationResource5 = new OrganizationResource();
        $organizationResource5->setName('5. Иное (укажите) ');
        $this->addOrganizationResource($organizationResource5);
    }
}
