<?php

namespace NKO\OrderBundle\Traits\MixedReport;

use NKO\OrderBundle\Entity\Report\MixedReport\Attachment;

trait AttachmentTrait
{
    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\Attachment", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $analyticAttachments;

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnalyticAttachments()
    {
        return $this->analyticAttachments;
    }

    public function addAnalyticAttachment($analyticAttachment)
    {
        $this->analyticAttachments[] = $analyticAttachment;
        $analyticAttachment->setReport($this);

        return $this;
    }

    public function removeAnalyticAttachment(Attachment $event)
    {
        $this->analyticAttachments->removeElement($event);

        return $this;
    }
}
