<?php

namespace NKO\OrderBundle\Traits\MixedReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\MixedReport\Result;
use Symfony\Component\Validator\Constraints as Assert;

trait MixedResultsTrait
{
    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\Result", mappedBy="employeesResultMixedReport", orphanRemoval=true, cascade={"all"})
     */
    protected $mixedEmployeeResults;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\Result", mappedBy="beneficiariesResultMixedReport", orphanRemoval=true, cascade={"all"})
     */
    protected $mixedBeneficiariesResults;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MixedReport\Result", mappedBy="resultMixedReports", orphanRemoval=true, cascade={"all"})
     */
    protected $mixedResults;


    public function addMixedEmployeeResult(Result $individualResult)
    {
        $this->mixedEmployeeResults[] = $individualResult;
        $individualResult->setEmployeesResultMixedReport($this);

        return $this;
    }

    public function removeMixedEmployeeResult(Result $individualResult)
    {
        $this->mixedEmployeeResults->removeElement($individualResult);

        return $this;
    }

    public function getMixedEmployeeResults()
    {
        return $this->mixedEmployeeResults;
    }

    public function addMixedBeneficiariesResult(Result $individualResult)
    {
        $this->mixedBeneficiariesResults[] = $individualResult;
        $individualResult->setBeneficiariesResultMixedReport($this);

        return $this;
    }

    public function removeMixedBeneficiariesResult(Result $individualResult)
    {
        $this->mixedBeneficiariesResults->removeElement($individualResult);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMixedBeneficiariesResults()
    {
        return $this->mixedBeneficiariesResults;
    }

    public function addMixedResult(Result $individualResult)
    {
        $this->mixedResults[] = $individualResult;
        $individualResult->setResultMixedReports($this);

        return $this;
    }

    public function removeMixedResult(Result $individualResult)
    {
        $this->mixedResults->removeElement($individualResult);
    }

    public function getMixedResults()
    {
        return $this->mixedResults;
    }
}
