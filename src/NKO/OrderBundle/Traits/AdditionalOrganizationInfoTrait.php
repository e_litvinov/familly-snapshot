<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

trait AdditionalOrganizationInfoTrait
{
    /**
     * @Assert\NotBlank(
     *     groups={
     *         "AdditionalAddress"
     *     }
     * )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\SettingEntity\FederalDistrict")
     */
    protected $legalFederalDistrict;

    /**
     * @Assert\NotBlank(
     *     groups={
     *         "AdditionalAddress"
     *     }
     * )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $legalLocality;

    /**
     * @Assert\NotBlank(
     *     groups={
     *         "AdditionalAddress"
     *     }
     * )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\SettingEntity\FederalDistrict")
     */
    protected $actualFederalDistrict;

    /**
     * @Assert\NotBlank(
     *     groups={
     *         "AdditionalAddress"
     *     }
     * )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $actualLocality;

    public function getLegalFederalDistrict()
    {
        return $this->legalFederalDistrict;
    }

    public function setLegalFederalDistrict($legalFederalDistrict)
    {
        $this->legalFederalDistrict = $legalFederalDistrict;
        
        return $this;
    }

    public function getLegalLocality()
    {
        return $this->legalLocality;
    }

    public function setLegalLocality($legalLocality)
    {
        $this->legalLocality = $legalLocality;
        
        return $this;
    }

    public function getActualFederalDistrict()
    {
        return $this->actualFederalDistrict;
    }

    public function setActualFederalDistrict($actualFederalDistrict)
    {
        $this->actualFederalDistrict = $actualFederalDistrict;
        
        return $this;
    }

    public function getActualLocality()
    {
        return $this->actualLocality;
    }

    public function setActualLocality($actualLocality)
    {
        $this->actualLocality = $actualLocality;
        
        return $this;
    }
}
