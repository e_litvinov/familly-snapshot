<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.6.18
 * Time: 14.18
 */

namespace NKO\OrderBundle\Traits\AnalyticReport;


use NKO\OrderBundle\Entity\Report\AnalyticReport\Context;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement;

trait MonitoringTrait
{
    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext", mappedBy="reportMonitoringResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $newMonitoringElements;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="monitoringChange",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringChanges;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $monitoringDevelopments;


    /**
     * Add monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext $monitoringResult
     *
     * @return MonitoringTrait
     */
    public function addMonitoringResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext $monitoringResult)
    {
        $this->monitoringResults[] = $monitoringResult;
        $monitoringResult->setReportMonitoringResult($this);

        return $this;
    }

    /**
     * Remove monitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext $monitoringResult
     */
    public function removeMonitoringResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext $monitoringResult)
    {
        $this->monitoringResults->removeElement($monitoringResult);
    }

    /**
     * Get monitoringResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringResults()
    {
        return $this->monitoringResults;
    }

    /**
     * Add newMonitoringElement
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement $newMonitoringElement
     *
     * @return MonitoringTrait
     */
    public function addNewMonitoringElement(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement $newMonitoringElement)
    {
        $this->newMonitoringElements[] = $newMonitoringElement;
        $newMonitoringElement->setReport($this);

        return $this;
    }

    /**
     * Remove newMonitoringElement
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement $newMonitoringElement
     */
    public function removeNewMonitoringElement(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement $newMonitoringElement)
    {
        $this->newMonitoringElements->removeElement($newMonitoringElement);
    }

    /**
     * Get newMonitoringElements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewMonitoringElements()
    {
        return $this->newMonitoringElements;
    }

    /**
     * Add monitoringChange
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $monitoringChange
     *
     * @return MonitoringTrait
     */
    public function addMonitoringChange(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $monitoringChange)
    {
        $this->monitoringChanges[] = $monitoringChange;
        $monitoringChange->setMonitoringChange($this);

        return $this;
    }

    /**
     * Remove monitoringChange
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $monitoringChange
     */
    public function removeMonitoringChange(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $monitoringChange)
    {
        $this->monitoringChanges->removeElement($monitoringChange);
    }

    /**
     * Get monitoringChanges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringChanges()
    {
        return $this->monitoringChanges;
    }

    /**
     * Add monitoringDevelopment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment
     *
     * @return MonitoringTrait
     */
    public function addMonitoringDevelopment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment)
    {
        $this->monitoringDevelopments[] = $monitoringDevelopment;
        $monitoringDevelopment->setReport($this);

        return $this;
    }

    /**
     * Remove monitoringDevelopment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment
     */
    public function removeMonitoringDevelopment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment $monitoringDevelopment)
    {
        $this->monitoringDevelopments->removeElement($monitoringDevelopment);
    }

    /**
     * Get monitoringDevelopments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitoringDevelopments()
    {
        return $this->monitoringDevelopments;
    }
}