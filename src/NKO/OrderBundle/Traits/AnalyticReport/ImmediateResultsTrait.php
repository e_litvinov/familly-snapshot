<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 7.7.18
 * Time: 10.57
 */

namespace NKO\OrderBundle\Traits\AnalyticReport;

use NKO\OrderBundle\Entity\BaseReport as Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Utils\Report\DirectResultType;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints as Assert;

trait ImmediateResultsTrait
{
    /**
     *
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="reportPracticeResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceAnalyticResults;

    /**
     *
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="reportExpectedResult",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $expectedAnalyticResults;


    /**
     * Add practiceAnalyticResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceAnalyticResult
     *
     * @return Report
     */
    public function addPracticeAnalyticResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceAnalyticResult)
    {
        $practiceAnalyticResult->setType(DirectResultType::PRACTICE_RESULT);
        $practiceAnalyticResult->setReportPracticeResult($this);
        $this->practiceAnalyticResults[] = $practiceAnalyticResult;

        return $this;
    }

    /**
     * Remove practiceAnalyticResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceAnalyticResult
     */
    public function removePracticeAnalyticResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceAnalyticResult)
    {
        $this->practiceAnalyticResults->removeElement($practiceAnalyticResult);
    }

    /**
     * Get practiceAnalyticResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeAnalyticResults()
    {
        return $this->practiceAnalyticResults;
    }

    /**
     * set practiceAnalyticResults
     *
     * @return Report
     */
    public function setPracticeAnalyticResults($practiceAnalyticResults)
    {
        foreach ($practiceAnalyticResults as $practiceAnalyticResult) {
            $this->addPracticeAnalyticResult($practiceAnalyticResult);
        }
        return $this;
    }


    public function addExpectedAnalyticResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $expectedAnalyticResult)
    {
        $expectedAnalyticResult->setType(DirectResultType::EXPECTED_RESULT);
        $expectedAnalyticResult->setReportExpectedResult($this);
        $this->expectedAnalyticResults[] = $expectedAnalyticResult;

        return $this;
    }

    /**
     * Remove expectedAnalyticResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $expectedAnalyticResult
     */
    public function removeExpectedAnalyticResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $expectedAnalyticResult)
    {
        $this->expectedAnalyticResults->removeElement($expectedAnalyticResult);
    }

    /**
     * Get expectedAnalyticResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpectedAnalyticResults()
    {
        return $this->expectedAnalyticResults;
    }

    /**
     * set expectedAnalyticResults
     *
     * @return Report
     */
    public function setExpectedAnalyticResults($expectedAnalyticResults)
    {
        foreach ($expectedAnalyticResults as $expectedAnalyticResult) {
            $this->addExpectedAnalyticResult($expectedAnalyticResult);
        }
        return $this;
    }

    /**
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $result
     * @param string $nameOfCollection
     * @param string $linkedResult
     *
     * @return bool
     */
    public function isExistResult($result, $nameOfCollection, $linkedResult)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        /** @var  $analyticResult  DirectResult*/
        foreach ($propertyAccessor->getValue($this, $nameOfCollection) as $analyticResult) {
            if ($propertyAccessor->getValue($analyticResult, $linkedResult)->getId() === $result->getId()) {
                return true;
            }
        }

        return false;
    }
}
