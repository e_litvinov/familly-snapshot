<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 5.7.18
 * Time: 10.47
 */

namespace NKO\OrderBundle\Traits\AnalyticReport;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Publication;

trait PublicationLoaderTrait
{
    /**
     * create publications
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $object
     * @param array $indicators
     *
     */
    public function createPublications(BaseReport $object, $indicators)
    {
        foreach($indicators as $item) {
            $publication = new Publication();
            $publication->setIndicator($item);
            $object->addAnalyticPublication($publication);
        }
    }
}