<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 7/2/18
 * Time: 4:34 PM
 */

namespace NKO\OrderBundle\Traits\AnalyticReport;

use NKO\OrderBundle\Entity\BaseReport;

trait AttachmentTrait
{
    /**
     * @Assert\Valid()
     * @CustomAssert\Feedback(
     *     inversedBy="expectedAnalyticResults",
     *     groups={"AnalyticReport-2019"}
     * )
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceFeedbackAttachments;

    /**
     * @Assert\Valid()
     * @CustomAssert\Feedback(
     *     inversedBy="practiceAnalyticResults",
     *     groups={"AnalyticReport-2019"}
     * )
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback", mappedBy="reportSpreadAttachment",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceSpreadFeedbackAttachments;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment", mappedBy="reportPracticeSuccessStory",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceSuccessStoryAttachments;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment", mappedBy="reportSpreadPracticeSuccessStory",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceSpreadSuccessStoryAttachments;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $publicationAttachments;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $materialAttachments;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $factorAttachments;

    /**
     * Add practiceFeedbackAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceFeedbackAttachment
     *
     * @return BaseReport
     */
    public function addPracticeFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceFeedbackAttachment)
    {
        $this->practiceFeedbackAttachments[] = $practiceFeedbackAttachment;
        $practiceFeedbackAttachment->setReport($this);

        return $this;
    }

    /**
     * Remove practiceFeedbackAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceFeedbackAttachment
     */
    public function removePracticeFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceFeedbackAttachment)
    {
        $this->practiceFeedbackAttachments->removeElement($practiceFeedbackAttachment);
    }

    /**
     * Get practiceFeedbackAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeFeedbackAttachments()
    {
        return $this->practiceFeedbackAttachments;
    }

    /**
     * Add practiceSpreadFeedbackAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceSpreadFeedbackAttachment
     *
     * @return BaseReport
     */
    public function addPracticeSpreadFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceSpreadFeedbackAttachment)
    {
        $this->practiceSpreadFeedbackAttachments[] = $practiceSpreadFeedbackAttachment;
        $practiceSpreadFeedbackAttachment->setReportSpreadAttachment($this);

        return $this;
    }

    /**
     * Remove practiceSpreadFeedbackAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceSpreadFeedbackAttachment
     */
    public function removePracticeSpreadFeedbackAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback $practiceSpreadFeedbackAttachment)
    {
        $this->practiceSpreadFeedbackAttachments->removeElement($practiceSpreadFeedbackAttachment);
    }

    /**
     * Get practiceSpreadFeedbackAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSpreadFeedbackAttachments()
    {
        return $this->practiceSpreadFeedbackAttachments;
    }


    /**
     * Add practiceSuccessStoryAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSuccessStoryAttachment
     *
     * @return BaseReport
     */
    public function addPracticeSuccessStoryAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSuccessStoryAttachment)
    {
        $this->practiceSuccessStoryAttachments[] = $practiceSuccessStoryAttachment;
        $practiceSuccessStoryAttachment->setReportPracticeSuccessStory($this);

        return $this;
    }

    /**
     * Remove practiceSuccessStoryAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSuccessStoryAttachment
     */
    public function removePracticeSuccessStoryAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSuccessStoryAttachment)
    {
        $this->practiceSuccessStoryAttachments->removeElement($practiceSuccessStoryAttachment);
    }

    /**
     * Get practiceSuccessStoryAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSuccessStoryAttachments()
    {
        return $this->practiceSuccessStoryAttachments;
    }

    /**
     * Add practiceSpreadSuccessStoryAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSpreadSuccessStoryAttachment
     *
     * @return BaseReport
     */
    public function addPracticeSpreadSuccessStoryAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSpreadSuccessStoryAttachment)
    {
        $this->practiceSpreadSuccessStoryAttachments[] = $practiceSpreadSuccessStoryAttachment;
        $practiceSpreadSuccessStoryAttachment->setReportSpreadPracticeSuccessStory($this);

        return $this;
    }

    /**
     * Remove practiceSpreadSuccessStoryAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSpreadSuccessStoryAttachment
     */
    public function removePracticeSpreadSuccessStoryAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment $practiceSpreadSuccessStoryAttachment)
    {
        $this->practiceSpreadSuccessStoryAttachments->removeElement($practiceSpreadSuccessStoryAttachment);
    }

    /**
     * Get practiceSpreadSuccessStoryAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSpreadSuccessStoryAttachments()
    {
        return $this->practiceSpreadSuccessStoryAttachments;
    }

    /**
     * Add publicationAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment $publicationAttachment
     *
     * @return BaseReport
     */
    public function addPublicationAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment $publicationAttachment)
    {
        $this->publicationAttachments[] = $publicationAttachment;
        $publicationAttachment->setReport($this);

        return $this;
    }

    /**
     * Remove publicationAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment $publicationAttachment
     */
    public function removePublicationAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment $publicationAttachment)
    {
        $this->publicationAttachments->removeElement($publicationAttachment);
    }

    /**
     * Get publicationAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPublicationAttachments()
    {
        return $this->publicationAttachments;
    }

    /**
     * Add materialAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment $materialAttachment
     *
     * @return BaseReport
     */
    public function addMaterialAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment $materialAttachment)
    {
        $this->materialAttachments[] = $materialAttachment;
        $materialAttachment->setReport($this);

        return $this;
    }

    /**
     * Remove materialAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment $materialAttachment
     */
    public function removeMaterialAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment $materialAttachment)
    {
        $this->materialAttachments->removeElement($materialAttachment);
    }

    /**
     * Get materialAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterialAttachments()
    {
        return $this->materialAttachments;
    }

    /**
     * Add factorAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment $factorAttachment
     *
     * @return BaseReport
     */
    public function addFactorAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment $factorAttachment)
    {
        $this->factorAttachments[] = $factorAttachment;
        $factorAttachment->setReport($this);

        return $this;
    }

    /**
     * Remove factorAttachment
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment $factorAttachment
     */
    public function removeFactorAttachment(\NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment $factorAttachment)
    {
        $this->factorAttachments->removeElement($factorAttachment);
    }

    /**
     * Get factorAttachments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFactorAttachments()
    {
        return $this->factorAttachments;
    }
}
