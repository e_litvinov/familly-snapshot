<?php

namespace NKO\OrderBundle\Traits\AnalyticReport;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait PriorityDirectionTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection", inversedBy="analyticReports")
     * @ORM\JoinColumn(name="priority_direction_id", referencedColumnName="id")
     */
    protected $priorityDirection;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $priorityDirectionEtc;


    /**
     * Set priorityDirection
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection $priorityDirection
     *
     * @return Report
     */
    public function setPriorityDirection(\NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection $priorityDirection = null)
    {
        $this->priorityDirection = $priorityDirection;
        if ($priorityDirection) {
            $priorityDirection->addAnalyticReport($this);
        }

        return $this;
    }

    /**
     * Get priorityDirection
     *
     * @return \NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection
     */
    public function getPriorityDirection()
    {
        return $this->priorityDirection;
    }

    /**
     * @return string
     */
    public function getPriorityDirectionEtc()
    {
        return $this->priorityDirectionEtc;
    }

    /**
     * @param string $priorityDirectionEtc
     */
    public function setPriorityDirectionEtc($priorityDirectionEtc)
    {
        $this->priorityDirectionEtc = $priorityDirectionEtc;
    }

    /**
     * @Assert\Callback(
     *    groups={"AnalyticReport-2018", "AnalyticReport-2019"}
     * )
     */
    public function isValidPriorityDirections(ExecutionContextInterface $context, $payload)
    {
        if (!$this->priorityDirection && !$this->priorityDirectionEtc) {
            $context->buildViolation('please, choose priority direction')
                ->atPath('priorityDirection')
                ->addViolation();
        }
    }
}
