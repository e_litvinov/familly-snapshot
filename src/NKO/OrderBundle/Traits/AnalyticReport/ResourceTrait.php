<?php

namespace NKO\OrderBundle\Traits\AnalyticReport;


trait ResourceTrait
{
    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\KeyRisk", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $keyRisks;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext", mappedBy="reportPartnerActivity",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $partnerActivities;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $humanResources;

    /**
     * Add partnerActivity
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext $partnerActivity
     *
     * @return Report
     */
    public function addPartnerActivity(\NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext $partnerActivity)
    {
        $this->partnerActivities[] = $partnerActivity;
        $partnerActivity->setReportPartnerActivity($this);

        return $this;
    }

    /**
     * Remove partnerActivity
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext $partnerActivity
     */
    public function removePartnerActivity(\NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext $partnerActivity)
    {
        $this->partnerActivities->removeElement($partnerActivity);
    }

    /**
     * Get partnerActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartnerActivities()
    {
        return $this->partnerActivities;
    }

    /**
     * Add keyRisk
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KeyRisk $keyRisk
     *
     * @return Report
     */
    public function addKeyRisk(\NKO\OrderBundle\Entity\Report\AnalyticReport\KeyRisk $keyRisk)
    {
        $this->keyRisks[] = $keyRisk;
        $keyRisk->setReport($this);

        return $this;
    }

    /**
     * Remove keyRisk
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\KeyRisk $keyRisk
     */
    public function removeKeyRisk(\NKO\OrderBundle\Entity\Report\AnalyticReport\KeyRisk $keyRisk)
    {
        $this->keyRisks->removeElement($keyRisk);
    }

    /**
     * Get keyRisks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeyRisks()
    {
        return $this->keyRisks;
    }

    /**
     * Add humanResource
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource $humanResource
     *
     * @return Report
     */
    public function addHumanResource(\NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource $humanResource)
    {
        $this->humanResources[] = $humanResource;
        $humanResource->setReport($this);

        return $this;
    }

    /**
     * Remove humanResource
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource $humanResource
     */
    public function removeHumanResource(\NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource $humanResource)
    {
        $this->humanResources->removeElement($humanResource);
    }

    /**
     * Get humanResources
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHumanResources()
    {
        return $this->humanResources;
    }

}