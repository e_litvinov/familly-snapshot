<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/4/17
 * Time: 10:23 AM
 */

namespace NKO\OrderBundle\Traits\AnalyticReport;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait ProjectCardTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "MixedReportKNS-2018", "MixedReportKNS-2019", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectName;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "MixedReportKNS-2018", "MixedReportKNS-2019", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column( type="datetime", nullable=true)
     *
     */
    protected $startDateProject;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "MixedReportKNS-2018", "MixedReportKNS-2019", "AnalyticReport-2019", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    protected $finishDateProject;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-AnalyticReport-2018", "HarborAnalyticReport-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true, length=1024)
     */
    protected $projectPurpose;

    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    public function getProjectName()
    {
        return $this->projectName;
    }

    public function setStartDateProject($startDateProject)
    {
        $this->startDateProject = $startDateProject;

        return $this;
    }

    public function getStartDateProject()
    {
        return $this->startDateProject;
    }

    public function setFinishDateProject($finishDateProject)
    {
        $this->finishDateProject = $finishDateProject;

        return $this;
    }

    public function getFinishDateProject()
    {
        return $this->finishDateProject;
    }

    /**
     * @return string
     */
    public function getProjectPurpose()
    {
        return $this->projectPurpose;
    }

    /**
     * @param string $projectPurpose
     */
    public function setProjectPurpose($projectPurpose)
    {
        $this->projectPurpose = $projectPurpose;
    }

    /**
     * @Assert\Callback(
     *     groups={"AnalyticReport-2018", "KNS-AnalyticReport-2018", "MixedReportKNS-2018"}
     *     )
     */
    public function isValidFinishDate(ExecutionContextInterface $context, $payload)
    {
        if ($this->startDateProject >= $this->finishDateProject) {
            $context->buildViolation('greater_start_date')
                ->atPath('finishDateProject')
                ->addViolation();
        }
    }
}
