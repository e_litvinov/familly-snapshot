<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 26.09.17
 * Time: 15:19
 */

namespace NKO\OrderBundle\Traits\AnalyticReport;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

trait AnalyticReportTrait
{


    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="analyticReportLinkPractice")
     */
    protected $practiceMonitoringResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult", mappedBy="analyticReportLinkIntroduction")
     */
    protected $introductionMonitoringResults;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext", mappedBy="socialNotSolvedProblemReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $socialNotSolvedProblems;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext", mappedBy="socialSolvedProblemReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $socialSolvedProblems;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="socialMeasure",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $socialMeasures;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="nextSocialMeasure",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $nextSocialMeasures;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext", mappedBy="realizationFactorReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $realizationFactors;

    /**
     * @Assert\Valid()
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Publication", mappedBy="report",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $analyticPublications;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext", mappedBy="practiceSolvedProblemReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceSolvedProblems;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext", mappedBy="practiceNotSolvedProblemReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceNotSolvedProblems;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\Context", mappedBy="stabilityAnalyticReport",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $resultStability;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext", mappedBy="introductionFactor",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $introductionFactors;






    /**
     * @return mixed
     */
    public function getPracticeMonitoringResults()
    {
        return $this->practiceMonitoringResults;
    }

    public function setPracticeMonitoringResults($practiceMonitoringResults)
    {
        if (!$practiceMonitoringResults) {
            if ($this->practiceMonitoringResults) {
                foreach ($this->practiceMonitoringResults as $result) {
                    $result->setAnalyticReportLinkPractice(null);
                }
            }
        }
        $this->practiceMonitoringResults = $practiceMonitoringResults;
    }

    public function addPracticeMonitoringResult(MonitoringResult $practiceMonitoringResult)
    {
        $practiceMonitoringResult->setAnalyticReportLinkPractice($this);
        $this->practiceMonitoringResults[] = $practiceMonitoringResult;
    }

    /**
     * @return mixed
     */
    public function getIntroductionMonitoringResults()
    {
        return $this->introductionMonitoringResults;
    }

    public function setIntroductionMonitoringResults($introductionMonitoringResults)
    {
        if (!$introductionMonitoringResults) {
            if ($this->introductionMonitoringResults) {
                foreach ($this->introductionMonitoringResults as $result) {
                    $result->setAnalyticReportLinkIntroduction(null);
                }
            }
        }
        $this->introductionMonitoringResults = $introductionMonitoringResults;
    }

    public function addIntroductionMonitoringResult(MonitoringResult $introductionMonitoringResult)
    {
        $introductionMonitoringResult->setAnalyticReportLinkIntroduction($this);
        $this->introductionMonitoringResults[] = $introductionMonitoringResult;
    }

    /**
     * Add socialMeasure
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $socialMeasure
     *
     * @return Report
     */
    public function addSocialMeasure(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $socialMeasure)
    {
        $this->socialMeasures[] = $socialMeasure;
        $socialMeasure->setSocialMeasure($this);

        return $this;
    }

    /**
     * Remove socialMeasure
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $socialMeasure
     */
    public function removeSocialMeasure(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $socialMeasure)
    {
        $this->socialMeasures->removeElement($socialMeasure);
    }

    /**
     * Get socialMeasures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialMeasures()
    {
        return $this->socialMeasures;
    }


    /**
     * Add publication
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $analyticPublication
     *
     * @return Report
     */
    public function addAnalyticPublication(\NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $publication)
    {
        $this->analyticPublications[] = $publication;
        $publication->setReport($this);

        return $this;
    }

    /**
     * Remove publication
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $analyticPublication
     */
    public function removeAnalyticPublication(\NKO\OrderBundle\Entity\Report\AnalyticReport\Publication $publication)
    {
        $this->analyticPublications->removeElement($publication);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnalyticPublications()
    {
        return $this->analyticPublications;
    }

    /**
     * Add introductionFactor
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $introductionFactor
     *
     * @return Report
     */
    public function addIntroductionFactor(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $introductionFactor)
    {
        $this->introductionFactors[] = $introductionFactor;
        $introductionFactor->setIntroductionFactor($this);

        return $this;
    }

    /**
     * Remove introductionFactor
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $introductionFactor
     */
    public function removeIntroductionFactor(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $introductionFactor)
    {
        $this->introductionFactors->removeElement($introductionFactor);
    }

    /**
     * Get introductionFactors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIntroductionFactors()
    {
        return $this->introductionFactors;
    }


    /**
     * Add resultStability
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $resultStability
     *
     * @return Report
     */
    public function addResultStability(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $resultStability)
    {
        $this->resultStability[] = $resultStability;
        $resultStability->setStabilityAnalyticReport($this);

        return $this;
    }

    /**
     * Remove resultStability
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $resultStability
     */
    public function removeResultStability(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $resultStability)
    {
        $this->resultStability->removeElement($resultStability);
    }

    /**
     * Get resultStability
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultStability()
    {
        return $this->resultStability;
    }



    /**
     * Remove practiceMonitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $practiceMonitoringResult
     */
    public function removePracticeMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $practiceMonitoringResult)
    {
        $this->practiceMonitoringResults->removeElement($practiceMonitoringResult);
    }

    /**
     * Remove introductionMonitoringResult
     *
     * @param \NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $introductionMonitoringResult
     */
    public function removeIntroductionMonitoringResult(\NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult $introductionMonitoringResult)
    {
        $this->introductionMonitoringResults->removeElement($introductionMonitoringResult);
    }


    /**
     * Add socialNotSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialNotSolvedProblem
     *
     * @return Report
     */
    public function addSocialNotSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialNotSolvedProblem)
    {
        $this->socialNotSolvedProblems[] = $socialNotSolvedProblem;
        $socialNotSolvedProblem->setSocialNotSolvedProblemReport($this);

        return $this;
    }

    /**
     * Remove socialNotSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialNotSolvedProblem
     */
    public function removeSocialNotSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialNotSolvedProblem)
    {
        $this->socialNotSolvedProblems->removeElement($socialNotSolvedProblem);
    }

    /**
     * Get socialNotSolvedProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialNotSolvedProblems()
    {
        return $this->socialNotSolvedProblems;
    }

    /**
     * Add socialSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialSolvedProblem
     *
     * @return Report
     */
    public function addSocialSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialSolvedProblem)
    {
        $this->socialSolvedProblems[] = $socialSolvedProblem;
        $socialSolvedProblem->setSocialSolvedProblemReport($this);

        return $this;
    }

    /**
     * Remove socialSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialSolvedProblem
     */
    public function removeSocialSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $socialSolvedProblem)
    {
        $this->socialSolvedProblems->removeElement($socialSolvedProblem);
    }

    /**
     * Get socialSolvedProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialSolvedProblems()
    {
        return $this->socialSolvedProblems;
    }


    /**
     * Add nextSocialMeasure
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $nextSocialMeasure
     *
     * @return Report
     */
    public function addNextSocialMeasure(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $nextSocialMeasure)
    {
        $this->nextSocialMeasures[] = $nextSocialMeasure;
        $nextSocialMeasure->setNextSocialMeasure($this);

        return $this;
    }

    /**
     * Remove nextSocialMeasure
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\Context $nextSocialMeasure
     */
    public function removeNextSocialMeasure(\NKO\OrderBundle\Entity\Report\AnalyticReport\Context $nextSocialMeasure)
    {
        $this->nextSocialMeasures->removeElement($nextSocialMeasure);
    }

    /**
     * Get nextSocialMeasures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNextSocialMeasures()
    {
        return $this->nextSocialMeasures;
    }

    /**
     * Add realizationFactor
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $realizationFactor
     *
     * @return Report
     */
    public function addRealizationFactor(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $realizationFactor)
    {
        $this->realizationFactors[] = $realizationFactor;
        $realizationFactor->setRealizationFactorReport($this);

        return $this;
    }

    /**
     * Remove realizationFactor
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $realizationFactor
     */
    public function removeRealizationFactor(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $realizationFactor)
    {
        $this->realizationFactors->removeElement($realizationFactor);
    }

    /**
     * Get realizationFactors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealizationFactors()
    {
        return $this->realizationFactors;
    }

    /**
     * Add practiceSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceSolvedProblem
     *
     * @return Report
     */
    public function addPracticeSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceSolvedProblem)
    {
        $this->practiceSolvedProblems[] = $practiceSolvedProblem;
        $practiceSolvedProblem->setPracticeSolvedProblemReport($this);

        return $this;
    }

    /**
     * Remove practiceSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceSolvedProblem
     */
    public function removePracticeSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceSolvedProblem)
    {
        $this->practiceSolvedProblems->removeElement($practiceSolvedProblem);
    }

    /**
     * Get practiceSolvedProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeSolvedProblems()
    {
        return $this->practiceSolvedProblems;
    }

    /**
     * Add practiceNotSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceNotSolvedProblem
     *
     * @return Report
     */
    public function addPracticeNotSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceNotSolvedProblem)
    {
        $this->practiceNotSolvedProblems[] = $practiceNotSolvedProblem;
        $practiceNotSolvedProblem->setPracticeNotSolvedProblemReport($this);

        return $this;
    }

    /**
     * Remove practiceNotSolvedProblem
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceNotSolvedProblem
     */
    public function removePracticeNotSolvedProblem(\NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext $practiceNotSolvedProblem)
    {
        $this->practiceNotSolvedProblems->removeElement($practiceNotSolvedProblem);
    }

    /**
     * Get practiceNotSolvedProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeNotSolvedProblems()
    {
        return $this->practiceNotSolvedProblems;
    }
}