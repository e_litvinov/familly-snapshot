<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 6/26/18
 * Time: 2:49 PM
 */

namespace NKO\OrderBundle\Traits\Loader;

use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplication2018;

trait FarvaterLoaderTrait
{
    /**
     * @param $collection array|ArrayCollection
     * @param $relatedCollection array|ArrayCollection
     * @return \Doctrine\Common\Collections\Collection|static
     */
    public function getCollectionDiff($collection, $relatedCollection)
    {
        if (is_array($collection)) {
            $collection = new ArrayCollection($collection);
        }

        if (is_array($relatedCollection)) {
            $relatedCollection = new ArrayCollection($relatedCollection);
        }

        $elements = $relatedCollection->filter(function ($group) use ($collection) {
            $elements = $collection->filter(function ($object) use ($group) {
                return ($object->getTitle() == $group);
            });

            return (!$elements->count());
        });

        return $elements;
    }

    public function updateCollection($collection, $relatedArray, $class, $application, $toRemove = true)
    {
        $array = [];

        foreach ($collection as $item) {
            $array[] = $item->getTitle();
        }

        $valuesToAdd = array_diff($relatedArray, $array);

        foreach ($valuesToAdd as $value) {
            $this->entityManager->persist($this->createObject($class, $value, $application));
        }

        if ($toRemove) {
            $valuesToRemove = array_diff($array, $relatedArray);
            foreach ($valuesToRemove as $value) {
                $object = $this->entityManager->getRepository($class)->findOneBy([
                    'projectApplication' => $application,
                    'title' => $value
                ]);
                $this->entityManager->remove($object);
            }
        }
    }

    /**
     * @param $class
     * @param $title string
     * @param $application FarvaterApplication2018
     * @return mixed
     */
    public function createObject($class, $title, $application)
    {
        $item = new $class();
        $item->setTitle($title);
        $item->setProjectApplication($application);
        return $item;
    }
}