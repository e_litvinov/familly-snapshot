<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

trait BankDetailsTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *         "BankDetails"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 10,
     *     minMessage = "INN must be at least 10 characters long",
     *     groups={
     *          "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="INN", type="string", length=10, nullable=true)
     */
    protected $iNN;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 9,
     *     minMessage = "KPP must be at least 9 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="KPP", type="string", length=9, nullable=true)
     */
    protected $kPP;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 8,
     *     minMessage = "OKPO must be at least 8 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="OKPO", type="string", length=10, nullable=true)
     */
    protected $oKPO;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Regex(
     *     pattern="/((\d+(\.?\d*);?)\s*)*\d\;?\.{0}\s*$/",
     *     message="invalid OKVED",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="OKVED", type="string", length=255, nullable=true)
     */
    protected $oKVED;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="bankName", type="string", length=255, nullable=true)
     */
    protected $bankName;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="bankLocation", type="string", length=255, nullable=true)
     */
    protected $bankLocation;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 10,
     *     minMessage = "BankINN must be at least 10 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="bankINN", type="string", length=10, nullable=true)
     */
    protected $bankINN;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 9,
     *     minMessage = "BankKPP must be at least 9 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="bankKPP", type="string", length=9, nullable=true)
     */
    protected $bankKPP;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *  @Assert\Length(
     *     min = 20,
     *     minMessage = "correspondentAccount must be at least 20 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="correspondentAccount", type="string", length=20, nullable=true)
     */
    protected $correspondentAccount;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 9,
     *     minMessage = "BIK must be at least 9 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="BIK", type="string", length=9, nullable=true)
     */
    protected $bIK;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     * @Assert\Length(
     *     min = 20,
     *     minMessage = "paymentAccount must be at least 20 characters long",
     *     groups={
     *              "BankDetails"
     *            }
     *     )
     *
     * @ORM\Column(name="paymentAccount", type="string", length=20, nullable=true)
     */
    protected $paymentAccount;



    /**
     * Set iNN
     *
     * @param string $iNN
     */
    public function setINN($iNN)
    {
        $this->iNN = $iNN;

        return $this;
    }

    /**
     * Get iNN
     *
     * @return string
     */
    public function getINN()
    {
        return $this->iNN;
    }

    /**
     * Set kPP
     *
     * @param string $kPP
     */
    public function setKPP($kPP)
    {
        $this->kPP = $kPP;

        return $this;
    }

    /**
     * Get kPP
     *
     * @return string
     */
    public function getKPP()
    {
        return $this->kPP;
    }

    /**
     * Set oKPO
     *
     * @param string $oKPO
     *
     * @return BaseApplication
     */
    public function setOKPO($oKPO)
    {
        $this->oKPO = $oKPO;

        return $this;
    }

    /**
     * Get oKPO
     *
     * @return string
     */
    public function getOKPO()
    {
        return $this->oKPO;
    }

    /**
     * Set oKVED
     *
     * @param string $oKVED
     */
    public function setOKVED($oKVED)
    {
        $this->oKVED = $oKVED;

        return $this;
    }

    /**
     * Get oKVED
     *
     * @return string
     */
    public function getOKVED()
    {
        return $this->oKVED;
    }

    /**
     * Set bankName
     *
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * Get bankName
     *
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * Set bankLocation
     *
     * @param string $bankLocation
     */
    public function setBankLocation($bankLocation)
    {
        $this->bankLocation = $bankLocation;

        return $this;
    }

    /**
     * Get bankLocation
     *
     * @return string
     */
    public function getBankLocation()
    {
        return $this->bankLocation;
    }

    /**
     * Set bankINN
     *
     * @param string $bankINN
     */
    public function setBankINN($bankINN)
    {
        $this->bankINN = $bankINN;

        return $this;
    }

    /**
     * Get bankINN
     *
     * @return string
     */
    public function getBankINN()
    {
        return $this->bankINN;
    }

    /**
     * Set bankKPP
     *
     * @param string $bankKPP
     */
    public function setBankKPP($bankKPP)
    {
        $this->bankKPP = $bankKPP;

        return $this;
    }

    /**
     * Get bankKPP
     *
     * @return string
     */
    public function getBankKPP()
    {
        return $this->bankKPP;
    }

    /**
     * Set correspondentAccount
     *
     * @param string $correspondentAccount
     */
    public function setCorrespondentAccount($correspondentAccount)
    {
        $this->correspondentAccount = $correspondentAccount;

        return $this;
    }

    /**
     * Get correspondentAccount
     *
     * @return string
     */
    public function getCorrespondentAccount()
    {
        return $this->correspondentAccount;
    }

    /**
     * Set bIK
     *
     * @param string $bIK
     */
    public function setBIK($bIK)
    {
        $this->bIK = $bIK;

        return $this;
    }

    /**
     * Get bIK
     *
     * @return string
     */
    public function getBIK()
    {
        return $this->bIK;
    }

    /**
     * Set paymentAccount
     *
     * @param string $paymentAccount
     *
     */
    public function setPaymentAccount($paymentAccount)
    {
        $this->paymentAccount = $paymentAccount;

        return $this;
    }

    /**
     * Get paymentAccount
     *
     * @return string
     */
    public function getPaymentAccount()
    {
        return $this->paymentAccount;
    }
}
