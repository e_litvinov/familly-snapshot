<?php

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use NKO\OrderBundle\Entity\Farvater2017\OrganizationResource;

trait BudgetTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Budget"}
     *     )
     *
     * @ORM\Column(name="entire_period_estimated_financing", type="float", nullable=true)
     */
    private $entirePeriodEstimatedFinancing;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Budget"}
     *     )
     *
     * @ORM\Column(name="entire_period_estimated_co_financing", type="float", nullable=true)
     */
    private $entirePeriodEstimatedCoFinancing;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Budget", "Harbor-2019", "Harbor-2020"}
     *     )
     *
     * @ORM\Column(name="first_year_estimated_financing", type="float", nullable=true)
     */
    private $firstYearEstimatedFinancing;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"Budget"}
     *     )
     *
     * @ORM\Column(name="first_year_estimated_co_financing", type="float", nullable=true)
     */
    private $firstYearEstimatedCoFinancing;

    /**
     * Set entirePeriodEstimatedFinancing
     *
     * @param integer $entirePeriodEstimatedFinancing
     *
     * @return BaseApplication
     */
    public function setEntirePeriodEstimatedFinancing($entirePeriodEstimatedFinancing)
    {
        $this->entirePeriodEstimatedFinancing = $entirePeriodEstimatedFinancing;

        return $this;
    }

    /**
     * Get entirePeriodEstimatedFinancing
     *
     * @return integer
     */
    public function getEntirePeriodEstimatedFinancing()
    {
        return $this->entirePeriodEstimatedFinancing;
    }

    /**
     * Set entirePeriodEstimatedCoFinancing
     *
     * @param integer $entirePeriodEstimatedCoFinancing
     *
     * @return BaseApplication
     */
    public function setEntirePeriodEstimatedCoFinancing($entirePeriodEstimatedCoFinancing)
    {
        $this->entirePeriodEstimatedCoFinancing = $entirePeriodEstimatedCoFinancing;

        return $this;
    }

    /**
     * Get entirePeriodEstimatedCoFinancing
     *
     * @return integer
     */
    public function getEntirePeriodEstimatedCoFinancing()
    {
        return $this->entirePeriodEstimatedCoFinancing;
    }

    /**
     * Set firstYearEstimatedFinancing
     *
     * @param integer $firstYearEstimatedFinancing
     *
     * @return BaseApplication
     */
    public function setFirstYearEstimatedFinancing($firstYearEstimatedFinancing)
    {
        $this->firstYearEstimatedFinancing = $firstYearEstimatedFinancing;

        return $this;
    }

    /**
     * Get firstYearEstimatedFinancing
     *
     * @return integer
     */
    public function getFirstYearEstimatedFinancing()
    {
        return $this->firstYearEstimatedFinancing;
    }

    /**
     * Set firstYearEstimatedCoFinancing
     *
     * @param integer $firstYearEstimatedCoFinancing
     *
     * @return BaseApplication
     */
    public function setFirstYearEstimatedCoFinancing($firstYearEstimatedCoFinancing)
    {
        $this->firstYearEstimatedCoFinancing = $firstYearEstimatedCoFinancing;

        return $this;
    }

    /**
     * Get firstYearEstimatedCoFinancing
     *
     * @return integer
     */
    public function getFirstYearEstimatedCoFinancing()
    {
        return $this->firstYearEstimatedCoFinancing;
    }
}
