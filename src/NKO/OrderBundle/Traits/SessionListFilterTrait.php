<?php

namespace NKO\OrderBundle\Traits;

trait SessionListFilterTrait
{
    private $alreadyReseted = false;
    
    protected function resetSessionFilters()
    {
        $isNeedReset = $this->request->get('without_competition') == 1;
        if ($isNeedReset && !$this->alreadyReseted) {
            $this->request->getSession()->set($this->getCode().'.filter.parameters', []);
            $this->alreadyReseted = true;
        }

        return $isNeedReset;
    }
}
