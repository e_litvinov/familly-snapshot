<?php

namespace NKO\OrderBundle\Traits\FinanceReport;

trait FinanceReportTraitNotifications
{
    /**
     * Function for checking donation balance and receiving notification about residue
     *
     * @param $object
     *
     * @return string[]|null
     */
    public function checkDonationBalance($object)
    {
        $lastPeriod = $object->getReportForm()->getReportPeriods()->last();
        $reportPeriod = $object->getPeriod();

        if ($object != null && $object->getDonationBalance() > 0 && $lastPeriod == $reportPeriod) {
            return [
                'type' => 'sonata_flash_info',
                'message' => 'Необходимо обратиться в Фонд в связи с наличием остатка средств',
            ];
        }

        return null;
    }

    /**
     * Function for checking cost overruns by main expense and receiving a notification
     *
     * @param $object
     *
     * @return string[]|null
     */
    public function checkCostOverruns($object)
    {
        if ($object != null) {
            $financeSpent = $object->getFinanceSpent();

            for ($i = 0; $i < $financeSpentCount = count($financeSpent); $i++) {
                if ($i == 0 || $i >= 4 && $i <= 9) {
                    if ($financeSpent[$i]->getApprovedSum() != 0 && $financeSpent[$i]->getIncrementalCosts() != 0 &&
                        $financeSpent[$i]->getApprovedSum() * 1.1 <= $financeSpent[$i]->getIncrementalCosts()) {
                        return [
                            'type' => 'sonata_flash_info',
                            'message' => 'В связи с наличием ситуации превышения суммы расходов 
                                по статье по всему отчетному периоду к плановым более, чем на 10%, 
                                необходимо проверить корректность заполнения отчета или обратиться в 
                                Фонд для получения информации о перераспределении статей',
                        ];
                    }
                }
            }
        }

        return null;
    }
}
