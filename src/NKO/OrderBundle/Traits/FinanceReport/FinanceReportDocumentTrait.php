<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 5/22/18
 * Time: 12:28 PM
 */

namespace NKO\OrderBundle\Traits\FinanceReport;

use NKO\OrderBundle\Traits\FieldGetter;
use NKO\OrderBundle\Validator\Constraints as DocumentAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

trait FinanceReportDocumentTrait
{
    use FieldGetter;
    /**
     * @var \DateTime
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2018", "MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", nullable=true)
     */
    protected $file;

    private $_file;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"FinanceReport-2018", "MixedReportKNS-2018", "FinanceReport-2019", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ExpenseType", inversedBy="documents")
     * @ORM\JoinColumn(name="expense_type_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $expenseType;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\BaseReport")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $report;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @Assert\Callback
     */
    public function isValidRegulation(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getFile() && !$this->_file) {
            $context->buildViolation('please, upload file')
                ->atPath('file')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback()
     */
    public function isValidDate(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getReport()->getPeriod()) {
            return null;
        }

        if ($this->date < $this->getReport()->getPeriod()->getStartDate() || $this->date > $this->getReport()->getPeriod()->getFinishDate()) {
            $context->buildViolation('Выберите дату в диапазоне периода заполняемого отчета')
                ->atPath('date')
                ->addViolation();
        }
    }

    /**
     * @param $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param $file
     * @return $this
     */
    public function setFile($file)
    {
        if (!$this->_file && is_string($this->file)) {
            $this->_file = $this->file;
        }

        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \NKO\OrderBundle\Entity\ExpenseType|null $expenseType
     * @return $this
     */
    public function setExpenseType(\NKO\OrderBundle\Entity\ExpenseType $expenseType = null)
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpenseType()
    {
        return $this->expenseType;
    }

    /**
     * @param \NKO\OrderBundle\Entity\BaseReport|null $report
     * @return $this
     */
    public function setReport(\NKO\OrderBundle\Entity\BaseReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @param $deletedAt
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function getFileHelp()
    {
        return $this->getField('_file');
    }
}
