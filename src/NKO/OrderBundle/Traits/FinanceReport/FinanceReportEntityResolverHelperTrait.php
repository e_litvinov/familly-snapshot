<?php

namespace NKO\OrderBundle\Traits\FinanceReport;

use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\DependencyInjection\ContainerInterface;

/** @property $container ContainerInterface */
trait FinanceReportEntityResolverHelperTrait
{
    public function setIsSendAfterComplete($object, $isPreviosSend)
    {
        $object->setIsSend($isPreviosSend && $object->getReportTemplate()->getIsPlanAccepted());
    }

    public function preCompleteEdit($object = null, $form = null, $params = null)
    {
        $admin = $this->container->get('sonata.admin.pool')->getAdminByClass(get_class($object));
        $em = $this->container->get('doctrine')->getManager();
        $cache = $this->container->get('nko_order.resolver.cache_resolver');

        $reports = $em->getRepository(BaseReport::class)->findReportsByReportForm($object->getReportForm());
        foreach ($reports as $report) {
            $cache->generateItem($admin, $report->getId(), $report->getAuthor()->getId());
            $cache->delete();
        }
    }
}
