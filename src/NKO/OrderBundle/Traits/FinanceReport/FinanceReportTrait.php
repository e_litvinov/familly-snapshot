<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 07.06.17
 * Time: 17:38
 */

namespace NKO\OrderBundle\Traits\FinanceReport;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

trait FinanceReportTrait
{
    /**
     * @var float
     *
     * @ORM\Column(name="receipt_of_donations", type="float", nullable=true)
     */
    protected $receiptOfDonations;

    /**
     * @var float
     *
     * @ORM\Column(name="period_costs", type="float", nullable=true)
     */
    protected $periodCosts;

    /**
     * @var float
     *
     * @ORM\Column(name="donation_balance", type="float", nullable=true)
     */
    protected $donationBalance;

    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Register", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $registers;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\FinanceSpent", mappedBy="report", cascade={"all"}, orphanRemoval=true)
     */
    protected $financeSpent;

    public function setReceiptOfDonations($receiptOfDonations)
    {
        $this->receiptOfDonations = $receiptOfDonations;

        return $this;
    }

    public function getReceiptOfDonations()
    {
        return $this->receiptOfDonations;
    }

    public function setPeriodCosts($periodCosts)
    {
        $this->periodCosts = $periodCosts;

        return $this;
    }

    public function getPeriodCosts()
    {
        return $this->periodCosts;
    }

    public function setDonationBalance($donationBalance)
    {
        $this->donationBalance = $donationBalance;

        return $this;
    }

    public function getDonationBalance()
    {
        return $this->donationBalance;
    }

    public function addRegister(\NKO\OrderBundle\Entity\Register $register)
    {
        $this->registers[] = $register;

        return $this;
    }

    public function removeRegister(\NKO\OrderBundle\Entity\Register $register)
    {
        $this->registers->removeElement($register);
    }

    public function getRegisters()
    {
        return $this->registers;
    }

    public function addFinanceSpent(\NKO\OrderBundle\Entity\FinanceSpent $financeSpent)
    {
        $this->financeSpent[] = $financeSpent;
        $financeSpent->setReport($this);

        return $this;
    }

    public function removeFinanceSpent(\NKO\OrderBundle\Entity\FinanceSpent $financeSpent)
    {
        $this->financeSpent->removeElement($financeSpent);
    }

    public function getFinanceSpent()
    {
        return $this->financeSpent;
    }
}