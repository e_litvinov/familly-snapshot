<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/16/18
 * Time: 11:18 AM
 */

namespace NKO\OrderBundle\Traits;

trait HumanizedProperties
{
    public function getHumanizedCollections($collection)
    {
        $humanizedResult = '';
        foreach ($collection as $item) {
            $humanizedResult .= (string) $item . '; ';
        }

        return $humanizedResult;
    }
}