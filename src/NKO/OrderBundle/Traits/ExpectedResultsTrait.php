<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/15/18
 * Time: 12:13 PM
 */

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\BaseApplication;

trait ExpectedResultsTrait
{
    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="experienceApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $experienceItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="experienceEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $experienceEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="processApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $processItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="processEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $processEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="qualificationApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $qualificationItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="qualificationEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $qualificationEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="resourceApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $resourceBlockItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult", mappedBy="resourceEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $resourceBlockEtcItems;

    /**
     * Add experienceItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceItem
     *
     * @return BaseApplication
     */
    public function addExperienceItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceItem)
    {
        $experienceItem->setExperienceApplication($this);
        $this->experienceItems[] = $experienceItem;

        return $this;
    }

    /**
     * Remove experienceItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceItem
     */
    public function removeExperienceItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceItem)
    {
        $this->experienceItems->removeElement($experienceItem);
    }

    /**
     * Get experienceItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperienceItems()
    {
        return $this->experienceItems;
    }

    /**
     * Add experienceEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceEtcItem
     *
     * @return BaseApplication
     */
    public function addExperienceEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceEtcItem)
    {
        $experienceEtcItem->setExperienceEtcApplication($this);
        $this->experienceEtcItems[] = $experienceEtcItem;

        return $this;
    }

    /**
     * Remove experienceEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceEtcItem
     */
    public function removeExperienceEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $experienceEtcItem)
    {
        $this->experienceEtcItems->removeElement($experienceEtcItem);
    }

    /**
     * Get experienceEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExperienceEtcItems()
    {
        return $this->experienceEtcItems;
    }

    /**
     * Add processItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processItem
     *
     * @return BaseApplication
     */
    public function addProcessItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processItem)
    {
        $processItem->setProcessApplication($this);
        $this->processItems[] = $processItem;

        return $this;
    }

    /**
     * Remove processItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processItem
     */
    public function removeProcessItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processItem)
    {
        $this->processItems->removeElement($processItem);
    }

    /**
     * Get processItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProcessItems()
    {
        return $this->processItems;
    }

    /**
     * Add processEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processEtcItem
     *
     * @return BaseApplication
     */
    public function addProcessEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processEtcItem)
    {
        $processEtcItem->setProcessEtcApplication($this);
        $this->processEtcItems[] = $processEtcItem;

        return $this;
    }

    /**
     * Remove processEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processEtcItem
     */
    public function removeProcessEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $processEtcItem)
    {
        $this->processEtcItems->removeElement($processEtcItem);
    }

    /**
     * Get processEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProcessEtcItems()
    {
        return $this->processEtcItems;
    }

    /**
     * Add qualificationItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationItem
     *
     * @return Application
     */
    public function addQualificationItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationItem)
    {
        $qualificationItem->setQualificationApplication($this);
        $this->qualificationItems[] = $qualificationItem;

        return $this;
    }

    /**
     * Remove qualificationItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationItem
     */
    public function removeQualificationItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationItem)
    {
        $this->qualificationItems->removeElement($qualificationItem);
    }

    /**
     * Get qualificationItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQualificationItems()
    {
        return $this->qualificationItems;
    }

    /**
     * Add qualificationEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationEtcItem
     *
     * @return BaseApplication
     */
    public function addQualificationEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationEtcItem)
    {
        $qualificationEtcItem->setQualificationEtcApplication($this);
        $this->qualificationEtcItems[] = $qualificationEtcItem;
        return $this;
    }

    /**
     * Remove qualificationEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationEtcItem
     */
    public function removeQualificationEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $qualificationEtcItem)
    {
        $this->qualificationEtcItems->removeElement($qualificationEtcItem);
    }

    /**
     * Get qualificationEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQualificationEtcItems()
    {
        return $this->qualificationEtcItems;
    }

    /**
     * Add resourceBlockItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockItem
     *
     * @return BaseApplication
     */
    public function addResourceBlockItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockItem)
    {
        $resourceBlockItem->setResourceApplication($this);
        $this->resourceBlockItems[] = $resourceBlockItem;

        return $this;
    }

    /**
     * Remove resourceBlockItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockItem
     */
    public function removeResourceBlockItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockItem)
    {
        $this->resourceBlockItems->removeElement($resourceBlockItem);
    }

    /**
     * Get resourceBlockItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResourceBlockItems()
    {
        return $this->resourceBlockItems;
    }

    /**
     * Add resourceBlockEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockEtcItem
     *
     * @return BaseApplication
     */
    public function addResourceBlockEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockEtcItem)
    {
        $resourceBlockEtcItem->setResourceEtcApplication($this);
        $this->resourceBlockEtcItems[] = $resourceBlockEtcItem;

        return $this;
    }

    /**
     * Remove resourceBlockEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockEtcItem
     */
    public function removeResourceBlockEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult $resourceBlockEtcItem)
    {
        $this->resourceBlockEtcItems->removeElement($resourceBlockEtcItem);
    }

    /**
     * Get resourceBlockEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResourceBlockEtcItems()
    {
        return $this->resourceBlockEtcItems;
    }
}