<?php

namespace NKO\OrderBundle\Traits;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait ProjectResultsTrait
{
    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="knowledge_usage", type="text", nullable=true)
     */
    protected $knowledgeUsage;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="plan_of_knowledge_usage", type="text", nullable=true)
     */
    protected $planOfKnowledgeUsage;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="past_events", type="text", nullable=true)
     */
    protected $pastEvents;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="difficulties", type="text", nullable=true)
     */
    protected $difficulties;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="successStory", type="text", nullable=true)
     */
    protected $successStory;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"MixedReportKNS-2018", "MixedReportKNS-2019"}
     * )
     *
     * @ORM\Column(name="feedback", type="text", nullable=true)
     */
    protected $feedback;

    /**
     * Set knowledgeUsage
     *
     * @param string $knowledgeUsage
     *
     */
    public function setKnowledgeUsage($knowledgeUsage)
    {
        $this->knowledgeUsage = $knowledgeUsage;

        return $this;
    }

    /**
     * Get knowledgeUsage
     *
     * @return string
     */
    public function getKnowledgeUsage()
    {
        return $this->knowledgeUsage;
    }

    /**
     * Set planOfKnowledgeUsage
     *
     * @param string $planOfKnowledgeUsage
     *
     */
    public function setPlanOfKnowledgeUsage($planOfKnowledgeUsage)
    {
        $this->planOfKnowledgeUsage = $planOfKnowledgeUsage;

        return $this;
    }

    /**
     * Get planOfKnowledgeUsage
     *
     * @return string
     */
    public function getPlanOfKnowledgeUsage()
    {
        return $this->planOfKnowledgeUsage;
    }

    /**
     * Set pastEvents
     *
     * @param string $pastEvents
     *
     */
    public function setPastEvents($pastEvents)
    {
        $this->pastEvents = $pastEvents;

        return $this;
    }

    /**
     * Get pastEvents
     *
     * @return string
     */
    public function getPastEvents()
    {
        return $this->pastEvents;
    }


    /**
     * Set successStory
     *
     * @param string $successStory
     *
     */
    public function setSuccessStory($successStory)
    {
        $this->successStory = $successStory;

        return $this;
    }

    /**
     * Get successStory
     *
     * @return string
     */
    public function getSuccessStory()
    {
        return $this->successStory;
    }

    /**
     * Set feedback
     *
     * @param string $feedback
     *
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;

        return $this;
    }

    /**
     * Get feedback
     *
     * @return string
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * Set difficulties
     *
     * @param string $difficulties
     *
     */
    public function setDifficulties($difficulties)
    {
        $this->difficulties = $difficulties;

        return $this;
    }

    /**
     * Get difficulties
     *
     * @return string
     */
    public function getDifficulties()
    {
        return $this->difficulties;
    }
}
