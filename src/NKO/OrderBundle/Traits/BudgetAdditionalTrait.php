<?php

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Constraints as Assert;

trait BudgetAdditionalTrait
{

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *             "AdditionBudget",
     *             "KNS2017-2"
     *         }
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *             "AdditionBudget",
     *             "KNS2017-2"
     *         }
     *     )
     * @Assert\Length(
     *     min = 8,
     *     minMessage = "OKTMO must be at least 8 characters long",
     *     groups={
     *             "AdditionBudget",
     *             "KNS2017-2"
     *         }
     *     )
     *
     * @ORM\Column(name="OKTMO", type="string", length=11, nullable=true)
     */
    private $oKTMO;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *             "AdditionBudget",
     *         }
     *     )
     * @Assert\Length(
     *     min = 20,
     *     minMessage = "KBK must be at least 20 characters long",
     *     groups={
     *             "AdditionBudget",
     *         }
     *     )
     *
     * @ORM\Column(name="KBK", type="string", length=255, nullable=true)
     */
    private $kBK;

    /**
     * Set oKTMO
     *
     * @param string $oKTMO
     *
     * @return BaseApplication
     */
    public function setOKTMO($oKTMO)
    {
        $this->oKTMO = $oKTMO;

        return $this;
    }

    /**
     * Get oKTMO
     *
     * @return string
     */
    public function getOKTMO()
    {
        return $this->oKTMO;
    }

    /**
     * Set kBK
     *
     * @param string $kBK
     *
     * @return BaseApplication
     */
    public function setKBK($kBK)
    {
        $this->kBK = $kBK;

        return $this;
    }

    /**
     * Get kBK
     *
     * @return string
     */
    public function getKBK()
    {
        return $this->kBK;
    }
}
