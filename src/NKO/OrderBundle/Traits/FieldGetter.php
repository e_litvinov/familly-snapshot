<?php

namespace NKO\OrderBundle\Traits;

use ReflectionProperty;

trait FieldGetter
{
    public function getField($name)
    {
        $reflection = new ReflectionProperty($this, $name);
        $reflection->setAccessible($name);
        return $reflection->getValue($this);
    }
}
