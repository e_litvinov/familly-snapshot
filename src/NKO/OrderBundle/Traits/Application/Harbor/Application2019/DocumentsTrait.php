<?php

namespace NKO\OrderBundle\Traits\Application\Harbor\Application2019;

use NKO\OrderBundle\Entity\BaseApplication;

trait DocumentsTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="is_bank_account_exist", type="string", nullable=true)
     */
    private $isBankAccountExist;

    private $_isBankAccountExist;

    /**
     * @var string
     *
     * @ORM\Column(name="report_for_ministry_of_justice", type="string", nullable=true)
     */
    private $reportForMinistryOfJustice;

    private $_reportForMinistryOfJustice;

    /**
     * Set isBankAccountExist
     *
     * @param string $isBankAccountExist
     *
     * @return BaseApplication
     */
    public function setIsBankAccountExist($isBankAccountExist)
    {
        if (!$this->_isBankAccountExist && is_string($this->isBankAccountExist)) {
            $this->_isBankAccountExist = $this->isBankAccountExist;
        }
        $this->isBankAccountExist = $isBankAccountExist;

        return $this;
    }

    /**
     * Get isBankAccountExist
     *
     * @return string
     */
    public function getIsBankAccountExist()
    {
        return $this->isBankAccountExist;
    }

    /**
     * Set reportForMinistryOfJustice
     *
     * @param string $reportForMinistryOfJustice
     *
     * @return BaseApplication
     */
    public function setReportForMinistryOfJustice($reportForMinistryOfJustice)
    {
        if (!$this->_reportForMinistryOfJustice && is_string($this->reportForMinistryOfJustice)) {
            $this->_reportForMinistryOfJustice = $this->reportForMinistryOfJustice;
        }
        $this->reportForMinistryOfJustice = $reportForMinistryOfJustice;

        return $this;
    }

    /**
     * Get reportForMinistryOfJustice
     *
     * @return string
     */
    public function getReportForMinistryOfJustice()
    {
        return $this->reportForMinistryOfJustice;
    }
}
