<?php

namespace NKO\OrderBundle\Traits\Application;

use NKO\OrderBundle\Traits\Application\KNS\Application2018\DocumentsTrait as OldDocumentsTrait;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;

trait AllDocumentsTrait
{
    use OldDocumentsTrait;
    use DocumentsTrait;
}
