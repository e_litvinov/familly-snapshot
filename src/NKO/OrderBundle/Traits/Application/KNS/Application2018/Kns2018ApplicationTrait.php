<?php

namespace NKO\OrderBundle\Traits\Application\KNS\Application2018;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Entity\BaseApplication as Application;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;

trait Kns2018ApplicationTrait
{
    /**
     * @var bool
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    protected $isOrganizationHeadEqualProjectHead;

    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS-2018", "KNS-2019", "KNS-2020"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm")
     * @ORM\JoinColumn(name="organizationForm_id", referencedColumnName="id", nullable=true)
     */
    private $organizationForm;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *            }
     *     )
     * @Assert\Length(
     *     min = 20,
     *     minMessage = "paymentAccount must be at least 20 characters long",
     *     groups={
     *
     *            }
     *     )
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $personalAccount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameAddressee;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"KNS-2018", "KNS-2019", "KNS-2020"}
     * )
     *
     * @ORM\Column(type="text", length=1000, nullable=true)
     */
    private $importanceProject;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup", mappedBy="application",
     *     orphanRemoval=true, cascade={"all"})
     */
    private $otherBeneficiaryGroups;

    /**
     * @Assert\Callback(groups={"KNS-2018", "KNS-2019", "KNS-2020"})
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPeopleCategories();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose people categories')
                ->atPath('peopleCategories')
                ->addViolation();
        }
    }

    /**
     * Set isOrganizationHeadEqualProjectHead
     *
     * @param boolean $isOrganizationHeadEqualProjectHead
     *
     * @return Application
     */
    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    /**
     * Get isOrganizationHeadEqualProjectHead
     *
     * @return boolean
     */
    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    /**
     * Set personalAccount
     *
     * @param string $personalAccount
     *
     * @return Application
     */
    public function setPersonalAccount($personalAccount)
    {
        $this->personalAccount = $personalAccount;

        return $this;
    }

    /**
     * Get personalAccount
     *
     * @return string
     */
    public function getPersonalAccount()
    {
        return $this->personalAccount;
    }

    /**
     * Set nameAddressee
     *
     * @param string $nameAddressee
     *
     * @return Application
     */
    public function setNameAddressee($nameAddressee)
    {
        $this->nameAddressee = $nameAddressee;

        return $this;
    }

    /**
     * Get nameAddressee
     *
     * @return string
     */
    public function getNameAddressee()
    {
        return $this->nameAddressee;
    }

    /**
     * Set importanceProject
     *
     * @param string $importanceProject
     *
     * @return Application
     */
    public function setImportanceProject($importanceProject)
    {
        $this->importanceProject = $importanceProject;

        return $this;
    }

    /**
     * Get importanceProject
     *
     * @return string
     */
    public function getImportanceProject()
    {
        return $this->importanceProject;
    }

    /**
     * Set organizationForm
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm
     *
     * @return Application
     */
    public function setOrganizationForm(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm = null)
    {
        $this->organizationForm = $organizationForm;

        return $this;
    }

    /**
     * Get organizationForm
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm
     */
    public function getOrganizationForm()
    {
        return $this->organizationForm;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return Application
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;

        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Add otherBeneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup
     *
     * @return Application
     */
    public function addOtherBeneficiaryGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups[] = $otherBeneficiaryGroup;
        $otherBeneficiaryGroup->setApplication($this);
        return $this;
    }

    /**
     * Remove otherBeneficiaryGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup
     */
    public function removeOtherBeneficiaryGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherBeneficiaryGroup)
    {
        $this->otherBeneficiaryGroups->removeElement($otherBeneficiaryGroup);
    }

    /**
     * Get otherBeneficiaryGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherBeneficiaryGroups()
    {
        return $this->otherBeneficiaryGroups;
    }
}
