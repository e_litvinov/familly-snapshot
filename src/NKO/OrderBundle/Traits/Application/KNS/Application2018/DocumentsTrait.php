<?php

namespace NKO\OrderBundle\Traits\Application\KNS\Application2018;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Validator\Constraints as ApplicationAssert;

trait DocumentsTrait
{
    /**
     * @var string
     *
     * @ApplicationAssert\PdfExtension(
     *     groups={"KNS-2018"}
     *     )
     *
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"KNS-2018"}
     *     )
     * @ORM\Column(name="signed_agreement", type="string", length=255, nullable=true)
     */
    protected $signedAgreement;

    protected $_signedAgreement;

    /**
     *
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     * @ORM\Column(name="authority_head", type="string", length=255, nullable=true)
     */
    protected $authorityHead;

    protected $_authorityHead;

    /**
     * @var string
     *
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"KNS-2018", "KNS-2019", "KNS-2020"}
     *     )
     * @ORM\Column(name="another_head", type="string", length=255, nullable=true)
     */
    protected $anotherHead;

    protected $_anotherHead;

    /**
     * @var string
     *
     * @Assert\File(
     *     maxSize = "50000k",
     *     mimeTypesMessage = "Please upload a valid PDF file",
     *     maxSizeMessage = "file must not exceed 50MB",
     *     notFoundMessage = "file not found",
     *     groups={"KNS-2018"}
     *     )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $subjectRegulation;

    protected $_subjectRegulation;

    /**
     * @Assert\Callback(
     *     groups={"KNS-2018", "KNS-2019"}
     *     )
     */
    public function isValidAuthorityHead(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getAuthorityHead() && !$this->_authorityHead) {
            $context->buildViolation('file not found')
                ->atPath('authorityHead')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={"KNS-2018"}
     *     )
     */
    public function isValidSignedAgreement(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getSignedAgreement() && !$this->_signedAgreement) {
            $context->buildViolation('file not found')
                ->atPath('signedAgreement')
                ->addViolation();
        }
    }

    /**
     * @Assert\Callback(
     *     groups={}
     *     )
     * @Assert\Callback(
     *     groups={}
     *     )
     */
    public function isValidSubjectRegulation(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getSubjectRegulation() && !$this->_subjectRegulation) {
            $context->buildViolation('file not found')
                ->atPath('subjectRegulation')
                ->addViolation();
        }
    }

    /**
     * Set signedAgreement
     *
     * @param string $signedAgreement
     *
     * @return BaseApplication
     */
    public function setSignedAgreement($signedAgreement)
    {
        if (!$this->_signedAgreement && is_string($this->signedAgreement)) {
            $this->_signedAgreement = $this->signedAgreement;
        }
        $this->signedAgreement = $signedAgreement;

        return $this;
    }

    /**
     * Get signedAgreement
     *
     * @return string
     */
    public function getSignedAgreement()
    {
        return $this->signedAgreement;
    }

    /**
     * Set authorityHead
     *
     * @param string $authorityHead
     *
     * @return BaseApplication
     */
    public function setAuthorityHead($authorityHead)
    {
        if (!$this->_authorityHead && is_string($this->authorityHead)) {
            $this->_authorityHead = $this->authorityHead;
        }
        $this->authorityHead = $authorityHead;

        return $this;
    }

    /**
     * Get authorityHead
     *
     * @return string
     */
    public function getAuthorityHead()
    {
        return $this->authorityHead;
    }

    /**
     * Get anotherHead
     *
     * @return string
     */
    public function getAnotherHead()
    {
        return $this->anotherHead;
    }

    /**
     * Set anotherHead
     *
     * @param string $anotherHead
     *
     * @return BaseApplication
     */
    public function setAnotherHead($anotherHead)
    {
        if (!$this->_anotherHead && is_string($this->anotherHead)) {
            $this->_anotherHead = $this->anotherHead;
        }
        $this->anotherHead = $anotherHead;

        return $this;
    }

    /**
     * Set subjectRegulation
     *
     * @param string $subjectRegulation
     *
     * @return BaseApplication
     */
    public function setSubjectRegulation($subjectRegulation)
    {
        if (!$this->_subjectRegulation && is_string($this->subjectRegulation)) {
            $this->_subjectRegulation = $this->subjectRegulation;
        }
        $this->subjectRegulation = $subjectRegulation;

        return $this;
    }

    /**
     * Get subjectRegulation
     *
     * @return string
     */
    public function getSubjectRegulation()
    {
        return $this->subjectRegulation;
    }
}
