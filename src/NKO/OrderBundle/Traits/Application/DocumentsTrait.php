<?php

namespace NKO\OrderBundle\Traits\Application;

use NKO\OrderBundle\Entity\BaseApplication;

trait DocumentsTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="organization_creation_resolution", type="string", nullable=true)
     */
    private $organizationCreationResolution;

    private $_organizationCreationResolution;

    /**
     * @var string
     *
     * @ORM\Column(name="budget", type="string", nullable=true)
     */
    private $budget;

    private $_budget;

    /**
     * Set organizationCreationResolution
     *
     * @param string $organizationCreationResolution
     *
     * @return BaseApplication
     */
    public function setOrganizationCreationResolution($organizationCreationResolution)
    {
        if (!$this->_organizationCreationResolution && is_string($this->organizationCreationResolution)) {
            $this->_organizationCreationResolution = $this->organizationCreationResolution;
        }
        $this->organizationCreationResolution = $organizationCreationResolution;

        return $this;
    }

    /**
     * Get organizationCreationResolution
     *
     * @return string
     */
    public function getOrganizationCreationResolution()
    {
        return $this->organizationCreationResolution;
    }

    /**
     * Set budget
     *
     * @param string $budget
     *
     * @return BaseApplication
     */
    public function setBudget($budget)
    {
        if (!$this->_budget && is_string($this->budget)) {
            $this->_budget = $this->budget;
        }
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }
}
