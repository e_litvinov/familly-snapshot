<?php

namespace NKO\OrderBundle\Traits\Application\Continuation;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Entity\ApplicationHistory;

trait ApplicationTrait
{
    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", inversedBy="followingApplications", cascade={"detach"})
     * @ORM\JoinColumn(name="linked_application_history_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $linkedApplicationHistory;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sumGrant;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ContextApplication", mappedBy="sustainabilityPracticeApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $sustainabilityPracticeApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ContextApplication", mappedBy="sustainabilitySpreadApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $sustainabilitySpreadApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ContextApplication", mappedBy="sustainabilityMonitoringApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $sustainabilityMonitoringApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication", mappedBy="realizationPracticeApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $realizationPracticeApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication", mappedBy="spreadPracticeApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $spreadPracticeApplications;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication", mappedBy="risePotentialApplication",
     *     cascade={"all"}, orphanRemoval=true)
     */
    protected $risePotentialApplications;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     *
     * @ORM\Column(name="practice_changes", type="text", nullable=true)
     */
    protected $practiceChanges;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $beneficiaryProblems;


    /**
     * @var string
     * @Assert\NotBlank(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $projectPurpose;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="expectedResultApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $expectedResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult", mappedBy="practiceResultApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $practiceResults;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\Resource", mappedBy="resourceApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $resourceItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\Resource", mappedBy="resourceEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $resourceEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessImplementationEtcItems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessDisseminationEtcApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessDisseminationEtcItems;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018"}
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $potential;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", }
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $monitoringProjectPurpose;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\Team", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $projectTeams;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $projectContPartners;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"}
     *     )
     * @ORM\Column(type="text", nullable=true)
     */
    protected $introductionProjectPurpose;

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018"})
     */
    public function isValidPracticeRegions(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPracticeRegions();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose anything')
                ->atPath('practiceRegions')
                ->addViolation();
        }
    }

    /**
     * Set practiceChanges
     *
     * @param string $practiceChanges
     *
     * @return Application
     */
    public function setPracticeChanges($practiceChanges)
    {
        $this->practiceChanges = $practiceChanges;
        return $this;
    }

    /**
     * Get practiceChanges
     *
     * @return string
     */
    public function getPracticeChanges()
    {
        return $this->practiceChanges;
    }


    /**
     * Set projectPurpose
     *
     * @param string $projectPurpose
     *
     * @return Application
     */
    public function setProjectPurpose($projectPurpose)
    {
        $this->projectPurpose = $projectPurpose;

        return $this;
    }

    /**
     * Get projectPurpose
     *
     * @return string
     */
    public function getProjectPurpose()
    {
        return $this->projectPurpose;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return Application
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;
        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Add beneficiaryProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem
     *
     * @return Application
     */
    public function addBeneficiaryProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem)
    {
        $beneficiaryProblem->setApplication($this);
        $this->beneficiaryProblems[] = $beneficiaryProblem;
        return $this;
    }

    /**
     * Remove beneficiaryProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem
     */
    public function removeBeneficiaryProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem)
    {
        $this->beneficiaryProblems->removeElement($beneficiaryProblem);
    }

    /**
     * Get beneficiaryProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryProblems()
    {
        return $this->beneficiaryProblems;
    }

    /**
     * Add practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     *
     * @return Application
     */
    public function addPracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions[] = $practiceRegion;
        return $this;
    }

    /**
     * Remove practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     */
    public function removePracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions->removeElement($practiceRegion);
    }

    /**
     * Get practiceRegions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeRegions()
    {
        return $this->practiceRegions;
    }

    public function removeExpectedResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $expectedResult)
    {
        $this->expectedResults->removeElement($expectedResult);
    }

    /**
     * Get expectedResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpectedResults()
    {
        return $this->expectedResults;
    }

    /**
     * Add expectedResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $expectedResult
     *
     * @return Application
     */
    public function addExpectedResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $expectedResult)
    {
        $expectedResult->setExpectedResultApplication($this);
        $this->expectedResults[] = $expectedResult;
        return $this;
    }

    /**
     * Add practiceResult
     *
     * @param \NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceResult
     *
     * @return Application
     */
    public function addPracticeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceResult)
    {
        $practiceResult->setPracticeResultApplication($this);
        $this->practiceResults[] = $practiceResult;
        return $this;
    }

    public function getPracticeResults()
    {
        return $this->practiceResults;
    }

    public function removePracticeResult(\NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult $practiceResult)
    {
        $this->practiceResults->removeElement($practiceResult);
    }



    /**
     * Add sustainabilityPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityPracticeApplication
     *
     * @return Application
     */
    public function addSustainabilityPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityPracticeApplication)
    {
        $this->sustainabilityPracticeApplications[] = $sustainabilityPracticeApplication;
        $sustainabilityPracticeApplication->setSustainabilityPracticeApplication($this);
        return $this;
    }

    /**
     * Remove sustainabilityPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityPracticeApplication
     */
    public function removeSustainabilityPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityPracticeApplication)
    {
        $this->sustainabilityPracticeApplications->removeElement($sustainabilityPracticeApplication);
    }

    /**
     * Get sustainabilityPracticeApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSustainabilityPracticeApplications()
    {
        return $this->sustainabilityPracticeApplications;
    }

    /**
     * Add sustainabilitySpreadApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilitySpreadApplication
     *
     * @return Application
     */
    public function addSustainabilitySpreadApplication(\NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilitySpreadApplication)
    {
        $this->sustainabilitySpreadApplications[] = $sustainabilitySpreadApplication;
        $sustainabilitySpreadApplication->setSustainabilitySpreadApplication($this);

        return $this;
    }

    /**
     * Remove sustainabilitySpreadApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilitySpreadApplication
     */
    public function removeSustainabilitySpreadApplication(\NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilitySpreadApplication)
    {
        $this->sustainabilitySpreadApplications->removeElement($sustainabilitySpreadApplication);
    }

    /**
     * Get sustainabilitySpreadApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSustainabilitySpreadApplications()
    {
        return $this->sustainabilitySpreadApplications;
    }

    /**
     * Add sustainabilityMonitoringApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityMonitoringApplication
     *
     * @return Application
     */
    public function addSustainabilityMonitoringApplication(\NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityMonitoringApplication)
    {
        $this->sustainabilityMonitoringApplications[] = $sustainabilityMonitoringApplication;
        $sustainabilityMonitoringApplication->setSustainabilityMonitoringApplication($this);

        return $this;
    }

    /**
     * Add projectTeam
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Team $projectTeam
     *
     * @return Application
     */
    public function addProjectTeam(\NKO\OrderBundle\Entity\Application\Continuation\Team $projectTeam)
    {
        $projectTeam->setApplication($this);
        $this->projectTeams[] = $projectTeam;

        return $this;
    }

    /**
     * Remove sustainabilityMonitoringApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityMonitoringApplication
     */
    public function removeSustainabilityMonitoringApplication(\NKO\OrderBundle\Entity\Application\Continuation\ContextApplication $sustainabilityMonitoringApplication)
    {
        $this->sustainabilityMonitoringApplications->removeElement($sustainabilityMonitoringApplication);
    }

    /**
     * Get sustainabilityMonitoringApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSustainabilityMonitoringApplications()
    {
        return $this->sustainabilityMonitoringApplications;
    }

    /**
     * Add realizationPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication
     *
     * @return Application
     */
    public function addRealizationPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication)
    {
        $this->realizationPracticeApplications[] = $realizationPracticeApplication;
        $realizationPracticeApplication->setRealizationPracticeApplication($this);
        return $this;
    }

    /**
     * Remove realizationPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication
     */
    public function removeRealizationPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $realizationPracticeApplication)
    {
        $this->realizationPracticeApplications->removeElement($realizationPracticeApplication);
    }

    /**
     * Get realizationPracticeApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRealizationPracticeApplications()
    {
        return $this->realizationPracticeApplications;
    }

    /**
     * Remove projectTeam
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Team $projectTeam
     */
    public function removeProjectTeam(\NKO\OrderBundle\Entity\Application\Continuation\Team $projectTeam)
    {
        $this->projectTeams->removeElement($projectTeam);
    }

    /**
     * Get projectTeams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectTeams()
    {
        return $this->projectTeams;
    }

    /**
     * Add projectContPartner
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner $projectContPartner
     *
     * @return Application
     */
    public function addProjectContPartner(\NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner $projectContPartner)
    {
        $projectContPartner->setApplication($this);
        $this->projectContPartners[] = $projectContPartner;

        return $this;
    }

    /**
     * Add spreadPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication
     *
     * @return Application
     */
    public function addSpreadPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication)
    {
        $this->spreadPracticeApplications[] = $spreadPracticeApplication;
        $spreadPracticeApplication->setSpreadPracticeApplication($this);
        return $this;
    }

    /**
     * Remove spreadPracticeApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication
     */
    public function removeSpreadPracticeApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $spreadPracticeApplication)
    {
        $this->spreadPracticeApplications->removeElement($spreadPracticeApplication);
    }

    /**
     * Get spreadPracticeApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpreadPracticeApplications()
    {
        return $this->spreadPracticeApplications;
    }

    /**
     * Add risePotentialApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication
     *
     * @return Application
     */
    public function addRisePotentialApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication)
    {
        $this->risePotentialApplications[] = $risePotentialApplication;
        $risePotentialApplication->setRisePotentialApplication($this);
        return $this;
    }

    /**
     * Remove risePotentialApplication
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication
     */
    public function removeRisePotentialApplication(\NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication $risePotentialApplication)
    {
        $this->risePotentialApplications->removeElement($risePotentialApplication);
    }

    /**
     * Get risePotentialApplications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRisePotentialApplications()
    {
        return $this->risePotentialApplications;
    }



    /**
     * Add effectivenessImplementationEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationEtcItem
     *
     * @return Application
     */
    public function addEffectivenessImplementationEtcItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationEtcItem)
    {
        $effectivenessImplementationEtcItem->setEffectivenessEtcApplication($this);
        $this->effectivenessImplementationEtcItems[] = $effectivenessImplementationEtcItem;

        return $this;
    }

    /**
     * Remove effectivenessImplementationEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationEtcItem
     */
    public function removeEffectivenessImplementationEtcItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessImplementationEtcItem)
    {
        $this->effectivenessImplementationEtcItems->removeElement($effectivenessImplementationEtcItem);
    }

    /**
     * Get effectivenessImplementationEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessImplementationEtcItems()
    {
        return $this->effectivenessImplementationEtcItems;
    }

    /**
     * Add effectivenessDisseminationEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationEtcItem
     *
     * @return Application
     */
    public function addEffectivenessDisseminationEtcItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationEtcItem)
    {
        $effectivenessDisseminationEtcItem->setEffectivenessDisseminationEtcApplication($this);
        $this->effectivenessDisseminationEtcItems[] = $effectivenessDisseminationEtcItem;

        return $this;
    }

    /**
     * Remove effectivenessDisseminationEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationEtcItem
     */
    public function removeEffectivenessDisseminationEtcItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessDisseminationEtcItem)
    {
        $this->effectivenessDisseminationEtcItems->removeElement($effectivenessDisseminationEtcItem);
    }

    /**
     * Get effectivenessDisseminationEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessDisseminationEtcItems()
    {
        return $this->effectivenessDisseminationEtcItems;
    }

    /**
     * Set potential
     *
     * @param string $potential
     *
     * @return Application
     */
    public function setPotential($potential)
    {
        $this->potential = $potential;

        return $this;
    }

    /**
     * Get potential
     *
     * @return string
     */
    public function getPotential()
    {
        return $this->potential;
    }

    /**
     * Set monitoringProjectPurpose
     *
     * @param string $monitoringProjectPurpose
     *
     * @return Application
     */
    public function setMonitoringProjectPurpose($monitoringProjectPurpose)
    {
        $this->monitoringProjectPurpose = $monitoringProjectPurpose;

        return $this;
    }

    /**
     * Get monitoringProjectPurpose
     *
     * @return string
     */
    public function getMonitoringProjectPurpose()
    {
        return $this->monitoringProjectPurpose;
    }

    /**
     * Add resourceItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceItem
     *
     * @return Application
     */
    public function addResourceItem(\NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceItem)
    {
        $this->resourceItems[] = $resourceItem;
        $resourceItem->setResourceApplication($this);

        return $this;
    }

    /**
     * Remove resourceItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceItem
     */
    public function removeResourceItem(\NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceItem)
    {
        $this->resourceItems->removeElement($resourceItem);
    }

    /**
     * Get resourceItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResourceItems()
    {
        return $this->resourceItems;
    }

    /**
     * Add resourceEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceEtcItem
     *
     * @return Application
     */
    public function addResourceEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceEtcItem)
    {
        $this->resourceEtcItems[] = $resourceEtcItem;
        $resourceEtcItem->setResourceEtcApplication($this);

        return $this;
    }

    /**
     * Remove resourceEtcItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceEtcItem
     */
    public function removeResourceEtcItem(\NKO\OrderBundle\Entity\Application\Continuation\Resource $resourceEtcItem)
    {
        $this->resourceEtcItems->removeElement($resourceEtcItem);
    }

    /**
     * Get resourceEtcItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResourceEtcItems()
    {
        return $this->resourceEtcItems;
    }

    /**
     * Remove projectContPartner
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner $projectContPartner
     */
    public function removeProjectContPartner(\NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner $projectContPartner)
    {
        $this->projectContPartners->removeElement($projectContPartner);
    }

    /**
     * Get projectContPartners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectContPartners()
    {
        return $this->projectContPartners;
    }

    /**
     * Set introductionProjectPurpose
     *
     * @param string $introductionProjectPurpose
     *
     * @return Application
     */
    public function setIntroductionProjectPurpose($introductionProjectPurpose)
    {
        $this->introductionProjectPurpose = $introductionProjectPurpose;

        return $this;
    }

    /**
     * Get introductionProjectPurpose
     *
     * @return string
     */
    public function getIntroductionProjectPurpose()
    {
        return $this->introductionProjectPurpose;
    }

    /**
     * Set linkedApplicationHistory
     *
     * @param ApplicationHistory $linkedApplicationHistory
     *
     * @return Application
     */
    public function setLinkedApplicationHistory(ApplicationHistory $linkedApplicationHistory = null)
    {
        $this->linkedApplicationHistory = $linkedApplicationHistory;

        return $this;
    }

    /**
     * Get linkedApplicationHistory
     *
     * @return ApplicationHistory
     */
    public function getLinkedApplicationHistory()
    {
        return $this->linkedApplicationHistory;
    }

    /**
     * Set sumGrant
     *
     * @param string $sumGrant
     *
     * @return Application
     */
    public function setSumGrant($sumGrant)
    {
        $this->sumGrant = $sumGrant;

        return $this;
    }

    /**
     * Get sumGrant
     *
     * @return string
     */
    public function getSumGrant()
    {
        return $this->sumGrant;
    }

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018"})
     */
    public function isValidPriorityDirection(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getPriorityDirection()) {
            $context->buildViolation('Wrong choose!')
                ->atPath('priorityDirection')
                ->addViolation();
        }
    }
}
