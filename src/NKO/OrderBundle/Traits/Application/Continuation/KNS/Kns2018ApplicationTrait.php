<?php

namespace NKO\OrderBundle\Traits\Application\Continuation\KNS;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\OrderBundle\Entity\BaseApplication as Application;

trait Kns2018ApplicationTrait
{
    /**
     * @Assert\NotBlank(message="value is invalid(field must be non empty)", groups={"KNS2017-2", "KNS2019-2"})
     *
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm")
     * @ORM\JoinColumn(name="organizationForm_id", referencedColumnName="id", nullable=true)
     */
    private $organizationForm;

    /**
     * @var bool
     * @ORM\Column(name="is_organization_head_equal_project_head", type="boolean", nullable=true)
     */
    protected $isOrganizationHeadEqualProjectHead;

    /**
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     groups={
     *
     *            }
     *     )
     * @Assert\Length(
     *     min = 20,
     *     minMessage = "paymentAccount must be at least 20 characters long",
     *     groups={
     *
     *            }
     *     )
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *              "KNS2017-2", "ContinuationApplicationKNS", "KNS2018-3", "KNS2019-2"
     *            }
     *     )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */

    private $personalAccount;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *            }
     *     )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameAddressee;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity", mappedBy="farvater2017Application",
     *      orphanRemoval=true, cascade={"all"})
     *
     * @Assert\Valid
     */
    private $practiceImplementationActivities;


    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $beneficiaryProblems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness", mappedBy="effectivenessApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $effectivenessItems;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $effectivenessComment;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result", mappedBy="application", cascade={"all"}, orphanRemoval=true)
     */
    protected $individualSocialResults;

    /**
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     groups={
     *            }
     *     )
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $individualSocialResultsComment;


    /**
     * Set isOrganizationHeadEqualProjectHead
     *
     * @param boolean $isOrganizationHeadEqualProjectHead
     *
     * @return Application
     */
    public function setIsOrganizationHeadEqualProjectHead($isOrganizationHeadEqualProjectHead)
    {
        $this->isOrganizationHeadEqualProjectHead = $isOrganizationHeadEqualProjectHead;

        return $this;
    }

    /**
     * Get isOrganizationHeadEqualProjectHead
     *
     * @return boolean
     */
    public function getIsOrganizationHeadEqualProjectHead()
    {
        return $this->isOrganizationHeadEqualProjectHead;
    }

    /**
     * Set organizationForm
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm
     *
     * @return Application
     */
    public function setOrganizationForm(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm $organizationForm = null)
    {
        $this->organizationForm = $organizationForm;

        return $this;
    }

    /**
     * Get organizationForm
     *
     * @return \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm
     */
    public function getOrganizationForm()
    {
        return $this->organizationForm;
    }

    /**
     * Set personalAccount
     *
     * @param string $personalAccount
     *
     * @return Application
     */
    public function setPersonalAccount($personalAccount)
    {
        $this->personalAccount = $personalAccount;

        return $this;
    }

    /**
     * Get personalAccount
     *
     * @return string
     */
    public function getPersonalAccount()
    {
        return $this->personalAccount;
    }

    /**
     * Set nameAddressee
     *
     * @param string $nameAddressee
     *
     * @return Application
     */
    public function setNameAddressee($nameAddressee)
    {
        $this->nameAddressee = $nameAddressee;

        return $this;
    }

    /**
     * Get nameAddressee
     *
     * @return string
     */
    public function getNameAddressee()
    {
        return $this->nameAddressee;
    }

    /**
     * Add practiceImplementationActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity
     *
     * @return Application
     */
    public function addPracticeImplementationActivity(\NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity)
    {
        $this->practiceImplementationActivities[] = $practiceImplementationActivity;
        $practiceImplementationActivity->setFarvater2017Application($this);

        return $this;
    }

    /**
     * Remove practiceImplementationActivity
     *
     * @param \NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity
     */
    public function removePracticeImplementationActivity(\NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity $practiceImplementationActivity)
    {
        $this->practiceImplementationActivities->removeElement($practiceImplementationActivity);
    }

    /**
     * Get practiceImplementationActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeImplementationActivities()
    {
        return $this->practiceImplementationActivities;
    }

    //TODO this is for fix bug of
    public function setPracticeImplementationActivities($practiceImplementationActivities)
    {
        $this->practiceImplementationActivities = new ArrayCollection();
        foreach ($practiceImplementationActivities as $implementationActivity) {
            $this->practiceImplementationActivities[] = $implementationActivity;
            $implementationActivity->setFarvater2017Application($this);
        }

        return $this;
    }

    /**
     * Add peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     *
     * @return Application
     */
    public function addPeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories[] = $peopleCategory;

        return $this;
    }

    /**
     * Remove peopleCategory
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory
     */
    public function removePeopleCategory(\NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory $peopleCategory)
    {
        $this->peopleCategories->removeElement($peopleCategory);
    }

    /**
     * Get peopleCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeopleCategories()
    {
        return $this->peopleCategories;
    }

    /**
     * Add beneficiaryProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem
     *
     * @return Application
     */
    public function addBeneficiaryProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem)
    {
        $this->beneficiaryProblems[] = $beneficiaryProblem;
        $beneficiaryProblem->setApplication($this);
        return $this;
    }

    /**
     * Remove beneficiaryProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem
     */
    public function removeBeneficiaryProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $beneficiaryProblem)
    {
        $this->beneficiaryProblems->removeElement($beneficiaryProblem);
    }

    /**
     * Get beneficiaryProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaryProblems()
    {
        return $this->beneficiaryProblems;
    }

    /**
     * @Assert\Callback(groups={"KNS2017-2"})
     */
    public function isValidPracticeRegions(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPracticeRegions();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose anything')
                ->atPath('practiceRegions')
                ->addViolation();
        }
    }

    /**
     * Add effectivenessItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem
     *
     * @return Application
     */
    public function addEffectivenessItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem)
    {
        $this->effectivenessItems[] = $effectivenessItem;
        $effectivenessItem->setEffectivenessApplication($this);
        return $this;
    }

    /**
     * Remove effectivenessItem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem
     */
    public function removeEffectivenessItem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness $effectivenessItem)
    {
        $this->effectivenessItems->removeElement($effectivenessItem);
    }

    /**
     * Get effectivenessItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEffectivenessItems()
    {
        return $this->effectivenessItems;
    }

    /**
     * Add practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     *
     * @return Application
     */
    public function addPracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions[] = $practiceRegion;

        return $this;
    }

    /**
     * Remove practiceRegion
     *
     * @param \NKO\OrderBundle\Entity\Region $practiceRegion
     */
    public function removePracticeRegion(\NKO\OrderBundle\Entity\Region $practiceRegion)
    {
        $this->practiceRegions->removeElement($practiceRegion);
    }

    /**
     * Get practiceRegions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPracticeRegions()
    {
        return $this->practiceRegions;
    }

    /**
     * Add individualSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result $individualSocialResult
     *
     * @return Application
     */
    public function addIndividualSocialResult(\NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result $individualSocialResult)
    {
        $this->individualSocialResults[] = $individualSocialResult;
        $individualSocialResult->setApplication($this);

        return $this;
    }

    /**
     * Remove individualSocialResult
     *
     * @param \NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result $individualSocialResult
     */
    public function removeIndividualSocialResult(\NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result $individualSocialResult)
    {
        $this->individualSocialResults->removeElement($individualSocialResult);
    }

    /**
     * Get individualSocialResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIndividualSocialResults()
    {
        return $this->individualSocialResults;
    }

    /**
     * @Assert\Callback(groups={"KNS2017-2", "KNS2019-2"})
     */
    public function isValidPeopleCategories(ExecutionContextInterface $context, $payload)
    {
        $categories = $this->getPeopleCategories();
        if ($categories->isEmpty()) {
            $context->buildViolation('please, choose people categories')
                ->atPath('peopleCategories')
                ->addViolation();
        }
    }

    /**
     * Set effectivenessComment
     *
     * @param string $effectivenessComment
     *
     * @return Application
     */
    public function setEffectivenessComment($effectivenessComment)
    {
        $this->effectivenessComment = $effectivenessComment;

        return $this;
    }

    /**
     * Get effectivenessComment
     *
     * @return string
     */
    public function getEffectivenessComment()
    {
        return $this->effectivenessComment;
    }

    /**
     * Set individualSocialResultsComment
     *
     * @param string $individualSocialResultsComment
     *
     * @return Application
     */
    public function setIndividualSocialResultsComment($individualSocialResultsComment)
    {
        $this->individualSocialResultsComment = $individualSocialResultsComment;

        return $this;
    }

    /**
     * Get individualSocialResultsComment
     *
     * @return string
     */
    public function getIndividualSocialResultsComment()
    {
        return $this->individualSocialResultsComment;
    }
}
