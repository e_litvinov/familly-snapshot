<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/14/18
 * Time: 12:10 PM
 */

namespace NKO\OrderBundle\Traits;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait SpecialistTargetGroupTrait
{
    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem", mappedBy="specialistApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $specialistProblems;

    /**
     * @Assert\Valid
     *
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup", mappedBy="specialistApplication", cascade={"all"}, orphanRemoval=true)
     */
    protected $otherSpecialistTargetGroups;

    /**
     * @Assert\Callback(groups={"ContinuationApplication-2018", "ContinuationApplication-2", "ContinuationSF18-2019"})
     */
    public function isValidSpecialistTargetGroups(ExecutionContextInterface $context, $payload)
    {
        if ($this->getSpecialistTargetGroups()->isEmpty()) {
            $context->buildViolation('please, choose anything')
                ->atPath('specialistTargetGroups')
                ->addViolation();
        }
    }

    /**
     * Add specialistProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $specialistProblem
     *
     * @return BaseApplication
     */
    public function addSpecialistProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $specialistProblem)
    {
        $specialistProblem->setSpecialistApplication($this);
        $this->specialistProblems[] = $specialistProblem;

        return $this;
    }

    /**
     * Remove specialistProblem
     *
     * @param \NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $specialistProblem
     */
    public function removeSpecialistProblem(\NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem $specialistProblem)
    {
        $this->specialistProblems->removeElement($specialistProblem);
    }

    /**
     * Get specialistProblems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialistProblems()
    {
        return $this->specialistProblems;
    }

    /**
     * Add otherSpecialistTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherSpecialistTargetGroup
     *
     * @return BaseApplication
     */
    public function addOtherSpecialistTargetGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherSpecialistTargetGroup)
    {
        $otherSpecialistTargetGroup->setSpecialistApplication($this);
        $this->otherSpecialistTargetGroups[] = $otherSpecialistTargetGroup;

        return $this;
    }

    /**
     * Remove otherSpecialistTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherSpecialistTargetGroup
     */
    public function removeOtherSpecialistTargetGroup(\NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup $otherSpecialistTargetGroup)
    {
        $this->otherSpecialistTargetGroups->removeElement($otherSpecialistTargetGroup);
    }

    /**
     * Get otherSpecialistTargetGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOtherSpecialistTargetGroups()
    {
        return $this->otherSpecialistTargetGroups;
    }

    /**
     * Add specialistTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\TargetGroup $specialistTargetGroup
     *
     * @return BaseApplication
     */
    public function addSpecialistTargetGroup(\NKO\OrderBundle\Entity\Application\Continuation\TargetGroup $specialistTargetGroup)
    {
        $this->specialistTargetGroups[] = $specialistTargetGroup;

        return $this;
    }

    /**
     * Remove specialistTargetGroup
     *
     * @param \NKO\OrderBundle\Entity\Application\Continuation\TargetGroup $specialistTargetGroup
     */
    public function removeSpecialistTargetGroup(\NKO\OrderBundle\Entity\Application\Continuation\TargetGroup $specialistTargetGroup)
    {
        $this->specialistTargetGroups->removeElement($specialistTargetGroup);
    }

    /**
     * Get specialistTargetGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialistTargetGroups()
    {
        return $this->specialistTargetGroups;
    }
}
