<?php

namespace NKO\OrderBundle\AdminConfigurator;

use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;

class OrganizationActivityAdmin implements AdminConfiguratorInterface
{
    use AdminConfiguratorTrait;

    const ORGANIZATION_INFO_DESCRIPTION = [
        'dateRegistrationOfOrganization' => '1.14 <b>Дата регистрации организации</b>',
        'primaryActivity' => '1.15 <b>Основные виды деятельности организации</b>(согласно Уставу, соответствующие направлениям Конкурса и предлагаемому проекту)',
        'mission'=>'1.16 <b>Миссия</b> организации (при наличии)',
        'countEmployeesLabel'=>'1.17 Количество <b>сотрудников и добровольцев</b> организации',
        'countStaffEmployees'=>'Штатных сотрудников (на дату подачи заявки), чел.',
        'countInvolvingEmployees'=>'Привлеченных специалистов (на дату подачи заявки), чел.',
        'countVolunteerEmployees'=>'Добровольцев (за последние 2 года), чел.',
        'projects' => '1.18 <b>Основные реализованные проекты (программы) организации</b> за последние два года (по теме данного Конкурса)',
        'publications'=>'1.20 <b>Публикации об организации за последние 2 года.</b><br><br><i>Укажите не более 5 релевантных материалов</i>',
        'linkToAnnualReport'=> '1.19 Ссылка на последний <b>годовой отчет</b> Организации (при наличии)',
        'linkToAnnualReportHelp'=> 'URL должен начинаться с http:// или https://'
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::ORGANIZATION_INFO_DESCRIPTION, $description);
        $formMapper
            ->add('dateRegistrationOfOrganization', DateType::class, [
                'required' => false,
                'label' => $description['dateRegistrationOfOrganization'],
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-date-format' => 'DD-MM-YYYY',
                    'data-date-start-date' => '01-01-1917',
                    'maxlength' => 10
                ]
            ])
            ->add('primaryActivity', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['primaryActivity'],
                'attr' => [
                    'maxlength' => 450]
            ])
            ->add('mission', FullscreenTextareaType::class, [
                'required' => false,
                'label' => $description['mission'],
                'attr' => [
                    'maxlength' => 450
                ]
            ])
            ->add('countEmployeesLabel', CustomTextType::class, [
                'help' => $description['countEmployeesLabel'],
            ])
            ->add('countStaffEmployees', TextType::class, [
                'required' => false,
                'label' => $description['countStaffEmployees'],
            ])
            ->add('countInvolvingEmployees', TextType::class, [
                'required' => false,
                'label' => $description['countInvolvingEmployees'],
            ])
            ->add('countVolunteerEmployees', TextType::class, [
                'required' => false,
                'label' => $description['countVolunteerEmployees'],
            ])
            ->add('projects', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['projects'],
                'by_reference' => false
            ], [
                'admin_code' => $parameters['admin_code']['projects'],
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('linkToAnnualReport', TextType::class, [
                'required' => false,
                'help' => $description['linkToAnnualReportHelp'],
                'label' => $description['linkToAnnualReport'],
                'attr' => [
                    'placeholder' => '-'
                ]
            ])
            ->add('publications', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['publications'],
                'by_reference' => false
            ], [
                'admin_code' => $parameters['admin_code']['publications'],
                'edit' => 'inline',
                'inline' => 'table',
            ]);

        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}