<?php

namespace NKO\OrderBundle\AdminConfigurator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BudgetAdmin implements AdminConfiguratorInterface
{
    use AdminConfiguratorTrait;

    const DESCRIPTIONS = [
        'entirePeriodEstimatedFinancing' => '<b>4.1.</b>Ориентировочная сумма запрашиваемого финансирования 
            <b>на весь период</b> реализации проекта <i>(в тыс. рублей)</i>',
        'entirePeriodEstimatedCoFinancing' => '<b>4.2.</b>Предполагаемая сумма софинансирования 
            <b>на весь период</b> реализации проекта (в тыс. рублей)<i>(в тыс. рублей)</i>',
        'firstYearEstimatedFinancing' => '<b>4.3.</b> Сумма запрашиваемого финансирования 
            <b>на первый год</b> реализации проекта (в точном соответствии с суммой, указанной в файле-приложении «Бюджет проекта»)
            <i>(в тыс. рублей)</i>',
        'firstYearEstimatedCoFinancing' => '<b>4.4.</b>Общая сумма софинансирования (за счет собственных средств и средств партнеров)
            <b>на первый год</b> реализации проекта (в точном соответствии с данными, указанными в файле-приложении «Бюджет проекта»)
             <i>(в тыс. рублей)</i>',
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::DESCRIPTIONS, $description);

        $formMapper
            ->add('entirePeriodEstimatedFinancing', TextType::class, [
                'label' => $description['entirePeriodEstimatedFinancing'],
                'required' => false
            ])
            ->add('entirePeriodEstimatedCoFinancing', TextType::class, [
                'label' => $description['entirePeriodEstimatedCoFinancing'],
                'required' => false
            ])
            ->add('firstYearEstimatedFinancing', TextType::class, [
                'label' => $description['firstYearEstimatedFinancing'],
                'required' => false
            ])
            ->add('firstYearEstimatedCoFinancing', TextType::class, [
                'label' => $description['firstYearEstimatedCoFinancing'],
                'required' => false
            ]);
    }
}
