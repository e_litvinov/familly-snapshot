<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 19.7.18
 * Time: 11.39
 */

namespace NKO\OrderBundle\AdminConfigurator\FinanceReport;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class FinanceSpentAdmin implements AdminConfiguratorInterface
{
    use AdminConfiguratorTrait;

    const DESCRIPTION = [
        'expenseType' => 'Статьи расходов', 'adminCode' => 'nko_order.admin.expense_type',
        'approvedSum' => 'Утвержденная сумма по статье на весь период реализации проекта, руб',
        'periodCosts' => 'Расходы отчетного периода, руб',
        'summaryIncrementalCosts' => 'Расходы нарастающим итогом, руб.',
        'balance' =>'Баланс (остаток / перерасход), руб.'
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::DESCRIPTION, $description);
        $formMapper
            ->add('expenseType', 'sonata_type_admin', [
                'label' => $description['expenseType'],
            ], [
                'admin_code' => $parameters['admin_code']['expenseType'],
            ])
            ->add('approvedSum', NumberType::class, [
                'required' => false,
                'label' => $description['approvedSum'],
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('periodCosts', NumberType::class, [
                'label' => $description['periodCosts'],
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('incrementalCosts', HiddenType::class, [])
            ->add('summaryIncrementalCosts', NumberType::class, [
                'label' => $description['summaryIncrementalCosts'],
                'mapped' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('balance', NumberType::class, [
                'label' => $description['balance'],
                'attr' => [
                    'readonly' => true
                ]
            ]);

        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}
