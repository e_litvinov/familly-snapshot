<?php

namespace NKO\OrderBundle\AdminConfigurator;

use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;

class BankDetailsAdmin implements AdminConfiguratorInterface
{
    const BANK_DETAILS_DESCRIPTION = [
        'bankDetails' => '<b>2.1 Банковские реквизиты организации</b>',
        'iNN' => 'ИНН',
        'kPP' => 'КПП',
        'oKPO' => 'ОКПО',
        'oKTMO' => 'ОКТМО',
        'oKVED' => 'ОКВЭД (через точку с запятой)',
        'kBK' => 'КБК (при наличии)',
        'bankName' => 'Наименование учреждения банка',
        'bankLocation' => 'Местонахождение банка',
        'bankINN' => 'ИНН банка',
        'bankKPP' => 'КПП банка',
        'correspondentAccount' => 'Корреспондентский счёт',
        'bIK' => 'БИК',
        'paymentAccount' => 'Расчётный счёт',
        'personalAccount' => 'Лицевой счет организации',
        'nameAddressee' => 'Наименование получателя'
    ];

    const HELPS = [
        'iNN' => 'ИНН должен состоять из 10 цифр',
        'kPP' => 'КПП должен состоять из 9 цифр',
        'oKPO' => 'ОКПО должен состоять из 8 или 10 цифр',
        'oKTMO' => 'ОКТМО должен состоять из 8 или 11 цифр',
        'kBK' => 'Если KБK нет, оставьте поле пустым',
        'bankINN' => 'ИНН банка должен состоять из 10 цифр',
        'bankKPP' => 'КПП банка должен состоять из 9 цифр',
        'correspondentAccount' => 'Корреспондентский счёт банка должен состоять из 20 цифр',
        'bIK' => 'БИК банка должен состоять из 9 цифр',
        'paymentAccount' => 'Расчётный счёт банка должен состоять из 20 цифр',
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = $description ? $description : self::BANK_DETAILS_DESCRIPTION;
        $formMapper
            ->add('bankDetails', CustomTextType::class, [
                'help' => $description['bankDetails'],
            ])
            ->add('iNN', TextType::class, [
                'label' => $description['iNN'],
                'required' => false,
                'attr' => [
                    'maxlength' => 10
                ],
                'help' => self::HELPS['iNN']
            ])
            ->add('kPP', TextType::class, [
                'label' => $description['kPP'],
                'required' => false,
                'attr' => [
                    'maxlength' => 9
                ],
                'help' => self::HELPS['kPP']
            ])
            ->add('oKPO',  TextType::class, [
                'label' => $description['oKPO'],
                'attr' => [
                    'maxlength' => 10
                ],
                'required' => false,
                'help' => self::HELPS['oKPO']
            ])
            ->add('oKTMO', TextType::class, [
                'label' => $description['oKTMO'],
                'required' => false,
                'attr' => [
                    'maxlength' => 11
                ],
                'help' => self::HELPS['oKTMO']
            ])
            ->add('oKVED', TextType::class, [
                'label' => $description['oKVED'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('kBK', TextType::class, [
                'label' => $description['kBK'],
                'required' => false,
                'help' => self::HELPS['kBK']
            ])
            ->add('bankName', TextType::class, [
                'label' => $description['bankName'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('bankLocation', TextType::class, [
                'label' => $description['bankLocation'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('bankINN', TextType::class, [
                'label' => $description['bankINN'],
                'required' => false,
                'attr' => [
                    'maxlength' => 10
                ],
                'help' => self::HELPS['bankINN']
            ])
            ->add('bankKPP', TextType::class, [
                'label' => $description['bankKPP'],
                'required' => false,
                'attr' => [
                    'maxlength' => 9
                ],
                'help' => self::HELPS['bankKPP']
            ])
            ->add('correspondentAccount', TextType::class, [
                'label' => $description['correspondentAccount'],
                'required' => false,
                'attr' => [
                    'maxlength' => 20
                ],
                'help' => self::HELPS['correspondentAccount']
            ])
            ->add('bIK', TextType::class, [
                'label' => $description['bIK'],
                'required' => false,
                'attr' => [
                    'maxlength' => 9
                ],
                'help' => self::HELPS['bIK']
            ])
            ->add('paymentAccount', TextType::class, [
                'label' => $description['paymentAccount'],
                'required' => false,
                'attr' => [
                    'maxlength' => 20
                ],
                'help' => self::HELPS['paymentAccount']
            ])
            ->add('personalAccount', TextType::class, [
                'label' => $description['personalAccount'],
                'required' => false,
            ])
            ->add('nameAddressee', TextType::class, [
                'label' => $description['nameAddressee'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ]
            ]);

        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}