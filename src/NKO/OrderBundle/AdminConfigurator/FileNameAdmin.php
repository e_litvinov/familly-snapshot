<?php

namespace NKO\OrderBundle\AdminConfigurator;

use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityManager;

class FileNameAdmin implements AdminConfiguratorInterface
{
    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $formMapper
            ->add('fileName', HiddenType::class, [
                'label' => false,
                'mapped' => false
            ]);
    }
}