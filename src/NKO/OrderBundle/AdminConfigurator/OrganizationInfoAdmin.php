<?php

namespace NKO\OrderBundle\AdminConfigurator;

use NKO\OrderBundle\Entity\Region;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use NKO\OrderBundle\Form\CustomTextType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;

class OrganizationInfoAdmin implements AdminConfiguratorInterface
{
    const ORGANIZATION_INFO_DESCRIPTION = [
        'competition' => '<b>Название конкурса</b>',
        'nko_name' => '1.1. <b>Полное название организации</b>',
        'abbreviation' => '1.2. <b>Сокращенное название организации</b>',
        'organizationForm' => '1.3. <b>Организационно-правовая форма</b> организации',
        'pSRN' => '1.4. <b>ОГРН организации</b>',
        'legalAddressLabel' => '1.5. Юридический <b>адрес</b>',
        'legalPostCode' => 'Индекс',
        'legalCity' => 'Населенный пункт',
        'legalRegion' => 'Регион',
        'legalStreet' => 'Улица, дом, корпус, номер офиса',
        'isAddressesEqual' => 'Фактический адрес совпадает с юридическим?',
        'actualAddressLabel' => '<span id="_actual_span">1.6 Фактический <b>адрес</b></span>',
        'actualPostCode' => 'Индекс',
        'actualRegion' => 'Регион',
        'actualCity' => 'Населенный пункт',
        'actualStreet' => 'Улица, дом, корпус, номер офиса',
        'siteLinks' => '1.7 <b>Сайт</b> организации в сети Интернет',
        'socialNetworkLinks' => '1.8 Страницы организации в <b>социальных сетях</b>',
        'email' => '1.9. <b>Адрес электронной почты</b> (для оперативного контакта с организацией)',
        'phoneLabel'=> '1.10 <b>Телефон</b> для оперативного контакта с организацией',
        'phoneLabelHelp' => '<label class="control-label">Телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'internationalCode' => '<label class="code">+7</label>',
        'headOfOrganizationLabel' => '<br><br>1.11. <b>Руководитель организации</b>',
        'headOfOrganizationFullName' => 'ФИО',
        'headOfOrganizationPosition' => 'Должность',
        'headOfOrganizationPhoneLabel'=>'<label class="control-label">Телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'headOfOrganizationPhoneInternationalCode'=> '<label class="code">+7</label>',
        'headOfOrganizationMobilePhoneLabel'=> '<label class="control-label">Мобильный телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'headOfOrganizationMobilePhoneInternationalCode'=>'<label class="code">+7</label>',
        'headOfOrganizationEmeil' => 'Адрес электронной почты',
        'isOrganizationHeadEqualProjectHead'=> 'Руководителем проекта является руководитель организации?',
        'headOfProjectLabel' => '<div id="_headOfProject">1.12. <b>Руководитель проекта</b></div>',
        'headOfProjectFullName' => 'ФИО',
        'headOfProjectPosition' => 'Должность',
        'headOfProjectPhoneLabel'=>'<div id="_headOfProject"><label class="control-label">Телефон</label></div><div id="_headOfProject"><label class="help">Код - 3 цифры, номер - 7 цифр</label></div>',
        'headOfProjectPhoneInternationalCode'=> '<div id="_headOfProject"><label class="lab_headOfProject code">+7</label></div>',
        'headOfProjectMobilePhoneLabel'=> '<div id="_headOfProject"><label class="lab_headOfProject control-label">Мобильный телефон</label></div><div id="_headOfProject"><label class="lab_headOfProject help">Код - 3 цифры, номер - 7 цифр</label></div>',
        'headOfProjectMobilePhoneInternationalCode'=>'<div id="_headOfProject"><label class="lab_headOfProject code">+7</label></div>',
        'headOfProjectEmeil' => 'Адрес электронной почты',
        'headOfAccountingLabel' => '1.13. <b class="head-of-accounting">Главный бухгалтер</b>',
        'headOfAccountingFullName'=> 'ФИО',
        'headOfAccountingEmeil'=>'Адрес электронной почты',
        'headOfAccountingPhoneLabel'=>'<label class="control-label head-of-accounting">Телефон</label><label class="help head-of-accounting">Код - 3 цифры, номер - 7 цифр</label>',
        'headOfAccountingPhoneInternationalCode'=>'<label class="code head-of-accounting">+7</label>',
        'headOfAccountingMobilePhoneLabel'=> '<label class="control-label">Мобильный телефон</label><label class="help">Код - 3 цифры, номер - 7 цифр</label>',
        'headOfAccountingMobilePhoneInternationalCode'=>'<label class="code">+7</label>',
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = $description ? $description : self::ORGANIZATION_INFO_DESCRIPTION;
        $formMapper
            ->add('competition')
            ->add('author.nko_name', TextType::class, [
                'label' => $description['nko_name'],
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('name', TextType::class, [
                    'label' => $description['nko_name'],
                    'required' => false,
                ])
            ->add('abbreviation', TextType::class, [
                'label' => $description['abbreviation'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('organizationForm', ModelType::class, [
                'label' => $description['organizationForm'],
                'expanded' => true,
                'multiple' => false,
                'btn_add'=>false,
                'required' => false,
                'placeholder'=> false,
            ])
            ->add('author.pSRN', TextType::class, [
                'label' => $description['pSRN'],
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('pSRN', TextType::class, [
                    'required' => false,
                    'disabled' => true,
                    'label' => $description['pSRN'],
                    'attr' => [
                        'maxlength' => 13,
                        'help' => '<label class="help">ОГРН должен состоять из 13 цифр без пробелов.</label>'
                        ]
                ])
            ->add('legalAddressLabel', CustomTextType::class, [
                'help' => $description['legalAddressLabel'],
            ])
            ->add('legalPostCode', TextType::class, [
                'label' => $description['legalPostCode'],
                'required' => false,
                'attr' => [
                    'maxlength' => 6
                ]
            ])
            ->add('legalRegion', 'sonata_type_model', [
                'required' => false,
                "property" => "name",
                'label' => $description['legalRegion'],
                'choices' => $em
                    ->getRepository(Region::class)
                    ->findBy([], [
                        'name' => 'ASC'
                    ])
            ])
            ->add('legalCity', TextType::class, [
                'label' => $description['legalCity'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ],
            ])
            ->add('legalStreet', TextType::class, [
                'label' => $description['legalStreet'],
                'required' => false,
            ])
            ->add('isAddressesEqual', CheckboxType::class, [
                'label' => $description['isAddressesEqual'],
                'required' => false,
            ])
            ->add('actualAddressLabel', CustomTextType::class, [
                    'help' => $description['actualAddressLabel'],
            ])
            ->add('actualPostCode', TextType::class, [
                'label' => $description['actualPostCode'],
                'required' => false,
                'attr' => [
                    'maxlength' => 6
                ]
            ])
            ->add('actualRegion', 'sonata_type_model', [
                'required' => false,
                "property" => "name",
                'label' => $description['actualRegion'],
                'choices' => $em
                    ->getRepository(Region::class)
                    ->findBy([], [
                        'name' => 'ASC'
                    ])
            ])
            ->add('actualCity', TextType::class, [
                'label' => $description['actualCity'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ],
            ])
            ->add('actualStreet', TextType::class, [
                'label' => $description['actualStreet'],
                'required' => false,
            ])
            ->add('siteLinks', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['siteLinks'],
                    'btn_add' => 'Добавить',
                    'by_reference' => false
                ], [
                    'admin_code' => $parameters['admin_code']['siteLinks'],
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('socialNetworkLinks', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['socialNetworkLinks'],
                'btn_add' => "Добавить",
                'by_reference' => false
            ], [
                'admin_code' => $parameters['admin_code']['socialNetworkLinks'],
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('email', TextType::class, [
                'label' => $description['email'],
                'required' => false,
            ])
            ->add('phoneLabel', CustomTextType::class, [
                'help' => $description['phoneLabel'],
            ])
            ->add('phoneLabelHelp', CustomTextType::class, [
                'help' => $description['phoneLabelHelp'],
            ])
            ->add('internationalCode', CustomTextType::class, [
                'help' => $description['internationalCode'],
            ])
            ->add('phoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                    ]
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ]
            ])
            ->add('headOfOrganizationLabel', CustomTextType::class, [
                'help' => $description['headOfOrganizationLabel'],
            ])
            ->add('headOfOrganizationFullName', TextType::class, [
                'label' => 'ФИО',
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ],
            ])
            ->add('headOfOrganizationPosition', TextType::class, [
                'label' => 'Должность',
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ],
            ])
            ->add('headOfOrganizationPhoneLabel', CustomTextType::class, [
                'help' => $description['headOfOrganizationPhoneLabel'],
            ])
            ->add('headOfOrganizationPhoneInternationalCode', CustomTextType::class, [
                'help' => $description['headOfOrganizationPhoneInternationalCode'],
            ])
            ->add('headOfOrganizationPhoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ],
            ])
            ->add('headOfOrganizationPhone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ],
            ])
            ->add('headOfOrganizationMobilePhoneLabel', CustomTextType::class, [
                'help' => $description['headOfOrganizationMobilePhoneLabel'],
            ])
            ->add('headOfOrganizationMobilePhoneInternationalCode', CustomTextType::class, [
                'help' => $description['headOfOrganizationMobilePhoneInternationalCode'],
            ])
            ->add('headOfOrganizationMobilePhoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ],
            ])
            ->add('headOfOrganizationMobilePhone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ],
            ])
            ->add('headOfOrganizationEmeil', TextType::class, [
                'label' => $description['headOfOrganizationEmeil'],
                'required' => false,
            ])
            ->add('isOrganizationHeadEqualProjectHead', CheckboxType::class, [
                'label' => $description['isOrganizationHeadEqualProjectHead'],
                'required' => false
            ])
            ->add('headOfProjectLabel', CustomTextType::class, [
                'help' => $description['headOfProjectLabel'],
            ])
            ->add('headOfProjectFullName', TextType::class, [
                'label' => $description['headOfProjectFullName'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ],
            ])
            ->add('headOfProjectPosition', TextType::class, [
                'label' => $description['headOfProjectPosition'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ],
            ])
            ->add('headOfProjectPhoneLabel', CustomTextType::class, [
                'help' => $description['headOfProjectPhoneLabel'],
            ])
            ->add('headOfProjectPhoneInternationalCode', CustomTextType::class, [
                'help' => $description['headOfProjectPhoneInternationalCode'],
            ])
            ->add('headOfProjectPhoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ],
            ])
            ->add('headOfProjectPhone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ],
            ])
            ->add('headOfProjectMobilePhoneLabel', CustomTextType::class, [
                'help' => $description['headOfProjectMobilePhoneLabel'],
            ])
            ->add('headOfProjectMobilePhoneInternationalCode', CustomTextType::class, [
                'help' => $description['headOfProjectMobilePhoneInternationalCode'],
            ])
            ->add('headOfProjectMobilePhoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ],
            ])
            ->add('headOfProjectMobilePhone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ],
            ])
            ->add('headOfProjectEmeil', TextType::class, [
                'label' => $description['headOfProjectEmeil'],
                'required' => false,
            ])
            ->add('headOfAccountingLabel', CustomTextType::class, [
                'help' =>  $description['headOfAccountingLabel'],
            ])
            ->add('headOfAccountingFullName', TextType::class, [
                'label' => $description['headOfAccountingFullName'],
                'required' => false,
                'attr' => [
                    'maxlength' => 255
                ]
            ])
            ->add('headOfAccountingPhoneLabel', CustomTextType::class, [
                'help' => $description['headOfAccountingPhoneLabel'],
            ])
            ->add('headOfAccountingPhoneInternationalCode', CustomTextType::class, [
                'help' => $description['headOfAccountingPhoneInternationalCode'],
            ])
            ->add('headOfAccountingPhoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ],
            ])
            ->add('headOfAccountingPhone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ],
            ])
            ->add('headOfAccountingMobilePhoneLabel', CustomTextType::class, [
                'help' => $description['headOfAccountingMobilePhoneLabel'],
            ])
            ->add('headOfAccountingMobilePhoneInternationalCode', CustomTextType::class, [
                'help' => $description['headOfAccountingMobilePhoneInternationalCode'],
            ])
            ->add('headOfAccountingMobilePhoneCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 3,
                    'placeholder' => 'Код'
                ],
            ])
            ->add('headOfAccountingMobilePhone', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'maxlength' => 7,
                    'placeholder' => 'Номер'
                ],
            ])
            ->add('headOfAccountingEmeil', TextType::class, [
                'label' => $description['headOfAccountingEmeil'],
                'required' => false,
            ]);

        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}
