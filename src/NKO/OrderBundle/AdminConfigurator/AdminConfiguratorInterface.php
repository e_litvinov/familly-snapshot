<?php

namespace NKO\OrderBundle\AdminConfigurator;

use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityManager;

interface AdminConfiguratorInterface
{
    public static function configureFormFields(FormMapper $formMapper, $parameters = null,  EntityManager $em = null, $description = null);
}