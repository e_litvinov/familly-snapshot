<?php

namespace NKO\OrderBundle\AdminConfigurator\Application\Continuation\KNS;

use NKO\OrderBundle\Form\CustomTextType;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;

class ProjectCartAdmin implements AdminConfiguratorInterface
{
    const DESCRIPTION = [
        'competitionNameDuplicate' => '<b>Название конкурса</b>:',
        'realizationYearDuplicate' => '<b>Год реализации</b>',
        'authorNKONameDuplicate' => '<b>Получатель пожертвования:</b>',
        'contractDuplicate' => '<b>Договор пожертвования N:</b>',
        'projectNameDuplicate' => '<b>Название Проекта:</b>',
        'priorityDirectionDuplicate' => '<b>Приоритетное направление:</b>',
        'deadLineLabelDuplicate' => '<b>Сроки реализации Проекта:</b>',
        'deadLineStartDuplicate' => '<b>С:</b>',
        'deadLineFinishDuplicate' => '<b>По:</b>',
        'sumGrantDuplicate' => '<b>Сумма пожертвования, руб:</b>',
        'headOfOrganizationDuplicate' => '<b>Руководитель организации</b> (ФИО, должность):',
        'headOfAccountingDuplicate' => '<b>Главный бухгалтер:</b>',
        'phoneDuplicate' => '<b>Телефон организации:</b>',
        'emailDuplicate' => '<b>Электронная почта:</b>',
        'sumGrant' => '<b>Сумма гранта:</b>',
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::DESCRIPTION, $description);

        $formMapper
            ->add('competitionNameDuplicate', TextType::class, [
                'label' => $description['competitionNameDuplicate'],
                'mapped' => false,
                'required' => false,
                'data' => isset($parameters['competition']) ? $parameters['competition'] : null,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('realizationYearDuplicate', TextType::class, [
                'label' => $description['realizationYearDuplicate'],
                'mapped' => false,
                'required' => false,
                'data' => isset($parameters['year']) ? $parameters['year'] : null,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('authorNKONameDuplicate', TextType::class, [
                'label' => $description['authorNKONameDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('contractDuplicate', TextType::class, [
                'label' => $description['contractDuplicate'],
                'mapped' => false,
                'required' => false,
                'data' => isset($parameters['contract']) ? $parameters['contract'] : null,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('projectNameDuplicate', TextType::class, [
                'label' => $description['projectNameDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('priorityDirectionDuplicate', TextType::class, [
                'label' => $description['priorityDirectionDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('deadLineLabelDuplicate', CustomTextType::class, [
                'help' => self::DESCRIPTION['deadLineLabelDuplicate'],
                'required' => false
            ])
            ->add('deadLineStartDuplicate', TextType::class, [
                'label' => self::DESCRIPTION['deadLineStartDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('deadLineFinishDuplicate', TextType::class, [
                'label' => self::DESCRIPTION['deadLineFinishDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('sumGrantDuplicate', TextType::class, [
                'label' => $description['sumGrantDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('headOfOrganizationDuplicate', TextType::class, [
                'label' => $description['headOfOrganizationDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('headOfAccountingDuplicate', TextType::class, [
                'label' => $description['headOfAccountingDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('phoneDuplicate', TextType::class, [
                'label' => $description['phoneDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('emailDuplicate', TextType::class, [
                'label' => $description['emailDuplicate'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
        ;
    }
}
