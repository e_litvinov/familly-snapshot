<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 19.7.18
 * Time: 11.39
 */

namespace NKO\OrderBundle\AdminConfigurator\AnalyticReport;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PriorityDirectionAdmin implements AdminConfiguratorInterface
{
    const PROJECT_REALIZATION = [
        'priorityDirectionLabel' => '<b>2.1. Приоритетное направление Конкурса:</b>',
        'priorityDirectionEtcLabel' => '<b>Иные эффективные практики</b> в сфере профилактики социального сиротства и 
            семейного устройства детей-сирот и детей, оставшихся без попечения родителей (укажите, какие именно)',
        'purpose' => '<b>2.2 Цель проекта (в отношении реализации Практики)</b>',
        'territory' => '<b>2.3. Территория реализации практики</b>',
        'expectedAnalyticResults' => '<b>2.4. Непосредственные результаты</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'practiceAnalyticIndividualResults' => '<b>Индивидуальные показатели</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'unplannedImmediateResults' => '<b>2.4.1. Незапланированные непосредственные результаты</b>',
        'practiceChanges' => '<b>2.5. Были ли внесены изменения в Практику в рамках реализации проекта (новые элементы, 
            иные целевые группы и пр.)? Если да, то что именно было сделано? Какие выводы Вы для себя сделали?</b>',
        'baseResultPracticeRealization' => '<b>2.6. Основные выводы о реализации практики, достигнутых результатах</b>',
        'unplannedSocialResults' => '<b>2.6.1.1. Незапланированные социальные результаты</b>',
        'successStories' => '<b>2.6.2. Истории, факторы успеха</b><br><i>Кратко опишите, какие были успехи в ходе реализации 
            Практики. Какие факторы способствовали успешной реализации Практики? В случае наличия подробных историй успеха 
            (описание успешного кейса/активности; прямая речь благополучателей или сотрудников, работающих с благополучателями), 
            прикрепите их как приложение в П.6.2.1.</i>',
        'difficulties' => '<b>2.6.3. Трудности в реализации практики (в рамках проекта)</b><br><i><ul><li>Трудности или 
            непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс 
            реализации Проекта (в отношении реализации Практики); что из запланированного не удалось выполнить и почему;</li>
            <li>Пути решения проблем / способы выхода из затруднительных ситуаций или пояснения, почему их нельзя 
            решить силами вашего проекта.</li></ul></i>',
        'lessons' => '<b>2.6.4. Извлечённые уроки</b><br><i>Какие уроки вы для себя извлекли в ходе реализации Практики 
            (в т.ч. благодаря сбору обратной связи)? </i>',
        'socialResults' => '<b>2.6. Основные выводы о реализации практики, достигнутых результатах</b><br><br>
            <b>2.6.1. Социальные результаты </b>'
    ];


    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        switch ($parameters['class']) {
            case Application::class:
                self::configureFormFieldsWithFarvaterApplication($formMapper, $parameters, $em, $description);
                break;
            default:
                self::configureFormFieldsDefault($formMapper, $parameters, $em, $description);
                break;
        }
    }

    private static function configureFormFieldsDefault(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::PROJECT_REALIZATION, $description);

        $formMapper
            ->add('priorityDirection', 'sonata_type_model', [
                'label' => $description['priorityDirectionLabel'],
                'required' => false,
                'expanded'=>true,
            ])
            ->add('priorityDirectionEtc', TextType::class, [
                'label' => $description['priorityDirectionEtcLabel'],
                'required' => false
            ])
            ->add('practiceImplementation.purpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['purpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('territories', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['territory'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.territory',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('expectedAnalyticResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['expectedAnalyticResults'],
            ), array(
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.expected_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('practiceImplementation.unplannedImmediateResults', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedImmediateResults'],
            ))
            ->add('practiceImplementation.practiceChanges', FullscreenTextareaType::class, array(
                'by_reference' => false,
                'required' => false,
                'label' => $description['practiceChanges'],
            ))
            ->add('implementationSocialResults', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['socialResults'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.social_result',
                'edit' => 'inline',
                'inline' => 'table',
                'delete' => 'inline',
            ])
            ->add('practiceImplementation.unplannedSocialResults', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedSocialResults'],
            ))
            ->add('practiceImplementation.successStories', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['successStories'],
            ))
            ->add('practiceImplementation.difficulties', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['difficulties'],
            ))
            ->add('practiceImplementation.lessons', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['lessons'],
            ))
        ;
    }

    private static function configureFormFieldsWithFarvaterApplication(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::PROJECT_REALIZATION, $description);

        $formMapper
            ->add('priorityDirection', 'sonata_type_model', [
                'label' => $description['priorityDirectionLabel'],
                'required' => false,
                'expanded'=>true,
            ])
            ->add('priorityDirectionEtc', TextType::class, [
                'label' => $description['priorityDirectionEtcLabel'],
                'required' => false
            ])
            ->add('practiceImplementation.purpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['purpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('territories', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['territory'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.territory',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('expectedAnalyticResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['expectedAnalyticResults'],
            ), array(
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.expected_analytic_result_farvater',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('practiceAnalyticIndividualResults', 'sonata_type_collection', array(
                    'required' => false,
                    'label' => $description['practiceAnalyticIndividualResults'],
            ), array(
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.expected_analytic_result_farvater',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('practiceImplementation.unplannedImmediateResults', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedImmediateResults'],
            ))
            ->add('practiceImplementation.practiceChanges', FullscreenTextareaType::class, array(
                'by_reference' => false,
                'required' => false,
                'label' => $description['practiceChanges'],
            ))
            ->add('implementationSocialResults', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['socialResults'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.social_result',
                'edit' => 'inline',
                'inline' => 'table',
                'delete' => 'inline',
            ])
            ->add('practiceImplementation.unplannedSocialResults', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedSocialResults'],
            ))
            ->add('practiceImplementation.successStories', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['successStories'],
            ))
            ->add('practiceImplementation.difficulties', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['difficulties'],
            ))
            ->add('practiceImplementation.lessons', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['lessons'],
            ))
        ;
    }
}
