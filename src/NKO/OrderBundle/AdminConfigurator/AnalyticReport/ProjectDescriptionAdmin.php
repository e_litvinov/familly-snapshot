<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 26.6.18
 * Time: 14.16
 */

namespace NKO\OrderBundle\AdminConfigurator\AnalyticReport;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use NKO\OrderBundle\Form\CustomTextType;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProjectDescriptionAdmin implements AdminConfiguratorInterface
{
    use AdminConfiguratorTrait;
    
    const DESCRIPTION = [
        'contactsLabel' => 'КОНТАКТНАЯ ИНФОРМАЦИЯ',
        'recipientLabel' => '<b>Получатель пожертвования:</b>',
        'contractLabel' => '<b>Договор пожертвования N:</b>',
        'dateLabel' => '<b>Дата предоставления отчета:</b>',
        'projectNameLabel' => '<b>Название Проекта:</b>',
        'trainingGrounds' => '<b>Организация </b> – стажировочная площадка: ',
        'trainingGroundsHelp' => 'Поле заполняется автоматически после заполнения поля "стажировочная площадка"',
        'deadLineLabel' => '<b>Сроки реализации Проекта:</b>',
        'startDateLabel' => 'Дата начала',
        'finishDateLabel' => 'Дата окончания',
        'yearLabel' => '<b>Отчетный период:</b>',
        'grantLabel' => '<b>Сумма пожертвования:</b>',
        'directorLabel' => '<b>Руководитель:</b>',
        'accountantLabel' => '<b>Бухгалтер организации:</b>',
        'fullNameLabel' => 'ФИО',
        'positionLabel' => 'Должность',
        'practiceLabel' => 'ПРАКТИКА ПРОЕКТА',
        'priorityDirectionEtc' => '<b>Иные эффективные практики</b> в сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей <i>(укажите, какие именно)</i>'
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::DESCRIPTION, $description);

        $values = [
            'contract' => null,
            'updatedAt' => null,
            'projectName' => null,
            'trainingGroundsSecond' => null,
            'year' => null,
            'sumGrant' => null
        ];
        $values = array_key_exists('value', $parameters) ? array_merge($values, $parameters['value']) : $values;

        $formMapper
            ->add('author.nko_name', null, [
                'label' => $description['recipientLabel'],
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('contract', TextType::class, [
                'label' => $description['contractLabel'],
                'data' => $values['contract'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('updatedAt', TextType::class, [
                'label' => $description['dateLabel'],
                'data' => $values['updatedAt'],
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('projectName', TextType::class, [
                'label' => $description['projectNameLabel'],
                'data' => $values['projectName'],
                'required' => false,
            ])
            ->add('trainingGroundsSecond', TextareaType::class, [
                'label' => $description['trainingGrounds'],
                'help' => $description['trainingGroundsHelp'],
                'required' => false,
                'mapped' => false,
                'data' => $values['trainingGroundsSecond'],
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('deadLineLabel', CustomTextType::class, [
                    'help' => $description['deadLineLabel'],
                    'required' => false
                ])
            ->add('startDateProject', 'sonata_type_datetime_picker', [
                'label' => $description['startDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false,
            ])
            ->add('finishDateProject', DateTimePickerType::class, [
                'label' => $description['finishDateLabel'],
                'format' => 'dd.MM.yyyy',
                'dp_pick_time' => false,
                'required' => false
            ])
            ->add('reportForm.year', TextType::class, [
                'label' => $description['yearLabel'],
                'data' => $values['year'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('sumGrant', TextType::class, [
                'label' => $description['grantLabel'],
                'data' => $values['sumGrant'],
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('directorLabel', CustomTextType::class, [
                    'help' => $description['directorLabel'],
                    'required' => false,
                ])
            ->add('directorName', TextType::class, [
                'label' => $description['fullNameLabel'],
                'required' => false
            ])
            ->add('directorPosition', TextType::class, [
                'label' => $description['positionLabel'],
                'required' => false
            ])
            ->add('accountantLabel', CustomTextType::class, [
                'help' => $description['accountantLabel'],
                'required' => false,
            ])
            ->add('accountantName', TextType::class, [
                'label' => $description['fullNameLabel'],
                'required' => false
            ])
            ->add('accountantPosition', TextType::class, [
                'label' => $description['positionLabel'],
                'required' => false
            ]);

        AdminConfiguratorTrait::changeOptions($formMapper, $parameters);
        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}
