<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 7/2/18
 * Time: 5:04 PM
 */

namespace NKO\OrderBundle\AdminConfigurator\AnalyticReport;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Sonata\AdminBundle\Form\FormMapper;

class AttachmentAdmin implements AdminConfiguratorInterface
{
    use AdminConfiguratorTrait;

    const DESCRIPTION = [
        'practiceFeedback' => '<b>6.1. Обратная связь</b><br><br>
            <b>6.1.1. Обратная связь. Практика</b>',
        'practiceSocialFeedback' => '<b>6.1.2. Реализация практики – социальные результаты</b>',
        'practiceSpreadFeedback' => '<b>6.1.2. Обратная связь. Распространение практики</b>',
        'practiceSocialSpreadFeedback' => '<b>6.1.4. Распространение и внедрение практики – социальные результаты</b>',
        'practiceSuccessStoryAttachments' => '<b>6.2. Истории успеха<br><br>6.2.1. Истории успеха. Реализация практики</br>',
        'practiceSocialFeedback' => '<b>6.1.2. Реализация практики – социальные результаты</b>',
        'practiceSpreadSuccessStoryAttachments' => '<b>6.2.2. Истории успеха. Распространение практики</b>',
        'factorAttachments' => '<b>6.5. Сведения о внедрении практики </b>',
        'materialAttachments' => '<b>6.4. Издание информационных и методических материалов</b>',
        'publicationAttachments' => '<b>6.3. Публикации в рамках проекта</b>',
    ];

    const HELPS = [
        'practiceSuccessStoryAttachments' => '<i>Данное поле не обязательно для заполнения каждый месяц. Пожалуйста, заполните его в свободной форме, если в прошедшем месяце у вас проходили интересные мероприятия в ходе реализации практики или вы получили важные отклики от ваших клиентов или сотрудников. Здесь могут быть:
            <ul><li>Описание успешного кейса/активности</li>
            <li>Прямая речь благополучателей или сотрудников, работающих с благополучателями.</li></ul></i>',
        'feedbackPracticeSocialAttachments' => '<i>Список строк формируется по строкам таблицы 2.6.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'practiceSpreadSuccessStoryAttachments' => '<i>
            Данное поле не обязательно для заполнения каждый месяц. Пожалуйста, заполните его в свободной форме, если в прошедшем месяце у вас проходили интересные мероприятия в ходе распространения практики или вы получили важные отклики от специалистов. Здесь могут быть:
            <ul><li>Описание успешного кейса/активности</li>
            <li>Прямая речь обучавшихся специалистов</li></ul></i>',
        'feedbackSpreadPracticeSocialAttachments' => '<i>Список строк формируется по строкам таблицы 3.4.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'feedbackPracticeAttachments' => 'Список строк формируется по строкам таблицы 2.3.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.',
        'feedbackSpreadPracticeSocialAttachments' => '<i>Список строк формируется по строкам таблицы 3.4.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.</i>',
        'feedbackSpreadPracticeAttachments' => 'Список строк формируется по строкам таблицы 3.1.1., по которым выбрано “Да” в поле “Собирали ли обратную связь”.',
        'publicationAttachments' => 'Количество строк таблицы определяется значением “Число сообщений в СМИ, инициированных в рамках проекта. Факт” в таблице 3.2.',
        'materialAttachments' => 'Количество строк таблицы определяется значением “Число изданных информационных / методических материалов, инициированных в рамках проекта. Факт” в таблице 3.2.',
        'factorAttachments' => 'Кол-во строк в зависимости от значения показателя 15.1. Факт<br>Пожалуйста, приложите файлы, подтверждающие факты внедрения практики.',
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null, $helps = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::DESCRIPTION, $description);
        $helps = AdminConfiguratorTrait::getCompletedDescription(self::HELPS, $helps);

        $formMapper
            ->add('practiceFeedbackAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['practiceFeedback'],
                    'btn_add' => false,
                    'by_reference' => false,
                    'help' => $helps['feedbackPracticeAttachments']
                ], [
                    'admin_code' => 'nko_order.admin.analytic_report.feedback',
                ])
            ->add('practiceSocialFeedbackAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['practiceSocialFeedback'],
                    'btn_add' => false,
                    'by_reference' => false,
                    'help' => $helps['feedbackPracticeSocialAttachments']
                ], [
                    'admin_code' => 'nko_order.admin.analytic_report.feedback',
                ])
            ->add('practiceSpreadFeedbackAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['practiceSpreadFeedback'],
                    'btn_add' => false,
                    'by_reference' => false,
                    'help' => $helps['feedbackSpreadPracticeAttachments']
                ], [
                    'admin_code' => 'nko_order.admin.analytic_report.feedback',
                ])
            ->add('practiceSocialSpreadFeedbackAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['practiceSocialSpreadFeedback'],
                    'btn_add' => false,
                    'by_reference' => false,
                    'help' => $helps['feedbackSpreadPracticeSocialAttachments']
                ], [
                    'admin_code' => 'nko_order.admin.analytic_report.feedback',
                ])
            ->add('practiceSuccessStoryAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['practiceSuccessStoryAttachments'],
                    'btn_add' => 'Добавить',
                    'by_reference' => false,
                    'help' => $helps['practiceSuccessStoryAttachments']
                ], [
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                    'admin_code' => $parameters['admin_code']['practiceSuccessStoryAttachments'],
                ])
            ->add('practiceSpreadSuccessStoryAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['practiceSpreadSuccessStoryAttachments'],
                    'btn_add' => 'Добавить',
                    'by_reference' => false,
                    'help' => $helps['practiceSpreadSuccessStoryAttachments']
                ], [
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                    'admin_code' => $parameters['admin_code']['practiceSpreadSuccessStoryAttachments'],

                ])
            ->add('publicationAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['publicationAttachments'],
                    'by_reference' => false,
                    'help' => $helps['publicationAttachments']
                ], [
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('materialAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['materialAttachments'],
                    'by_reference' => false,
                    'help' => $helps['materialAttachments']
                ], [
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                ])
            ->add('factorAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => $description['factorAttachments'],
                    'by_reference' => false,
                    'help' => $helps['factorAttachments']
            ], [
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',

                ])
            ->add('monitoringDevelopmentAttachments', 'sonata_type_collection', [
                    'required' => false,
                    'label' => isset($description['monitoringDevelopmentAttachments']) ? $description['monitoringDevelopmentAttachments'] : null,
                    'btn_add' => false,
                    'by_reference' => false,
                ], [
                    'edit' => 'inline',
                    'delete' => 'inline',
                    'inline' => 'table',
                    'admin_code' => $parameters['admin_code']['monitoringDevelopmentAttachments'],
                ]);

        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}
