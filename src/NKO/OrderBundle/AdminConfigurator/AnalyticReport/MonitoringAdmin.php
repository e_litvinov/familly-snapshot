<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 28.6.18
 * Time: 14.38
 */

namespace NKO\OrderBundle\AdminConfigurator\AnalyticReport;

use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use Sonata\AdminBundle\Form\FormMapper;

class MonitoringAdmin implements AdminConfiguratorInterface
{
    const DESCRIPTION = [
        'monitoringResults' => '<b>4.1. Результаты проекта по блоку «Мониторинг и оценка результатов, повышение организационного потенциала»</b>',
        'newMonitoringElements' =>'<b>4.2 Элементы, добавленные в систему  мониторинга и оценки</b>',
        'monitoringChanges' => '<b>4.3. Что вы изменили / стали делать иначе в системе мониторинга и оценки вашей организации за отчетный период?</b>',
        'monitoringDevelopments' => '<b>4.4. План проекта по блоку «Мониторинг и оценка результатов, повышение организационного потенциала» на следующий год</b><br><br>'
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::DESCRIPTION, $description);

        $formMapper
            ->add('monitoringResults', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['monitoringResults'],
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => $parameters['admin_code']['tripleContext'],
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('newMonitoringElements', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['newMonitoringElements'],
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.analytic_report.monitoring_element',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ])
            ->add('monitoringChanges', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['monitoringChanges'],
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.analytic_report.context',
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ])
            ->add('monitoringDevelopments', 'sonata_type_collection', [
                'required' => false,
                'label' => isset($description['monitoringDevelopments']) ? $description['monitoringDevelopments'] : null,
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
            ]);

        AdminConfiguratorTrait::changeOptions($formMapper, $parameters);
        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}