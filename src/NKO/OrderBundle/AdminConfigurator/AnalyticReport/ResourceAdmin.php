<?php

namespace NKO\OrderBundle\AdminConfigurator\AnalyticReport;

use NKO\OrderBundle\Form\FullscreenTextareaType;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;

class ResourceAdmin implements AdminConfiguratorInterface
{
    const RESOURCE_DESCRIPTION = [
        'keyRisks' => '<b>5.1. Ключевые риски проекта</b>',
        'partnerActivities' => '<b>5.2. Активность партнеров и доноров проекта</b>',
        'commandConclusion' => '<b>5.2. Выводы относительно команды проекта и её работы</b><br>
            <i>Как вы в целом оцениваете работу вашей команды, взаимодействие внутри команды в ходе проекта?
             Что получилось, а что – не очень? Что можно было бы сделать по-другому?<br>
            Какие были изменения в составе команды? Какие позитивные и негативные изменения произошли в 
            команде и её работе в ходе реализации проекта (например, повышение профессионального уровня сотрудников, 
            профессиональные награды, или наоборот – рост нагрузки, ухудшение климата в организации, текучка кадров и пр.)?</b>',
        'humanResources' => '<b>5.3. Кадровые ресурсы проекта</b>',
        'partnerConclusion' => '<b>5.3. Выводы относительно участия в проекте партнёров и доноров</b><br>
            <i>Как вы в целом оцениваете участие партнёров и доноров проекта? кого особенно хотелось бы поблагодарить и 
            за что? Кто и почему фактически участвовал не так, как вы планировали, кто «подвёл»?</i></b>',
    ];

    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::RESOURCE_DESCRIPTION, $description);
        $formMapper
            ->add('keyRisks', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['keyRisks'],
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table'
            ])
            ->add('partnerActivities', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['partnerActivities'],
                'btn_add' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => $parameters['admin_code']['partnerActivities'],
            ])
            ->add('commandConclusion', FullscreenTextareaType::class, [
                'label' => $description['commandConclusion'],
                'required' => false,
            ])
            ->add('humanResources', 'sonata_type_collection', [
                'required' => false,
                'label' => $description['humanResources'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'delete' => 'inline',
                'inline' => 'table',
                'admin_code' => $parameters['admin_code']['humanResources'],
            ])
            ->add('partnerConclusion', FullscreenTextareaType::class, [
                'label' => $description['partnerConclusion'],
                'required' => false,
            ])
        ;

        AdminConfiguratorTrait::removeExtraFields($formMapper, $parameters);
    }
}
