<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 19.7.18
 * Time: 13.13
 */

namespace NKO\OrderBundle\AdminConfigurator\AnalyticReport;


use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\AdminConfigurator\AdminConfiguratorInterface;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Form\FullscreenTextareaType;
use NKO\OrderBundle\Traits\AdminConfiguratorTrait;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class SpreadPracticeAdmin implements AdminConfiguratorInterface
{
    const SPREAD_PRACTICE = [
        'purpose' => '<b>3.1. Цель проекта (распространение и внедрение практики) </b>',
        'practiceAnalyticResults' => '<b>3.2. Непосредственные результаты</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'expectedAnalyticIndividualResults' => '<b>Индивидуальные показатели</b><br>Если таблица не умещается на экране, 
            Вы можете воспользоваться горизонтальной прокруткой под ней.',
        'unplannedImmediateResults' => '<b>3.2.1. Незапланированные непосредственные результаты</b>',
        'publications' => '<b>3.3. Публикации</b><br><br>
            <i>После заполнения таблицы загрузите сканы публикаций или укажите гиперссылки на них во вкладке ПРИЛОЖЕНИЯ в поле 6.3.</i>',
        'unplannedSocialResults' => '<b>3.4.1.1. Незапланированные социальные результаты</b>',
        'successStories' => '<b>3.4.2. Истории, факторы успеха</b><br><i>Кратко опишите, какие были успехи в ходе распространения 
            и внедрения Практики. Какие позитивные изменения произошли в работе организаций, внедривших вашу практику? 
            Какие факторы способствовали успешному внедрению Практики? В случае наличия подробных историй успеха (описание 
            успешного кейса/активности; прямая речь обучавшихся специалистов), прикрепите их как приложение в П.6.2.2.</i>',
        'difficulties' => '<b>3.4.3. Трудности в распространении и внедрении практики (в рамках проекта)</b><br><i><ul><li>Трудности 
            или непредвиденные обстоятельства, потребовавшие привлечения дополнительных ресурсов и/или повлиявших на процесс 
            распространения и внедрения практики; что из запланированного не удалось выполнить и почему;</li>
            <li></li>Пути решения проблем / способы выхода из затруднительных ситуаций или пояснения, почему их нельзя решить 
            силами вашего проекта.</ul></i>',
        'lessons' => '<b>3.4.4. Извлечённые уроки</b><br><i>Какие уроки вы для себя извлекли в ходе распространения и внедрения 
            Практики (в т.ч. благодаря сбору обратной связи)? </i>',
        'socialResults' => '<b>3.4. Основные выводы о распространении и внедрении практики, достигнутых результатах</b><br><br>
            <b>3.4.1. Социальные результаты </b>'
    ];


    public static function configureFormFields(FormMapper $formMapper, $parameters = null, EntityManager $em = null, $description = null)
    {
        switch ($parameters['class']) {
            case Application::class:
                self::configureFormFieldsWithFarvaterApplication($formMapper,$parameters,$em,$description);

                break;
            default:
                self::configureFormFieldsDefault($formMapper,$parameters,$em,$description);
                break;
        }
    }

    private static function configureFormFieldsWithFarvaterApplication($formMapper, $parameters, $em, $description)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::SPREAD_PRACTICE, $description);
        $formMapper
            ->add('practiceIntroduction.purpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['purpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('practiceAnalyticResults','sonata_type_collection', [
                'label' => $description['practiceAnalyticResults'],
                'required' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.practice_analytic_result_farvater',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('expectedAnalyticIndividualResults', 'sonata_type_collection', array(
                'required' => false,
                'label' => $description['expectedAnalyticIndividualResults'],
            ), array(
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.practice_analytic_result_farvater',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('practiceIntroduction.unplannedImmediateResults', TextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedImmediateResults'],
            ))
            ->add('analyticPublications', 'sonata_type_collection',array(
                'required' => false,
                'label' => $description['publications'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ),
            array(
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.publication',
                'edit' => 'inline',
                'inline' => 'table',
            ))
            ->add('introductionSocialResults', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['socialResults'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.social_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])

            ->add('practiceIntroduction.unplannedSocialResults', TextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedSocialResults'],
            ))
            ->add('practiceIntroduction.successStories', TextareaType::class, array(
                'required' => false,
                'label' => $description['successStories'],
            ))
            ->add('practiceIntroduction.difficulties', TextareaType::class, array(
                'required' => false,
                'label' => $description['difficulties'],
            ))
            ->add('practiceIntroduction.lessons', TextareaType::class, array(
                'required' => false,
                'label' => $description['lessons'],
            ))
        ;
    }

    private static function configureFormFieldsDefault($formMapper, $parameters, $em, $description)
    {
        $description = AdminConfiguratorTrait::getCompletedDescription(self::SPREAD_PRACTICE, $description);
        $formMapper
            ->add('practiceIntroduction.purpose', FullscreenTextareaType::class, array(
                'required' => false,
                'label' => $description['purpose'],
                'attr' => array('maxlength' => 450),
            ))
            ->add('practiceAnalyticResults','sonata_type_collection', [
                'label' => $description['practiceAnalyticResults'],
                'required' => false,
                'by_reference' => false,
            ], [
                'admin_code' => 'nko_order.admin.report.analytic_report.report2018.practice_analytic_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('practiceIntroduction.unplannedImmediateResults', TextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedImmediateResults'],
            ))
            ->add('analyticPublications', 'sonata_type_collection',array(
                'required' => false,
                'label' => $description['publications'],
                'btn_add' => 'Добавить',
                'by_reference' => false,
            ),
                array(
                    'admin_code' => 'nko_order.admin.report.analytic_report.report2018.publication',
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->add('introductionSocialResults', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => $description['socialResults'],
            ], [
                'admin_code' => 'nko_order.admin.analytic_report_2018.social_result',
                'edit' => 'inline',
                'inline' => 'table',
            ])

            ->add('practiceIntroduction.unplannedSocialResults', TextareaType::class, array(
                'required' => false,
                'label' => $description['unplannedSocialResults'],
            ))
            ->add('practiceIntroduction.successStories', TextareaType::class, array(
                'required' => false,
                'label' => $description['successStories'],
            ))
            ->add('practiceIntroduction.difficulties', TextareaType::class, array(
                'required' => false,
                'label' => $description['difficulties'],
            ))
            ->add('practiceIntroduction.lessons', TextareaType::class, array(
                'required' => false,
                'label' => $description['lessons'],
            ))
        ;
    }
}