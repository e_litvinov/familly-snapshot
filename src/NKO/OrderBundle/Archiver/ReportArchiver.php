<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 09.03.17
 * Time: 11:53
 */

namespace NKO\OrderBundle\Archiver;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\ReportHistory;

class ReportArchiver
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function archive(BaseReport $object)
    {
        $serializedReport = serialize($object);

        $history = new ReportHistory();
        $history->setData($serializedReport);
        $history->setIsAccepted($object->getIsAccepted());
        $history->setIsSend($object->getIsSend() && !$object->getIsAutosaved());
        $history->setPsrn($object->getPsrn());
        $history->setNkoName($object->getAuthor()->getNkoName());
        $history->setPeriod($object->getPeriod());
        $history->setReportForm($object->getReportForm());
        $history->setUnderConsideration($object->getUnderConsideration());

        return $history;
    }
}
