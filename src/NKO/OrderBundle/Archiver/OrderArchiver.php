<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 09.03.17
 * Time: 11:53
 */

namespace NKO\OrderBundle\Archiver;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater2018Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class OrderArchiver
{
    /**
     * @var EntityManager
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function archive(BaseApplication $object)
    {
        $serializedObject = serialize($object);
        $history = new ApplicationHistory();

        $history->setData($serializedObject);
        $history->setNkoName($object->getAuthor()->getNkoName());
        $history->setPsrn($object->getAuthor()->getPsrn());
        $history->setUsername($object->getAuthor()->getUsername());
        $history->setLegalRegion($this->getLegalRegion($object));
        $history->setIsSend($object->getIsSend() && !$object->getIsAutosaved());
        $history->setCompetition($object->getCompetition());
        $history->setIsAppropriate(true);
        $history->setIsSpread(false);
        $history->setWinner($object->getWinner());
        $history->setIsAutosaved($object->getIsAutosaved());

        return $history;
    }
    
    private function getLegalRegion($object)
    {
        $legalRegion = null;
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        if ($propertyAccessor->isReadable($object, 'legalRegion')) {
            return (string) $object->getLegalRegion();
        }
        
        if ($object instanceof Farvater2018Application) {
            $relatedApplicationHistory = $this->container->get('nko_order.resolver.related_competition_resolver')->getApplicationHistory($object);
            $legalRegion = unserialize($relatedApplicationHistory->getData())->getLegalRegion();
        }
        
        return $legalRegion;
    }
}
