<?php

namespace NKO\OrderBundle\Archiver;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\MarkListHistory;

class MarkListArchiver
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function archive(MarkList $object)
    {
        $serialized = serialize($object);

        $history = new MarkListHistory();
        $history->setData($serialized);

        $history->setIsSend($object->getIsSend() && !$object->getIsAutosaved());
        $history->setAuthor($object->getApplication()->getAuthor());
        $history->setCompetition($object->getApplication()->getCompetition());
        $history->setExpert($object->getExpert());
        $history->setApplicationHistory($object->getApplication());

        return $history;
    }
}
