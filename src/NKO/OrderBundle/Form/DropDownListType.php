<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/9/17
 * Time: 3:05 PM
 */

namespace NKO\OrderBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

class DropDownListType extends AbstractType
{
    public function getParent()
    {
        return EntityType::class;
    }
}