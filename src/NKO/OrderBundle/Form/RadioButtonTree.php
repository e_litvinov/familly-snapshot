<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/30/17
 * Time: 11:52 AM
 */

namespace NKO\OrderBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;


class RadioButtonTree extends AbstractType
{
    public function getParent()
    {
        return EntityType::class;
    }
}