<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/8/17
 * Time: 12:00 PM
 */

namespace NKO\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class PhoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new CallbackTransformer(
            function ($string) {
                $data['code'] =  (string)substr($string, 2, 3);
                $data['phone'] = (string)substr($string, 5, 7);

                return $data;
            },
            function ($data) {
                $phone = '+7'.$data['code'].$data['phone'];

                return $phone;
            }
        ))
        ;
    }

    public function getParent()
    {
        return TextType::class;
    }

}