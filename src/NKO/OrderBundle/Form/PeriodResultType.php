<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1.8.18
 * Time: 22.53
 */

namespace NKO\OrderBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;

class PeriodResultType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('totalAmount', null, ['attr' => ['placeholder' => '0']])
            ->add('newAmount', null, ['attr' => ['placeholder' => '0']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PeriodResult::class,
            'validation_groups' => false,
        ]);
    }
}
