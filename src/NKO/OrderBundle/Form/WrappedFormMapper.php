<?php

namespace NKO\OrderBundle\Form;

use Sonata\AdminBundle\Admin\AdminInterface;

class WrappedFormMapper
{
    private $admin;
    private $sections = [];
    private $tabs = [];
    private $currentTab;

    public function __construct(array $sections, array $tabs, AdminInterface $admin, $reflection = null)
    {
        $this->admin = $admin;
        $this->sections = $sections;
        $this->tabs = $tabs;
        $reflection ? $this->changeFormLabels($reflection) : null;
    }

    /**
     * добавляет поле.
     * 1 варинт
     * ->add($name, $afterField, $type, $options, $additional)
     * вставит данное поле после $afterField
     *
     * 2 вариант
     * ->add($name, $afterField, $type, $options, $additional, TRUE)
     * вставит поле ПЕРВЫМ на вкладке, которая содержит $afterField
     *
     * 3 варинт (создание новой вкладки и добавление к ней полей)
     * ->addTab($tab, $tabName)
     *     ->add($name, null, $type, $options, $additional)
     *
     *
     * @param $name, название нового поля
     * @param $afterField, после какого поля вставить поле
     * @param $type, тип поля (TextType::class и др.)
     * @param array $options (парметры 1, как при описании поля в formMapper (label, required и др.))
     * @param array $additional (парметры 2, как при описании поля в formMapper(admin_code и др))
     * @param bool $liftUp (установить первым на вкладке)
     * @return $this
     */
    public function add($name, $afterField, $type, array $options = [], array $additional = [], $liftUp = null)
    {
        $this->sections[$this->currentTab][$name] = ['type' => $type, 'options' => $options, 'additional'=> $additional];

        if ($afterField) {
            $this->move($name, $afterField, $liftUp);
        }

        return $this;
    }

    /**
     * перемещает поле, вставля после $pasteAfterField.
     * Если $liftUp = true - установит поле первым на вкладке, которая содержит $pasteAfterField
     *
     * @param $field
     * @param $pasteAfterField
     * @param null $liftUp
     * @return $this
     */
    public function move($field, $pasteAfterField, $liftUp = null)
    {
        $targetTab = $value = null;

        foreach ($this->sections as $key => &$section) {
            $targetTab = array_key_exists($pasteAfterField, $section) ? $key : $targetTab;
            if (array_key_exists($field, $section)) {
                $value = $section[$field];
                unset($section[$field]);
            }
        }

        if ($liftUp) {
            $this->sections[$targetTab] = [$field => $value] + $this->sections[$targetTab];
        } else {
            $i = array_search($pasteAfterField, array_keys($this->sections[$targetTab]))+1;
            $this->sections[$targetTab] = array_slice($this->sections[$targetTab], 0, $i, true) + [$field => $value] + array_slice($this->sections[$targetTab], $i, null, true);
        }

        $this->setCurrentTab($targetTab);
        return $this;
    }


    /**
     * Изменение поля
     * @param $name
     * @param $type
     * @param array $options
     * @param array $additional
     * @param null $previouslyClear, очистить все параметры, кроме названия поля
     * @return $this
     */
    public function change($name, $type, $options = [], $additional = [], $previouslyClear = null)
    {
        foreach ($this->sections as $key => &$section) {
            if (array_key_exists($name, $section)) {
                if ($previouslyClear) {
                    $section[$name] = ['type' => null, 'options' => [], 'additional'=> []];
                }

                $section[$name]['type'] = $type ? $type : $section[$name]['type'];
                $section[$name]['options'] = $options ? array_merge($section[$name]['options'], $options) : $section[$name]['options'];
                $section[$name]['additional'] = $additional ? array_merge($section[$name]['additional'], $additional) : $section[$name]['additional'];

                $this->setCurrentTab($key);
                break;
            }
        }

        return $this;
    }

    /**
     * Удаление поля
     * @param $name, навание поля, которое нужно удалить
     * @return $this
     */
    public function remove($name)
    {
        foreach ($this->sections as $key => &$section) {
            if (is_array($name)) {
                $removeFields = array_intersect_key(array_flip($name), $section);
                foreach ($removeFields as $item => $field) {
                    unset($section[$item]);
                }
            } elseif (array_key_exists($name, $section)) {
                unset($section[$name]);
            }
        }

        return $this;
    }

    /**
     * Изменение значений:
     * tabs  - константа TABS
     * label - константа DESCRIPTIONS
     * helps - константа HELPS
     *
     * examples:
     * TABS = ['tab' => 'value'];
     * DESCRIPTIONS = ['name property entity' => 'value'];
     * HELPS = ['name property entity' => 'value'];
     *
     * ДАННЫЕ КОНСТАНТЫ ДОЛЖНЫ НАХОДИТЬСЯ В ТЕКУЩЕМ АДМИН КЛАССЕ!
     *
     * @param $reflection
     */
    public function changeFormLabels($reflection)
    {
        //заменяем название вкладок
        if ($reflection->hasConstant('TABS')) {
            foreach ($reflection->getConstant('TABS') as $key => $tabDescription) {
                if (array_key_exists($key, $this->tabs)) {
                    $this->tabs[$key] = $tabDescription;
                }
            }
        }

        $constName = $this->admin->hasParentFieldDescription() ?
            $this->getConstName($this->admin->getParentFieldDescription()->getFieldName()) : 'DESCRIPTIONS';

        $helps = $reflection->hasConstant('HELPS') ? $reflection->getConstant('HELPS') : null;
        $descriptions = $reflection->hasConstant($constName) ? $reflection->getConstant($constName) : null;

        //заменяем название label и helps
        foreach ($this->sections as $key => &$tab) {
            !$this->currentTab ? $this->setCurrentTab($key) : null;

            foreach ($tab as $item => $field) {
                if ($helps && array_key_exists($item, $helps)) {
                    $tab[$item]['options']['help'] = $helps[$item];
                }

                if ($descriptions && array_key_exists($item, $descriptions)) {
                    $tab[$item]['options']['label'] = $descriptions[$item];
                }
            }
        }
    }

    /**
     * Обязательно указывается, как завершение изменений формы! Устанавливает значения в админ класс, по которым далее
     * выстраивается форма
     *
     * @return $this
     */
    public function end()
    {
        method_exists($this->admin, 'setTabs') ? $this->admin->setTabs($this->tabs) : null;
        $this->admin->setFields($this->sections);
        return $this;
    }

    /**
     * @param $tab, идентификатор вкладки
     * @param $tabName, название вкладки
     * @param null $pasteAfterTab, вставить поле указанной вкладки, null- вставить в конец, 'first' - первой
     * @return $this
     */
    public function addTab($tab, $tabName, $pasteAfterTab = null)
    {
        if (!array_key_exists($tab, $this->tabs)) {
            $this->tabs[$tab] = $tabName;
            if ($pasteAfterTab) {
                $pasteAfterTab === 'first' ? $this->moveTab($tab, null) : $this->moveTab($tab, $pasteAfterTab);
            }
        }

        $this->setCurrentTab($tab);
        return $this;
    }

    /**
     * изменение позиции вкладки относительно других
     * @param $tab
     * @param null $pasteAfterTab, указавается вкладка, после которой должна отобраить выбранная. Если null - станет первой вкладкой
     * @return $this
     */
    public function moveTab($tab, $pasteAfterTab = null)
    {
        if (array_key_exists($tab, $this->tabs)) {
            $value = $this->tabs[$tab];
            unset($this->tabs[$tab]);

            if (!$pasteAfterTab) {
                $this->tabs = [$tab => $value] + $this->tabs;
            } else {
                $i = array_search($pasteAfterTab, array_keys($this->tabs))+1;
                $this->tabs = array_slice($this->tabs, 0, $i, true) + [$tab => $value] + array_slice($this->tabs, $i, null, true);
            }
        }

        $this->setCurrentTab($tab);
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function removeTab($name)
    {
        if (array_key_exists($name, $this->tabs)) {
            unset($this->tabs[$name]);
        }

        return $this;
    }

    /**
     * @param $tab
     * @return $this
     */
    public function setCurrentTab($tab)
    {
        if (array_key_exists($tab, $this->tabs)) {
            $this->currentTab = $tab;
        }

        return $this;
    }

    private function getConstName($table)
    {
        return strtoupper(preg_replace('/[A-Z]/', '_$0', $table));
    }

    public function issetTab($name)
    {
        return key_exists($name, $this->tabs);
    }
}
