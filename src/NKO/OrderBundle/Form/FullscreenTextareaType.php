<?php
namespace NKO\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FullscreenTextareaType extends AbstractType
{
    public function getParent()
    {
        return TextareaType::class;
    }
}