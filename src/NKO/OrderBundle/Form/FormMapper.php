<?php

namespace NKO\OrderBundle\Form;

use Sonata\AdminBundle\Form\FormMapper as BaseFormMapper;

class FormMapper extends BaseFormMapper
{
    public function add($name, $type = null, array $options = [], array $fieldDescriptionOptions = [])
    {
        $admin = $this->getAdmin();
        if ($admin->getIsEmbeddedAdmin()) {
            $admin->addField($name, [
                'type' => $type,
                'options' => $options,
                'additional' => $fieldDescriptionOptions]);
            return $this;
        }

        return parent::add($name, $type, $options, $fieldDescriptionOptions);
    }

    public function with($name, array $options = [])
    {
        $admin = $this->getAdmin();
        if ($admin->getIsEmbeddedAdmin() && method_exists($admin, 'setGroup')) {
            $admin->setGroup($name);
        }

        return parent::with($name, $options);
    }
}
