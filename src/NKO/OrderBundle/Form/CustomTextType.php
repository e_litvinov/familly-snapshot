<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.08.16
 * Time: 9:26
 */

namespace NKO\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomTextType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'mapped' => false,
            'required' => false,
        ));
    }

    public function getParent()
    {
        return HiddenType::class;
    }
}