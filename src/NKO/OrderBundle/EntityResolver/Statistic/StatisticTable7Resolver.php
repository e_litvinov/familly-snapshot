<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class StatisticTable7Resolver extends DefaultStatisticResolver
{
    const LIMIT_ID = 10000;

    //id последнего открытого отчета, который должен попасть в статистику, остальные просле проплаты!

    protected $resolver;

    const GENERATOR_EXCEL = 'nko_order.statistic7_report.excel';


    const AJAX = ['ajaxLoadPeriodByReportForm'];

    public function __construct(EntityManager $em, $resolver = null)
    {
        $this->em = $em;
        $this->resolver = $resolver;
    }

    public function initForm()
    {
        $arr['reportForms'] = $this->em->getRepository(ReportForm::class)
            ->createQueryBuilder('r')
            ->innerJoin('r.competition', 'c')
            ->where('r.reportClass = :reportClass')
            ->setParameters([
                'reportClass' => Report::class,
            ])
            ->getQuery()
            ->getResult();

        $arr['lastPeriods'] = $this->em->getRepository(PeriodReport::class)->getPeriodValues(reset($arr['reportForms']));

        $arr = $this->defineAjaxHandlers(self::AJAX, $arr);

        return $arr;
    }

    public function checkRequestData(Request $request)
    {
        $request = $this->getRequest($request);

        if ((count($request) < 2)) {
            return 'Choose all variables';
        }

        return null;
    }

    public function buildDataStructure(Request $request)
    {
        $request = $this->getRequest($request);
        $reportForm = $this->em->getRepository(ReportForm::class)->find($request['reportForm']);

        $startPeriod = $this->em->getRepository(PeriodReport::class)->getFirstPeriodByReportForm($request['reportForm']);
        $finishPeriod = $this->em->getRepository(PeriodReport::class)->find($request['period']);
        $allPeriods = $this->getAllPeriods($request, $startPeriod, $finishPeriod);

        $histories = $this->getHistoriesByPeriods($reportForm->getId(), $allPeriods);

        $reports = $this->compactReportsByPstrn($histories, $finishPeriod);
        $arr['Data'] = $reports;

        return $arr;
    }

    public function getAllPeriods($request, $startPeriod, $finishPeriod)
    {
        $periodsId = [];
        $periods = $this->resolver->getPeriodsByConfines($request['reportForm'], $startPeriod, $finishPeriod);
        foreach ($periods as $period) {
            $periodsId[] = $period->getId();
        }

        return $periodsId;
    }

    public function getPeriods(Request $request)
    {
        $periods = $this->em->getRepository(PeriodReport::class)->getPeriodValues($request->get('reportForm'));
        return new JsonResponse($periods, 200);
    }

    private function compactReportsByPstrn(array $histories, PeriodReport $finishPeriod)
    {
        $reports = [];
        $periods = [];
        $i = 1;
        /** @var ReportHistory $history */
        foreach ($histories as $history) {
            if ($history->getPeriod()->getFinishDate() <= $finishPeriod->getFinishDate()) {
                /** @var Report $report */
                $report = unserialize($history->getData());
                if (!key_exists($history->getPsrn(), $reports)) {
                    $reports[$history->getPsrn()]['number'] = $i++;
                    $reports[$history->getPsrn()]['name'] = $history->getNkoName();
                    $reports[$history->getPsrn()]['period'] = $report->getPeriod()->getName();
                    $reports[$history->getPsrn()]['receiptOfDonations'] = $report->getReceiptOfDonations();
                    $reports[$history->getPsrn()]['incrementalCosts'] = $report->getIncrementalCosts();
                    $reports[$history->getPsrn()]['donationBalance'] = $report->getDonationBalance();
                    $periods[$history->getPsrn()] = $report->getPeriod()->getFinishDate();
                } elseif ($report->getPeriod()->getFinishDate() > $periods[$history->getPsrn()]) {
                    $reports[$history->getPsrn()]['period'] = $report->getPeriod()->getName();
                    $reports[$history->getPsrn()]['receiptOfDonations'] = $report->getReceiptOfDonations();
                    $reports[$history->getPsrn()]['incrementalCosts'] = $report->getIncrementalCosts();
                    $reports[$history->getPsrn()]['donationBalance'] = $report->getDonationBalance();
                    $periods[$history->getPsrn()] = $report->getPeriod()->getFinishDate();
                }
            }
        }

        return $reports;
    }
}
