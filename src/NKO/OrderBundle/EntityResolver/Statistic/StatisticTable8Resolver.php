<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater2018;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as Farvater2016;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as Harbor2019;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as KNS2017SecondStage;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report as AnalyticReportKNS2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNS2018;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReport;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNS2019;
use NKO\OrderBundle\Entity\Application as KNS2016;
use NKO\OrderBundle\Entity\Report as AnalyticReport2016;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report as AnalyticReport2017;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as Mixed2019;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport2017;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report as AnalyticReportKNS2017;

class StatisticTable8Resolver extends DefaultStatisticResolver
{
    //чтобы побороть исклчения при отрытии в дальнейшем, указывать id отчетов-исключений которые не соответствуют шаблону
    const COMPETITIONS = [
        Farvater2016::class => [
            2016 => MonitoringReport::class,
            2017 => FinanceReport2017::class,
            2018 => FinanceReport2018::class,
        ],
        Farvater2017::class => [
            2017 => FinanceReport2017::class,
            2018 => FinanceReport2018::class,
            2019 => FinanceReport2019::class,
        ],
        Farvater2018::class => [
            2018 => FinanceReport2018::class,
            2019 => FinanceReport2019::class,
        ],
        KNS2016::class => [
            2016 => AnalyticReport2016::class,
            2017 => FinanceReport2017::class,
            2018 => FinanceReport2018::class
        ],
        KNS2017::class => [
            2017 => FinanceReport2017::class,
            2018 => FinanceReport2018::class,
            2019 => FinanceReport2019::class
        ],
        KNS2017SecondStage::class => [
            2017 => AnalyticReportKNS2017::class,
            2018 => FinanceReport2018::class,
            2019 => FinanceReport2019::class
        ],
        KNS2018::class => [
            2018 => MixedReport::class,
            2019 => FinanceReport2019::class
        ],
        KNS2019::class => [
            2019 => Mixed2019::class
        ],
        Harbor2019::class => [
            2019 => FinanceReport2019::class
        ]
    ];

    const LAST_ID = 104;

    const TAMBOW_PSRN = 1026801118981;
    const PODSOLNUH_PSRN = 1097800001804;

    const ADDITIONAL = [
        self::TAMBOW_PSRN => [
            'competition' => KNS2016::class,
            'year' => [
                '2019' => MonitoringReport::class,
            ]
        ],
        self::PODSOLNUH_PSRN => [
            'competition' => Farvater2016::class,
            'year' => [
                '2019' => MonitoringReport::class,
            ]
        ]
    ];

    const START_YEAR = '-01-01 00:00:00';
    const GENERATOR_EXCEL = 'nko_order.statistic8_report.excel';

    public function buildDataStructure(Request $request)
    {
        $grantsByApplicationType = [];
        foreach (self::COMPETITIONS as $applicationType => $reportTypes) {
            $grants = $this->getBaseDataByGroups($reportTypes, $applicationType);
            $grantsByApplicationType[$applicationType] = $grants;
        }

        //получение историй заявок, сначала id, потом объекты из соображения быстроты запроса
        $historyIds = $this->getHistoryIds($grantsByApplicationType);

        $histories = $this->em->getRepository(ApplicationHistory::class)
            ->findBy(['id' => array_column($historyIds, 'id')]);
        //------------------------

        $this->addApplicationToGrant($histories, $grantsByApplicationType);

        $outputData = $this->getLineArray($grantsByApplicationType);

        return $this->changeData($this->groupByOrganization($outputData));
    }

    public function changeData($data)
    {
        $grants= [];
        foreach (self::ADDITIONAL as $psrn => $organization) {
            foreach ($organization['year'] as $year => $item) {
                $grants = array_merge($grants, $this->getGranForSpecialApplications($psrn, $organization['competition'], $item, $year));
            }
        }

        foreach ($grants as $item) {
            $psrn = $item->getApplicationHistory()->getPsrn();

            if (key_exists($psrn, $data)) {
                $target = &$data[$psrn][0];
                $target['baseSum'] += $item->getSumGrant();
            }
        }

        return $data;
    }

    public function getLineArray($grantsByApplicationType)
    {
        $outputData = [];
        foreach ($grantsByApplicationType as $item) {
            $outputData = array_merge($outputData, array_values($item));
        }

        return $outputData;
    }

    public function groupByOrganization($data)
    {
        $groups = [];
        foreach ($data as $item) {
            $groups[$item['psrn']][] = $item;
        }

        return $groups;
    }

    public function addApplicationToGrant($histories, &$grantsByApplicationType)
    {
        foreach ($histories as $history) {
            $appClass = $history->getCompetition()->getApplicationClass();
            $psrn = $history->getPsrn();

            if (!key_exists($appClass, $grantsByApplicationType)) {
                continue;
            }

            $psrns = array_column($grantsByApplicationType[$appClass], 'psrn');
            $index = array_search($psrn, $psrns);

            //если организация подала заявку на несколько волн КНС, но может выиграть только одну
            if (!empty($grantsByApplicationType[$appClass][$index]['application']) && !$history->getWinner()) {
                continue;
            }

            $grantsByApplicationType[$appClass][$index]['application'] = unserialize($history->getData());
        }
    }

    //ПОЛУЧАЕМ ID ИСТОРИЙ, т.к. если получать историю целиком, то запрос ложится по времени
    public function getHistoryIds($grants)
    {
        $query = $this->em->createQueryBuilder();

        $query
            ->select('a.id')
            ->from(ApplicationHistory::class, 'a')
            ->innerJoin('a.competition', 'c');

        $i = 0;
        $params = ['isSpread' => true, 'isSend' => true, 'isAutosaved' => 0, 'winner' => 1];
        foreach ($grants as $key => $item) {
            $i++;
            $classKey = 'appType'.$i;
            $psrnKey = 'psrn'.$i;

            $params = array_merge($params, [
                $classKey => $key,
                $psrnKey => array_column($item, 'psrn')
            ]);
            $query->orWhere('c.applicationClass IN (:'. $classKey .') AND a.psrn IN (:'.$psrnKey.')');
        }

        return $query
            ->andWhere($query->expr()->orX(
                $query->expr()->andX(
                    $query->expr()->eq('a.isSpread', ':isSpread'),
                    $query->expr()->eq('a.isSend', ':isSend')
                ),
                $query->expr()->andX(
                    $query->expr()->eq('a.isSend', ':isSend'),

                    $query->expr()->orX(
                        $query->expr()->isNull('a.isAutosaved'),
                        $query->expr()->eq('a.isAutosaved', ':isAutosaved')
                    ),
                    $query->expr()->eq('a.winner', ':winner')
                )
            ))
            ->setParameters($params)
            ->getQuery()
            ->getResult();
    }

    public function getBaseDataByGroups($reportTypes, $applicationType)
    {
        $query = $this->em
            ->createQueryBuilder()
            ->select('SUM(g.sumGrant) AS baseSum, a.psrn AS psrn, MAX(rf.year) as maxYear, MIN(rf.year) as minYear')
            ->from(GrantConfig::class, 'g')
            ->innerJoin('g.applicationHistory', 'a')
            ->innerJoin('a.competition', 'c')
            ->innerJoin('g.reportForm', 'rf');

        //формирование условий
        $i = 0;
        $params = ['applicationClass' => $applicationType];
        foreach ($reportTypes as $key => $item) {
            $i++;
            $classKey = 'reportClass'.$i;
            $yearKey = 'year'.$i;
            $params = array_merge($params, [
                $classKey => $item,
                $yearKey => new \DateTime($key.self::START_YEAR)
            ]);
            $query->orWhere('rf.reportClass=:'. $classKey .' AND rf.year = :'.$yearKey. ' AND rf.id <= :lastReportFormId');
        }
        //--------------------

        $params = array_merge($params, [
            'lastReportFormId' => self::LAST_ID
        ]);

        return $query
            ->andWhere('g.sumGrant IS NOT NULL')
            ->andWhere('c.applicationClass =:applicationClass')
            ->setParameters($params)
            ->groupBy('psrn')
            ->getQuery()
            ->getResult();
    }

    public function getGranForSpecialApplications($psrn, $applicationClass, $reportClass, $year)
    {
        return $this->em
            ->createQueryBuilder()
            ->select('g')
            ->from(GrantConfig::class, 'g')
            ->innerJoin('g.applicationHistory', 'a')
            ->innerJoin('a.competition', 'c')
            ->innerJoin('g.reportForm', 'rf')
            ->where('a.psrn =:psrn')
            ->andWhere('c.applicationClass =:applicationClass')
            ->andWhere('rf.reportClass =:reportClass')
            ->andWhere('rf.year =:year')
            ->setParameters([
                'applicationClass' => $applicationClass,
                'psrn' => $psrn,
                'reportClass' => $reportClass,
                'year' => new \DateTime($year.'-01-01')
            ])
            ->getQuery()
            ->getResult();
    }
}
