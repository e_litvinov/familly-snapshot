<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use Doctrine\ORM\EntityManager;
use phpDocumentor\Reflection\Types\Static_;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\ReportHistory;

class DefaultStatisticResolver implements StatisticResolverInterface
{
    protected $data = [];
    protected $resolver;
    protected $em;
    const LIMIT_ID = 100;

    public function __construct(EntityManager $em, $resolver = null)
    {
        $this->em = $em;
        $this->resolver = $resolver;
    }

    public function initForm()
    {
        return [];
    }

    public function checkRequestData(Request $request)
    {
        return null;
    }

    public function buildDataStructure(Request $request)
    {
        return [];
    }

    public function getDataFromAdmin(AbstractAdmin $admin)
    {
        $reflection =  (new \ReflectionClass($admin));
        $this->data['dataFromAdmin'] = $reflection->getConstants();
    }

    public function addServiceData($data)
    {
        if ($this->keyExists('dataFromAdmin', $this->data)) {
            $data['reportNumber'] = $this->keyExists('TABLE_NUMBER', $this->data['dataFromAdmin']);
            $data['reportName'] = $this->keyExists('REPORT_NAME', $this->data['dataFromAdmin']);
            $data['dataFromAdmin'] = $this->data['dataFromAdmin'];
        }

        return $data;
    }

    public function getRequest(Request $request)
    {
        return $request->request->all() ?: [];
    }

    public function getExcelService()
    {
        $reflection = (new \ReflectionClass(static::class));
        return $reflection->hasConstant('GENERATOR_EXCEL') ?  $reflection->getConstant('GENERATOR_EXCEL') : null;
    }

    public function defineAjaxHandlers($ajax, array $data)
    {
        $data['ajax'] = $ajax;
        return $data;
    }

    public function keyExists($key, array $arr)
    {
        return key_exists($key, $arr) ? $arr[$key] : null;
    }


    /**
     * @param $reportForms array|int
     * @param $periods array
     *
     * @return array
     */
    public function getHistoriesByPeriods($reportForms, $periods)
    {
        $arr = $this->em
            ->createQueryBuilder()
            ->select('r.id')
            ->from(ReportHistory::class, 'r')
            ->where('r.reportForm in (:reportForm)')
            ->andWhere('r.isSend = :isSend')
            ->andWhere('r.period in (:periods)')
            ->andWhere('r.reportForm < :limitId')
            ->setParameters([
                'reportForm' => $reportForms,
                'isSend' => true,
                'periods' => $periods,
                'limitId' => static::LIMIT_ID,
            ])
            ->getQuery()
            ->getResult();

        if (!$arr) {
            return [];
        }

        return $this->getHistoriesByIds($arr);
    }

    /**
     * @param $reportForms array|int
     *
     * @return array
     */
    public function getHistories($reportForms)
    {
        $arr = $this->em
            ->createQueryBuilder()
            ->select('r.id')
            ->from(ReportHistory::class, 'r')
            ->where('r.reportForm in (:reportForm)')
            ->andWhere('r.isSend = :isSend')
            ->andWhere('r.reportForm < :limitId')
            ->setParameters([
                'reportForm' => $reportForms,
                'isSend' => true,
                'limitId' => static::LIMIT_ID,
            ])
            ->getQuery()
            ->getResult();

        if (!$arr) {
            return [];
        }

        return $this->getHistoriesByIds($arr);
    }

    private function getHistoriesByIds(array $arr)
    {
        return $this->em->createQueryBuilder()
            ->select('r')
            ->from(ReportHistory::class, 'r')
            ->where('r.id IN (:ids)')
            ->setParameters(['ids' => array_merge_recursive(...$arr)['id']])
            ->getQuery()
            ->getResult();
    }
}
