<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\OrderBundle\Entity\Farvater2017\Application as FarvaterApplication2017;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as BriefApplication2016;

class StatisticTable5Resolver extends DefaultStatisticResolver
{
    const GENERATOR_EXCEL = 'nko_order.statistic3_report.excel';
    const QUERY = 'practiceAnalyticResults';

    public function initForm()
    {
        $reportForms = $this->em->getRepository(ReportForm::class)
            ->createQueryBuilder('r')
            ->innerJoin('r.competition', 'c')
            ->where('r.reportClass = :reportClass')
            ->andWhere('c.applicationClass IN (:arr)')
            ->setParameters([
                'reportClass' => Report::class,
                'arr' => [FarvaterApplication2017::class, BriefApplication2016::class]
            ])
            ->getQuery()
            ->getResult();

        return ['reportForms' => $reportForms];
    }

    public function buildDataStructure(Request $request)
    {
        $reportForm = $this->getRequest($request)['reportForm'];
        $histories = $this->getHistories($reportForm);

        $generator = $this->fillStructure($histories);
        foreach ($generator as $item) {
            $arr[0][] = $item;
        }
        $arr['reportNumber'] = $this->keyExists('TABLE_NUMBER', $this->data['dataFromAdmin']);

        return $arr;
    }

    private function fillStructure($histories)
    {
        foreach ($histories as $history) {
            $report = unserialize($history->getData());
            $feedback = $this->getFeedbackData($report->getPracticeFeedbackAttachments());

            $author = $this->em->getRepository(NKOUser::class)->find($report->getAuthor());
            $baseData['author'] = $author->getNkoName();
            $baseData['projectName'] = $report->getProjectName();

            $results = $this->getResultsData($report->getPracticeAnalyticResults());
            foreach ($results as $item) {
                $arr = array_merge($baseData, $item);

                $curFeedback = array_filter($feedback, function ($result) use ($item) {
                    return $result['id'] === $item['id'];
                });

                if (count($curFeedback) > 1) {
                    foreach ($curFeedback as $value) {
                        yield array_merge($arr, $this->addFeedbackFields($item, $value));
                    }
                } else {
                    yield array_merge($arr, $this->addFeedbackFields($item, reset($curFeedback)));
                }
            }
        }
    }

    private function addFeedbackFields($item, $value)
    {
        $item['feedbackComment'] = isset($value['feedbackComment']) ? $value['feedbackComment'] : '-';
        $item['solution'] = isset($value['feedbackComment']) ? $value['solution'] : '-';

        return $item;
    }

    private function getResultsData($results)
    {
        $data = [];
        foreach ($results as $key => $result) {
            $data[$key]['id'] = $result->getId();
            $data[$key]['service'] = $result->getService();
            $data[$key]['isFeedback'] = $result->getIsFeedback();
            $data[$key]['comment'] = $result->getComment();
        }

        return $data;
    }

    private function getFeedbackData($feedback)
    {
        $data = [];
        foreach ($feedback as $key => $item) {
            $data[$key]['id'] = $item->getPractice() ? $item->getPractice()->getId() : null;
            $data[$key]['feedbackComment'] = $item->getComment();
            $data[$key]['solution'] = $item->getAdministrativeSolution();
        }

        return $data;
    }
}
