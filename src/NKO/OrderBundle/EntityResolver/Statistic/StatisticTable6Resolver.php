<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 31.01.19
 * Time: 11:23
 */

namespace NKO\OrderBundle\EntityResolver\Statistic;

use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as SF2016Application;
use NKO\OrderBundle\Entity\Farvater2017\Application as SF2017Application;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as SF2018Application;
use NKO\OrderBundle\Entity\Application as KNS2016Application;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017_1Application;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as KNS2017_2Application;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as Harbor2019Application;
use NKO\OrderBundle\Entity\Application\Harbor\Application2020\Application as Harbor2020Application;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNS2018Application;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNS2019Application;

class StatisticTable6Resolver extends DefaultStatisticResolver
{
    const COMPETITIONS = [
        'CФ-2016' => [
            'applications' => [SF2016Application::class],
            'conditions' => [
                'except' => [99],
                'additional' => [
                    70 => [2016, 2018]
                ],
            ],
        ],
        'CФ-2017' => [
            'applications' => [SF2017Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [],
            ],
        ],
        'CФ-2018' => [
            'applications' => [SF2018Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [],
            ],
        ],
        'КНС 2016' => [
            'applications' => [KNS2016Application::class],
            'conditions' => [
                'except' => [70],
                'additional' => [],
            ],
        ],
        'КНС 2017' => [
            'applications' => [KNS2017_1Application::class, KNS2017_2Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [
                    70 => [2019]
                ],
            ],
        ],
        'КНС 2018' => [
            'applications' => [KNS2018Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [],g
            ],
        ],
        'КНС 2019' => [
            'applications' => [KNS2019Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [],
            ],
        ],
        'СГ-2019' => [
            'applications' => [Harbor2019Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [
                    99 => [2019]
                ],
            ],
        ],
        'СГ-2020' => [
            'applications' => [Harbor2020Application::class],
            'conditions' => [
                'except' => [],
                'additional' => [
                    99 => [2020]
                ],
            ],
        ],
    ];

    const YEARS = [2016 => 2016, 2017 => 2017, 2018 => 2018, 2019 => 2019, 2020 => 2020];
    const GENERATOR_EXCEL = 'nko_order.statistic6_report.excel';
    const INDICATOR_CODE = 'practice_implementation';
    const INDICATOR_TYPE = 'social';

    private $yearFrom;
    private $yearTo;

    public function initForm()
    {
        $arr['yearPeriod'] = self::YEARS;

        return $arr;
    }

    public function buildDataStructure(Request $request)
    {
        $arr = [];
        foreach (self::COMPETITIONS as $competition) {
            $reportFormIdsPacked = $this->em->getRepository(ReportForm::class)->getByYearAndCompetition($this->yearFrom, $this->yearTo, $competition['applications']);
            $reportFormIds = $this->unpackNestedIdField($reportFormIdsPacked);
            $this->fixReportFormIds($reportFormIds, $competition['conditions']['additional'], $competition['conditions']['except'], $this->yearFrom, $this->yearTo);
            $histories = $this->getHistories($reportFormIds);

            $arr[] = $this->getStructuredData($histories);
        }

        $response['Data'] = $arr;
        $response['YearPeriod'] = [
            'from' => $this->yearFrom,
            'to' => $this->yearTo
        ];

        return $response;
    }

    public function checkRequestData(Request $request)
    {
        $this->yearFrom = $this->getRequest($request)['yearReportFrom'];
        $this->yearTo = $this->getRequest($request)['yearReportTo'];

        if ($this->yearTo < $this->yearFrom) {
            return "Начало отсчёто не должно быть больше, чем окончание";
        }

        return null;
    }

    private function unpackNestedIdField($arr)
    {
        $result = [];

        foreach ($arr as $element) {
            $result[] =  $element['id'];
        }

        return $result;
    }


    private function getStructuredData($histories)
    {
        $results = $this->getPeriodResults($histories);
        $returnedData = [];

        if (!$results) {
            return [];
        }

        foreach ($results as $result) {
            $sumValuesPlan = 0;
            $sumValuesFact = 0;
            foreach ($result as $periodResult) {
                /** @var $periodResult PeriodResult */
                $sumValuesFact += $periodResult->getNewAmount();
                $sumValuesPlan += $periodResult->getTotalAmount();
            }

            $returnedData[] = [
                'fact' => $sumValuesFact,
                'plan' => $sumValuesPlan
            ];
        }

        return $returnedData;
    }

    private function getPeriodResults($histories)
    {
        if (!$histories) {
            return null;
        }

        $results = [];

        foreach ($histories as $history) {
            /** @var MonitoringReport $report */
            $report = unserialize($history->getData());

            foreach ($report->getMonitoringResults() as $key => $result) {
                if ($this->isNeededResult($result)) {
                    // getFinalPeriodResult return [[periodResult]]
                    $finalResultInArrayCollection = $result->getFinalPeriodResult();
                    $finalResultInArray = reset($finalResultInArrayCollection);
                    $results[$key][] = reset($finalResultInArray);
                }
            }
        }

        return $results;
    }

    // Проверяет результат на принадлежность к нужному индикатору
    private function isNeededResult(MonitoringResult $result)
    {
        $indicator = $result->getIndicator();

        $parent = $indicator->getParent();
        $grandparent = $parent ? $parent->getParent() : null;

        //Если предок или пропредок является "РЕАЛИЗАЦИЯ ПРАКТИКИ" и являются ли социальными
        return $indicator->getType() == self::INDICATOR_TYPE
            && ($this->isPracticeImplementationIndicator($grandparent) || $this->isPracticeImplementationIndicator($parent));
    }

    private function isPracticeImplementationIndicator(Indicator $indicator = null)
    {
        // Является ли индикатор "РЕАЛИЗАЦИЯ ПРАКТИКИ"
        return $indicator && $indicator->getCode() == self::INDICATOR_CODE;
    }

    private function fixReportFormIds(array &$reportFormIds, array $additional, array $except, $yearFrom, $yearTo)
    {
        foreach ($except as $item) {
            if (false == $index = array_search($item, $reportFormIds)) {
                unset($reportFormIds[$index]);
            }
        }

        foreach ($additional as $reportFormId => $years) {
            foreach ($years as $year) {
                if ($year >= (int) $yearFrom && $year <= (int) $yearTo) {
                    $reportFormIds[] = $reportFormId;
                    continue 2;
                }
            }
        }
    }
}
