<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;

class StatisticTable3Resolver extends DefaultStatisticResolver
{
    const GENERATOR_EXCEL = 'nko_order.statistic3_report.excel';

    const QUERY = [
        3 => [
            ['expectedAnalyticResults', 'practiceAnalyticIndividualResults'],
            ['practiceAnalyticResults', 'expectedAnalyticIndividualResults'],
        ],
        4 => [
            'practiceFeedbackAttachments',
            'practiceSpreadFeedbackAttachments'
        ]
    ];

    public function initForm()
    {
        $reportForms = $this->em->getRepository(ReportForm::class)->findBy([
            'reportClass' => Report::class
        ]);

        return ['reportForms' => $reportForms];
    }

    public function buildDataStructure(Request $request)
    {
        $request = $this->getRequest($request);
        $reportNumber = $this->keyExists('TABLE_NUMBER', $this->data['dataFromAdmin']);

        foreach (self::QUERY[$reportNumber] as $item) {
            $data = [];
            if (is_array($item)) {
                foreach ($item as $table) {
                    $data[] = $this->getData($request, $table, $reportNumber);
                }
                $arr[] = $this->sortData($data);
            } else {
                $arr[] = $this->getData($request, $item, $reportNumber);
            }
        }

        $arr['reportNumber'] = $reportNumber;

        return $arr;
    }

    public function getData($request, $item, $number)
    {
        return $this->em->getRepository(Report::class)->findDataForAnalytics($request['reportForm'], $item, $number);
    }

    private function sortData($arr)
    {
        $mainArr = [];
        $currentAuthor = reset($arr)[0]['authorId'];

        foreach (reset($arr) as $item) {
            if ($item['authorId'] !== $currentAuthor) {
                foreach (end($arr) as $value) {
                    if ($value['authorId'] === $currentAuthor) {
                        $mainArr[] = $value;
                    }
                }

                $currentAuthor = $item['authorId'];
            }
            $mainArr[] = $item;
        }

        return $mainArr;
    }
}
