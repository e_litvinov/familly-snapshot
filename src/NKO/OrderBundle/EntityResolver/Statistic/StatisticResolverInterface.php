<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use Symfony\Component\HttpFoundation\Request;

interface StatisticResolverInterface
{
    public function initForm();

    public function checkRequestData(Request $request);

    public function buildDataStructure(Request $request);
}
