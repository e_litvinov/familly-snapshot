<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;

class StatisticTable2Resolver extends DefaultStatisticResolver
{
    const GENERATOR_EXCEL = 'nko_order.statistic2_report.excel';
    const EXCEPT = [70, 99];
    const LABEL = 'Итого';
    const LIMIT_ID = 103;

    /**
     * union - from histories or from
     * Тут важен порядок.
    */
    const ORDER = [
        'СФ-2016' => [
            'union' => false,
            'reportForms' => [
                2016 => [7, 70],
                2017 => [8],
                2018 => [9, 70],
            ],
            'results' => [],
        ],
        'СФ-2016 ИТОГО' => [
            'union' => true,
            'label' => self::LABEL,
            'from' => ['СФ-2016'],
            'results' => [],
        ],
        'СФ-2017' => [
            'union' => false,
            'reportForms' => [
                2017 => [10],
                2018 => [37],
                2019 => [59],
            ],
            'results' => [],
        ],
        'СФ-2017 ИТОГО' => [
            'union' => true,
            'label' => self::LABEL,
            'from' => ['СФ-2017'],
            'results' => [],
        ],
        'СФ-2018' => [
            'union' => false,
            'reportForms' => [
                2018 => [41],
                2019 => [62],
            ],
            'results' => [],
        ],
        'СФ-2018 ИТОГО' => [
            'union' => true,
            'label' => self::LABEL,
            'from' => ['СФ-2018'],
            'results' => [],
        ],
        'КНС-2016' => [
            'union' => false,
            'reportForms' => [
                2017 => [11],
                2018 => [12],
            ],
            'results' => [],
        ],
        'КНС-2016 ИТОГО' => [
            'union' => true,
            'label' => self::LABEL,
            'from' => ['КНС-2016'],
            'results' => [],
        ],
        'КНС-2017' => [
            'union' => false,
            'reportForms' => [
                2018 => [42],
                2019 => [60, 70],
            ],
            'results' => [],
        ],
        'КНС-2017 ИТОГО' => [
            'union' => true,
            'label' => self::LABEL,
            'from' => ['КНС-2017'],
            'results' => [],
        ],
        'КНС-2017-2' => [
            'union' => false,
            'reportForms' => [
                2018 => [47],
                2019 => [61],
            ],
            'results' => [],
        ],
        'КНС-2017-2 ИТОГО' => [
            'union' => true,
            'label' => self::LABEL,
            'from' => ['КНС-2017-2'],
            'results' => [],
        ],
        'КНС-2019-1' => [
            'union' => false,
            'reportForms' => [
                2019 => [83],
            ],
            'results' => [],
        ],
        'КНС-2019-2' => [
            'union' => false,
            'reportForms' => [
                2019 => [84],
            ],
            'results' => [],
        ],
        'КНС-2019-3' => [
            'union' => false,
            'reportForms' => [
                2019 => [85],
            ],
            'results' => [],
        ],
        'КНС-2019-4' => [
            'union' => false,
            'reportForms' => [
                2019 => [86],
            ],
            'results' => [],
        ],
        'КНС-2019-5' => [
            'union' => false,
            'reportForms' => [
                2019 => [87],
            ],
            'results' => [],
        ],
        'КНС-2019-6' => [
            'union' => false,
            'reportForms' => [
                2019 => [88],
            ],
            'results' => [],
        ],
        'СГ-2019' => [
            'union' => false,
            'reportForms' => [
                2019 => [89, 99],
            ],
            'results' => [],
        ],
        'Итого СФ' => [
            'union' => true,
            'label' => 'Итого СФ',
            'from' => ['СФ-2016 ИТОГО', 'СФ-2017 ИТОГО', 'СФ-2018 ИТОГО'],
            'results' => [],
        ],
        'Итого КНС' => [
            'union' => true,
            'label' => 'Итого КНС',
            'from' => ['КНС-2016 ИТОГО', 'КНС-2017 ИТОГО', 'КНС-2017-2 ИТОГО', 'КНС-2019-1', 'КНС-2019-2', 'КНС-2019-3', 'КНС-2019-4', 'КНС-2019-5', 'КНС-2019-6'],
            'results' => [],
        ],
        'ВСЕГО' => [
            'union' => true,
            'label' => 'ВСЕГО',
            'from' => ['Итого КНС', 'Итого СФ', 'СГ-2019'],
            'results' => [],
        ],
    ];

    /** Тут важен порядок. */
    const INDICATORS_ID = [65, 69, 72, 75, 78];

    public function buildDataStructure(Request $request)
    {
        $arr['competitions'] = $this->em->getRepository(ReportForm::class)
            //Проверить self::EXCEPT нужно ли
            ->getCompetitionsWithReportByReportClass(Report::class, self::EXCEPT, self::LIMIT_ID);

        $histories = $this->getHistories(array_column($arr['competitions'], 'reportFormId'));
        $arr['results'] = $this->compactHistories($histories);

        return $arr;
    }

    protected function compactHistories(array $histories)
    {
        $structure = self::ORDER;
        foreach ($structure as &$element) {
            if ($element['union']) {
                $this->calculateUnion($element, $structure);
            } else {
                $this->calculateReportForms($element, $histories);
            }
        }

        return $structure;
    }

    protected function calculateUnion(array &$element, array $structure)
    {
        $data = $this->initDataArray();

        foreach ($element['from'] as $key) {
            $data = $this->sumResults($data, $structure[$key]);
        }

        $element['results'][] = $data;
    }

    protected function calculateReportForms(array &$element, array $histories)
    {

        foreach ($element['reportForms'] as $year => $ids) {
            $element['results'][$year] = $this->getCompactValues($ids, $histories);
        }
    }

    protected function getCompactValues(array $ids, array $histories)
    {
        $histories = array_filter($histories, function (ReportHistory $history) use ($ids) {
            return in_array($history->getReportForm()->getId(), $ids);
        });

        $rawData = $this->getRawData($histories);

        return $this->compactRawData($rawData);
    }

    protected function getRawData(array $histories)
    {
        $data = [];
        foreach ($histories as $history) {
            /** @var Report $report */
            $report = unserialize($history->getData());

            $monitoringResults = $report->getMonitoringResults()->filter(function (MonitoringResult $result) {
                return in_array($result->getIndicator()->getId(), self::INDICATORS_ID);
            });

            $data[] = $this->getPeriodResults($monitoringResults);
        }

        return $data;
    }

    protected function getPeriodResults(\Doctrine\Common\Collections\Collection $monitoringResults)
    {
        $periodResults = [];
        foreach ($monitoringResults as $monitoringResult) {
            /** @var $monitoringResult MonitoringResult */
            $periodResults[$monitoringResult->getIndicator()->getId()] = $monitoringResult->getPeriodResults()->filter(function (PeriodResult $result) {
                return $result->isFinalResult();
            })->first();
        }

        return $periodResults;
    }

    protected function compactRawData(array $rawData)
    {
        $data = $this->initDataArray();

        foreach ($rawData as $item) {
            foreach ($item as $id => $result) {
                $data[$id]['fact'] += $result->getNewAmount();
                $data[$id]['plan'] += $result->getTotalAmount();
            }
        }

        return $data;
    }

    protected function initDataArray()
    {
        $data = [];
        foreach (self::INDICATORS_ID as $id) {
            $data[$id]['fact'] = 0;             //newAmount
            $data[$id]['plan'] = 0;             //totalAmount
        }

        return $data;
    }

    private function sumResults(array $data, array $element)
    {
        foreach ($element['results'] as $result) {
            foreach ($result as $id => $value) {
                $data[$id]['fact'] += $value['fact'];
                $data[$id]['plan'] += $value['plan'];
            }
        }

        return $data;
    }
}
