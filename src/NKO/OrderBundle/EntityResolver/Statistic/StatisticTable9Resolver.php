<?php

namespace NKO\OrderBundle\EntityResolver\Statistic;

use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as MonitoringReportHarbor2020;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\PeriodReport;

class StatisticTable9Resolver extends DefaultStatisticResolver
{
    const GENERATOR_EXCEL = 'nko_order.statistic9_report.excel';
    const AJAX = ['ajaxLoadReportFormByYear', 'ajaxLoadPeriodsByReportForm'];
    const LIMIT_ID = 1000;

    public function initForm()
    {
        $years = $this->em->getRepository(ReportForm::class)->getYearsByReportClass(MonitoringReportHarbor2020::class);

        $arr['reportForms'] = $this->em->getRepository(ReportForm::class)->findBy([
            'year' => reset($years)['year'],
            'reportClass' => MonitoringReportHarbor2020::class
        ]);
        $arr['periods'] = $this->em->getRepository(PeriodReport::class)->getPeriodValues(reset($arr['reportForms']));

        if ($years) {
            foreach ($years as $year) {
                $arr['years'][$year['year']->format('Y')] = $year['year']->format('Y-m-d');
            }
        }

        $arr = $this->defineAjaxHandlers(self::AJAX, $arr);

        return $arr;
    }

    public function checkRequestData(Request $request)
    {
        $request = $this->getRequest($request);

        if ((count($request) < 3)) {
            return 'Choose all variables';
        }

        $this->data['confines'] = $this->resolver->getConfines($request['period_0'], $request['period_1']);

        if (!$this->resolver->checkCorrectConfines(...$this->data['confines'])) {
            return 'Start date greater than finish date';
        }

        return null;
    }

    public function buildDataStructure(Request $request)
    {
        $request = $this->getRequest($request);
        $allPeriods = $this->getAllPeriods($request);
        $reportIds = [];
        $results = [];

        $reportHistories = $this->getHistories($request['reportForm']);
        foreach ($reportHistories as $history) {
            $report = unserialize($history->getData());
            $reportIds[] = $report->getId();
            $author = $this->em->getRepository(NKOUser::class)->find($report->getAuthor()->getId());
            $default = [
                'isSend' => $history->getIsSend(),
                'report' => $report->getId(),
                'author' => $author->getNkoName()
            ];

            $monitoringResults = $report->getMonitoringResults();

            foreach ($monitoringResults as $res) {
                if (!$res->getIndicator()->getParent()) {
                    continue;
                }

                $item = [];
                $item['name'] = $res->getIndicator()->getTitle();
                $item['type'] = $res->getIndicator()->getType();

                //получаем Code
                $parentInd = $res->getIndicator()->getParent();
                $code = $parentInd->getCode();
                if ($parentInd->getParent()) {
                    $code = $parentInd->getParent()->getCode() ?: $code;
                }
                $item['code'] = $code;

                //необходимые периоды с результатами по диапазону (включая isFinal)
                $periodResults = $res->getPeriodResults();
                $necessaryResults = $periodResults->filter(function ($periodRes) use ($allPeriods) {
                    if ($periodRes->isFinalResult()) {
                        return true;
                    }
                    return in_array($periodRes->getPeriod()->getId(), $allPeriods);
                });

                //суммирование значений
                $item['fact'] = null;
                foreach ($necessaryResults as $periodRes) {
                    if ($periodRes->isFinalResult()) {
                        $item['plan'] = $periodRes->getTotalAmount();
                        continue;
                    }
                    $item['fact'] += $periodRes->getNewAmount();
                }

                $results[] = array_merge($item, $default);
            }
        }

        //обработка полученны резуьтатов и построение структуры для excel
        foreach ($this->getConditionsForStructuring() as $condition) {
            $section = [];
            foreach ($reportIds as $report) {
                $section[] = array_filter($results, function ($result) use ($report, $condition) {
                    return $report === $result['report'] && $condition($result);
                });
            }
            $structuredData[] = $section;
        }

        list($start, $finish) = $this->data['confines'];
        $reportForm = $this->em->getRepository(ReportForm::class)->find($request['reportForm']);
        $structuredData[] = compact("start", "finish", "reportForm");

        return $structuredData;
    }

    private function getConditionsForStructuring()
    {
        return [
            function ($arr) {
                return $arr['code'] ==='practice_implementation' && ($arr['type'] ==='social' || $arr['type'] === null);
            },
            function ($arr) {
                return $arr['code'] ==='practice_implementation' && $arr['type'] ==='prod';
            },
            function ($arr) {
                return $arr['code'] ==='practice_dissemination';
            },
            function ($arr) {
                return $arr['code'] ==='total';
            },
            function ($arr) {
                return $arr['code'] ==='individual_results';
            }
        ];
    }

    //получаем ID периодов, далее используются в фильтре
    public function getAllPeriods($request)
    {
        $periodsId = [];
        $periods = $this->resolver->getPeriodsByConfines($request['reportForm'], ...$this->data['confines']);
        foreach ($periods as $period) {
            $periodsId[] = $period->getId();
        }

        return $periodsId;
    }

    //AJAX
    public function getPeriods(Request $request)
    {
        $periods = $this->em->getRepository(PeriodReport::class)->getPeriodValues($request->get('reportForm'));
        return new JsonResponse($periods, 200);
    }

    public function getReportForm(Request $request)
    {
        $reportForms = $this->em->getRepository(ReportForm::class)->getReportFormByYear(
            new \DateTime($request->get('year')),
            MonitoringReportHarbor2020::class
        );

        return new JsonResponse($reportForms, 200);
    }
}
