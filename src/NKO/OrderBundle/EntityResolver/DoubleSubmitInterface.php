<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 1.10.18
 * Time: 13.59
 */

namespace NKO\OrderBundle\EntityResolver;

interface DoubleSubmitInterface
{
    /**
     * @return bool
     */
    public function isDoubleSubmit();
}
