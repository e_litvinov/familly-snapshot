<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;

class MonitoringHarbor2020ReportResolver extends DefaultReportResolver
{
    const INDIVIDUAL_RESULT_COUNT = 10;
    const INDIVIDUAL_RES_COUNT = 14;
    /** @var $this->em EntityManager */
    private $em;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function initEdit($object, $request, $params = [])
    {
        return parent::initEdit($object, $request, $params);
    }

    public function createAssociations(Report $object)
    {
        $reportTemplate = $object->getReportTemplate();

        $reportForm = $object->getReportForm();
        $templateResults = !$reportForm->getIsSelectable() ? $reportTemplate->getMonitoringResults() : false;
        $periods = $reportForm->getReportPeriods();
        $indicators = $this->em->getRepository(Indicator::class)->findAllExceptCodeAndMonitoringType(IndicatorInterface::CUSTOM_VALUE, IndicatorInterface::NEWMONITORINGINDICATORTYPE);

        foreach ($indicators as $indicator) {
            $monitoringResult = $this->createMonitoringResult($indicator, $object, $periods);

            if ($templateResults) {
                $result = $templateResults->filter(function ($object) use ($indicator) {
                    return $object->getIndicator()->getId() == $indicator->getId();
                });

                if (!$result->isEmpty()) {
                    $periodResults = $result->first()->getPeriodResults();

                    foreach ($periodResults as $result) {
                        $periodResult = clone $result;
                        $monitoringResult->addPeriodResult($periodResult);
                    }
                } elseif ($indicator->getParent()) {
                    $this->createPeriodResult($monitoringResult, true, null);
                }
            } else {
                $this->createPeriodResult($monitoringResult, true, null);
            }

            if ($indicator->getCode() == IndicatorInterface::PRE_CUSTOM_VALUE) {
                $index = $indicator->getIndexNumber();
                $customIndicator = $this->createCustomIndicator($indicator->getParent(), ++$index);
                $customMonitoringResult = $this->createMonitoringResult($customIndicator, $object, $periods);
                $this->createPeriodResult($customMonitoringResult, true, null);
            }

            if ($indicator->getCode() == IndicatorInterface::INDIVIDUAL_RES_CODE) {
                $index = self::INDIVIDUAL_RES_COUNT;
                for ($i = 0; $i < self::INDIVIDUAL_RESULT_COUNT; $i++) {
                    $customIndicator = $this->createCustomIndicator($indicator, $i + 1);
                    $customMonitoringResult = $this->createMonitoringResult($customIndicator, $object, $periods);
                    $this->createPeriodResult($customMonitoringResult, true, null);
                }
            }
        }

        $document = new MonitoringDocument();
        $object->addDocument($document);
    }

    private function createMonitoringResult($indicator, $object, $periods)
    {
        $monitoringResult = new MonitoringResult();
        $monitoringResult->setIndicator($indicator);
        $object->addMonitoringResult($monitoringResult);
        if ($indicator->getParent()) {
            foreach ($periods as $period) {
                $this->createPeriodResult($monitoringResult, false, $period);
            }
        }

        return $monitoringResult;
    }

    private function createPeriodResult($monitoringResult, $isFinalResult, $period = null)
    {
        $periodResult = new PeriodResult();
        $periodResult->setPeriod($period);
        $periodResult->setIsFinalResult($isFinalResult);
        $monitoringResult->addPeriodResult($periodResult);
    }

    private function createCustomIndicator($parent, $index)
    {
        $indicator = new Indicator();
        $indicator->setCode(IndicatorInterface::CUSTOM_VALUE);
        $indicator->setMonitoringType(IndicatorInterface::NEWMONITORINGINDICATORTYPE);
        $indicator->setParent($parent);
        $indicator->setIndexNumber($index);
        return $indicator;
    }
}
