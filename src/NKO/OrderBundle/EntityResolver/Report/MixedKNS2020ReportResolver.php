<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Document;
use NKO\OrderBundle\Entity\Report\MixedReport\Attachment;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Result;
use NKO\OrderBundle\Entity\Report\MixedReport\ResultType;
use NKO\OrderBundle\Entity\TrainingEvent;
use NKO\OrderBundle\Resolver\FinanceResolver;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTraitNotifications;
use NKO\OrderBundle\Utils\ReportTypes;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MixedKNS2020ReportResolver extends DefaultReportResolver
{
    private $em;
    private $financeResolver;

    public function __construct(ContainerInterface $container, EntityManager $em, FinanceResolver $financeResolver)
    {
        parent::__construct($container);
        $this->em = $em;
        $this->financeResolver = $financeResolver;
    }

    public function createAssociations(Report $object)
    {
        $object->addEvent(new TrainingEvent());
        $object->addMixedBeneficiariesResult(new Result());
        $object->addMixedEmployeeResult(new Result());
        $object->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
        $object->addDocument(new Document());
        $object->addAnalyticAttachment(new Attachment());
        $this->setResultTypes($object);
        $this->financeResolver->create($object);
    }


    private function setResultTypes($object)
    {
        $mixedResultIndex = 1;

        $resultTypes = $this->em->getRepository(ResultType::class)
            ->createQueryBuilder('r')
            ->where('r.type like :type')
            ->setParameters([
                'type' => '%' . ReportTypes::MIXED_KNS_REPORT . '%'
            ])
            ->getQuery()
            ->getResult();

        foreach ($resultTypes as $resultType) {
            $mixedResult = new Result();
            $mixedResult->setCustomIndex($mixedResultIndex++);
            $mixedResult->setLinkedTypes($resultType);
            $object->addMixedResult($mixedResult);
        }
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        $currentDate = new \DateTime();
        $updatedAt = $object->getUpdatedAt() ? $object->getUpdatedAt() : $currentDate;
        $updatedAt = $updatedAt->format('d.m.Y H:i');

        $formView->children['updatedAt']->vars['value'] = $updatedAt;
    }

    public function skipUpdate($object = null, $form = null, $params = null)
    {
        if (key_exists('request', $params) &&
            $params['request']->get('completeEdit') != "completeEditionButton" &&
            $params['request']->get('autosaver') != "true"
        ) {
            $costOverruns = $this->checkCostOverruns($object);
            if ($costOverruns) {
                $this->flashBag->add($costOverruns['type'], $costOverruns['message']);
            }

            $donationBalance = $this->checkDonationBalance($object);
            if ($donationBalance) {
                $this->flashBag->add($donationBalance['type'], $donationBalance['message']);
            }
        }
    }

    /**
     * Function for checking cost overruns by main expense and receiving a notification
     *
     * @param $object
     *
     * @return string[]|null
     */
    public function checkCostOverruns($object)
    {
        if ($object != null) {
            $financeSpent = $object->getFinanceSpent();

            for ($i = 0; $i < $financeSpentCount = count($financeSpent); $i++) {
                if ($financeSpent[$i]->getPeriodCosts() != 0 &&
                    $financeSpent[$i]->getApprovedSum() * 1.1 <= $financeSpent[$i]->getPeriodCosts()
                ) {
                    return [
                        'type' => 'sonata_flash_info',
                        'message' => 'В связи с наличием ситуации превышения суммы расходов 
                    по статье по всему отчетному периоду к плановым более, чем на 10%, 
                    необходимо проверить корректность заполнения отчета или обратиться в 
                    Фонд для получения информации о перераспределении статей',
                    ];
                }
            }
        }

        return null;
    }

    /**
     * Function for checking donation balance and receiving notification about residue
     *
     * @param $object
     *
     * @return string[]|null
     */
    public function checkDonationBalance($object)
    {
        if ($object != null && $object->getIncrementalCosts() > 0) {
            return [
                'type' => 'sonata_flash_info',
                'message' => 'Необходимо обратиться в Фонд в связи с наличием остатка средств',
            ];
        }

        return null;
    }
}
