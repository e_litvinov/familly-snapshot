<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Report\AnalyticReport\FactorAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringDevelopment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Context;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DoubleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KeyRisk;
use NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Utils\Report\PublicationIndicators;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;

class AnalyticReportResolver extends DefaultReportResolver
{
    /** @var $this->em EntityManager */
    private $em;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->em = $this->container->get('doctrine')->getManager();
        $this->isDoubleSubmit = true;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }

        return [];
    }

    public function createAssociations(Report $object)
    {
        $object->addPracticeAnalyticResult(new DirectResult());
        $object->addSocialNotSolvedProblem(new DoubleContext());
        $object->addSocialSolvedProblem(new DoubleContext());
        $object->addSocialMeasure(new Context());
        $object->addRealizationFactor(new DoubleContext());
        $object->addNextSocialMeasure(new Context());
        $object->addExpectedAnalyticResult(new DirectResult());
        $object->createPublications($object, PublicationIndicators::ANALYTIC_REPORT_PUBLICATIONS_INDICATOR);
        $object->addPracticeNotSolvedProblem(new DoubleContext());
        $object->addPracticeSolvedProblem(new DoubleContext());
        $object->addResultStability(new Context());
        $object->addIntroductionFactor(new DoubleContext());
        $object->addMonitoringResult(new TripleContext());
        $object->addNewMonitoringElement(new MonitoringElement());
        $object->addMonitoringChange(new Context());
        $object->addMonitoringDevelopment(new MonitoringDevelopment());
        $object->addKeyRisk(new KeyRisk());
        $object->addPartnerActivity(new QuadrupleContext());
        $object->addHumanResource(new HumanResource());
        $object->addPracticeFeedbackAttachment(new Feedback());
        $object->addPracticeSpreadFeedbackAttachment(new Feedback());
        $object->addPracticeSuccessStoryAttachment(new Attachment());
        $object->addPracticeSpreadSuccessStoryAttachment(new Attachment());
        $this->addPublicationAttachments($object);
        $this->addMaterialAttachments($object);
        $object->addFactorAttachment(new FactorAttachment());
        $object->addMonitoringAttachment(new MonitoringAttachment());

        $this->loadApplicationValues($object);

        if (!$this->getMonitoringReport($object)) {
            return [
                'flash' => [
                'type' => 'sonata_flash_error',
                'message' => 'error_monitoring_report_does_not_exist',
                ], 'redirectTo' => self::BASE_LIST_ROUTE,
            ];
        }

        $this->loadMonitoringResultsDependency($object);
    }

    public function initEdit($object, $request, $params = [])
    {
        $this->addPublicationAttachments($object);
        $this->addMaterialAttachments($object);

        return [];
    }

    private function addPublicationAttachments(Report $object)
    {
        $countPublications = $object->getAnalyticPublications()->filter(function ($entity) {
            return $entity->getIndicator() == PublicationIndicators::ANALYTIC_REPORT_PUBLICATIONS_INDICATOR[0];
        })->first()->getFact();

        $countAttachments = count($object->getPublicationAttachments());
        if ($countAttachments < $countPublications) {
            for ($i = 0; $i < $countPublications - $countAttachments; $i++) {
                $object->addPublicationAttachment(new PublicationAttachment());
            }
        } elseif ($countAttachments > $countPublications) {
            for ($i = $countAttachments - $countPublications; $i > 0; $i--) {
                $object->removePublicationAttachment($object->getPublicationAttachments()[$countAttachments - $i]);
            }
        }
    }

    private function addMaterialAttachments($object)
    {
        $countMaterials = $object->getAnalyticPublications()->filter(function ($entity) {
            return $entity->getIndicator() == PublicationIndicators::ANALYTIC_REPORT_PUBLICATIONS_INDICATOR[1];
        })->first()->getFact();

        $countAttachments = count($object->getMaterialAttachments());
        if ($countAttachments < $countMaterials) {
            for ($i = 0; $i < $countMaterials - $countAttachments; $i++) {
                $object->addMaterialAttachment(new MaterialAttachment());
            }
        } elseif ($countAttachments > $countMaterials) {
            for ($i = $countAttachments - $countMaterials; $i > 0; $i--) {
                $object->removeMaterialAttachment($object->getMaterialAttachments()[$countAttachments - $i]);
            }
        }
    }

    private function loadApplicationValues(Report $report)
    {
        /**
         * @var Competition $competition
         */
        $competition = $report->getReportForm()->getCompetition();

        $applicationHistory = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
            'psrn' => $report->getPsrn(),
            'competition' => $competition,
            'isSend' => true
        ]);

        $application = unserialize($applicationHistory->getData());
        $report->setProjectPurpose($application->getPurpose());
    }

    private function loadMonitoringResultsDependency(Report $report)
    {
        $monitoringReport = $this->getMonitoringReport($report);
        $this->get('nko_order.loader.monitoring_results')->load($monitoringReport->getMonitoringResults(), $report);
    }

    private function getMonitoringReport(Report $report)
    {
        $reportForm = $report->getReportForm();
        $monitoringReportForm = $this->em->getRepository(ReportForm::class)->findOneBy([
            'competition' => $reportForm->getCompetition(),
            'year' => $reportForm->getYear(),
            'reportClass' => MonitoringReport::class
        ]);

        $monitoringReport = $this->em->getRepository(MonitoringReport::class)->findOneBy([
            'author' => $report->getAuthor(),
            'reportForm' => $monitoringReportForm
        ]);

        return $monitoringReport;
    }
}
