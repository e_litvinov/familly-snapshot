<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report\MixedReport\ResultType;
use NKO\OrderBundle\Utils\ReportTypes;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\Report\MixedReport\Attachment;
use NKO\OrderBundle\Entity\Report\MixedReport\Result;
use NKO\OrderBundle\Entity\TrainingEvent;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Resolver\FinanceResolver;

class MixedReportResolver extends DefaultReportResolver
{
    private $em;
    private $financeResolver;

    public function __construct(ContainerInterface $container, EntityManager $em, FinanceResolver $financeResolver)
    {
        parent::__construct($container);
        $this->em = $em;
        $this->financeResolver = $financeResolver;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }

        return [];
    }

    public function createAssociations(BaseReport $object)
    {
        $object->addEvent(new TrainingEvent());
        $object->addMixedEmployeeResult(new Result());
        $object->addAnalyticAttachment(new Attachment());
        $object->addMixedBeneficiariesResult(new Result());
        $object->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
        $this->setResultTypes($object);
        $this->financeResolver->create($object);
    }

    private function setResultTypes($object)
    {
        $mixedResultIndex = 1;

        $resultTypes = $this->em->getRepository(ResultType::class)
            ->createQueryBuilder('r')
            ->where('r.type like :type')
            ->setParameters([
                'type' => '%' . ReportTypes::MIXED_KNS_REPORT . '%'
            ])
            ->getQuery()
            ->getResult();

        foreach ($resultTypes as $resultType) {
            $mixedResult = new Result();
            $mixedResult->setCustomIndex($mixedResultIndex++);
            $mixedResult->setLinkedTypes($resultType);
            $object->addMixedResult($mixedResult);
        }
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        $currentDate = new \DateTime();
        $updatedAt = $object->getUpdatedAt() ? $object->getUpdatedAt() : $currentDate;
        $updatedAt = $updatedAt->format('d.m.Y H:i');

        $formView->children['updatedAt']->vars['value'] = $updatedAt;
    }
}
