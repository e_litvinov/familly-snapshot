<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\EntityResolver\CreateResolverInterface;
use NKO\OrderBundle\EntityResolver\DoubleSubmitInterface;
use NKO\OrderBundle\EntityResolver\EditResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class DefaultReportResolver implements CreateResolverInterface, EditResolverInterface, DoubleSubmitInterface
{
    const BASE_LIST_ROUTE = 'admin_nko_order_basereport_list';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var bool
     */
    protected $isDoubleSubmit;                          //Если нужен двойной сабмит

    /**
     * @var bool
     */
    protected $isReportValid;                          //Если нужна кастомная валидация

    /**
     * @var bool
     */
    protected $additionConditions;                      //Дополнительные условия в if для проверки валидности

    /** @var FlashBag */
    protected $flashBag;

    public function __construct($container)
    {
        $this->container = $container;
        $this->flashBag = $container->get('session.flash_bag');
        $this->isDoubleSubmit = false;
        $this->additionConditions = true;
        $this->isReportValid = true;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function initCreate($object, $request, $params = [])
    {
        if ($this->checkIssetReport($object, $params['byPeriod'])) {
            return [
                'flash' => [
                    'type' => 'sonata_flash_error',
                    'message' => 'Does_not_find_last_report',
                    ],
                'redirectTo' => 'list'
            ];
        }

        if (method_exists($this, 'createAssociations')) {
            return $this->createAssociations($object);
        }

        return [];
    }

    public function afterSubmitInvalidCreate($object, $form, $params = [])
    {
        return;
    }

    public function afterSubmitCreate($object, $form, $params = [])
    {
        $this->logAction($object, 'Добавление отчета');

        return;
    }

    public function afterSubmitValidCreate($object, $form, $params = [])
    {
        return;
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        return;
    }

    public function logSubmitEdit($object, $form, $params = [])
    {
        $nameOfAction = 'Сохранение отчета пользователем';

        $request = $params['request'];
        if ($request->get('send') == "sendButton" && $form->isValid()) {
            $nameOfAction = 'Отправка отчета';
        }

        $this->logAction($object, $nameOfAction);
    }

    /**
     * @param BaseReport $object
     * @param Request $request
     * @param array $params
     * @return array
     */
    public function initEdit($object, $request, $params = [])
    {
        return [];
    }

    public function afterSubmitInvalidEdit($object, $form, $params = [])
    {
        return;
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        return;
    }

    public function checkReport($object)
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isDoubleSubmit()
    {
        return $this->isDoubleSubmit;
    }

    /**
     * @return bool
     */
    public function isAdditionConditions()
    {
        return $this->additionConditions;
    }

    /**
     * @return bool
     */
    public function isReportValid()
    {
        return $this->isReportValid;
    }

    public function skipUpdate($object = null, $form = null, $params = null)                 //Если в отчёте нужен другой сабмит
    {
        return false;
    }

    protected function checkIssetReport($object, $period = null)
    {
        $em = $this->container->get('doctrine')->getManager();
        $generalCondition = [
            'reportForm' => $object->getReportForm(),
            'applicationHistory' => $object->getApplicationHistory(),
        ];

        $generalCondition = $period ? array_merge($generalCondition, ['period' => $object->getPeriod()]) : $generalCondition;
        return $em->getRepository(BaseReport::class)->findOneBy($generalCondition);
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        return;
    }

    public function submitAlternative(BaseReport $object, $form, $params = [])
    {
        $message = $object->getIsAutosaved() ? 'Загружена автосохраненная версия' : 'Загружена версия, сохраненная вручную';

        $this->flashBag->add('sonata_flash_info', $message);
        $request = key_exists('request', $params) ?  $params['request'] : null;
        if ($request &&  parse_url($request->headers->get('referer'))['path'] != parse_url($request->getUri())['path']) {
            $this->logAction($object, 'Открытие отчета в редакторе');
        }
    }

    public function preDeleteAction($object, $form, $params = [])
    {
        $this->logAction($object, 'Удаление отчета');
    }

    protected function logAction($object, $nameOfAction)
    {
        $service = $this->container->get('NKO\LoggerBundle\Loggers\ReportLogger');
        $service->logAction($nameOfAction, $object);
    }

    public function setIsSendAfterComplete($object, $isPreviosSend)
    {
        $object->setIsSend($isPreviosSend);
    }

    public function preCompleteEdit($object = null, $form = null, $params = null)
    {
        return;
    }
}
