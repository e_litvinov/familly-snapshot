<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportEntityResolverHelperTrait;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTraitNotifications;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Resolver\FinanceResolver;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Document;

class FinanceReportResolver extends DefaultReportResolver
{
    use FinanceReportTraitNotifications;
    use FinanceReportEntityResolverHelperTrait;

    private $em;
    private $financeResolver;

    public function __construct(ContainerInterface $container, EntityManager $em, FinanceResolver $financeResolver)
    {
        parent::__construct($container);
        $this->em = $em;
        $this->financeResolver = $financeResolver;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }
    }

    public function createAssociations(BaseReport $object)
    {
        $this->financeResolver->create($object);
        $object->addDocument(new Document());
    }

    public function skipUpdate($object = null, $form = null, $params = null)
    {
        if (key_exists('request', $params) &&
            $params['request']->get('completeEdit') != "completeEditionButton" &&
            $params['request']->get('autosaver') != "true"
        ) {
            $costOverruns =  $this->checkCostOverruns($object);
            if ($costOverruns) {
                $this->flashBag->add($costOverruns['type'], $costOverruns['message']);
            }

            $donationBalance = $this->checkDonationBalance($object);
            if ($donationBalance) {
                $this->flashBag->add($donationBalance['type'], $donationBalance['message']);
            }
        }
    }
}
