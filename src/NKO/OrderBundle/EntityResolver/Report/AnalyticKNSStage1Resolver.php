<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report\AnalyticReport\Context;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\ReportResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1\TrainingEvent;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application;

class AnalyticKNSStage1Resolver extends DefaultReportResolver
{
    const DIRECT_RESULT = [
        'Число обученных сотрудников организации / членов общественного объединения',
        'Число сотрудников, организации / членов общественного объединения, которые получили/ат новые знания благодаря реализации проекта',
        'Число внедренных практик (технологий, услуг, моделей и пр.) в деятельность организации благодаря реализации проекта'
    ];

    public function __construct($container)
    {
        parent::__construct($container);
        $this->isDoubleSubmit = true;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }
    }

    public function createAssociations(Report $object)
    {
        $object->addReportTrainingEvent(new TrainingEvent());
        $object->addLesson(new Context());
        $object->addSuccessStory(new Context());
        $object->addFeedbackItem(new Context());

        foreach (self::DIRECT_RESULT as $result) {
            $newResult = new ReportResult();
            $newResult->setResult($result);
            $object->addDirectEmployeeResult($newResult);
        }
        $object->addQualitativeEmployeeResult(new ReportResult());
        $object->addOtherQualitativeEmployeeResult(new ReportResult());
        $object->addSocialReportResult(new ReportResult());
        $object->addOtherSocialResult(new ReportResult());
        $object->addTrainingEventAttachment(new Attachment());
        $object->addEtcAttachment(new Attachment());
        $object->addBeneficiaryResult(new ReportResult());
        $object->addOtherBeneficiaryResult(new ReportResult());
    }
}
