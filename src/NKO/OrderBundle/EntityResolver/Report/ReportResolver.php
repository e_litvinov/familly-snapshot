<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\BeneficiaryReportResult;
use NKO\OrderBundle\Entity\DocumentReport;
use NKO\OrderBundle\Entity\EmployeeReportResult;
use NKO\OrderBundle\Entity\Expense;
use NKO\OrderBundle\Entity\FinanceSpent;
use NKO\OrderBundle\Entity\Register;
use NKO\OrderBundle\Entity\TrainingEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ReportResolver extends DefaultReportResolver
{
    /** @var $this->em EntityManager */
    private $em;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }
    }

    public function createAssociations(Report $object)
    {
        // временное решение, пока не станет понятно, как выбирается конкурс, на который заполняется отчет
        $competition = $this->em->getRepository('NKOOrderBundle:Competition')->find(3);
        $expenseTypes = $competition->getExpenseTypes();

        if ($expenseTypes->isEmpty()) {
            return [
                'flash' => [
                    'type' => 'sonata_flash_error',
                    'message' => 'flash_expense_type_error',
                ], 'redirectTo' => 'list',
            ];
        }

        foreach ($expenseTypes as $expenseType) {
            $register = new Register();
            $register->addExpense(new Expense());
            $register->setExpenseType($expenseType);
            $object->addRegister($register);
            $register->setReport($object);

            $financeSpent = new FinanceSpent();
            $financeSpent->setExpenseType($expenseType);
            $object->addFinanceSpent($financeSpent);
        }

        $object->addEmployeeReportResult(new EmployeeReportResult());
        $object->addBeneficiaryReportResult(new BeneficiaryReportResult());
        $object->addEvent(new TrainingEvent());
        $object->addDocument(new DocumentReport());

        $current_user = $this->container->get('security.token_storage')->getToken()->getUser();



















        /*@toDo нормальное получение competitionId*/
        $applicationHistory = $this->em->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findSendByAuthor($current_user, 3);

        if ($applicationHistory == null) {
            return [
                'flash' => [
                    'type' => 'sonata_flash_error',
                    'message' => 'flash_create_error_report_application_not_exist',
                ], 'redirectTo' => 'list',
            ];
        }
        $application = unserialize($applicationHistory->getData());

        $object->setRecipient($application->getName());
        $object->setContract($applicationHistory->getContract());
        $object->setProjectName($application->getProjectName());
        $object->setTraineeshipTopic($application->getTraineeshipTopic());
        $organization = $application->getOrganization();
        if ($organization != null) {
            $organization = $this->em->getRepository("NKOOrderBundle:Organization")->findOneBy(array('id'=>$organization->getId()));
        }
        $object->setOrganization($organization);

        $object->setDeadLineStart($application->getDeadLineStart());
        $object->setDeadLineFinish($application->getDeadLineFinish());
        $object->setSumGrant($applicationHistory->getSumGrant());
        $object->setPurpose($application->getProjectPurpose());
        $object->setApplicationHistory($applicationHistory);
        $object->setBeneficiaryGroups($application->getBeneficiaryGroups());
        $object->setBeneficiaryGroupName($application->getBeneficiaryGroupName());


        return [];
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        $docForms = $form['documents'];
        $docs = $object->getDocuments();
        foreach ($docs as $key => $doc) {
            if (count($docForms[$key]['file']->getErrors())) {
                $doc->setFile(null);
            }
        }
    }
    public function initEdit($object, $request, $params = [])
    {
        $object->setUpdatedAt(new \DateTime());

        return [];
    }
}
