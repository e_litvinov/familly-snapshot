<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MaterialAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Publication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PublicationAttachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\ExpectedResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource;
use NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2019\Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Context;
use NKO\OrderBundle\Utils\Report\PublicationIndicators;
use NKO\OrderBundle\Utils\ReportTypes;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use Symfony\Component\PropertyAccess\PropertyAccess;

class AnalyticReport2019Resolver extends DefaultReportResolver
{

    /** @var $em EntityManager */
    private $em;

    public function __construct($container, EntityManager $em)
    {
        $this->em = $em;
        parent::__construct($container);
        $this->isDoubleSubmit = true;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }
    }

    public function initEdit($object, $request, $params = [])
    {
        $this->controlNumberOfAttachments($object);
    }

    public function createAssociations(Report $object)
    {
        $object->addTerritory(new Territory());

        $object->addResourceBlockItem(new ExpectedResult());
        $object->addResourceBlockEtcItem(new ExpectedResult());
        $object->addQualificationEtcItem(new ExpectedResult());
        $object->addQualificationItem(new ExpectedResult());
        $object->addProcessItem(new ExpectedResult());
        $object->addProcessEtcItem(new ExpectedResult());
        $object->addExperienceItem(new ExpectedResult());
        $object->addExperienceEtcItem(new ExpectedResult());
        $object->addNewMonitoringElement(new MonitoringElement());
        $object->addMonitoringChange(new Context());

        $object->addHumanResource(new HumanResource());
        $object->addPartnerActivity(new QuadrupleContext());

        $object->addPracticeFeedbackAttachment(new Feedback());
        $object->addPracticeSpreadFeedbackAttachment(new Feedback());
        $object->addPracticeSocialFeedbackAttachment(new Feedback());
        $object->addPracticeSocialSpreadFeedbackAttachment(new Feedback());
        $object->addPracticeSuccessStoryAttachment(new Attachment());
        $object->addPracticeSpreadSuccessStoryAttachment(new Attachment());
        $object->addMonitoringDevelopmentAttachment(new Attachment());

        $practiceImplementation = new PracticeResult();
        $practiceIntroduction = new PracticeResult();
        $this->em->persist($practiceImplementation);
        $this->em->persist($practiceIntroduction);

        $object->setPracticeImplementation($practiceImplementation);
        $object->setPracticeIntroduction($practiceIntroduction);

        $this->loadApplication($object);
        $this->loadMonitoringReport($object);

        $object->createPublications($object, PublicationIndicators::ANALYTIC_REPORT2019_PUBLICATIONS_INDICATOR);
    }

    private function loadApplication(Report $report)
    {
        $application = $this->getApplication($report);
        $this->container->get('NKO\OrderBundle\Loader\Loader')->load($report, $application);
    }

    private function getApplication($report)
    {
        /** @var Report $report*/
        $application = $this->em->getRepository(BaseApplication::class)->findOneBy([
            'author' => $report->getAuthor(),
            'competition' => $report->getReportForm()->getCompetition()
        ]);

        return $application;
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        $linkedForm = $object->getReportForm()->getLinkedForm();
        $monitoringReport = $linkedForm ? $this->em->getRepository(MonitoringReport::class)->findReportsByReportForm($linkedForm, $object->getAuthor()) : null;
        $amountFactorAttachment = $monitoringReport ? $monitoringReport->getAmountInMonitoringReport() : 0;

        $formView->vars['class'] = ReportTypes::ANALYTIC_REPORT_2018;
        $formView->vars['amount'] = $amountFactorAttachment;
    }

    private function loadMonitoringReport(Report $report)
    {
        $linkedForm = $report->getReportForm()->getLinkedForm();
        $monitoringReport = $linkedForm ? $this->em->getRepository(MonitoringReport::class)->findReportsByReportForm($linkedForm, $report->getAuthor()) : null;

        if ($monitoringReport) {
            $this->container->get('NKO\OrderBundle\Loader\Loader')->load($report, $monitoringReport);
        }
    }

    public function controlNumberOfAttachments(Report $object)
    {
        $indicators = PublicationIndicators::ANALYTIC_REPORT2019_PUBLICATIONS_INDICATOR;
        $this->controlNumberOfPublicationAttachment($object, $indicators[PublicationIndicators::MESSAGES], PublicationAttachment::class, $object->getPublicationAttachments());
        $this->controlNumberOfPublicationAttachment($object, $indicators[PublicationIndicators::PUBLISHED], MaterialAttachment::class, $object->getMaterialAttachments());
    }

    public function controlNumberOfPublicationAttachment(Report $object, $keyPublication, $class, $attachments)
    {
        $analyticPublication = $object->getAnalyticPublications()->filter(function ($publication) use ($keyPublication) {
            return $publication->getIndicator() == $keyPublication;
        })->first();

        $needCount = $analyticPublication->getFact() ?: 0;
        $existCount = $attachments->count();
        $difference = $needCount - $existCount;

        if ($difference > 0) {
            while ($difference--) {
                $newElement = new $class();
                $attachments[] = $newElement;
                $newElement->setReport($object);
            }
        } else {
            while ($difference++) {
                $attachments->removeElement($attachments->last());
            }
        }
    }
}
