<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Utils\ReportTypes;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Context;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MonitoringElement;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\PracticeResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\TripleContext;
use NKO\OrderBundle\Utils\Report\PublicationIndicators;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource;
use NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\SocialResult;

class AnalyticReport2018Resolver extends DefaultReportResolver
{
    /** @var $em EntityManager */
    private $em;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->em = $this->container->get('doctrine')->getManager();
        $this->isDoubleSubmit = true;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }
    }

    public function createAssociations(Report $object)
    {
        $practiceIntroduction = new PracticeResult();
        $practiceImplementation = new PracticeResult();
        $this->em->persist($practiceIntroduction);
        $this->em->persist($practiceImplementation);

        $object->addTerritory(new Territory());
        $object->addHumanResource(new HumanResource());
        $object->addPartnerActivity(new QuadrupleContext());
        $object->addPracticeFeedbackAttachment(new Feedback());
        $object->addPracticeSpreadFeedbackAttachment(new Feedback());
        $object->addPracticeSuccessStoryAttachment(new Attachment());
        $object->addPracticeSpreadSuccessStoryAttachment(new Attachment());
        $object->addMonitoringChange(new Context());
        $object->addNewMonitoringElement(new MonitoringElement());
        $object->addMonitoringResult(new TripleContext());
        $object->addMonitoringDevelopmentAttachment(new Attachment());
        $object->createPublications($object, PublicationIndicators::ANALYTIC_REPORT2018_PUBLICATIONS_INDICATOR);
        $object->addImplementationSocialResult(new SocialResult());
        $object->addIntroductionSocialResult(new SocialResult());
        $object->setPracticeIntroduction($practiceIntroduction);
        $object->setPracticeImplementation($practiceImplementation);

        $this->loadApplication($object);
        $this->loadMonitoringReport($object);
    }

    private function loadApplication(Report $report)
    {
        $application = $this->getApplication($report);
        $this->container->get('NKO\OrderBundle\Loader\Loader')->load($report, $application);
    }

    private function loadMonitoringReport(Report $report)
    {
        $linkedForm = $report->getReportForm()->getLinkedForm();
        $monitoringReport = $linkedForm ? $this->em->getRepository(MonitoringReport::class)->findReportsByReportForm($linkedForm, $report->getAuthor()) : null;

        if ($monitoringReport) {
            $this->container->get('NKO\OrderBundle\Loader\Loader')->load($report, $monitoringReport);
        }
    }
    
    private function getApplication($report)
    {
        /** @var Report $report*/
        $application = $this->em->getRepository(BaseApplication::class)->findOneBy([
            'author' => $report->getAuthor(),
            'competition' => $report->getReportForm()->getCompetition()
        ]);

        return $application;
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        $linkedForm = $object->getReportForm()->getLinkedForm();
        $monitoringReport = $linkedForm ? $this->em->getRepository(MonitoringReport::class)->findReportsByReportForm($linkedForm, $object->getAuthor()) : null;
        $amountFactorAttachment = $monitoringReport ? $monitoringReport->getAmountInMonitoringReport() : 0;

        $formView->vars['class'] = ReportTypes::ANALYTIC_REPORT_2018;
        $formView->vars['amount'] = $amountFactorAttachment;
    }
}
