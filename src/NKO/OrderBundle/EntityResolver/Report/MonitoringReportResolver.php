<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use phpDocumentor\Reflection\Types\Null_;

class MonitoringReportResolver extends DefaultReportResolver
{
    const INDIVIDUAL_RESULT_COUNT = 10;
    const MONITORING_RESULTS_COUNT = 76;

    /** @var $this->em EntityManager */
    private $em;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->em = $this->container->get('doctrine')->getManager();
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }

        return [];
    }

    public function createAssociations(Report $object)
    {
        $reportTemplate = $object->getReportTemplate();

        $reportForm = $object->getReportForm();
        $templateResults = !$reportForm->getIsSelectable() ? $reportTemplate->getMonitoringResults() : false;
        $periods = $reportForm->getReportPeriods();
        $indicators = $this->em->getRepository(Indicator::class)->findAllExceptCode(IndicatorInterface::CUSTOM_VALUE);

        foreach ($indicators as $indicator) {
            $monitoringResult = $this->createMonitoringResult($indicator, $object, $periods);

            if ($templateResults) {
                $result = $templateResults->filter(function ($object) use ($indicator) {
                    return $object->getIndicator()->getId() == $indicator->getId();
                });

                if (!$result->isEmpty()) {
                    $periodResults = $result->first()->getPeriodResults();

                    foreach ($periodResults as $result) {
                        $periodResult = clone $result;
                        $monitoringResult->addPeriodResult($periodResult);
                    }
                } elseif ($indicator->getParent()) {
                    $this->createPeriodResult($monitoringResult, true, null);
                }
            } else {
                $this->createPeriodResult($monitoringResult, true, null);
            }

            if ($indicator->getCode() == IndicatorInterface::PRE_CUSTOM_VALUE) {
                $index = $indicator->getIndexNumber();
                $customIndicator = $this->createCustomIndicator($indicator->getParent(), ++$index);
                $customMonitoringResult = $this->createMonitoringResult($customIndicator, $object, $periods);
                $this->createPeriodResult($customMonitoringResult, true, null);
            }

            if ($indicator->getCode() == IndicatorInterface::INDIVIDUAL_RES_CODE) {
                $index = IndicatorInterface::INDIVIDUAL_RES_COUNT;
                for ($i = 0; $i < self::INDIVIDUAL_RESULT_COUNT; $i++) {
                    $customIndicator = $this->createCustomIndicator($indicator, $index + $i);
                    $customMonitoringResult = $this->createMonitoringResult($customIndicator, $object, $periods);
                    $this->createPeriodResult($customMonitoringResult, true, null);
                }
            }
        }

        $document = new MonitoringDocument();
        $object->addDocument($document);





    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        $yearFilling = $object->getReportForm()->getYear() >= new \DateTime('2018-01-01');
        //$this->isReportValid = $yearFilling ?: $this->isReportValidChecker($object, $yearFilling);
        $this->isReportValid = true;
    }

    public function checkReport($object)
    {
        $this->checkIssetReport($object);
    }

    private function createMonitoringResult($indicator, $object, $periods)
    {
        $monitoringResult = new MonitoringResult();
        $monitoringResult->setIndicator($indicator);
        $object->addMonitoringResult($monitoringResult);
        if ($indicator->getParent()) {
            foreach ($periods as $period) {
                $this->createPeriodResult($monitoringResult, false, $period);
            }
        }

        return $monitoringResult;
    }

    private function createPeriodResult($monitoringResult, $isFinalResult, $period = null)
    {
        $periodResult = new PeriodResult();
        $periodResult->setPeriod($period);
        $periodResult->setIsFinalResult($isFinalResult);
        $monitoringResult->addPeriodResult($periodResult);
    }

    private function createCustomIndicator($parent, $index)
    {
        $indicator = new Indicator();
        $indicator->setCode(IndicatorInterface::CUSTOM_VALUE);
        $indicator->setParent($parent);
        $indicator->setIndexNumber($index);
        return $indicator;
    }

    private function checkFilesValidation($form, $report)
    {
        for ($i = 0; $i < count($form['documents']); $i ++) {
            if (count($form['documents'][$i]['file']->getErrors())) {
                $report->getDocuments()[$i]->setFile(null);
            }
        }
    }

    private function isReportValidChecker(Report $report, $yearFilling)
    {
        $isSocialValid = false;
        $isProdValid = false;

        $monitoringResults = $report->getMonitoringResults();
        foreach ($monitoringResults as $monitoringResult) {
            $type = $monitoringResult->getIndicator()->getType();

            if ($type == IndicatorInterface::SOCIAL_TYPE && !$isSocialValid) {
                $isSocialValid = $this->isPeriodResultsValid($monitoringResult->getPeriodResults());
            }

            if ($yearFilling) {
                $isProdValid = true;
            } elseif ($type == IndicatorInterface::PROD_TYPE && !$isProdValid) {
                $isProdValid = $this->isPeriodResultsValid($monitoringResult->getPeriodResults());
            }
        }
        return ($isSocialValid && $isProdValid);
    }

    private function isPeriodResultsValid($periodResults)
    {
        foreach ($periodResults as $periodResult) {
            if (!$periodResult->isFinalResult()) {
                if ($periodResult->getNewAmount()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function afterSubmitInvalidEdit($object, $form, $params = [])
    {
        foreach ($form['documents'] as $document) {
            $data = $document->getData();
            if (count($document['file']->getErrors())) {
                $data->setFile(null);
            }
        }
    }
}
