<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Document;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportEntityResolverHelperTrait;
use NKO\OrderBundle\Traits\FinanceReport\FinanceReportTraitNotifications;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Resolver\FinanceResolver;

class FinanceReport2019Resolver extends DefaultReportResolver
{
    use FinanceReportTraitNotifications;
    use FinanceReportEntityResolverHelperTrait;

    private $em;
    private $financeResolver;

    public function __construct(ContainerInterface $container, EntityManager $em, FinanceResolver $financeResolver)
    {
        parent::__construct($container);
        $this->em = $em;
        $this->financeResolver = $financeResolver;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }

        $response = $this->checkIsSendPreviousReport($object, $request);
        if ($response) {
            return $response;
        }
    }

    public function createAssociations(BaseReport $object)
    {
        $this->financeResolver->create($object);
        $object->addDocument(new Document());
    }

    public function initEdit($object, $request, $params = [])
    {
        $response = $this->financeResolver->isSentReport($object, $request);
        $this->additionConditions = $response['parameters']['sentReport'];
        $this->isReportValid = $this->additionConditions;

        return $response;
    }

    protected function checkIsSendPreviousReport($object, $request)
    {
        $pastPeriods = $this->em->getRepository(PeriodReport::class)->findPastReportPeriods($object);

        $reportForm = $object->getReportForm();
        if ($pastPeriods) {
            $sentReport = $this->em->getRepository(BaseReport::class)->findOneBy([
                'isSend' => true,
                'reportForm' => $reportForm,
                'psrn' => $object->getAuthor()->getPsrn(),
                'period' => array_pop($pastPeriods)
            ]);

            if (!$sentReport) {
                return [
                    'flash' => [
                        'type' => 'sonata_flash_error',
                        'message' => 'Does_not_find_last_send_report',
                    ],
                    'redirectTo' => 'list'
                ];
            }
        }
    }

    public function skipUpdate($object = null, $form = null, $params = null)
    {
        if (key_exists('request', $params) &&
            $params['request']->get('completeEdit') != "completeEditionButton" &&
            $params['request']->get('autosaver') != "true"
        ) {
            $costOverruns =  $this->checkCostOverruns($object);
            if ($costOverruns) {
                $this->flashBag->add($costOverruns['type'], $costOverruns['message']);
            }

            $donationBalance = $this->checkDonationBalance($object);
            if ($donationBalance) {
                $this->flashBag->add($donationBalance['type'], $donationBalance['message']);
            }
        }
    }
}
