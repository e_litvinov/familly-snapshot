<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource;
use NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Publication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;

class AnalyticKNSReport2018Resolver extends DefaultReportResolver
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->isDoubleSubmit = true;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }

        return [];
    }

    public function createAssociations($object)
    {
        $object->addHumanResource(new HumanResource());
        $object->addPartnerActivity(new QuadrupleContext());
        $object->addPublication(new Publication());
        $object->addTerritory(new Territory());
        $object->addExpectedAnalyticResult(new DirectResult());
        $object->addPracticeAnalyticResult(new DirectResult());
        $object->addPracticeFeedbackAttachment(new Feedback());
        $object->addPracticeSuccessStoryAttachment(new Attachment());
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        $currentDate = new \DateTime();
        $updatedAt = $object->getUpdatedAt() ? $object->getUpdatedAt() : $currentDate;
        $updatedAt = $updatedAt->format('d.m.Y H:i');

        $formView->children['updatedAt']->vars['value'] = $updatedAt;
    }
}
