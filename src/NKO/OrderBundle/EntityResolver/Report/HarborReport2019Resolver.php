<?php

namespace NKO\OrderBundle\EntityResolver\Report;

use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\Report2019\Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource;
use NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Publication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\DirectResult as Experience;

class HarborReport2019Resolver extends DefaultReportResolver
{
    public function __construct($container)
    {
        parent::__construct($container);
        $this->isDoubleSubmit = true;
    }

    public function initCreate($object, $request, $params = [])
    {
        $response = parent::initCreate($object, $request, $params);
        if ($response) {
            return $response;
        }

        return [];
    }

    public function createAssociations($object)
    {
        /** @var $object Report */
        $object->addTerritory(new Territory());
        $object->addExpectedAnalyticResult(new DirectResult());
        $object->addPracticeAnalyticResult(new DirectResult());
        $object->addPublication(new Publication());
        $object->addExperience(new Experience());
        $object->addPartnerActivity(new QuadrupleContext());
        $object->addHumanResource(new HumanResource());
        $object->addPracticeFeedbackAttachment(new Feedback());
        $object->addPracticeSuccessStoryAttachment(new Attachment());
        $object->addPracticeSpreadSuccessStoryAttachment(new Attachment());

        $application = unserialize($object->getApplicationHistory()->getData());
        $object->setProjectName($application->getProjectName());
    }

    public function afterFormViewCreateEdit($object, $formView)
    {
        $updatedAt = $object->getUpdatedAt() ? $object->getUpdatedAt() : new \DateTime();
        $updatedAt = $updatedAt->format('d.m.Y H:i');

        $formView->children['updatedAt']->vars['value'] = $updatedAt;
    }
}
