<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/29/18
 * Time: 3:31 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;

class KNS2017ApplicationResolver extends DefaultApplicationResolver
{
    public function afterSubmitEdit($object, $form, $params = [])
    {
        $user = $params['user'];

        if (count($form['regulation']->getErrors())) {
            $object->setRegulation(null);
        }
        if (count($form['organizationCreationResolution']->getErrors())) {
            $object->setOrganizationCreationResolution(null);
        }
        if (count($form['budget']->getErrors())) {
            $object->setBudget(null);
        }
        $object->setReadyToSent(false);
        $object->setIsSend(false);
        if (get_class($user) == 'NKO\UserBundle\Entity\NKOUser') {
            $object->setAuthor($user);
            $object->setPSRN($user->getPsrn());
        }
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $request = $params['request'];
        $object->setReadyToSent(true);
        $object->setRegulation($form['regulation']->getData());
        $object->setOrganizationCreationResolution($form['organizationCreationResolution']->getData());
        $object->setBudget($form['budget']->getData());
        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }
}
