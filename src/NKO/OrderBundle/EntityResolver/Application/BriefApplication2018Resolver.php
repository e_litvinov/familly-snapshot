<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/30/18
 * Time: 12:17 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Loader\Application\Farvater\Application2018\SocialResultLoader;
use NKO\OrderBundle\Loader\Application\Farvater\Application2018\TargetGroupLoader;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSource;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\Publication;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplciation2018;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class BriefApplication2018Resolver extends DefaultApplicationResolver
{
    /**
     * @var TargetGroupLoader $targetGroupLoader
     */
    private $targetGroupLoader;

    /**
     * @var SocialResultLoader $socialResultLoader
     */
    private $socialResultLoader;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    public function __construct(ContainerInterface $container, EntityManager $em, TargetGroupLoader $targetGroupLoader, SocialResultLoader $socialResultLoader, FlashBag $flashBag)
    {
        $this->targetGroupLoader = $targetGroupLoader;
        $this->socialResultLoader = $socialResultLoader;
        $this->entityManager = $em;

        parent::__construct($container, $flashBag);
    }

    public function initCreate($object, $request, $params = [])
    {
        if (parent::initCreate($object, $request, $params)) {
            $user = $params['user'];
            $this->createFinancingsources($object);
            $this->container->get(get_class($object).'\EffectivenessLoader')->load($object);

            if ($user instanceof NKOUser) {
                $object->setName($user->getNkoName());

                return true;
            }
        }
        return false;
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        if (count($form['regulation']->getErrors())) {
            $object->setRegulation(null);
        }

        if (count($form['signedAgreement']->getErrors())) {
            $object->setSignedAgreement(null);
        }

        $object->setReadyToSent(false);
        $object->setIsSend(false);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $request = $params['request'];

        $object->setReadyToSent(true);
        $object->setRegulation($form['regulation']->getData());
        $object->setSignedAgreement($form['signedAgreement']->getData());

        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }

        $application = $this->entityManager->getRepository(FarvaterApplciation2018::class)->findOneBy([
            'author' => $object->getAuthor()
        ]);

        if (!$application) {
            return;
        }

        $this->targetGroupLoader->update($application, $object);
        $this->socialResultLoader->update($application, $object);
    }

    public function createAssociations(BaseApplication $application)
    {
        $application->addSocialNetworkLink(new SocialNetworkLink());
        $application->addSiteLink(new SiteLink());
        $application->addBeneficiaryProblem(new Problem());
        $application->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
        $application->addEffectivenessEtcItem(new Effectiveness());
        $application->addProject(new Project());
        $application->addPublication(new Publication());
    }

    private function createFinancingSources(BaseApplication $application)
    {
        $em = $this->container->get('Doctrine')->getManager();
        $types = $em->getRepository(FinancingSourceType::class)->createQueryBuilder('o')
                ->where('o.types like :type')
                ->setParameter('type', '%' . ApplicationTypes::BRIEF_APPLICATION_2018 . '%')
                ->getQuery()
                ->getResult()
        ;

        foreach ($types as $type) {
            $source = new FinancingSource();
            $source->setType($type);
            $application->addFinancingSource($source);
        }
    }
}
