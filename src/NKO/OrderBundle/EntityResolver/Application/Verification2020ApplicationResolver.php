<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Application\Verification\Application2020\Application;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Document\FileWithFields;
use NKO\OrderBundle\Entity\Document\FileWithTitle;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;

class Verification2020ApplicationResolver extends DefaultApplicationResolver
{
    public function createAssociations(BaseApplication $object)
    {
        /** @var $object Application*/
        $object->addSiteLink(new SiteLink());
        $object->addSocialNetworkLink(new SocialNetworkLink());
        $object->addMeasurementTool(new FileWithTitle());
        $object->addResultConfirmingDocument(new FileWithFields());
        $object->addPerformanceConfirmingDocument(new FileWithTitle());
        $object->addPracticeRegulationDocument(new FileWithTitle());
        $object->addResultDocument(new FileWithTitle());
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        /** @var $object Application*/
        $object->setReadyToSent(false);
        $object->setIsSend(false);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        /** @var $object Application*/
        $request = $params['request'];

        $object->setReadyToSent(true);

        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }
}
