<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BeneficiaryResult;
use NKO\OrderBundle\Entity\Employee;
use NKO\OrderBundle\Entity\EmployeeResult;
use NKO\OrderBundle\Entity\Measure;
use NKO\OrderBundle\Entity\Risk;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\Publication;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;

class KNS2018ApplicationResolver extends ApplicationResolverDecorator
{
    public function createAssociations(BaseApplication $application)
    {
        $application->addSocialNetworkLink(new SocialNetworkLink());
        $application->addSiteLink(new SiteLink());
        $application->addProject(new Project());
        $application->addPublication(new Publication());
        $application->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
        $application->addEmployee(new Employee());
        $application->addMeasure(new Measure());
        $application->addEmployeeResult(new EmployeeResult());
        $application->addBeneficiaryResult(new BeneficiaryResult());
        $application->addRisk(new Risk());
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        if (count($form['signedAgreement']->getErrors())) {
            $object->setSignedAgreement(null);
        }
        if (count($form['subjectRegulation']->getErrors())) {
            $object->getSubjectRegulation(null);
        }
        if (count($form['authorityHead']->getErrors())) {
            $object->setAuthorityHead(null);
        }
        if (count($form['anotherHead']->getErrors())) {
            $object->setAnotherHead(null);
        }
        parent::afterSubmitEdit($object, $form, $params);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $object->setSignedAgreement($form['signedAgreement']->getData());
        $object->setSubjectRegulation($form['subjectRegulation']->getData());
        $object->setAuthorityHead($form['authorityHead']->getData());
        $object->setAnotherHead($form['anotherHead']->getData());
        parent::afterSubmitValidEdit($object, $form, $params);
    }
}
