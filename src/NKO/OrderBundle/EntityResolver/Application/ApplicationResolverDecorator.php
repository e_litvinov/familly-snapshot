<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class ApplicationResolverDecorator extends DefaultApplicationResolver
{
    protected $applicationResolver;

    public function __construct(ContainerInterface $container, $applicationResolver, FlashBag $flashBag)
    {
        $this->applicationResolver = $applicationResolver;
        parent::__construct($container, $flashBag);
    }
    public function initCreate($object, $request, $params = [])
    {
        return parent::initCreate($object, $request, $params);
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        $this->applicationResolver->afterSubmitEdit($object, $form, $params);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $this->applicationResolver->afterSubmitValidEdit($object, $form, $params);
    }

    public function createAssociations(BaseApplication $object)
    {
        $this->applicationResolver->createAssociations($object);
    }
}
