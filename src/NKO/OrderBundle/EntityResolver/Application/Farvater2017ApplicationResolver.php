<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/29/18
 * Time: 3:17 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;

class Farvater2017ApplicationResolver extends DefaultApplicationResolver
{
    public function afterSubmitEdit($object, $form, $params = [])
    {
        if (count($form['regulation']->getErrors())) {
            $object->setRegulation(null);
        }
        if (count($form['projectBudget']->getErrors())) {
            $object->setProjectBudget(null);
        }
        if (count($form['bankLetterExistenceAccount']->getErrors())) {
            $object->setBankLetterExistenceAccount(null);
        }
        if (count($form['processingPersonalDataConsent']->getErrors())) {
            $object->setProcessingPersonalDataConsent(null);
        }
        if (count($form['confirmingAuthorityPersonDocument']->getErrors())) {
            $object->setConfirmingAuthorityPersonDocument(null);
        }
        if (count($form['confirmingLegalStatusEntityDocument']->getErrors())) {
            $object->setConfirmingLegalStatusEntityDocument(null);
        }
        if (count($form['latestAnnualReport']->getErrors())) {
            $object->setLatestAnnualReport(null);
        }
        if (count($form['ministryJusticeReport']->getErrors())) {
            $object->setMinistryJusticeReport(null);
        }

        $object->setReadyToSent(false);
        $object->setIsSend(false);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $request = $params['request'];
        $object->setReadyToSent(true);
        $object->setRegulation($form['regulation']->getData());
        $object->setProjectBudget($form['projectBudget']->getData());
        $object->setBankLetterExistenceAccount($form['bankLetterExistenceAccount']->getData());
        $object->setProcessingPersonalDataConsent($form['processingPersonalDataConsent']->getData());
        $object->setConfirmingAuthorityPersonDocument($form['confirmingAuthorityPersonDocument']->getData());
        $object->setConfirmingLegalStatusEntityDocument($form['confirmingLegalStatusEntityDocument']->getData());
        $object->setLatestAnnualReport($form['latestAnnualReport']->getData());
        $object->setMinistryJusticeReport($form['ministryJusticeReport']->getData());

        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }
}
