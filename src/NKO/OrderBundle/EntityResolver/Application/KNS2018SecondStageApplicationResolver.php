<?php

namespace NKO\OrderBundle\EntityResolver\Application;

class KNS2018SecondStageApplicationResolver extends ApplicationResolverDecorator
{
    public function initCreate($object, $request, $params = [])
    {
        return $this->applicationResolver->initCreate($object, $request, $params);
    }
}
