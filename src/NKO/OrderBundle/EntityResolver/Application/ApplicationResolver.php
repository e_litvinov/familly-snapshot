<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/24/18
 * Time: 6:30 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;


class ApplicationResolver extends DefaultApplicationResolver
{
    public function afterSubmitCreate($object, $form, $params = [])
    {
        $user = $params['user'];
        if(count($form['regulation']->getErrors()))
        {
            $object->setRegulation(null);
        }
        if(count($form['budget']->getErrors()))
        {
            $object->setBudget(null);
        }
        $object->setReadyToSent(false);
        $object->setIsSend(false);
        if (get_class($user) == 'NKO\UserBundle\Entity\NKOUser')
        {
            $object->setAuthor($user);
            $object->setPSRN($user->getPsrn());
        }
    }

    public function afterSubmitValidCreate($object, $form, $params = [])
    {
        $object->setReadyToSent(true);
    }

    public function initEdit($object, $request, $params = [])
    {
        //@todo Убрать после выяснения причин исчезновения полей
        if(count($object->getProjectResults()) < 3){
            $object->refreshProjectResults();
        }
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        $user = $params['user'];
        if (count($form['regulation']->getErrors())) {
            $object->setRegulation(null);
        }
        if (count($form['organizationCreationResolution']->getErrors())) {
            $object->setOrganizationCreationResolution(null);
        }
        if (count($form['budget']->getErrors())) {
            $object->setBudget(null);
        }
        $object->setReadyToSent(false);
        $object->setIsSend(false);
        if (get_class($user) == 'NKO\UserBundle\Entity\NKOUser') {
            $object->setAuthor($user);
            $object->setPSRN($user->getPsrn());
        }
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $object->setReadyToSent(true);
        $object->setRegulation($form['regulation']->getData());
        $object->setOrganizationCreationResolution($form['organizationCreationResolution']->getData());
        $object->setBudget($form['budget']->getData());
    }
}