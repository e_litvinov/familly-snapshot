<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/1/18
 * Time: 4:12 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class DefaultApplicationResolver
{
    protected $container;
    protected $flashBag;

    public function __construct(ContainerInterface $container, FlashBag $flashBag)
    {
        $this->container = $container;
        $this->flashBag = $flashBag;
    }

    public function initCreate($object, $request, $params = [])
    {
        $user = $params['user'];
        $competition_id = $request->get('competition');
        $competition = $this->getContainer()->get('Doctrine')->getRepository(Competition::class)->find($competition_id);
        $object->setCompetition($competition);
        $this->createAssociations($object);

        if ($user instanceof NKOUser) {
            $object->setAuthor($user);
            return true;
        }

        return false;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function setLinkedApplicationHistory(BaseApplication $object)
    {
        $applicationHistory = $this->container->get('nko_order.resolver.related_competition_resolver')->getApplicationHistory($object);
        if (!$applicationHistory) {
            return false;
        }
        $object->setLinkedApplicationHistory($applicationHistory);
        return true;
    }

    public function createAssociations(BaseApplication $object)
    {
    }

    public function afterSubmitInvalidCreate($object, $form, $params = [])
    {
    }

    public function afterSubmitCreate($object, $form, $params = [])
    {
        $this->logAction($object, 'Добавление заявки');
    }

    public function afterSubmitValidCreate($object, $form, $params = [])
    {
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
    }

    public function logSubmitEdit($object, $form, $params = [])
    {
        $nameOfAction = 'Сохранение заявки пользователем';

        $request = $params['request'];
        if ($request->get('send') == "sendButton" && $form->isValid()) {
            $nameOfAction = 'Отправка заявки';
        }

        $this->logAction($object, $nameOfAction);
    }

    public function initEdit($object, $request, $params = [])
    {
    }

    public function afterSubmitInvalidEdit($object, $form, $params = [])
    {
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
    }

    public function submitAlternative(BaseApplication $object, $form, $params = [])
    {
        $message = $object->getIsAutosaved() ? 'Загружена автосохраненная версия' : 'Загружена версия, сохраненная вручную';

        $this->flashBag->add('sonata_flash_info', $message);

        $request = key_exists('request', $params) ?  $params['request'] : null;
        if ($request &&  parse_url($request->headers->get('referer'))['path'] != parse_url($request->getUri())['path']) {
            $this->logAction($object, 'Открытие заявки в редакторе');
        }
    }

    public function preDeleteAction($object, $form, $params = [])
    {
        $this->logAction($object, 'Удаление заявки');
    }

    protected function logAction($object, $nameOfAction)
    {
        $service = $this->container->get('NKO\LoggerBundle\Loggers\ApplicationLogger');
        $service->logAction($nameOfAction, $object);
    }
}
