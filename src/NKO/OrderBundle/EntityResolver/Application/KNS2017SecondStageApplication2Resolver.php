<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Risk;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\Publication;
use NKO\OrderBundle\Entity\Farvater2017\ProjectMember;
use NKO\OrderBundle\Entity\Farvater2017\ProjectPartner;
use NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\BaseApplication;

class KNS2017SecondStageApplication2Resolver extends DefaultApplicationResolver
{
    public function initCreate($object, $request, $params = [])
    {
        if (parent::initCreate($object, $request, $params)) {
            $user = $params['user'];
            $this->container->get(get_class($object).'\EffectivenessLoader')->load($object);
            if ($user instanceof NKOUser) {
                $object->setName($user->getNkoName());
                return true;
            }
        }
        return false;
    }

    public function createAssociations(BaseApplication $application)
    {
        $application->addSocialNetworkLink(new SocialNetworkLink());
        $application->addSiteLink(new SiteLink());
        $application->addProject(new Project());
        $application->addPublication(new Publication());
        $application->addProjectMember(new ProjectMember());
        $application->addProjectPartner(new ProjectPartner());
        $application->addPracticeImplementationActivity(new PracticeImplementationActivity());
        $application->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
        $application->addIndividualSocialResult(new Result());
        $application->addBeneficiaryProblem(new Problem());
        $application->addRisk(new Risk());
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        $object->setReadyToSent(false);
        $object->setIsSend(false);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $object->setReadyToSent(true);
        $request = $params['request'];
        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }
}
