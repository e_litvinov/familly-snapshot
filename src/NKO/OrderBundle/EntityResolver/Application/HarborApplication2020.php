<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Application\Harbor\Application2020\Application;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\Publication;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;

class HarborApplication2020 extends DefaultApplicationResolver
{
    public function createAssociations(BaseApplication $object)
    {
        /** @var $object Application*/
        $object->addSiteLink(new SiteLink());
        $object->addSocialNetworkLink(new SocialNetworkLink());
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        /** @var $object Application*/
        $object->setReadyToSent(false);
        $object->setIsSend(false);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        /** @var $object Application*/
        $request = $params['request'];

        $object->setReadyToSent(true);

        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }
}
