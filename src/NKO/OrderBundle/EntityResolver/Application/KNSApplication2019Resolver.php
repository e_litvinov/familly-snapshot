<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\PropertyAccess\PropertyAccess;

class KNSApplication2019Resolver extends ApplicationResolverDecorator
{
    const COLLECTIONS_WITH_DEFAULT_VALUES = [
        'getBeneficiaryResults', 'getProjectResults', 'getEmployeeResults'
    ];

    public function createAssociations(BaseApplication $object)
    {
        parent::createAssociations($object);
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach (self::COLLECTIONS_WITH_DEFAULT_VALUES as $method) {
            $results = $accessor->getValue($object, $method);
            foreach ($results as $result) {
                $result->setTargetValue(0);
                $result->setApproximateTargetValue(0);
            }
        }
    }
}
