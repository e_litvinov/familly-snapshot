<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/13/18
 * Time: 10:54 AM
 */

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Result;
use NKO\OrderBundle\Entity\Risk;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication;
use NKO\OrderBundle\Loader\Application\Farvater\Application2018\SocialResultLoader;
use NKO\OrderBundle\Loader\Application\Farvater\Application2018\TargetGroupLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadActivity;
use NKO\OrderBundle\Entity\Farvater2017\MonitoringResultsActivity;
use NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity;
use NKO\OrderBundle\Entity\Farvater2017\ProjectPartner;
use NKO\OrderBundle\Entity\Farvater2017\ProjectMember;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\IntroductionIndex;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;

class Farvater2018ApplicationResolver extends ApplicationResolverDecorator
{
    const BENEFICIARY_PROBLEMS = 'beneficiaryProblems';

    /**
     * @var TargetGroupLoader $targetGroupLoader
     */
    private $targetGroupLoader;

    /**
     * @var SocialResultLoader $socialResultLoader
     */
    private $socialResultLoader;

    public function __construct(
        ContainerInterface $container,
        FlashBag $flashBag,
        TargetGroupLoader $targetGroupLoader,
        SocialResultLoader $socialResultLoader,
        $applicationResolver
    ) {
        $this->targetGroupLoader = $targetGroupLoader;
        $this->socialResultLoader = $socialResultLoader;

        parent::__construct($container, $applicationResolver, $flashBag);
    }

    public function initCreate($object, $request, $params = [])
    {
        if (parent::initCreate($object, $request, $params)) {
            if (!$this->setLinkedApplicationHistory($object)) {
                $this->flashBag->add('sonata_flash_error', 'no_linked_application');
                return false;
            }

            $relatedApplication = unserialize($object->getLinkedApplicationHistory()->getData());
            $this->targetGroupLoader->load($object, $relatedApplication);
            $this->socialResultLoader->load($object, $relatedApplication);

            return true;
        }

        return false;
    }

    public function createAssociations(BaseApplication $application)
    {
        $application->addOtherSpecialistTargetGroup(new OtherBeneficiaryGroup());
        $application->addSpecialistProblem(new Problem());
        $application->addRealizationPracticeApplication(new DoubleContextApplication());
        $application->addSpreadPracticeApplication(new DoubleContextApplication());
        $application->addRisePotentialApplication(new DoubleContextApplication());
        $application->addExperienceItem(new ExpectedResult());
        $application->addExperienceEtcItem(new ExpectedResult());
        $application->addProcessItem(new ExpectedResult());
        $application->addProcessEtcItem(new ExpectedResult());
        $application->addQualificationItem(new ExpectedResult());
        $application->addQualificationEtcItem(new ExpectedResult());
        $application->addResourceBlockItem(new ExpectedResult());
        $application->addResourceBlockEtcItem(new ExpectedResult());
        $application->addRisk(new Risk());
        $application->addRealizationPractice(new Risk());
        $application->addSpreadPractice(new Risk());
        $application->addProjectSocialResult(new Result());
        $application->addProjectSocialIndividualResult(new Result());
        $application->addProjectDirectResult(new Result());
        $application->addProjectDirectIndividualResult(new Result());
        $application->addPracticeSocialResult(new Result());
        $application->addPracticeDirectResult(new Result());
        $application->addPracticeDirectIndividualResult(new Result());
        $application->addPracticeImplementationActivity(new PracticeImplementationActivity());
        $application->addPracticeSpreadActivity(new PracticeSpreadActivity());
        $application->addMonitoringResultsActivity(new MonitoringResultsActivity());
        $application->addProjectPartner(new ProjectPartner());
        $application->addProjectMember(new ProjectMember());

        $introductionIndex = new IntroductionIndex();
        $introductionIndex->setIndexName(Farvater2018ApplicationUtils::INTRODUCTION_INDEX);
        $application->addIntroductionPractice($introductionIndex);
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        if (count($form['authorityHead']->getErrors())) {
            $object->setAuthorityHead(null);
        }
        parent::afterSubmitEdit($object, $form, $params);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $object->setAuthorityHead($form['authorityHead']->getData());
        parent::afterSubmitValidEdit($object, $form, $params);
    }
}
