<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/24/18
 * Time: 6:30 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Application\Continuation\ContextApplication;
use NKO\OrderBundle\Entity\Application\Continuation\DoubleContextApplication;
use NKO\OrderBundle\Entity\Application\Continuation\ExpectedResult;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Effectiveness;
use NKO\OrderBundle\Entity\Application\Continuation\ProjectPartner;
use NKO\OrderBundle\Entity\Application\Continuation\Team;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Application\Continuation\Resource;
use NKO\OrderBundle\Entity\BaseApplication;

class ContinuationApplicationResolver extends DefaultApplicationResolver
{
    public function initCreate($object, $request, $params = [])
    {
        if (!parent::initCreate($object, $request, $params)) {
            return false;
        }
        if (!$this->setLinkedApplicationHistory($object)) {
            $this->flashBag->add('sonata_flash_error', 'no_linked_application');
            return false;
        }

        $this->container->get(get_class($object).'\EffectivenessLoader')->load($object);
        return true;
    }

    public function afterSubmitEdit($object, $form, $params = [])
    {
        $object->setReadyToSent(false);
        $object->setIsSend(false);
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $request = $params['request'];
        $object->setReadyToSent(true);
        if ($request->get('send') == "sendButton") {
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }

    public function createAssociations(BaseApplication $object)
    {
        $object->addSustainabilityPracticeApplication(new ContextApplication());
        $object->addSustainabilitySpreadApplication(new ContextApplication());
        $object->addSustainabilityMonitoringApplication(new ContextApplication());
        $object->addRealizationPracticeApplication(new DoubleContextApplication());
        $object->addSpreadPracticeApplication(new DoubleContextApplication());
        $object->addRisePotentialApplication(new DoubleContextApplication());
        $object->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
        $object->addBeneficiaryProblem(new Problem());
        $object->addExpectedResult(new DirectResult());
        $object->addPracticeResult(new DirectResult());
        $object->addResourceItem(new Resource());
        $object->addResourceEtcItem(new Resource());
        $object->addEffectivenessDisseminationEtcItem(new Effectiveness());
        $object->addEffectivenessImplementationEtcItem(new Effectiveness());
        $object->addExperienceItem(new ExpectedResult());
        $object->addExperienceEtcItem(new ExpectedResult());
        $object->addProcessItem(new ExpectedResult());
        $object->addProcessEtcItem(new ExpectedResult());
        $object->addQualificationItem(new ExpectedResult());
        $object->addQualificationEtcItem(new ExpectedResult());
        $object->addResourceBlockItem(new ExpectedResult());
        $object->addResourceBlockEtcItem(new ExpectedResult());
        $object->addProjectTeam(new Team());
        $object->addProjectContPartner(new ProjectPartner());
        $object->addOtherSpecialistTargetGroup(new OtherBeneficiaryGroup());
        $object->addSpecialistProblem(new Problem());
    }
}
