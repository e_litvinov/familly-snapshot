<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application;
use NKO\OrderBundle\Entity\Farvater2017\OrganizationResource;
use NKO\OrderBundle\Entity\Farvater2017\ProjectMember;
use NKO\OrderBundle\Entity\Farvater2017\ProjectPartner;
use NKO\OrderBundle\Entity\Project;
use NKO\OrderBundle\Entity\Publication;
use NKO\OrderBundle\Entity\Risk;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\SocialNetworkLink;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Result;
use NKO\OrderBundle\Entity\BriefApplication2017\OtherBeneficiaryGroup;

class ContinuationKNSApplicationResolver extends ApplicationResolverDecorator
{
    public function __construct(ContainerInterface $container, $applicationResolver, FlashBag $flashBag)
    {
        parent::__construct($container, $applicationResolver, $flashBag);
    }

    public function initCreate($object, $request, $params = [])
    {
        if (!parent::initCreate($object, $request, $params)) {
            return false;
        }

        if ($object->getCompetition()->getRelatedCompetition() && !$this->setLinkedApplicationHistory($object)) {
            $this->flashBag->add('sonata_flash_error', 'no_linked_application');
            return false;
        }

        $this->container->get(get_class($object).'\EffectivenessLoader')->load($object);

        if ($object->getCompetition()->getRelatedCompetition()) {
            $this->container->get('NKO\OrderBundle\Loader\Loader')->load($object, unserialize($object->getLinkedApplicationHistory()->getData()));
        } else {
            $this->addAssociations($object);
        }

        return true;
    }

    public function createAssociations(BaseApplication $object)
    {
        /** @var $object Application */
        $object->addPracticeImplementationActivity(new PracticeImplementationActivity());
        $object->addBeneficiaryProblem(new Problem());
        $object->addIndividualSocialResult(new Result());
        $object->addOtherBeneficiaryGroup(new OtherBeneficiaryGroup());
    }

    private function addAssociations(BaseApplication $object)
    {
        /** @var $object Application */
        $object->addProject(new Project());
        $object->addPublication(new Publication());
        $object->addRisk(new Risk());
        $object->addProjectMember(new ProjectMember());
        $object->addProjectPartner(new ProjectPartner());
        $object->addSiteLink(new SiteLink());
        $object->addSocialNetworkLink(new SocialNetworkLink());
        $object->preOrganizationResources();
    }
}
