<?php

namespace NKO\OrderBundle\EntityResolver\Application;

use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContinuationSecondStageApplicationResolver extends ApplicationResolverDecorator
{
    public function __construct(ContainerInterface $container, $applicationResolver, FlashBag $flashBag)
    {
        parent::__construct($container, $applicationResolver, $flashBag);
    }

    public function initCreate($object, $request, $params = [])
    {
        if (!parent::initCreate($object, $request, $params)) {
            return false;
        }

        if (!$this->setLinkedApplicationHistory($object)) {
            $this->flashBag->add('sonata_flash_error', 'no_linked_application');
            return false;
        }

        $this->container->get('NKO\OrderBundle\Loader\Loader')->load($object, unserialize($object->getLinkedApplicationHistory()->getData()));
        return true;
    }

    public function createAssociations(BaseApplication $object)
    {
        $object->addTerritory(new Territory());
        parent::createAssociations($object);
    }
}
