<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/29/18
 * Time: 2:43 PM
 */

namespace NKO\OrderBundle\EntityResolver\Application;


class BriefApplicationResolver extends DefaultApplicationResolver
{
    public function afterSubmitEdit($object, $form, $params = [])
    {
        $user = $params['user'];
        if (count($form['regulation']->getErrors())) {
            $object->setRegulation(null);
        }
        if (count($form['authorityDirectorDocument']->getErrors())) {
            $object->setAuthorityDirectorDocument(null);
        }
        if (count($form['organizationHeadConsent']->getErrors())) {
            $object->setOrganizationHeadConsent(null);
        }
        if (count($form['projectHeadConsent']->getErrors())) {
            $object->setProjectHeadConsent(null);
        }
        if (count($form['accountantHeadConsent']->getErrors())) {
            $object->setAccountantHeadConsent(null);
        }
        if (count($form['sendingApplicationPersonConsent']->getErrors())) {
            $object->setSendingApplicationPersonConsent(null);
        }

        $object->setReadyToSent(false);
        $object->setIsSend(false);
        if (get_class($user) == 'NKO\UserBundle\Entity\NKOUser') {
            $object->setAuthor($user);
            $object->setPSRN($user->getPsrn());
        }
    }

    public function afterSubmitValidEdit($object, $form, $params = [])
    {
        $request = $params['request'];

        $object->setReadyToSent(true);
        $object->setRegulation($form['regulation']->getData());
        $object->setAccountantHeadConsent($form['accountantHeadConsent']->getData());
        $object->setProjectHeadConsent($form['projectHeadConsent']->getData());
        $object->setOrganizationHeadConsent($form['organizationHeadConsent']->getData());
        $object->setSendingApplicationPersonConsent($form['sendingApplicationPersonConsent']->getData());
        $object->setAuthorityDirectorDocument($form['authorityDirectorDocument']->getData());
        if($request->get('send') == "sendButton"){
            $object->setIsFirstTimeSent(true);
            $object->setIsSend(true);
        }
    }
}