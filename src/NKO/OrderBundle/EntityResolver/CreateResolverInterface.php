<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/1/18
 * Time: 4:27 PM
 */

namespace NKO\OrderBundle\EntityResolver;


interface CreateResolverInterface
{
    public function initCreate($object, $request, $params = []);

    public function afterSubmitInvalidCreate($object, $form, $params = []);

    public function afterSubmitCreate($object, $form, $params = []);

    public function afterSubmitValidCreate($object, $form, $params = []);
}