<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/1/18
 * Time: 4:29 PM
 */

namespace NKO\OrderBundle\EntityResolver;


interface EditResolverInterface
{
    public function afterSubmitEdit($object, $form, $params = []);

    public function initEdit($object, $request, $params = []);

    public function afterSubmitInvalidEdit($object, $form, $params = []);

    public function afterSubmitValidEdit($object, $form, $params = []);

}