<?php

namespace NKO\OrderBundle\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;
use NKO\UserBundle\Entity\NKOUser;

class ReportListVoter extends Voter
{
    /**
     * @var EntityManager
     */
    protected $em;

    const CREATE = 'ROLE_SONATA_ADMIN_NKO_ORDER_REPORT_LIST';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE))) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $current_user = $token->getUser();

        return $this->canCreate( $current_user);
    }


    private function canCreate($current_user)
    {
        $current_user_application_history = $this->em
            ->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findByCurrentUserLastCreatedAt($current_user);

        $result = $this->em
            ->getRepository('NKOOrderBundle:Report')
            ->findBy(array('applicationHistory' => $current_user_application_history));

        if(!($current_user instanceof NKOUser)
            || ($current_user instanceof NKOUser
                     && $current_user_application_history
                     && $current_user_application_history->getContract()
                     && $result)
            || ($result == null && $current_user_application_history != null
                    && $current_user_application_history->getContract() != null)){

            return true;
        }

        return false;
    }
}