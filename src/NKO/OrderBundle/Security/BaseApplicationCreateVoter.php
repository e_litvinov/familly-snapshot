<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/28/17
 * Time: 1:29 PM
 */

namespace NKO\OrderBundle\Security;

use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;

class BaseApplicationCreateVoter extends Voter
{
    const CREATE_BASE_APPLICATION  = 'ROLE_SONATA_ADMIN_NKO_ORDER_BASE_APPLICATION_CREATE';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE_BASE_APPLICATION))) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $current_user = $token->getUser();

        return $this->canCreate($current_user);
    }

    private function canCreate($current_user)
    {
        $session = new Session();
        if ($session->get('competition_id')
            && array_key_exists('value', $session->get('competition_id'))
            && $session->get('competition_id')['value']) {
            /** @var Competition $competition */
            $competition = $this->em->getRepository("NKOOrderBundle:Competition")
                ->find($session->get('competition_id')['value']);

            $competitionRepository = $this->em->getRepository("NKOOrderBundle:Competition");

            $notAllowedByReports = !$competitionRepository->isCompetitionAllowedByReports($competition, $current_user);

            if (!$competition ||
                !$competitionRepository->isCurrentCompetition($competition) ||
                $notAllowedByReports
            ) {
                return false;
            }

            if ($competition->isAllowedToUSer($current_user)) {
                $result = $this
                    ->em
                    ->getRepository($competition->getApplicationClass())
                    ->findBy(array('author'=>$current_user, 'competition' => $competition));
                if ($result == null) {
                    return true;
                }
            }
        } else {
            $availiableCompetition = $this->em->getRepository('NKOOrderBundle:Competition')
                ->findCountAvailiableUserCompetition($current_user);

            if ($availiableCompetition) {
                return true;
            }
        }

        return false;
    }
}
