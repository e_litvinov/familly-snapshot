<?php

namespace NKO\OrderBundle\Security;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Admin\BaseApplicationAdmin;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Competition;
use NKO\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ApplicationEditVoter extends Voter
{
    /** @var EntityManager $em */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        return is_subclass_of($subject, BaseApplication::class);
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if ($currentUser->hasRole('ROLE_EMPLOYEE_ADMIN') ||
            $currentUser->hasRole('ROLE_SUPER_ADMIN') ||
            $currentUser->hasRole('ROLE_MODERATOR')
        ) {
            return true;
        }

        return $this->checkAccessForEdit($subject);
    }

    /**
     * @param BaseApplication $object
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function checkAccessForEdit(BaseApplication $object)
    {
        $subject = $object;
        $author = $subject->getAuthor();

        /**
         * @var Competition $competition
         */
        $competition = $subject->getCompetition();

        if ($subject->getWinner() && $competition->isEditableByWinner() && !$subject->getIsAccepted()) {
            return true;
        }

        return $this->checkAccessForUser($competition, $author);
    }

    /**
     * @param Competition $competition
     * @param $author
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function checkAccessForUser(Competition $competition, $author)
    {
        $em = $this->em;
        $competitionRepository = $em->getRepository(Competition::class);

        if (!$competitionRepository->isCurrentCompetition($competition)) {
            return false;
        }

        $competition->getOrganizations()->initialize();
        $allowedByReports = $competitionRepository->isCompetitionAllowedByReports($competition, $author);
        $allowedToOrganization = $competition->isAllowedToUSer($author);

        return ($allowedByReports && $allowedToOrganization);
    }
}
