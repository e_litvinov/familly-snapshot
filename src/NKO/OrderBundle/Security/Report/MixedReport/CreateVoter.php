<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 10/23/18
 * Time: 2:19 PM
 */

namespace NKO\OrderBundle\Security\Report\MixedReport;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Admin\Report\MixedReport\KNS\ReportAdmin;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CreateVoter extends Voter
{
    const CREATE = [
        'ROLE_NKO_ORDER_ADMIN_REPORT_MIXED_REPORT_KNS_REPORT_CREATE',
        'ROLE_NKO_ORDER_ADMIN_REPORT_MIXED_REPORT_REPORT2019_KNS_REPORT_CREATE',
        'ROLE_NKO_ORDER_ADMIN_REPORT_MIXED_REPORT_REPORT2020_KNS_REPORT_CREATE',
    ];

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, self::CREATE)) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if (!$currentUser->hasRole('ROLE_NKO')) {
            return false;
        }

        if (is_array($subject)) {
            return $this->canCreate($currentUser, $subject['reportFormId'], $subject['periodId']);
        }

        if ($subject instanceof ReportAdmin) {
            return $this->canCreate(
                $currentUser,
                $subject->getSubject()->getReportForm()->getId(),
                $subject->getSubject()->getPeriod()->getId()
            );
        }
        return false;
    }

    public function canCreate($currentUser, $reportFormId, $periodId)
    {
        $reportForm = $this->em->getRepository(ReportForm::class)->find($reportFormId);

        $applicationHistory = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
            'isSpread' => true,
            'author' => $currentUser,
            'competition' => $reportForm->getCompetition()
        ]);

        if (!$applicationHistory) {
            return false;
        }

        $report = $this->em->getRepository(BaseReport::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId,
            'period' => $periodId
        ]);

        if ($report) {
            return false;
        }

        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId
        ]);

        if (!$grant) {
            return false;
        }

        if (!$grant->getSumGrant()) {
            return false;
        }

        $template = $this->em->getRepository(ReportTemplate::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId
        ]);

        if (!$template) {
            return false;
        }

        return true;
    }
}
