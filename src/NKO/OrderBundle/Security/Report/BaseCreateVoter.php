<?php

namespace NKO\OrderBundle\Security\Report;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;

class BaseCreateVoter extends Voter
{
    const CREATE = 'ROLE_SONATA_ADMIN_NKO_ORDER_BASE_REPORT_CREATE';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE))) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();
        if (!$currentUser->hasRole('ROLE_NKO')) {
            return false;
        }

        return true;
    }
}
