<?php


namespace NKO\OrderBundle\Security\Report\FinanceReport;

use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceReportInterface;
use NKO\UserBundle\Entity\User;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EditVoter extends Voter
{
    const EDIT_FINANCE_REPORT  = 'ROLE_SONATA_ADMIN_NKO_ORDER_FINANCE_REPORT_EDIT';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT_FINANCE_REPORT])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (!$user->hasRole('ROLE_NKO') && !$user->hasRole('ROLE_SUPER_ADMIN')) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT_FINANCE_REPORT:
                return $this->canEdit($subject, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(BaseReport $object, User $user)
    {
        foreach ($object->getAuthor()->getReports() as $report) {
            if ($object instanceof FinanceReportInterface) {
                if ($report->getUserBlockedFinanceReport() != 0 &&
                    $user->getId() !== $object->getUserBlockedFinanceReport()) {
                    return false;
                }
            }
        }

        return true;
    }
}
