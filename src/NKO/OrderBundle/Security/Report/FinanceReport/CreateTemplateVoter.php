<?php

namespace NKO\OrderBundle\Security\Report\FinanceReport;

use NKO\OrderBundle\Admin\Report\FinanceReport\Report2018\ReportTemplateAdmin;
use NKO\OrderBundle\Admin\Report\FinanceReport\ReportAdmin;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;


class CreateTemplateVoter extends Voter
{
    const CREATE = 'ROLE_NKO_ORDER_ADMIN_REPORT_FINANCE_REPORT_REPORT_2018_REPORT_TEMPLATE_CREATE';

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::CREATE])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if (!$currentUser->hasRole('ROLE_NKO')) {
            return false;
        }

        if (is_array($subject)) {
            return $this->canCreate($subject['report_form_id'], $subject['app_history_id']);
        }

        if ($subject instanceof ReportTemplateAdmin) {
            return true;
        }

        return false;
    }


    private function canCreate($reportFormId, $applicationHistoryId)
    {
        if (!$applicationHistoryId) {
            return false;
        }

        if (!$reportFormId) {
            return false;
        }

        $reportTemplate = $this->em->getRepository(ReportTemplate::class)->findOneBy([
            'applicationHistory' => $applicationHistoryId,
            'reportForm' => $reportFormId
        ]);

        if ($reportTemplate) {
            return false;
        }

        return true;
    }
}