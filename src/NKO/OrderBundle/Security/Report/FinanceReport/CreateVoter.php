<?php

namespace NKO\OrderBundle\Security\Report\FinanceReport;

use NKO\OrderBundle\Admin\Report\FinanceReport\ReportAdmin;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;

class CreateVoter extends Voter
{
    const CREATE = 'ROLE_NKO_ORDER_ADMIN_REPORT_FINANCE_REPORT_REPORT_CREATE';
    const CREATE_2018 = 'ROLE_NKO_ORDER_ADMIN_REPORT_FINANCE_REPORT_REPORT_2018_REPORT_CREATE';
    const CREATE_2019 = 'ROLE_NKO_ORDER_ADMIN_REPORT_FINANCE_REPORT_REPORT_2019_REPORT_CREATE';

    const ATTRIBUTES = [
        self::CREATE,
        self::CREATE_2018,
        self::CREATE_2019
    ];

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, self::ATTRIBUTES)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if (!$currentUser->hasRole('ROLE_NKO')) {
            return false;
        }
        if (is_array($subject)) {
            return $this->canCreate($currentUser, $subject['reportFormId'], $subject['periodId']);
        }

        if ($subject instanceof ReportAdmin) {
            return $this->canCreate(
                $currentUser,
                $subject->getSubject()->getReportForm()->getId(),
                $subject->getSubject()->getPeriod()->getId()
            );
        }
        return false;
    }


    private function canCreate($currentUser, $reportFormId, $periodId)
    {
        $reportForm = $this->em->getRepository(ReportForm::class)->find($reportFormId);

        $applicationHistory = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
            'isSpread' => true,
            'author' => $currentUser,
            'competition' => $reportForm->getCompetition()
        ]);

        if (!$applicationHistory) {
            return false;
        }

        $report = $this->em->getRepository(BaseReport::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId,
            'period' => $periodId
        ]);
        if ($report) {
            return false;
        }

        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId
        ]);

        if (!$grant) {
            return false;
        }

        if (!$grant->getSumGrant()) {
            return false;
        }

        $template = $this->em->getRepository(ReportTemplate::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId
        ]);

        if (!$template) {
            return false;
        }

        return true;
    }
}
