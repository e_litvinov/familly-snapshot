<?php

namespace NKO\OrderBundle\Security\Report\AnalyticReport;

use NKO\OrderBundle\Admin\ReportAdmin;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;

class CreateVoter extends Voter
{
    const CREATE = 'ROLE_NKO_ORDER_ADMIN_REPORT_ANALYTIC_REPORT_REPORT_CREATE';
    const CREATE_ANALYTIC_REPORT_2018 = 'ROLE_NKO_ORDER_ADMIN_REPORT_ANALYTIC_REPORT_REPORT2018_REPORT_CREATE';
    const CREATE_ANALYTIC_REPORT_2019 = 'ROLE_NKO_ORDER_ADMIN_REPORT_ANALYTIC_REPORT_REPORT2019_REPORT_CREATE';
    const CREATE_ANALYTIC_KNS_REPORT_2018 = 'ROLE_NKO_ORDER_ADMIN_REPORT_ANALYTIC_REPORT_REPORT2018_KNS_REPORT_CREATE';
    const CREATE_MIXED_REPORT_KNS = 'ROLE_NKO_ORDER_ADMIN_REPORT_MIXED_REPORT_KNS_REPORT_CREATE';


    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [
                self::CREATE,
                self::CREATE_ANALYTIC_REPORT_2018,
                self::CREATE_ANALYTIC_REPORT_2019,
                self::CREATE_ANALYTIC_KNS_REPORT_2018,
                self::CREATE_MIXED_REPORT_KNS,
            ])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if (!$currentUser->hasRole('ROLE_NKO')) {
            return false;
        }

        if (is_array($subject)) {
            return $this->canCreate($currentUser, $subject['reportFormId']);
        }

        if ($subject instanceof ReportAdmin) {
            return $this->canCreate($currentUser, $subject->getSubject()->getReportForm()->getId());
        }

        return false;
    }


    private function canCreate($currentUser, $reportFormId)
    {
        $reportForm = $this->em->getRepository(ReportForm::class)->find($reportFormId);

        $applicationHistory = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
            'isSpread' => true,
            'author' => $currentUser,
            'competition' => $reportForm->getCompetition()
        ]);

        if (!$applicationHistory) {
            return false;
        }

        $report = $this->em->getRepository(BaseReport::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId,
        ]);
        if ($report) {
            return false;
        }

        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId
        ]);

        if (!$grant || !$grant->getSumGrant()) {
            return false;
        }

        return true;
    }
}
