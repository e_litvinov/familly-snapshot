<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/1/17
 * Time: 2:14 PM
 */
namespace NKO\OrderBundle\Security\Report\AnalyticReport\KNS;

use NKO\OrderBundle\Admin\ReportAdmin;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\ReportForm;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;

class CreateKNS1Voter extends Voter
{
    const CREATE = 'ROLE_NKO_ORDER_ADMIN_ANALYTIC_REPORT_KNS_1_CREATE';

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE))) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $currentUser = $token->getUser();

        if (!$currentUser->hasRole('ROLE_NKO')) {
            return false;
        }

        if (is_array($subject)) {
            return $this->canCreate($currentUser, $subject['reportFormId']);
        }

        if ($subject instanceof ReportAdmin) {
            return $this->canCreate($currentUser, $subject->getSubject()->getReportForm()->getId());
        }

        return false;
    }


    private function canCreate($currentUser, $reportFormId)
    {
        $reportForm = $this->em->getRepository(ReportForm::class)->find($reportFormId);

        $applicationHistory = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
            'isSend' => true,
            'competition' =>  $reportForm->getCompetition(),
            'author' => $currentUser
        ]);

        if (!$applicationHistory) {
            return false;
        }

        $report = $this->em->getRepository(BaseReport::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId,
        ]);
        if ($report) {
            return false;
        }

        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'applicationHistory' => $applicationHistory,
            'reportForm' => $reportFormId
        ]);

        if (!$grant || !$grant->getSumGrant()) {
            return false;
        }

        return true;
    }
}
