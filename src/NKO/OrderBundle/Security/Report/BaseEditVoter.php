<?php

namespace NKO\OrderBundle\Security\Report;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\UserBundle\Entity\NKOUser;
use NKO\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BaseEditVoter extends Voter
{
    const EDIT  = 'BASE_REPORT_EDIT_VOTER';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, [self::EDIT])) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return true;
        }

        if ($user->hasRole('ROLE_NKO')) {
            return $this->canNKOEdit($subject, $user);
        }

        return false;
    }

    private function canNKOEdit(BaseReport $object, NKOUser $user)
    {
        return $object->getAuthor()->getId() == $user->getId() && !($object->getIsAccepted() || $object->getUnderConsideration());
    }
}
