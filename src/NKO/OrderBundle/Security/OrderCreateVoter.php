<?php

namespace NKO\OrderBundle\Security;

use NKO\OrderBundle\Admin\CompetitionAdmin;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Doctrine\ORM\EntityManager;

class OrderCreateVoter extends Voter
{
    const CREATE_KNS2016_APPLICATION = 'ROLE_SONATA_ADMIN_NKO_ORDER_APPLICATION_CREATE';
    const CREATE_KNS2017_APPLICATION = 'ROLE_SONATA_ADMIN_NKO_ORDER_KNS2017_APPLICATION_CREATE';
    const CREATE_BRIEF_APPLICATION2017 = 'ROLE_SONATA_ADMIN_NKO_ORDER_BRIEF_APPLICATION_CREATE';
    const CREATE_BRIEF_APPLICATION2016 = 'ROLE_SONATA_ADMIN_NKO_ORDER_BRIEF_APPLICATION2016_APPLICATION_CREATE';
    const CREATE_FARVATER_APPLICATION2016 = 'ROLE_SONATA_ADMIN_NKO_ORDER_FARVATER_FARVATER_APPLICATION_CREATE';
    const CREATE_FARVATER_APPLICATION2017 = 'ROLE_NKO_ORDER_ADMIN_FARVATER2017_APPLICATION_CREATE';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(
            self::CREATE_KNS2016_APPLICATION,
            self::CREATE_KNS2017_APPLICATION,
            self::CREATE_BRIEF_APPLICATION2017,
            self::CREATE_BRIEF_APPLICATION2016,
            self::CREATE_FARVATER_APPLICATION2016,

            ))) {
              return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $current_user = $token->getUser();

        if($subject instanceof Competition){

            return $this->canCreate( $current_user, $subject);
        }

        return true;
    }


    private function canCreate($current_user, $competition)
    {

        if(!$competition || !$this->em->getRepository("NKOOrderBundle:Competition")->isCurrentCompetition($competition)){
            return false;
        }

        if((!$competition->getOrganizations()->isEmpty() && $competition->getOrganizations()->contains($current_user))
            || $competition->getOrganizations()->isEmpty()){
            $result = $this
                ->em
                ->getRepository($competition->getApplicationClass())
                ->findBy(array('author'=>$current_user, 'competition' => $competition));
            if($result == null){
                return true;
            }
        }

       return false;
    }




}
