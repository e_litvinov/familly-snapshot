<?php


namespace NKO\OrderBundle\Security;

use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use NKO\OrderBundle\Entity\Application;
use Doctrine\ORM\EntityManager;

class ReportEditVoter extends Voter
{
    const CREATE = 'ROLE_SONATA_ADMIN_NKO_ORDER_REPORT_EDIT';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CREATE))) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $current_user = $token->getUser();

        return $this->canCreate( $current_user);
    }


    private function canCreate($current_user)
    {
        $current_user_application_history = $this->em->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findByCurrentUserLastCreatedAt($current_user);

        if(!$current_user instanceof NKOUser || ($current_user instanceof NKOUser && $current_user_application_history != null && $current_user_application_history->getContract() != null)){

            return true;
        }

        return false;
    }

}