<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application;

class Kns2019ApplicationDocGenerator extends AbstractKNSApplicationDocGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/application_kns2019.docx';
    const VIEW_FILE_NAME = 'KNS_application_2019';

    const SITE_LINKS = 'siteLinks';
    const SOCIAL_NETWORK_LINKS = 'socialNetworkLinks';
    const TRAINEESHIP_FORMAT_NAME = 'traineeshipFormatName';

    private $socialNetworkLinks;
    private $siteLinks;
    /** @var $object Application */
    protected $object;

    protected function setConstants()
    {
        $this->tables = parent::TABLES;

        $this->siteLinks = $this->tables[self::SITE_LINKS];
        unset($this->tables[self::SITE_LINKS]);

        $this->socialNetworkLinks = $this->tables[self::SOCIAL_NETWORK_LINKS];
        unset($this->tables[self::SOCIAL_NETWORK_LINKS]);

        $this->fields = parent::FIELDS;
        $this->fileName = self::FILE_NAME;
        $this->viewFileName = self::VIEW_FILE_NAME;
    }

    protected function fillTemplate()
    {
        //deleting 3.7.5 field
        if (!$this->object->getTraineeshipFormatName()) {
            $this->document->cloneRow(self::TRAINEESHIP_FORMAT_NAME, 0);
        }

        parent::fillTemplate();

        if ($this->object->getSiteLinks()[0]->getLink()) {
            $this->wordTemplateService->fillTables([self::SITE_LINKS => $this->siteLinks]);
        } else {
            foreach ($this->siteLinks as $key => $value) {
                $this->document->setValue($key, '-');
            }
        }
        
        if ($this->object->getSocialNetworkLinks()[0]->getLink()) {
            $this->wordTemplateService->fillTables([self::SOCIAL_NETWORK_LINKS => $this->socialNetworkLinks]);
        } else {
            foreach ($this->socialNetworkLinks as $key => $value) {
                $this->document->setValue($key, '-');
            }
        }
    }
}
