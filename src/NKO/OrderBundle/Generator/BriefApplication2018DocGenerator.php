<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/20/18
 * Time: 1:16 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;
use NKO\OrderBundle\Resolver\CollectionResolver;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class BriefApplication2018DocGenerator extends FileGenerator
{
    const FIELDS = [
        'abbreviation',
        'organizationForm',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingEmeil',
        'dateRegistrationOfOrganization',
        'primaryActivity',
        'mission',
        'countStaffEmployees',
        'countInvolvingEmployees',
        'countVolunteerEmployees',
        'anotherFinancingSource',
        'projectName',
        'priorityDirection',
        'priorityDirectionEtc',
        'briefPracticeDescription',
        'effectivenessFact',
        'effectivenessPlan',
        'isReadyEtc',
        'advantages',
        'disadvantages',
        'practiceDevelopment'
    ];

    const TABLES = [
        'siteLinks' => [
            'site_link_td' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_td' => 'link'
        ],
        'projects' => [
            'project_num_td' => self::AUTO_NUM,
            'project_name_td' => 'projectName',
            'project_date_td' => 'deadline',
            'project_result_td' => 'result',
            'project_partners_td' => 'donorsPartnersAndTheirRole'
        ],
        'publications' => [
            'publication_name_td' => 'publicationName',
            'publication_link_td' => 'link'
        ],
        'financingSources' => [
            'source_type_td' => 'type',
            'source_value_td' => 'value',
        ],
        'practiceRegions' => [
            'region_name_td' => 'name',
        ],
        'otherBeneficiaryGroups' => [
            'group_num_td' => self::AUTO_NUM,
            'group_name_td' => 'name',
        ],
        'beneficiaryProblems' => [
            'problem_num_td' => self::AUTO_NUM,
            'problem_target_group_td' => 'targetGroup',
            'problem_content_td' => 'content',
            'problem_social_result_td' => 'humanizedSocialResults',
            'problem_custom_result_td' => 'customSocialResult',
        ],
        'effectivenessItems' => [
            'eff_result_td' => 'result',
            'eff_indicator_td' => 'indicator',
            'eff_indicator_value_td' => 'indicatorValue',
        ],
        'effectivenessEtcItems' => [
            'eff_etc_result_td' => 'result',
            'eff_etc_indicator_td' => 'customIndicator',
            'eff_etc_indicator_value_td' => 'indicatorValue',
        ],
        'isReady' => [
            'is_ready_td' => 'name',
        ]
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/brief_application_2018_template.docx';
    const VIEW_FILE_NAME = 'Brief_application_2018';
    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'category_name_td';

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * @var CollectionResolver $collectionResolver
     */
    private $collectionResolver;

    public function __construct(
        $rootDir,
        FillWordTemplateService $fillWordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        CollectionResolver $collectionResolver,
        ProxyResolver $proxyResolver
    ) {
        $this->wordTemplateService = $fillWordTemplateService;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->collectionResolver = $collectionResolver;
        $this->proxyResolver = $proxyResolver;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);

        $this->setObject($object);

        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->wordTemplateService->setObject($this->collectionResolver->fetchCollectionByFieldName($this->object, 'financingSources'));
        $this->wordTemplateService->setObject($this->collectionResolver->fetchCollectionByFieldName($this->object, 'effectivenessItems'));

        $this->fillAuthorData();
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME);
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
    }

    private function fillAuthorData()
    {
        $author = $this->getObject()->getAuthor();
        $this->document->setValue('nko_name', $author->getNkoName());
        $this->document->setValue('psrn', $author->getPsrn());
    }
}