<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 22.11.18
 * Time: 11.32
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_Chart;
use PHPExcel_Chart_Axis;
use PHPExcel_Chart_DataSeries;
use PHPExcel_Chart_DataSeriesValues;
use PHPExcel_Chart_Layout;
use PHPExcel_Chart_Legend;
use PHPExcel_Chart_PlotArea;
use PHPExcel_Chart_Title;
use PHPExcel_IOFactory;
use PHPExcel_Style_Border;
use Symfony\Component\HttpFoundation\Response;

class ExcelDiagramGenerator extends FileGenerator
{

    const VIEW_FILE_NAME = 'diagram';

    const START_ROW = 1;
    const START_COLUMN = 0;
    const CONSTANT = 13;
    const START = 3;
    const STRING_OFFSET = 7;

    const FIRST_TAB_NAME = "Сводная таблица";
    const INDICATOR = "Показатель";

    const MAJOR_UNIT_LIMIT = 10;

    private $yAxisLabel;
    private $xAxisLabel;


/**   const TEST = [
        [
            'title' => '1. Количество детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства',
            'values' => [
                2016 => 2,
                2017 => 0,
            ]
        ], [
            'title' => '1. Пункт №2',
            'values' => [
                2017 => 2033,
                2018 => 4334,
                2019 => 1423
            ]
        ],
    ];*/

    /**
     * @var string $rootDir
     */
    private $rootDir;
    private $sheetTitle;

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
    }

    /**
     * @param array $object
     *  $object['results']['title'] - string - name
     *  $object['results']['values'] - array of int - values
     *
     *  $object['types'] - array of type diagrams
     *
     * @return Response
     *
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function generate($object)
    {
        $objectValues = $object['results'];


        $layout = new PHPExcel_Chart_Layout();
        $layout->setShowVal(true);

        $tempFileName = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);

        $tempOutFileName = $tempFileName.'.xlsx';

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        $i = self::START_ROW;
        $j = self::START_COLUMN;

        $dataSeriesLabels = [];
        $dataSeriesValues = [];
        $xAxisTickValues = [];
        $majorUnits = [];
        $this->setChartAttributs($sheet);

        foreach ($objectValues as $index) {
            $values = $index['values'];
            $title = $index['title'];

            $this->setCellStyles($sheet->setCellValueByColumnAndRow($j, $i + 1, $title, true))
                ->getStyle()->getFont()->setBold(true);
            $count = $this->fillIndex($values, $sheet, $j + 1, $i, $rangeLabels, $rangeValues, $majorUnit);

            $majorUnits[] = $majorUnit;
            $dataSeriesLabels[] = $this->generateChartSeries('', PHPExcel_Chart_DataSeriesValues::DATASERIES_TYPE_NUMBER, $count);
            $xAxisTickValues[] = $this->generateChartSeries($rangeLabels, PHPExcel_Chart_DataSeriesValues::DATASERIES_TYPE_STRING, $count);
            $dataSeriesValues[] = $this->generateChartSeries($rangeValues, PHPExcel_Chart_DataSeriesValues::DATASERIES_TYPE_NUMBER, $count);
            $i += self::START;
        }

        $sheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(self::START_COLUMN))->setAutoSize(true);

        foreach ($object['types'] as $index => $type) {
            $sheet = new \PHPExcel_Worksheet(null, $type[4]);
            $objPHPExcel->addSheet($sheet, $index + 1);

            foreach ($objectValues as $key => $indexType) {
                $series = $this->addCHart($dataSeriesLabels[$key], $xAxisTickValues[$key], $dataSeriesValues[$key], $type);
                $plotArea = new PHPExcel_Chart_PlotArea($layout, array($series));

                if ($type[3]) {
                    $legend = new PHPExcel_Chart_Legend($type[3], null, false);
                } else {
                    $legend = null;
                }
                $this->putChart($sheet, $plotArea, $key, $indexType['title'], $legend, $majorUnits[$key]);
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->setIncludeCharts(true);
        $objWriter->save($tempOutFileName);

        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME, ExcelDownloader::EXCEL2007);
    }

    private function fillIndex($array, $sheet, $j, $i, &$rangeLabels, &$rangeValues, &$maxValue)
    {
        $count = 0;
        $row = '$'. (string)($i);
        $rowValue = '$'. (string)($i + 1);
        $rangeLabels = '!$'. PHPExcel_Cell::stringFromColumnIndex($j). $row. ':';
        $rangeValues = '!$'. PHPExcel_Cell::stringFromColumnIndex($j). $rowValue. ':';
        $maxValue = 0;

        foreach ($array as $year => $value) {
            $maxValue = $value > $maxValue ? $value : $maxValue;
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($j, $i, $year, true));
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($j, $i + 1, $value, true));
            $j++;
            $count++;
        }

        $rangeLabels .= '$'. PHPExcel_Cell::stringFromColumnIndex($j - 1). $row;
        $rangeValues .= '$'. PHPExcel_Cell::stringFromColumnIndex($j - 1). $rowValue;

        //Count of columns
        return $count;
    }

    private function generateChartSeries($coordinates, $type, $count)
    {
        if ($coordinates) {
            $source = $this->sheetTitle. $coordinates;
        } else {
            $source = '';
        }

        $series = new PHPExcel_Chart_DataSeriesValues($type, $source, null, $count);

        return [$series];
    }

    private function addCHart($dataSeriesLabels, $xAxisTickValues, $dataSeriesValues, $type)
    {
        return new PHPExcel_Chart_DataSeries(
            $type[0],
            $type[1],
            range(0, count($dataSeriesValues) - 1),
            $dataSeriesLabels,
            $xAxisTickValues,
            $dataSeriesValues,
            $type[2]
        );
    }

    private function putChart($sheet, $plotArea, $key, $title, $legend, $maxValue)
    {
        $const = self::CONSTANT;

        $majorUnit = $maxValue > self::MAJOR_UNIT_LIMIT ? null : 1;

        $axis = new PHPExcel_Chart_Axis();
        $axis->setAxisOptionsProperties('nextTo', null, null, null, null, null, 0, null, $majorUnit);

        $chart = new PHPExcel_Chart(
            'chart1',
            new PHPExcel_Chart_Title($sheet->getTitle()),
            $legend,
            $plotArea,
            true,
            0,
            $this->xAxisLabel,
            $this->yAxisLabel,
            $axis
        );

        $start = (string)(self::START + $key * ($const + self::STRING_OFFSET + self::START));
        $finish = (string)(($key + 1) * ($const + self::STRING_OFFSET + self::START));

        $chart->setTopLeftPosition('A'. $start);
        $chart->setBottomRightPosition('H'. $finish);

        $sheet->setCellValueByColumnAndRow(self::START_COLUMN, $finish + 1, $title, true)
            ->getStyle()->getFont()->setBold(true);

        $sheet->addChart($chart);
    }

    /**
     * @param $cell  PHPExcel_Cell
     *
     * @return PHPExcel_Cell
     * @throws \PHPExcel_Exception
     */
    private function setCellStyles($cell)
    {
        $cell->getStyle()->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $cell->getStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        $cell->getStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $cell->getStyle()->getAlignment()->setWrapText(true);

        return $cell;
    }

    private function setChartAttributs($sheet)
    {
        $sheet->setTitle(self::FIRST_TAB_NAME);
        $this->sheetTitle = "'".self::FIRST_TAB_NAME."'";

        $this->yAxisLabel = new PHPExcel_Chart_Title('Факт');
        $this->xAxisLabel = new PHPExcel_Chart_Title('Год');

        $this->setCellStyles($sheet->setCellValueByColumnAndRow(self::START_COLUMN, self::START_ROW, self::INDICATOR))
            ->getStyle()->getFont()->setBold(true);
    }
}
