<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 4:49 PM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\PdfDownloader;
use NKO\OrderBundle\Entity\Organization;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\TraineeshipTopic;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class KnsReportPdfGenerator extends FileGenerator
{
    const FILE_NAME = 'NKOOrderBundle:CRUD:download_report.html.twig';
    const VIEW_FILE_NAME = 'Kns_report';

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var TwigEngine $twigEngine
     */
    private $twigEngine;

    /**
     * @var FlashBag $flashBag
     */
    private $flashBag;

    /**
     * KnsReportPdfGenerator constructor.
     *
     * @param PdfDownloader $downloader
     * @param EntityManager $entityManager
     * @param TwigEngine $twigEngine
     * @param FlashBag $flashBag
     */
    public function __construct(
        PdfDownloader $downloader,
        EntityManager $entityManager,
        TwigEngine $twigEngine,
        FlashBag $flashBag
    ) {
        $this->downloader = $downloader;
        $this->entityManager = $entityManager;
        $this->twigEngine = $twigEngine;
        $this->flashBag = $flashBag;
    }

    /**
     * @param mixed $object
     * @return bool|\Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function generate($object)
    {
        $this->setObject($object);

        if ($this->getObject()->getTraineeshipTopic()) {
            $this->getObject()->setTraineeshipTopic($this->entityManager->getRepository(TraineeshipTopic::class)->find(
                $this->getObject()->getTraineeshipTopic()->getId()
            ));
        }
        if ($this->getObject()->getOrganization()) {
            $this->getObject()->setOrganization($this->entityManager->getRepository(Organization::class)->find(
                $this->getObject()->getOrganization()->getId()
            ));
        }

        try {
            $html = $this->twigEngine->render(self::FILE_NAME, [
                'report' => $object
            ]);

            return $this->downloader->getStreamedResponse($html, self::VIEW_FILE_NAME);
        } catch (\Twig_Error $error) {
            $this->flashBag->add('sonata_flash_error', 'cannot_render_template');

            return false;
        }
    }
}
