<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/8/18
 * Time: 12:46 PM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use NKO\UserBundle\Entity\NKOUser;

class FillWordTemplateService
{
    /**
     * @var \Symfony\Component\PropertyAccess\PropertyAccessor
     */
    protected $accessor;

    /**
     * @var EntityManager $entityManager
     */
    protected $entityManager;

    protected $document;

    protected $object;

    const AUTO_NUM = 'auto_num';

    public function __construct(
        PropertyAccessor $propertyAccessor,
        EntityManager $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->accessor = $propertyAccessor;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }


    public function fillTables($tables)
    {
        foreach ($tables as $tableName => $value) {
            if ($this->accessor->isReadable($this->object, $tableName)) {
                $this->createTable($tableName, $tables);
            }
        }

        return $this->getDocument();
    }

    private function createTable($table, $tables)
    {
        $collection = $this->accessor->getValue($this->object, $table);
        $countRows = $collection->count();
        if ($countRows == 0) {
            foreach ($tables[$table] as $templateVar => $objAttr) {
                $this->document->setValue("{$templateVar}", " ");
            }
            return;
        }

        $i = 1;
        $isCloneRow = false;
        foreach ($collection as $item) {
            foreach ($tables[$table] as $templateVar => $objAttr) {
                if (!$isCloneRow) {
                    $this->document->cloneRow($templateVar, $countRows);
                    $isCloneRow = true;
                }
                if ($objAttr == self::AUTO_NUM) {
                    $this->document->setValue("{$templateVar}#{$i}", $i);
                } else {
                    $value = $this->accessor->getValue($item, $objAttr);
                    $this->document->setValue("{$templateVar}#{$i}", mb_convert_encoding($this->formatValue($value), 'UTF-8'));
                }
            }
            $i++;
        }
    }

    private function formatValue($value)
    {
        if (is_bool($value)) {
            return $value ? 'да' : 'нет';
        } elseif ($value instanceof \DateTime) {
            return $value->format('d.m.Y');
        } elseif (is_float($value)) {
            return number_format($value, 2, '.', '');
        }

        return (string)$value;
    }

    public function fillTemplate($fields)
    {
        $this->reloadObjectAuthor($this->object);
        foreach ($fields as $variable) {
            $this->document->setValue($variable, $this->getObjectValue($variable));
        }

        return $this->getDocument();
    }

    private function getObjectValue($attribute)
    {
        $value = null;
        if ($this->accessor->isReadable($this->object, $attribute)) {
            if ($this->accessor->getValue($this->object, $attribute) instanceof PersistentCollection) {
                foreach ($this->accessor->getValue($this->object, $attribute) as $item) {
                    $value .= (string)$item . ' ';
                }
            }
            $value = $this->formatValue($this->accessor->getValue($this->object, $attribute));
        }

        return $value;
    }

    public function fillTrainingGrounds($isParent, $templateVariable, $fieldName = 'trainingGrounds')
    {
        $value = '';
        if ($isParent) {
            $grounds = $this->accessor->getValue($this->object, $fieldName);
            foreach ($grounds as $ground) {
                if ($ground->getParent() == null) {
                    $value =  $ground;
                }
            }
        } else {
            $grounds = $this->accessor->getValue($this->object, $fieldName);
            foreach ($grounds as $ground) {
                if ($ground->getParent() != null) {
                    $value =  $ground;
                }
            }
        }
        $this->document->setValue($templateVariable, $this->formatValue($value));
    }

    public function fillPeopleCategoriesTable($templateFieldName, $writeWithParent = null)
    {
        $categories = $this->object->getPeopleCategories();
        $parentIds = [];

        foreach ($categories as $category) {
            if ($category->getParent()) {
                $id = $category->getParent()->getId();
                if (!in_array($id, $parentIds)) {
                    $parentIds[] = $id;
                }
            }
        }
        $categories = [];

        foreach ($this->object->getPeopleCategories() as $category) {
            if (!in_array($category->getId(), $parentIds)) {
                $categories[] = $category;
            }
        }

        $names = [];
        if ($writeWithParent) {
            $names = $this->getNamesWithParentTable($categories);
        } else {
            $names = $this->getNamesWithoutParentTable($categories);
        }

        $this->document->cloneRow($templateFieldName, count($categories));
        $i = 0;
        foreach ($names as $name) {
            $i++;
            $this->document->setValue($templateFieldName . "#" . $i, $name);
        }
    }

    private function getNamesWithParentTable($categories)
    {
        $names = [];

        foreach ($categories as $category) {
            $categoryTree = [];
            $categoryTree[] = $category->getName();

            while ($category->getParent()) {
                $category = $category->getParent();
                $categoryTree[] = $category->getName();
            }
            $names[] = implode('/', array_reverse($categoryTree));
        }
        return $names;
    }

    private function getNamesWithoutParentTable($categories)
    {
        $names = [];

        foreach ($categories as $category) {
            $names[] = $category->getName();
        }

        return $names;
    }

    public function reloadObjectAuthor($object)
    {
        $author = $this->entityManager->getRepository(NKOUser::class)->find($object->getAuthor());
        $object->setAuthor($author);
    }
}
