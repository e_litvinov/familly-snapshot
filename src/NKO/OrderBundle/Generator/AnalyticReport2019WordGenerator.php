<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\GrantConfig;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class AnalyticReport2019WordGenerator extends FileGenerator
{
    private $em;
    private $rootDir;

    const TEMPLATE_PATH = '/../src/NKO/OrderBundle/Resources/templates/analytic_report2019.docx';

    const VIEW_FILE_NAME = 'Report';

    const FIELDS = [
        'author.nko_name',
        'updatedAt',
        'projectName',
        'startDateProject',
        'finishDateProject',
        'directorName',
        'directorPosition',
        'accountantName',
        'accountantPosition',
        'priorityDirection',
        'priorityDirectionEtc',
        'practiceImplementation.purpose',
        'practiceImplementation.unplannedImmediateResults',
        'practiceImplementation.practiceChanges',
        'practiceImplementation.unplannedSocialResults',
        'practiceImplementation.successStories',
        'practiceImplementation.difficulties',
        'practiceImplementation.lessons',
        'practiceIntroduction.purpose',
        'practiceIntroduction.unplannedImmediateResults',
        'practiceIntroduction.unplannedSocialResults',
        'practiceIntroduction.successStories',
        'practiceIntroduction.difficulties',
        'practiceIntroduction.lessons',
        'expectedMonitoringResults',
        'practicePurpose',
    ];

    const TABLES = [
        'territories' => [
            'region_td' => 'region',
            'locality_td' => 'locality',
        ],
        'monitoringResults' => [
            'mon_res_first_td' => 'first',
            'mon_res_second_td' => 'second',
            'mon_res_third_td' => 'third',
        ],
        'newMonitoringElements' => [
            'new_mon_elements_num_td' => self::AUTO_NUM,
            'new_mon_elem_text_td' => 'text',
        ],
        'monitoringChanges' => [
            'monitoring_changes_num_td' => self::AUTO_NUM,
            'mon_changes_context_td' => 'context',
        ],
        'partnerActivities' => [
            'partner_activities_first_td' => 'first',
            'partner_activities_second_td' => 'second',
            'partner_activities_third_td' => 'third',
            'partner_activities_fours_td' => 'fours',
        ],
        'humanResources' => [
            'human_resource_executors_td' => 'executorsCount',
            'human_resource_substitutions_td' => 'substitutionsCount',
            'human_resource_rates_td' => 'ratesCount',
            'human_resource_individual_td' => 'individualJobSpecialistCount',
            'human_resource_comment_td' => 'comment',
        ],

        'practiceSuccessStoryAttachments' => [
            'practice_sucess_story_num_td' => self::AUTO_NUM,
            'practice_sucess_story_title' => 'title',
        ],
        'practiceSpreadSuccessStoryAttachments' => [
            'practice_spread_story_num_td' => self::AUTO_NUM,
            'practice_spread_story_title' => 'title',
        ],
        'publicationAttachments' => [
            'pub_att_author' => 'author',
            'pub_att_name' => 'name',
            'pub_att_date' => 'date',
            'pub_att_media_name' => 'mediaName',
            'pub_att_media_link' => 'mediaLink',
        ],
        'materialAttachments' => [
            'mat_att_author' => 'author',
            'mat_att_name' => 'name',
            'mat_att_date' => 'date',
            'mat_att_edition' => 'edition',
            'mat_att_media_link' => 'mediaLink',
            'mat_att_comment' => 'comment',
        ],
        'factorAttachments' => [
            'factor_name' => 'name',
            'factor_person' => 'person',
            'factor_link' => 'link',
            'factor_comment' => 'comment',
        ],
        'monitoringDevelopmentAttachments' => [
            'mon_dev_num_td' => self::AUTO_NUM,
            'mon_dev_title' => 'title',
            'mon_dev_text' => 'text',
        ],
        'analyticPublications' => [
            'analyt_pub_indicator' => 'indicator',
            'analyt_pub_fact' => 'fact',
        ],
        'experienceItems' => [
            'expItm_num_td' => self::AUTO_NUM,
            'expItm_direction_td' => 'direction',
            'expItm_indicator_td' => 'indicator',
            'expItm_method_td' => 'measurementMethod',
            'expItm_comment_td' => 'comment',
        ],
        'experienceEtcItems' => [
            'expItmEtc_num_td' => self::AUTO_NUM,
            'expItmEtc_direction_td' => 'CustomDirection',
            'expItmEtc_indicator_td' => 'indicator',
            'expItmEtc_method_td' => 'measurementMethod',
            'expItmEtc_comment_td' => 'comment',
        ],
        'processItems' => [
            'procItm_num_td' => self::AUTO_NUM,
            'procItm_direction_td' => 'direction',
            'procItm_indicator_td' => 'indicator',
            'procItm_method_td' => 'measurementMethod',
            'procItm_comment_td' => 'comment',
        ],
        'processEtcItems' => [
            'procItmEtc_num_td' => self::AUTO_NUM,
            'procItmEtc_direction_td' => 'CustomDirection',
            'procItmEtc_indicator_td' => 'indicator',
            'procItmEtc_method_td' => 'measurementMethod',
            'procItmEtc_comment_td' => 'comment',
        ],
        'qualificationItems' => [
            'qualItm_num_td' => self::AUTO_NUM,
            'qualItm_direction_td' => 'direction',
            'qualItm_indicator_td' => 'indicator',
            'qualItm_method_td' => 'measurementMethod',
            'qualItm_comment_td' => 'comment',
        ],
        'qualificationEtcItems' => [
            'qualItmEtc_num_td' => self::AUTO_NUM,
            'qualItmEtc_direction_td' => 'CustomDirection',
            'qualItmEtc_indicator_td' => 'indicator',
            'qualItmEtc_method_td' => 'measurementMethod',
            'qualItmEtc_comment_td' => 'comment',
        ],
        'resourceBlockItems' => [
            'resItm_num_td' => self::AUTO_NUM,
            'resItm_direction_td' => 'direction',
            'resItm_indicator_td' => 'indicator',
            'resItm_method_td' => 'measurementMethod',
            'resItm_comment_td' => 'comment',
        ],
        'resourceBlockEtcItems' => [
            'resItmEtc_num_td' => self::AUTO_NUM,
            'resItmEtc_direction_td' => 'CustomDirection',
            'resItmEtc_indicator_td' => 'indicator',
            'resItmEtc_method_td' => 'measurementMethod',
            'resItmEtc_comment_td' => 'comment',
        ],
    ];

    const ANALYTIC_RESULTS = [
        'expectedAnalyticResults' => [
            'exp_an_res_service' => 'service',
            'exp_an_res_target' => 'targetGroup',
            'exp_an_res_indicator' => 'indicator',
            'exp_an_res_service_fact' => 'serviceFactValue',
            'exp_an_res_service_plan' => 'servicePlanValue',
            'exp_an_res_method' => 'methods',
            'exp_an_res_feedback' => 'isFeedback',
            'exp_an_res_comment' => 'comment',
        ],
        'implementationSocialResults' => [
            'impl_result' => 'result',
            'impl_indicator' => 'customIndicator',
            'impl_service_fact' => 'FactValue',
            'impl_service_plan' => 'PlanValue',
            'impl_method' => 'methods',
            'impl_feedback' => 'isFeedback',
            'impl_comment' => 'comment',
        ],
        'practiceAnalyticResults' => [
            'practice_an_res_service' => 'service',
            'practice_an_res_practice' => 'practiceFormat',
            'practice_an_res_target' => 'targetGroup',
            'practice_indicator' => 'indicator',
            'practice_an_res_service_fact' => 'serviceFactValue',
            'practice_an_res_service_plan' => 'servicePlanValue',
            'practice_an_res_method' => 'methods',
            'practice_an_res_feedback' => 'isFeedback',
            'practice_an_res_comment' => 'comment',
            'practice_an_res_grantee' => 'grantee',
        ],
        'introductionSocialResults' => [
            'intr_result' => 'result',
            'intr_indicator' => 'customIndicator',
            'intr_service_fact' => 'FactValue',
            'intr_service_plan' => 'PlanValue',
            'intr_method' => 'methods',
            'intr_feedback' => 'isFeedback',
            'intr_comment' => 'comment',
        ],
    ];

    const FEEDBACK_TABLES = [
        'practiceFeedbackAttachments' => [
            'practice_feedback_practice' => 'practice',
            'practice_feedback_comment' => 'comment',
            'practice_feedback_solution' => 'administrativeSolution',
        ],
        'practiceSocialFeedbackAttachments' => [
            'practice_social_feedback_practice' => 'practiceEffectiveness',
            'practice_social_feedback_comment' => 'comment',
            'practice_social_feedback_solution' => 'administrativeSolution',
        ],
        'practiceSpreadFeedbackAttachments' => [
            'spread_feedback_practice' => 'practice',
            'spread_feedback_comment' => 'comment',
            'spread_feedback_solution' => 'administrativeSolution',
        ],
        'practiceSocialSpreadFeedbackAttachments' => [
            'spread_social_feedback_practice' => 'practiceEffectiveness',
            'spread_social_feedback_comment' => 'comment',
            'spread_social_feedback_solution' => 'administrativeSolution',
        ]
    ];

    public function __construct(
        $rootDir,
        PropertyAccessor $propertyAccessor,
        FillWordTemplateService $wordTemplateService,
        DocDownloader $downloader,
        EntityManager $entityManager
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->em = $entityManager;
        $this->accessor =$propertyAccessor;
        $this->rootDir = $rootDir;
    }

    public function generate($object)
    {
        $applicationClass = $object->getReportForm()->getCompetition()->getApplicationClass();
        $this->document = new TemplateProcessor($this->rootDir . self::TEMPLATE_PATH);

        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);
        $this->fillTemplate($applicationClass);
        $this->fillRelatedFields($object);

        $filename = tempnam(sys_get_temp_dir(), 'Report');
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate($applicationClass)
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $tables = self::TABLES;
        $this->wordTemplateService->fillTables($tables);
        $this->fillFeedbackTables();
        $this->fillAnalyticResults();
    }

    private function fillRelatedFields($report)
    {
        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'reportForm' => $report->getReportForm(),
            'applicationHistory' => $report->getApplicationHistory()
        ]);

        $this->document->setValue('sumGrant', $grant ? $grant->getSumGrant() : null);
        $this->document->setValue('contract', $grant ? $grant->getContract() : null);
        $this->document->setValue('year', $report->getReportForm()->getYear()? $report->getReportForm()->getYear()->format('Y') : '');
    }

    protected function fillAnalyticResults()
    {
        foreach (self::ANALYTIC_RESULTS as $table => $fields) {
            $this->fillAnalyticResult($table, $fields);
        }
    }

    protected function fillAnalyticResult($table, $fields)
    {
        $collection = $this->accessor->getValue($this->object, $table);
        $countRows = $collection->count();
        if ($countRows == 0) {
            foreach ($fields as $templateVar => $objAttr) {
                $this->document->setValue("{$templateVar}", " ");
            }
            return;
        }

        $i = 1;
        $isCloneRow = false;
        foreach ($collection as $item) {
            foreach ($fields as $templateVar => $objAttr) {
                if (!$isCloneRow) {
                    $this->document->cloneRow($templateVar, $countRows);
                    $isCloneRow = true;
                }

                if ($objAttr == 'methods') {
                    $titles = [];
                    foreach ($this->accessor->getValue($item, $objAttr) as $method) {
                        $titles[] = $this->accessor->getValue($method, 'name');
                    }
                    $customMethods = $this->accessor->isReadable($item, 'customMethod') ?
                        $this->accessor->getValue($item, 'customMethod') :
                        $this->accessor->getValue($item, 'customMethods')
                    ;
                    if ($customMethods) {
                        $titles[] = $customMethods;
                    }

                    $value = implode(', ', $titles);
                } else {
                    $value = $this->accessor->getValue($item, $objAttr);
                }

                $this->document->setValue("{$templateVar}#{$i}", mb_convert_encoding($this->formatValue($value), 'UTF-8'));
            }
            $i++;
        }
    }

    public function fillFeedbackTables()
    {
        foreach (self::FEEDBACK_TABLES as $key => $table) {
            $collection = $this->accessor->getValue($this->object, $key);
            $countRows = $collection->count();
            if ($countRows == 0) {
                foreach ($table as $templateVar => $objAttr) {
                    $this->document->setValue("{$templateVar}", " ");
                }
                return;
            }

            $i = 1;
            $isCloneRow = false;
            foreach ($collection as $item) {
                foreach ($table as $templateVar => $objAttr) {
                    if (!$isCloneRow) {
                        $this->document->cloneRow($templateVar, $countRows);
                        $isCloneRow = true;
                    }

                    if ($objAttr == 'practice' && $this->accessor->getValue($item, $objAttr)) {
                        $value = $this->accessor->getValue($item, $objAttr)->getService();
                    } elseif ($objAttr == 'practiceEffectiveness' && $this->accessor->getValue($item, $objAttr)) {
                        $value = $this->accessor->getValue($item, $objAttr)->getResult();
                    } else {
                        $value = $this->accessor->getValue($item, $objAttr);
                    }

                    $this->document->setValue("{$templateVar}#{$i}", mb_convert_encoding($this->formatValue($value), 'UTF-8'));
                }
                $i++;
            }
        }
    }

    private function formatValue($value)
    {
        if (is_bool($value)) {
            return $value ? 'да' : 'нет';
        } elseif ($value instanceof \DateTime) {
            return $value->format('d.m.Y');
        } elseif (is_float($value)) {
            return number_format($value, 2, '.', '');
        }

        return (string)$value;
    }
}
