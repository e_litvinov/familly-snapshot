<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class Verification2020ApplicationDocGenerator extends FileGenerator
{
    const FIELDS = [
        'abbreviation',
        'organizationForm',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalFederalDistrict',
        'legalLocality',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualFederalDistrict',
        'actualLocality',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPosition',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingEmeil',
        'iNN',
        'kPP',
        'oKPO',
        'oKTMO',
        'oKVED',
        'kBK',
        'bankName',
        'bankLocation',
        'bankINN',
        'bankKPP',
        'correspondentAccount',
        'bIK',
        'paymentAccount',
        'personalAccount',
        'nameAddressee',
        'projectName',
        'linkToAnnualReport',
        'linkToReportForMinistryOfJustice',
        'deadLineStart',
        'deadLineFinish',
        'trainingGround',
        'priorityDirectionEtc',
        'practiceName',
    ];

    const TABLES = [
        'siteLinks' => [
            'site_link_td' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_td' => 'link'
        ],
        'measurementTools' => [
            'measurementTools_td' => self::AUTO_NUM,
            'measurementTools_title_td' => 'title',
        ],
        'ResultConfirmingDocuments' => [
            'resultConfirmingDocuments_td' => self::AUTO_NUM,
            'resultConfirmingDocuments_first_td' => 'firstField',
            'resultConfirmingDocuments_second_td' => 'secondField',
        ],
        'performanceConfirmingDocuments' => [
            'performanceConfirmingDocuments_td' => self::AUTO_NUM,
            'performanceConfirmingDocuments_title_td' => 'title',
        ],
        'practiceRegulationDocuments' => [
            'practiceRegulationDocuments_td' => self::AUTO_NUM,
            'practiceRegulationDocuments_title_td' => 'title',
        ],
        'ResultDocuments' => [
            'resultDocuments_td' => self::AUTO_NUM,
            'resultDocuments_title_td' => 'title',
        ],
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/verification_application2020.docx';
    const VIEW_FILE_NAME = 'Verification_Application2020';

    /**
     * @var ProxyResolver $proxyResolver
     */
    protected $proxyResolver;

    /** @var EntityManager  */
    private $em;

    /**
     * Kns2018ApplicationDocGenerator constructor.
     *
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param ProxyResolver $proxyResolver
     *
     * @param EntityManager $em
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver,
        EntityManager $em
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->proxyResolver = $proxyResolver;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
        $this->em = $em;
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);
        $this->fillTemplate();
        $this->fillNormativeActs($object);

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $realAuthor = $this->em->find(get_class($this->getObject()->getAuthor()), $this->getObject()->getAuthor()->getId());
        $this->fillAuthorData($realAuthor);

        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
    }

    protected function fillAuthorData($author = null)
    {
        $author =  $author ?: $this->object->getAuthor();

        $this->document->setValue('psrn', $author->getPsrn());
        $this->document->setValue('nko_name', $author->getNkoName());

        $dateSendApplication = $this->getObject()->getIsSend() ? $this->object->getUpdatedAt()->format('d.m.Y') : null;
        $this->document->setValue('updatedAt', $dateSendApplication);
    }

    private function fillNormativeActs(BaseApplication $object)
    {
        $titles = [];
        foreach ($object->getNormativeActs() as $normativeAct) {
            $titles[] = $normativeAct->getTitle();
        }

        $this->document->setValue('normativeAct', implode(', ', $titles));
    }
}
