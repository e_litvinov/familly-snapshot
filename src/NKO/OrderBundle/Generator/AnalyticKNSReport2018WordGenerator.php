<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 13.8.18
 * Time: 15.43
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class AnalyticKNSReport2018WordGenerator extends FileGenerator
{
    private $em;
    private $rootDir;

    const TEMPLATE_PATH = '/../src/NKO/OrderBundle/Resources/templates/analytic_kns_report2018.docx';

    const VIEW_FILE_NAME = 'Report';

    const FIELDS = [
        'reportForm.competition.name',
        'updatedAt',
        'projectName',
        'startDateProject',
        'finishDateProject',
        'directorName',
        'directorPosition',
        'accountantName',
        'accountantPosition',
        'phone',
        'phoneCode',
        'email',
        'priorityDirection',
        'projectPurpose',
        'traineeshipPractice',
        'practiceIntroduction',
        'traineeshipSupport',
        'successStories',
        'realizationProblem',
        'lessons',
        'realizationExperience',
        'projectExperience',
    ];

    const TABLES = [
        'territories' => [
            'region_td' => 'region',
            'locality_td' => 'locality',
        ],
        'practiceAnalyticResults' => [
            'pra_an_res_service_td' => 'service',
            'pra_an_res_target_group_td' => 'targetGroup',
            'pra_an_res_indicator_td' => 'indicator',
            'pra_an_res_service_plan_td' => 'servicePlanValue',
            'pra_an_res_service_fact_td' => 'serviceFactValue',
            'pra_an_res_measurement_method_td' => 'measurementMethod',
            'pra_an_res_feedback_td' => 'isFeedback',
            'pra_an_res_comment_td' => 'comment',
        ],
        'expectedAnalyticResults' => [
            'exp_an_res_target_group_td' => 'targetGroup',
            'exp_an_res_service_td' => 'service',
            'exp_an_res_indicator_td' => 'indicator',
            'exp_an_res_service_plan_td' => 'servicePlanValue',
            'exp_an_res_service_fact_td' => 'serviceFactValue',
            'exp_an_res_measurement_method_td' => 'measurementMethod',
            'exp_an_res_comment_td' => 'comment',
        ],

        'publications' => [
            'pub_author_td' => 'author',
            'pub_title_td' => 'title',
            'pub_date_td' => 'date',
            'pub_location_td' => 'location',
            'pub_link_td' => 'link',
        ],

        'partnerActivities' => [
            'partner_act_first_td' => 'first',
            'partner_act_second_td' => 'second',
            'partner_act_third_td' => 'third',
        ],
        'humanResources' => [
            'hum_res_executors_td' => 'executorsCount',
            'hum_res_substitutions_td' => 'substitutionsCount',
            'hum_res_rates_td' => 'ratesCount',
            'hum_res_individualJobSpecialist_td' => 'individualJobSpecialistCount',
            'hum_res_comment_td' => 'comment',
        ],
        'practiceFeedbackAttachments' => [
            'feedback_service_td' => 'service',
            'feedback_comment_td' => 'comment',
            'feedback_administrativeSolution_td' => 'administrativeSolution',
        ],
        'practiceSuccessStoryAttachments' => [
            'feedback_success_td' => self::AUTO_NUM,
            'feedback_success_title_td' => 'title',
        ],

    ];

    public function __construct(
        $rootDir,
        PropertyAccessor $propertyAccessor,
        FillWordTemplateService $wordTemplateService,
        DocDownloader $downloader,
        EntityManager $entityManager
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->em = $entityManager;
        $this->accessor =$propertyAccessor;
        $this->rootDir = $rootDir;
    }

    public function generate($object)
    {
        $this->document = new TemplateProcessor($this->rootDir . self::TEMPLATE_PATH);
        Settings::setOutputEscapingEnabled(true);

        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($object);
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGrounds');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGrounds.parent');
        $this->fillRelatedFields($object);

        $filename = tempnam(sys_get_temp_dir(), 'Report');
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }


    /** @param $report Report */
    private function fillRelatedFields($report)
    {
        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'reportForm' => $report->getReportForm(),
            'applicationHistory' => $report->getApplicationHistory()
        ]);

        $this->document->setValue('author.nko_name', $report->getAuthor()->getNkoName());
        $this->document->setValue('sumGrant', $grant ? $grant->getSumGrant() : null);
        $this->document->setValue('contract', $grant ? $grant->getContract() : null);
        $this->document->setValue('year', $report->getReportForm()->getYear()? $report->getReportForm()->getYear()->format('Y') : '');
    }
}
