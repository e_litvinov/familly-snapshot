<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Worksheet;
use Symfony\Component\HttpFoundation\Response;

class ExcelStatistic7Generator extends FileGenerator
{
    const VIEW_FILE_NAME = 'statistic_table';
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/statistic7_report.xlsx';

    const START_ROW = 3;
    const COLUMN_START = 0;

    /**
     * @var string $rootDir
     */
    private $rootDir;

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
    }

    /**
     * @param mixed $object
     * @return Response
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function generate($object)
    {
        $filename = $this->rootDir . self::FILE_NAME;
        $tempFileName = tempnam(sys_get_temp_dir(), 'seventh_summary_analytics');
        $tempOutFileName = $tempFileName.'.xlsx';
        file_put_contents($tempOutFileName, $filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        /** @var PHPExcel_Worksheet $sheet */
        $sheet = $excelObj->getActiveSheet();

        $structuredData = $object['Data'];
        $i = self::START_ROW;
        foreach ($structuredData as $competitionArray) {
            $j = self::COLUMN_START;
            foreach ($competitionArray as $value) {
                $this->setValue($j, $i, $value, $sheet);
            }
            $i++;
        }
        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);
        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME);
    }

    public function setValue(&$j, &$i, $value, $sheet)
    {
        $cell = $sheet->setCellValueByColumnAndRow($j++, $i, $value ? $value : 0, true);
        $this->setCellStyles($cell);
        $sheet->getRowDimension($i)->setRowHeight(-1);
        if (is_float($value)) {
            $cell->getStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        }
    }

    public function setCellStyles(\PHPExcel_Cell $cell)
    {
        $cell->getStyle()->applyFromArray([
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'wrap' => true
            ]
        ])
        ;
    }
}
