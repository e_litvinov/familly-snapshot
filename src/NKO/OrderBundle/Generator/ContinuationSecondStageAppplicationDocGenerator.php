<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 17.10.18
 * Time: 9.33
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use NKO\OrderBundle\Entity\GrantConfig;
use Doctrine\ORM\EntityManager;

class ContinuationSecondStageAppplicationDocGenerator extends FileGenerator
{
    const  FIELDS = [
        'projectName',
        'deadLineStart',
        'deadLineFinish',
        'sumGrant',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfAccountingFullName',
        'headOfAccountingPosition',
        'accountantPosition',
        'briefPracticeDescription',
        'practiceChanges',
        'beneficiaryGroupsProblems',
        'projectPurpose',
        'introductionProjectPurpose',
        'monitoringProjectPurpose',
        'expectedResultsComment',
        'effectivenessImplementationItemsComment',
        'practiceResultsComment',
        'effectivenessDisseminationItemsComment',
    ];

    const TABLES = [
        'territories' => [
            'region_td' => 'region',
            'locality_td' => 'locality',
        ],
        'otherBeneficiaryGroups' => [
            'oth_ben_group_tb_num' => self::AUTO_NUM,
            'oth_ben_group_tb_name' => 'name'
        ],
        'beneficiaryProblems' => [
            'ben_prob_tb_num' => self::AUTO_NUM,
            'ben_prob_target_group' => 'targetGroup',
            'ben_prob_content' => 'content',
            'ben_prob_socialResults' => 'humanizedSocialResults',
            'ben_prob_customSocialResult' => 'customSocialResult'
        ],
        'expectedResults' => [
            'expected_tb_num' => self::AUTO_NUM,
            'expected_service' => 'service',
            'expected_is_public_event' => 'isPublicEvent',
            'expected_target_group' => 'problem',
            'expected_start_date_fact' => 'startDateFact',
            'expected_finish_date_fact' => 'finishDateFact',
            'expected_locality' => 'locality',
            'expected_ service _fact_value' => 'serviceFactValue',
            'expected_service_plan_value' => 'servicePlanValue',
            'expected_beneficiary_plan_value' => 'beneficiaryPlanValue',
            'expected_is_feedback' => 'isFeedback',
            'expected_methods' => 'humanizedMethods',
            'expected_customMethod' => 'comment'
        ],
        'sustainabilityPracticeApplications' => [
            'sut_pract_tb_num' => self::AUTO_NUM,
            'sut_pract_context' => 'context'
        ],
        'realizationPracticeApplications' => [
            'realiz_pract_tb_num' => self::AUTO_NUM,
            'realiz_pract_title' => 'title',
            'realiz_pract_context' => 'context'
        ],
        'effectivenessImplementationEtcItems' => [
            'effect_impl_etc_itm_result' => 'result',
            'effect_impl_etc_itm_indicator' => 'customIndicator',
            'effect_impl_etc_itm_fact_value' => 'factValue',
            'effect_impl_etc_itm_plan_value' => 'planValue',
            'effect_impl_etc_itm_methods' => 'humanizedMethods',
            'effect_impl_etc_itm_customMethod' => 'comment'
        ],
        'specialistTargetGroups' => [
            'tbs_num' => self::AUTO_NUM,
            'title_target_group' => 'title'
        ],
        'otherSpecialistTargetGroups' => [
            'tb_num' => self::AUTO_NUM,
            'target_group_name' => 'name'
        ],
        'specialistProblems' => [
            'spec_problem_tb_num' => self::AUTO_NUM,
            'spec_problem_target_group' => 'targetGroup',
            'spec_problem_specialist_competition' => 'humanizedSpecialistCompetitions',
            'spec_problem_content' => 'content',
            'spec_problem_socialResults' => 'humanizedSocialResults',
            'spec_problem_customSocialResult' => 'customSocialResult'
        ],
        'practiceResults' => [
            'practice_tb_num' => self::AUTO_NUM,
            'practice_service' => 'service',
            'practice_is_public_event' => 'isPublicEvent',
            'practice_practice_format' => 'practiceFormat',
            'practice_target_group' => 'problem',
            'practice_start_date_fact' => 'startDateFact',
            'practice_finish_date_fact' => 'finishDateFact',
            'practice_locality' => 'locality',
            'practice_ service _fact_value' => 'serviceFactValue',
            'practice_service_plan_value' => 'servicePlanValue',
            'practice_beneficiary_plan_value' => 'beneficiaryPlanValue',
            'practice_methods' => 'humanizedMethods',
            'practice_is_feedback' => 'isFeedback',
            'practice_customMethod' => 'comment'
        ],
        'effectivenessDisseminationEtcItems' => [
            'eff_diss_etc_result' => 'result',
            'eff_diss_etc_indicator' => 'customIndicator',
            'eff_diss_etc_fact_value' => 'factValue',
            'eff_diss_etc_plan_value' => 'planValue',
            'eff_diss_etc_methods' => 'humanizedMethods',
            'eff_diss_etc_customMethod' => 'comment'
        ],
        'sustainabilitySpreadApplications' => [
            'sut_spr_app_tb_num' => self::AUTO_NUM,
            'sut_spr_app_context' => 'context'
        ],
        'spreadPracticeApplications' => [
            'spr_pract_app_tb_num' => self::AUTO_NUM,
            'spr_pract_app_title' => 'title',
            'spr_pract_app_context' => 'context'
        ],
        'experienceItems' => [
            'exp_itm_tb_num' => self::AUTO_NUM,
            'exp_itm_direction' => 'direction',
            'exp_itm_indicator' => 'indicator',
            'exp_itm_measurement_method' => 'measurementMethod',
            'exp_itm_comment' => 'comment'
        ],
        'experienceEtcItems' => [
            'exp_etc_itm_tb_num' => self::AUTO_NUM,
            'exp_etc_itm_direction' => 'customDirection',
            'exp_etc_itm_indicator' => 'indicator',
            'exp_etc_itm_measurement_method' => 'measurementMethod',
            'exp_etc_itm_comment' => 'comment'
        ],
        'processItems' => [
            'proc_itm_tb_num' => self::AUTO_NUM,
            'proc_itm_direction' => 'direction',
            'proc_itm_indicator' => 'indicator',
            'proc_itm_measurement_method' => 'measurementMethod',
            'proc_itm_comment' => 'comment'
        ],
        'processEtcItems' => [
            'proc_etc_itm_tb_num' => self::AUTO_NUM,
            'proc_etc_itm_direction' => 'customDirection',
            'proc_etc_itm_indicator' => 'indicator',
            'proc_etc_itm_measurement_method' => 'measurementMethod',
            'proc_etc_itm_comment' => 'comment'
        ],
        'qualificationItems' => [
            'qual_itm_tb_num' => self::AUTO_NUM,
            'qual_itm_direction' => 'direction',
            'qual_itm_indicator' => 'indicator',
            'qual_itm_measurement_method' => 'measurementMethod',
            'qual_itm_comment' => 'comment'
        ],
        'qualificationEtcItems' => [
            'qual_etc_itm_tb_num' => self::AUTO_NUM,
            'qual_etc_itm_direction' => 'customDirection',
            'qual_etc_itm_indicator' => 'indicator',
            'qual_etc_itm_measurement_method' => 'measurementMethod',
            'qual_etc_itm_comment' => 'comment'
        ],
        'resourceBlockItems' => [
            'res_block_itm_tb_num' => self::AUTO_NUM,
            'res_block_itm_direction' => 'direction',
            'res_block_itm_indicator' => 'indicator',
            'res_block_itm_measurement_method' => 'measurementMethod',
            'res_block_itm_comment' => 'comment'
        ],
        'resourceBlockEtcItems' => [
            'res_block_etc_itm_tb_num' => self::AUTO_NUM,
            'res_block_etc_itm_direction' => 'customDirection',
            'res_block_etc_itm_indicator' => 'indicator',
            'res_block_etc_itm_measurement_method' => 'measurementMethod',
            'res_block_etc_itm_comment' => 'comment'
        ],
        'sustainabilityMonitoringApplications' => [
            'sust_mon_app_tb_num' => self::AUTO_NUM,
            'sust_mon_app_context' => 'context'
        ],
        'risePotentialApplications' => [
            'risk_pot_app_tb_num' => self::AUTO_NUM,
            'risk_pot_app_title' => 'title',
            'risk_pot_app_context' => 'context'
        ],
        'projectTeams' => [
            'project_team_tb_num' => self::AUTO_NUM,
            'project_team_name' => 'name',
            'project_team_role' => 'role',
            'project_team_responsibility' => 'responsibility',
            'project_team_relationship' => 'relationship',
            'project_team_description' => 'description',
            'project_team_is_new' => 'isNew'
        ],
        'projectContPartners' => [
            'project_par_tb_num' => self::AUTO_NUM,
            'project_par_organization_name' => 'organizationName',
            'project_par_brief_description' => 'briefDescription',
            'project_par_participation' => 'participation',
            'project_par_is_new' => 'isNew'
        ],
        'resourceItems' => [
            'hum_res_name_resource' => 'nameResourceType',
            'hum_res_description_resource' => 'descriptionResource'
        ],
        'resourceEtcItems' => [
            'hum_res_etc_name_resource' => 'nameResource',
            'hum_res_etc_description_resource' => 'descriptionResource'
        ]
    ];

    const VIEW_FILE_NAME = 'Application';
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/continuation_second_stage_application.docx';
    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'tb_people_category';

    /**
     * @var RelatedCompetitionResolver $relatedCompetitionResolver
     */
    protected $relatedCompetitionResolver;

    /**
     * ContinuationApplicationDocGenerator constructor.
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param RelatedCompetitionResolver $relatedCompetitionResolver
     *
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        RelatedCompetitionResolver $relatedCompetitionResolver,
        EntityManager $entityManager
    ) {
        $this->accessor = $propertyAccessor;
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->relatedCompetitionResolver = $relatedCompetitionResolver;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
        $this->em = $entityManager;
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);

        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);

        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);

        $this->fillCustomFields();
    }

    protected function fillCustomFields()
    {
        $competition = $this->accessor->getValue($this->object, 'competition');
        $grant = $this->em->getRepository(GrantConfig::class)->findGrantByAuthorAndReportForm($this->object->getCompetition()->getRelatedReportForm(), $this->object->getAuthor());

        $this->document->setValue('year', (string)$this->accessor->getValue($competition, 'realizationYear')->format('Y'));
        $this->document->setValue('competition', (string)$this->accessor->getValue($competition, 'name'));
        $this->document->setValue('nko_name', $this->object->getAuthor()->getNkoName());
        $this->document->setValue('phone', $this->getObject()->getAuthor()->getPhone());
        $this->document->setValue('email', $this->getObject()->getAuthor()->getEmail());
        $this->document->setValue('phoneCode', $this->object->getAuthor()->getPhoneCode());

        $this->document->setValue('priorityDirectionName', mb_convert_encoding($this->object->getPriorityDirection(), 'UTF-8'));
        $this->document->setValue('contract', mb_convert_encoding($grant ? $grant->getContract() : null, 'UTF-8'));
    }
}
