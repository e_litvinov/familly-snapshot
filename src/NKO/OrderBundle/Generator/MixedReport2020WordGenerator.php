<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Expense;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Register;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceSpent;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use NKO\OrderBundle\Traits\ExpenseTypeTrait;

class MixedReport2020WordGenerator extends FileGenerator
{
    use ExpenseTypeTrait;

    private $em;
    private $rootDir;

    const VIEW_FILE_NAME = 'MixedReport';
    const TEMPLATE_PATH =  '/../src/NKO/OrderBundle/Resources/templates/mixed_report_2020.docx';

    const FIELDS = [
        'author.nko_name',
        'reportForm.competition.name',
        'updatedAt',
        'startDateProject',
        'finishDateProject',
        'directorName',
        'directorPosition',
        'phoneCode',
        'phone',
        'email',
        'purpose',
        'knowledgeUsage',
        'planOfKnowledgeUsage',
        'pastEvents',
        'difficulties',
        'successStory',
        'feedback',
        'receiptOfDonations',
        'periodCosts',
        'incrementalCosts',
        'donationBalanceDocument'
    ];

    const TABLES = [
        'events' => [
            'events_num_td' => self::AUTO_NUM,
            'content_td' => 'eventName',
            'time_td' => 'time',
            'place_td' => 'place',
            'count_member_td' => 'count_member',
        ],
        'mixedResults' => [
            'linked_types_name_td' => 'linkedTypes.name',
            'service_plan_value_td' => 'servicePlanValue',
            'service_fact_value_td' => 'serviceFactValue',
            'service_expected_value_td' => 'serviceExpectedValue',
            'measurement_method_td' => 'humanizedMeasurementMethods',
            'comment_td' => 'comment',
        ],
        'mixedEmployeeResults' => [
            'employee_result_num_td' => self::AUTO_NUM,
            'employee_result_td' => 'result',
            'employee_indicator_td' => 'indicator',
            'employee_service_plan_value_td' => 'servicePlanValue',
            'employee_service_fact_value_td' => 'serviceFactValue',
            'employee_expected_value_td' => 'serviceExpectedValue',
            'employee_measurement_method_td' => 'humanizedMeasurementMethods',
            'employee_comment_td' => 'comment',
        ],
        'mixedBeneficiariesResults' => [
            'beneficiaries_result_num_td' => self::AUTO_NUM,
            'beneficiaries_result_td' => 'result',
            'beneficiaries_indicator_td' => 'indicator',
            'beneficiaries_service_plan_value_td' => 'servicePlanValue',
            'beneficiaries_service_fact_value_td' => 'serviceFactValue',
            'beneficiaries_expected_value_td' => 'serviceExpectedValue',
            'beneficiaries_measurement_method_td' => 'humanizedMeasurementMethods',
            'beneficiaries_comment_td' => 'comment',
        ],
        'financeSpent' => [
            'finance_expense_type' => 'expenseType.titleWithNumber',
            'finance_approved_sum' => 'approvedSum',
            'finance_period_costs' => 'periodCosts',
            'finance_balance' => 'balance',
        ],
        'otherBeneficiaryGroups' => [
            'group_num_td' => self::AUTO_NUM,
            'group_name_td' => 'name',
        ],
    ];

    const REGISTERS_TABLE = [
        'register_num' => 'number',
        'register_proofProductExpenseDocument' => 'proofProductExpenseDocument',
        'register_proofPaidGoodsDocument' => 'proofPaidGoodsDocument',
        'register_name' => 'name',
        'register_sum' => 'sum',
    ];

    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'category_name_td';

    public function __construct(
        $rootDir,
        PropertyAccessor $propertyAccessor,
        FillWordTemplateService $wordTemplateService,
        DocDownloader $downloader,
        EntityManager $entityManager
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->em = $entityManager;
        $this->accessor =$propertyAccessor;
        $this->rootDir = $rootDir;
    }

    public function generate($object)
    {
        $this->document = new TemplateProcessor($this->rootDir . self::TEMPLATE_PATH);
        Settings::setOutputEscapingEnabled(true);

        $this->setObject($object);
        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), 'Report');
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate()
    {
        ExpenseTypeTrait::addReportForm($this->object);
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
        $this->fillOrganizationalFields($this->object);
        $this->fillRegisterTables($this->object);
        $this->fillTotalFinanceSpent($this->object);
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);
    }

    private function fillOrganizationalFields($report)
    {
        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
            'reportForm' => $report->getReportForm(),
            'applicationHistory' => $report->getApplicationHistory()
        ]);

        $application = $this->getApplicationFromReport($report);
        $projectName = $application->getProjectName();
        $trainingGrounds = $application->getTrainingGrounds();

        if (!$trainingGrounds->isEmpty()) {
            foreach ($trainingGrounds as $trainingGround) {
                if ($trainingGround->getParent()) {
                    $trainingGroundsSecond = $trainingGround;
                    break;
                }
            }
        }

        $this->document->setValue('sumGrant', $grant ? $grant->getSumGrant() : null);
        $this->document->setValue('contract', $grant ? $grant->getContract() : null);
        $this->document->setValue('projectName', $projectName);
        $this->document->setValue('trainingGroundsSecond', isset($trainingGroundsSecond) ? mb_convert_encoding($trainingGroundsSecond, 'UTF-8') : null);
    }

    public function fillRegisterTables($report)
    {
        $registers = $this->em->getRepository(Register::class)->findBy(['report' => $report->getId()]);
        $expenses = $this->em->getRepository(Expense::class)->findExpensesByRegister($registers);
        $registerNumber = 1;

        if ($registers) {
            foreach ($registers as $register) {
                $collectionExpenses = array_filter($expenses, function ($expense) use ($register) {
                    return $expense->getRegister()->getId() === $register->getId();
                });

                $countRows = count($collectionExpenses);
                if ($countRows == 0) {
                    foreach (self::REGISTERS_TABLE as $key => $templateVar) {
                        $field = $key  . '_' . $registerNumber;
                        $this->document->setValue("{$field}", " ");
                    }
                    return;
                }

                $i = 1;
                $numberRov = 1;
                $isCloneRow = false;

                foreach ($collectionExpenses as $item) {
                    foreach (self::REGISTERS_TABLE as $key => $templateVar) {
                        $tag = $key . '_' . $registerNumber;

                        if (!$isCloneRow) {
                            $this->document->cloneRow($tag, $countRows);
                            $isCloneRow = true;
                        }

                        if ($templateVar === 'number') {
                            $this->document->setValue("{$tag}#{$i}", $numberRov++);
                            continue;
                        }

                        $value = $this->accessor->getValue($item, $templateVar);
                        $this->document->setValue("{$tag}#{$i}", mb_convert_encoding($value, 'UTF-8'));
                    }
                    $i++;
                }

                $tag = 'totalExpenseValue' . '_' . $registerNumber;
                $value = $this->accessor->getValue($register, 'totalExpenseValue');
                $this->document->setValue("{$tag}", $value);
                $registerNumber++;
            }
        }
    }

    public function fillTotalFinanceSpent($report)
    {
        $financeSpents = $this->em->getRepository(FinanceSpent::class)->getSumFinanceSpent($report);
        if ($financeSpents) {
            $this->document->setValue('finance_approved_sum_total', isset($financeSpents['approvedSum']) ? round($financeSpents['approvedSum'], 2) : null);
            $this->document->setValue('finance_period_costs_total', isset($financeSpents['periodCosts']) ? round($financeSpents['periodCosts'], 2) : null);
            $this->document->setValue('finance_balance_total', isset($financeSpents['balance']) ? round($financeSpents['balance'], 2) : null);
        }
    }

    protected function getApplicationFromReport($report)
    {
        /** @var BaseReport $report */
        $application = unserialize($report->getApplicationHistory()->getData());

        if ($report->getReportForm()->isDataTakesFromActualApplication()) {
            $application = $this->em->getRepository(BaseApplication::class)->find($application->getId());
        }

        return $application;
    }
}
