<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ContinuationSecondStageApplication2019DocGenerator extends ContinuationSecondStageAppplicationDocGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/continuation_2019_second_stage_application.docx';

    const  FIELDS = [
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'legalFederalDistrict',
        'legalLocality',
        'actualFederalDistrict',
        'actualLocality',
    ];

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * ContinuationApplicationDocGenerator constructor.
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param RelatedCompetitionResolver $relatedCompetitionResolver
     *
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        RelatedCompetitionResolver $relatedCompetitionResolver,
        EntityManager $entityManager
    ) {
        $this->accessor = $propertyAccessor;
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->relatedCompetitionResolver = $relatedCompetitionResolver;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
        $this->em = $entityManager;
    }


    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->wordTemplateService->fillTemplate(array_merge(parent::FIELDS, self::FIELDS));
        $this->wordTemplateService->fillTables(parent::TABLES);

        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);

        $this->fillCustomFields();
    }

    protected function fillCustomFields()
    {
        $this->document->setValue('normativeActs', $this->getNormativeActs($this->object));
        parent::fillCustomFields();
    }

    private function getNormativeActs($object)
    {
        $titles = [];
        foreach ($object->getNormativeActs() as $normativeAct) {
            $titles[] = $normativeAct->getTitle();
        }

        return implode(', ', $titles);
    }
}
