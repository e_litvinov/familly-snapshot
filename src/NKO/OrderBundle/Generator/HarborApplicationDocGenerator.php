<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class HarborApplicationDocGenerator extends FileGenerator
{
    const FIELDS = [
        'abbreviation',
        'organizationForm',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPosition',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingEmeil',
        'iNN',
        'kPP',
        'oKPO',
        'oKTMO',
        'oKVED',
        'kBK',
        'bankName',
        'bankLocation',
        'bankINN',
        'bankKPP',
        'correspondentAccount',
        'bIK',
        'paymentAccount',
        'personalAccount',
        'nameAddressee',
        'projectName',
        'linkToAnnualReport',
        'linkToReportForMinistryOfJustice',
    ];

    const FINANCING = 'firstYearEstimatedFinancing';

    const TABLES = [
        'siteLinks' => [
            'site_link_td' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_td' => 'link'
        ],
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/harbor_application2019.docx';
    const VIEW_FILE_NAME = 'Harbor_Application2019';

    /**
     * @var ProxyResolver $proxyResolver
     */
    protected $proxyResolver;

    /** @var EntityManager  */
    private $em;

    /**
     * Kns2018ApplicationDocGenerator constructor.
     *
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param ProxyResolver $proxyResolver
     *
     * @param EntityManager $em
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver,
        EntityManager $em
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->proxyResolver = $proxyResolver;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
        $this->em = $em;
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);
        $this->fillTemplate();
        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $realAuthor = $this->em->find(get_class($this->getObject()->getAuthor()), $this->getObject()->getAuthor()->getId());
        $this->fillAuthorData($realAuthor);

        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
        $this->fillFinancing();
    }

    protected function fillAuthorData($author = null)
    {
        $author =  $author ?: $this->object->getAuthor();

        $this->document->setValue('psrn', $author->getPsrn());
        $this->document->setValue('nko_name', $author->getNkoName());

        $dateSendApplication = $this->getObject()->getIsSend() ? $this->object->getUpdatedAt()->format('d.m.Y') : null;
        $this->document->setValue('updatedAt', $dateSendApplication);
    }

    protected function fillFinancing()
    {
        $value = $this->accessor->getValue($this->object, self::FINANCING);
        $this->wordTemplateService->getDocument()->setValue(self::FINANCING, number_format($value, 5, '.', ''));
    }
}
