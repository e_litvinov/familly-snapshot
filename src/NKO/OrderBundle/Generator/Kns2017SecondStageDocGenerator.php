<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class Kns2017SecondStageDocGenerator extends FileGenerator
{
    const FIELDS = [
        'abbreviation',
        'organizationForm',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPosition',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingEmeil',
        'dateRegistrationOfOrganization',
        'primaryActivity',
        'mission',
        'countStaffEmployees',
        'countInvolvingEmployees',
        'countVolunteerEmployees',
        'iNN',
        'kPP',
        'oKPO',
        'oKTMO',
        'oKVED',
        'kBK',
        'bankName',
        'bankLocation',
        'bankINN',
        'bankKPP',
        'correspondentAccount',
        'bIK',
        'paymentAccount',
        'personalAccount',
        'nameAddressee',
        'projectName',
        'priorityDirectionEtc',
        'knowledgeIntroductionDuringProjectImplementation',
        'knowledgeIntroductionAfterProjectImplementation',
        'deadLineStart',
        'deadLineFinish',
        'projectPurpose',
        'linkToAnnualReport',
        'effectivenessComment',
        'individualSocialResultsComment',
        'projectRelevance'
    ];

    const TABLES = [
        'beneficiaryResults' => [
            'beneficiary_result_td' => self::AUTO_NUM,
            'result_td' => 'result',
            'indicator_td' => 'indicator',
            'target_value_td' => 'targetValue',
            'approximate_target_value_td' => 'approximateTargetValue',
            'method_measurement_td' => 'linkedMethod',
            'comment_td' => 'methodMeasurement'
        ],
        'siteLinks' => [
            'site_link_td' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_td' => 'link'
        ],
        'projects' => [
            'project_num_td' => self::AUTO_NUM,
            'project_name_td' => 'projectName',
            'project_date_td' => 'deadline',
            'project_result_td' => 'result',
            'financing_source_type_td' => 'humanizedFinancingSourceTypes'
        ],
        'publications' => [
            'publication_name_td' => 'publicationName',
            'publication_link_td' => 'link'
        ],
        'otherBeneficiaryGroups' => [
            'group_num_td' => self::AUTO_NUM,
            'group_name_td' => 'name',
        ],
        'organizationResources' => [
            'resource_name_td' => 'name',
            'resource_description_td' => 'description',
        ],
        'projectPartners' => [
            'project_partners_name_td' => 'organizationName',
            'project_partners_information_td' => 'briefInformation',
            'project_participation_num_td' => 'projectParticipation'
        ],
        'projectMembers' => [
            'project_members_name_td' => 'fullName',
            'project_members_role_td' => 'role',
            'project_members_relationship_td' => 'employmentRelationship',
            'project_members_information_td' => 'briefInformation'
        ],
        'practiceImplementationActivities' => [
            'implementation_num_td' => self::AUTO_NUM,
            'implementation_activity_td' => 'activity',
            'implementation_deadline_td' => 'deadline',
            'implementation_results_td' => 'expectedResults',
        ],
        'practiceRegions' => [
            'region_name_td' => 'name',
        ],
        'beneficiaryProblems' => [
            'problem_num_td' => self::AUTO_NUM,
            'problem_target_group_td' => 'targetGroup',
            'problem_content_td' => 'content',
            'problem_social_result_td' => 'humanizedSocialResults',
            'problem_custom_result_td' => 'customSocialResult',
        ],
        'individualSocialResults' => [
            'res_social_result_td' => 'socialResult',
            'res_target_group_td' => 'targetGroup',
            'res_indicator_td' => 'indicator',
            'res_first_year_td' => 'firstYearTargetValue',
            'res_target_value_td' => 'targetValue',
            'res_measurement_td' => 'humanizedMeasurementMethod',
            'res_custom_measurement_td' => 'customMeasurementMethod',
        ],
        'risks' => [
            'key_risk_td' => 'keyRisk',
            'action_to_reduce_risk_td' => 'actionToReduceRisk'
        ],
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/kns2017_second_stage.docx';
    const VIEW_FILE_NAME = 'KNS_application_2018';
    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'category_name_td';

    /**
     * @var ProxyResolver $proxyResolver
     */
    protected $proxyResolver;

    /**
     * Kns2018ApplicationDocGenerator constructor.
     *
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param ProxyResolver $proxyResolver
     *
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->proxyResolver = $proxyResolver;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);
        $this->fillTemplate();
        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->proxyResolver->resolveProxies($this->object));
        $this->fillAuthorData();
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);
        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGroundParent');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGround');
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
        $this->fillEffectivenessTable();
    }

    protected function fillAuthorData($author = null)
    {
        $author =  $author ?: $this->object->getAuthor();

        $this->document->setValue('psrn', $author->getPsrn());
        $this->document->setValue('nko_name', $author->getNkoName());

        $dateSendApplication = $this->getObject()->getIsSend() ? $this->object->getUpdatedAt()->format('d.m.Y') : null;
        $this->document->setValue('updatedAt', $dateSendApplication);
    }

    public function fillEffectivenessTable()
    {
        $table = 'effectivenessItems';
        $fields = [
            'eff_indicator_td' => 'indicator',
            'eff_factValue_td' => 'factValue',
            'eff_planValue_td' => 'planValue',
            'eff_measurementMethod_td' => 'measurementMethod',
            'eff_comment_td' => 'comment',
        ];
        $collection = $this->accessor->getValue($this->object, $table);
        $countRows = $collection->count();
        if ($countRows == 0) {
            foreach ($fields as $templateVar => $objAttr) {
                $this->document->setValue("{$templateVar}", " ");
            }
            return;
        }

        $i = 1;
        $isCloneRow = false;
        foreach ($collection as $item) {
            foreach ($fields as $templateVar => $objAttr) {
                if (!$isCloneRow) {
                    $this->document->cloneRow($templateVar, $countRows);
                    $isCloneRow = true;
                }

                if ($objAttr == 'indicator') {
                    $child = $this->proxyResolver->resolveProxies($this->accessor->getValue($item, $objAttr));
                    $parent = $this->proxyResolver->resolveProxies($child->getParent());
                    $child->setParent($parent);
                    $value = $child->getTitleWithNumber();
                } else if ($objAttr == 'measurementMethod') {
                    $itemValue = $this->accessor->getValue($item, $objAttr);
                    $value = !$itemValue ? $itemValue : $this->proxyResolver->resolveProxies($itemValue);
                } else {
                    $value = $this->accessor->getValue($item, $objAttr);
                }

                $this->document->setValue("{$templateVar}#{$i}", mb_convert_encoding($value, 'UTF-8'));
            }
            $i++;
        }
    }
}
