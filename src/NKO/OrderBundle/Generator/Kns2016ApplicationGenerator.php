<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 6:11 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\PdfDownloader;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;

class Kns2016ApplicationGenerator extends FileGenerator
{
    const FILE_NAME = 'NKOOrderBundle:CRUD:show.html.twig';
    const VIEW_FILE_NAME = 'KNS_Application_2016';

    /**
     * @var TwigEngine $twigEngine
     */
    private $twigEngine;

    /**
     * @var FlashBag $flashBag
     */
    private $flashBag;

    public function __construct(
        PdfDownloader $downloader,
        TwigEngine $twigEngine,
        FlashBag $flashBag
    ) {
        $this->downloader = $downloader;
        $this->twigEngine = $twigEngine;
        $this->flashBag = $flashBag;
    }

    public function generate($object)
    {
        $this->setObject($object);

        try {
            $html = $this->twigEngine->render(self::FILE_NAME,  [
                'application' => $object,
                'action' => 'show'
            ]);

            return $this->downloader->getStreamedResponse($html, self::VIEW_FILE_NAME);
        } catch (\Twig_Error $error) {
            $this->flashBag->add('sonata_flash_error','cannot_render_template');

            return false;
        }
    }
}