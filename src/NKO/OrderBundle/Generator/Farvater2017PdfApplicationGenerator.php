<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 7:00 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\PdfDownloader;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Resolver\RelatedApplicationsResolver;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use NKO\OrderBundle\Resolver\ProxyResolver;

class Farvater2017PdfApplicationGenerator extends FileGenerator
{
    const FILE_NAME = 'NKOOrderBundle:CRUD/farvater2017:show.html.twig';

    /**
     * @var RelatedApplicationsResolver $relatedApplicationsResolver
     */
    private $relatedApplicationsResolver;

    /**
     * @var TwigEngine $twigEngine
     */
    private $twigEngine;

    /**
     * @var FlashBag $flashBag
     */
    private $flashBag;

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * Farvater2017PdfApplicationGenerator constructor.
     * @param PdfDownloader $downloader
     * @param RelatedApplicationsResolver $relatedApplicationsResolver
     * @param ProxyResolver $proxyResolver
     * @param TwigEngine $twigEngine
     * @param FlashBag $flashBag
     */
    public function __construct(
        PdfDownloader $downloader,
        RelatedApplicationsResolver $relatedApplicationsResolver,
        ProxyResolver $proxyResolver,
        TwigEngine $twigEngine,
        FlashBag $flashBag
    ) {
        $this->downloader = $downloader;
        $this->proxyResolver = $proxyResolver;
        $this->relatedApplicationsResolver = $relatedApplicationsResolver;
        $this->twigEngine = $twigEngine;
        $this->flashBag = $flashBag;
    }

    public function generate($object)
    {
        $object = $this->proxyResolver->resolveProxies($object);
        $this->setObject($object);
        $briefApplication = $this->relatedApplicationsResolver->getRelatedApplications(
            $object,
            BriefApplication::class,
            true
        );

        if (!$briefApplication) {
            return false;
        }

        try {
            $html = $this->twigEngine->render(self::FILE_NAME,  [
                    'farvaterApplication' => $object,
                    'briefApplication' => $briefApplication,
                    'action' => 'show'
                ]);

            return $this->downloader->getStreamedResponse($html, $this->getObject()->getAuthor()->getPsrn());
        } catch (\Twig_Error $error) {
            $this->flashBag->add('sonata_flash_error','cannot_render_template');

            return false;
        }
    }
}