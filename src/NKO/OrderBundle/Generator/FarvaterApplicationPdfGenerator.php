<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 6:46 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\PdfDownloader;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class FarvaterApplicationPdfGenerator extends FileGenerator
{
    const FILE_NAME = 'NKOOrderBundle:CRUD/farvater_application2016:show.html.twig';
    const VIEW_FILE_NAME = 'Farvater_2017';

    /**
     * @var TwigEngine $twigEngine
     */
    private $twigEngine;

    /**
     * @var FlashBag $flashBag
     */
    private $flashBag;

    /**
     * FarvaterApplicationPdfGenerator constructor.
     *
     * @param PdfDownloader $downloader
     * @param TwigEngine $twigEngine
     * @param FlashBag $flashBag
     */
    public function __construct(
        PdfDownloader $downloader,
        TwigEngine $twigEngine,
        FlashBag $flashBag
    ) {
        $this->downloader = $downloader;
        $this->twigEngine = $twigEngine;
        $this->flashBag = $flashBag;
    }

    /**
     * @param mixed $object
     * @return bool|\Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        $this->setObject($object);

        try {
            $html = $this->twigEngine->render(self::FILE_NAME,  [
                'application' => $object,
                'action' => 'show'
            ]);

            return $this->downloader->getStreamedResponse($html, self::VIEW_FILE_NAME);
        } catch (\Twig_Error $error) {
            $this->flashBag->add('sonata_flash_error','cannot_render_template');

            return false;
        }
    }
}