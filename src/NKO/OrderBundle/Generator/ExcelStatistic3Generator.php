<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use PHPExcel_Style_Border;

class ExcelStatistic3Generator extends FileGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/empty_file.xlsx';
    const VIEW_FILE_NAME = 'statistic_table';

    const FIELDS = [
        3 => [
            'Полное название организации',
            'Название проекта',
            'Практика',
            'Услуги, мероприятия и пр.',
            'Собирали ли обратную связь'
        ],
        4 => [
            'Полное название организации',
            'Название проекта',
            'Практика',
            'Услуги, мероприятия и пр., по которым собиралась обратная связь',
            'Комментарий (выводы, цитаты)',
            'Принимались ли управленческие решения по результатам обратной связи',
        ],
        5 => [
            'Полное название организации',
            'Название проекта',
            'Услуги, мероприятия и пр. ',
            'Собирали ли обратную связь',
            'Комментарий',
            'Комментарий (выводы, цитаты)',
            'Принимались ли управленческие решения по результатам обратной связи',
        ],
    ];

    const COLUMN_WIDTH = [
        3 => 'E',
        4 => 'F',
        5 => 'G'
    ];

    const PRACTICE = ['Реализация ', 'Распространение и внедрение'];
    const FEEDBACK = ['Нет', 'Да'];

    private $rootDir;

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
    }

    public function generate($object)
    {
        $filename = $this->rootDir . self::FILE_NAME;
        $tempFileName = tempnam(sys_get_temp_dir(), 'Summary_analytics');
        $tempOutFileName = $tempFileName.'.xlsx';
        file_put_contents($tempOutFileName, $filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        $sheet = $excelObj->getActiveSheet();
        $reportNumber = array_pop($object);
        $this->setHeaderStyle($sheet, self::COLUMN_WIDTH[$reportNumber], self::FIELDS[$reportNumber]);
        $this->fillSheet($sheet, $object);

        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);

        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME);
    }

    private function setHeaderStyle(\PHPExcel_Worksheet $sheet, $finish, $items)
    {
        foreach ($items as $key => $item) {
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($key, 1, $item, true));
        }

        foreach (range('A', $finish) as $column) {
            $sheet->getColumnDimension($column)->setWidth(45);
            $sheet->getStyle($column.'1')->getFont()->setBold(true);
            $sheet->getStyle($column.'1')->applyFromArray(
                [
                    'fill' => [
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'B8FA86']],
                ]
            );
        }
    }

    public function fillSheet(\PHPExcel_Worksheet $sheet, $items)
    {
        $practiceIndex = 0;
        $j=2;
        foreach ($items as $item) {
            foreach ($item as $result) {
                $i = 0;
                foreach ($result as $index => $value) {
                    if ($index === 'authorId' || $index === 'id') {
                        continue;
                    }

                    if ($index === 'practice') {
                        $value = self::PRACTICE[$practiceIndex];
                    }

                    if ($index === 'isFeedback') {
                        $value = self::FEEDBACK[$value];
                    }

                    $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $value, true));
                }
                $j++;
            }
            $practiceIndex++;
        }
    }

    public function setCellStyles($cell)
    {
        $cell->getStyle()->applyFromArray([
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']]
            ]
        ]);

        $cell->getStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        $cell->getStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $cell->getStyle()->getAlignment()->setWrapText(true);
    }
}
