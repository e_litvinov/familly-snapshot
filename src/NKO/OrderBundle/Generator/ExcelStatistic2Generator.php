<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 19.11.18
 * Time: 13.18
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use NKO\OrderBundle\Utils\NameListGenerator\ApplicationNames;
use PHPExcel_Cell;
use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet;
use Symfony\Component\HttpFoundation\Response;

class ExcelStatistic2Generator extends FileGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/statistic2_report.xlsx';
    const VIEW_FILE_NAME = 'statistic_table2';

    const START_ROW = 6;
    const COLUMN_START = 3;

    /**
     * @var string $rootDir
     */
    private $rootDir;

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
    }

    public function generate($parameters)
    {
        $filename = $this->rootDir . self::FILE_NAME;
        $tempFileName = tempnam(sys_get_temp_dir(), 'Second_summary_analytics');
        $tempOutFileName = $tempFileName.'.xlsx';
        file_put_contents($tempOutFileName, $filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);
        /** @var PHPExcel_Worksheet $sheet */
        $sheet = $excelObj->getActiveSheet();
        $results = $parameters['results'];

        $columnsNumber = self::COLUMN_START;
        foreach ($results as $element) {
            foreach ($element['results'] as $column) {
                $this->fillColumn($column, $sheet, $columnsNumber++);
            }
        }

        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);

        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME);
    }

    private function fillColumn(array $column, PHPExcel_Worksheet $sheet, $columnNumber)
    {
        $rowNumber = self::START_ROW;

        foreach ($column as $row) {
            $sheet->setCellValueByColumnAndRow($columnNumber, $rowNumber++, $row['plan']);
            $sheet->setCellValueByColumnAndRow($columnNumber, $rowNumber++, $row['fact']);
        }
    }
}
