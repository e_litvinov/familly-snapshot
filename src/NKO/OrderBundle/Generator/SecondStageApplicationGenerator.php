<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 5:56 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class SecondStageApplicationGenerator extends FileGenerator
{
    const FIELDS = [
        'name',
        'abbreviation',
        'pSRN',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPosition',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingEmeil',
        'dateRegistrationOfOrganization',
        'primaryActivity',
        'mission',
        'countStaffEmployees',
        'countInvolvingEmployees',
        'countVolunteerEmployees',
        'linkToAnnualReport',
        'iNN',
        'kPP',
        'oKPO',
        'oKVED',
        'bankName',
        'bankLocation',
        'bankINN',
        'bankKPP',
        'correspondentAccount',
        'bIK',
        'paymentAccount',
        'recipientName',
        'oktmo2',
        'kbk2',
        'projectName',
        'deadlineStart',
        'deadlineFinish',
        'projectPurpose',
        'projectRelevance',
        'beneficiaryGroupEtc',
        'childrenCategoryName',
        'projectImplementation',
        'choosingGroundExplanation',
        'dateStartOfInternship',
        'traineeshipFormatName',
        'knowledgeIntroductionDuringProjectImplementation',
        'knowledgeIntroductionAfterProjectImplementation',
        'requestedFinancingMoney',
        'cofinancingMoney',
    ];

    const TABLES = [
        'projects' => [
            'project_num_tb' => self::AUTO_NUM,
            'project_name_tb' => 'projectName',
            'project_date_tb' => 'deadline',
            'project_result_tb' => 'result',
            'project_partners_tb' => 'donorsPartnersAndTheirRole'
        ],
        'publications' => [
            'publication_num_tb' => self::AUTO_NUM,
            'publication_name_tb' => 'publicationName',
            'publication_year_tb' => 'yearPublication',
            'publication_link_tb' => 'link'
        ],
        'employees' => [
            'employee_num_tb' => self::AUTO_NUM,
            'employee_name_tb' => 'fullName',
            'employee_position_tb' => 'position',
            'employee_experience_tb' => 'experienceAndEducation',
            'employee_activity_tb' => 'educationalActivity'
        ],
        'traineeships' => [
            'traineeship_num_tb' => self::AUTO_NUM,
            'traineeship_action_tb' => 'action',
            'traineeship_deadline_tb' => 'deadline',
            'traineeship_location_tb' => 'actionLocation'
        ],
        'measures' => [
            'measure_num_tb' => self::AUTO_NUM,
            'measure_action_tb' => 'action',
            'measure_deadline_tb' => 'deadline',
            'measure_location_tb' => 'actionLocation'
        ],
        'projectResults' => [
            'result_criteria_tb' => 'resultCriteria',
            'target_value_tb' => 'targetValue',
            'approximate_target_value_tb' => 'approximateTargetValue',
            'method_measurement_tb' => 'methodMeasurement'
        ],
        'employeeResults' => [
            'employee_result_num_tb' => self::AUTO_NUM,
            'employee_result_tb' => 'result',
            'employee_result_indicator_tb' => 'indicator',
            'employee_result_target_value_tb' => 'targetValue',
            'employee_result_approximate_target_value_tb' => 'approximateTargetValue',
            'employee_result_method_measurement_tb' => 'methodMeasurement'
        ],
        'beneficiaryResults' => [
            'beneficiary_result_num_tb' => self::AUTO_NUM,
            'beneficiary_result_tb' => 'result',
            'beneficiary_result_indicator' => 'indicator',
            'beneficiary_result_target_value_tb' => 'targetValue',
            'beneficiary_result_approximate_target_value_tb' => 'approximateTargetValue',
            'beneficiary_result_method_measurement_tb' => 'methodMeasurement'
        ],
        'risks' => [
            'key_risk_tb' => 'keyRisk',
            'action_to_reduce_risk_tb' => 'actionToReduceRisk'
        ],
        'siteLinks' => [
            'sitelink_tb' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_tb' => 'link'
        ],
        'beneficiaryGroups' => [
            'beneficiary_group_name_tb' => 'groupName'
        ],
        'childrenCategories' => [
            'children_category_tb' => 'categoryName'
        ],
        'traineeshipFormats' => [
            'traineeship_format' => 'formatName'
        ],
        'socialResults' => [
            'social_result_tb' => 'name'
        ]
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/application_kns2017-2.docx';
    const VIEW_FILE_NAME = 'Zayavka-KNS';

    /**
     * SecondStageApplicationGenerator constructor.
     *
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);
        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGroundParent');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGround');

        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
    }
}