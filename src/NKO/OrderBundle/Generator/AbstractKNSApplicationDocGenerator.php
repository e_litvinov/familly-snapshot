<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccessor;

abstract class AbstractKNSApplicationDocGenerator extends FileGenerator
{
    /**
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param ProxyResolver $proxyResolver
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver
    ) {
        $this->setConstants();
        $this->wordTemplateService = $wordTemplateService;
        $this->proxyResolver = $proxyResolver;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . $this->fileName);
    }

    const FIELDS = [
        'abbreviation',
        'organizationForm',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPosition',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingEmeil',
        'dateRegistrationOfOrganization',
        'primaryActivity',
        'mission',
        'countStaffEmployees',
        'countInvolvingEmployees',
        'countVolunteerEmployees',
        'iNN',
        'kPP',
        'oKPO',
        'oKTMO',
        'oKVED',
        'kBK',
        'bankName',
        'bankLocation',
        'bankINN',
        'bankKPP',
        'correspondentAccount',
        'bIK',
        'paymentAccount',
        'personalAccount',
        'nameAddressee',
        'projectName',
        'deadLineStart',
        'deadLineFinish',
        'projectPurpose',
        'importanceProject',
        'projectImplementation',
        'choosingGroundExplanation',
        'dateStartOfInternship',
        'knowledgeIntroductionDuringProjectImplementation',
        'knowledgeIntroductionAfterProjectImplementation',
        'cofinancingMoney',
        'requestedFinancingMoney',
        'traineeshipFormatName',
        'linkToAnnualReport'
    ];

    const TABLES = [
        'employeeResults' => [
            'project_result_num_td' => self::AUTO_NUM,
            'result_td' => 'result',
            'indicator_td' => 'indicator',
            'target_value_td' => 'targetValue',
            'approximate_target_value_td' => 'approximateTargetValue',
            'method_measurement_td' => 'linkedMethod',
            'comment_td' => 'methodMeasurement',
        ],
        'employees' => [
            'employee_num_td' => self::AUTO_NUM,
            'full_name_td' => 'fullName',
            'position_td' => 'position',
            'experience_and_education_td' => 'experienceAndEducation',
            'educational_activity_td' => 'educationalActivity'
        ],
        'measures' => [
            'measure_num_td' => self::AUTO_NUM,
            'action_td' => 'action',
            'dead_line_td' => 'deadline',
            'action_location_td' => 'actionLocation',
        ],
        'projectResults' => [
            'approximate_target_value_td' => 'approximateTargetValue',
            'result_criteria_td' => 'resultCriteria',
            'target_value_td' => 'targetValue',
            'linked_method_td' => 'linkedMethod',
            'method_measurement_td' => 'methodMeasurement',
        ],
        'beneficiaryResults' => [
            'beneficiary_result_td' => self::AUTO_NUM,
            'result_td' => 'result',
            'indicator_td' => 'indicator',
            'target_value_td' => 'targetValue',
            'approximate_target_value_td' => 'approximateTargetValue',
            'method_measurement_td' => 'linkedMethod',
            'comment_td' => 'methodMeasurement'
        ],
        'risks' => [
            'key_risk_td' => 'keyRisk',
            'action_to_reduce_risk_td' => 'actionToReduceRisk'
        ],
        'siteLinks' => [
            'site_link_td' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_td' => 'link'
        ],
        'projects' => [
            'project_num_td' => self::AUTO_NUM,
            'project_name_td' => 'projectName',
            'project_date_td' => 'deadline',
            'project_result_td' => 'result',
            'financing_source_type_td' => 'humanizedFinancingSourceTypes'
        ],
        'publications' => [
            'publication_name_td' => 'publicationName',
            'publication_link_td' => 'link'
        ],
        'otherBeneficiaryGroups' => [
            'group_num_td' => self::AUTO_NUM,
            'group_name_td' => 'name',
        ],
        'socialResults' => [
            'social_name_td' => 'name'
        ],
        'traineeshipFormats' => [
            'traineeship_formats_td' => 'formatName'
        ],
    ];

    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'category_name_td';

    /**
     * @var ProxyResolver $proxyResolver
     */
    protected $proxyResolver;

    protected $tables;
    protected $fields;
    protected $fileName;
    protected $viewFileName;

    /** Method for initialize constants */
    abstract protected function setConstants();

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);

        $this->setObject($object);
        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), $this->viewFileName);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, $this->viewFileName);
    }

    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->fillAuthorData();
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME);
        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGroundParent');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGround');
        $this->wordTemplateService->fillTemplate($this->fields);
        $this->wordTemplateService->fillTables($this->tables);
    }

    protected function fillAuthorData()
    {
        $object = $this->proxyResolver->resolveProxies($this->object);
        $this->document->setValue('nko_name', $object->getAuthor()->getNkoName());
        $this->document->setValue('psrn', $object->getAuthor()->getPsrn());

        $dateSendApplication = $this->getObject()->getIsSend() ? $this->object->getUpdatedAt()->format('d.m.Y') : null;
        $this->document->setValue('updatedAt', $dateSendApplication);
    }
}
