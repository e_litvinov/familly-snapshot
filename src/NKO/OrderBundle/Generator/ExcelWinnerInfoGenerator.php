<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/8/17
 * Time: 3:07 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Resolver\RelatedApplicationsResolver;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017Application;

class ExcelWinnerInfoGenerator
{
    /**
     * @var ExcelDownloader $downloader
     */
    private $downloader;

    /**
     * @var string $rootDir
     */
    private $rootDir;

    /**
     * @var RelatedApplicationsResolver $relatedApplicationsResolver
     */
    private $relatedApplicationsResolver;

    /**
     * ExcelWinnerInfoGenerator constructor.
     * @param string $rootDir
     * @param ExcelDownloader $downloader
     * @param RelatedApplicationsResolver $relatedApplicationsResolver
     */
    public function __construct(
        $rootDir,
        ExcelDownloader $downloader,
        RelatedApplicationsResolver $relatedApplicationsResolver
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
        $this->relatedApplicationsResolver = $relatedApplicationsResolver;
    }

    public function getWinnersTable($applications, $className)
    {
        $excelObj = null;
        $tempFilename = null;

        switch ($className) {
            case Farvater2017Application::class:
                $filename = $this->rootDir . '/../src/NKO/OrderBundle/Resources/templates/winners.xls';
                $tempFilename = $this->downloader->getTempExcelFilename($filename);
                $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
                $excelObj = $excelReader->load($filename);
                $this->setFarvater2017WinnersExcel($applications,$excelObj->getActiveSheet());
                break;
        }

        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempFilename);

        return $tempFilename;
    }

    private function setFarvater2017WinnersExcel($applications, $sheet)
    {

        $applications = $this->relatedApplicationsResolver->getRelatedApplications($applications, 'NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication');

        $i = 2;
        $appsCount = count($applications);
        foreach ($applications as $application) {
            if ($application instanceof ApplicationHistory) {
                $application = unserialize($application->getData());
            }
            if($application instanceof BriefApplication) {
                $sheet->setCellValueByColumnAndRow(0, $i, $application->getName());
                $sheet->setCellValueByColumnAndRow(1, $i, $application->getAbbreviation());
                $sheet->setCellValueByColumnAndRow(2, $i, $application->getLegalRegion());
                $practiceRegions = '';
                $regions = $application->getPracticeRegions();
                foreach ($regions as $region) {
                    $practiceRegions .= $region->getName() . PHP_EOL;
                }
                $sheet->setCellValueByColumnAndRow(3, $i, $practiceRegions);
                $sheet->setCellValueByColumnAndRow(4, $i, $application->getPriorityDirection());
                $sheet->setCellValueByColumnAndRow(5, $i, $application->getPriorityDirectionEtc());
                $sheet->setCellValueByColumnAndRow(6, $i, $application->getProjectName());
                $sheet->setCellValueByColumnAndRow(7, $i, $application->getEmail());
                $sheet->setCellValueByColumnAndRow(8, $i, '+7' . " "
                    . $application->getPhoneCode() . " "
                    . $application->getPhone());
                $sheet->setCellValueByColumnAndRow(9, $i, $application->getHeadOfOrganizationFullName());
                $sheet->setCellValueByColumnAndRow(10, $i, $application->getHeadOfOrganizationPosition());
                $sheet->setCellValueByColumnAndRow(11, $i,
                $application->getHeadOfOrganizationPhoneCode() . $application->getHeadOfOrganizationPhone());
                $sheet->setCellValueByColumnAndRow(12, $i,
                $application->getHeadOfOrganizationMobilePhoneCode() . $application->getHeadOfOrganizationMobilePhone());
                $sheet->setCellValueByColumnAndRow(13, $i, $application->getHeadOfOrganizationEmeil());

                $sheet->setCellValueByColumnAndRow(14, $i, $application->getHeadOfProjectFullName());
                $sheet->setCellValueByColumnAndRow(15, $i, $application->getHeadOfProjectPosition());
                $sheet->setCellValueByColumnAndRow(16, $i,
                $application->getHeadOfProjectPhoneCode() . $application->getHeadOfProjectPhone());
                $sheet->setCellValueByColumnAndRow(17, $i,
                    $application->getHeadOfProjectMobilePhoneCode() . $application->getHeadOfProjectMobilePhone());
                $sheet->setCellValueByColumnAndRow(18, $i, $application->getHeadOfProjectEmeil());

                $sheet->setCellValueByColumnAndRow(19, $i, $application->getHeadOfAccountingFullName());
                $sheet->setCellValueByColumnAndRow(20, $i,
                $application->getHeadOfAccountingPhoneCode() . $application->getHeadOfAccountingPhone());
                $sheet->setCellValueByColumnAndRow(21, $i, $application->getHeadOfAccountingEmeil());
                $sites = '';
                $socialNetworks = '';
                foreach($application->getSiteLinks() as $site){
                    $sites .= $site->getLink() . ' ';
                }
                foreach($application->getSocialNetworkLinks() as $site){
                    $socialNetworks .= $site->getLink() . ' ';
                }

                $sheet->setCellValueByColumnAndRow(22, $i, $sites);
                $sheet->setCellValueByColumnAndRow(23, $i, $socialNetworks);

                $sheet->setCellValueByColumnAndRow(24, $i, $application->getPsrn() . ' ');

                $i++;
            }
        }

        $sheet->getStyle('A1:T' . (--$i))->applyFromArray($this->setTableBorder());

        foreach (range('A', 'M') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
            for($i = 0; $i < $appsCount + 1; $i++) {
                $sheet->getStyle($columnID.($i+1))->getAlignment()->setWrapText(true);
                $sheet->getRowDimension($i + 1)->setRowHeight(-1);
            }
            if($columnID == "A") {
                $sheet->getColumnDimension($columnID)->setAutoSize(false);
                $sheet->getColumnDimension($columnID)->setWidth(500);
            }
        }
    }

    public function setTableBorder()
    {
        return array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'))
            ));
    }
}