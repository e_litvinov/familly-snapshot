<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/09/18
 * Time: 10:48 AM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\ExcelDownloader;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MethodInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Resolver\ProxyResolver;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017Application;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as SecondStageApplication;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;
use NKO\UserBundle\Entity\NKOUser;
use PHPExcel_Style_Border;

class ExcelStatistic1Generator extends FileGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/statistic1_report.xlsx';
    const VIEW_FILE_NAME = 'statistic_table1';

    const START_ROW = [
        'firstSheet' => 7,
        'lastSheet' => 3,
        'default' => 4,
    ];

    /**
     * @var string $rootDir
     */
    private $rootDir;

    private $defaultValueCell;

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
    }

    public function generate($object)
    {
        $filename = $this->rootDir . self::FILE_NAME;
        $tempFileName = tempnam(sys_get_temp_dir(), 'Summary_analytics');
        $tempOutFileName = $tempFileName.'.xlsx';
        file_put_contents($tempOutFileName, $filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        $filterData = array_pop($object);
        foreach ($object as $key => $shareResults) {
            $excelObj->setActiveSheetIndex($key);
            $sheet = $excelObj->getActiveSheet();

            if ($key === 0) {
                $title = 'Тип отчета: '.$filterData['reportForm']->getTitle().'. Период: c "'.$filterData['start']->getName().'" по "'.$filterData['finish']->getName().'"';
                $sheet->setCellValueByColumnAndRow(0, 2, $title);
            }

            $this->fillSheet($sheet, $shareResults, $this->getDisplayMethodData($object, $shareResults));
        }

        $excelObj->setActiveSheetIndex(0);
        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);

        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME);
    }

    public function fillSheet(\PHPExcel_Worksheet $sheet, $shareResults, $featureSheet)
    {
        $j = self::START_ROW[$featureSheet];
        $countRow = 1;
        $currentSum = [];
        $this->defaultValueCell = '-';

        foreach ($shareResults as $organization) {
            $i = 0;
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $countRow++, true));
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, reset($organization)['author'], true));
            $currentSum = $this->fillRow($organization, $sheet, $j, $featureSheet, $currentSum);
            $j++;
        }
        $this->defaultValueCell = null;

        $cell = $sheet->setCellValueByColumnAndRow(0, $j, 'ИТОГО', true);
        $cell->getStyle()->getFont()->setBold(true);
        $this->setCellStyles($cell);

        $this->setCellStyles($sheet->getCellByColumnAndRow(1, $j));
        $this->fillRow($currentSum, $sheet, $j, $featureSheet);
    }

    public function fillRow($items, $sheet, $j, $featureSheet, $currentSum = null)
    {
        $i = 2;
        foreach ($items as $result) {
            if ($featureSheet === 'firstSheet' && !next($items)) {
                $this->setValuesToCells($i, $j, $sheet, $result, true);
            } elseif ($featureSheet === 'lastSheet') {
                $this->setValuesToCells($i, $j, $sheet, $result, true);
            } else {
                $this->setValuesToCells($i, $j, $sheet, $result, false);
            }
            $currentSum = isset($currentSum) ? $this->sumTotalValues($currentSum, $i, $result) : null;
        }
        return $currentSum;
    }

    public function setValuesToCells(&$i, $j, $sheet, $result, $isThirdValue)
    {
        if ($isThirdValue) {
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $result['isSend'] && isset($result['name']) ? $result['name'] : $this->defaultValueCell, true));
        }

        $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $result['isSend'] && isset($result['plan']) ? $result['plan'] : 0, true));
        $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $result['isSend'] && isset($result['fact']) ? $result['fact'] : 0, true));
    }

    public function sumTotalValues($currentSum, $key, $result)
    {
        if (!array_key_exists($key, $currentSum)) {
            $currentSum[$key]['fact'] = 0;
            $currentSum[$key]['plan'] = 0;
            $currentSum[$key]['isSend'] = true;
        }

        if ($result['isSend']) {
            $currentSum[$key]['fact'] += $result['fact'];
            $currentSum[$key]['plan'] += $result['plan'];
        }

        return $currentSum;
    }

    public function getDisplayMethodData($object, $item)
    {
        switch ($item) {
            case reset($object):
                return 'firstSheet';
                break;
            case end($object):
                return 'lastSheet';
                break;
            default:
                return 'default';
                break;
        }
    }

    public function setCellStyles($cell)
    {
        $cell->getStyle()->applyFromArray([
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'wrap' => true
            ]
        ]);
    }
}
