<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Resolver\CollectionResolver;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;
use NKO\OrderBundle\Traits\HumanizedProperties;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application;
use NKO\OrderBundle\Resolver\ProxyResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class Farvater2018ApplicationDocGenerator extends FileGenerator
{
    use HumanizedProperties;

    const FIELDS_RELATED_APPLICATION = [
        'abbreviation',
        'organizationForm',
        'legalPostCode',
        'legalRegion',
        'legalCity',
        'legalStreet',
        'actualPostCode',
        'actualRegion',
        'actualCity',
        'actualStreet',
        'email',
        'phone',
        'phoneCode',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPosition',
        'headOfProjectPhone',
        'headOfProjectPhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhone',
        'headOfAccountingPhoneCode',
        'headOfAccountingEmeil',
        'dateRegistrationOfOrganization',
        'primaryActivity',
        'mission',
        'countStaffEmployees',
        'countInvolvingEmployees',
        'countVolunteerEmployees',
        'anotherFinancingSource',
        'projectName',
        'priorityDirection',
        'priorityDirectionEtc',
        'briefPracticeDescription',
        'isReadyEtc',
        'effectivenessFact',
        'effectivenessPlan',
        'advantages',
        'disadvantages',
    ];

    const TABLES_RELATED_APPLICATION = [
        'siteLinks' => [
            'site_link_td' => 'link'
        ],
        'socialNetworkLinks' => [
            'social_network_link_td' => 'link'
        ],
        'projects' => [
            'project_num_td' => self::AUTO_NUM,
            'project_name_td' => 'projectName',
            'project_date_td' => 'deadline',
            'project_result_td' => 'result',
            'project_partners_td' => 'donorsPartnersAndTheirRole'
        ],
        'publications' => [
            'publication_name_td' => 'publicationName',
            'publication_link_td' => 'link'
        ],
        'financingSources' => [
            'source_type_td' => 'type',
            'source_value_td' => 'value',
        ],
        'practiceRegions' => [
            'region_name_td' => 'name',
        ],
        'otherBeneficiaryGroups' => [
            'group_num_td' => self::AUTO_NUM,
            'group_name_td' => 'name',
        ],
        'beneficiaryProblems' => [
            'problem_num_td' => self::AUTO_NUM,
            'problem_target_group_td' => 'targetGroup',
            'problem_content_td' => 'content',
            'problem_social_result_td' => 'humanizedSocialResults',
            'problem_custom_result_td' => 'customSocialResult',
        ],
        'effectivenessItems' => [
            'eff_result_td' => 'result',
            'eff_indicator_td' => 'indicator',
            'eff_indicator_value_td' => 'indicatorValue',
        ],
        'effectivenessEtcItems' => [
            'eff_etc_result_td' => 'result',
            'eff_etc_indicator_td' => 'customIndicator',
            'eff_etc_indicator_value_td' => 'indicatorValue',
        ],
        'isReady' => [
            'is_ready_td' => 'name',
        ],
    ];

    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'category_name_td';

    const TABLES = [
        'practiceImplementationActivities' => [
            'implemetation_num_td' => self::AUTO_NUM,
            'implementation_activity_td' => 'activity',
            'implementation_dead_line_td' => 'deadLine',
            'implementation_expected_results_td' => 'expectedResults',
        ],
        'practiceSpreadActivities' => [
            'spread_num_td' => self::AUTO_NUM,
            'spread_activity_td' => 'activity',
            'spread_dead_line_td' => 'deadLine',
            'spread_expected_results_td' => 'expectedResults',
        ],
        'monitoringResultsActivities' => [
            'monitoring_implemetation_num_td' => self::AUTO_NUM,
            'monitoring_activity_td' => 'activity',
            'monitoring_dead_line_td' => 'deadLine',
            'monitoring_expected_results_td' => 'expectedResults',
        ],
        'projectMembers' => [
            'full_name_td' => 'fullName',
            'role_td' => 'role',
            'functional_responsibilities_td' => 'functionalResponsibilities',
            'employment_relationship_td' => 'employmentRelationship',
            'brief_information_td' => 'briefInformation',
        ],
        'projectPartners' => [
            'organization_name_td' => 'organizationName',
            'brief_information_td' => 'briefInformation',
            'project_participation_td' => 'projectParticipation',
        ],
        'organizationResources' => [
            'name_td' => 'name',
            'description_td' => 'description',
        ],
        'experienceItems' => [
            'experience_num_td' => self::AUTO_NUM,
            'direction_td' => 'direction',
            'indicator_td' => 'indicator',
            'comment_td' => 'comment',
        ],
        'experienceEtcItems' => [
            'experience_etc_num_td' => self::AUTO_NUM,
            'custom_direction_etc_td' => 'customDirection',
            'indicator_etc_td' => 'indicator',
            'comment_etc_td' => 'comment',
        ],
        'processItems' => [
            'process_items_num_td' => self::AUTO_NUM,
            'process_direction_td' => 'direction',
            'process_indicator_td' => 'indicator',
            'process_comment_td' => 'comment',
        ],
        'processEtcItems' => [
            'process_items_etc_num_td' => self::AUTO_NUM,
            'process_custom_direction_etc_td' => 'customDirection',
            'process_indicator_etc_td' => 'indicator',
            'process_comment_etc_td' => 'comment',
        ],
        'qualificationItems' => [
            'qualification_items_num_td' => self::AUTO_NUM,
            'qualification_direction_td' => 'direction',
            'qualification_indicator_td' => 'indicator',
            'qualification_comment_td' => 'comment',
        ],
        'qualificationEtcItems' => [
            'qualification_items_etc_num_td' => self::AUTO_NUM,
            'qualification_custom_direction_etc_td' => 'customDirection',
            'qualification_indicator_etc_td' => 'indicator',
            'qualification_comment_etc_td' => 'comment',
        ],
        'resourceBlockItems' => [
            'resource_items_num_td' => self::AUTO_NUM,
            'resource_direction_td' => 'direction',
            'resource_indicator_td' => 'indicator',
            'resource_comment_td' => 'comment',
        ],
        'resourceBlockEtcItems' => [
            'resource_items_etc_num_td' => self::AUTO_NUM,
            'resource_custom_direction_etc_td' => 'customDirection',
            'resource_indicator_etc_td' => 'indicator',
            'resource_comment_etc_td' => 'comment',
        ],
        'risePotentialApplications' => [
            'rise_potential_num_td' => self::AUTO_NUM,
            'rise_potential_title_td' => 'title',
            'rise_potential_context_td' => 'context',
        ],
        'risks' => [
            'risks_num_td' => self::AUTO_NUM,
            'key_risk_td' => 'keyRisk',
            'action_to_reduce_risk_td' => 'actionToReduceRisk',
        ],
        'realizationPracticeApplications' => [
            'realization_practice_num_td' => self::AUTO_NUM,
            'realization_practice_title_td' => 'title',
            'realization_practice_context_td' => 'context',
        ],
        'realizationPractices' => [
            'realization_practices_num_td' => self::AUTO_NUM,
            'realization_practices_key_risk_td' => 'keyRisk',
            'realization_practices_action_to_reduce_td' => 'actionToReduceRisk',
        ],
        'otherSpecialistTargetGroups' => [
            'other_specialist_target_group_num_td' => self::AUTO_NUM,
            'other_specialist_target_group_name_td' => 'name',
        ],
        'introductionPractices' => [
            'introduction_practices_index_name_td' => 'indexName',
            'introduction_practices_first_td' => 'firstYearTargetValue',
            'introduction_practices_second_td' => 'secondYearTargetValue',
            'introduction_practices_third_td' => 'thirdYearTargetValue',
        ],
        'spreadPracticeApplications' => [
            'spread_practice_num_td' => self::AUTO_NUM,
            'spread_practice_title_td' => 'title',
            'spread_practice_context_td' => 'context',
        ],
        'spreadPractices' => [
            'spread_practices_num_td' => self::AUTO_NUM,
            'spread_practices_key_risk_td' => 'keyRisk',
            'spread_practices_action_td' => 'actionToReduceRisk',
        ],
        'specialistTargetGroups' => [
            'title_target_group' => 'title'
        ],
        'projectSocialResults' => [
            'socialResult_td' => 'socialResult',
            'targetGroup_td' => 'targetGroup',
            'indicator_td' => 'indicator',
            'targetValue_td' => 'targetValue',
            'approximateValue_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'projectSocialIndividualResults' => [
            'socialResult_td' => 'socialResult',
            'targetGroup_td' => 'targetGroup',
            'indicator_td' => 'customIndicator',
            'targetValue_td' => 'targetValue',
            'approximateValue_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'projectDirectResults' => [
            'content_td' => 'content',
            'targetGroup_td' => 'targetGroup',
            'indicator_td' => 'indicator',
            'targetValue_td' => 'targetValue',
            'approximateValue_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'projectDirectIndividualResults' => [
            'content_td' => 'content',
            'targetGroup_td' => 'targetGroup',
            'indicator_td' => 'customIndicator',
            'targetValue_td' => 'targetValue',
            'approximateValue_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'practiceSocialResults' => [
            'socialResult_td' => 'socialResult',
            'indicator_td' => 'customIndicator',
            'targetValue_td' => 'targetValue',
            'approximateValue_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'practiceDirectResults' => [
            'content_td' => 'content',
            'target_group_td' => 'targetGroup',
            'indicator_td' => 'indicator',
            'target_value_td' => 'targetValue',
            'approximate_value_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'practiceDirectIndividualResults' => [
            'content_td' => 'content',
            'targetGroup_td' => 'targetGroup',
            'indicator_td' => 'customIndicator',
            'targetValue_td' => 'targetValue',
            'approximateValue_td' => 'approximateValue',
            'service_td' => 'service'
        ],
        'specialistProblems' => [
            'specialist_problem_num_td' => self::AUTO_NUM,
            'specialist_problem_target_group_td' => 'targetGroup',
            'specialist_problem_content_td' => 'content',
            'specialist_problem_social_results_td' => 'humanizedSocialResults',
            'specialist_problem_custom_social_result_td' => 'customSocialResult',
        ],
    ];

    const FIELDS = [
        'practiceImplementationDescription',
        'practiceSpreadDescription',
        'monitoringResultsDescription',
        'entirePeriodEstimatedFinancing',
        'entirePeriodEstimatedCoFinancing',
        'firstYearEstimatedFinancing',
        'firstYearEstimatedCoFinancing',
        'wishDevelopPractice',
        'wishDevelopPractice',
        'developmentSectionComment',
        'projectPurpose',
        'tasksHeadTarget',
        'descriptionSectionComment',
        'motivationSpreadPractice',
        'experienceSpread',
        'technologySpread',
        'introductionProjectPurpose',
        'taskOfTarget',
        'commentIntroductionPractice',
        'spreadSectionComment',
        'projectSocialComment',
        'projectDirectComment',
        'practiceSocialComment',
        'practiceDirectComment',
        'latestAnnualReportLink',
        'ministryJusticeReportLink',
        'monitoringProjectPurpose'
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/application_farvater2018.docx';
    const VIEW_FILE_NAME = 'Farvater_application_2018';

    /**
     * @var RelatedCompetitionResolver $relatedCompetitionResolver
     */
    protected $relatedCompetitionResolver;

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var CollectionResolver $collectionResolver
     */
    private $collectionResolver;

    /**
     * Farvater2018ApplicationDocGenerator constructor.
     *
     * @param $rootDir
     * @param PropertyAccessor $propertyAccessor
     * @param FillWordTemplateService $wordTemplateService
     * @param DocDownloader $downloader
     * @param ProxyResolver $proxyResolver
     * @param RelatedCompetitionResolver $relatedCompetitionResolver
     * @param EntityManager $entityManager
     * @param CollectionResolver $collectionResolver
     */
    public function __construct(
        $rootDir,
        PropertyAccessor $propertyAccessor,
        FillWordTemplateService $wordTemplateService,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver,
        RelatedCompetitionResolver $relatedCompetitionResolver,
        EntityManager $entityManager,
        CollectionResolver $collectionResolver
    ) {
        $this->accessor = $propertyAccessor;
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->proxyResolver = $proxyResolver;
        $this->relatedCompetitionResolver = $relatedCompetitionResolver;
        $this->entityManager = $entityManager;
        $this->collectionResolver = $collectionResolver;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
    }

    /**
     * @param mixed $object
     * @param null $history
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object, $history = null)
    {
        Settings::setOutputEscapingEnabled(true);

        $this->setObject($object);

        $this->fillTemplate($object, self::FIELDS, self::TABLES);

        $isSpread = $history ? $history->getIsSpread() : null;
        $relatedApplication = $this->relatedCompetitionResolver->getApplicationHistory($object, $isSpread);

        if ($relatedApplication) {
            $dataRelatedApplication = unserialize($relatedApplication->getData());

            $organizationForm = $this->entityManager->getRepository(OrganizationForm::class)->find($dataRelatedApplication->getOrganizationForm()->getId());
            $dataRelatedApplication->setOrganizationForm($organizationForm);

            $this->fillTemplate($dataRelatedApplication, self::FIELDS_RELATED_APPLICATION, self::TABLES_RELATED_APPLICATION);
        }

        $this->fillIndexes();
        $this->fillAuthorData();

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate($application, $fields, $tables)
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($application);
        $this->wordTemplateService->fillTemplate($fields);
        $this->wordTemplateService->fillTables($tables);

        if ($application instanceof Application) {
            $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME);
        }
    }

    private function fillAuthorData()
    {
        $author = $this->getObject()->getAuthor();
        $author = $this->proxyResolver->resolveProxies($author);
        $this->document->setValue('nko_name', $author->getNkoName());
        $this->document->setValue('psrn', $author->getPsrn());
    }

    private function fillIndexes()
    {
        $sentApplication = $this->entityManager->getRepository(ApplicationHistory::class)->findSpreadByAuthor(
            $this->object->getAuthor(),
            $this->object->getCompetition()->getId()
        );
        $this->document->setValue('number_spread', $sentApplication ? $sentApplication->getNumber() : null);
        $this->document->setValue('contract', $sentApplication ? $sentApplication->getContract(): null);
    }
}
