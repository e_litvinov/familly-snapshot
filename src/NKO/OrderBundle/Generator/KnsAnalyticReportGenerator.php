<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 12/6/17
 * Time: 12:27 PM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class KnsAnalyticReportGenerator extends FileGenerator
{
    const  FIELDS = [
        'projectName',
        'startDateProject',
        'finishDateProject',
        'directorName',
        'directorPosition',
        'directorPhone',
        'directorMobilePhone',
        'directorEmail',
        'priorityDirection',
        'priorityDirectionEtc',
        'projectPurpose',
        'trainingGroundParent',
        'trainingGround',
        'expectationsAndReality',
        'trainingStart',
        'trainingEnd',
        'comment',
        'projectExperience',
        'nextExperience'

    ];
    const TABLES = [
        'traineeshipFormats' => [
            'traineeship_format' => 'formatName'
        ],
        'reportTrainingEvents' => [
            'tb_num' => self::AUTO_NUM,
            'tb_event' => 'trainingEvent',
            'tb_start' => 'start',
            'tb_end' => 'end',
            'tb_place' => 'place',
            'tb_employee_count' => 'participantCount'
        ],
        'directEmployeeResults' => [
            'tb_result' => 'result',
            'tb_plan' => 'plan',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
            ],
        'qualitativeEmployeeResults' => [
            'tb_num' => self::AUTO_NUM,
            'tb_result' => 'result',
            'tb_indicator' => 'indicator',
            'tb_plan' => 'plan',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
        ],
        'otherQualitativeEmployeeResults' => [
            'tb_num' => self::AUTO_NUM,
            'tb_result' => 'result',
            'tb_indicator' => 'indicator',
            'tb_plan' => 'plan',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
        ],
        'socialReportResults' => [
            'tb_num' => self::AUTO_NUM,
            'tb_result' => 'result',
            'tb_indicator' => 'indicator',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
        ],
        'otherSocialResults' => [
            'tb_num' => self::AUTO_NUM,
            'tb_result' => 'result',
            'tb_indicator' => 'indicator',
            'tb_plan' => 'plan',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
        ],
        'beneficiaryResults' => [
            'tb_num' => self::AUTO_NUM,
            'tb_target_group' => 'targetGroup',
            'tb_result' => 'result',
            'tb_indicator' => 'indicator',
            'tb_plan' => 'plan',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
        ],
        'otherBeneficiaryResults' => [
            'tb_num' => self::AUTO_NUM,
            'tb_custom_target_group' => 'customTargetGroup',
            'tb_result' => 'result',
            'tb_indicator' => 'indicator',
            'tb_plan' => 'plan',
            'tb_fact' => 'fact',
            'tb_expected' => 'expected',
            'tb_measurementMethod' => 'measurementMethod',
            'tb_comment' => 'comment'
        ],
        'lessons' => [
            'tb_num' => self::AUTO_NUM,
            'tb_context' => 'context'
        ],
        'successStories' => [
            'tb_num' => self::AUTO_NUM,
            'tb_context' => 'context'
        ],
        'feedbackItems' => [
            'tb_num' => self::AUTO_NUM,
            'tb_context' => 'context'
        ],
        'trainingEventAttachments' => [
            'tb_num' => self::AUTO_NUM,
            'tb_content'  => 'content'
        ],
        'etcAttachments' => [
            'tb_num' => self::AUTO_NUM,
            'tb_content'  => 'content'
        ]
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/kns_analytic_report.docx';
    const VIEW_FILE_NAME = 'KNS_Report';

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * KnsAnalyticReportGenerator constructor.
     *
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param EntityManager $entityManager
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        EntityManager $entityManager
    ) {
        $this->accessor = $propertyAccessor;
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->entityManager = $entityManager;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);

        $this->setObject($object);
        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillCustomFields()
    {
        $author = $this->entityManager->getRepository(NKOUser::class)->find($this->object->getAuthor()->getId());
        $this->document->setValue('author', $author->getNkoName());

        $grant = $this->entityManager->getRepository(GrantConfig::class)
            ->findOneBy([
                'reportForm' => $this->object->getReportForm(),
                'applicationHistory' => $this->object->getApplicationHistory()
            ]);
        $this->document->setValue('contract', $grant->getContract());
        $this->document->setValue('sumGrant', $grant->getSumGrant());
    }

    private function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->getDocument());
        $this->wordTemplateService->setObject($this->getObject());

        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGroundParent');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGround');
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);
        $this->fillCustomFields();
    }

}