<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 2/15/18
 * Time: 11:12 AM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\PropertyAccess\PropertyAccess;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ContinuationApplicationDocGenerator extends FileGenerator
{
    const  FIELDS = [
        'projectName',
        'deadLineStart',
        'deadLineFinish',
        'sumGrant',
        'headOfOrganizationFullName',
        'headOfOrganizationPosition',
        'headOfAccountingFullName',
        'headOfAccountingPosition',
        'accountantPosition',
        'briefPracticeDescription',
        'practiceChanges',
        'beneficiaryGroupsProblems',
        'projectPurpose',
        'introductionProjectPurpose',
        'potential',
        'monitoringProjectPurpose'

    ];

    const TABLES = [
        'otherBeneficiaryGroups' => [
            'tb_num' => self::AUTO_NUM,
            'tb_other_beneficiary_group' => 'name'
        ],
        'beneficiaryProblems' => [
            'tb_num' => self::AUTO_NUM,
            'target_group' => 'targetGroup',
            'content' => 'content',
            'socialResults' => 'humanizedSocialResults',
            'customSocialResult' => 'customSocialResult'
        ],
        'practiceRegions' => [
            'region_name' => 'name'
        ],
        'expectedResults' => [
            'expected_tb_num' => self::AUTO_NUM,
            'expected_service' => 'service',
            'expected_is_public_event' => 'isPublicEvent',
            'expected_target_group' => 'problem',
            'expected_start_date_fact' => 'startDateFact',
            'expected_finish_date_fact' => 'finishDateFact',
            'expected_locality' => 'locality',
            'expected_service_fact_value' => 'serviceFactValue',
            'expected_beneficiary_fact_value' => 'beneficiaryFactValue',
            'expected_service_plan_value' => 'servicePlanValue',
            'expected_beneficiary_plan_value' => 'beneficiaryPlanValue',
            'expected_is_feedback' => 'isFeedback',
            'expected_methods' => 'humanizedMethods',
            'expected_customMethod' => 'comment'
        ],
        'sustainabilityPracticeApplications' => [
            'tb_num' => self::AUTO_NUM,
            'context' => 'context'
        ],
        'realizationPracticeApplications' => [
            'tb_num' => self::AUTO_NUM,
            'title' => 'title',
            'context' => 'context'
        ],
        'effectivenessImplementationItems' => [
            'result' => 'result',
            'indicator' => 'indicator',
            'fact_value' => 'factValue',
            'plan_value' => 'planValue',
            'methods' => 'humanizedMethods',
            'customMethod' => 'comment'
        ],
        'effectivenessImplementationEtcItems' => [
            'result' => 'result',
            'indicator' => 'customIndicator',
            'fact_value' => 'factValue',
            'plan_value' => 'planValue',
            'methods' => 'humanizedMethods',
            'customMethod' => 'comment'
        ],
        'specialistTargetGroups' => [
            'title_target_group' => 'title'
        ],
        'otherSpecialistTargetGroups' => [
            'tb_num' => self::AUTO_NUM,
            'target_group_name' => 'name'
        ],
        'specialistProblems' => [
            'tb_num' => self::AUTO_NUM,
            'target_group' => 'targetGroup',
            'specialist_competition' => 'humanizedSpecialistCompetitions',
            'content' => 'content',
            'socialResults' => 'humanizedSocialResults',
            'customSocialResult' => 'customSocialResult'
        ],
        'practiceResults' => [
            'tb_num' => self::AUTO_NUM,
            'service' => 'service',
            'is_public_event' => 'isPublicEvent',
            'practice_format' => 'practiceFormat',
            'target_group' => 'problem',
            'start_date_fact' => 'startDateFact',
            'finish_date_fact' => 'finishDateFact',
            'locality' => 'locality',
            'service_fact_value' => 'serviceFactValue',
            'beneficiary_fact_value' => 'beneficiaryFactValue',
            'service_plan_value' => 'servicePlanValue',
            'beneficiary_plan_value' => 'beneficiaryPlanValue',
            'methods' => 'humanizedMethods',
            'is_feedback' => 'isFeedback',
            'customMethod' => 'comment'
        ],
        'effectivenessDisseminationItems' => [
            'result' => 'result',
            'indicator' => 'indicator',
            'fact_value' => 'factValue',
            'plan_value' => 'planValue',
            'methods' => 'humanizedMethods',
            'customMethod' => 'comment'
        ],
        'effectivenessDisseminationEtcItems' => [
            'result' => 'result',
            'indicator' => 'customIndicator',
            'fact_value' => 'factValue',
            'plan_value' => 'planValue',
            'methods' => 'humanizedMethods',
            'customMethod' => 'comment'
        ],
        'sustainabilitySpreadApplications' => [
            'tb_num' => self::AUTO_NUM,
            'context' => 'context'
        ],
        'spreadPracticeApplications' => [
            'tb_num' => self::AUTO_NUM,
            'title' => 'title',
            'context' => 'context'
        ],
        'experienceItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'direction',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'experienceEtcItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'customDirection',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'processItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'direction',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'processEtcItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'customDirection',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'qualificationItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'direction',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'qualificationEtcItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'customDirection',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'resourceBlockItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'direction',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'resourceBlockEtcItems' => [
            'tb_num' => self::AUTO_NUM,
            'direction' => 'customDirection',
            'indicator' => 'indicator',
            'measurement_method' => 'measurementMethod',
            'comment' => 'comment'
        ],
        'sustainabilityMonitoringApplications' => [
            'tb_num' => self::AUTO_NUM,
            'context' => 'context'
        ],
        'risePotentialApplications' => [
            'tb_num' => self::AUTO_NUM,
            'title' => 'title',
            'context' => 'context'
        ],
        'projectTeams' => [
            'tb_num' => self::AUTO_NUM,
            'name' => 'name',
            'role' => 'role',
            'responsibility' => 'responsibility',
            'relationship' => 'relationship',
            'description' => 'description',
            'is_new' => 'isNew'
        ],
        'projectContPartners' => [
            'tb_num' => self::AUTO_NUM,
            'organization_name' => 'organizationName',
            'brief_description' => 'briefDescription',
            'participation' => 'participation',
            'is_new' => 'isNew'
        ],
        'resourceItems' => [
            'name_resource' => 'nameResourceType',
            'description_resource' => 'descriptionResource'
        ],
        'resourceEtcItems' => [
            'name_resource' => 'nameResource',
            'description_resource' => 'descriptionResource'
        ]

    ];

    const VIEW_FILE_NAME = 'Application';
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/continuation_application_template.docx';
    const PEOPLE_CATEGORY_TEMPLATE_NAME = 'tb_people_category';

    /**
     * @var RelatedCompetitionResolver $relatedCompetitionResolver
     */
    private $relatedCompetitionResolver;

    /**
     * ContinuationApplicationDocGenerator constructor.
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param RelatedCompetitionResolver $relatedCompetitionResolver
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        RelatedCompetitionResolver $relatedCompetitionResolver
    ) {
        $this->accessor = $propertyAccessor;
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->relatedCompetitionResolver = $relatedCompetitionResolver;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);

        $this->fillTemplate();

        $filename = tempnam(sys_get_temp_dir(), self::VIEW_FILE_NAME);
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);

        $this->fillCustomFields();
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);
    }

    private function fillCustomFields()
    {
        $competition = $this->accessor->getValue($this->object, 'competition');
        $this->document->setValue('year', (string)$this->accessor->getValue($competition, 'realizationYear')->format('Y'));
        $this->document->setValue('competition', (string)$this->accessor->getValue($competition, 'name'));
        $this->document->setValue('nko_name', $this->object->getAuthor()->getNkoName());

        $this->document->setValue('priorityDirectionName', mb_convert_encoding($this->object->getPriorityDirection(), 'UTF-8'));
        $applicationHistory = $this->relatedCompetitionResolver->getApplicationHistory($this->getObject());
        $this->document->setValue('contract', mb_convert_encoding($applicationHistory->getContract(), 'UTF-8'));
    }
}
