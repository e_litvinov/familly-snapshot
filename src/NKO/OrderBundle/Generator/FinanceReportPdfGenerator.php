<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 4:03 PM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\PdfDownloader;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Resolver\ProxyResolver;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;

class FinanceReportPdfGenerator extends FileGenerator
{
    const VIEW_FILE_NAME = 'Finance_report';
    const FILE_NAME = [
        FinanceReport::class => 'NKOOrderBundle:CRUD/report/finance_report:pdf_template.html.twig',
        FinanceReport2018::class => 'NKOOrderBundle:CRUD/report/finance_report/report_2018:pdf_template.html.twig',
        FinanceReport2019::class => 'NKOOrderBundle:CRUD/report/finance_report/report_2019:pdf_template.html.twig',
    ];

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var TwigEngine $twigEngine
     */
    private $twigEngine;

    /**
     * @var FlashBag $flashBag
     */
    private $flashBag;

    /**
     * FinanceReportPdfGenerator constructor.
     *
     * @param PdfDownloader $downloader
     * @param ProxyResolver $proxyResolver
     * @param EntityManager $entityManager
     * @param TwigEngine $twigEngine
     * @param FlashBag $flashBag
     */
    public function __construct(
        PdfDownloader $downloader,
        ProxyResolver $proxyResolver,
        EntityManager $entityManager,
        TwigEngine $twigEngine,
        FlashBag $flashBag
    ) {
        $this->downloader = $downloader;
        $this->entityManager = $entityManager;
        $this->proxyResolver = $proxyResolver;
        $this->twigEngine = $twigEngine;
        $this->flashBag = $flashBag;
    }

    /**
     * @param mixed $object
     * @return bool|\Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        $reportFrom = $object->getReportForm();
        switch (get_class($object)) {
            case FinanceReport2019::class:
                $expenseTypes = $reportFrom->getExpenseTypeFromExpenseTypeConfig();
                break;
            default:
                $expenseTypes = $reportFrom->getExpenseTypes();
                break;
        }

        $nkoUser = $this->entityManager->getRepository(NKOUser::class)->find($object->getAuthor()->getId());
        $this->setObject($object);
        try {
            $html = $this->twigEngine->render(self::FILE_NAME[get_class($object)], [
                'report' => $object,
                'expenseTypes' => $expenseTypes,
                'nkoUser' => $nkoUser
            ]);

            return $this->downloader->getStreamedResponse($html, self::VIEW_FILE_NAME);
        } catch (\Twig_Error $error) {
            $this->flashBag->add('sonata_flash_error', 'cannot_render_template');

            return false;
        }
    }
}
