<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 12:53 PM
 */

namespace NKO\OrderBundle\Generator;


use Symfony\Component\HttpFoundation\Response;

interface FileGeneratorInterface
{
    /**
     * @param mixed $object
     * @return Response
     */
    public function generate($object);
}