<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 17.03.17
 * Time: 11:40
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Utils\ContactsExcelGenerator\DefaultFields;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017Application;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater2018Application;
use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\UserBundle\Entity\NKOUser;

class ExcelContactsGenerator
{
    const FIELDS_SERVICE_END = '\ContactFields';

    //УКАЗЫВАЕМ ЗАЯВКИ, ПО КОТОРЫМ НУЖНО ПОЛУЧИТЬ СВЯЗАННУЮ
    //в массиве указываются поля, которые нужно получить не из связанной, а текущей заявки
    const RELATED_APPLICATION_TYPES = [
        Farvater2018Application::class => ['FirstYearEstimatedFinancing'],
    ];

    private $container;

    /** @var DefaultFields */
    private $fieldsService;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getContactsExcel($applications, $className)
    {
        $filename = $this->container->get('kernel')->getRootDir() . '/../src/NKO/OrderBundle/Resources/templates/empty_file.xlsx';
        $tempFilename = $this->container->get('nko_order.downloader.excel_downloader')->getTempExcelFilename($filename);
        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        if ($this->container->has($className.self::FIELDS_SERVICE_END)) {
            $this->fieldsService = $this->container->get($className.self::FIELDS_SERVICE_END);
        } else {
            $this->fieldsService = $this->container->get('Default\ContactFields');
        }

        $this->fillDataTemplate($applications, $this->fieldsService->getFields(), $excelObj->getActiveSheet(), $className);
        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempFilename);

        return $tempFilename;
    }

    private function fillDataTemplate($applications, $keys, $sheet, $className)
    {
        $isApplicationHistory = $this->isApplicationHistory(reset($applications));
        $accessor = PropertyAccess::createPropertyAccessor();
        $j=2;

        foreach ($applications as $application) {
            $i = 0;

            //если объект ApplicationHistory, то получаем BaseApplication
            if ($isApplicationHistory) {
                $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $application->getNumber(), true));
                $application = unserialize($application->getData());
            }

            //в конкурсе Brief17, почему-то оказалась заявка с типом Farvater17??
            if (get_class($application) !== $className) {
                continue;
            }

            $this->setHeaderStyle($sheet, $keys, $application, $isApplicationHistory);

            //если нужно, получаем связанную заявку
            $relatedApplication = $this->getRelatedApplication($application, $applications);

            foreach ($keys as $key) {
                $object = $this->getCurrentObject($application, $relatedApplication, $key);
                $value = $this->getValue($key, $object, $accessor);

                $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $value, true));
            }
            $j++;
        }
    }

    //ВЫЗОВ МЕТОДОВ ФОРМАТИРОВАНИЕ ЗНАЧЕНИЙ
    //если $this не содержит метода получения, то значение берется у объекта завки
    private function getValue($key, $application, $accessor)
    {
        $method = 'get'.ucfirst($key);
        if ((new \ReflectionClass($this))->hasMethod($method)) {
            $value = (new \ReflectionMethod($this, $method))->invokeArgs($this, [$application]);
        } else {
            $value = $accessor->getValue($application, $key);
        }

        return $value;
    }

    private function getCurrentObject($application, $relatedApplication, $key)
    {
        if (!$relatedApplication || $key === 'isSend') {
            return $application;
        }

        // особенная Farvater2017
        if ($application instanceof Farvater2017Application) {
            return $relatedApplication;
        }
        //------------------------

        //если есть связанная заявка, то по умолчанию значения будут браться и ее, т.к. контактная ифна расопложена там
        //если нужно из текущей значения - указать их в константе RELATED_APPLICATION_TYPES
        return (!in_array($key, self::RELATED_APPLICATION_TYPES[get_class($application)])) ?
            $relatedApplication : $application;
    }

    //ПОЛУЧЕНИЕ СВЯЗАННОЙ ЗАВКИ
    private function getRelatedApplication($application, $applications)
    {
        if (key_exists(get_class($application), self::RELATED_APPLICATION_TYPES)) {
            $relatedApplicationHistory = $this->container->get('nko_order.resolver.related_competition_resolver')
                ->getApplicationHistory($application);

            return unserialize($relatedApplicationHistory->getData());
        }

        // особенная Farvater2017
        if ($application instanceof Farvater2017Application) {
            return $this->getRelatedApplicationsFarvater17($application, $applications);
        }
        //------------------------

        return null;
    }

    private function isApplicationHistory($application)
    {
        return $application instanceof ApplicationHistory;
    }

    //КОНТЕНТ ШАПКИ ТАБЛИЦЫ
    private function setHeaderStyle(\PHPExcel_Worksheet $sheet, $items, $application, $appHis = null)
    {
        static $isCalled = null;
        if ($isCalled) {
            return;
        }

        $i=0;
        if ($appHis) {
            $cell = $sheet->setCellValueByColumnAndRow($i++, 1, 'Номер заявки', true);
            $this->setHeaderCellStyles($sheet, $cell);
        }
        $translations = $this->fieldsService->getTranslations();
        foreach ($items as $item) {
            $cell = $sheet->setCellValueByColumnAndRow($i++, 1, $translations[$item], true);
            $this->setHeaderCellStyles($sheet, $cell);
        }
        $isCalled = true;
    }

    //ФОРМАТИРОВАНИЕ ВСЕХ ЯЧЕЕК
    public function setCellStyles($cell)
    {
        $cell->getStyle()->applyFromArray([
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']]
            ]
        ]);

        $cell->getStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_TOP);
        $cell->getStyle()->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $cell->getStyle()->getAlignment()->setWrapText(true);
    }

    //ФОРМАТИРОВАНИЕ ЯЧЕЕК ШАПКИ ТАБЛИЦЫ
    public function setHeaderCellStyles($sheet, $cell)
    {
        $this->setCellStyles($cell);
        $sheet->getColumnDimension($cell->getColumn())->setWidth(30);

        $style = $cell->getStyle();
        $style->getFont()->setBold(true);
        $style->applyFromArray([
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => ['rgb' => 'B8FA86']
            ],
        ]);
    }

    //ОСОБЕННЫЙ ФАРВАТЕР2017-ПОЛНАЯ, ПОВЕДЕНИЕ НЕМНОГО ОТЛИЧАЕТСЯ ОТ ОСТАЛЬНЫХ
    private function getRelatedApplicationsFarvater17($application, $applications)
    {
        static $objects = null;

        if (!$objects) {
            $objects = $this->container->get('nko.resolver.related_applications_resolver')->getRelatedApplications($applications, BriefApplication::class);
        }

        $relatedApplication = array_filter($objects, function ($object) use ($application) {
            return $object->getAuthor()->getId() === $application->getAuthor()->getId();
        });

        return $relatedApplication ? reset($relatedApplication) : null;
    }


    //ПОЛУЧЕНИЕ ДАННЫХ -------------------------

    public function getTrainingGrounds(BaseApplication $application)
    {
        $organizations = $application->getTrainingGrounds();
        $value = "";
        foreach ($organizations as $organization) {
            if ($organization->getParent()) {
                $value .= $organization . '. ';
            }
        }
        return $value;
    }

    public function getTrainingGroundsDirection(BaseApplication $application)
    {
        $organizations = $application->getTrainingGrounds();
        $value = "";
        foreach ($organizations as $organization) {
            if (!$organization->getParent()) {
                $value .= $organization . '. ';
            }
        }
        return $value;
    }

    public function getDeadLine(BaseApplication $application)
    {
        $deadLineStart = $application->getDeadLineStart() ?
            $application->getDeadLineStart()->format('d.m.Y') : '';
        $deadLineFinish = $application->getDeadLineFinish() ?
            $application->getDeadLineFinish()->format('d.m.Y') : '';

        return $deadLineStart . ' - ' . $deadLineFinish;
    }

    public function getDateStartOfInternship(BaseApplication $application)
    {
        return $application->getDateStartOfInternship() ?
            $application->getDateStartOfInternship()->format('d.m.Y') : '';
    }

    public function getPhone(BaseApplication $application)
    {
        return "+7 " . $application->getPhoneCode() . $application->getPhone();
    }

    public function getAuthorPhone(BaseApplication $application)
    {
        $author =  $application->getAuthor();

        return "+7 " . $author->getPhoneCode() . $author->getPhone();
    }

    public function getAuthorEmeil(BaseApplication $application)
    {
        $author =  $application->getAuthor();

        return $author->getEmail();
    }

    public function getFullOrganizationName(BaseApplication $application)
    {
        $author =  $application->getAuthor();
        $name = $author->getNkoName();

        if (!$name) {
            $em = $this->container->get('Doctrine')->getManager();
            $name =  $em->getRepository(NKOUser::class)->find($application->getAuthor()->getId());
        }

        return $name;
    }

    public function getHeadOfOrganizationPhone(BaseApplication $application)
    {
        return "+7 " .
            $application->getHeadOfOrganizationPhoneCode() . " " .
            $application->getHeadOfOrganizationPhone();
    }

    public function getHeadOfOrganizationMobilePhone(BaseApplication $application)
    {
        return "+7 " .
            $application->getHeadOfOrganizationMobilePhoneCode() . " " .
            $application->getHeadOfOrganizationMobilePhone();
    }

    public function getHeadOfProjectPhone(BaseApplication $application)
    {
        return "+7 " .
            $application->getHeadOfProjectPhoneCode() . " " .
            $application->getHeadOfOrganizationPhone();
    }

    public function getHeadOfProjectMobilePhone(BaseApplication $application)
    {
        return "+7 " .
            $application->getHeadOfProjectMobilePhoneCode() . " " .
            $application->getHeadOfProjectMobilePhone();
    }

    public function getHeadOfAccountingMobilePhone(BaseApplication $application)
    {
        return "+7 ".
            $application->getHeadOfAccountingMobilePhoneCode() . " " .
            $application->getHeadOfAccountingMobilePhone();
    }

    public function getHeadOfAccountingPhone(BaseApplication $application)
    {
        return "+7 ".
            $application->getHeadOfAccountingPhoneCode() . " " .
            $application->getHeadOfAccountingPhone();
    }

    public function getLegalAddress(BaseApplication $application)
    {
        return $application->getLegalCity() . " " . $application->getLegalRegion() . " " .
            $application->getLegalStreet() . " " . $application->getLegalPostCode();
    }

    public function getActualAddress(BaseApplication $application)
    {
        return $application->getActualCity() . " " . $application->getActualRegion() . " " .
            $application->getActualStreet() . " " . $application->getActualPostCode();
    }

    public function getSiteLinks(BaseApplication $application)
    {
        $links = "";
        $sites = $application->getSiteLinks();
        foreach ($sites as $site) {
            $links.= $site->getLink() . " ";
        }
        return $links;
    }

    public function getSocialNetworkLinks(BaseApplication $application)
    {
        $links = "";
        $socials = $application->getSocialNetworkLinks();
        foreach ($socials as $social) {
            $links.=  $social->getLink() . " ";
        }
        return $links;
    }

    public function getIsSend(BaseApplication $application)
    {
        return $application->getIsSend() ? 'Да' : 'Нет';
    }

    public function getThemeInternship(BaseApplication $application)
    {
        $organizations = $application->getTrainingGrounds();
        $value = "";
        foreach ($organizations as $organization) {
            if (!$organization->getParent()) {
                $value .= $organization . '. ';
            }
        }
        return $value;
    }

    public function getPracticeRegions(BaseApplication $application)
    {
        $val = '';
        $regions = $application->getPracticeRegions();
        foreach ($regions as $region) {
            $val .= $region->getName() . PHP_EOL;
        }

        return $val;
    }
}
