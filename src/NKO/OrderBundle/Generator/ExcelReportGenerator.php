<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 8/10/17
 * Time: 10:48 AM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\ExcelDownloader;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MethodInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Resolver\ProxyResolver;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017Application;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as SecondStageApplication;
use NKO\OrderBundle\Resolver\RelatedCompetitionResolver;
use NKO\UserBundle\Entity\NKOUser;

class ExcelReportGenerator extends FileGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/monitoring_report.xls';
    const VIEW_FILE_NAME = 'Monitoring_report';

    /**
     * @var string $rootDir
     */
    private $rootDir;

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * @var RelatedCompetitionResolver $relatedCompetition
     */
    private $relatedCompetition;

    private $zeroMeasures = [];

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader,
        ProxyResolver $proxyResolver,
        EntityManager $entityManager,
        RelatedCompetitionResolver $relatedCompetition
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
        $this->proxyResolver = $proxyResolver;
        $this->entityManager = $entityManager;
        $this->relatedCompetition = $relatedCompetition;
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function generate($object)
    {
        $this->setObject($object);

        $filename = $this->rootDir . self::FILE_NAME;
        $tempFilename = $this->downloader->getTempExcelFilename($filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($tempFilename);
        $excelObj = $excelReader->load($tempFilename);
        $this->fillMonitoringReport($excelObj->getActiveSheet());

        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempFilename);

        return $this->downloader->getStreamedResponse($tempFilename, self::VIEW_FILE_NAME);
    }

    /**
     * @param $report
     * @param \PHPExcel_Worksheet $sheet
     * @throws \PHPExcel_Exception
     */
    public function fillMonitoringReport(\PHPExcel_Worksheet $sheet)
    {
        /**
         * @var Report $report
         */

        $styleArray = [
            'font' => [
                'bold' => true
            ]
        ];

        $report = $this->getObject();

        //create title
        $author = $this->entityManager->getRepository(NKOUser::class)->find($report->getAuthor());
        $applicationHistory = $this->entityManager->getRepository(ApplicationHistory::class)->find(
            $report->getApplicationHistory()->getId()
        );
        
        $this->createRow(
            $sheet,
            0,
            1,
            'Мониторинговый отчет'
        );

        $this->createRow(
            $sheet,
            0,
            2,
            'Название организации: ' . $author->getNkoName()
        );

        $this->createRow(
            $sheet,
            0,
            3,
            'Название проекта: ' . $applicationHistory->getProjectName()
        );

        if ($report->getReportForm() && $report->getReportForm()->getYear()) {
            $this->createRow(
                $sheet,
                0,
                4,
                'Период: ' . $report->getReportForm()->getYear()->format('Y')
            );
        }

        $this->createRow($sheet, 0, 5, '№ п/п', true, "A5:A6");
        $this->createRow($sheet, 1, 5, 'Показатель', true, "B5:B6");
        $this->setColumnSize($sheet, 'B', 50);


        // create header
        $i = 2;

        $periodColumns = [];
        /**
         * @var PeriodReport $period
         */

        if ($report->getPeriods()) {
            foreach ($report->getPeriods() as $obj) {
                $this->zeroMeasures[] = $obj->getName();
            }
        }

        $reportForm = $this->proxyResolver->resolveProxies($report->getReportForm());

        foreach ($reportForm->getReportPeriods() as $period) {
            $name = $period->getName();

            if (!$name) {
                $period = $this->proxyResolver->resolveProxies($period);
                $name = $period->getName();
            }

            $this->createRow($sheet, $i, 5, $name);
            $periodColumns[$name] = $i;
            $this->createRow($sheet, $i, 6, 'Всего');
            $col = \PHPExcel_Cell::stringFromColumnIndex($i);
            $nextCol = \PHPExcel_Cell::stringFromColumnIndex($i + 1);
            $this->createRow($sheet, $i + 1, 6, 'В т.ч. новых', true, "{$col}5:{$nextCol}5");
            $i +=2;
        }

        $col = \PHPExcel_Cell::stringFromColumnIndex($i);
        $nextCol = \PHPExcel_Cell::stringFromColumnIndex($i + 1);
        $this->createRow($sheet, $i, 5, 'Итого за год', true, "{$col}5:{$nextCol}5");
        $sheet->setCellValueByColumnAndRow($i++, 6, 'ПЛАН');
        $sheet->setCellValueByColumnAndRow($i++, 6, 'ФАКТ');
        $col = \PHPExcel_Cell::stringFromColumnIndex($i);
        $sheet->mergeCells("{$col}5:{$col}6");
        $sheet->setCellValueByColumnAndRow(
            $i++,
            5,
            'Итого за весь период реализации проекта'
        );
        $col = \PHPExcel_Cell::stringFromColumnIndex($i);
        $sheet->mergeCells("{$col}5:{$col}6");
        $sheet->setCellValueByColumnAndRow(
            $i++,
            5,
            'Метод сбора данных (выбор из вариантов )'
        );
        $col = \PHPExcel_Cell::stringFromColumnIndex($i);
        $sheet->mergeCells("{$col}5:{$col}6");
        $sheet->setCellValueByColumnAndRow($i++, 5, 'Метод сбора данных (добавить свой)');
        $col = \PHPExcel_Cell::stringFromColumnIndex($i);
        $sheet->mergeCells("{$col}5:{$col}6");
        $sheet->setCellValueByColumnAndRow($i, 5, 'Комментарии');

        $columnsCount = $i;

        for ($k = 1; $k < 5; $k++) {
            $col = \PHPExcel_Cell::stringFromColumnIndex($i);
            $sheet->mergeCells("A{$k}:{$col}{$k}");
        }

        // create table body
        $i = 0;
        $j = 7;
        $periodCount = $reportForm->getReportPeriods()->count();
        /**
         * @var MonitoringResult $result
         */

        foreach ($report->getMonitoringResults() as $result) {
            if ($result->getIndicator()->getParent() != null
                && $result->getIndicator()->getParent()->getIndexNumber()) {
                $sheet->setCellValueExplicit(
                    \PHPExcel_Cell::stringFromColumnIndex($i) . $j,
                    $result->getIndicator()->getParent()->getIndexNumber() . '.' .
                    $result->getIndicator()->getIndexNumber(),
                    \PHPExcel_Cell_DataType::TYPE_STRING
                );
                $sheet->getStyle(\PHPExcel_Cell::stringFromColumnIndex($i++) . $j)
                    ->getAlignment()
                    ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            } else {
                $sheet->setCellValueByColumnAndRow($i++, $j, $result->getIndicator()->getIndexNumber());
                $col =  \PHPExcel_Cell::stringFromColumnIndex($i);
                $sheet->getStyle("{$col}{$j}")->applyFromArray($styleArray);
            }

            $sheet->setCellValueByColumnAndRow($i++, $j, $result->getIndicator()->getTitle());
            
            /**
             * @var PeriodResult $periodResult
             */
            foreach ($result->getPeriodResults() as $periodResult) {
                $period = $periodResult->getPeriod();

                if ($period) {
                    $name = $period->getName();

                    if (!$name) {
                        $period = $this->entityManager->getRepository(PeriodReport::class)->find($period->getId());
                        $name = $period->getName();
                    }

                    if (array_key_exists($name, $periodColumns)) {
                        $i = $periodColumns[$name];
                        $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 12);
                        $this->setBackgroundColor($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), $j, $name);
                        $sheet->setCellValueByColumnAndRow(
                            $i++,
                            $j,
                            (integer) $periodResult->getTotalAmount()
                        );
                        $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 12);
                        $this->setBackgroundColor($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), $j, $name);
                        $sheet->setCellValueByColumnAndRow(
                            $i++,
                            $j,
                            (integer) $periodResult->getNewAmount()
                        );
                        $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 12);
                    }
                }

                if ($periodResult->getIsFinalResult()) {
                    $i = $periodCount * 2 + 2;

                    if (!$result->getIndicator()->getParent()) {
                        $sheet->setCellValueByColumnAndRow($i++, $j, null);
                    } elseif (!$periodResult->getTotalAmount()) {
                        $sheet->setCellValueByColumnAndRow($i++, $j, 0);
                    } else {
                        $sheet->setCellValueByColumnAndRow($i++, $j, $periodResult->getTotalAmount());
                    }

                    $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 12);
                    $sheet->setCellValueByColumnAndRow($i++, $j, $periodResult->getNewAmount());
                    $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 12);
                }
            }

            $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 45);

            $sheet->setCellValueByColumnAndRow($i++, $j, $result->getTotalValue());
            $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 50);

            if (!$result->getIndicator()->getParent()) {
                $sheet->setCellValueByColumnAndRow($i++, $j, null);
            } elseif ($result->getMethod() && !$reportForm->getIsSelectable()) {
                $sheet->setCellValueByColumnAndRow($i++, $j, $result->getMethod());
            } elseif ($result->getLinkedMethods()
                && $result->getLinkedMethods()->count()
                && $reportForm->getIsSelectable()
            ) {
                $sheet->setCellValueByColumnAndRow($i++, $j, $result->getHumanizedMethods());
            } else {
                $sheet->setCellValueByColumnAndRow($i++, $j, MethodInterface::NOT_CHOSEN);
            }

            $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 50);
            $sheet->setCellValueByColumnAndRow($i++, $j, $result->getCustomMethod());
            $this->setColumnSize($sheet, \PHPExcel_Cell::stringFromColumnIndex($i), 50);
            $sheet->setCellValueByColumnAndRow($i, $j, $result->getComment());
            $j++;
            $i = 0;
        }

        $j--;
        $col = \PHPExcel_Cell::stringFromColumnIndex($columnsCount);
        $sheet->getStyle("A5:{$col}{$j}")->applyFromArray($this->setTableBorder());

        $j += 3;
        $sheet->setCellValueByColumnAndRow(
            1,
            $j++,
            'ДАТА,    ПОДПИСЬ РУКОВОДИТЕЛЯ ОРГАНИЗАЦИИ,      
            ПЕЧАТЬ ОРГАНИЗАЦИИ'
        );
        $sheet->setCellValueByColumnAndRow(
            1,
            $j++,
            '_______________          _______________   
            /                     /'
        );
        $sheet->setCellValueByColumnAndRow(
            1,
            $j,
            '                                                
            М.П.             /'
        );

        for ($i = 0; $i < $columnsCount + 1; $i++) {
            $col = \PHPExcel_Cell::stringFromColumnIndex($i);
            $sheet->getStyle("{$col}5")->applyFromArray($styleArray);
            $sheet->getStyle("{$col}6")->applyFromArray($styleArray);

            for ($j = 1; $j < 83; $j++) {
                $sheet->getStyle("{$col}{$j}")->getAlignment()->setWrapText(true);
                $sheet->getRowDimension($j + 1)->setRowHeight(-1);
            }
        }
    }

    private function setColumnSize($sheet, $column, $size)
    {
        $sheet->getColumnDimension($column)->setAutoSize(false);
        $sheet->getColumnDimension($column)->setWidth($size);
    }

    private function createRow($sheet, $i, $j, $value, $isMerge = false, $mergeColumn = null)
    {
        $sheet->setCellValueByColumnAndRow($i, $j, $value);
        if ($isMerge) {
            $sheet->mergeCells($mergeColumn);
        }
    }

    public function setBackgroundColor($sheet, $row, $column, $name)
    {
        if (in_array($name, $this->zeroMeasures)) {
            $sheet->getStyle($row.$column)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('argb' => '##a0a0a0')),
                )
            );
        }
    }

    public function setTableBorder()
    {
        return array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'))
            ));
    }
}
