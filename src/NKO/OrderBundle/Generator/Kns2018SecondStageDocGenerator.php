<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Doctrine\ORM\EntityManager;

class Kns2018SecondStageDocGenerator extends Kns2017SecondStageDocGenerator
{
    const FIELDS = [
        'entirePeriodEstimatedFinancing',
        'entirePeriodEstimatedCoFinancing',
        'firstYearEstimatedFinancing',
        'firstYearEstimatedCoFinancing',
        'projectImplementation'
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/kns2018_second_stage.docx';

    /**
     * Kns2018ApplicationDocGenerator constructor.
     *
     * @param string $rootDir
     * @param FillWordTemplateService $wordTemplateService
     * @param PropertyAccessor $propertyAccessor
     * @param DocDownloader $downloader
     * @param ProxyResolver $proxyResolver
     * @param EntityManager $em
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver,
        EntityManager $em
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->proxyResolver = $proxyResolver;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
        $this->em = $em;
    }


    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->proxyResolver->resolveProxies($this->object->getAuthor());
        $this->wordTemplateService->setObject($this->object);
        $realAuthor = $this->em->find(get_class($this->getObject()->getAuthor()), $this->getObject()->getAuthor()->getId());
        $this->fillAuthorData($realAuthor);
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);
        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGroundParent');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGround');
        $this->wordTemplateService->fillTemplate(array_merge(parent::FIELDS, self::FIELDS));
        $this->wordTemplateService->fillTables(self::TABLES);
        $this->fillEffectivenessTable();
    }
}
