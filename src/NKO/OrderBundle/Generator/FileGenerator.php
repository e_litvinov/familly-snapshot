<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 12:51 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Downloader\ExcelDownloader;
use NKO\OrderBundle\Downloader\PdfDownloader;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\ReportHistory;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\PropertyAccess\PropertyAccessor;

abstract class FileGenerator implements FileGeneratorInterface
{
    /**
     * @var PropertyAccessor $accessor
     */
    protected $accessor;

    /**
     * @var TemplateProcessor $document
     */
    protected $document;

    /**
     * @var object
     */
    protected $object;

    /**
     * @var FillWordTemplateService $wordTemplateService
     */
    protected $wordTemplateService;

    /**
     * @var DocDownloader|ExcelDownloader|PdfDownloader $downloader
     */
    protected $downloader;

    const AUTO_NUM = 'auto_num';

    /**
     * @return TemplateProcessor
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param TemplateProcessor $document
     */
    public function setDocument(TemplateProcessor $document)
    {
        $this->document = $document;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param object $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    /**
     * @return PropertyAccessor
     */
    public function getAccessor()
    {
        return $this->accessor;
    }

    /**
     * @param PropertyAccessor $accessor
     */
    public function setAccessor(PropertyAccessor $accessor)
    {
        $this->accessor = $accessor;
    }

    /**
     * @return FillWordTemplateService
     */
    public function getWordTemplateService()
    {
        return $this->wordTemplateService;
    }

    /**
     * @param FillWordTemplateService $wordTemplateService
     */
    public function setWordTemplateService($wordTemplateService)
    {
        $this->wordTemplateService = $wordTemplateService;
    }

    /**
     * @return DocDownloader|PdfDownloader|ExcelDownloader
     */
    public function getDownloader()
    {
        return $this->downloader;
    }

    /**
     * @param DocDownloader|PdfDownloader|ExcelDownloader  $downloader
     */
    public function setDownloader($downloader)
    {
        $this->downloader = $downloader;
    }
}