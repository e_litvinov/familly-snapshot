<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.7.18
 * Time: 15.29
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\UserBundle\Entity\NKOUser;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplication;


class AnalyticReport2018WordGenerator extends FileGenerator
{
    private $em;
    private $rootDir;

    const TEMPLATE_PATH = [
        ContinuationApplication::class => '/../src/NKO/OrderBundle/Resources/templates/analytic_report2018.docx',
        FarvaterApplication::class => '/../src/NKO/OrderBundle/Resources/templates/analytic_report2018_farvater.docx'
    ];

    const VIEW_FILE_NAME = 'Report';

    const FIELDS = [
        'author.nko_name',
        'updatedAt',
        'projectName',
        'startDateProject',
        'finishDateProject',
        'directorName',
        'directorPosition',
        'accountantName',
        'accountantPosition',
        'priorityDirection',
        'priorityDirectionEtc',
        'practiceImplementation.purpose',
        'practiceImplementation.unplannedImmediateResults',
        'practiceImplementation.practiceChanges',
        'practiceImplementation.unplannedSocialResults',
        'practiceImplementation.successStories',
        'practiceImplementation.difficulties',
        'practiceImplementation.lessons',
        'practiceIntroduction.purpose',
        'practiceIntroduction.unplannedImmediateResults',
        'practiceIntroduction.unplannedSocialResults',
        'practiceIntroduction.successStories',
        'practiceIntroduction.difficulties',
        'practiceIntroduction.lessons'
    ];

    const TABLES = [
        'territories' => [
            'region_td' => 'region',
            'locality_td' => 'locality',
        ],
        'monitoringResults' => [
            'mon_res_first_td' => 'first',
            'mon_res_second_td' => 'second',
            'mon_res_third_td' => 'third',
        ],
        'newMonitoringElements' => [
            'new_mon_elements_num_td' => self::AUTO_NUM,
            'new_mon_elem_text_td' => 'text',
        ],
        'monitoringChanges' => [
            'monitoring_changes_num_td' => self::AUTO_NUM,
            'mon_changes_context_td' => 'context',
        ],
        'partnerActivities' => [
            'partner_activities_first_td' => 'first',
            'partner_activities_second_td' => 'second',
            'partner_activities_third_td' => 'third',
            'partner_activities_fours_td' => 'fours',
        ],
        'humanResources' => [
            'human_resource_executors_td' => 'executorsCount',
            'human_resource_substitutions_td' => 'substitutionsCount',
            'human_resource_rates_td' => 'ratesCount',
            'human_resource_individual_td' => 'individualJobSpecialistCount',
            'human_resource_comment_td' => 'comment',
        ],

        'practiceSuccessStoryAttachments' => [
            'practice_sucess_story_num_td' => self::AUTO_NUM,
            'practice_sucess_story_title' => 'title',
        ],
        'practiceSpreadSuccessStoryAttachments' => [
            'practice_spread_story_num_td' => self::AUTO_NUM,
            'practice_spread_story_title' => 'title',
        ],
        'publicationAttachments' => [
            'pub_att_author' => 'author',
            'pub_att_name' => 'name',
            'pub_att_date' => 'date',
            'pub_att_media_name' => 'mediaName',
            'pub_att_media_link' => 'mediaLink',
        ],
        'materialAttachments' => [
            'mat_att_author' => 'author',
            'mat_att_name' => 'name',
            'mat_att_date' => 'date',
            'mat_att_edition' => 'edition',
            'mat_att_media_link' => 'mediaLink',
            'mat_att_comment' => 'comment',
        ],
        'factorAttachments' => [
            'factor_name' => 'name',
            'factor_person' => 'person',
            'factor_link' => 'link',
            'factor_comment' => 'comment',
        ],
        'monitoringDevelopmentAttachments' => [
            'mon_dev_num_td' => self::AUTO_NUM,
            'mon_dev_title' => 'title',
            'mon_dev_text' => 'text',
        ],
        'analyticPublications' => [
            'analyt_pub_indicator' => 'indicator',
            'analyt_pub_fact' => 'fact',
        ],
        'expectedAnalyticResults' => [
            'exp_an_res_service' => 'linkedResult.service',
            'exp_an_res_target' => 'linkedResult.targetGroup',
            'exp_an_res_service_fact' => 'serviceFactValue',
            'exp_an_res_service_plan' => 'linkedResult.servicePlanValue',
            'exp_an_res_beneficiary_fact' => 'beneficiaryFactValue',
            'exp_an_res_beneficiary_plan' => 'linkedResult.beneficiaryPlanValue',
            'exp_an_res_method' => 'measurementMethod',
            'exp_an_res_feedback' => 'isFeedback',
            'exp_an_res_comment' => 'comment',
        ],
        'implementationSocialResults' => [
            'implementation_result' => 'result',
            'implementation_comment' => 'comment',
        ],
        'practiceAnalyticResults' => [
            'practice_an_res_service' => 'linkedResult.service',
            'practice_an_res_practice' => 'practiceFormat',
            'practice_an_res_target' => 'linkedResult.targetGroup',
            'practice_an_res_service_fact' => 'serviceFactValue',
            'practice_an_res_service_plan' => 'linkedResult.servicePlanValue',
            'practice_an_res_beneficiary_fact' => 'beneficiaryFactValue',
            'practice_an_res_beneficiary_plan' => 'linkedResult.beneficiaryPlanValue',
            'practice_an_res_method' => 'measurementMethod',
            'practice_an_res_feedback' => 'isFeedback',
            'practice_an_res_comment' => 'comment',
            'practice_an_res_grantee' => 'grantee',
        ],
        'introductionSocialResults' => [
            'introduction_result' => 'result',
            'introduction_comment' => 'comment',
        ],
    ];

    const TABLE_RESULTS_CONTINUATION = [
        'expectedAnalyticResults' => [
            'exp_an_res_service' => 'linkedResult.service',
            'exp_an_res_target' => 'linkedResult.problem',
            'exp_an_res_service_fact' => 'serviceFactValue',
            'exp_an_res_service_plan' => 'linkedResult.servicePlanValue',
            'exp_an_res_beneficiary_fact' => 'beneficiaryFactValue',
            'exp_an_res_beneficiary_plan' => 'linkedResult.beneficiaryPlanValue',
            'exp_an_res_method' => 'measurementMethod',
            'exp_an_res_feedback' => 'isFeedback',
            'exp_an_res_comment' => 'comment',
        ],
        'practiceAnalyticResults' => [
            'practice_an_res_service' => 'linkedResult.service',
            'practice_an_res_practice' => 'practiceFormat',
            'practice_an_res_target' => 'linkedResult.problem',
            'practice_an_res_service_fact' => 'serviceFactValue',
            'practice_an_res_service_plan' => 'linkedResult.servicePlanValue',
            'practice_an_res_beneficiary_fact' => 'beneficiaryFactValue',
            'practice_an_res_beneficiary_plan' => 'linkedResult.beneficiaryPlanValue',
            'practice_an_res_method' => 'measurementMethod',
            'practice_an_res_feedback' => 'isFeedback',
            'practice_an_res_comment' => 'comment',
            'practice_an_res_grantee' => 'grantee',
        ],
    ];

    const TABLE_RESULTS_FARVATER = [
        'expectedAnalyticResults' => [
            'exp_an_res_service' => 'linkedFarvaterResult.service',
            'exp_an_res_target' => 'linkedFarvaterResult.targetGroup',
            'exp_an_res_indicator' => 'linkedFarvaterResult.indicator',
            'exp_an_res_target_value' => 'linkedFarvaterResult.targetValue',
            'exp_an_res_service_fact' => 'serviceFactValue',
            'exp_an_res_method' => 'measurementMethod',
            'exp_an_res_feedback' => 'isFeedback',
            'exp_an_res_comment' => 'comment',
        ],
        'practiceAnalyticIndividualResults' => [
            'exp_an_inv_res_service' => 'linkedFarvaterResult.service',
            'exp_an_inv_res_target' => 'linkedFarvaterResult.targetGroup',
            'exp_an_inv_res_indicator' => 'linkedFarvaterResult.indicator',
            'exp_an_inv_res_target_value' => 'linkedFarvaterResult.targetValue',
            'exp_an_inv_res_fact' => 'serviceFactValue',
            'exp_an_inv_res_method' => 'measurementMethod',
            'exp_an_inv_res_feedback' => 'isFeedback',
            'exp_an_inv_res_comment' => 'comment',
        ],
        'practiceAnalyticResults' => [
            'practice_an_res_service' => 'linkedFarvaterResult.service',
            'practice_an_res_format' => 'practiceFormat',
            'practice_an_res_target' => 'linkedFarvaterResult.targetGroup',
            'practice_an_res_indicator' => 'linkedFarvaterResult.indicator',
            'practice_an_res_target_val' => 'linkedFarvaterResult.targetValue',
            'practice_an_res_service_fact' => 'serviceFactValue',
            'practice_an_res_method' => 'measurementMethod',
            'practice_an_res_feedback' => 'isFeedback',
            'practice_an_res_grantee' => 'grantee',
            'practice_an_res_comment' => 'comment',
        ],
        'expectedAnalyticIndividualResults' => [
            'practice_an_inv_res_service' => 'linkedFarvaterResult.service',
            'practice_an_inv_res_format' => 'practiceFormat',
            'practice_an_inv_res_target' => 'linkedFarvaterResult.targetGroup',
            'practice_an_inv_res_indicator' => 'linkedFarvaterResult.indicator',
            'practice_an_inv_res_target_val' => 'linkedFarvaterResult.targetValue',
            'practice_an_inv_res_service_fact' => 'serviceFactValue',
            'practice_an_inv_res_method' => 'measurementMethod',
            'practice_an_inv_res_feedback' => 'isFeedback',
            'practice_an_inv_res_grantee' => 'grantee',
            'practice_an_inv_res_comment' => 'comment',
        ],
    ];

    const FEEDBACK_TABLES = [
        'practiceFeedbackAttachments' => [
            'practice_feedback_practice' => 'practice',
            'practice_feedback_comment' => 'comment',
            'practice_feedback_solution' => 'administrativeSolution',
        ],
        'practiceSpreadFeedbackAttachments' => [
            'spread_feedback_practice' => 'practice',
            'spread_feedback_comment' => 'comment',
            'spread_feedback_solution' => 'administrativeSolution',
        ]
    ];

    public function __construct(
        $rootDir,
        PropertyAccessor $propertyAccessor,
        FillWordTemplateService $wordTemplateService,
        DocDownloader $downloader,
        EntityManager $entityManager
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->em = $entityManager;
        $this->accessor =$propertyAccessor;
        $this->rootDir = $rootDir;
    }

    public function generate($object)
    {
        $applicationClass = $object->getReportForm()->getCompetition()->getApplicationClass();
        $this->document = new TemplateProcessor($this->rootDir . self::TEMPLATE_PATH[$applicationClass]);

        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);
        $this->fillTemplate($applicationClass);
        $this->fillRelatedFields($object);

        $filename = tempnam(sys_get_temp_dir(), 'Report');
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate($applicationClass)
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);
        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $tables = ($applicationClass === FarvaterApplication::class) ? (self::TABLE_RESULTS_FARVATER) : (self::TABLE_RESULTS_CONTINUATION);
        $tables = array_merge(self::TABLES, $tables);
        $this->wordTemplateService->fillTables($tables);
        $this->fillFeedbackTables();
    }

    private function fillRelatedFields($report)
    {
        $grant = $this->em->getRepository(GrantConfig::class)->findOneBy([
                'reportForm' => $report->getReportForm(),
                'applicationHistory' => $report->getApplicationHistory()
            ]);

        $this->document->setValue('sumGrant', $grant ? $grant->getSumGrant() : null);
        $this->document->setValue('contract', $grant ? $grant->getContract() : null);
        $this->document->setValue('year', $report->getReportForm()->getYear()? $report->getReportForm()->getYear()->format('Y') : '');
    }

    public function fillFeedbackTables()
    {
        foreach (self::FEEDBACK_TABLES as $key => $table) {
            $collection = $this->accessor->getValue($this->object, $key);
            $countRows = $collection->count();
            if($countRows == 0) {
                foreach($table as $templateVar => $objAttr) {
                    $this->document->setValue("{$templateVar}", " ");
                }
                return;
            }

            $i = 1;
            $isCloneRow = false;
            foreach($collection as $item) {
                foreach($table as $templateVar => $objAttr) {
                    if(!$isCloneRow) {
                        $this->document->cloneRow($templateVar, $countRows);
                        $isCloneRow = true;
                    }

                    $value = ($objAttr == 'practice' && $this->accessor->getValue($item, $objAttr)) ?
                        $this->accessor->getValue($item, $objAttr)->getRelatedService() :
                        $this->accessor->getValue($item, $objAttr);

                    $this->document->setValue("{$templateVar}#{$i}", mb_convert_encoding($value, 'UTF-8'));
                }
                $i++;
            }
        }
    }
}