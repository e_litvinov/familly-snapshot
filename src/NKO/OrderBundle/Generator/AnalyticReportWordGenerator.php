<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/10/17
 * Time: 12:01 PM
 */

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report;
use NKO\UserBundle\Entity\NKOUser;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\HttpFoundation\StreamedResponse;
use PhpOffice\PhpWord\Settings;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class AnalyticReportWordGenerator extends FileGenerator
{
    const HEADER_REPORT = [
        'kns' => 'Курс на семью',
        'farvater' => 'Семейный фарватер'
    ];

    const  FIELDS = [
        'updatedAt',
        'projectName',
        'startDateProject',
        'finishDateProject',
        'directorName',
        'directorPosition',
        'priorityDirection',
        'priorityDirectionEtc',
        'projectPurpose',
        'accountantName',
        'accountantPosition',
    ];

    const TABLES = [
        'socialSolvedProblems' => [
            'tb_socialSolvedProblems_num' => self::AUTO_NUM,
            'tb_socialSolvedProblems_title' => 'title',
            'tb_socialSolvedProblems_context' => 'context'
        ],
        'socialNotSolvedProblems' => [
            'tb_socialNotSolvedProblems_num' => self::AUTO_NUM,
            'tb_socialNotSolvedProblems_title' => 'title',
            'tb_socialNotSolvedProblems_context' => 'context'
        ],
        'socialMeasures' => [
            'tb_socialMeasures_num' => self::AUTO_NUM,
            'tb_socialMeasures_context' => 'context'
        ],
        'nextSocialMeasures' => [
            'tb_nextSocialMeasures_num' => self::AUTO_NUM,
            'tb_nextSocialMeasures_context' => 'context'
        ],
        'realizationFactors' => [
            'tb_realizationFactors_num' => self::AUTO_NUM,
            'tb_realizationFactors_title' => 'title',
            'tb_realizationFactors_context' => 'context'

        ],
        'analyticPublications' => [
            'tb_analyticPublication_indicator' => 'indicator',
            'tb_analyticPublication_plan' => 'plan',
            'tb_analyticPublication_fact' => 'fact'
        ],
        'practiceSolvedProblems' => [
            'tb_practiceSolvedProblems_num' => self::AUTO_NUM,
            'tb_practiceSolvedProblems_title' => 'title',
            'tb_practiceSolvedProblems_context' => 'context'
        ],
        'practiceNotSolvedProblems' => [
            'tb_practiceNotSolvedProblems_num' => self::AUTO_NUM,
            'tb_practiceNotSolvedProblems_title' => 'title',
            'tb_practiceNotSolvedProblems_context' => 'context'
        ],
        'resultStability' => [
            'tb_practiceStability_num' => self::AUTO_NUM,
            'tb_practiceSolvedProblems_context' => 'context'
        ],
        'introductionFactors' => [
            'tb_introductionFactors_num' => self::AUTO_NUM,
            'tb_introductionFactors_title' => 'title',
            'tb_introductionFactors_context' => 'context'
        ],
        'monitoringResults' => [
            'tb_monitoringDevelopment_event' => 'first',
            'tb_monitoringDevelopment_expectedResult' => 'second',
            'tb_monitoringDevelopment_comment' => 'third'
        ],
        'newMonitoringElements' => [
            'tb_monitoringEvements_num' => self::AUTO_NUM,
            'tb_monitoringEvements_context' => 'text'
        ],
        'monitoringChanges' => [
            'tb_monitoringChanges_num' => self::AUTO_NUM,
            'tb_monitoringChanges_context' => 'context'
        ],
        'monitoringDevelopments' => [
            'tb_monitoringDevelopment_event' => 'event',
            'tb_monitoringDevelopment_expectedResult' => 'expectedResult',
            'tb_monitoringDevelopment_comment' => 'comment'
        ],
        'keyRisks' => [
            'tb_key_risk' => 'keyRisk',
            'tb_key_risk_isRealized' => 'isRealized',
            'tb_key_risk_act' => 'act',
            'tb_key_risk_result' => 'result',
            'tb_key_risk_planRisk' => 'planRisk',
            'tb_key_risk_planAct' => 'planAct'
        ],
        'partnerActivities' => [
            'tb_partnerActivities_first' => 'first',
            'tb_partnerActivities_second' => 'second',
            'tb_partnerActivities_third' => 'third',
            'tb_partnerActivities_fours' => 'fours'
        ],
        'humanResources' => [
            'tb_humanResources_executorsCount' => 'executorsCount',
            'tb_humanResources_substitutionsCount' => 'substitutionsCount',
            'tb_humanResources_ratesCount' => 'ratesCount',
            'tb_humanResources_individualJobSpecialistCount' => 'individualJobSpecialistCount',
            'tb_humanResources_comment' => 'comment'
        ],
        'practiceFeedbackAttachments' => [
            'tb_service' => 'practice',
            'tb_comment' => 'comment',
            'tb_administrative_solution' => 'administrativeSolution'
        ],
        'practiceSpreadFeedbackAttachments' => [
            'tb_service' => 'spreadPractice',
            'tb_comment' => 'comment',
            'tb_administrative_solution' => 'administrativeSolution'
        ],
        'practiceSuccessStoryAttachments' => [
            'tb_num' => self::AUTO_NUM,
            'tb_success_name' => 'title',
            'tb_success_context' => 'text'
        ],
        'practiceSpreadSuccessStoryAttachments' => [
            'tb_num' => self::AUTO_NUM,
            'tb_success_name' => 'title',
            'tb_success_context' => 'text'
        ],
        'publicationAttachments' => [
            'tb_author_pub' => 'author',
            'tb_name_pub' => 'name',
            'tb_date_pub' => 'date',
            'tb_media_name' => 'mediaName',
            'tb_media_link_pub' => 'mediaLink'
        ],
        'materialAttachments' => [
            'tb_author' => 'author',
            'tb_name' => 'name',
            'tb_date' => 'date',
            'tb_edition' => 'edition',
            'tb_media_link' => 'mediaLink',
            'tb_comment' => 'comment'
        ],
        'factorAttachments' => [
            'tb_factor_name' => 'name',
            'tb_person' => 'person',
            'tb_link' => 'link',
            'tb_comment' => 'comment'
        ],
        'monitoringAttachments' => [
            'tb_num' => self::AUTO_NUM,
            'tb_name' => 'linkedElementText',
        ],
        'practiceAnalyticResults' => [
            'tb_practiceAnalyticResults_service' => 'service',
            'tb_practiceAnalyticResults_targetGroup' => 'targetGroup',
            'tb_practiceAnalyticResults_fact_1' => 'serviceFactValue',
            'tb_practiceAnalyticResults_plan_1' => 'servicePlanValue',
            'tb_practiceAnalyticResults_fact_2' => 'beneficiaryFactValue',
            'tb_practiceAnalyticResults_plan_2' => 'beneficiaryPlanValue',
            'tb_practiceAnalyticResults_measurementMethod' => 'measurementMethod',
            'tb_practiceAnalyticResults_isFeedback' => 'isFeedback',
            'tb_practiceAnalyticResults_comment' => 'comment',
            'tb_practiceAnalyticResults_fact_start' => 'startDateFact',
            'tb_practiceAnalyticResults_fact_end' => 'finishDatePlan',
            'tb_practiceAnalyticResults_plan_start' => 'startDatePlan',
            'tb_practiceAnalyticResults_plan_end' => 'finishDateFact'
        ],
        'expectedAnalyticResults' => [
            'tb_expectedAnalyticResult_service' => 'service',
            'tb_expectedAnalyticResult_practiceFormat' => 'practiceFormat',
            'tb_expectedAnalyticResult_targetGroup' => 'targetGroup',
            'tb_expectedAnalyticResult_fact_1' => 'serviceFactValue',
            'tb_expectedAnalyticResult_plan_1' => 'servicePlanValue',
            'tb_expectedAnalyticResult_fact_2' => 'beneficiaryFactValue',
            'tb_expectedAnalyticResult_plan_2' => 'beneficiaryPlanValue',
            'tb_expectedAnalyticResult_measurementMethod' => 'measurementMethod',
            'tb_expectedAnalyticResult_idFeedback' => 'isFeedback',
            'tb_expectedAnalyticResult_comment' => 'comment',
            'tb_expectedAnalyticResult_fact_start' => 'startDateFact',
            'tb_expectedAnalyticResult_fact_end' => 'finishDatePlan',
            'tb_expectedAnalyticResult_plan_start' => 'startDatePlan',
            'tb_expectedAnalyticResult_plan_end' => 'finishDateFact',
            'tb_expectedAnalyticResult_grantee' => 'grantee'
        ]
    ];

    const MONITORING_RESULT_FIELDS = [
        'tb_parent_num',
        'tb_child_num',
        'tb_index_name',
        'tb_plan',
        'tb_fact',
        'method',
        'comment'
    ];

    const TEMPLATE_PATH = '/../src/NKO/OrderBundle/Resources/templates/analytic_report.docx';
    const VIEW_FILE_NAME = 'Report';

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * AnalyticReportWordGenerator constructor.
     * @param string $rootDir
     * @param PropertyAccessor $propertyAccessor
     * @param FillWordTemplateService $wordTemplateService
     * @param DocDownloader $downloader
     * @param EntityManager $entityManager
     */
    public function __construct(
        $rootDir,
        PropertyAccessor $propertyAccessor,
        FillWordTemplateService $wordTemplateService,
        DocDownloader $downloader,
        EntityManager $entityManager
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->downloader = $downloader;
        $this->entityManager = $entityManager;
        $this->accessor =$propertyAccessor;
        $this->document = new TemplateProcessor($rootDir . self::TEMPLATE_PATH);
    }

    /**
     * @param mixed $object
     * @return \Symfony\Component\HttpFoundation\Response|StreamedResponse
     */
    public function generate($object)
    {
        Settings::setOutputEscapingEnabled(true);
        $this->setObject($object);

        $this->fillTemplate();
        $this->fillHeaderReport();

        $filename = tempnam(sys_get_temp_dir(), 'Report');
        $this->document->saveAs($filename);

        return $this->downloader->getStreamedResponse($filename, self::VIEW_FILE_NAME);
    }

    private function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->wordTemplateService->setObject($this->object);

        $this->wordTemplateService->fillTemplate(self::FIELDS);
        $this->wordTemplateService->fillTables(self::TABLES);

        $this->fillCustomFields();
    }

    private function fillCustomFields()
    {
        $reportForm = $this->accessor->getValue($this->object, 'reportForm');
        $this->document->setValue('year', (string)$this->accessor->getValue($reportForm, 'year')->format('Y'));
        $author = $this->entityManager->getRepository(NKOUser::class)->find($this->object->getAuthor()->getId());
        $this->document->setValue('nko_name', $author->getNkoName());

        if($this->object instanceof Report) {
            $grant = $this->entityManager->getRepository(GrantConfig::class)
                ->findOneBy([
                    'reportForm' => $this->object->getReportForm(),
                    'applicationHistory' => $this->object->getApplicationHistory()
                ]);

            $this->document->setValue('contract', $grant->getContract());
            $this->document->setValue('sumGrant', $grant->getSumGrant());
            $this->fillMonitoringResults('practiceMonitoringResults', self::MONITORING_RESULT_FIELDS);
            $this->fillMonitoringResults('introductionMonitoringResults', self::MONITORING_RESULT_FIELDS);
        }
    }

    private function fillMonitoringResults($table, $attributes)
    {
        $countRows = count($this->accessor->getValue($this->object, $table));
        $collection = $this->accessor->getValue($this->object, $table);
        if($countRows == 0) {
            foreach($attributes as $attribute) {
                $this->document->setValue("{$attribute}", " ");
            }
        }

        $i = 1;
        $isCloneRow = false;
        foreach($collection as $item) {
            foreach($attributes as $attribute) {
                if(!$isCloneRow) {
                    $this->document->cloneRow($attribute, $countRows);
                    $isCloneRow = true;
                }

                switch ($attribute) {
                    case 'tb_parent_num':
                        $value = $item->getIndicator()->getParent()->getIndexNumber() ?  mb_convert_encoding($item->getIndicator()->getParent()->getIndexNumber(),  'UTF-8').'.' : '';
                        $this->document->setValue("{$attribute}#{$i}", $value);
                        break;
                    case 'tb_child_num':
                        $this->document->setValue("{$attribute}#{$i}", mb_convert_encoding($item->getIndicator()->getIndexNumber(), 'UTF-8'));
                        break;
                    case 'tb_index_name':
                        $this->document->setValue("{$attribute}#{$i}", $item->getIndicator()->getTitle());
                        break;
                    case 'tb_plan':
                        $this->document->setValue("{$attribute}#{$i}", $item->getFinalPeriodResult()->first()->getTotalAmount());
                        break;
                    case 'tb_fact':
                        $this->document->setValue("{$attribute}#{$i}", $item->getFinalPeriodResult()->first()->getNewAmount());
                        break;
                    default:
                        $value = $this->accessor->getValue($item, $attribute);
                        $this->document->setValue("{$attribute}#{$i}", mb_convert_encoding($value, 'UTF-8'));
                        break;
                }
            }
            $i++;
        }
    }

    public function fillHeaderReport()
    {
        $typeApplication = $this->object->getReportForm()->getCompetition()->getApplicationClass();
        $typeApplication = $typeApplication == 'NKO\OrderBundle\Entity\Report' ? self::HEADER_REPORT['kns'] : self::HEADER_REPORT['farvater'];
        $this->document->setValue('headerReport', $typeApplication);
    }
}