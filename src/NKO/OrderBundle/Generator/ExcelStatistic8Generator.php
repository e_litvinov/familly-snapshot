<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use NKO\OrderBundle\Entity\ApplicationHistory;
use PHPExcel_Style_Border;
use Symfony\Component\PropertyAccess\PropertyAccess;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater2018;
use NKO\OrderBundle\Entity\Farvater2017\Application as Farvater2017;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as Farvater2016;
use NKO\OrderBundle\Entity\Farvater\FarvaterApplication as Farvater2016Second;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application as Harbor2019;
use NKO\OrderBundle\Entity\KNS2017\Application as KNS2017;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as KNS2017SecondStage;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNS2018;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application as KNS2019;
use NKO\OrderBundle\Entity\Application as KNS2016;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Competition;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication as Brief2017;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as Mixed2019;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2017\Application as KNS2017SecondStage2;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application as KNS2018SecondStage;
use NKO\OrderBundle\Entity\Region;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;

class ExcelStatistic8Generator extends FileGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/statistic8_report.xlsx';
    const VIEW_FILE_NAME = 'statistic_table';

    const MISSED_COMPETITION_ORGANIZATION_ID = 404;

    private $rootDir;

    /** @var EntityManager  */
    private $em;

    private $competitions;
    private $users;

    private $container;

    private $relatedApplications = [];

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader,
        EntityManager $em,
        ContainerInterface $container
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
        $this->em = $em;
        $this->container = $container;
    }

    const FIELDS = [
        'organizationName', 'competition', 'organizationForm', 'region', 'actualAddress', 'pSRN',
        'iNN', 'projectName', 'priorityDirection', 'sum', 'headOfOrganizationFullName', 'mobileHeadOfOrganization',
        'headOfOrganizationEmeil', 'legalAddress', 'startProjectRealization', 'finishProjectRealization', 'peopleCategories'
    ];

    const COMPETITIONS = [
        Farvater2018::class => 'СФ-2018',
        Farvater2017::class => 'СФ-2017',
        Farvater2016::class => 'СФ-2016',
        Harbor2019::class => 'СГ-2019',
        KNS2016::class => 'КНС-2016',
        KNS2017::class => 'КНС-2017',
        KNS2017SecondStage::class => 'КНС-2017',
        KNS2018::class => 'КНС-2018',
        KNS2019::class => 'КНС-2019'
    ];

    const TAMBOW_PSRN = 1026801118981;
    const PODSOLNUH_PSRN = 1097800001804;

    public function generate($object)
    {
        $filename = $this->rootDir . self::FILE_NAME;
        $tempFileName = tempnam(sys_get_temp_dir(), 'Summary_analytics');
        $tempOutFileName = $tempFileName.'.xlsx';
        file_put_contents($tempOutFileName, $filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        $sheet = $excelObj->getActiveSheet();
        $this->setHeaderStyle($sheet);
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $this->getCompetitions();
        $this->fetchUsers($object);
        $this->fillSheet($sheet, $object);

        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);

        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME);
    }

    public function fillSheet(\PHPExcel_Worksheet $sheet, $items)
    {
        $j = 3;
        $countRow = 1;

        foreach ($items as $item) {
            $data = $this->getData($item);
            $i = 0;
            $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $countRow++, true), null);
            foreach ($data as $key => $value) {
                $isNumberFromat = $key === 'pSRN' ? true : null;
                $this->setCellStyles($sheet->setCellValueByColumnAndRow($i++, $j, $value, true), $isNumberFromat);
            }
            $j++;
        }
    }

    private function fetchUsers($items)
    {
        $keys = array_keys($items);
        $this->users = $this->em->getRepository(NKOUser::class)->findBy(['psrn' => $keys]);

        return;
    }

    private function getUser($psrn = null, $id = null)
    {
        $value =  array_filter($this->users, function ($user) use ($psrn, $id) {
            if ($psrn) {
                return $this->getValue($user, 'psrn') == $psrn;
            } else {
                return $this->getValue($user, 'id') == $id;
            }
        });

        return $value ? reset($value) : null;
    }

    public function getData($item)
    {
        $fields = array_flip(self::FIELDS);
        $fields['sum'] = 0;
        $fields['competition'] = '';
        $fields['projectName'] = '';
        $fields['priorityDirection'] = '';
        $fields['startProjectRealization'] = '';
        $fields['finishProjectRealization'] = '';
        $fields['peopleCategories'] = '';

        foreach ($item as $key => $result) {
            $lastApplication = $result['application'];
            $fields['sum'] += (int)$result['baseSum'];
            $fields['competition'] = $this->getCompetition($lastApplication, $fields['competition']);

            if ($lastApplication instanceof Farvater2017 || $lastApplication instanceof Farvater2018) {
                $lastApplication = $this->getRelatedApplication($lastApplication);
            }

            $this->setProjectName($lastApplication, $fields);
            $fields['priorityDirection'] = $this->getPriorityDirection($lastApplication, $fields['priorityDirection']);
            $this->getDateProjectRealization($result, $fields['startProjectRealization'], $fields['finishProjectRealization']);

            $fields['peopleCategories'] = $this->getPeopleCategories($lastApplication, $fields['peopleCategories']);
        }

        $fields['priorityDirection'] = $fields['priorityDirection'] ?: '-';
        //Данные заполняются по последней заявке, если две и более

        //если несколько заявок-победителей от одной организации - заполняем по последней
        $lastApplication = count($item) > 1 ? $this->findLastApplicationByCompetition($item) : (end($item)['application']);


        if ($lastApplication instanceof Farvater2017 || $lastApplication instanceof Farvater2018) {
            $tempApplication = $lastApplication;
            $lastApplication = $this->getRelatedApplication($lastApplication);
        }

        //СФ-2016 все отчеты привязаны к красткой СФ-2016, а ИНН указано в СФ-2016 полная. поэтому подгружаем связанную полную
        if ($lastApplication instanceof Farvater2016) {
            $farvater2016Second = $this->getLastLoader($lastApplication, Farvater2016Second::class);
            $this->setINN($farvater2016Second, $fields);
            $fields['legalAddress'] = $this->getLegalAddress($farvater2016Second);
        } else if ($lastApplication instanceof Brief2017) {
            $this->setINN($tempApplication, $fields);
        } else{
            $this->setINN($lastApplication, $fields);
        }

        if (!$lastApplication instanceof Farvater2016) {
            $fields['legalAddress'] = $this->getLegalAddress($lastApplication);
        }

        $this->setRegisterData($lastApplication, $fields);
        $this->setHeadOfOrganizationContacts($lastApplication, $fields);
        $this->setAddress($lastApplication, $fields);
        $this->setRegion($lastApplication, $fields);
        $this->setHeadOfOrganizationFullName($lastApplication, $fields);

        return $fields;
    }

    public function getPriorityDirection($application, $direction)
    {
        switch (get_class($application)) {
            case KNS2016::class:
                $application = $this->getLastLoader($application, ContinuationApplication::class);
                break;
            case KNS2017::class:
            case KNS2017SecondStage::class:
                $application = $this->getLastLoader($application, KNS2017SecondStage2::class);
                break;
            case KNS2018::class:
                $application = $this->getLastLoader($application, KNS2018SecondStage::class);
                break;
        }

        if ($this->accessor->isReadable($application, 'priorityDirections') && $values = $this->accessor->getValue($application, 'priorityDirections')) {
            $direction .= $direction ? ', ' : '';
            $count = count($values)-1;
            foreach ($values as $key => $item) {
                $direction.= $item;
                $direction .= $key !== $count ? ', ' : '';
            }
        }

        if (!$direction && $this->accessor->isReadable($application, 'priorityDirection') && $value = $this->accessor->getValue($application, 'priorityDirection')) {
            $direction .= $direction ? ', ' : '';

            if (!$value->getName()) {
                $value = $this->em->getRepository(PriorityDirection::class)->find($this->accessor->getValue($application, 'priorityDirection'));
                $value = $this->accessor->getValue($value, 'name');
            }

            $direction.= $value;
        }

        if (!$direction) {
            $direction .= $direction ? ', ' : '';
            $direction = $this->getTrainingGroundsDirection($application, 'trainingGrounds');
        }

        return $direction;
    }

    public function getPeopleCategories($application, $currentPeopleCategory)
    {
        $peopleCategory = $this->getTrainingGroundsDirection($application, 'peopleCategories');
        if (!$peopleCategory) {
            $peopleCategory = $this->getTrainingGroundsDirection($application, 'beneficiaryGroups');
        }

        if (!$peopleCategory) {
            $peopleCategory = $this->getTrainingGroundsDirection($application, 'targetGroups');
            $peopleCategory = str_replace(['(уточните)', '(укажите)'], ['', 'группы'], $peopleCategory);
        }

        if ($peopleCategory) {
            $currentPeopleCategory .= $currentPeopleCategory ? '. ' : '';
            $currentPeopleCategory .= (string) $peopleCategory;
        }

        if ($this->accessor->isReadable($application, 'otherBeneficiaryGroups')) {
            $issetOtherGroup = null;
            $otherGroups = $this->accessor->getValue($application, 'otherBeneficiaryGroups');

            if ($otherGroups) {
                $temp = null;
                if (count($otherGroups) > 1) {
                    $temp = true;
                } else {
                    $temp = $otherGroups->first();
                    $temp = (string) $temp;
                }

                if ($temp) {
                    $currentPeopleCategory .= $currentPeopleCategory ? ', Иные группы' : '';
                }
            }
        }

        if ($this->accessor->isReadable($application, 'beneficiaryGroupName')) {
            if ($this->accessor->getValue($application, 'beneficiaryGroupName')) {
                $currentPeopleCategory .= $currentPeopleCategory ? ', Иные группы' : '';
            }
        }

        if ($this->accessor->isReadable($application, 'beneficiaryGroupEtc')) {
            if ($this->accessor->getValue($application, 'beneficiaryGroupEtc')) {
                $currentPeopleCategory .= $currentPeopleCategory ? ', Иные группы' : '';
            }
        }

        return $currentPeopleCategory;
    }

    public function getTrainingGroundsDirection($application, $fieldName)
    {
        $value = "";

        if ($this->accessor->isReadable($application, $fieldName)) {
            $grounds = $this->accessor->getValue($application, $fieldName);
            if ($grounds) {
                foreach ($grounds as $ground) {
                    if (!$this->accessor->isReadable($ground, 'parent') || !$ground->getParent()) {
                        $value .= $ground . ', ';
                    }
                }
                $value = mb_substr($value, 0, -2);
            }
        }

        return (string) $value;
    }


    public function setProjectName($application, &$fields)
    {
        $fields['projectName'] = $this->getProjectName($application, $fields['projectName']);
    }

    public function setRegion($application, &$fields)
    {
        $region = $this->getValue($application, 'actualRegion') ?: '-';
        if (!$this->getValue($region, 'name')) {
            $region = $this->em->getRepository(Region::class)->find($region->getId());
        }

        $fields['region'] = $region ?: '-';

    }

    public function setHeadOfOrganizationFullName($application, &$fields)
    {
        $fields['headOfOrganizationFullName'] = $this->getValue($application, 'headOfOrganizationFullName') ?: '-';
    }

    public function setRegisterData($application, &$fields)
    {
        $user = $this->getValue($application, 'author');
        $user = $user ? $this->getUser(null, $user->getId()) : $this->getUser($this->getValue($application, 'pSRN'), null);

        $fields['pSRN'] = $this->getValue($user, 'psrn') ?: '-';
        $fields['organizationName'] = $this->getValue($user, 'nko_name') ?: '-';
        $fields['organizationForm'] = $this->getValue($user, 'status') ?: '-';
    }

    public function getCompetition($application, $competition)
    {
        if ($this->accessor->isReadable($application, 'competition')) {
            $competition .= $competition ? ', ' : '';
            $competitionObject = $this->competitions[$application->getCompetition()->getId()];
            $applicationClass = $competitionObject->getApplicationClass();

            if ($application instanceof Farvater2016) {
                $psrn = $this->accessor->getValue($application, 'psrn');
                if ($psrn == self::PODSOLNUH_PSRN) {
                    return  'СФ-2016, СГ-2019';
                }
            } else if ($application instanceof KNS2016) {
                $author =$this->getUser(null, $application->getAuthor()->getId());
                $psrn = $author->getPSRN();
                if ($psrn == self::TAMBOW_PSRN) {
                    return  'КНС-2016';
                }
            }

            if (key_exists($applicationClass, self::COMPETITIONS)) {
                $competition .= self::COMPETITIONS[$applicationClass];
            }

            return $competition;
        }
    }

    public function getDateProjectRealization($application, &$startRealization, &$finishRealization)
    {
        $startRealization .= $startRealization ? ', ' : '';
        $finishRealization .= $finishRealization ? ', ' : '';

        $startRealization .= !empty($application['minYear']) ? (new \DateTime($application['minYear']))->format('Y') : '';
        $finishRealization .= !empty($application['maxYear']) ? (new \DateTime($application['maxYear']))->format('Y') : '';
    }

    public function setAddress($application, &$fields)
    {
        $actualCity = $this->getValue($application, 'actualCity');
        $actualStreet = $this->getValue($application, 'actualStreet');
        $actualAddress = $actualCity.', '.$actualStreet;

        $fields['actualAddress'] = $actualAddress ?: '-';
    }

    public function getRelatedApplication($lastApplication)
    {
        $id = $lastApplication->getId();
        if (!empty($this->relatedApplications[$id])) {
            return $this->relatedApplications[$id];
        }

        if ($lastApplication instanceof Farvater2018) {
            $relatedApplicationHistory = $this->container->get('nko_order.resolver.related_competition_resolver')->getApplicationHistory($lastApplication);
            if ($relatedApplicationHistory) {
                $lastApplication = unserialize($relatedApplicationHistory->getData());
            }
        }

        if ($lastApplication instanceof Farvater2017) {
            $lastApplication = $this->getLastLoader($lastApplication, Brief2017::class);
        }

        $this->relatedApplications[$id] = $lastApplication;

        return $lastApplication;
    }

    public function getLastLoader($lastApplication, $class)
    {
        $object = null;
        $user = $lastApplication->getAuthor();
        $user = $user ? $this->getUser(null, $user->getId()) : $this->getUser($lastApplication->getPSRN());

        $lastApplication->setAuthor($user);

        $relatedApplicationHistory = $this->container->get('nko.resolver.related_applications_resolver')
            ->getLinkedApplicationHistories([$lastApplication], $class);

        if ($relatedApplicationHistory) {
            $object = unserialize(reset($relatedApplicationHistory)['data']);
        }

        return $object;
    }

    public function setHeadOfOrganizationContacts($application, &$fields)
    {
        $mobileCode = $this->getValue($application, 'headOfOrganizationMobilePhoneCode');
        $mobilePhone = $this->getValue($application, 'headOfOrganizationMobilePhone');
        $headOfOrganizationEmeil = $this->getValue($application, 'headOfOrganizationEmeil');
        $mobile = '+'.$mobileCode.' '.$mobilePhone;

        $fields['mobileHeadOfOrganization'] = $mobile ?: '-';
        $fields['headOfOrganizationEmeil'] = $headOfOrganizationEmeil ?: '-';
    }

    public function getLegalAddress($application)
    {
        return $application->getLegalCity();
    }

    public function getProjectName($application, $projectName)
    {
        if ($this->accessor->isReadable($application, 'projectName')) {
            $projectName .= $projectName ? ', ' : '';
            $projectName .= $this->accessor->getValue($application, 'projectName');
        }

        return $projectName;
    }

    public function setINN($application, &$fields)
    {
        $iNN = $this->getValue($application, 'iNN');
        if (!$iNN) {
            $iNN = $this->getValue($application, 'bankINN');
        }

        $fields['iNN'] = $iNN ?: '-';
    }


    public function getValue($lastApplication, $propertyName)
    {
        $value = $this->getProperty($lastApplication, $propertyName);
        if (!$value) {
            return null;
        }

        return $value;
    }

    public function getProperty($application, $propertyName)
    {
        if ($this->accessor->isReadable($application, $propertyName)) {
            return $this->accessor->getValue($application, $propertyName);
        }

        return null;
    }

    public function setValue(&$j, &$i, $value, $sheet)
    {
        $sheet->setCellValueByColumnAndRow($j++, $i, $value ? $value : 0);
    }

    public function getCompetitions()
    {
        $competitions = $this->em->getRepository(Competition::class)->findAll();
        foreach ($competitions as $competition) {
            $this->competitions[$competition->getId()] = $competition;
        }
    }

    public function findLastApplicationByCompetition($applications)
    {
        $application = array_shift($applications);

        $current = [
            'date' =>  $this->getCompetitionDateFinish($application['application']),
            'object' => $application
        ];

        foreach ($applications as $item) {
            $date = $this->getCompetitionDateFinish($item['application']);
            if ($date > $current['date']) {
                $current['date'] = $date;
                $current['object'] = $item;
            }
        }

        return $current['object']['application'];
    }

    private function getCompetitionDateFinish($object)
    {
        $date = null;
        if ($this->accessor->isReadable($object, 'competition')) {
            $competition = $this->competitions[$object->getCompetition()->getId()];
            $date = $competition->getFinishDate();
        }

        return $date;
    }

    private function setHeaderStyle(\PHPExcel_Worksheet $sheet)
    {
        foreach (range('A', 'R') as $column) {

            if ($column !== 'A') {
                $sheet->getColumnDimension($column)->setWidth(40);
            }

            $sheet->getStyle($column.'1')->getFont()->setBold(true);
            $sheet->getStyle($column.'1')->applyFromArray(
                [
                    'fill' => [
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => ['rgb' => 'B8FA86']],
                ]
            );
        }
    }

    public function setCellStyles($cell, $isNumberFormat)
    {
        $options = [
            'borders' => [
                'allborders' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_TOP,
                'wrap' => true
            ]
        ];

        $style = $cell->getStyle();
        $style->applyFromArray($options);

        if ($isNumberFormat) {
            $style->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        }
    }
}
