<?php

namespace NKO\OrderBundle\Generator;

class Kns2018ApplicationDocGenerator extends AbstractKNSApplicationDocGenerator
{
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/application_kns2018.docx';
    const VIEW_FILE_NAME = 'KNS_application_2018';

    protected function setConstants()
    {
        $this->tables = parent::TABLES;
        $this->fields = parent::FIELDS;
        $this->fileName = self::FILE_NAME;
        $this->viewFileName = self::VIEW_FILE_NAME;
    }
}
