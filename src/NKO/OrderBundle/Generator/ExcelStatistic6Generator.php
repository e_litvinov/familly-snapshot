<?php

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\ExcelDownloader;
use PHPExcel_Worksheet;
use Symfony\Component\HttpFoundation\Response;

class ExcelStatistic6Generator extends FileGenerator
{
    const VIEW_FILE_NAME = 'statistic_table';
    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/statistic6_report.xlsx';

    const START_ROW = 7;
    const COLUMN_START = 2;

    const LABEL_START_ROW = 2;
    const LABEL_COLUMN_START = 0;
    const LAST_COLUMN = 40;

    /**
     * @var string $rootDir
     */
    private $rootDir;

    public function __construct(
        $rootDir,
        ExcelDownloader $downloader
    ) {
        $this->rootDir = $rootDir;
        $this->downloader = $downloader;
    }

    /**
     * @param mixed $object
     * @return Response
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function generate($object)
    {
        $filename = $this->rootDir . self::FILE_NAME;
        $tempFileName = tempnam(sys_get_temp_dir(), 'Second_summary_analytics');
        $tempOutFileName = $tempFileName.'.xlsx';
        file_put_contents($tempOutFileName, $filename);

        $excelReader = \PHPExcel_IOFactory::createReaderForFile($filename);
        $excelObj = $excelReader->load($filename);

        /** @var PHPExcel_Worksheet $sheet */
        $sheet = $excelObj->getActiveSheet();

        $informantionString = sprintf(
            "Тип отчета: Мониторинговый отчет Период: c 'Январь %d' по 'Декабрь %d'",
            $object['YearPeriod']['from'],
            $object['YearPeriod']['to']
        );
        $sheet->setCellValueByColumnAndRow(self::LABEL_COLUMN_START, self::LABEL_START_ROW, $informantionString);


        $structuredData = $object['Data'];
        $i = self::START_ROW;
        foreach ($structuredData as $competitionArray) {
            $j = self::COLUMN_START;
            if ($competitionArray) {
                foreach ($competitionArray as $value) {
                    $this->setValue($j, $i, $value['plan'], $sheet);
                    $this->setValue($j, $i, $value['fact'], $sheet);
                }
            } else {
                while ($j < self::LAST_COLUMN) {
                    $this->setValue($j, $i, 0, $sheet);
                }
            }

            $i++;
        }

        $objWriter = new \PHPExcel_Writer_Excel5($excelObj);
        $objWriter->save($tempOutFileName);

        return $this->downloader->getStreamedResponse($tempOutFileName, self::VIEW_FILE_NAME);
    }

    public function setValue(&$j, &$i, $value, $sheet)
    {
        $sheet->setCellValueByColumnAndRow($j++, $i, $value ? $value : 0);
    }
}
