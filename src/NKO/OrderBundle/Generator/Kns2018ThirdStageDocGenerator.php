<?php

namespace NKO\OrderBundle\Generator;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Downloader\DocDownloader;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Resolver\ProxyResolver;
use PhpOffice\PhpWord\TemplateProcessor;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class Kns2018ThirdStageDocGenerator extends Kns2017SecondStageDocGenerator
{
    const FIELDS = [
        'projectImplementation',
        'legalFederalDistrict',
        'legalLocality',
        'actualFederalDistrict',
        'actualLocality',
        'sumGrant',
        'contract',
    ];

    const TABLES = [
        'publications' => [
            'publication_name_td' => 'publicationName',
            'publication_year_td' => 'yearPublication',
            'publication_link_td' => 'link'
        ],
    ];

    const UNSET_FIELDS = [
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingMobilePhone',
    ];

    const FILE_NAME = '/../src/NKO/OrderBundle/Resources/templates/kns2018_third_stage.docx';

    protected $em;

    public function __construct(
        $rootDir,
        FillWordTemplateService $wordTemplateService,
        PropertyAccessor $propertyAccessor,
        DocDownloader $downloader,
        ProxyResolver $proxyResolver,
        EntityManager $em
    ) {
        $this->wordTemplateService = $wordTemplateService;
        $this->proxyResolver = $proxyResolver;
        $this->accessor = $propertyAccessor;
        $this->downloader = $downloader;
        $this->document = new TemplateProcessor($rootDir . self::FILE_NAME);
        $this->em = $em;
    }


    protected function fillTemplate()
    {
        $this->wordTemplateService->setDocument($this->document);
        $this->proxyResolver->resolveProxies($this->object->getAuthor());
        $this->wordTemplateService->setObject($this->object);
        $realAuthor = $this->em->find(get_class($this->getObject()->getAuthor()), $this->getObject()->getAuthor()->getId());
        $this->fillAuthorData($realAuthor);

        $this->fillFirstTab($this->object);
        $this->wordTemplateService->fillPeopleCategoriesTable(self::PEOPLE_CATEGORY_TEMPLATE_NAME, true);
        $this->wordTemplateService->fillTrainingGrounds(true, 'trainingGroundParent');
        $this->wordTemplateService->fillTrainingGrounds(false, 'trainingGround');

        $this->wordTemplateService->fillTemplate(array_diff(array_merge(parent::FIELDS, self::FIELDS), self::UNSET_FIELDS));
        $this->wordTemplateService->fillTables(array_merge(parent::TABLES, self::TABLES));
        $this->fillEffectiveness();
    }


    private function fillFirstTab($object)
    {
        /** @var \DateTime $date */
        $date = $object->getCompetition()->getRealizationYear();
        $year = $date ? $date->format('Y') : (new \DateTime())->format('Y');


        $competitionName = $object->getCompetition()->getName();

        $this->document->setValue('competitionName', $competitionName);
        $this->document->setValue('realizationYear', $year);
        $this->document->setValue('normativeActs', $this->getNormativeActs($object));
    }


    protected function fillEffectiveness()
    {
        $table = 'effectivenessKNSItems';
        $fields = [
            'eff_indicator_td' => 'indicator',
            'eff_factValue_td' => 'factValue',
            'eff_planValue_td' => 'planValue',
            'eff_measurementMethod_td' => 'methods',
            'eff_comment_td' => 'comment',
        ];
        $collection = $this->accessor->getValue($this->object, $table);
        $countRows = $collection->count();
        if ($countRows == 0) {
            foreach ($fields as $templateVar => $objAttr) {
                $this->document->setValue("{$templateVar}", " ");
            }
            return;
        }

        $i = 1;
        $isCloneRow = false;
        foreach ($collection as $item) {
            foreach ($fields as $templateVar => $objAttr) {
                if (!$isCloneRow) {
                    $this->document->cloneRow($templateVar, $countRows);
                    $isCloneRow = true;
                }

                if ($objAttr == 'indicator') {
                    $indicator = $this->accessor->getValue($item, $objAttr);
                    $value = $indicator->getTitle();
                } elseif ($objAttr == 'methods') {
                    $titles = [];
                    foreach ($this->accessor->getValue($item, $objAttr) as $method) {
                        $titles[] = $this->accessor->getValue($method, 'name');
                    }

                    $value = implode(', ', $titles);
                } else {
                    $value = $this->accessor->getValue($item, $objAttr);
                }

                $this->document->setValue("{$templateVar}#{$i}", mb_convert_encoding($value, 'UTF-8'));
            }
            $i++;
        }
    }

    private function getNormativeActs($object)
    {
        $titles = [];
        foreach ($object->getNormativeActs() as $normativeAct) {
            $titles[] = $normativeAct->getTitle();
        }

        return implode(', ', $titles);
    }
}
