<?php
/**
 * Created by PhpStorm.
 * User: oksana
 * Date: 1/9/18
 * Time: 6:29 PM
 */

namespace NKO\OrderBundle\Generator;

use NKO\OrderBundle\Downloader\PdfDownloader;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\StreamedResponse;

class BriefApplication2017PdfGenerator extends FileGenerator
{
    const TEMPLATE_PATH = 'NKOOrderBundle:CRUD/brief_application:show.html.twig';
    const VIEW_FILE_NAME = 'Application_2017';

    /**
     * @var TwigEngine $twigEngine
     */
    private $twigEngine;

    /**
     * BriefApplication2017PdfGenerator constructor.
     * @param PdfDownloader $downloader
     * @param TwigEngine $twigEngine
     */
    public function __construct(
        PdfDownloader $downloader,
        TwigEngine $twigEngine
    ) {
        $this->downloader = $downloader;
        $this->twigEngine = $twigEngine;
    }

    /**
     * @param mixed $object
     * @return bool|\Symfony\Component\HttpFoundation\Response|StreamedResponse
     * @throws \Twig_Error
     */
    public function generate($object)
    {
        $html = $this->twigEngine->render(self::TEMPLATE_PATH, [
            'application' => $object,
            'action' => 'show'
        ]);

        $this->setObject($object);
        $author = $this->getObject()->getAuthor();
        $psrn = $author ? $author->getPsrn() : $this->getObject()->getPsrn();
        $viewFileName = $psrn . ' _' . self::VIEW_FILE_NAME;

        return $this->downloader->getStreamedResponse($html, $viewFileName);
    }
}