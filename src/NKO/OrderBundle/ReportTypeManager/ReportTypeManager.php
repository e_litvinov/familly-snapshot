<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/7/17
 * Time: 9:56 AM
 */

namespace NKO\OrderBundle\ReportTypeManager;


class ReportTypeManager
{
    private $reports;
    const TYPES = [ 'Аналитический' => 'analytical',
                    'Финансовый' => 'financial',
                    'Смешанный' => 'mixed',
                    'Содержательный КНС' => 'analytic-kns'
        ];

    public function __construct()
    {
        $this->reports = array();
    }

    public function addReportsClass($report, $alias)
    {
        $this->reports[$alias] = get_class($report);
    }

    public function getReportsClass()
    {
        return $this->reports;
    }

    public function addReportsType($report, $alias)
    {
        if(array_key_exists($alias,self::TYPES )){
            $this->reports[$alias] = self::TYPES[$alias];
        }
    }

    public function getReportsType()
    {
        return $this->reports;
    }

}