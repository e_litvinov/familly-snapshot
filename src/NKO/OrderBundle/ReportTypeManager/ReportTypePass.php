<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 6/7/17
 * Time: 10:16 AM
 */

namespace NKO\OrderBundle\ReportTypeManager;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ReportTypePass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('nko.report_type_manager')) {
            return;
        }

        $definition = $container->findDefinition('nko.report_type_manager');

        $this->addMethod($container, $definition, 'nko.report_class', 'addReportsClass');

    }

    private function addMethod($container, $definition, $taggedServicesName, $method)
    {
        $taggedServices = $container->findTaggedServiceIds($taggedServicesName);

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall($method, array(
                    new Reference($id),
                    $attributes["alias"]
                ));
            }
        }

    }

}