<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.08.16
 * Time: 1:39
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\BeneficiaryGroup;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;

class LoadBeneficiaryGroupData implements FixtureInterface
{
    public function createGroup(ObjectManager $manager, $groupName)
    {
        $group = $manager
            ->getRepository('NKOOrderBundle:BeneficiaryGroup')
            ->findOneBy(array('groupName' => $groupName));

        if(!$group) {
            $group = new BeneficiaryGroup();
            $group->setGroupName($groupName);
            $manager->persist($group);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->createGroup($manager, "кризисные кровные семьи с детьми");
        $this->createGroup($manager, "кандидаты в замещающие (приемные) родители");
        $this->createGroup($manager, "замещающие (приемные) семьи с детьми");
        $this->createGroup($manager, "дети-сироты и дети, оставшиеся без попечения родителей, " .
            "воспитываемые в детских учреждениях");
        $this->createGroup($manager, "выпускники детских учреждений и дети,в отношении которых " .
            "прекращена опека (попечительство) в замещающей семье (дети в возрасте от 18 до 23 лет)");

        $manager->flush();
    }
}