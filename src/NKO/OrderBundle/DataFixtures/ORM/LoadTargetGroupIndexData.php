<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/3/17
 * Time: 10:56 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use NKO\OrderBundle\Entity\Farvater\TargetGroupIndex;

class LoadTargetGroupIndexData implements FixtureInterface
{
    public function createIndex(ObjectManager $manager, $name) {
        $index = $manager->getRepository('NKOOrderBundle:Farvater\TargetGroupIndex')
            ->findOneBy(array('name' => $name));

        if(!$index) {
            $index = new TargetGroupIndex();
            $index->setName($name);
            $manager->persist($index);
        }

    }

    public function load(ObjectManager $manager) {
        $this->createIndex($manager, "кризисные кровные семьи с детьми");
        $this->createIndex($manager, "кандидаты в замещающие (приемные) родители");
        $this->createIndex($manager, "замещающие (приемные) семьи");
        $this->createIndex($manager, "дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях");
        $this->createIndex($manager, "«выпускники»: дети в возрасте от 18 до 23 лет – выпускники детских учреждений; дети, в отношении которых прекращена опека (попечительство) в замещающей семье");
        $this->createIndex($manager, "специалисты сферы защиты детства (уточните)");
        $this->createIndex($manager, "иные (укажите)");
        $this->createIndex($manager, "подростки");
        $this->createIndex($manager, "сиблинги (братья, сестры)");
        $this->createIndex($manager, "дети с ограниченными возможностями здоровья (уточните)");
        $this->createIndex($manager, "дети со специальными потребностями в обучении и др. (уточните)");
        $this->createIndex($manager, "иные (укажите)");
        $manager->flush();
    }
}