<?php

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 11.08.16
 * Time: 10:31
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Region;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Kernel;

class LoadRegionData implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        /**
         * @var Kernel $kernel
         */
        $kernel = $this->container->get('kernel');

        $contentFromJsonFile = file_get_contents($kernel->locateResource('@NKOOrderBundle/Resources/fixtures/russia.federal-districts.json'));

        $convertJsonToArray = json_decode($contentFromJsonFile, true);
        $districts = $convertJsonToArray['data'];
        foreach ($districts as $district) {
            $areas = $district['areas'];
            foreach ($areas as $area) {
                if(!$manager->getRepository('NKOOrderBundle:Region')->findOneBy(array('name' => $area['name']))) {
                    $region = new Region();
                    $region->setName($area['name']);
                    $manager->persist($region);
                }
            }
        }
        if(!$manager->getRepository('NKOOrderBundle:Region')->findOneBy(array('name' => 'Республика Крым'))) {
            $region1 = new Region();
            $region1->setName('Республика Крым');
            $manager->persist($region1);
        }
        if(!$manager->getRepository('NKOOrderBundle:Region')->findOneBy(array('name' => 'Город Севастополь'))) {
            $region2 = new Region();
            $region2->setName('Город Севастополь');
            $manager->persist($region2);
        }
        $manager->flush();
    }
}