<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/19/17
 * Time: 9:39 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\SpreadActivityIndex;

class LoadSpreadActivityIndexData implements FixtureInterface
{
    public function createIndex(ObjectManager $manager, $name)
    {
        $index = $manager->getRepository('NKOOrderBundle:Farvater\SpreadActivityIndex')
            ->findOneBy(array('name' => $name));
        if(!$index) {
            $index = new SpreadActivityIndex();
            $index->setName($name);
            $manager->persist($index);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createIndex($manager, "подготовка, издание методических материалов");
        $this->createIndex($manager, "размещение информации олнайн (сайты и пр.)");
        $this->createIndex($manager, "электронные рассылки созданных материалов, ссылок на них");
        $this->createIndex($manager, "участие в публичных мероприятиях (презентации, выступления и пр.)");
        $this->createIndex($manager, "организация и проведение публичных мероприятий (конференций, круглых столов и пр.)");
        $this->createIndex($manager, "вебинары");
        $this->createIndex($manager, "групповые тренинги");
        $this->createIndex($manager, "индивидуальные консультации (очно / онлайн)");
        $this->createIndex($manager, "супервизии");
        $this->createIndex($manager, "коучинг / наставничество");
        $this->createIndex($manager, "организация стажировок на базе организации / выезды специалистов");

        $manager->flush();
    }
}