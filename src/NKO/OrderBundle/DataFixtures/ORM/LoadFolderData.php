<?php


namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Folder;
use NKO\OrderBundle\Entity\DocumentType;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;

class LoadFolderData implements FixtureInterface
{
    private function createFolder(ObjectManager $manager, $folderName, $documentTypes, $reportClass)
    {
        $folder = $manager->getRepository('NKOOrderBundle:Folder')->findOneBy(array('folder' => $folderName));
        if(!$folder) {
            $folder = new Folder();
            $manager->persist($folder);
        }
        $folder->setFolder($folderName);
        $folder->setReportClass($reportClass);
        if (!empty($documentTypes)) {
            foreach ($documentTypes as $type) {
                $this->createDocumentType($manager, $type, $folder);
            }
        }
        return $folder;
    }

    private function createDocumentType(ObjectManager $manager, $type, $folder)
    {
        $documentType = $manager->getRepository('NKOOrderBundle:DocumentType')->findOneBy(array('documentType' => $type));
        if(!$documentType) {
            $documentType = new DocumentType();
            $documentType->setFolder($folder);
            $manager->persist($documentType);
        }
        $documentType->setDocumentType($type);
    }

    public function load(ObjectManager $manager)
    {
        $reportClass = Report::class;
        $this->createFolder($manager, 'Транспортные расходы', array('Приказы', 'Отчеты по командировкам',
            'Посадочные талоны',  'Счета, акты на оказанные услуги', 'Авансовые отчеты',
            'Заявления сотрудников о выплате суточных', 'Внутренние локальные акты по командировкам', 'Путевые листы', 'ПТС',
            'Товарные, кассовые чеки, накладные', 'Другие документы' ), $reportClass);

        $this->createFolder($manager, 'Расходы на последующие мероприятия по внедрению полученных знаний и опыта',
            array('Товарные накладные','Акты на оказанные услуги', 'Договора',
                'Требования-накладные', 'Акты передачи','Приказы','Другие документы'), $reportClass);
        $this->createFolder($manager, 'Аналитическая отчетность',
            array( 'Списки участников мероприятий с подписями',  'Фотоальбомы',
                'Программы мероприятий', 'Печатные материалы', 'Другие документы'), $reportClass);
        $manager->flush();

        $reportClass = FinanceReport::class;
        $paidFolder = $this->createFolder($manager, 'Платежные документы', array(), $reportClass);
        $confirmFolder = $this->createFolder($manager, 'Подтверждающие документы', array(), $reportClass);
        $this->createFolder($manager, 'Безналичный расчет', array(
            'Платежное поручение',
            'Банковская выписка',
            'Расходный ордер',
            'Регистр по з/п',
            'Иное'
        ), $reportClass)->setParent($paidFolder);
        $this->createFolder($manager, 'Наличный расчет', array(
            'Кассовый чек',
            'Товарный чек',
            'Авансовый отчет',
            'БСО',
            'Платежная ведомость',
            'Расходный кассовый ордер',
            'Приходный кассовый ордер',
            'Иное'
        ), $reportClass)->setParent($paidFolder);
        $this->createFolder($manager, 'Оплата труда', array(
            'Расчетно-платежная ведомость',
            'Табель учета р/времени',
            'Трудовой договор',
            'Дополнительное соглашение',
            'Приказ',
            'Справка о начислении налогов и взносов с з/п',
            'Приказ о назначении круга ли по проекту'
        ), $reportClass)->setParent($confirmFolder);
        $this->createFolder($manager, 'Материальные расходы', array(
            'Счет на оплату',
            'Акт выполненных работ',
            'Товарная накладная',
            'Товарно-транспортная накладная',
            'Требование-накладная',
            'ОС-1',
            'Путевой лист',
            'УПД',
            'Договор',
            'Дополнительное соглашение',
            'Приказ по подотчетным лицам'
        ), $reportClass)->setParent($confirmFolder);
        $this->createFolder($manager, 'Командировочные расходы', array(
            'Приказ на командировку',
            'Положение о командировке (указать р-р командир-х)',
            'Отчет о командировке',
            'Билет',
            'Посадочный талон',
            'Счет на оплату',
            'Акт выполненных работ'
        ), $reportClass)->setParent($confirmFolder);
        $manager->flush();
    }
}