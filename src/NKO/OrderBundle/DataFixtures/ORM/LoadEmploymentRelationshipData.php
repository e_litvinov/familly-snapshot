<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:30 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\EmploymentRelationship;

class LoadEmploymentRelationshipData implements FixtureInterface
{
    public function createEmploymentRelationship(ObjectManager $manager, $name)
    {
        $relationship = $manager->getRepository('NKOOrderBundle:Farvater\EmploymentRelationship')
            ->findOneBy(array('name' => $name));

        if(!$relationship) {
            $relationship = new EmploymentRelationship();
            $relationship->setName($name);
            $manager->persist($relationship);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createEmploymentRelationship($manager, "собственный сотрудник");
        $this->createEmploymentRelationship($manager, "привлеченный сотрудник");
        $this->createEmploymentRelationship($manager, "доброволец");

        $manager->flush();
    }
}