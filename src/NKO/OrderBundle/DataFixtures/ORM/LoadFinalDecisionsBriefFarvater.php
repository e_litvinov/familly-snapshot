<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/24/17
 * Time: 2:47 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\FinalDecision;
use NKO\OrderBundle\Entity\BriefApplication2016\Application;

class LoadFinalDecisionsBriefFarvater implements FixtureInterface
{
    public function createFinalDecision(ObjectManager $manager, $name, $type)
    {
        $finalDecision = $manager->getRepository('NKOOrderBundle:FinalDecision')->findOneBy(array('name' => $name));

        if (!$finalDecision) {
            $finalDecision = new FinalDecision();
            $finalDecision->setName($name);
            $finalDecision->setTypeDecision($type);
            $manager->persist($finalDecision);
        }

        $competition = $manager
            ->getRepository('NKOOrderBundle:Competition')
            ->findOneBy(['applicationClass' => Application::class]);

        if (!$finalDecision->getCompetitions()->contains($competition) && $competition) {
            $finalDecision->addCompetition($competition);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->createFinalDecision($manager, "Допустить проект к участию во II этапе Конкурса", false);
        $this->createFinalDecision($manager, "Допустить проект к участию во II этапе (с условием внесения изменений в дизайн проекта и/или его бюджет)", false);
        $this->createFinalDecision($manager, "Отклонить проект", true);

        $manager->flush();
    }
}
