<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/19/17
 * Time: 9:47 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\ParticipationIndex;

class LoadParticipationIndexData implements FixtureInterface
{
    public function createIndex(ObjectManager $manager, $name)
    {
        $index = $manager->getRepository('NKOOrderBundle:Farvater\ParticipationIndex')
            ->findOneBy(array('name' => $name));

        if (!$index) {
            $index = new ParticipationIndex();
            $index->setName($name);
            $manager->persist($index);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createIndex($manager, "Число специалистов, принявших участие в мероприятиях по распространению практики");
        $this->createIndex($manager, "Число организаций, принявших участие в мероприятиях по распространению");

        $manager->flush();
    }
}