<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:45 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\ResourceType;

class LoadResourceTypeData implements FixtureInterface
{
    public function createResource(ObjectManager $manager, $name)
    {
        $relationship = $manager->getRepository('NKOOrderBundle:Farvater\ResourceType')->findOneBy(array('name' => $name));
        if(!$relationship) {
            $relationship = new ResourceType();
            $relationship->setName($name);
            $manager->persist($relationship);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createResource($manager, "Помещение");
        $this->createResource($manager, "Оборудование");
        $this->createResource($manager, "Финансовые средства");
        $this->createResource($manager, "Труд добровольцев");

        $manager->flush();
    }
}