<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 3:09 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\TargetAudience;

class LoadTargetAudienceData implements FixtureInterface
{
    public function createAudience(ObjectManager $manager, $name, $type)
    {
        $audience = $manager->getRepository('NKOOrderBundle:Farvater\TargetAudience')
            ->findOneBy(array('name' => $name));

        if($audience){
            $audience->setApplicationType($type);
        }
        else{
            $audience = new TargetAudience();
            $audience->setName($name);
            $audience->setApplicationType($type);
        }

        $manager->persist($audience);
    }

    public function load(ObjectManager $manager) {
        $this->createAudience($manager, "специалисты Служб сопровождения и семейного устройства", 'Farvater-2016');
        $this->createAudience($manager, "воспитатели, педагоги, психологи и другие специалисты детских домов, школ-интернатов и иных учреждений для детей", 'Farvater-2016');
        $this->createAudience($manager, "педагоги, психологи и другие сотрудники общеобразовательных учреждений (школы, детские сады, техникумы и пр.)", 'Farvater-2016');
        $this->createAudience($manager, "специалисты органов опеки и попечительства", 'Farvater-2016');
        $this->createAudience($manager, "сотрудники негосударственных некоммерческих организаций, деятельность которых направлена на содействие в области профилактики социального сиротства, семейного устройства", 'Farvater-2016');
        $this->createAudience($manager, "ассоциации, сообщества замещающих родителей и пр.", 'Farvater-2016');
        $this->createAudience($manager, "иные организации и специалисты сферы защиты детства (укажите, какие именно)", 'Farvater-2016');
        
        $this->createAudience($manager, 'Службы сопровождения и центры семейного устройства', 'Farvater-2017');
        $this->createAudience($manager, 'Отделы / органы опеки и попечительства', 'Farvater-2017');
        $this->createAudience($manager, 'Негосударственные некоммерческие организации (НКО), деятельность которых направлена на содействие в области профилактики социального сиротства, семейного устройства', 'Farvater-2017');
        $this->createAudience($manager, 'Детские дома, школы-интернаты и иные учреждения, в которых воспитываются дети', 'Farvater-2017');
        $this->createAudience($manager, 'Школы, детские сады, техникумы и иные общеобразовательные учреждения', 'Farvater-2017');
        $this->createAudience($manager, 'Ассоциации, сообщества приёмных (замещающих) родителей и пр.', 'Farvater-2017');

        $manager->flush();
    }
}