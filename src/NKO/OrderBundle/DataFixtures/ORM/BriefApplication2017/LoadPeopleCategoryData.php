<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/9/17
 * Time: 1:57 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\BriefApplication2017;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;

class LoadPeopleCategoryData implements FixtureInterface
{
    public function createCategory(ObjectManager $manager, $name, $parentGroup = null)
    {
        $category = $manager->getRepository('NKOOrderBundle:BriefApplication2017\PeopleCategory')
            ->findOneBy(array('name' => $name));

        if(!$category) {
            $category = new PeopleCategory();
            $category->setName($name);
            $category->setParent($parentGroup);
            $manager->persist($category);
        }

        return $category;
    }

    public function load(ObjectManager $manager)
    {
        $group1 = $this->createCategory($manager, 'Кризисные кровные семьи с детьми');
        $this->createCategory($manager, 'Родители (кризисные кровные семьи)', $group1);
        $group2 = $this->createCategory($manager, 'Дети (кризисные кровные семьи)', $group1);
        $this->createCategory($manager, 'Дети с ОВЗ (кризисные кровные семьи)', $group2);
        $this->createCategory($manager, 'Подростки (кризисные кровные семьи)', $group2);
        $this->createCategory($manager, 'Дети до 3 лет (кризисные кровные семьи)', $group2);

        $group3 = $this->createCategory($manager, 'Замещающие семьи с детьми');
        $this->createCategory($manager, 'Родители (замещающие семьи)', $group3);
        $group4 = $this->createCategory($manager, 'Дети (замещающие семьи)', $group3);
        $this->createCategory($manager, 'Дети с ОВЗ (замещающие семьи)', $group4);
        $this->createCategory($manager, 'Подростки (замещающие семьи)', $group4);
        $this->createCategory($manager, 'Дети до 3 лет (замещающие семьи)', $group4);
        $this->createCategory($manager, 'Сиблинги (замещающие семьи)', $group4);

        $group5 = $this->createCategory($manager, 'Кандидаты в замещающие родители');
        $this->createCategory($manager, 'Кандидаты в замещающие родители', $group5);

        $group6 = $this->createCategory($manager, 'Дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях');
        $this->createCategory($manager, 'Дети с ОВЗ (дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях)', $group6);
        $this->createCategory($manager, 'Сиблинги (дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях)', $group6);
        $this->createCategory($manager, 'Подростки (дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях)', $group6);
        $this->createCategory($manager, 'Дети до 3 лет (дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях)', $group6);

        $group7 = $this->createCategory($manager, '«Выпускники»: дети в возрасте от 18 до 23 лет – выпускники детских учреждений; дети, в отношении которых прекращена опека (попечительство) в замещающей семье');
        $this->createCategory($manager, 'Выпускники', $group7);

        $manager->flush();
    }
}