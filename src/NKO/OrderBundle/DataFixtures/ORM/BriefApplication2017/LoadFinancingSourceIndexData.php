<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/30/17
 * Time: 3:13 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\BriefApplication2017;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\BriefApplication2017\FinancingSourceIndex;

class LoadFinancingSourceIndexData implements FixtureInterface
{
    public function createIndex(ObjectManager $manager, $name)
    {
        if(!$manager->getRepository('NKOOrderBundle:BriefApplication2017\FinancingSourceIndex')->findOneBy(array('name' => $name))) {
            $index = new FinancingSourceIndex();
            $index->setName($name);
            $manager->persist($index);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createIndex($manager, "грант");
        $this->createIndex($manager, "субсидия");
        $this->createIndex($manager, "пожертвование");
        $this->createIndex($manager, "государственный заказ");
        $this->createIndex($manager, "муниципальный заказ");
        $this->createIndex($manager, "собственные средства организации");
        $this->createIndex($manager, "иное");

        $manager->flush();
    }

}