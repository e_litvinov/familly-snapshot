<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 5/24/17
 * Time: 12:21 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Farvater\FarvaterApplication;
use NKO\OrderBundle\Entity\FinalDecision;
use NKO\OrderBundle\Entity\MarkCriteria;
use NKO\OrderBundle\Entity\QuestionMark;

class LoadFarvaterMarkAttribute implements FixtureInterface
{

    public function createFinalDecision(ObjectManager $manager, $name, $type, $competition)
    {
        $finalDecision = $manager->getRepository('NKOOrderBundle:FinalDecision')->findOneBy(array('name' => $name));

        if (!$finalDecision) {
            $finalDecision = new FinalDecision();
            $finalDecision->setName($name);
            $finalDecision->setTypeDecision($type);
            $manager->persist($finalDecision);
        }

        if (!$finalDecision->getCompetitions()->contains($competition)) {
            $finalDecision->addCompetition($competition);
        }
    }


    public function createCriteria(ObjectManager $manager, $criterionName)
    {
        $criterion = $manager->getRepository('NKOOrderBundle:MarkCriteria')->findOneBy(array('criterion' => $criterionName));

        if (!$criterion) {
            $criterion = new MarkCriteria();
            $criterion->setCriterion($criterionName);

            $manager->persist($criterion);
            $manager->flush();
        }

        return $criterion;
    }

    public function createQuestion(ObjectManager $manager, $data, $criteria, $competition)
    {
        $question = $manager->getRepository('NKOOrderBundle:QuestionMark')->findOneBy(array('question' => $data['name']));
        
        if (!$question) {
            $question = new QuestionMark();
            $question->setQuestion($data['name']);
            $question->setCriteria($criteria);
            $question->setRangeMark($data['range']);
            $manager->persist($question);
        }

        if (!$question->getCompetitions()->contains($competition)) {
            $question->addCompetition($competition);
        }
    }


    public function load(ObjectManager $manager)
    {
        $competition = $manager
            ->getRepository('NKOOrderBundle:Competition')
            ->findOneBy(['applicationClass' => FarvaterApplication::class]);

        if (!$competition) {
            $competition = new Competition();
            $competition->setName('Полная заявка СФ-2016');
            $dateString = '01.01.2016';
            list($day, $month, $year) = explode('.', $dateString);
            $date = new \DateTime();
            $date->setDate($year, $month, $day);
            $competition->setStartDate($date);
            $dateString = '09.07.2016';
            list($day, $month, $year) = explode('.', $dateString);
            $date = new \DateTime();
            $date->setDate($year, $month, $day);
            $competition->setFinishDate($date);
            $competition->setApplicationClass('NKO\OrderBundle\Entity\Farvater\FarvaterApplication');
            $manager->persist($competition);
            $manager->flush();
        }

        $this->createFinalDecision($manager, 'Поддержать проект', false, $competition);
        $this->createFinalDecision($manager, 'Поддержать проект (с условием внесения изменений в дизайн проекта и/или его бюджет)', false, $competition);
        $this->createFinalDecision($manager, "Отклонить проект", true, $competition);

        $criterion = $this->createCriteria($manager, 'ПРАКТИКА');
        $this->createQuestion(
            $manager,
            ['name' => 'Результативность практики для Конкурса и Программы ',
                'range' => 10],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Эффективность практики ',
                'range' => 15],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Преимущественная ориентация практики на «сложные» категории детей ',
                'range' => 5],
            $criterion,
            $competition
        );

        $criterion = $this->createCriteria($manager, 'РАСПРОСТРАНЕНИЕ ПРАКТИКИ');
        $this->createQuestion(
            $manager,
            ['name' => 'Ориентация проекта на внедрение практики другими заинтересованными организациями и специалистами; на формирование отраслевых ресурсных центров – стажировочных площадок ',
                'range' => 20],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Потенциал практики с точки зрения ее внедрения в деятельность других организаций и специалистов сферы защиты детства ',
                'range' => 10],
            $criterion,
            $competition
        );

        $criterion = $this->createCriteria($manager, 'МОНИТОРИНГ И ОЦЕНКА');
        $this->createQuestion(
            $manager,
            ['name' => 'Заинтересованность организации-заявителя в повышении эффективности практики и своей деятельности за счет мониторинга и оценки результатов ',
                'range' => 10],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Продуманность системы мониторинга и оценки результатов проекта ',
                'range' =>5],
            $criterion,
            $competition
        );

        $criterion = $this->createCriteria($manager, 'ДИЗАЙН ПРОЕКТА');
        $this->createQuestion(
            $manager,
            ['name' => 'Логичность и реалистичность проекта ',
                'range' => 10],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Устойчивость результатов проекта ',
                'range' => 10],
            $criterion,
            $competition
        );
        $criterion = $this->createCriteria($manager, 'БЮДЖЕТ');
        $this->createQuestion(
            $manager,
            ['name' => 'Обоснованность и эффективность бюджета проекта ',
                'range' => 20],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Доля со-финансирования (за счет собственных средств и средств организаций-партнеров) в бюджете проекта ',
                'range' => 20],
            $criterion,
            $competition
        );
        $this->createQuestion(
            $manager,
            ['name' => 'Организационный потенциал Заявителя ',
                'range' => 20],
            $criterion,
            $competition
        );

        $manager->flush();
    }
}
