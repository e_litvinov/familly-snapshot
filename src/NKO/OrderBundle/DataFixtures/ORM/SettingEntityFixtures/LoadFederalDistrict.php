<?php


namespace NKO\OrderBundle\DataFixtures\ORM\SettingEntityFixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\SettingEntity\FederalDistrict;

class LoadFederalDistrict implements FixtureInterface
{
    const DISTRICTS = [
        'ЦФО',
        'СЗФО',
        'ЮФО',
        'СКФО',
        'ПФО',
        'УрФО',
        'СФО',
        'ДФО',
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::DISTRICTS as $district) {
            if (!$manager->getRepository(FederalDistrict::class)->findOneBy(array('name' => $district))) {
                $region = new FederalDistrict();
                $region->setName($district);
                $manager->persist($region);
            }
        }

        $manager->flush();
    }
}
