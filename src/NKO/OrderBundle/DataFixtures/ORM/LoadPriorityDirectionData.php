<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:05 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;

class LoadPriorityDirectionData implements FixtureInterface
{
    public function createDirection(ObjectManager $manager, $name, $type)
    {
        if(!$manager->getRepository('NKOOrderBundle:Farvater\PriorityDirection')->findOneBy(array('name' => $name))) {
            $direction = new PriorityDirection();
            $direction->setName($name);
            $direction->setApplicationType($type);
            $manager->persist($direction);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createDirection($manager, "профилактика социального сиротства, работа с «кровными» (биологическими) семьями", 'farvater_application');
        $this->createDirection($manager, "профилактика социального сиротства через работу с кровными (биологическими) семьями/ работа по возврату детей из детских учреждений в кровные семьи", 'brief_application');
        $this->createDirection($manager, "подготовка детей, воспитываемых в организациях для детей-сирот и детей, оставшихся без попечения родителей, к семейному устройству", 'application');
        $this->createDirection($manager, "подготовка и сопровождение замещающих семей, профилактика вторичного сиротства и работа с семьей по профилактике изъятия детей из семей", 'farvater_application');
        $this->createDirection($manager, "подготовка и сопровождение замещающих семей, профилактика вторичного сиротства (профилактика отобраний (изъятий) / отказов детей из замещающих семей)", 'brief_application');
        $this->createDirection($manager, "«полный цикл» подготовки детей к семейному устройству и подбора для них, подготовки и сопровождения замещающих семей, работа по возврату в кровные семьи", 'farvater_application');
        $this->createDirection($manager, "«полный цикл» подготовки детей к семейному устройству и подбора для них, подготовки и сопровождения замещающих семей, работа по возврату детей в кровные семьи и сопровождение этих семей, профилактики социального сиротства", 'brief_application');
        $this->createDirection($manager, "реформирование организаций для детей-сирот и детей, оставшихся без попечения родителей (детских домов, школ-интернатов, домов ребенка, социальных приютов и пр.)", 'application');
        $this->createDirection($manager, "постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет", 'farvater_application');
        $this->createDirection($manager, "постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет – выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека (попечительство) в замещающей семье", 'brief_application');
        $this->createDirection($manager, "активизация поддержки замещающих и кризисных семей со стороны окружения", 'farvater_application');
        $this->createDirection($manager, "активизация поддержки замещающих и «кризисных» кровных семей со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.); развитие взаимоподдержки, консолидации сообществ членов замещающих семей (родителей, «выпускников» замещающих семей)", 'brief_application');
        $this->createDirection($manager, "методическое сопровождение служб / центров по профилактике социального сиротства и/или по семейному устройству детей-сирот и детей, оставшихся без попечения родителей", 'farvater_application');
        $this->createDirection($manager, "иные эффективные практики в сфере профилактики социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей (укажите, какие именно)", 'farvater_application');

        $manager->flush();
    }
}