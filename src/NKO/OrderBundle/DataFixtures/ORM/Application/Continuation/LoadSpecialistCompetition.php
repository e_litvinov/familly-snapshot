<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Continuation;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Continuation\SpecialistCompetition;


class LoadSpecialistCompetition implements FixtureInterface
{
    private function createSpecialistCompetitionData(ObjectManager $manager, $title)
    {
        $specialistCompetition = $manager->getRepository(SpecialistCompetition::class)
            ->findOneBy(['title' => $title]);

        if(!$specialistCompetition) {
            $specialistCompetition = new SpecialistCompetition();
            $manager->persist($specialistCompetition);
        }

        $specialistCompetition->setTitle($title);
    }

    public function load(ObjectManager $manager)
    {
        $this->createSpecialistCompetitionData($manager,'CФ');
        $this->createSpecialistCompetitionData($manager, 'КНС');
        $this->createSpecialistCompetitionData($manager, 'Нет');

        $manager->flush();
    }
}