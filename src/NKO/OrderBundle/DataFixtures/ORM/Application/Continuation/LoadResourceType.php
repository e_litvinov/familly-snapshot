<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Continuation;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Continuation\ResourceType;


class LoadResourceType implements FixtureInterface
{
    private function createResourceTypeData(ObjectManager $manager, $code, $title)
    {
        $resourceType = $manager->getRepository(ResourceType::class)
            ->findOneBy(['code' => $code]);

        if(!$resourceType) {
            $resourceType = new ResourceType();
            $manager->persist($resourceType);
        }

        $resourceType->setCode($code);
        $resourceType->setTitle($title);
    }

    public function load(ObjectManager $manager)
    {
        $this->createResourceTypeData($manager, 'room', 'Помещение');
        $this->createResourceTypeData($manager, 'equipment', 'Оборудование');
        $this->createResourceTypeData($manager, 'financial_resources', 'Финансовые средства');
        $this->createResourceTypeData($manager, 'work_volunteers', 'Труд добровольцев');

        $manager->flush();
    }
}