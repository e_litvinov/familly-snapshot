<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Continuation;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Continuation\TargetGroup;
use NKO\OrderBundle\Utils\ApplicationTypes;

class LoadTargetGroup implements FixtureInterface
{
    const GROUPS = [
        ['Специалисты служб сопровождения и центров семейного устройства',[
            ApplicationTypes::CONTINUATION_APPLICATION_2018,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018
        ]],
        ['Специалисты отделов / органов опеки и попечительств',[
            ApplicationTypes::CONTINUATION_APPLICATION_2018,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018
        ]],
        ['Специалисты негосударственных некоммерческих организаций (НКО), деятельность которых направлена на содействие в области профилактики социального сиротства, семейного устройства', [
            ApplicationTypes::CONTINUATION_APPLICATION_2018,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018
        ]],
        ['Специалисты детских домов, школ-интернатов и иных учреждений, в которых воспитываются дети',[
            ApplicationTypes::CONTINUATION_APPLICATION_2018,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018
        ]],
        ['Специалисты школ, детских садов, техникумов и иных общеобразовательных учреждений',[
            ApplicationTypes::CONTINUATION_APPLICATION_2018,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018
        ]],
        ['Члены ассоциаций, сообществ приёмных (замещающих) родителей и пр.',[
            ApplicationTypes::CONTINUATION_APPLICATION_2018,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018
        ]],
        ['Нет',[
            ApplicationTypes::CONTINUATION_APPLICATION_2018
        ]]
    ];

    private function createTargetGroup(ObjectManager $manager, $title, $type)
    {
        $targetGroup = $manager->getRepository(TargetGroup::class)
            ->findOneBy(['title' => $title]);

        if (!$targetGroup) {
            $targetGroup = new TargetGroup();
            $manager->persist($targetGroup);
        }

        $targetGroup->setTitle($title);
        $targetGroup->setType($type);
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::GROUPS as $group) {
            $this->createTargetGroup($manager, $group[0], $group[1]);
        }
        $manager->flush();
    }
}
