<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Continuation;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Continuation\LaborRelationship;
use NKO\OrderBundle\Entity\Application\Continuation\ResourceType;

class LoadLaborRelationship implements FixtureInterface
{
    private function createRelationshipData(ObjectManager $manager, $name)
    {
        $relationship = $manager->getRepository(LaborRelationship::class)
            ->findOneBy(['name' => $name]);

        if(!$relationship) {
            $relationship = new LaborRelationship();
            $manager->persist($relationship);
        }

        $relationship->setName($name);
    }

    public function load(ObjectManager $manager)
    {
        $this->createRelationshipData($manager, 'В штате');
        $this->createRelationshipData($manager, 'Привлеченный специалист');

        $manager->flush();
    }
}