<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/18/18
 * Time: 12:29 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Continuation;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Continuation\ResultDirection;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType;

class LoadResultDirectionData implements FixtureInterface
{
    const TYPE_VALUES = [
        ResultDirection::EXPERIENCE_TYPE => [
           'Увеличить число проектов/программ, в отношении которых в организации регулярно проводится МиО.',
           'Ввести практику оценки с участием детей.',
           'Апробировать новый для организации тип оценки.',
           'Распространять уже имеющийся собственный опыт МиО.'
        ],
        ResultDirection::PROCESS_TYPE => [
            'Доработать систему МиО для проекта (на основе реализации проекта, распространения опыта, обучающих мероприятий).',
            'Сформировать пакет инструментария для сбора данных по каждому результату и показателю проекта / программы / миссии организации (анкеты, формы учета и пр.).',
            'Разработать и внедрить механизм анализа и внутреннего обсуждения полученных в ходе МиО данных.',
            'Разработать и внедрить механизм практического использования данных, полученных в ходе МиО (в т.ч. внесение изменений в деятельность).',
            'Определить круг ключевых внутренних и внешних стейкхолдеров.',
            'Разработать коммуникационную политику в области МиО.',
            'Формализовать процессы МиО: разработать внутренний регламент (или его аналог) для проведения МиО.',
            'Закрепить функционал в отношении МиО в должностных инструкциях сотрудников.'
        ],
        ResultDirection::QUALIFICATION_TYPE => [
            'Базовые знания по МиО.',
            'Навыки разработки инструментов, выбора методов сбора данных.',
            'Навыки анализа полученных данных.'
        ],
        ResultDirection::QUALIFICATION_CONTINUATION_SECOND_STAGE => [
            'Базовые знания по МиО.',
            'Навыки разработки инструментов, выбора методов сбора данных.',
            'Навыки анализа полученных данных.',
            'Навыки для проведения внутренней оценки в организации.',
        ],
        ResultDirection::QUALIFICATION_FARVATER_TYPE => [
            'Базовые знания по МиО.',
            'Навыки разработки инструментов, выбора методов сбора данных.',
            'Навыки анализа полученных данных.',
            'Навыки для проведения внутренней оценки организации.'
        ],
        ResultDirection::RESOURCE_TYPE => [
            'Повысить доступность для сотрудников организации информации по МиО (онлайн-ресурсы, методическая литература, формирование внутренних баз данных и пр.).',
            'Сформировать внутреннюю базу инструментария по сбору данных.',
            'Создать / адаптировать / приобрести IT-систему для хранения и анализа данных.',
            'Ввести практику бюджетирования МиО в организации/проектах.'
        ]
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::TYPE_VALUES as $key => $values) {
            $this->createType($manager, $key, $values);
        }
        $manager->flush();
    }

    private function createType(ObjectManager $manager, $type, $values)
    {
        foreach ($values as $value) {
            $this->createResultDirection($manager, $type, $value);
        }
    }

    private function createResultDirection(ObjectManager $manager, $type, $value)
    {
        $result = $manager->getRepository(ResultDirection::class)->findOneBy([
            'type'  => $type,
            'title' => $value
        ]);

        if (!$result) {
            $result = new ResultDirection();
            $manager->persist($result);
        }

        $result->setType($type);
        $result->setTitle($value);
    }
}
