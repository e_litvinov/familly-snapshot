<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Application\KNS\Application2020;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\KNS\Application2020\Regulation;

class LoadRegulationData implements FixtureInterface
{
    const data = [
        '481' => 'Постановление Правительства РФ от 24.05.2014 №481',
        '442' => 'Федеральный закон от 28.12.2013 №442-ФЗ',
        '44' => 'Федеральный закон от 05.04.2013 №44-ФЗ',
        'normativeAct' => 'Указанные нормативные акты не применяем',
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::data as $key => $item) {
            $this->createValue($manager, $key, $item);
        }

        $manager->flush();
    }

    private function createValue($manager, $code, $value)
    {
        $regulation = $manager->getRepository(Regulation::class)->findOneBy([
            'code' => $code
        ]);

        if (!$regulation) {
            $regulation = new Regulation();
            $regulation->setCode($code);
            $manager->persist($regulation);
        }

        $regulation->setTitle($value);
    }
}
