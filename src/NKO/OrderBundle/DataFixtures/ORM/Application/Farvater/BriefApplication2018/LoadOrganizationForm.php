<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Farvater\BriefApplication2018;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\OrganizationForm;


class LoadOrganizationForm implements FixtureInterface
{
    private function createOrganizationFormData(ObjectManager $manager, $code, $title)
    {
        $organizationForm = $manager->getRepository(OrganizationForm::class)
            ->findOneBy(['code' => $code]);

        if(!$organizationForm) {
            $organizationForm = new OrganizationForm();
            $manager->persist($organizationForm);
        }

        $organizationForm->setCode($code);
        $organizationForm->setTitle($title);
    }

    public function load(ObjectManager $manager)
    {
        $this->createOrganizationFormData($manager, 'state_institutions', 'Казенные бюджетные учреждения');
        $this->createOrganizationFormData($manager, 'autonomous_institutions', 'Автономные бюджетные учреждения');
        $this->createOrganizationFormData($manager, 'other_institutions', 'Иные некоммерческие организации');

        $manager->flush();
    }
}