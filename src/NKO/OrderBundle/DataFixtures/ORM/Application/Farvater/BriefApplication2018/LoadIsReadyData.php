<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/9/17
 * Time: 1:57 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Farvater\BriefApplication2018;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;
use NKO\OrderBundle\Entity\DropDownEntity;

class LoadIsReadyData implements FixtureInterface
{
    private $fieldName;

    public function createCategory(ObjectManager $manager, $name, $parentGroup = null)
    {
        $object = $manager->getRepository(DropDownEntity::class)
            ->findOneBy([
                'name' => $name,
                'fieldName' => $this->fieldName
            ]);

        if(!$object) {
            $object = new DropDownEntity();
            $manager->persist($object);
        }

        $object->setName($name);
        $object->setParent($parentGroup);
        $object->setFieldName($this->fieldName);

        return $object;
    }

    public function load(ObjectManager $manager)
    {
        $this->fieldName = DropDownEntity::IS_READY_FIELD;
        $group1 = $this->createCategory($manager, 'Да');
        $group2 = $this->createCategory($manager, 'Нет');
        $group3 = $this->createCategory($manager, 'Затрудняюсь ответить');

        $this->createCategory($manager, 'принять специалистов других организаций на своей базе', $group1);
        $this->createCategory($manager, 'поездка в организацию для представления своей практики', $group1);
        $this->createCategory($manager, 'публикация материалов, описывающих технологию практики', $group1);
        $this->createCategory($manager, 'иное', $group1);

        $manager->flush();
    }
}