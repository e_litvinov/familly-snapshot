<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 1/18/18
 * Time: 12:29 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Application\Farvater\BriefApplication2018;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\FinancingSourceType;
use NKO\OrderBundle\Entity\BeneficiaryGroup;
use NKO\OrderBundle\Utils\ApplicationTypes;

class LoadFinancingSourceTypesData implements FixtureInterface
{
    const TYPE_VALUES = [
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'own_funds',
            'title' => 'Собственные средства организации'
        ],
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'nko_proceeds',
            'title' => 'Поступления от других российских НКО'
        ],
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'commercial_proceeds',
            'title' => 'Поступления от коммерческих организаций'
        ],
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'private_donations',
            'title' => 'Пожертвования частных лиц'
        ],
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'federal_financing',
            'title' => 'Финансирование из федерального бюджета'
        ],
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'subject_financing',
            'title' => 'Финансирование из бюджета субъекта РФ'
        ],
        [
            'types' => [ApplicationTypes::BRIEF_APPLICATION_2018, ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'local_financing',
            'title' => 'Финансирование из местного (муниципального) бюджета'
        ],
        [
            'types' => [ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'grant',
            'title' => 'Грант'
        ],
        [
            'types' => [ApplicationTypes::KNS_APPLICATION_2018],
            'code' => 'etc',
            'title' => 'Другое'
        ]
    ];

    public function load(ObjectManager $manager)
    {
        foreach (self::TYPE_VALUES as $values) {
            $this->createType($manager, $values);
        }
        $manager->flush();
    }

    private function createType(ObjectManager $manager, $values)
    {
        $type = $manager->getRepository(FinancingSourceType::class)->findOneBy(['code' => $values['code']]);
        if (!$type) {
            $type = new FinancingSourceType();
            $manager->persist($type);
        }

        $type->setCode($values['code']);
        $type->setTitle($values['title']);
        $type->setTypes($values['types']);
    }
}