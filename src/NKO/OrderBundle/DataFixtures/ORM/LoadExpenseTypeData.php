<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:45 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Entity\ExpenseTypeInterface;
use NKO\OrderBundle\Entity\Farvater\ResourceType;
use NKO\OrderBundle\Utils\Numbers;

class LoadExpenseTypeData implements FixtureInterface
{
    const FIRST = '<b>Документы, подтверждающие произведенные расходы (№, дата и
                наименование документа)</b><br><br>
                <i>Табель, расчетно-платежная ведомость, справка, расчет по налогам и взносам, трудовой договор, 
                ДГПХ, ТЗ, акт, дополнительное соглашение</i>';

    const SECOND = '<b>Документы, подтверждающие произведенные расходы (№, дата и
                наименование документа)</b><br><br>
                <i>ТОРГ-12, УПД, счет-фактура, счет, акт выполненных работ, договор</i>';

    const THIRD = '<b>Документы, подтверждающие произведенные расходы (№, дата и
                наименование документа)</b><br><br>
                <i>Счет, договор, акт выполненных работ, отчет</i>';

    const FOURTH = '<b>Документы, подтверждающие произведенные расходы (№, дата и
                наименование документа)</b><br><br>
                <i>Договор, акт выполненных работ, отчет, счет</i>';

    const FIFTH = '<b>Документы, подтверждающие произведенные расходы (№, дата и
                наименование документа)</b><br><br>
                <i>Договор, акт выполненных работ, отчет, счет, проездной билет, ж/д балет, посадочный талон, ваучер на проживание</i>';



    const DOCUMENT_FIRST = '<b>Документ(-ы), подтверждающий (-ие) получение оплаченных
                            товаров/услуг (№, дата и наименование документа)</b><br><br>
                <i>Реестр, п/поручение, платежная ведомость</i>';

    const DOCUMENT_SECOND = '<b>Документ(-ы), подтверждающий (-ие) получение оплаченных
                            товаров/услуг (№, дата и наименование документа)</b><br><br>
                <i>П/поручение, кассовый чек, товарный чек, БСО</i>';


    public function load(ObjectManager $manager)
    {
        $salaryExpenseType = $this->createExpenseType(
            $manager,
            "Оплата труда",
            ExpenseTypeInterface::SALARY_CODE
        );
        $this->createExpenseType(
            $manager,
            "Оплата труда штатных сотрудников, включая НДФЛ и страховые взносы",
            ExpenseTypeInterface::STAFF_SALARY_CODE,
            [
                Numbers::ONE => self::FIRST
            ],
            [
                Numbers::ONE => self::DOCUMENT_FIRST
            ]
        )->setParent($salaryExpenseType);
        $this->createExpenseType(
            $manager,
            "Оплата труда привлеченных специалистов, включая НДФЛ и страховые взносы",
            ExpenseTypeInterface::SPECIALIST_SALARY_CODE,
            [
                Numbers::ONE => self::FIRST
            ],
            [
                Numbers::ONE => self::DOCUMENT_FIRST
            ]
        )->setParent($salaryExpenseType);
        $this->createExpenseType(
            $manager,
            "Оплата труда административно-управленческого персонала, включая НДФЛ и страховые взносы",
            ExpenseTypeInterface::MANAGER_SALARY_CODE,
            [
                Numbers::ONE => self::FIRST
            ],
            [
                Numbers::ONE => self::DOCUMENT_FIRST
            ]
        )->setParent($salaryExpenseType);
        $this->createExpenseType(
            $manager,
            "Материальные затраты (оборудование, расходные материалы, канцелярия, продукты и т.д.)",
            ExpenseTypeInterface::MATERIAL_COSTS_CODE,
            [
                Numbers::ONE => self::SECOND
            ],
            [
                Numbers::ONE => self::DOCUMENT_SECOND
            ]
        );
        $this->createExpenseType(
            $manager,
            "Услуги, работы",
            ExpenseTypeInterface::SERVICE_CODE,
            [
                Numbers::ONE => self::THIRD
            ],
            [
                Numbers::ONE => self::DOCUMENT_SECOND
            ]
        );
        $this->createExpenseType(
            $manager,
            "Аренда помещений",
            ExpenseTypeInterface::RENT_CODE,
            [
                Numbers::ONE => self::FOURTH
            ],
            [
                Numbers::ONE => self::DOCUMENT_SECOND
            ]
        );
        $this->createExpenseType(
            $manager,
            "Транспортные и прочие расходы на поездку",
            ExpenseTypeInterface::TRANSPORT_CODE,
            [
                Numbers::ONE => self::FIFTH
            ],
            [
                Numbers::ONE => self::DOCUMENT_SECOND
            ]
        );
        $this->createExpenseType(
            $manager,
            "Связь, почтовые и курьерские услуги",
            ExpenseTypeInterface::COMMUNICATION_CODE,
            [
                Numbers::ONE => self::FOURTH
            ],
            [
                Numbers::ONE => self::DOCUMENT_SECOND
            ]
        );
        $this->createExpenseType(
            $manager,
            "Иные расходы",
            ExpenseTypeInterface::OTHER_CODE,
            [
                Numbers::ONE => self::FOURTH,
                Numbers::TWO => self::FOURTH,
            ],
            [
                Numbers::ONE => self::DOCUMENT_SECOND,
                Numbers::TWO => self::DOCUMENT_SECOND,
            ]
        );

        $this->createExpenseType(
            $manager,
            "Оплата привлеченных организаций/ специалистов (включая налоги и страховые взносы) и сопутствующие расходы ",
            ExpenseTypeInterface::INVOLVED_CODE,
            [
                Numbers::TWO => self::FIRST
            ],
            [
                Numbers::TWO => self::DOCUMENT_FIRST
            ]
        );
        $this->createExpenseType(
            $manager,
            "Расходы на последующие мероприятия по внедрению полученных знаний и опыта",
            ExpenseTypeInterface::EVENT_CODE,
            [
                //TODO ??
                Numbers::TWO => self::FIFTH
            ],
            [
                //TODO ??
                Numbers::TWO => self::DOCUMENT_SECOND
            ]
        );
        $this->createExpenseType(
            $manager,
            "Транспортные и прочие расходы на поездку к месту стажировки/обучающего мероприятия",
            ExpenseTypeInterface::TRANSPORT_CODE_SECOND,
            [
                Numbers::TWO => self::FIFTH
            ],
            [
                Numbers::TWO => self::DOCUMENT_SECOND
            ]
        );

        $manager->flush();
    }

    private function createExpenseType(ObjectManager $manager, $title, $code, $helpProofProductExpenseDocument = [], $helpProofPaidGoodsDocument = [])
    {
        $expenseType = $manager->getRepository('NKOOrderBundle:ExpenseType')->findOneBy(['code' => $code]);
        if (!$expenseType) {
            $expenseType = new ExpenseType();
            $expenseType->setCode($code);
            $manager->persist($expenseType);
        }
        $expenseType->setTitle($title);
        $expenseType->setHelpProofProductExpenseDocument($helpProofProductExpenseDocument);
        $expenseType->setHelpProofPaidGoodsDocument($helpProofPaidGoodsDocument);

        return $expenseType;
    }
}
