<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/9/17
 * Time: 3:56 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\FinalDecision;

class LoadMarksFinalDecisionsData implements FixtureInterface
{
    public function createFinalDecision(ObjectManager $manager, $name, $type = null)
    {
        if(!$manager->getRepository('NKOOrderBundle:FinalDecision')->findOneBy(array('name' => $name))) {
            $finalDecision = new FinalDecision();
            $finalDecision->setName($name);
            $finalDecision->setTypeDecision($type);
            $manager->persist($finalDecision);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createFinalDecision($manager, "Допустить к участию во II этапе Конкурса", false);
        $this->createFinalDecision($manager, "Отклонить заявку", true);
        $this->createFinalDecision($manager, "Поддержать");
        $this->createFinalDecision($manager, "Поддержать с доработкой");
        $this->createFinalDecision($manager, "Не поддерживать");

        $manager->flush();
    }

}