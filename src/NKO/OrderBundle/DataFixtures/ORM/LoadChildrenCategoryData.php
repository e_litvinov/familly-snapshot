<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.08.16
 * Time: 2:10
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\ChildrenCategory;

class LoadChildrenCategoryData implements FixtureInterface
{
    public function createCategory(ObjectManager $manager, $categoryName, $type)
    {
        $category = $manager
            ->getRepository('NKOOrderBundle:ChildrenCategory')
            ->findOneBy(array('categoryName' => $categoryName));

        if ($category) {
            $category->setType($type);
        }
        else {
            $category = new ChildrenCategory();
            $category->setCategoryName($categoryName);
            $category->setType($type);
        }
        $manager->persist($category);
    }

    public function load(ObjectManager $manager) {
        $this->createCategory($manager, "дети-подростки (с 10 лет)", 'KNS');
        $this->createCategory($manager, "подростки", 'farvater');
        $this->createCategory($manager, "сиблинги (братья, сестры)", 'application');
        $this->createCategory($manager, "дети с ограниченными возможностями здоровья", 'KNS');
        $this->createCategory($manager, "дети с ограниченными возможностями здоровья (уточните)", 'farvater');
        $this->createCategory($manager, "дети со специальными потребностями в обучении и др. (уточните)", 'farvaterAndKNS2016');
        $this->createCategory($manager, "иные (укажите)", 'farvater');
        $manager->flush();
    }
}
