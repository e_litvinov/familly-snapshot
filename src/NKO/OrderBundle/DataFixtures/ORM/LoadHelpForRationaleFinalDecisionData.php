<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 5/6/17
 * Time: 12:54 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Competition;

class LoadHelpForRationaleFinalDecisionData implements FixtureInterface
{
    /**
     * @param Competition $competition
     * @param $help
     */
    private function createHelp($manager, $competition, $help)
    {
        if($competition && !$competition->getHelpForRationaleFinalDecision()) {
            $competition->setHelpForRationaleFinalDecision($help);
        }
    }

    public function load(ObjectManager $manager)
    {
        $competition = $manager->getRepository("NKOOrderBundle:Competition")->find(8);

        $this->createHelp($manager, $competition, 'Обоснуйте, пожалуйста, свое решение: насколько данная организация является перспективной для реализации и распространения опыта, готова ли она стать стажировочной площадкой. Если Вы выбираете итоговое решение “Поддержать с условием внесения изменений”, в обосновании итогового решения необходимо написать, какая именно доработка требуется');
        $manager->flush();
    }

}