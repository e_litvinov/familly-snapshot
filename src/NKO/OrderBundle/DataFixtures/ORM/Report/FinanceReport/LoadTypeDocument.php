<?php

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018;
use NKO\OrderBundle\Utils\ReportTypes;

class LoadTypeDocument implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createTypeDocument($manager, [
            'title' => 'Договор',
            'code' => 'contract',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Акт выполненных работ',
            'code' => 'workPerformed',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Техническое задание',
            'code' => 'technicalTask',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Товарные накладные (ТОРГ-12)',
            'code' => 'commodityInvoices',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Счет на оплату',
            'code' => 'invoicePayment',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Товарный чек',
            'code' => 'salesReceipt',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Кассовый чек',
            'code' => 'cashReceipt',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'УПД',
            'code' => 'upd',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Счет-фактура',
            'code' => 'invoice',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Платежное поручение',
            'code' => 'paymentOrder',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Реестр выплаты заработной платы',
            'code' => 'registryPayments',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Банковская выписка',
            'code' => 'bankStatement',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Трудовой договор',
            'code' => 'employmentContract',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Приказ',
            'code' => 'order',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Штатное расписание',
            'code' => 'staffSchedule',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Расчетно-платежная ведомость',
            'code' => 'settlementPayroll',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Справка-расчет о начисленных взносах',
            'code' => 'helpCalculation',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Дополнительное соглашение',
            'code' => 'supplementaryAgreement',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Иное',
            'code' => 'other',
            'typeReport' => [ReportTypes::FINANCE_REPORT_2018, ReportTypes::MIXED_KNS_REPORT]
        ]);

        $this->createTypeDocument($manager, [
            'title' => 'Посадочный талон',
            'code' => 'ticket',
            'typeReport' => [ReportTypes::MIXED_KNS_REPORT]
        ]);

        $manager->flush();
    }

    private function createTypeDocument(ObjectManager $manager, $values)
    {
        $typeDocument = $manager->getRepository(Report2018\TypeDocument::class)->findOneBy(['code' => $values['code']]);
        if (!$typeDocument) {
            $typeDocument = new Report2018\TypeDocument();
        }
        $typeDocument->setCode($values['code']);
        $typeDocument->setTitle($values['title']);
        $typeDocument->setTypes($values['typeReport']);

        $manager->persist($typeDocument);
    }
}
