<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 21.8.18
 * Time: 15.20
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Report\MixedReport;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\MixedReport\ResultType;
use NKO\OrderBundle\Utils\ReportTypes;

class LoadResults implements FixtureInterface
{
    const RESULTS = [
        'AlreadyLearned' => 'Число обученных сотрудников организации / членов общественного объединения',
        'WillBeLearned' => 'Число сотрудников, организации / членов общественного объединения, которые получат новые знания благодаря реализации проекта',
        'organisationPractice' => 'Число внедренных практик (технологий, услуг, моделей и пр.) в деятельность организации благодаря реализации проекта',
    ];

    private $em;

    public function load(ObjectManager $manager)
    {
        $this->em = $manager;

        $this->createResultTypes('AlreadyLearned', self::RESULTS['AlreadyLearned'], ReportTypes::MIXED_KNS_REPORT, [
            ReportTypes::MIXED_KNS_REPORT,
        ]);
        $this->createResultTypes('WillBeLearned', self::RESULTS['WillBeLearned'], ReportTypes::MIXED_KNS_REPORT, [
            ReportTypes::MIXED_KNS_REPORT,
        ]);
        $this->createResultTypes('organisationPractice', self::RESULTS['organisationPractice'], ReportTypes::MIXED_KNS_REPORT, [
            ReportTypes::MIXED_KNS_REPORT,
        ]);

        $manager->flush();
    }

    private function createResultTypes($alias, $name, $typeToFind, $types)
    {
        $resultType = $this->em->getRepository(ResultType::class)
            ->createQueryBuilder('r')
            ->where('r.type like :type')
            ->andWhere('r.alias = :alias')
            ->setParameters([
                'type' => '%' . $typeToFind . '%',
                'alias' => $alias
            ])
            ->getQuery()
            ->getOneOrNullResult();

        if (!$resultType) {
            $resultType = new ResultType();
            $this->em->persist($resultType);
        }

        $resultType->setName($name);
        $resultType->setType($types);
        $resultType->setAlias($alias);

        return $resultType;
    }
}
