<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/8/17
 * Time: 5:29 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Event;

class LoadEventData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createEvent($manager, 'Освоение специалистами компетенций по мониторингу и оценке', 'analytic_report');
        $this->createEvent($manager, 'Разработка внутренних регламентов по мониторингу и оценке', 'analytic_report');
        $this->createEvent($manager, 'Создание системы мониторинга', 'analytic_report');
        $this->createEvent($manager, 'Совершенствование системы мониторинга', 'analytic_report');
        $this->createEvent($manager, 'Реализация мониторинга', 'analytic_report');
        $this->createEvent($manager, 'Использование результатов мониторинга (решения и действия по улучшению работы)', 'analytic_report');
        $this->createEvent($manager, 'Подготовка к проведению оценки эффективности практики', 'analytic_report');
        $this->createEvent($manager, 'Проведение оценки эффективности практики', 'analytic_report');
        $this->createEvent($manager, 'Разработка или совершенствование инструментария для мониторинга и оценки', 'analytic_report');
        $this->createEvent($manager, 'Разработка или совершенствование инструментария для сбора обратной связи от благополучателей', 'analytic_report');
        $this->createEvent($manager, 'Разработка типовых методик оценки вашей практики', 'analytic_report');
        $this->createEvent($manager, 'Трансляция своих разработок в сфере мониторинга и оценки', 'analytic_report');
        $this->createEvent($manager, 'Иное', 'analytic_report');

        $manager->flush();
    }

    private function createEvent(ObjectManager $manager, $name, $code = null)
    {
        $temp = $manager->getRepository('NKOOrderBundle:Report\AnalyticReport\Event')
            ->findBy(['name' => $name, 'code' => $code]);
        if(!$temp) {

            $event = new Event();
            $event->setName($name);
            $event->setCode($code);

            $manager->persist($event);

            return $event;
        }

        return $temp;
    }
}