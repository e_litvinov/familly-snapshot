<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/9/17
 * Time: 3:32 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\Utils\Application\KNS2018ApplicationUtils;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ConstValues;
use NKO\OrderBundle\Utils\ReportTypes;

class LoadMeasurementMethodData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createMeasurementMethod($manager, 'тест', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016  => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 0,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
        ], 1);
        $this->createMeasurementMethod($manager, 'анкетирование', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 1,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
        ], 2);
        $this->createMeasurementMethod($manager, 'интервью', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 3,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
        ], 3);
        $this->createMeasurementMethod($manager, 'наблюдение', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 4,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
        ], 4);
        $this->createMeasurementMethod($manager, 'экспертная оценка', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 5,
        ], 5);
        $this->createMeasurementMethod(
            $manager,
            'анализ документов (форма регистрации данных, список благополучателей, документы от государственных партнеров)',
            ApplicationTypes::BRIEF_APPLICATION_2016,
            [
                ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ],
            6
        );
        $this->createMeasurementMethod($manager, 'свой метод', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
        ], 7);
        $this->createMeasurementMethod($manager, 'нет', ApplicationTypes::BRIEF_APPLICATION_2016, [
            ApplicationTypes::BRIEF_APPLICATION_2016 => ConstValues::AUTO,
        ], 8);

        $this->createMeasurementMethod($manager, 'нет (нулевой показатель)', ApplicationTypes::CONTINUATION_APPLICATION_2018, [
            ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
        ], 9);

        $this->createMeasurementMethod($manager, 'тест', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 10);
        $this->createMeasurementMethod($manager, 'анкетирование', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 11);
        $this->createMeasurementMethod($manager, 'интервью', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 12);
        $this->createMeasurementMethod($manager, 'наблюдение', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 13);
        $this->createMeasurementMethod($manager, 'экспертная оценка', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,

            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 14);
        $this->createMeasurementMethod($manager, 'форма регистрации данных (журнал, список)', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO
        ], 15);
        $this->createMeasurementMethod($manager, 'другое', ReportTypes::ANALYTIC_REPORT_KNS_1, [
            ReportTypes::ANALYTIC_REPORT_KNS_1 => ConstValues::AUTO
        ], 16);

        $this->createMeasurementMethod($manager, 'форма учета данных (лист регистрации, журнал учета и пр.)', ApplicationTypes::KNS_APPLICATION_2018, [
            ApplicationTypes::KNS_APPLICATION_2018,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 17);
        $this->createMeasurementMethod($manager, 'сертификат', ApplicationTypes::KNS_APPLICATION_2018, [
            ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,
        ], 18);
        $this->createMeasurementMethod($manager, 'иное', ApplicationTypes::KNS_APPLICATION_2018, [
            ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2018 . KNS2018ApplicationUtils::BENEFICIARY_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::PROJECT_RESULTS_FIELD_NAME => ConstValues::AUTO,
            ApplicationTypes::KNS_APPLICATION_2020 . KNS2018ApplicationUtils::EMPLOYEE_RESULTS_FIELD_NAME => ConstValues::AUTO,
        ], 19);
        $this->createMeasurementMethod($manager, 'список благополучателей', ApplicationTypes::KNS_APPLICATION_2_2017, [
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 7,
        ], 20);
        $this->createMeasurementMethod($manager, 'свой метод', ApplicationTypes::KNS_APPLICATION_2_2017, [
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 8,
            ReportTypes::MIXED_KNS_REPORT_2019 => ConstValues::AUTO,
            ReportTypes::MIXED_KNS_REPORT_2020 => ConstValues::AUTO,

        ], 21);
        $this->createMeasurementMethod($manager, 'показатель не относится к проекту', ApplicationTypes::KNS_APPLICATION_2_2017, [
            ApplicationTypes::KNS_APPLICATION_2_2017 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 9,
        ], 22);

        $this->createMeasurementMethod($manager, 'форма регистрационных данных (журнал, чек-лист)', ReportTypes::ANALYTIC_REPORT_2018, [
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_KNS => 6,
        ], 23);

        $this->createMeasurementMethod($manager, 'документы от государственных партнеров', ReportTypes::ANALYTIC_REPORT_2018, [
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ReportTypes::ANALYTIC_REPORT_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
        ], 24);

        $this->createMeasurementMethod($manager, ' свой метод', ReportTypes::ANALYTIC_REPORT_2018, [
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
        ], 25);

        $this->createMeasurementMethod($manager, 'свой способ', ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019, [
            ReportTypes::ANALYTIC_REPORT_2018 => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_APPLICATION_2018_MEATHURE => ConstValues::AUTO,
            ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2019 => ConstValues::AUTO,
        ], 26);

        //continue sequence "staticId", last value 25!!!

        $manager->flush();
    }

    private function createMeasurementMethod(ObjectManager $manager, $name, $typeToFind, $types, $staticId)
    {
        $measurementMethod = $manager->getRepository(MeasurementMethod::class)
            ->createQueryBuilder('m')
            ->where('m.type like :type')
            ->andWhere('m.name = :name')
            ->setParameters([
                'type' => '%' . $typeToFind . '%',
                'name' => $name
            ])
            ->getQuery()
            ->getOneOrNullResult();

        if (!$measurementMethod) {
            $measurementMethod = new MeasurementMethod();
            $manager->persist($measurementMethod);
        }

        $measurementMethod->setName($name);
        $measurementMethod->setType($types);
        $measurementMethod->setStaticId($staticId);

        return $measurementMethod;
    }
}
