<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Report\AnalyticReport;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\NormativeAct;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ReportTypes;

class LoadNormativeAct implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createNormativeAct($manager, 'Постановление Правительства РФ от 24.05.2014 N 481', ReportTypes::HARBOR_REPORT_2019);
        $this->createNormativeAct($manager, 'Федеральный закон от 28.12.2013 N 442-ФЗ', ReportTypes::HARBOR_REPORT_2019);
        $this->createNormativeAct($manager, 'Федеральный закон от 05.04.2013 № 44-ФЗ', ReportTypes::HARBOR_REPORT_2019);
        $this->createNormativeAct($manager, 'Указанные нормативные акты не применяем', ReportTypes::HARBOR_REPORT_2019);

        $this->createNormativeAct($manager, 'Постановление Правительства РФ от 24.05.2014 N 481', ApplicationTypes::KNS_APPLICATION_3_2018);
        $this->createNormativeAct($manager, 'Федеральный закон от 28.12.2013 N 442-ФЗ', ApplicationTypes::KNS_APPLICATION_3_2018);
        $this->createNormativeAct($manager, 'Федеральный закон от 05.04.2013 № 44-ФЗ', ApplicationTypes::KNS_APPLICATION_3_2018);
        $this->createNormativeAct($manager, 'Указанные нормативные акты не применяем', ApplicationTypes::KNS_APPLICATION_3_2018);

        $manager->flush();
    }

    private function createNormativeAct(ObjectManager $manager, $title, $code)
    {
        $temp = $manager->getRepository(NormativeAct::class)
            ->findBy(['title' => $title, 'code' => $code]);
        if (!$temp) {
            $event = new NormativeAct();
            $event->setTitle($title);
            $event->setCode($code);

            $manager->persist($event);

            return $event;
        }

        return $temp;
    }
}
