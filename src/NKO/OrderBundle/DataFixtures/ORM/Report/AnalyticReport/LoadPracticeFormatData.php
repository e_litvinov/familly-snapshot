<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/13/17
 * Time: 3:54 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PracticeFormat;

class LoadPracticeFormatData implements FixtureInterface
{
    const SF_2016 = 'brief_sf-2016';

    public function load(ObjectManager $manager)
    {
        $this->createPracticeFormat($manager,
            'организация стажировок на базе организации / выезды специалистов',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'групповые тренинги',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'индивидуальные консультации (очно/онлайн)',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'электронные рассылки материалов, ссылок на них',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'участие в публичных мероприятиях (презентации, выступления и пр.)',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'организация и проведение публичных мероприятий (конференций, круглых столов и пр.)',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'вебинары',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'супервизии/коучинг/наставничество',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'мониторинг внедрения практики другими организациями',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'распространение изданных методических материалов',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'размещение информации онлайн (сайты и пр.)',
            self::SF_2016);
        $this->createPracticeFormat($manager,
            'иное',
            self::SF_2016);

        $manager->flush();

    }

    private function createPracticeFormat(ObjectManager $manager, $name, $type = null)
    {
        $temp = $manager->getRepository('NKOOrderBundle:Report\AnalyticReport\PracticeFormat')
            ->findBy(['name' => $name, 'type' => $type]);
        if(!$temp) {
            $practiceFormat = new PracticeFormat();
            $practiceFormat->setName($name);
            $practiceFormat->setType($type);

            $manager->persist($practiceFormat);

            return $practiceFormat;
        }

        return $temp;
    }

}