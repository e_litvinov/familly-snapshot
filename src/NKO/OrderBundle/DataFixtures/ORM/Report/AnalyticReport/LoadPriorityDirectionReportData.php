<?php

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 10/4/17
 * Time: 3:33 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\PriorityDirection;
use NKO\OrderBundle\Utils\PriorityDirectionTypes;


class LoadPriorityDirectionReportData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createPriorityDirection($manager,
            'профилактика социального сиротства через работу с кровными (биологическими) семьями/ работа по возврату детей из детских учреждений в кровные семьи',
            PriorityDirectionTypes::BRIEF_SF_2016);
        $this->createPriorityDirection($manager,
            'подготовка детей, воспитываемых в организациях для детей-сирот и детей, оставшихся без попечения родителей, к семейному устройству',
            PriorityDirectionTypes::BRIEF_SF_2016);
        $this->createPriorityDirection($manager,
            'подготовка и сопровождение замещающих семей, профилактика вторичного сиротства (профилактика отобраний (изъятий) / отказов детей из замещающих семей)',
            PriorityDirectionTypes::BRIEF_SF_2016);
        $this->createPriorityDirection($manager,
            '«полный цикл» подготовки детей к семейному устройству и подбора для них, подготовки и сопровождения замещающих семей, работа по возврату детей в кровные семьи и сопровождение этих семей, профилактики социального сиротства',
            PriorityDirectionTypes::BRIEF_SF_2016);
        $this->createPriorityDirection($manager,
            'реформирование организаций для детей-сирот и детей, оставшихся без попечения родителей (детских домов, школ-интернатов, домов ребенка, социальных приютов и пр.)',
            PriorityDirectionTypes::BRIEF_SF_2016);
        $this->createPriorityDirection($manager,
            'постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет – выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека (попечительство) в замещающей семье',
            PriorityDirectionTypes::BRIEF_SF_2016);
        $this->createPriorityDirection($manager,
            'активизация поддержки замещающих и «кризисных» кровных семей со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.); развитие взаимоподдержки, консолидации сообществ членов замещающих семей (родителей, «выпускников» замещающих семей)',
            PriorityDirectionTypes::BRIEF_SF_2016);

        $manager->flush();

    }

    private function createPriorityDirection(ObjectManager $manager, $name, $type = null)
    {
        $temp = $manager->getRepository('NKOOrderBundle:Report\AnalyticReport\PriorityDirection')
            ->findBy(['name' => $name, 'type' => $type]);
        if(!$temp) {
            $priorityDirection = new PriorityDirection();
            $priorityDirection->setName($name);
            $priorityDirection->setType($type);

            $manager->persist($priorityDirection);

            return $priorityDirection;
        }

        return $temp;
    }
}