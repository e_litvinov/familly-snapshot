<?php

namespace NKO\OrderBundle\DataFixtures\ORM\Report\AnalyticReport;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Harbor\TrainingGround;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ReportTypes;

class LoadTrainingGround implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createTrainingGround(
            $manager,
            'Поддержка и сопровождение низкоресурсных кровных семей на разных этапах семейного неблагополучия, с риском отказа / отобрания ребенка из кровной семьи;',
            ReportTypes::HARBOR_REPORT_2019
        );
        $this->createTrainingGround(
            $manager,
            'Поддержка и сопровождение кровных семей, где один или два родителя имеют проблемы со здоровьем (ОВЗ, инвалидность, ментальные проблемы);',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Поддержка и сопровождение семей, где один или два родителя имеют алкогольную, наркотическую или иную форму зависимостей; ',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Работа по повышению родительских компетенций, ненасильственного воспитания детей в кровных семьях;',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Работа с детьми и членами кровных семей, подвергшихся семейному насилию (в т.ч. проживающие в кризисных центрах временного проживания);',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Поддержка и сопровождение кровных семей с детьми, имеющими проблемы со здоровьем (ОВЗ, инвалидность), девиантное поведение и пр.;',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Реабилитация кровных семей и активизация ресурсов кровных родственников на сохранение или возврат ребенка в кровную семью / поддержка отношений детей с кровными родителями и родственниками; ',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Сопровождение кровных семей после возвращения детей;',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Иные эффективные практики в сфере профилактики социального сиротства, работы с кровной семьёй.',
            ReportTypes::HARBOR_REPORT_2019
        );

        $this->createTrainingGround(
            $manager,
            'Профилактика сиротства в кровных семьях с детьми, где родитель(и) имеют алкогольную, наркотическую или иную форму зависимостей.',
            ApplicationTypes::HARBOR_2020
        );

        $this->createTrainingGround(
            $manager,
            'Профилактика сиротства в кровных семьях, где хотя бы один из детей и/или родителей  имеет ОВЗ / инвалидность.',
            ApplicationTypes::HARBOR_2020
        );

        $this->createTrainingGround(
            $manager,
            'Поддержка и сопровождение многодетных и неполных кровных семей на разных этапах семейного неблагополучия
                (ограничение или лишение родителей родительских прав; риск отказа / отобрания ребенка из семьи).',
            ApplicationTypes::HARBOR_2020
        );

        $this->createTrainingGround(
            $manager,
            'Поддержка и сопровождение кровных семей, где ребенок временно помещается в организацию для детей-сирот и 
                детей, оставшихся без попечения родителей (в т.ч. «по заявлению») или специализированные учреждения для несовершеннолетних',
            ApplicationTypes::HARBOR_2020
        );

        $manager->flush();
    }

    private function createTrainingGround(ObjectManager $manager, $title, $code)
    {
        $event = $manager->getRepository(TrainingGround::class)
            ->findOneBy(['title' => $title, 'code' => $code]);
        if (!$event) {
            $event = new TrainingGround();
            $event->setCode($code);

            $manager->persist($event);
        }

        $event->setTitle($title);

        return $event;
    }
}
