<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/13/17
 * Time: 2:36 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee;

class LoadGranteeData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createGrantee($manager, 'Семейный Фарватер', 'brief_sf-2016');
        $this->createGrantee($manager, 'Курс на семью', 'brief_sf-2016');
        $this->createGrantee($manager, 'Грантополучатели нескольких конкурсов', 'brief_sf-2016');
        $this->createGrantee($manager, 'Не участвовали', 'brief_sf-2016');

        $this->createGrantee($manager, 'Семейный Фарватер', 'analytic_report_2019');
        $this->createGrantee($manager, 'Курс на семью', 'analytic_report_2019');
        $this->createGrantee($manager, 'Семейная гавань', 'analytic_report_2019');
        $this->createGrantee($manager, 'Грантополучатели нескольких конкурсов', 'analytic_report_2019');
        $this->createGrantee($manager, 'Не участвовали', 'analytic_report_2019');

        $this->createGrantee($manager, 'Не участвовали', 'analytic_report_2019_2');
        $this->createGrantee($manager, 'Семейный Фарватер', 'analytic_report_2019_2');
        $this->createGrantee($manager, 'Курс на семью', 'analytic_report_2019_2');
        $this->createGrantee($manager, 'Семейная гавань', 'analytic_report_2019_2');
        $this->createGrantee($manager, 'Грантополучатели нескольких конкурсов', 'analytic_report_2019_2');

        $manager->flush();
    }

    private function createGrantee(ObjectManager $manager, $name, $type = null)
    {
        $temp = $manager->getRepository('NKOOrderBundle:Report\AnalyticReport\Grantee')
            ->findBy(['name' => $name, 'type' => $type]);
        if (!$temp) {
            $grantee = new Grantee();
            $grantee->setName($name);
            $grantee->setType($type);

            $manager->persist($grantee);

            return $grantee;
        }

        return $temp;
    }
}
