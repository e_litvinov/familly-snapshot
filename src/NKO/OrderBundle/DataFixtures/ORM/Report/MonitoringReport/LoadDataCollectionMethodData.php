<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:45 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Method;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MethodInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class LoadDataCollectionMethodData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createMethod($manager, [
            'title' => 'Teст',
            'code' => MethodInterface::TEST_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Анкетирование',
            'code' => MethodInterface::QUESTIONNAIRE_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Интервью',
            'code' => MethodInterface::INTERVIEW_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Наблюдение',
            'code' => MethodInterface::OBSERVATION_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Экспертная оценка',
            'code' => MethodInterface::EXPERT_REVIEW_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Форма регистрационных данных (журнал, чек-лист)',
            'code' => MethodInterface::FORM_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Список благополучателей',
            'code' => MethodInterface::LIST_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Документы от государственных партнеров',
            'code' => MethodInterface::DOCS_CODE
        ]);

        $this->createMethod($manager, [
            'title' => 'Свой метод',
            'code' => MethodInterface::CUSTOM_CODE
        ]);

        $manager->flush();
    }

    private function createMethod(ObjectManager $manager, $values)
    {
        $method = $manager->getRepository('NKOOrderBundle:Report\MonitoringReport\Method')->findOneBy(['code' => $values['code']]);
        if(!$method) {
            $method = new Method();
            $manager->persist($method);
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($values as $key => $value) {
            $accessor->setValue($method, $key, $value);
        }

        return $method;
    }
}