<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:45 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Utils\Analytic\SummaryMonitoring;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoadIndicatorData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // I.
        $root = $this->createIndicator($manager, [
            'title' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
            'code' => IndicatorInterface::PRACTICE_IMPL_CODE,
            'parent' => null,
        ]);

        // 1.
        $parentIndex = 1;
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей-сирот и детей, ' .
                        'оставшихся без попечения родителей, ' .
                        'переданных на семейные формы устройства',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE,
            'analyticTypes' => [
                SummaryMonitoring::SECOND_TYPE
            ]
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. сиблингов',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 2.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей, возвращённых в кровные семьи',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE,
            'analyticTypes' => [
                SummaryMonitoring::SECOND_TYPE
            ]
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 3.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество предотвращённых случаев отобрания (изъятий), ' .
                        'отказов детей из кровных семей',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE,
            'analyticTypes' => [
                SummaryMonitoring::SECOND_TYPE
            ]
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 4.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество предотвращённых случаев отобрания (изъятий), ' .
                        'отказов от детей из замещающих семей',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE,
            'analyticTypes' => [
                SummaryMonitoring::SECOND_TYPE
            ]
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 5.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей, улучшивших своё благополучие',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE,
            'analyticTypes' => [
                SummaryMonitoring::SECOND_TYPE
            ]
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших психическое состояние',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших физическое состояние',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. повысивших уровень развития, навыков',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших детско-родительские отношения',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших показатели успеваемости',
            'parent' => $parent,
            'code' => IndicatorInterface::PRE_CUSTOM_VALUE,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 6.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество кровных кризисных семей, ' .
                        'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::FAMILY_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей',
            'parent' => $parent,
            'code' => IndicatorInterface::CHILDREN_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. родителей',
            'parent' => $parent,
            'code' => IndicatorInterface::PARENT_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. кровных кризисных семей, ' .
                        'в отношении которых повышен уровень поддержки со стороны окружения ' .
                        '(родственники, друзья, школы, детские сады, соседи и пр.)',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        // 7.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество замещающих семей, ' .
                        'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::FAMILY_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей',
            'parent' => $parent,
            'code' => IndicatorInterface::CHILDREN_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. родителей',
            'parent' => $parent,
            'code' => IndicatorInterface::PARENT_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. замещающих семей, ' .
                        'в отношении которых повышен уровень поддержки со стороны окружения ' .
                        '(родственники, друзья, школы, детские сады, соседи и пр.)',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        // 8.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей в ДУ, ' .
                        'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::CHILDREN_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей, прошедших подготовку к самостоятельной жизни ' .
                        'или к семейному устройству',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        // 9.
        $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество выпускников ДУ и замещающих семей, ' .
                        'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::GRADUATES_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        // 10.
        $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество родителей, прошедших подготовку ' .
                        '(в замещающие родители)',
            'parent' => $root,
            'code' => IndicatorInterface::PARENT_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        // 11.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество семей, прошедших подготовку (в замещающие семьи)',
            'parent' => $root,
            'code' => IndicatorInterface::FAMILY_CODE,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'из них: количество семей, принявших детей на воспитание',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч.детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. сиблингов',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        // 12.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество отобраний (изъятий), отказов от детей  из семей, ' .
                        'сопровождаемых в рамках реализуемого проекта',
            'parent' => $root,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. из замещающих семей',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. из кровных семей',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
            'type' => IndicatorInterface::PROD_TYPE
        ]);


        // II.
        $root = $this->createIndicator($manager, [
            'title' => 'РАСПРОСТРАНЕНИЕ ПРАКТИКИ',
            'code' => IndicatorInterface::PRACTICE_DISS_CODE,
            'parent' => null
        ]);

        // 13.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество мероприятий по распространению практики среди специалистов',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. мероприятия, организованные в рамках проекта',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. мероприятия других организаций',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 14.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество специалистов, принявших участие в мероприятиях проекта',
            'parent' => $root,
            'code' => IndicatorInterface::EXPERT_CODE,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. служб сопровождения и центров семейного устройства',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. отделов / органов опеки и попечительства',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. негосударственных некоммерческих организаций (НКО), ' .
                        'деятельность которых направлена на содействие в области ' .
                        'профилактики социального сиротства, семейного устройства',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детских домов, школ-интернатов ' .
                        'и иных учреждений, в которых воспитываются дети',
            'parent' => $parent
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. школ, детских садов, ' .
                        'техникумов и иных общеобразовательных учреждений',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. ассоциаций, сообществ приёмных (замещающих) ' .
                        'родителей и пр.',
            'parent' => $parent,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // 15.
        $parent = $this->createIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество организаций, специалисты которых приняли ' .
                        'участие в мероприятиях по распространению практики, ' .
                        'познакомились/обучились вашей практике',
            'parent' => $root,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        $childIndex = 1;
        $this->createIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. количество организаций, внедривших вашу практику',
            'parent' => $parent,
            'code' => IndicatorInterface::ORGANIZATION_CODE,
            'type' => IndicatorInterface::SOCIAL_TYPE
        ]);

        // Итоги.
        $root = $this->createIndicator($manager, [
            'title' => 'Итоги:',
            'code' => IndicatorInterface::TOTAL_CODE,
            'parent' => null
        ]);

        $this->createIndicator($manager, [
            'title' => 'Всего семей, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::FAMILY_CODE
        ]);

        $this->createIndicator($manager, [
            'title' => 'Всего детей, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::CHILDREN_CODE
        ]);

        $this->createIndicator($manager, [
            'title' => 'Всего выпускников, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::GRADUATES_CODE
        ]);

        $this->createIndicator($manager, [
            'title' => 'Всего родителей, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::PARENT_CODE
        ]);

        $this->createIndicator($manager, [
            'title' => 'Всего специалистов, принявших участие в мероприятиях проектов',
            'parent' => $root,
            'code' => IndicatorInterface::EXPERT_CODE
        ]);

        $this->createIndicator($manager, [
            'title' => 'Всего организаций, внедривших практику',
            'parent' => $root,
            'code' => IndicatorInterface::ORGANIZATION_CODE
        ]);

        $this->createIndicator($manager, [
            'title' => 'ИНДИВИДУАЛЬНЫЕ РЕЗУЛЬТАТЫ ПО ПРОЕКТАМ',
            'code' => IndicatorInterface::INDIVIDUAL_RES_CODE,
            'parent' => null
        ]);

        $this->createIndicatorForMonitoringReport2020($manager);


        $manager->flush();
    }

    private function createIndicatorForMonitoringReport2020(ObjectManager $manager)
    {
        $root = $this->createNewMonitoingIndicator($manager, [
            'title' => 'РЕАЛИЗАЦИЯ ПРАКТИКИ',
            'code' => IndicatorInterface::PRACTICE_IMPL_CODE,
            'parent' => null,
        ]);

        // 1.
        $parentIndex = 1;
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей, возвращённых в кровные семьи',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
        ]);
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
        ]);
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => ' в т.ч. детей, временно помещенных в ДУ',
            'parent' => $parent,
        ]);

        //2
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество предотвращённых случаев отобрания (изъятий), ' .
                'отказов детей из кровных семей',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей, временно помещенных в ДУ',
            'parent' => $parent,
        ]);

        // 3.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей, улучшивших своё благополучие',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших психическое состояние',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших физическое состояние',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. повысивших уровень развития, навыков',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших детско-родительские отношения',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. улучшивших показатели успеваемости',
            'parent' => $parent,
            'code' => IndicatorInterface::PRE_CUSTOM_VALUE,
        ]);

        // 4.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество семей, в которых улучшены детско-родительские отношения',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. семей, временно поместивших ребенка в ДУ',
            'parent' => $parent,
        ]);
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => ' в т.ч. семей, воспитывающих ребенка (детей) с ОВЗ',
            'parent' => $parent,
        ]);


        // 5.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество родителей, которые восстановлены в родительских правах или в отношении которых отменено ограничение родительских прав',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. восстановлены в родительских правах',
            'parent' => $parent,
        ]);
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. отменено ограничение родительских прав',
            'parent' => $parent,
        ]);

        // 6.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество родителей с зависимостями, у которых наблюдается улучшение (снижение / прекращение потребления)',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. прошедших лечение от алкогольной, наркотической или иной зависимости',
            'parent' => $parent,
        ]);
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. находящихся в ремиссии более 3 месяцев',
            'parent' => $parent,
        ]);

        // 7.
        $parentIndex = 7;
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество кровных кризисных семей, ' .
                'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::FAMILY_CODE,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей',
            'parent' => $parent,
            'code' => IndicatorInterface::CHILDREN_CODE,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. родителей',
            'parent' => $parent,
            'code' => IndicatorInterface::PARENT_CODE
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. кровных кризисных семей, ' .
                'в т.ч. семей, в отношении которых  повышен уровень поддержки со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. семей, где у родителя(ей) есть зависимость',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. семей, где у родителя(ей) есть инвалидность',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. семей, воспитывающих ребенка (детей) с ОВЗ',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. многодетных семей',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. неполных семей',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. семей, временно поместивших ребенка в ДУ',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. семей, снятых с различных видов учета (СОП, КДН и пр.)',
            'parent' => $parent,
            'code' => IndicatorInterface::PRE_CUSTOM_VALUE,
        ]);

        // 8.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество детей в ДУ, ' .
                'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::CHILDREN_CODE
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей, временно помещенных в ДУ',
            'parent' => $parent,
        ]);

        // 9.
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество выпускников ДУ и замещающих семей, ' .
                'получивших поддержку в рамках реализуемого проекта',
            'parent' => $root,
            'code' => IndicatorInterface::GRADUATES_CODE,
        ]);

        // 10.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество отобраний (изъятий), отказов от детей  из семей, ' .
                'сопровождаемых в рамках реализуемого проекта',
            'parent' => $root,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. из кровных семей',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей-подростков',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детей с ОВЗ',
            'parent' => $parent,
        ]);


        // II.
        $root = $this->createNewMonitoingIndicator($manager, [
            'title' => 'РАСПРОСТРАНЕНИЕ ПРАКТИКИ',
            'code' => IndicatorInterface::PRACTICE_DISS_CODE,
            'parent' => null
        ]);

        // 11.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество мероприятий по распространению практики среди специалистов',
            'parent' => $root,
        ]);

        // 12.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество специалистов, принявших участие в мероприятиях проекта',
            'parent' => $root,
            'code' => IndicatorInterface::EXPERT_CODE,
        ]);

        $childIndex = 1;
        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. служб сопровождения и центров семейного устройства',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. отделов / органов опеки и попечительства',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. негосударственных некоммерческих организаций (НКО), ' .
                'деятельность которых направлена на содействие в области ' .
                'профилактики социального сиротства, семейного устройства',
            'parent' => $parent,
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. детских домов, школ-интернатов ' .
                'и иных учреждений, в которых воспитываются дети',
            'parent' => $parent
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $childIndex++,
            'title' => 'в т.ч. школ, детских садов, ' .
                'техникумов и иных общеобразовательных учреждений',
            'parent' => $parent,
        ]);

        // 13.
        $parent = $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'Количество организаций, специалисты которых приняли ' .
                'участие в мероприятиях по распространению практики, ' .
                'познакомились/обучились вашей практике',
            'parent' => $root,
        ]);

        // Итоги.
        $root = $this->createNewMonitoingIndicator($manager, [
            'title' => 'Итоги:',
            'code' => IndicatorInterface::TOTAL_CODE,
            'parent' => null
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'title' => 'Всего семей, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::FAMILY_CODE
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'title' => 'Всего детей, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::CHILDREN_CODE
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'title' => 'Всего выпускников, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::GRADUATES_CODE
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'title' => 'Всего родителей, получивших поддержку в рамках проекта',
            'parent' => $root,
            'code' => IndicatorInterface::PARENT_CODE
        ]);

        $this->createNewMonitoingIndicator($manager, [
            'title' => 'Всего специалистов, принявших участие в мероприятиях проектов',
            'parent' => $root,
            'code' => IndicatorInterface::EXPERT_CODE
        ]);

        // III.

        $this->createNewMonitoingIndicator($manager, [
            'indexNumber' => $parentIndex++,
            'title' => 'III ИНДИВИДУАЛЬНЫЕ РЕЗУЛЬТАТЫ ПО ПРОЕКТАМ',
            'code' => IndicatorInterface::INDIVIDUAL_RES_CODE,
            'parent' => null
        ]);
    }

    private function createNewMonitoingIndicator(ObjectManager $manager, $values)
    {
        $values['monitoringType'] = IndicatorInterface::NEWMONITORINGINDICATORTYPE;
        return $this->createIndicator($manager, $values);
    }

    private function createIndicator(ObjectManager $manager, $values, $checkMonitoringType = false)
    {
        $params = [
            'title' => $values['title'],
            'parent' => $values['parent']
        ];

        if (isset($values['monitoringType'])) {
            $params['monitoringType'] = $values['monitoringType'];
        }

        $indicator = $manager->getRepository('NKOOrderBundle:Report\MonitoringReport\Indicator')->findOneBy($params);
        if (!$indicator) {
            $indicator = new Indicator();
            $manager->persist($indicator);
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($values as $key => $value) {
            $accessor->setValue($indicator, $key, $value);
        }

        return $indicator;
    }
}
