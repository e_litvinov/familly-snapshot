<?php

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/20/17
 * Time: 3:20 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\KNS2017;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Utils\ApplicationTypes;

class LoadTrainingGroundData implements FixtureInterface
{
    public function createGround(ObjectManager $manager, $name, $competition, $staticId, $parentGround = null)
    {
        $ground = $manager->getRepository(TrainingGround::class)
            ->findOneBy([
                'staticId' => $staticId
            ]);

        if ($ground) {
            $ground->setParent($parentGround);
            $ground->setCompetition($competition);
        } else {
            $ground = new TrainingGround();
            $ground->setName($name);
            $ground->setParent($parentGround);
            $ground->setCompetition($competition);
        }
        $ground->setStaticId($staticId);

        $manager->persist($ground);

        return $ground;
    }

    public function load(ObjectManager $manager)
    {
        $ground1 = $this->createGround(
            $manager,
            'Профилактика  социального  сиротства  через  работу  с  «кровными» (биологическими) семьями',
            'kns2017',
            1
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация «Центр развития инновационных социальных услуг «Партнёрство каждому ребенку» (г. Санкт-Петербург)',
            'kns2017',
            2,
            $ground1
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация дополнительного образования и социального обслуживания «Новые перспективы» (г. Санкт-Петербург)',
            'kns2017',
            3,
            $ground1
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация социальных услуг «Родительский центр «Подсолнух» (г. Санкт-Петербург)',
            'kns2017',
            4,
            $ground1
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд «Дети наши» (г. Москва)',
            'kns2017',
            5,
            $ground1
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение Центр содействия семейному воспитанию «Вера. Надежда. Любовь», г. Москва',
            'kns2017',
            6,
            $ground1
        );
        $this->createGround(
            $manager,
            'Некоммерческая организация «Благотворительный фонд «Даунсайд Ап» (г. Москва)',
            'kns2017',
            7,
            $ground1
        );
        $this->createGround(
            $manager,
            'Региональная общественная организация «Красноярский центр лечебной педагогики» (г. Красноярск)',
            'kns2017',
            8,
            $ground1
        );
        $this->createGround(
            $manager,
            'Санкт-Петербургский Общественный Благотворительный Фонд «Родительский мост» (г. Санкт-Петербург)',
            'kns2017',
            9,
            $ground1
        );
        $this->createGround(
            $manager,
            'Свердловская общественная организация по содействию семьям с детьми в трудной жизненной ситуации «Аистенок» (г. Екатеринбург)',
            'kns2017',
            10,
            $ground1
        );
        $this->createGround(
            $manager,
            'Тамбовское областное государственное автономное общеобразовательное учреждение «Котовская школа-интернат для обучающихся с ограниченными возможностями здоровья» (г. Котовск)',
            'kns2017',
            11,
            $ground1
        );
        $this->createGround(
            $manager,
            'Частное учреждение социального обслуживания «Центр развития семейных форм устройства детей» (г. Мурманск)',
            'kns2017',
            12,
            $ground1
        );

        $ground2 = $this->createGround(
            $manager,
            'Подготовка и сопровождение замещающих семей, профилактика вторичного сиротства (профилактика отобраний (изъятий)/отказов детей из замещающих семей)',
            'kns2017',
            13
        );
        $this->createGround(
            $manager,
            'Общественная организация Шегарского района Томской области помощи детям и семьям группы риска по социальному сиротству «Рука в руке» (с. Мельниково Томской области)',
            'kns2017',
            14,
            $ground2
        );
        $this->createGround
        ($manager,
            'Межрегиональная общественная организация содействия программе воспитания подрастающего поколения «Старшие Братья Старшие Сестры» (г. Москва)',
            'kns2017',
            15,
            $ground2
        );
        $this->createGround(
            $manager,
            'Государственное казённое образовательное учреждение для детей, нуждающихся в психолого-педагогической и медико-социальной помощи, «Волгоградский областной центр психолого-медико-социального сопровождения» (г. Волгоград)',
            'kns2017',
            16,
            $ground2
        );

        $ground3 = $this->createGround(
            $manager,
            '«Полный цикл» подготовки детей к семейному устройству и подбора для них, подготовки и сопровождения замещающих семей, работа по возврату детей в кровные семьи и сопровождение этих семей, профилактики социального сиротства',
            'kns2017',
            17
        );
        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение «Центр содействия семейному устройству детей-сирот и детей, оставшихся без попечения родителей г. Уссурийска» (г. Уссурийск Приморского края)',
            'kns2017',
            18,
            $ground3
        );
        $this->createGround(
            $manager,
            'Благотворительный Фонд «Помощь детям, затронутым эпидемией ВИЧ-инфекции «Дети плюс» (г. Москва)',
            'kns2017',
            19,
            $ground3
        );

        $ground4 = $this->createGround(
            $manager,
            'Реформирование организаций для детей-сирот и детей, оставшихся без попечения родителей (детских домов, школ-интернатов, домов ребенка, социальных приютов и пр.)',
            'kns2017',
            20
        );
        $this->createGround(
            $manager,
            'Муниципальное казенное учреждение города Новосибирска «Центр помощи детям, оставшимся без попечения родителей «Созвездие» (г. Новосибирск)',
            'kns2017',
            21,
            $ground4
        );
        $this->createGround(
            $manager,
            'Региональная благотворительная общественная организация «Центр лечебной педагогики» (г. Москва)',
            'kns2017',
            22,
            $ground4
        );

        $ground5 = $this->createGround(
            $manager,
            'Постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет – выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека (попечительство) в замещающей семье',
            'kns2017',
            23
        );
        $this->createGround(
            $manager,
            'Частное учреждение дополнительного образования и реализации социальных проектов «Центр социально-психологической помощи» (г. Киров)',
            'kns2017',
            24,
            $ground5
        );

        $ground6 = $this->createGround(
            $manager,
            ' Профилактика социального сиротства через работу с кровными (биологическими) семьями / работа по возврату детей из детских учреждений в кровные семьи',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            25
        );
        $this->createGround(
            $manager,
            ' Автономная  некоммерческая организация «Центр развития инновационных социальных услуг «Партнёрство каждому ребенку» (г. Санкт-Петербург)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            26,
            $ground6);
        $this->createGround(
            $manager,
            ' Автономная  некоммерческая организация дополнительного образования и социального обслуживания «Новые перспективы» (г. Санкт-Петербург)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            27,
            $ground6
        );
        $this->createGround(
            $manager,
            'Автономная  некоммерческая организация социальных услуг «Родительский центр «Подсолнух» (г. Санкт-Петербург)  ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            28,
            $ground6
        );
        $this->createGround(
            $manager,
            'Благотворительный  фонд «Дети наши» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            29,
            $ground6
        );
        $this->createGround(
            $manager,
            'Государственное  бюджетное учреждение города Москвы Центр содействия семейному воспитанию «Вера. Надежда. Любовь» Департамента труда и социальной защиты населения города Москвы (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            30,
            $ground6
        );
        $this->createGround(
            $manager,
            'Некоммерческая  организация «Благотворительный фонд «Даунсайд Ап» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            31,
            $ground6
        );
        $this->createGround(
            $manager,
            'Региональная  общественная организация «Красноярский центр лечебной педагогики» (г. Красноярск)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            32,
            $ground6
        );
        $this->createGround(
            $manager,
            'Санкт-Петербургский  Общественный Благотворительный Фонд «Родительский мост» (г. Санкт-Петербург) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            33,
            $ground6
        );
        $this->createGround(
            $manager,
            'Свердловская  общественная организация по содействию семьям с детьми в трудной жизненной ситуации «Аистенок» (г. Екатеринбург) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            34,
            $ground6
        );
        $this->createGround(
            $manager,
            'Тамбовское  областное государственное автономное общеобразовательное учреждение  «Котовская школа-интернат для обучающихся с ограниченными возможностями здоровья» (г. Котовск);',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            35,
            $ground6
        );
        $this->createGround(
            $manager,
            'Частное учреждение  социального обслуживания «Центр развития семейных форм устройства детей» (г. Мурманск) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            36,
            $ground6
        );
        $this->createGround(
            $manager,
            'Областное государственное бюджетное учреждение социального обслуживания "Комплексный центр социального обслуживания населения Баяндаевского района" (Иркутская область)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            37,
            $ground6
        );
        $this->createGround(
            $manager,
            'Краевое государственное бюджетное учреждение "Организация, осуществляющая обучение, для детей-сирот и детей, оставшихся без попечения родителей "Детский дом 5" (Хабаровский край)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            38,
            $ground6
        );
        $this->createGround(
            $manager,
            'Тамбовское областное государственное бюджетное образовательное учреждение «Моршанская школа-интернат» (Тамбовская область)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            39,
            $ground6
        );
        $this->createGround(
            $manager,
            'Благотворительный  фонд «Волонтеры в помощь детям-сиротам» (г.Москва)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            40,
            $ground6
        );
        $this->createGround(
            $manager,
            'Негосударственное  образовательное частное  учреждение дополнительного образования взрослых «Институт политики детства и прикладной социальной работы» (г. Санкт-Петербург) ',
            'kns2017-2',
            41,
            $ground6
        );
        $this->createGround(
            $manager,
            'Государственное  бюджетное учреждение  социального обслуживания Новосибирской области «Социально-реабилитационный центр для несовершеннолетних «Снегири» (г. Новосибирск) ' ,
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            42,
            $ground6
        );
        $this->createGround(
            $manager,
            'Региональная общественная организация родителей детей-инвалидов «Дорогою добра» Кировской области (г.Киров)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            43,
            $ground6
        );
        $this->createGround(
            $manager,
            'Местная религиозная организация православный Приход храма в честь Архангела Михаила г. Смоленска Смоленской Епархии Русской Православной Церкви (Смоленская область)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            44,
            $ground6
        );

        $ground7 = $this->createGround(
            $manager,
            'Подготовка  и сопровождение замещающих семей, профилактика вторичного сиротства (профилактика отобраний (изъятий) / отказов детей из замещающих семей) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            45
            );
        $this->createGround(
            $manager,
            'Общественная  организация Шегарского района Томской области помощи детям и семьям группы риска по социальному сиротству «Рука в руке» (с. Мельниково Томской области) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            46,
            $ground7
        );
        $this->createGround(
            $manager,
            'Государственное  казённое образовательное учреждение для детей, нуждающихся в психолого-педагогической и медико-социальной помощи, «Волгоградский областной центр психолого-медико-социального сопровождения» (г. Волгоград) ',
            'kns2017-2',
            47,
            $ground7
        );
        $this->createGround(
            $manager,
            'Межрегиональная  общественная организация содействия  программе воспитания подрастающего поколения «Старшие Братья Старшие Сестры» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            48,
            $ground7
        );
        $this->createGround(
            $manager,
            'Некоммерческое партнерство Международный благотворительный центр «Надежда» (Удмуртская республика)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            49,
            $ground7
        );
        $this->createGround(
            $manager,
            'Государственное  бюджетное учреждение Ростовской области центр психолого-педагогической, медицинской и социальной помощи (г.Ростов-на-Дону) ',
            'kns2017-2',
            50,
            $ground7
        );
        $this->createGround(
            $manager,
            'Новосибирская  городская общественная организация усыновителей «День аиста» (г. Новосибирск) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            51,
            $ground7
        );
        $this->createGround(
            $manager,
            'Благотворительный  фонд поддержки детей-сирот «Надежда». Новочебоксарск) ',
            'kns2017-2',
            52,
            $ground7
        );
        $this->createGround(
            $manager,
            'Муниципальное бюджетное образовательное учреждение дополнительного образования "Центр психолого-педагогической, медицинской и социальной помощи "Семья" городского округа город Уфа (республика Башкортостан)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            53,
            $ground7
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение социального обслуживания Республики Карелия «Центр помощи детям, оставшимся без попечения родителей, № 3» (республика Карелия)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            54,
            $ground7
        );
        $this->createGround(
            $manager,
            'Благотворительный  фонд «Арифметика добра» (г. Москва)  ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            55,
            $ground7
        );
        $this->createGround(
            $manager,
            'Некоммерческий  благотворительный фонд «Надежда» ( г.Владимир) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            56,
            $ground7
        );
            $this->createGround(
                $manager,
                'Некоммерческая  организация Благотворительный фонд помощи детям-сиротам «Здесь и сейчас» ( г. Москва) ',
                'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
                57,
                $ground7
            );
        $this->createGround(
            $manager,
            'Областное государственное казенное учреждение "Центр помощи детям, оставшимся без попечения родителей, Бакчарского района" (Томская область)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            58,
            $ground7
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное  учреждение города Москвы Центр социальной помощи семье и детям «Измайлово» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            59,
            $ground7
        );

        $ground8 = $this->createGround(
            $manager,
            '«Полный цикл» подготовки  детей к семейному устройству и подбора  для них, подготовки и сопровождения замещающих семей, работа по возврату детей в кровные семьи и сопровождение этих семей, профилактики социального сиротства ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            60
        );
        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение "Центр содействия семейному устройству детей-сирот и детей, оставшихся без попечения родителей г. Уссурийска" (Приморский край)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            61,
            $ground8
        );
        $this->createGround(
            $manager,
            'Благотворительный Фонд  «Помощь детям, затронутым эпидемией ВИЧ-инфекции «Дети плюс» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            62,
            $ground8
        );
        $this->createGround(
            $manager,
            'Бюджетное учреждение  социального обслуживания для детей-сирот и детей, оставшихся без попечения родителей, Вологодской области «Череповецкий центр помощи детям, оставшимся без попечения родителей, «Наши дети» (г. Череповец) ',
            'kns2017-2',
            63,
            $ground8
        );
        $this->createGround(
            $manager,
            'Краевое государственное  казенное учреждение для детей-сирот и детей, оставшихся без попечения родителей «Ачинский детский дом» ( г. Ачинск Красноярский край) ',
            'kns2017-2',
            64,
            $ground8
        );
        $this->createGround(
            $manager,
            'Фонд поддержки семьи и детей  «Хранители детства» (г. Москва) ',
            'kns2017-2',
            65,
            $ground8
        );

        $ground9 = $this->createGround(
            $manager,
            ' Реформирование  организаций для детей-сирот и детей, оставшихся без попечения родителей (детских домов, школ-интернатов, домов ребенка, социальных приютов и пр.) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            66
        );
        $this->createGround(
            $manager,
            'Муниципальное  казенное учреждение города Новосибирска «Центр помощи детям, оставшимся без попечения родителей «Созвездие» (г. Новосибирск) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            67,
            $ground9
        );
        $this->createGround(
            $manager,
            'Региональная  благотворительная общественная организация «Центр лечебной педагогики» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            68,
            $ground9
        );
        $this->createGround(
            $manager,
            'Автономная  некоммерческая организация «Семья детям» (г. Екатеринбург) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            69,
            $ground9
        );

        $ground10 = $this->createGround(
            $manager,
            'Постинтернатное  сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет – выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека (попечительство) в замещающей семье ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            70
        );
        $this->createGround(
            $manager,
            'Частное учреждение  дополнительного образования и реализации социальных проектов «Центр социально-психологической помощи» (г. Киров) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            71,
            $ground10
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд  добровольной помощи детям «Владмама» (г.Владивосток ) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            72,
            $ground10
        );
        $this->createGround(
            $manager,
            'Благотворительный  фонд содействия образованию детей-сирот «Большая Перемена» (г. Москва) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            73,
            $ground10
        );

        $this->createGround(
            $manager,
            'Государственное бюджетное общеобразовательное учреждение «Мензелинская  школа-интернат для детей-сирот и детей, оставшихся без попечения родителей, с ограниченными возможностями здоровья» (Республика Татарстан)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            74,
            $ground10
        );

        $ground11 = $this->createGround(
            $manager,
            'Активизация  поддержки замещающих и «кризисных» кровных семей со стороны окружения (родственники, друзья, школа, детские сады, соседи и пр.); развитие взаимоподдержки, консолидации сообществ членов замещающих семей  (родителей, «выпускников» замещающих семей)',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            75
        );
        $this->createGround(
            $manager,
            'Краевое  государственное бюджетное учреждение социального обслуживания «Комплексный центр социального обслуживания населения города Барнаула» (г. Барнаул) ',
            'kns2017-2'.ApplicationTypes::KNS_APPLICATION_2019,
            76,
            $ground11
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд "Центр социальной адаптации святителя Василия Великого" (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            77,
            $ground6
        );

        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение социального обслуживания населения Ростовской области "Социально-реабилитационный центр для несовершеннолетних г. Ростова-на-Дону" (г.Ростов-на-Дону)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            78,
            $ground6
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация "Центр программ профилактики и социальной реабилитации" (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            79,
            $ground6
        );

        $this->createGround(
            $manager,
            'Бюджетное учреждение Омской области «Комплексный центр социального обслуживания населения Москаленского района» (Омская область)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            80,
            $ground6
        );

        $this->createGround(
            $manager,
            'Ассоциация пациентов и специалистов, помогающих людям с ВИЧ, вирусными гепатитами и другими социально значимыми заболеваниями "Е.В.А" (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            81,
            $ground6
        );

        $this->createGround(
            $manager,
            'Бюджетное учреждение социального обслуживания Вологодской области "Комплексный центр социального обслуживания населения города Череповца "Забота" (Вологодская область)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            82,
            $ground6
        );

        $this->createGround(
            $manager,
            'Фонд "Национальный фонд защиты детей от жестокого обращения" (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            83,
            $ground6
        );

        $this->createGround(
            $manager,
            'Государственное учреждение дополнительного образования "Областная детская эколого-биологическая станция" (г. Кемерово)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            84,
            $ground6
        );

        $this->createGround(
            $manager,
            'Лесосибирский детский дом им. Ф.Э. Дзержинского (Красноярский край)',
            ApplicationTypes::KNS_APPLICATION_2019,
            85,
            $ground6
        );

        $this->createGround(
            $manager,
            'Муниципальное бюджетное образовательное учреждение для детей, нуждающихся в психолого-педагогической и медико-социальной помощи Петрозаводского городского округа «Центр психолого–медико-социального сопровождения» (республика Карелия)',
            ApplicationTypes::KNS_APPLICATION_2019,
            86,
            $ground7
        );

        $this->createGround(
            $manager,
            'Государственное образовательное учреждение высшего образования Московской области Московский государственный областной университет (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2019,
            87,
            $ground7
        );

        $this->createGround(
            $manager,
            'Региональное общественная организация "Ассоциация замещающих семей Московской области" (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            88,
            $ground7
        );

        $this->createGround(
            $manager,
            'Смоленское областное государственное бюджетное учреждение "Центр психолого-медико-социального сопровождения детей и семей" (г. Смоленск)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            89,
            $ground7
        );

        $this->createGround(
            $manager,
            'Карельская региональная общественная организация по социальной помощи населению "Гармония" (республика Карелия)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            90,
            $ground7
        );

        $this->createGround(
            $manager,
            'Некоммерческая организация "Ассоциация замещающих семей Свердловской области" (г. Екатеринбург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            91,
            $ground7
        );

        $this->createGround(
            $manager,
            'Областное государственное казенное учреждение социального обслуживания "Центр социальной помощи семье и детям Нижнеилимского района" (Иркутская область)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            92,
            $ground7
        );

        $this->createGround(
            $manager,
            'Благотворительный фонд "Сохраняя жизнь" (г.Оренбург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            93,
            $ground7
        );

        $this->createGround(
            $manager,
            'Благотворительный фонд "Апрель" (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            94,
            $ground7
        );

        $this->createGround(
            $manager,
            'Архангельской региональной общественной организации «Приемная семья» (г. Архангельск)',
            ApplicationTypes::KNS_APPLICATION_2019,
            95,
            $ground7
        );

        $this->createGround(
            $manager,
            'Областное государственное казенное учреждение социального обслуживания "Центр помощи детям, оставшимся без попечения родителей, г.Братска" (Иркутская область)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            96,
            $ground7
        );

        $this->createGround(
            $manager,
            'Союз Приемных Родителей, Усыновителей, Опекунов и Попечителей, и их Объединений (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            97,
            $ground7
        );

        $this->createGround(
            $manager,
            'ДБФ "Счастливые дети" (Красноярский край)',
            ApplicationTypes::KNS_APPLICATION_2019,
            98,
            $ground7
        );

        $this->createGround(
            $manager,
            'Бюджетное учреждение социального обслуживания для детей-сирот и детей, оставшихся без попечения родителей, Вологодской области «Череповецкий центр помощи детям, оставшимся без попечения родителей, «Наши дети» (Вологодская область)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            99,
            $ground8
        );

        $this->createGround(
            $manager,
            'Санкт-Петербургское государственное бюджетное учреждение социального обслуживания населения «Социально-реабилитационный центр для несовершеннолетних «Дом милосердия» (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            100,
            $ground8
        );

        $this->createGround(
            $manager,
            'Детский благотворительный фонд "Солнечный Город" (г. Новосибирск)',
            ApplicationTypes::KNS_APPLICATION_2019,
            101,
            $ground9
        );

        $this->createGround(
            $manager,
            'Пермская региональная благотворительная общественная организация "Солнечный круг" (г. Пермь)',
            ApplicationTypes::KNS_APPLICATION_2019,
            102,
            $ground9
        );

        $this->createGround(
            $manager,
            'Санкт-Петербургское государственное бюджетное учреждение социального обслуживания населения "Центр социальной помощи семье и детям Пушкинского района "Аист" (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            103,
            $ground10
        );

        $this->createGround(
            $manager,
            'Краевое государственное автономное учреждение «Камчатский ресурсный центр содействия развитию семейных форм устройства» (Камчатский край)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            104,
            $ground10
        );

        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение "Центр по развитию семейных форм устройства детей, оставшихся без попечения родителей, и постинтернатному сопровождению" (Хабаровский край)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            105,
            $ground11
        );

        $ground12 = $this->createGround(
            $manager,
            'Подготовка детей, воспитываемых в организациях для детей-сирот и детей, оставшихся без попечения родителей, к семейному устройству',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            106
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация «Агентство социальной поддержки семьи и защиты семейных ценностей «Моя семья»» (г. Ярославль)',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            107,
            $ground12
        );
        $this->createGround(
            $manager,
            'Союз Приёмных родителей, Усыновителей, Опекунов, Попечителей и их Объединений (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2019,
            108,
            $ground12
        );
        
        // for FarvaterApplication2018
        $ground13 = $this->createGround(
            $manager,
            'Профилактика социального сиротства через работу с «кровными» (биологическими) семьями',
            ApplicationTypes::KNS_APPLICATION_2018,
            109
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация «Центр развития инновационных социальных услуг «Партнёрство каждому ребёнку» (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2018,
            110,
            $ground13
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация дополнительного образования и социального обслуживания «Перспективы» (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2018,
            111,
            $ground13
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация социальных услуг «Родительский центр «Подсолнух» (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2018,
            112,
            $ground13
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд «Дети наши» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            113,
            $ground13
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение Центр содействия семейному воспитанию «Вера. Надежда. Любовь» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            114,
            $ground13
        );
        $this->createGround(
            $manager,
            'Некоммерческая организация «Благотворительный фонд  «Даунсайд Ап» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            115,
            $ground13
        );
        $this->createGround(
            $manager,
            'Региональная общественная организация Красноярский центр лечебной педагогики (г. Красноярск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            116,
            $ground13
        );
        $this->createGround(
            $manager,
            'Санкт-Петербургский общественный благотворительный фонд «Родительский мост» (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2018,
            117,
            $ground13
        );
        $this->createGround(
            $manager,
            'Межрегиональная общественная организация по содействию семьям с детьми в трудной жизненной ситуации «Аистёнок» (Свердловская область, г. Екатеринбург)',
            ApplicationTypes::KNS_APPLICATION_2018,
            118,
            $ground13
        );
        $this->createGround(
            $manager,
            'Тамбовское областное государственное автономное общеобразовательное учреждение «Котовская школа-интернат  для обучающихся с ограниченными возможностями здоровья» (Тамбовская область, г. Котовск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            119,
            $ground13
        );
        $this->createGround(
            $manager,
            'Частное учреждение социального обслуживания «Центр развития семейных форм устройства детей» (г. Мурманск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            120,
            $ground13
        );
        $this->createGround(
            $manager,
            'Областное государственное бюджетное учреждение социального обслуживания «Комплексный центр социального обслуживания населения Баяндаевского района» (Иркутская область, с. Баяндай)',
            ApplicationTypes::KNS_APPLICATION_2018,
            121,
            $ground13
        );
        $this->createGround(
            $manager,
            'Краевое государственное образовательное учреждение «Организация, осуществляющая обучение, для детей-сирот и детей, оставшихся без попечения родителей «Детский дом 5» (г. Хабаровск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            122,
            $ground13
        );
        $this->createGround(
            $manager,
            'Тамбовское областное государственное бюджетное образовательное учреждение «Моршанская школа-интернат» (Тамбовская область, г. Моршанск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            123,
            $ground13
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд «Волонтёры в помощь детям-сиротам» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            124,
            $ground13
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение социального обслуживания Новосибирской области «Социально-реабилитационный центр для несовершеннолетних «Снегири» (г. Новосибирск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            125,
            $ground13
        );
        $this->createGround(
            $manager,
            'Региональная общественная организация родителей детей-инвалидов «Дорогою добра» (г. Киров)',
            ApplicationTypes::KNS_APPLICATION_2018,
            126,
            $ground13
        );
        $this->createGround(
            $manager,
            'Местная религиозная организация православный Приход храма в честь Архангела Михаила г. Смоленска Смоленской Епархии Русской Православной Церкви (Московский Патриархат) (г. Смоленск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            127,
            $ground13
        );

        $ground14 = $this->createGround(
            $manager,
            'Подготовка и сопровождение замещающих семей, профилактика вторичного сиротства (профилактика отобраний (изъятий)/отказов детей из замещающих семей)',
            ApplicationTypes::KNS_APPLICATION_2018,
            128
        );
        $this->createGround(
            $manager,
            'Общественная организация Шегарского района Томской области помощи детям и семьям группы риска по социальному сиротству «Рука в руке» (Томская область, с. Мельниково)',
            ApplicationTypes::KNS_APPLICATION_2018,
            129,
            $ground14
        );
        $this->createGround(
            $manager,
            'Государственное казенное учреждение для детей, нуждающихся в психолого-педагогической и медико-социальной помощи, «Волгоградский областной центр психолого-медико-социального сопровождения» (г. Волгоград)',
            ApplicationTypes::KNS_APPLICATION_2018,
            130,
            $ground14
        );
        $this->createGround(
            $manager,
            'Межрегиональная общественная организация содействия программе воспитания подрастающего поколения «Старшие Братья Старшие Сестры» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            131,
            $ground14
        );
        $this->createGround(
            $manager,
            'Некоммерческое партнёрство Международный благотворительный центр «Надежда» (Удмуртская Республика, г. Ижевск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            132,
            $ground14
        );
        $this->createGround(
            $manager,
            'Новосибирская городская общественная организация усыновителей «День аиста» (г. Новосибирск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            133,
            $ground14
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд поддержки детей-сирот «Надежда» (Чувашская Республика, г. Новочебоксарск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            134,
            $ground14
        );
        $this->createGround(
            $manager,
            'Муниципальное бюджетное образовательное учреждение дополнительного образования Центр психолого-педагогической, медицинской и социальной помощи «Семья» (Республика Башкортостан, г. Уфа)',
            ApplicationTypes::KNS_APPLICATION_2018,
            135,
            $ground14
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение социального обслуживания Республики Карелия «Центр помощи детям, оставшимся без попечения родителей, №3» (Республика Карелия, пгт Калевала)',
            ApplicationTypes::KNS_APPLICATION_2018,
            136,
            $ground14
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд «Арифметика добра» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            137,
            $ground14
        );
        $this->createGround(
            $manager,
            'Некоммерческий благотворительный фонд «Надежда» (г. Владимир)',
            ApplicationTypes::KNS_APPLICATION_2018,
            138,
            $ground14
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд «Здесь и сейчас» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            139,
            $ground14
        );
        $this->createGround(
            $manager,
            'Областное государственное казенное учреждение «Центр помощи детям, оставшимся без попечения родителей, Бакчарского района» (Томская область, с. Бакчар)',
            ApplicationTypes::KNS_APPLICATION_2018,
            140,
            $ground14
        );
        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение города Москвы Центр социальной помощи семье и детям «Измайлово» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            141,
            $ground14
        );

        $ground15 = $this->createGround(
            $manager,
            '«Полный цикл» подготовки детей к семейному устройству и подбора для них, подготовки и сопровождения замещающих семей, работа по возврату детей в кровные семьи и сопровождение этих семей, профилактика социального сиротства',
            ApplicationTypes::KNS_APPLICATION_2018,
            142
        );
        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение «Центр содействия семейному устройству детей-сирот и детей, оставшихся без попечения родителей» (Приморский край, г. Уссурийск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            143,
            $ground15
        );
        $this->createGround(
            $manager,
            'Благотворительный Фонд «Помощь детям, затронутым эпидемией ВИЧ-инфекции «Дети плюс» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            144,
            $ground15
        );
        $this->createGround(
            $manager,
            'БУ СО ВО «Череповецкий центр помощи детям, оставшимся без попечения родителей, «Наши дети» (Вологодская область, г. Череповец)',
            ApplicationTypes::KNS_APPLICATION_2018,
            145,
            $ground15
        );
        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение для детей-сирот и детей, оставшихся без попечения родителей, «Ачинский детский дом» (Красноярский край, г. Ачинск)',
            ApplicationTypes::KNS_APPLICATION_2018,
            146,
            $ground15
        );
        $this->createGround(
            $manager,
            'Фонд поддержки семьи и детей «Хранители детства» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            147,
            $ground15
        );

        $ground16 = $this->createGround(
            $manager,
            'Реформирование организаций для детей-сирот и детей, оставшихся без попечения родителей (детских домов, школ-интернатов, домов ребенка, социальных приютов и пр.)',
            ApplicationTypes::KNS_APPLICATION_2018.ApplicationTypes::KNS_APPLICATION_2020,
            148
        );
        $this->createGround(
            $manager,
            'Муниципальное казенное учреждение города Новосибирска «Центр помощи детям, оставшимся без попечения родителей, «Созвездие» (г. Новосибирск)',
            ApplicationTypes::KNS_APPLICATION_2018.ApplicationTypes::KNS_APPLICATION_2020,
            149,
            $ground16
        );
        $this->createGround(
            $manager,
            'Региональная благотворительная общественная организация  «Центр лечебной педагогики» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            150,
            $ground16
        );
        $this->createGround(
            $manager,
            'Автономная некоммерческая организация «Семья детям» (Свердловская область, г. Екатеринбург)',
            ApplicationTypes::KNS_APPLICATION_2018,
            151,
            $ground16
        );

        $ground17 = $this->createGround(
            $manager,
            'Постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет- выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека в замещающей семье',
            ApplicationTypes::KNS_APPLICATION_2018,
            152
        );
        $this->createGround(
            $manager,
            'Частное учреждение дополнительного образования и реализации социальных проектов «Центр социально-психологической помощи» (г. Киров)',
            ApplicationTypes::KNS_APPLICATION_2018,
            153,
            $ground17
        );
        $this->createGround(
            $manager,
            ' Благотворительный фонд добровольной помощи детям «Владмама» (Приморский край, г. Владивосток)',
            ApplicationTypes::KNS_APPLICATION_2018,
            154,
            $ground17
        );
        $this->createGround(
            $manager,
            'Благотворительный фонд содействия образованию детей-сирот «Большая перемена» (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            155,
            $ground17
        );

        $ground18 = $this->createGround(
            $manager,
            'Активизация поддержки замещающих и кризисных семей со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.); развитие взаимоподдержки, консолидации сообществ членов замещающих семей (родителей, «выпускников» замещающих семей)',
            ApplicationTypes::KNS_APPLICATION_2018,
            156
        );
        $this->createGround(
            $manager,
            'Краевое государственное учреждение социального обслуживания «Комплексный центр социального обслуживания населения города Барнаула» (Алтайский край, г. Барнаул)',
            ApplicationTypes::KNS_APPLICATION_2018,
            157,
            $ground18
        );

        $this->createGround(
            $manager,
            'Автономная некоммерческая организация "Центр программ профилактики и социальной реабилитации" (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2018,
            158,
            $ground13
        );

        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение для детей-сирот и детей, оставшихся без попечения родителей "Лесосибирский детский дом им. Ф.Э. Дзержинского"',
            ApplicationTypes::KNS_APPLICATION_2019,
            159,
            $ground6
        );

        $this->createGround(
            $manager,
            'Хабаровская краевая общественная организация замещающих семей "Чужих детей не бывает"',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            160,
            $ground6
        );

        $this->createGround(
            $manager,
            'Муниципальное бюджетное учреждение города Челябинска «Центр помощи детям, оставшимся без попечения родителей, «Акварель»',
            ApplicationTypes::KNS_APPLICATION_2019.ApplicationTypes::KNS_APPLICATION_2020,
            161,
            $ground7
        );

        $this->createGround(
            $manager,
            'ДБФ "Счастливые дети" (Красноярский край)',
            ApplicationTypes::KNS_APPLICATION_2020,
            162,
            $ground12
        );

        $this->createGround(
            $manager,
            'Региональное общественное движение «Петербургские родители», (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2020,
            163,
            $ground10
        );

        $this->createGround(
            $manager,
            'Детский благотворительный фонд "Солнечный Город" (г. Новосибирск)',
            ApplicationTypes::KNS_APPLICATION_2020,
            164,
            $ground16
        );

        $this->createGround(
            $manager,
            'Пермская региональная благотворительная общественная организация "Солнечный круг" (г. Пермь)',
            ApplicationTypes::KNS_APPLICATION_2020,
            165,
            $ground16
        );

        $this->createGround(
            $manager,
            'Муниципальное бюджетное учреждение Петрозаводского городского округа «Центр психолого-педагогической помощи и социальной поддержки» (республика Карелия)',
            ApplicationTypes::KNS_APPLICATION_2020,
            166,
            $ground7
        );

        $this->createGround(
            $manager,
            'Краевое государственное казенное учреждение «Центр содействия семейному устройству детей-сирот и детей, оставшихся без попечения родителей, № 1 г. Владивостока» (г. Владивосток)',
            ApplicationTypes::KNS_APPLICATION_2020,
            167,
            $ground7
        );

        $this->createGround(
            $manager,
            'Санкт-Петербургская Благотворительная общественная организация «Перспективы» (г. Санкт-Петербург) ',
            ApplicationTypes::KNS_APPLICATION_2020,
            168,
            $ground6);

        $this->createGround(
            $manager,
            'Санкт-Петербургская общественная благотворительная организация «Общество Свт. Иоасафа», (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2020,
            169,
            $ground6
        );

        $this->createGround(
            $manager,
            'Союз Приёмных родителей, Усыновителей, Опекунов, Попечителей и их Объединений (г. Москва)',
            ApplicationTypes::KNS_APPLICATION_2020,
            171,
            $ground7
        );

        $this->createGround(
            $manager,
            'Межрегиональная общественная организация по содействию семьям с детьми в трудной жизненной ситуации «Аистенок» (г. Екатеринбург)',
            ApplicationTypes::KNS_APPLICATION_2020,
            172,
            $ground6
        );

        $this->createGround(
            $manager,
            'Частное учреждение социального обслуживания «Социальный центр – SOS Мурманска» (г. Мурманск)',
            ApplicationTypes::KNS_APPLICATION_2020,
            173,
            $ground6
        );

        $this->createGround(
            $manager,
            'КГКУ «Лесосибирский детский дом им. Ф.Э. Дзержинского» (Красноярский край)',
            ApplicationTypes::KNS_APPLICATION_2020,
            174,
            $ground6
        );

        $this->createGround(
            $manager,
            'Архангельская региональная общественная организация «Приемная семья» (г. Архангельск)',
            ApplicationTypes::KNS_APPLICATION_2020,
            175,
            $ground7
        );

        $this->createGround(
            $manager,
            'Автономная некоммерческая организация «Семья детям» (г. Екатеринбург)',
            ApplicationTypes::KNS_APPLICATION_2020,
            176,
            $ground16
        );

        $this->createGround(
            $manager,
            'Краевое государственного бюджетное учреждение социального обслуживания "Комплексный центр социального обслуживания населения города Барнаула" (Алтайский край)',
            ApplicationTypes::KNS_APPLICATION_2020,
            177,
            $ground11
        );

        $this->createGround(
            $manager,
            'СПБ ГБУ «Центр реабилитации инвалидов и детей-инвалидов Приморского района Санкт-Петербурга» (г. Санкт-Петербург)',
            ApplicationTypes::KNS_APPLICATION_2020,
            178,
            $ground6
        );

        $this->createGround(
            $manager,
            'Государственное бюджетное учреждение для детей, нуждающихся в психолого-педагогической и медико-социальной помощи, «Волгоградский областной центр психолого-медико-социального сопровождения» (г. Волгоград)',
            ApplicationTypes::KNS_APPLICATION_2020,
            179,
            $ground6
        );

        // СЛЕДУЮЩЕЕ ЗНАЧЕНИЕ staticId - 180!! для нового Ground обязательно инкрементировать (значение должно быть уникально).

        $manager->flush();
    }
}