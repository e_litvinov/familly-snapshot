<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/27/17
 * Time: 4:29 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\KNS2017;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater2017\Application;
use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Utils\ApplicationTypes;
use NKO\OrderBundle\Utils\ConstValues;

class LoadSocialResultData implements FixtureInterface
{
    public function createResult(ObjectManager $manager, $name, $types, $staticId)
    {
        $result = $manager->getRepository(SocialResult::class)->findOneBy(array('name' => $name));
        if (!$result) {
            $result = new SocialResult();
            $manager->persist($result);
        }

        $result->setName($name);
        $result->setApplicationType($types);
        $result->setStaticId($staticId);
    }

    public function load(ObjectManager $manager)
    {
        $this->createResult(
            $manager,
            "Увеличение числа детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства (в том числе подростков, детей с ОВЗ, сиблингов)",
            [
                ApplicationTypes::BRIEF_APPLICATION_2017 => ConstValues::AUTO,
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_KNS => 0
            ],
            1
        );
        $this->createResult(
            $manager,
            "Увеличение числа детей, возвращенных в кровные семьи (в том числе подростков и детей с ОВЗ)",
            [
                ApplicationTypes::BRIEF_APPLICATION_2017 => ConstValues::AUTO,
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_KNS => 1
            ],
            2
        );
        $this->createResult(
            $manager,
            "Уменьшение количества отобраний (изъятий)/отказов детей из кровных и замещающих семей",
            [
                ApplicationTypes::BRIEF_APPLICATION_2017 => ConstValues::AUTO
            ],
            3
        );
        $this->createResult(
            $manager,
            "Улучшение благополучия детей и семей – участников Программы",
            [
                ApplicationTypes::BRIEF_APPLICATION_2017 => ConstValues::AUTO,
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_KNS => 4
            ],
            4
        );
        $this->createResult(
            $manager,
            "Рост уровня готовности детей к самостоятельной жизни – они становятся полноценными гражданами, обеспечивающими благополучие общества",
            [
                ApplicationTypes::BRIEF_APPLICATION_2017 => ConstValues::AUTO,
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_KNS => 5
            ],
            5
        );
        $this->createResult(
            $manager,
            "Иные социальные результаты.",
            [
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO
            ],
            6
        );
        $this->createResult(
            $manager,
            "Уменьшение количества изъятий/отказов детей из кровных семей",
            [
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_KNS => 2
            ],
            7
        );
        $this->createResult(
            $manager,
            "Уменьшение количества изъятий/отказов детей из замещающих семей",
            [
                ApplicationTypes::BRIEF_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::KNS_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::CONTINUATION_KNS => 3
            ],
            8
        );

        $this->createResult(
            $manager,
            "Уменьшение количества изъятий/отказов детей из кровных и замещающих семей",
            [
                ApplicationTypes::CONTINUATION_SECOND_STAGE_APPLICATION_2018 => ConstValues::AUTO,
            ],
            9
        );

        $this->createResult(
            $manager,
            "Повышены доступность и качество профессиональной помощи семьям и детям в области профилактики 
            социального сиротства и семейного устройства детей-сирот и детей, оставшихся без попечения родителей;",
            [
                ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::FARVATER_APPLICATION_2018 => ConstValues::AUTO
            ],
            10
        );
        $this->createResult(
            $manager,
            "Повышен уровень компетенций специалистов сферы защиты детства по темам, приоритетным для достижения 
            долгосрочных социальных результатов",
            [
                ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::FARVATER_APPLICATION_2018 => ConstValues::AUTO
            ],
            11
        );
        $this->createResult(
            $manager,
            "Сформированы отраслевые ресурсные центры – стажировочные площадки в области профилактики социального 
            сиротства и семейного устройства, эффективно тиражирующие свои практики",
            [
                ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::FARVATER_APPLICATION_2018 => ConstValues::AUTO
            ],
            12
        );
        $this->createResult(
            $manager,
            "Эффективная практика, тиражируемая участниками проекта, внедряется в других организациях",
            [
                ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO,
                ApplicationTypes::FARVATER_APPLICATION_2018 => ConstValues::AUTO
            ],
            13
        );
        $this->createResult(
            $manager,
            "Свой вариант",
            [
                ApplicationTypes::CONTINUATION_APPLICATION_2018 => ConstValues::AUTO
            ],
            14
        );

        //continue sequence "staticId", last value 14!!!
        $manager->flush();
    }
}
