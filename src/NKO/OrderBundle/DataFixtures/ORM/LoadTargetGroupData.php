<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 11:36 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\TargetGroup;

class LoadTargetGroupData implements FixtureInterface
{
    public function createGroup(ObjectManager $manager, $name)
    {
        if(!$manager->getRepository('NKOOrderBundle:Farvater\TargetGroup')->findOneBy(array('name' => $name))) {
            $group = new TargetGroup();
            $group->setName($name);
            $manager->persist($group);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createGroup($manager, "кризисные кровные семьи с детьми");
        $this->createGroup($manager, "кандидаты в замещающие (приемные) родители");
        $this->createGroup($manager, "замещающие (приемные) семьи");
        $this->createGroup($manager, "дети-сироты и дети, оставшиеся без попечения родителей, воспитываемые в детских учреждениях");
        $this->createGroup($manager, "«выпускники»: дети в возрасте от 18 до 23 лет – выпускники детских учреждений; дети, в отношении которых прекращена опека (попечительство) в замещающей семье");
        $this->createGroup($manager, "специалисты сферы защиты детства (уточните)");
        $this->createGroup($manager, "иные (укажите)");

        $manager->flush();
    }
}