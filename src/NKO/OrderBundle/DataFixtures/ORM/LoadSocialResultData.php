<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/17/17
 * Time: 2:47 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\SocialResult;

class LoadSocialResultData implements FixtureInterface
{
    public function createResult(ObjectManager $manager, $name)
    {
        $result = $manager->getRepository(SocialResult::class)->findOneBy(array('name' => $name));
        if(!$result) {
            $result = new SocialResult();
            $manager->persist($result);
        }

        $result->setName($name);
    }

    public function load(ObjectManager $manager) {
        $this->createResult($manager, "Увеличение доли (числа) детей, воспитываемых в семьях");
        $this->createResult($manager, "Сокращение доли (числа) детей, изъятых из кровных семей");
        $this->createResult($manager, "Сокращение доли (числа) случаев возврата детей из замещающих семей");
        $this->createResult($manager, "Увеличение доли (числа) детей «сложных» категорий, воспитываемых в семьях (кровных и принятых в замещающие семьи)");
        $this->createResult($manager, "Рост числа профессиональных замещающих семей");
        $this->createResult($manager, "Повышение уровня готовности детей к самостоятельной жизни в обществе");
        $this->createResult($manager, "Повышение спроса на профессиональные услуги (в области профилактики сиротства) со стороны семей целевых групп");
        $this->createResult($manager, "Повышение родительских компетенций");
        $this->createResult($manager, "Улучшение детско-родительских отношений");
        $this->createResult($manager, "Улучшение психического состояния детей");
        $this->createResult($manager, "Улучшение физического состояния детей");
        $this->createResult($manager, "Повышение уровня развития и навыков у детей");
        $this->createResult($manager, "Повышение уровня поддержки замещающих семей со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)");
        $this->createResult($manager, "Повышение уровня поддержки кризисных кровных семей со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)");
        $this->createResult($manager, "Повышение доступности и качества экспертной поддержки для специалистов сферы детства");
        $this->createResult($manager, "Иной результат (уточните)");

        $manager->flush();
    }
}