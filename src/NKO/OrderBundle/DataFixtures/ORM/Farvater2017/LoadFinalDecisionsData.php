<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/26/17
 * Time: 11:35 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Farvater2017;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\FinalDecision;

class LoadFinalDecisionsData implements FixtureInterface
{
    const FARVATER_COMPETITION_ID = 8;

    public function createFinalDecision(ObjectManager $manager, $name, $type, $competition)
    {
        if(!$manager->getRepository('NKOOrderBundle:FinalDecision')->findOneBy(array('name' => $name))) {
            $finalDecision = new FinalDecision();
            $finalDecision->setName($name);
            $finalDecision->setTypeDecision($type);
            $finalDecision->addCompetition($competition);
            $manager->persist($finalDecision);
        }
    }

    public function load(ObjectManager $manager) {
        $farvaterCompetition = $manager->getRepository("NKOOrderBundle:Competition")->find(self::FARVATER_COMPETITION_ID);
        $this->createFinalDecision($manager, "Поддержать проект", false, $farvaterCompetition);
        $this->createFinalDecision($manager, "поддержать проект с условием внесения изменений в дизайн проекта и/или его бюджет", false, $farvaterCompetition);
        $this->createFinalDecision($manager, "Отклонить проект", true, $farvaterCompetition);

        $manager->flush();
    }

}