<?php

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 4/4/17
 * Time: 3:36 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Farvater2017;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater2017\PracticeSpreadFormat;

class LoadPracticeSpreadFormatData implements FixtureInterface
{
    public function createAudience(ObjectManager $manager, $name)
    {
        if(!$manager->getRepository('NKOOrderBundle:Farvater2017\PracticeSpreadFormat')
            ->findOneBy(array('name' => $name))){
            $format = new PracticeSpreadFormat();
            $format->setName($name);
            $manager->persist($format);
        }
    }


    public function load(ObjectManager $manager) {
        $this->createAudience($manager, "организация стажировок на базе организации / выезды специалистов");
        $this->createAudience($manager, "групповые тренинги");
        $this->createAudience($manager, "индивидуальные консультации (очно / онлайн)");
        $this->createAudience($manager, "электронные рассылки материалов, ссылок на них");
        $this->createAudience($manager, "участие в публичных мероприятиях (презентации, выступления и пр.)");
        $this->createAudience($manager, "организация и проведение публичных мероприятий (конференций, круглых столов и пр.)");
        $this->createAudience($manager, "вебинары");
        $this->createAudience($manager, "супервизии");
        $this->createAudience($manager, "коучинг / наставничество");
        $this->createAudience($manager, "мониторинг внедрения практики другими организациями");
        $this->createAudience($manager, "издание методических материалов");
        $this->createAudience($manager, "размещение информации онлайн (сайты и пр.)");

        $manager->flush();
    }
}