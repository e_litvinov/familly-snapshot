<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 4/5/17
 * Time: 7:46 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM\Farvater2017;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater2017\EmploymentRelationship;

class LoadEmploymentRelationshipData implements FixtureInterface
{
    public function createRelationship(ObjectManager $manager, $name)
    {
        if(!$manager->getRepository('NKOOrderBundle:Farvater2017\EmploymentRelationship')
            ->findOneBy(array('name' => $name))){
            $relationship = new EmploymentRelationship();
            $relationship->setName($name);
            $manager->persist($relationship);
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->createRelationship($manager, "собственный сотрудник");
        $this->createRelationship($manager, "привлеченный сотрудник");
        $this->createRelationship($manager, "доброволец");

        $manager->flush();
    }
}