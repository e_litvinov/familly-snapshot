<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/18/17
 * Time: 5:32 PM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\PublicationIndex;

class LoadPublicationIndexData implements FixtureInterface
{
    public function createIndex(ObjectManager $manager, $name)
    {
        $index = $manager->getRepository('NKOOrderBundle:Farvater\PublicationIndex')
            ->findOneBy(array('name' => $name));

        if(!$index) {
            $index = new PublicationIndex();
            $index->setName($name);
            $manager->persist($index);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createIndex($manager, "Число сообщений в СМИ, инициированных в рамках проекта");
        $this->createIndex($manager, "Число изданных информационных / методических материалов о практике проекта и их тираж");
        $this->createIndex($manager, "Тираж печатных информационных / методических материалов");
        $this->createIndex($manager, "Количество распространенных экземпляров информационных / методических материалов по проекту (например, число скачиваний, рассылок и пр.)");

        $manager->flush();
    }
}