<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 1/19/17
 * Time: 9:52 AM
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\Farvater\IntroductionIndex;

class LoadIntroductionIndexData implements FixtureInterface
{
    public function createIndex(ObjectManager $manager, $name)
    {
        $index = $manager->getRepository('NKOOrderBundle:Farvater\IntroductionIndex')
            ->findOneBy(array('name' => $name));

        if(!$index) {
            $index = new IntroductionIndex();
            $index->setName($name);
            $manager->persist($index);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createIndex($manager, "Число участников, получивших новые знания и компетенции");
        $this->createIndex($manager, "Число участников, у которых повысился уровень навыков по результатам распространения практики");
        $this->createIndex($manager, "Число подготовленных специалистов, внедривших практику в свою деятельность");
        $this->createIndex($manager, "Число организаций, внедривших практику в свою деятельность");

        $manager->flush();
    }
}