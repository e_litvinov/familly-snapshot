<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 12.08.16
 * Time: 2:31
 */

namespace NKO\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\OrderBundle\Entity\TraineeshipFormat;

class LoadTraineeshipFormatData implements FixtureInterface
{
    public function createFormat(ObjectManager $manager, $formatName)
    {
        if(!$manager->getRepository('NKOOrderBundle:TraineeshipFormat')->findOneBy(array('formatName' => $formatName))) {
            $group = new TraineeshipFormat();
            $group->setFormatName($formatName);
            $manager->persist($group);
        }
    }

    public function load(ObjectManager $manager) {
        $this->createFormat($manager, "поездка в организацию");
        $this->createFormat($manager, "дистанционное обучение (в форме переписки, " .
            "вебинара, скайп-семинаров и пр.");
        $this->createFormat($manager, "приглашение специалиста к себе в организацию");
        $manager->flush();
    }
}
