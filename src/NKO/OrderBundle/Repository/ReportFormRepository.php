<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 26.8.16
 * Time: 12.19
 */

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;

class ReportFormRepository extends EntityRepository
{
    const FORMAT = 'Y-m-d H:i:s';
    const START_YEAR = '-01-01 00:00:00';
    const LIMIT = 90;

    public function findAvailableReports()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf')
            ->from('NKOOrderBundle:Report\ReportForm', 'rf')
            ->where(':current_date between rf.startDate and rf.finishDate')
            ->setParameter('current_date', new \DateTime())
            ->getQuery()
            ->getResult();
    }

    public function findAvailableReportsByCompetitionIds($ids)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf')
            ->from('NKOOrderBundle:Report\ReportForm', 'rf')
            ->where(':current_date between rf.startDate and rf.finishDate')
            ->andWhere('rf.competition in (:ids)')
            ->setParameters([
                'current_date' => new \DateTime(),
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAvailableReportsByUser($user)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf')
            ->from('NKOOrderBundle:Report\ReportForm', 'rf')
            ->innerJoin('rf.competition', 'c')
            ->innerJoin('a', 'NKOOrderBundle:ApplicationHistory', Join::WITH, 'a.competitionId = c.id')
            ->where('a.isSend = 1')
            ->andWhere('a.author = :author')
            ->andWhere(':current_date between rf.startDate and rf.finishDate')
            ->setParameters([
                'current_date' => new \DateTime(),
                'author' => $user
            ])
            ->getQuery()
            ->getResult();
    }

    public function getCurrentReportFormByReportClass($reportClass)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('rf')
            ->from(ReportForm::class, 'rf')
            ->where(':current_date between rf.startDate and rf.finishDate')
            ->andWhere('rf.reportClass = :reportClass')
            ->setParameters([
                'current_date' => new \DateTime(),
                'reportClass' => $reportClass,
            ])
            ->getQuery()
            ->getResult()
            ;
    }

    public function getReportFormByYear($year, $reportClass)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf.id, rf.title')
            ->from(ReportForm::class, 'rf')
            ->where('rf.year = :year')
            ->andWhere('rf.reportClass = :reportClass')
            ->setParameters([
                'year' => $year,
                'reportClass' => $reportClass,
            ])
            ->getQuery()
            ->getResult();
    }

    public function getYearsByReportClass($reportClass, $lastYear = null)
    {
        $params = [
            'reportClass' => $reportClass,
        ];

        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf.year')
            ->from(ReportForm::class, 'rf')
            ->where('rf.reportClass = :reportClass')
            ->distinct(true)
            ->orderBy('rf.year', 'DESC')
        ;

        if ($lastYear) {
            $params['lastYear']  = $lastYear;
            $qb->andWhere('rf.year <= :lastYear');
        }

        return $qb
            ->setParameters($params)
            ->getQuery()
            ->getResult();
    }

    public function getCompetitionsWithReportByReportClass($class, $except = [], $limit = self::LIMIT)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf.id as reportFormId, rf.year, c.applicationClass, c.name, rf.title')
            ->from(ReportForm::class, 'rf')
            ->leftJoin('rf.competition', 'c')
            ->where('rf.reportClass = :class')
            ->andWhere('rf.deletedAt is null')
            ->andWhere('rf.id <= :limitId')
            ->andWhere('rf.id not in (:excepted)')
            ->orderBy('rf.year', 'ASC')
            ->orderBy('c.applicationClass', 'ASC')
            ->setParameters([
                'class' =>  $class,
                'excepted' =>  $except,
                'limitId' =>  $limit,
                ])
            ->getQuery()
            ->getResult()
            ;
    }

    public function getByYearAndCompetition($yearFrom, $yearTo, $competitions)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('
                rf.id
             ')
            ->from(ReportForm::class, 'rf')
            ->leftJoin(Competition::class, 'c', 'WITH', 'rf.competition = c.id')
            ->where("
                c.applicationClass in (:competitions)
                and rf.year between :yearFrom and :yearTo
                and rf.reportClass = :reportClass
            ")
            ->setParameters([
                'yearFrom' => \DateTime::createFromFormat(self::FORMAT, $yearFrom.self::START_YEAR),
                'yearTo' => \DateTime::createFromFormat(self::FORMAT, $yearTo.self::START_YEAR),
                'competitions' => $competitions,
                'reportClass' => MonitoringReport::class,
            ])
            ->getQuery()
            ->getResult()
        ;
    }
}
