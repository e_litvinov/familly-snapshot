<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\PeriodReport;

class PeriodReportRepository extends EntityRepository
{
    public function findNextReportPeriods($object)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from(PeriodReport::class, 'p')
            ->where(':reportForm MEMBER OF p.reportForms')
            ->andWhere('p.startDate > :currentPeriodDate')
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $object->getReportForm(),
                'currentPeriodDate' => $object->getPeriod()->getStartDate()
            ])
            ->getResult();
    }

    public function findPastReportPeriods($object)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from(PeriodReport::class, 'p')
            ->where(':reportForm MEMBER OF p.reportForms')
            ->andWhere('p.startDate < :currentPeriodDate')
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $object->getReportForm(),
                'currentPeriodDate' => $object->getPeriod()->getStartDate()
            ])
            ->getResult();
    }

    public function findReportPeriods($reportFromId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from(PeriodReport::class, 'p')
            ->where(':reportForm MEMBER OF p.reportForms')
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $reportFromId,
            ])
            ->getResult();
    }

    public function getPeriodValues($reportFromId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p. id, p.name')
            ->from(PeriodReport::class, 'p')
            ->where(':reportForm MEMBER OF p.reportForms')
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $reportFromId,
            ])
            ->getResult();
    }

    public function getFirstPeriodByReportForm($reportFromId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from(PeriodReport::class, 'p')
            ->where(':reportForm MEMBER OF p.reportForms')
            ->orderBy('p.finishDate', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->setParameters([
                'reportForm' => $reportFromId,
            ])
            ->getOneOrNullResult();
    }
}
