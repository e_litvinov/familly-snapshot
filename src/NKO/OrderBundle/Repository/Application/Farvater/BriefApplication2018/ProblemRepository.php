<?php

namespace NKO\OrderBundle\Repository\Application\Farvater\BriefApplication2018;

use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;

class ProblemRepository extends \Doctrine\ORM\EntityRepository
{
    public function getProblem($keyToFind, $applicationId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')
            ->from(Problem::class, 'o')
            ->where('o.' . $keyToFind . ' = ' . (string)$applicationId)
            ->getQuery()
            ->getResult();
    }
}
