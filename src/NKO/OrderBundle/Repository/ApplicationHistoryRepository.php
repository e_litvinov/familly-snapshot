<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Competition;

class ApplicationHistoryRepository extends EntityRepository
{
    public function findByLastCreatedAt($competitionId)
    {
        $query =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('MAX(sub.createdAt)')
            ->andWhere('sub.isSend = 1')
            ->andWhere('sub.competition = :competitionId')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'sub')
            ->setParameter('competitionId', $competitionId)
            ->groupBy('sub.author')
        ;

        return $this->getCreatedAtResult($query, $competitionId);
    }















    public function findByIsAppropriateCreatedAt($competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->andWhere('a.isSend = 1')
            ->andWhere('a.competition = :competitionId')
            ->andWhere('a.isSpread = 1')
            ->andWhere('a.isAppropriate = 1')
            ->setParameter('competitionId', $competitionId)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByIsSpreadByCompetition($competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->andWhere('a.isSend = 1')
            ->andWhere('a.isSpread = 1')
            ->andWhere('a.competition = :competitionId')
            ->setParameter('competitionId', $competitionId)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findSpreadByCompetition($competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->andWhere('a.isSpread = 1')
            ->andWhere('a.competition = :competitionId')
            ->setParameter('competitionId', $competitionId)
            ->getQuery()
            ->getResult()
            ;
    }

    private function getCreatedAtResult($query, $competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->where('a.createdAt IN ('.$query->getDQL().')')
            ->andWhere('a.competition = :competitionId')
            ->getQuery()
            ->setParameters(array('competitionId' => $competitionId))
            ->getResult();
    }

    public function findReadyToSpreadByCompetition($competitionId)
    {

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->andWhere('a.isSend = 1')
            ->andWhere('a.isAppropriate = 1')
            ->andWhere('a.isSpread = 0')
            ->andWhere('a.competition = :competitionId')
            ->setParameter('competitionId', $competitionId)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findMaxNumber($competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('MAX(a.number)')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->where('a.isSpread = 1')
            ->andWhere('a.competition = '.$competitionId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findSpreadedByPsrns($psrns, $competitionId)
    {
        $query =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->andWhere('a.isSpread = 1')
            ->andWhere('a.competition = ' . $competitionId);

        if (count($psrns) > 1) {
            $stringPsrns = implode(',', $psrns);
            $query
                ->andWhere('a.psrn in (' . $stringPsrns . ')');
        } else {
            $query
                ->andWhere('a.psrn = ' . $psrns);
        }

        return $query
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;
    }

    public function getSpreadApplicationHistory($reportForm, $user)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->innerjoin('a.competition', 'c')
            ->where('a.isSpread = :isSpread')
            ->andWhere('a.author = :author')
            ->andWhere(':reportForm MEMBER OF c.reportForms')
            ->setParameters([
                'reportForm' => $reportForm,
                'isSpread' => true,
                'author' => $user,
            ])
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findNotUsedByCompetition(Competition $competition, $isSend, $isSpread)
    {
        $query =  $this->getEntityManager()->createQueryBuilder()
            ->select('partial a.{id, username, number, isSend, isSpread}')
            ->from(ApplicationHistory::class, 'a')
            ->leftJoin('a.grants', 'g', Join::WITH, 'g.applicationHistory = a.id')
            ->leftJoin('a.markLists', 'm', Join::WITH, 'm.application = a.id')
            ->leftJoin('a.experts', 'e')
            ->leftJoin('a.followingApplications', 'f', Join::WITH, 'f.linkedApplicationHistory = a.id')
            ->leftJoin('a.reports', 'r', Join::WITH, 'r.applicationHistory = a.id')
            ->leftJoin('a.reportTemplates', 'rt', Join::WITH, 'rt.applicationHistory = a.id')
            ->where('g.applicationHistory is not null')
            ->orWhere('m.application is not null')
            ->orWhere('f.linkedApplicationHistory is not null')
            ->orWhere('r.applicationHistory is not null')
            ->orWhere('rt.applicationHistory is not null')
            ->andWhere('a.competition = :competition')
            ->andWhere('a.isSend = :isSend')
            ->andWhere('a.isSpread = :isSpread')
            ->setParameters([
                'competition' => $competition->getId(),
                'isSend' => $isSend,
                'isSpread' => $isSpread
            ])
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param string $applicationClassName
     *
     * @return mixed
     */
    public function findByNameOfApplication($applicationClassName)
    {
        $query =  $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->innerJoin('a.competition', 'c')
            ->where("a.isSend = true")
            ->orWhere("a.isSpread = true")
            ->andWhere("c.applicationClass = '".$applicationClassName."'")
        ;

        return  $query->getQuery()->getResult();
    }

    public function findSendOrSpreadApplication($competition, $isSend = true, $isSpread = true)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->where('a.isSend = :isSend AND a.competition = :competition')
            ->orWhere('a.isSpread = :isSpread AND a.competition = :competition')
            ->setParameters([
                'competition' => $competition,
                'isSpread' => $isSend,
                'isSend' => $isSpread
            ])
            ->getQuery()
            ->getResult();
    }

    public function findSendByAuthor($author, $competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->andWhere('a.isSend = 1')
            ->andWhere('a.author = :author')
            ->andWhere('a.isAppropriate = 1')
            ->andWhere('a.competition = :competitionId')
            ->setParameters([
                'author' => $author,
                'competitionId' => $competitionId
            ])
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findSpreadByAuthor($author, $competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->andWhere('a.isSpread = 1')
            ->andWhere('a.author = :author')
            ->andWhere('a.competition = :competitionId')
            ->setParameters([
                'author' => $author,
                'competitionId' => $competitionId
            ])
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findSendByCompetitions($competitions)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->andWhere('a.isSend = 1')
            ->andWhere('a.competition in (:competitions)')
            ->setParameters([
                'competitions' => $competitions
            ])
            ->orderBy('a.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByIds($ids)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->andWhere('a.id in (:ids)')
            ->setParameters([
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult()
            ;
    }

    public function findLastManualSubmittedHistory(BaseApplication $object, $checkAutosaved = true)
    {
        $query =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->andWhere('a.author = :author')
            ->andWhere('a.competition = :competition');

        if ($checkAutosaved) {
            $query->andWhere('a.isAutosaved = 0');
        }

        return $query
            ->setParameters([
                'author' => $object->getAuthor(),
                'competition' => $object->getCompetition()
            ])
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
}
