<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\MarkListHistory;

class MarkListHistoryRepository extends EntityRepository
{
    public function findSentMarkListHistory($markList)
    {
        return $this->getEntityManager()->getRepository(MarkListHistory::class)->findOneBy([
            'isSend' => true,
            'competition' => $markList->getApplication()->getCompetition()->getId(),
            'author' => $markList->getApplication()->getAuthor(),
            'expert' => $markList->getExpert()->getId()
        ]);
    }

    public function findByMarkList(MarkList $markList)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('m')
            ->from(MarkListHistory::class, 'm')
            ->andWhere('m.expert = :expert')
            ->andWhere('m.applicationHistory = :application')
            ->setParameters([
                'expert'=> $markList->getExpert(),
                'application'=> $markList->getApplication(),
            ])
            ->getQuery()
            ->getResult();
    }

    public function getSentMarkListHistories($application, $experts)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('m')
            ->from(MarkListHistory::class, 'm')
            ->where('m.isSend =:isSend')
            ->andWhere('m.competition =:competition')
            ->andWhere('m.author =:author')
            ->andWhere('m.expert IN (:experts)')
            ->setParameters([
                'isSend'=> true,
                'competition'=> $application->getCompetition()->getId(),
                'author' => $application->getAuthor()->getId(),
                'experts'=> $experts,
            ])
            ->getQuery()
            ->getResult();
    }
}
