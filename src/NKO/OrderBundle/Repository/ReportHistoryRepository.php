<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as FinanceReport2019;
use NKO\OrderBundle\Entity\ReportHistory;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Entity\Autosave\ReportAdditionalFields;

class ReportHistoryRepository extends EntityRepository
{
    const MAX_REPORT_FORM_ID = 100;

    public function findSendByLastCreatedAt()
    {
        $query =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('MAX(sub.createdAt)')
            ->andWhere('sub.isSend = 1')
            ->from('NKO\OrderBundle\Entity\ReportHistory', 'sub')
            ->groupBy('sub.psrn')
        ;

        return $this->getCreatedAtResult($query);
    }

    public function findSendByLastCreatedAtByReportForm($reportFormId)
    {
        $query =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('MAX(sub.createdAt)')
            ->andWhere('sub.isSend = 1')
            ->andWhere('sub.reportForm = ' . $reportFormId)
            ->from('NKO\OrderBundle\Entity\ReportHistory', 'sub')
            ->groupBy('sub.psrn')
        ;

        return $this->getCreatedAtResult($query);
    }

    private function getCreatedAtResult($query)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('partial h.{id, isSend}')
            ->from('NKO\OrderBundle\Entity\ReportHistory', 'h')
            ->where('h.createdAt IN ('.$query->getDQL().')')
            ->getQuery()
            ->getResult();
    }

    public function findByReport(BaseReport $object, $isSend = false)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('h')
            ->from(ReportHistory::class, 'h')
            ->where('h.psrn = :psrn')
            ->andWhere('h.reportForm = :reportForm')
            ->setParameters([
                'psrn' => $object->getPsrn(),
                'reportForm' => $object->getReportForm(),
            ]);

        if ($object->getPeriod()) {
            $qb
                ->andWhere('h.period = :period')
                ->setParameter('period', $object->getPeriod());
        } else {
            $qb
                ->andWhere('h.period is null');
        }
        if ($isSend) {
            $qb->andWhere('h.isSend = 1');
        }

        return $qb->getQuery()->getResult();
    }

    public function findByReportWithLimitAndOffset(BaseReport $object, $isSend = false, $limit = null, $offset = 0)
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('h')
            ->from(ReportHistory::class, 'h')
            ->where('h.psrn = :psrn')
            ->andWhere('h.reportForm = :reportForm')
            ->setParameters([
                'psrn' => $object->getPsrn(),
                'reportForm' => $object->getReportForm(),
            ]);

        if ($object->getPeriod()) {
            $qb
                ->andWhere('h.period = :period')
                ->setParameter('period', $object->getPeriod());
        } else {
            $qb
                ->andWhere('h.period is null');
        }
        if ($isSend) {
            $qb->andWhere('h.isSend = 1');
        }
        if ($limit) {
            $qb->setMaxResults($limit);
        }
        if ($offset) {
            $qb->setFirstResult($offset);
        }

        return $qb->getQuery()->getResult();
    }

    public function findByAuthorsAndReportFormsSendReportHistories($reportForms, $psrns, $reportClass)
    {
        $arr = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r.id')
            ->from(ReportHistory::class, 'r')
            ->leftJoin('r.reportForm', 'rf')
            ->leftJoin('rf.competition', 'c')
            ->where('c.applicationClass in (:reportForm)')
            ->andwhere('rf.reportClass = :reportClass')
            ->andwhere('r.psrn in (:psrns)')
            ->andWhere('r.isSend = :isSend')
            ->andWhere('rf.id <= :maxReportFormId')
            ->setParameters([
                'reportForm' => $reportForms,
                'psrns' => $psrns,
                'isSend' => true,
                'reportClass' => $reportClass,
                'maxReportFormId' => self::MAX_REPORT_FORM_ID,
            ])
            ->getQuery()
            ->getResult();

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r')
            ->from(ReportHistory::class, 'r')
            ->where('r.id in (:ids)')
            ->setParameters([
                'ids' => $arr,
            ])
            ->getQuery()
            ->getResult();
    }


    public function findLastManualSubmittedHistory(BaseReport $object, $checkAutosaved = true)
    {
        $options = [
            'author' => $object->getAuthor()->getPsrn(),
            'reportForm' => $object->getReportForm()
        ];

        $period = $object->getPeriod();
        if ($period) {
            $options = array_merge($options, ['period' => $period]);
        }

        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(ReportHistory::class, 'a')
            ->innerJoin(ReportAdditionalFields::class, 'ar', Join::WITH, 'a.id = ar.reportHistory')
            ->andWhere('a.psrn = :author')
            ->andWhere('a.reportForm = :reportForm');

        if ($checkAutosaved) {
            $query->andWhere('ar.isAutosaved = 0');
        }
        if ($period) {
            $query->andWhere('a.period = :period');
        }

        return $query
            ->setParameters($options)
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByPsrnAndReportForm($psrn, $reportForm)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rh')
            ->from(ReportHistory::class, 'rh')
            ->andWhere('rh.psrn = :author')
            ->andWhere('rh.reportForm = :reportForm')
            ->andWhere('rh.isSend = 1')
            ->setParameters([
                'author' => $psrn,
                'reportForm' => $reportForm,
            ])
            ->getQuery()
            ->getResult()
        ;
    }

    public function findIdsByReportClass($reportClass)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rh.id')
            ->from(ReportHistory::class, 'rh')
            ->leftJoin('rh.reportForm', 'rf')
            ->andWhere('rf.reportClass = :reportClass')
            ->andWhere('rh.isSend = 1')
            ->setParameters([
                'reportClass' => $reportClass,
            ])
            ->getQuery()
            ->getResult()
            ;
    }
    public function getIsAcceptedByPsrnAndReportFormAndPeriod($psrn, $reportForm, $period)
    {
        return $this->createQueryBuilder('rh')
            ->select('max(rh.isAccepted)')
            ->andWhere('rh.psrn = :psrn')
            ->andWhere('rh.reportForm = :reportForm')
            ->andWhere('rh.period = :period')
            ->setParameters([
                'psrn' => $psrn,
                'reportForm' => $reportForm,
                'period' => $period,
            ])
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findFinanceIsAcceptedByPsrn($psrn, $reportForm)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rh.id, rh.isAccepted')
            ->from(ReportHistory::class, 'rh')
            ->leftJoin('rh.reportForm', 'rf')
            ->andWhere('rh.psrn = :psrn')
            ->andWhere('rf.reportClass in (:financeReport, :financeReport2018, :financeReport2019)')
            ->andWhere('rf.id = :reportForm')
            ->orderBy('rh.id', 'ASC')
            ->setParameters([
                'psrn' => $psrn,
                'reportForm' => $reportForm,
                'financeReport' => FinanceReport::class,
                'financeReport2018' => FinanceReport2018::class,
                'financeReport2019' => FinanceReport2019::class,
            ])
            ->getQuery()
            ->getResult()
            ;
    }
}
