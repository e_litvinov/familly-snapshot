<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.8.18
 * Time: 11.13
 */

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\ExpenseType;

class ExpenseTypeRepository extends EntityRepository
{
    public function findExpenseTypesByReportForm($reportForm)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from(ExpenseType::class, 'e')
            ->innerJoin('e.expenseTypeConfig', 'n')
            ->where('n.reportForm = :reportForm')
            ->orderBy('n.number', 'ASC')
            ->getQuery()
            ->setParameters([
                'reportForm' => $reportForm,
            ])
            ->getResult();
    }
}
