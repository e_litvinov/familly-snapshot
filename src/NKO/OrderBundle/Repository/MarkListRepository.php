<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.7.18
 * Time: 18.38
 */

namespace NKO\OrderBundle\Repository;

use NKO\OrderBundle\Entity\MarkList;
use Doctrine\ORM\EntityRepository;

class MarkListRepository extends EntityRepository
{
    public function findMarksByExpert($competitionId, $experts)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('m')
            ->from(MarkList::class, 'm')
            ->innerJoin('m.expert', 'e')
            ->innerJoin('m.application', 'a')
            ->innerJoin('a.competition', 'c')
            ->where('m.expert IN (:experts)')
            ->andWhere('a.competition  = :competition')
            ->setParameters(['competition'=>$competitionId, 'experts'=>$experts])
            ->getQuery()
            ->getResult();
    }
}