<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\MarkCriteria;

class MarkCriteriaRepository extends EntityRepository
{
    public function findByCompetition($competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from(MarkCriteria::class, 'c')
            ->innerJoin('c.questions', 'q')
            ->where(':competition MEMBER OF q.competitions')
            ->setParameters(['competition' => $competitionId])
            ->getQuery()
            ->getResult();
    }
}