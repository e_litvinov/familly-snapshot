<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\ReportForm;

class BaseReportRepository extends EntityRepository
{
    public function findReportsByLinkedReportForm(BaseReport $report)
    {
        $form = $report->getReportForm();
        $author = $report->getAuthor();

        return  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r')
            ->from(BaseReport::class, 'r')
            ->join('r.reportForm', 'f')
            ->where('f.linkedForm = :form')
            ->andWhere('r.author = :author')
            ->setParameters([
                'form' => $form,
                'author' => $author
            ])
            ->getQuery()->getResult();
    }

    public function findReportsByReportForm($form)
    {
        return  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r')
            ->from(BaseReport::class, 'r')
            ->where('r.reportForm = :form')
            ->setParameters([
                'form' => $form,
            ])
            ->getQuery()->getResult();
    }
}
