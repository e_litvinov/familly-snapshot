<?php


namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ReportRepository extends EntityRepository
{
    //@toDo выяснить, где используются методы, изменить их и перенести в ApplicationHistoryRepository
    public function findByLastCreatedAt($username)
    {
        $query =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('MAX(sub.createdAt)')
            ->andWhere('sub.isSend = 1')
            ->andWhere("sub.username = '". $username . "'")
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'sub')
            ->groupBy('sub.username')
        ;

        return $this->getCretedAtResult($query);
    }

    private function getCretedAtResult($query)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from('NKO\OrderBundle\Entity\ApplicationHistory', 'a')
            ->where('a.createdAt IN ('.$query->getDQL().')' )
            ->getQuery()
            ->getResult();
    }

}