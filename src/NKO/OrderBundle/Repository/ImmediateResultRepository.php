<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01.11.16
 * Time: 16:09
 */

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ImmediateResultRepository extends EntityRepository
{
    public function findByCriteriaFromAccepted($criteria)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('res.planValue', 'res.factValue', 'res.expectedValue')
            ->from('NKO\OrderBundle\Entity\ImmediateResult', 'res')
            ->innerJoin('res.report','r')
            ->where('r.isAccepted = 1')
            ->andWhere('res.resultCriteria = :criteria')
            ->setParameter('criteria', $criteria)
            ->getQuery()
            ->getResult()
        ;
    }
}