<?php

namespace NKO\OrderBundle\Repository\KNS2017;

use NKO\OrderBundle\Entity\KNS2017\SocialResult;
use NKO\OrderBundle\Resolver\SortedFixtureValuesResolver;

/**
 * SocialResultRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SocialResultRepository extends \Doctrine\ORM\EntityRepository
{
    public function findCachedByApplicationTypeQuery($type)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')
            ->from(SocialResult::class, 'o')
            ->where('o.applicationType like :type')
            ->setParameter('type', '%' . $type. '%')
            ->getQuery()
            ->useResultCache(true);
    }

    public function findCachedByApplicationTypeData($type)
    {
        $results = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')
            ->from(SocialResult::class, 'o')
            ->where('o.applicationType like :type')
            ->setParameter('type', '%' . $type. '%')
            ->getQuery()
            ->useResultCache(true)
            ->getResult();

        return SortedFixtureValuesResolver::sortResultsByType($results, $type, 'applicationType');
    }
}
