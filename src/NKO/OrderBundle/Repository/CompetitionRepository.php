<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 26.8.16
 * Time: 12.19
 */

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\QuestionMark;

class CompetitionRepository extends EntityRepository
{
    public function findCurrentCompetitions($competitionClass = null, $date = null)
    {
        $parameters = [];
        if (!$date) {
            $date = new \DateTime();
        }
        $competitionQuery = $this->getEntityManager()
            ->createQueryBuilder("competition")
            ->select('c')
            ->from(Competition::class, 'c')
            ->where(':current_date between c.start_date and c.finish_date');
            $parameters['current_date'] = $date->format('Y-m-d H:i:s');

        if ($competitionClass) {
            $competitionQuery
                ->andWhere('c.applicationClass = :competitionClass');
            $parameters['competitionClass'] = $competitionClass;
        }
         return $competitionQuery->setParameters($parameters)->getQuery()->getResult();
    }

    public function isCurrentCompetition($competition)
    {
        if (in_array($competition, $this->findCurrentCompetitions())) {
            return true;
        }

        return false;
    }

    public function getAvaliableUserCompetitions($user)
    {
        $date = new \DateTime();
        $competitionQuery = $this->getEntityManager()
            ->createQueryBuilder("competition")
            ->select('c')
            ->from(Competition::class, 'c')
            ->join('NKO\OrderBundle\Entity\Application', 'a', 'a.competition')
            ->andWhere('a.author <> :userId')
            ->andWhere(':currentDate between c.start_date and c.finish_date')
            ->setParameters(['userId' => $user->getId(), 'currentDate' => $date->format('Y-m-d H:i:s')]);

        return $competitionQuery->getQuery()->getResult();
    }

    public function findCountAvailiableUserCompetition($currentUser)
    {
        $subquery = $this->getEntityManager()->createQueryBuilder()
            ->select('competition.id')
            ->from("NKOOrderBundle:Competition", 'competition')
            ->leftJoin('NKOOrderBundle:BaseApplication', 'application', 'WITH', 'competition.id = application.competition')
            ->where('application.author = :author')
            ->getDQL();

        $query  = $this->getEntityManager()->createQueryBuilder();
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('COUNT(comp)')
            ->from('NKOOrderBundle:Competition', 'comp')
            ->where($query->expr()->notIn('comp.id', $subquery))
            ->andWhere('comp.finish_date > :date');
        $count = $query
            ->setParameters(['author' => $currentUser, 'date' => new \DateTime()])
            ->getQuery()
            ->getSingleScalarResult();

        return $count;
    }

    public function getAvailiableCompetition($currentUser)
    {
        $subquery = $this->getEntityManager()->createQueryBuilder()
            ->select('competition.id')
            ->from("NKOOrderBundle:Competition", 'competition')
            ->leftJoin('NKOOrderBundle:BaseApplication', 'application', 'WITH', 'competition.id = application.competition')
            ->where('application.author = :author')
            ->getDQL();

        $query  = $this->getEntityManager()->createQueryBuilder();
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('comp')
            ->from('NKOOrderBundle:Competition', 'comp')
            ->where($query->expr()->notIn('comp.id', $subquery))
            ->andWhere('comp.finish_date > :date');
        $avaliableCompetition = $query
            ->setParameters(['author' => $currentUser, 'date' => new \DateTime()])
            ->getQuery()
            ->getResult();

        return $avaliableCompetition;
    }

    public function findLastHeldCompetition()
    {
        $competitions = $this->createQueryBuilder('c')
            ->where('c.finish_date < :today')
            ->orderBy('c.finish_date', 'DESC')
            ->setParameter('today', new \DateTime())
            ->getQuery()
            ->getResult();
        return reset($competitions);
    }

    public function findCompetitionsForEstimate()
    {
        $competitions = $this->createQueryBuilder('c')
            ->where('c.finish_date < :today')
            ->andWhere('c.finishEstimate > :today')
            ->orderBy('c.finish_date', 'DESC')
            ->setParameter('today', new \DateTime())
            ->getQuery()
            ->getResult();

        return $competitions;
    }

    public function getAvaliableNkoCompetitions($user)
    {
        $parameters = [];
            $date = new \DateTime();
        $competitionQuery = $this->getEntityManager()
            ->createQueryBuilder("competition")
            ->select('c')
            ->from('NKO\OrderBundle\Entity\Competition', 'c')
            ->where(':current_date between c.start_date and c.finish_date');
        $parameters['current_date'] = $date->format('Y-m-d H:i:s');

        $results =  $competitionQuery
            ->setParameters($parameters)
            ->getQuery()
            ->getResult()
        ;
        $competitions = [];
        /** @var Competition $result */
        foreach ($results as $result) {
            if ($result->isAllowedToUSer($user)) {
                $competitions [] = $result;
            }
        }
        $competitions = $this->checkCompetitionsOnBannedReportForm($competitions, $user);

        return $competitions;
    }

    private function checkCompetitionsOnBannedReportForm($competitions, $user)
    {
        $totalCompetitions = [];
        /** @var Competition $competition */
        foreach ($competitions as $competition) {
            $bannedReportForms = $competition->getBannedReportForms();
            $isSumGrunt = $this->checkReportForms($bannedReportForms, $user);
            $isBannedAccess = $competition->isOrganizationsAccess();

            $notInBan = $isBannedAccess && !$isSumGrunt;
            $isAllowed = !$isBannedAccess && (!count($bannedReportForms) || $isSumGrunt);

            if ($notInBan || $isAllowed) {
                $totalCompetitions[] = $competition;
            }
        }

        return $totalCompetitions;
    }

    /**
     * @param $reportForms array
     * @param $user NKOUser
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkReportForms($reportForms, $user)
    {
        return (bool) count(
            $this->getEntityManager()->createQueryBuilder()
            ->select('g.id')
            ->from(GrantConfig::class, 'g')
            ->leftJoin('g.applicationHistory', 'a')
            ->where('a.author = :author')
            ->andWhere('g.reportForm IN (:reportForms)')
            ->andWhere('g.sumGrant is not null')
            ->groupBy('g.id')
            ->setParameters([
                'author' => $user,
                'reportForms' => $reportForms,
            ])
            ->getQuery()->getResult()
        );
    }

    /**
     * Разрешаем, если нет организаций или одно из двух (не входит в запрещённые| входит в разрешённые)
     *
     * @param $competition Competition
     * @param $user NKOUser
     *
     * @return bool
     *
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isCompetitionAllowedByReports($competition, $user)
    {
        $sumGrantOfBannedReports = $this->checkReportForms($competition->getBannedReportForms(), $user);

        return !count($competition->getBannedReportForms()) ||
            (
                ($competition->isOrganizationsAccess() && !$sumGrantOfBannedReports) ||
                (!$competition->isOrganizationsAccess() && $sumGrantOfBannedReports)
            );
    }

    public function getCompetitionsByIds($ids)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c')
            ->from(Competition::class, 'c')
            ->andWhere('c.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function getCompetitionsByMarkQuestions()
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('c.id')
            ->from(QuestionMark::class, 'q')
            ->innerJoin('q.competitions', 'c')
            ->distinct(true)
            ->getQuery()
            ->getResult();
    }
}
