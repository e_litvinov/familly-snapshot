<?php

namespace NKO\OrderBundle\Repository\Report\MonitoringReport;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;

class IndicatorRepository extends EntityRepository
{
    public function createQueryBuilder($alias, $indexBy = null)
    {
        $qb = parent::createQueryBuilder($alias, $indexBy);

        $newMonitoringIndicatorType = IndicatorInterface::NEWMONITORINGINDICATORTYPE;
        return $qb->andWhere("{$alias}.monitoringType != '{$newMonitoringIndicatorType}' or {$alias}.monitoringType is null");
    }

    public function findAllExceptCode($code)
    {
        return $this->createQueryBuilder('i')
            ->where('i.code is null')
            ->orWhere('i.code != :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findAllExceptCodeAndMonitoringType($code, $monitoringType)
    {
        return parent::createQueryBuilder('i')
            ->andWhere('(i.code is null or i.code != :code)')
            ->andWhere('i.monitoringType = :monitoringType')
            ->setParameter('code', $code)
            ->setParameter('monitoringType', $monitoringType)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findExceptTotal()
    {
        $totalIndicator = $this->findOneBy(['code' => IndicatorInterface::TOTAL_CODE]);
        return $this->createQueryBuilder('i')
            ->where('i.parent != :parent')
            ->andWhere('i.code not in (:arr)')
            ->orWhere('i.code is null')
            ->setParameter(
                'arr',
                array(IndicatorInterface::TOTAL_CODE, IndicatorInterface::CUSTOM_VALUE),
                \Doctrine\DBAL\Connection::PARAM_INT_ARRAY
            )
            ->setParameter('parent', $totalIndicator->getId())
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByCodeWithoutCustom($code)
    {
        return $this->createQueryBuilder('r')
            ->select('c')
            ->innerJoin(Indicator::class, 'p', Join::WITH, 'r.parent = p.id')
            ->innerJoin(Indicator::class, 'c', Join::WITH, 'c.parent = r.id')
            ->where('p.code = :code')
            ->andwhere('c.code != :code_child OR c.code is NUll')
            ->setParameters(array('code' => $code, 'code_child' => 'custom'))
            ->getQuery()
            ->getResult();
    }

    public function findByEffectivenessIndicatorKNSSecondStage($code)
    {
        return $this->createQueryBuilder('a')
            ->select('c', 'b', 'd', 'g')
            ->innerJoin(Indicator::class, 'b', Join::WITH, 'b.parent = a.id')
            ->innerJoin(Indicator::class, 'c', Join::WITH, 'c.parent = b.id')
            ->innerJoin(Indicator::class, 'd', Join::WITH, 'd.parent = a.id')
            ->innerJoin(Indicator::class, 'f', Join::WITH, 'f.parent = a.id')
            ->innerJoin(Indicator::class, 'g', Join::WITH, 'g.parent = f.id')
            ->where('a.code = :oneCode AND  (c.type = :oneType)')
            ->andWhere('a.code = :oneCode AND d.code = :threeCode AND f.code = :threeCode AND f.indexNumber= :num')
            ->setParameters(
                [
                    'oneCode' => $code,
                    'oneType' => 'social',
                    'threeCode'=>'family',
                    'num'=> 11
                ]
            )
            ->getQuery()
            ->getResult();
    }

    public function findBYType($type)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.analyticTypes like :type')
            ->setParameter('type', '%'.$type.'%')
            ->getQuery()
            ->getResult();
    }
}
