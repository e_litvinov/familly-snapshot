<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 5.8.18
 * Time: 0.33
 */

namespace NKO\OrderBundle\Repository\Report\MonitoringReport;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use Doctrine\Common\Collections;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\Report\ReportForm;

class MonitoringResultRepository extends EntityRepository
{
    const FORMAT = 'Y-m-d H:i:s';
    const START_YEAR = '-01-01 00:00:00';

    public function findMonitoringResults($reportId)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder('m')
            ->select('m', 'i', 'r', 'lm', 'cm')
            ->from(MonitoringResult::class, 'm')
            ->innerJoin('m.indicator', 'i')
            ->leftJoin('m.periodResults', 'r')
            ->leftJoin('i.parent', 'p')
            ->leftJoin('m.linkedMethods', 'lm')
            ->leftJoin('m.method', 'cm')
            ->where('m.report = :report')
            ->setParameter('report', $reportId)
            ->getQuery()
            ->getResult();

        return new Collections\ArrayCollection($query);
    }

    public function getSumOfPeriodicResult($ids, $type)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('
               sum(res.newAmount) as fact,
               sum(res.totalAmount) as plan,
               i.title as title,
               i.id as indicatorId,
               rf.id as reportFormId
            ')
            ->from(MonitoringResult::class, 'm')
            ->leftJoin('m.indicator', 'i')
            ->leftJoin('m.report', 'r')
            ->leftJoin('r.reportForm', 'rf')
            ->leftJoin('m.periodResults', 'res', 'WITH', 'res.isFinalResult = true')
            ->where('r.reportForm in (:ids)')
            ->andWhere('r.isSend = true')
            ->andWhere('i.analyticTypes like :type')
            ->groupBy('r.reportForm, i.id')
            ->orderBy('i.id', 'ASC')
            ->setParameters(['ids'=> $ids, 'type'=> '%'.$type.'%'])
            ->getQuery()
            ->getResult();
    }

    public function getFact($indicatorIds, $competitions, $authors)
    {
        $query = $this->getReportFormsByCometitions($competitions);
        $expr = $this->getEntityManager()->getExpressionBuilder();
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('
               sum(res.newAmount) as fact,
               i.title as title,
               rf.year as year 
            ')
            ->from(MonitoringResult::class, 'm')
            ->leftJoin('m.indicator', 'i')
            ->leftJoin('m.report', 'r')
            ->leftJoin('r.reportForm', 'rf')
            ->leftJoin('m.periodResults', 'res', 'WITH', 'res.isFinalResult = true')
            ->where(
                $expr->in(
                    'r.reportForm',
                    $query->getDQL()
                )
            )
            ->andWhere('r.isSend = true')
            ->andWhere('r.author in (:authors)')
            ->andWhere('i.id in (:indicatorIds)')
            ->groupBy('r.reportForm')
            ->groupBy('i.id')
            ->groupBy('rf.year')
            ->orderBy('i.id', 'ASC')
            ->orderBy('rf.year', 'ASC')
            ->setParameters([
                'authors'=> $authors,
                'indicatorIds'=> $indicatorIds,
                'class' =>  Report::class,
                'competition' => $competitions
            ])
            ->getQuery()
            ->getResult();
    }

    private function getReportFormsByCometitions($competitions)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('rf2.id')
            ->from(ReportForm::class, 'rf2')
            ->leftJoin('rf2.competition', 'c2')
            ->where('rf2.reportClass = :class')
            ->andWhere('rf2.deletedAt is null')
            ->andWhere('c2.applicationClass in (:competition)')
            ->orderBy('c2.applicationClass', 'ASC')
            ;
    }
}
