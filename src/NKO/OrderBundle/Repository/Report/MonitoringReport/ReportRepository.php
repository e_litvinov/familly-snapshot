<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 16.7.18
 * Time: 14.11
 */

namespace NKO\OrderBundle\Repository\Report\MonitoringReport;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\UserBundle\Entity\NKOUser;
use Doctrine\ORM\Query\Expr\Join;

class ReportRepository extends EntityRepository
{
    public function findReportsByReportForm(ReportForm $reportForm, NKOUser $author)
    {

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r')
            ->from(Report::class, 'r')
            ->where('r.reportForm = :form')
            ->andWhere('r.author = :author')
            ->setParameters([
                'form' => $reportForm,
                'author' => $author
            ])
            ->getQuery()->getOneOrNullResult();
    }

    public function findPrevReport($author, $reportForm)
    {

        return $this->getEntityManager()
            ->createQueryBuilder('r')
            ->select('r')
            ->from(Report::class, 'r')
            ->join('r.reportForm', 'rf', Join::WITH, 'rf.reportClass = :reportClass')
            ->where('r.author = :author')
            ->andWhere('rf.competition = :competition')
            ->andWhere('rf.year < :year')
            ->orderBy('rf.year', 'DESC')
            ->setParameters([
                'reportClass' => Report::class,
                'author' => $author,
                'competition' => $reportForm->getCompetition(),
                'year'=> $reportForm->getYear(),
            ])
            ->setMaxResults(1)
            ->getQuery()
            ->useResultCache(true)
            ->getOneOrNullResult();
    }
}
