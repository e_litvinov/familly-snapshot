<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 10.7.18
 * Time: 15.55
 */

namespace NKO\OrderBundle\Repository\Report\FinanceReport;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\FinanceSpent;

class FinanceSpentRepository extends EntityRepository
{
    public function getSumFinanceSpent(BaseReport $report)
    {
        return  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('SUM(f.approvedSum) as approvedSum, SUM(f.periodCosts) as periodCosts, SUM(f.balance) as balance')
            ->from(FinanceSpent::class, 'f')
            ->where ('f.report=:report')
            ->setParameters(['report'=>$report->getId()])
            ->getQuery()
            ->getOneOrNullResult();
    }
}