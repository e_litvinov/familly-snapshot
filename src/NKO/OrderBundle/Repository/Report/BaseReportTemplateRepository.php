<?php

namespace NKO\OrderBundle\Repository\Report;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;

class BaseReportTemplateRepository extends EntityRepository
{
    public function getReportTemplate ($object)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r')
            ->from(BaseReportTemplate::class, 'r')
            ->where('r.reportForm = :reportForm')
            ->andWhere('r.author = :author')
            ->getQuery()
            ->setParameters([
                'author' => $object->getAuthor(),
                'reportForm' => $object->getReportForm()
            ])
            ->getOneOrNullResult()
            ;
    }
}