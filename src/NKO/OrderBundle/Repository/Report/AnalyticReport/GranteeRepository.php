<?php

namespace NKO\OrderBundle\Repository\Report\AnalyticReport;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Grantee;

class GranteeRepository extends EntityRepository
{
    public function findGranteesByTypeData($type)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('g')
            ->from(Grantee::class, 'g')
            ->where('g.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->useResultCache(true)
            ->getResult();
    }
    public function queryFindGranteesByTypeData($type)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('g')
            ->from(Grantee::class, 'g')
            ->where('g.type = :type')
            ->setParameter('type', $type)
            ;
    }
}
