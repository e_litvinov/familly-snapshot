<?php

namespace NKO\OrderBundle\Repository\Report\AnalyticReport\Report2018;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Application\Continuation\Application;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;

class ReportRepository extends EntityRepository
{
    /**
     * @param Application $application
     */
    public function findReportsByApplication($application)
    {
        /** @var \NKO\OrderBundle\Entity\BaseApplication $continuationApplication*/
        /** @var EntityManager $em*/
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('r')
            ->from(Report::class, 'r')
            ->join('r.reportForm', 'f')
            ->where('r.author = :author')
            ->andWhere('f.competition = :competition')
            ->setParameters([
                'author' => $application->getAuthor(),
                'competition' => $application->getCompetition()
            ])
            ->getQuery()
            ->getResult();
    }

    public function findDataForAnalytics($reportForm, $item, $number)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('
                a.id AS authorId,
                a.nko_name AS author,
                r.projectName AS projectName,
                r.id AS practice,
                (CASE WHEN (exL.service IS NOT NULL) THEN exL.service ELSE exLF.service END) AS service
                ')
            ->from(Report::class, 'r')
            ->innerJoin('r.author', 'a')
            ->innerJoin('r.'.$item, 'res');

        if ($number === 4) {
            $query
                ->innerJoin('res.practice', 'pract')
                ->leftJoin('pract.linkedFarvaterResult', 'exLF')
                ->leftJoin('pract.linkedResult', 'exL')
                ->addSelect('res.comment as comment, res.administrativeSolution as administrativeSolution');
        } else {
            $query
                ->leftJoin('res.linkedFarvaterResult', 'exLF')
                ->leftJoin('res.linkedResult', 'exL')
                ->addSelect('res.isFeedback as isFeedback');
        }

        return $query
            ->where('r.reportForm = :reportForm')
            ->andWhere('r.isSend = :isSend')
            ->setParameters(['reportForm' => $reportForm, 'isSend' => true])
            ->getQuery()
            ->getResult();
    }
}
