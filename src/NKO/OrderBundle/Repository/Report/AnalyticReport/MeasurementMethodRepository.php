<?php

namespace NKO\OrderBundle\Repository\Report\AnalyticReport;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use NKO\OrderBundle\Resolver\SortedFixtureValuesResolver;

class MeasurementMethodRepository extends EntityRepository
{
    public function findMethodsByType($type)
    {
        return  $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')
            ->from(MeasurementMethod::class, 'o')
            ->where('o.type like :type')
            ->setParameter('type', '%' .$type. '%')
            ->getQuery();
    }

    public function findMethodsByTypeData($type)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')
            ->from(MeasurementMethod::class, 'o')
            ->where('o.type like :type')
            ->setParameter('type', '%' .$type. '%')
            ->getQuery()
            //->useResultCache(true)
            ->getResult();

        //return SortedFixtureValuesResolver::sortResultsByType($results, $type, 'type');
    }
}
