<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\GrantConfig;

class GrantConfigRepository extends EntityRepository
{
    public function getGrantByReport($object)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('g')
            ->from(GrantConfig::class, 'g')
            ->innerJoin('g.applicationHistory', 'a')
            ->where('g.reportForm = :reportForm')
            ->andWhere('g.applicationHistory = :applicationHistory')
            ->andWhere('a.isSpread = :isSpread')
            ->getQuery()
            ->setParameters([
                'reportForm' => $object->getReportForm(),
                'applicationHistory' => $object->getApplicationHistory(),
                'isSpread' => true
            ])
            ->getOneOrNullResult();
    }

    public function findGrantByAuthorAndReportForm($reportForm, $author)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('o')->from(GrantConfig::class, 'o')
            ->leftJoin('o.applicationHistory', 'a')
            ->where('o.reportForm = :reportForm')
            ->andWhere('a.author = :author')
            ->setParameters([
                'reportForm' => $reportForm,
                'author' => $author,
            ])
            ->getQuery()->getOneOrNullResult();
    }
}
