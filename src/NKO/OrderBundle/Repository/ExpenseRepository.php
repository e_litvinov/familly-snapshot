<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.8.18
 * Time: 11.13
 */

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\Expense;

class ExpenseRepository extends EntityRepository
{
    public function findExpensesByRegister($registers)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from(Expense::class, 'e')
            ->where ('e.register IN (:registers)')
            ->getQuery()
            ->setParameters([
                'registers' => $registers,
            ])
            ->getResult();
    }
}
