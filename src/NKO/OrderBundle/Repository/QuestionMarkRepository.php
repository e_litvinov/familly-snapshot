<?php

namespace NKO\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\QuestionMark;

class QuestionMarkRepository extends EntityRepository
{
    public function findByCompetition($competitionId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('q')
            ->from(QuestionMark::class, 'q')
            ->join('q.competitions', 'c')
            ->where('c.id = :competitionId')
            ->setParameter('competitionId', $competitionId)
            ->getQuery()
            ->getResult();
    }

    public function findByCompetitionAndCriteria($competitionId, $criteriaId)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('q')
            ->from(QuestionMark::class, 'q')
            ->innerJoin('q.competitions', 'c')
            ->innerJoin('q.criteria', 'cr')
            ->where('c.id = :competition')
            ->andWhere('cr.id = :criteria')
            ->setParameters([
                'competition' => $competitionId,
                'criteria' => $criteriaId,
            ])
            ->getQuery()
            ->getResult();
    }
}