<?php

namespace NKO\OrderBundle\Repository\MarkListSequence;

use Doctrine\ORM\EntityRepository;
use NKO\OrderBundle\Entity\MarkListSequence\Sequence;

class SequenceRepository extends EntityRepository
{
    public function findByCompetition($competition, $currentSequence = null)
    {
        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('sc')
            ->from(Sequence::class, 'sc')
            ->innerJoin('sc.competition', 'c')
            ->leftJoin('sc.relatedCompetitions', 'rc');

        if ($currentSequence) {
            $query
                ->where('sc.id NOT :currentSequence')
                ->setParameters('currentSequence', $currentSequence);
        }

        return $query
            ->where('c.id = :competitionId')
            ->orWhere('rc.id = :competitionId')
            ->setParameters([
                'competitionId' => $competition->getId(),
            ])
            ->getQuery()
            ->getResult();
    }

    public function findAllWithoutCurrent($id)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('sc')
            ->from(Sequence::class, 'sc')
            ->where('sc.id != :id')
            ->setParameters([
                'id' => $id,
            ])
            ->getQuery()
            ->getResult();
    }
}