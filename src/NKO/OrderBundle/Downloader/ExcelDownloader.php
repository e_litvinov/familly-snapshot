<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 17.03.17
 * Time: 12:16
 */

namespace NKO\OrderBundle\Downloader;

use NKO\OrderBundle\Downloader\FileDownloaderInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExcelDownloader implements FileDownloaderInterface
{
    const EXCEL5 = 'Excel5';
    const EXCEL2007 = 'Excel2007';

    /**
     * @param $filename
     * @param $viewFilename
     * @param string $format
     *
     * @return StreamedResponse
     */
    public function getStreamedResponse($filename, $viewFilename, $format = self::EXCEL5)
    {
        $response = new StreamedResponse();
        $response->setCallback(function () use ($filename) {
            echo file_get_contents($filename);
            unlink($filename);
        });

        $this->setHeaders($response, $viewFilename, $format);

        return $response;
    }

    public function getTempExcelFilename($filename)
    {
        $temp_file_name = tempnam(sys_get_temp_dir(), 'Contacts_table_');
        $temp_out_file_name = $temp_file_name.'.xls';
        file_put_contents($temp_out_file_name, $filename);
        return $temp_out_file_name;
    }

    private function setHeaders(StreamedResponse $response, $viewFilename, $format)
    {
        switch ($format) {
            case self::EXCEL5:
                $this->setHeadersExcel5($response, $viewFilename);
                break;
            case self::EXCEL2007:
                $this->setHeadersExcel2007($response, $viewFilename);
                break;
            default:
                $this->setHeadersExcel5($response, $viewFilename);
        }
    }

    public function setHeadersExcel2007(StreamedResponse $response, $viewFilename)
    {
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $viewFilename . '.xlsx";');
    }

    public function setHeadersExcel5(StreamedResponse $response, $viewFilename)
    {
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $viewFilename . '.xls";');
    }
}
