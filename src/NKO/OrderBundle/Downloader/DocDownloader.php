<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 4/3/18
 * Time: 4:04 PM
 */

namespace NKO\OrderBundle\Downloader;

use Symfony\Component\HttpFoundation\StreamedResponse;

class DocDownloader implements FileDownloaderInterface
{
    /**
     * @param $filename
     * @param $viewFilename
     * @return StreamedResponse
     */
    public function getStreamedResponse($filename, $viewFilename)
    {
        $response = new StreamedResponse();
        $response->setCallback(function () use ($filename) {
            echo file_get_contents($filename);
            unlink($filename);
        });

        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $viewFilename . '.docx";');

        return $response;
    }
}