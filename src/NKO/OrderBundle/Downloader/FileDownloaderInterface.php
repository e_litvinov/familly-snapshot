<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 4/3/18
 * Time: 3:53 PM
 */

namespace NKO\OrderBundle\Downloader;

use Symfony\Component\HttpFoundation\StreamedResponse;

interface FileDownloaderInterface
{
    /**
     * @param $filename
     * @param $viewFilename
     * @return StreamedResponse
     */
    public function getStreamedResponse($filename, $viewFilename);
}