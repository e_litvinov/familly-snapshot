<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 26.8.16
 * Time: 14.23
 */

namespace NKO\OrderBundle\Downloader;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Process\Process;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class PdfDownloader implements FileDownloaderInterface
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getStreamedResponse($filename, $viewFileName)
    {
        $temp_file_name = tempnam(sys_get_temp_dir(), 'app_');
        $temp_file_html_name = $temp_file_name.'.html';
        file_put_contents($temp_file_html_name, $filename);
        $temp_out_file_name = $temp_file_name.'.pdf';

        // Run wkhtmltopdf with x-server
        $cmd = $this->container->getParameter('server_run');
        $cmd = str_replace(['%in', '%out'], [$temp_file_html_name, $temp_out_file_name], $cmd);
        $process = new Process($cmd);
        $process->run();

        unlink($temp_file_html_name);

        if(!file_exists($temp_out_file_name)){
            return false;
        }

        $response = new BinaryFileResponse($temp_out_file_name);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $viewFileName.'.pdf');

        return $response;
    }
}