<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/25/19
 * Time: 4:41 PM
 */

namespace NKO\OrderBundle\Factory;

interface FactoryInterface
{
    public function create($object);
}
