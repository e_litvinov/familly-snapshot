<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/25/19
 * Time: 4:33 PM
 */

namespace NKO\OrderBundle\Factory;

abstract class BaseFactory implements FactoryInterface
{
    private $modifiers = [];

    public function addModifier($viewGenerator)
    {
        $this->modifiers[] = $viewGenerator;
    }

    public function create($object)
    {
        $target = get_class($object);

        foreach ($this->modifiers as $generator) {
            if (in_array($target, $generator::getTargets())) {
                $generator->setObject($object);
                return $generator;
            }
        }

        throw new \RuntimeException(self::class.' for class '.$target.' doesn\'t exist');
    }
}
