<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/25/19
 * Time: 6:45 PM
 */

namespace NKO\OrderBundle\Factory\AdminListView;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class DocumentsGeneratorPass implements CompilerPassInterface
{
    const SERVICE_NAME = 'nko_order.factory.documents_list_view';
    const TAG = 'nko_order.documents_view_generator';

    public function process(ContainerBuilder $container)
    {
        if (!$container->has(self::SERVICE_NAME)) {
            return;
        }

        $definition = $container->findDefinition(self::SERVICE_NAME);

        $taggedServices = $container->findTaggedServiceIds(self::TAG);

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addModifier', array(
                new Reference($id)
            ));
        }
    }
}
