<?php

namespace NKO\OrderBundle\Factory\AdminListView\Generator;

use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report;
use NKO\OrderBundle\Resolver\DataGenerator\AnalyticAttachmentsDataGenerator;
use NKO\OrderBundle\Utils\Report\AttachmentParameters;

class Analytic2018KNSDocumentsGenerator implements GeneratorInterface
{
    /**
     * @var Report $object
     */
    private $object;

    /** @var AnalyticAttachmentsDataGenerator */
    private $dataGeneratorService;

    public function __construct(AnalyticAttachmentsDataGenerator $dataGeneratorService)
    {
        $this->dataGeneratorService = $dataGeneratorService;
    }

    public function getTargets()
    {
        return [
            Report::class
        ];
    }

    /**
     * @param Report $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }

    public function getTemplate()
    {
        return '@NKOOrder/CRUD/report/analytic_report/attachments_list.html.twig';
    }

    public function getParameters()
    {
        $this->dataGeneratorService->setData([
            'report' => $this->object,
            'parameters' => AttachmentParameters::ATTACHMENTS_PARAMETERS_ANALYTIC_REPORT_KNS2018
        ]);

        return [
            'attachments' => $this->dataGeneratorService->generateData()
        ];
    }
}
