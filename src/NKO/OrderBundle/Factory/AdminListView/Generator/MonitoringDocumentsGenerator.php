<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/25/19
 * Time: 5:41 PM
 */

namespace NKO\OrderBundle\Factory\AdminListView\Generator;

use Doctrine\Common\Collections\Collection;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Harbor2020Report\Report as Harbor2020MonitoringReport;

class MonitoringDocumentsGenerator extends BaseGenerator implements GeneratorInterface
{
    /**
     * @var MonitoringReport $object
     */
    private $object;

    /**
     * @return Collection
     */
    public function getItems()
    {
        return $this->object->getDocuments();
    }

    public function getTargets()
    {
        return [
            MonitoringReport::class,
            Harbor2020MonitoringReport::class,
        ];
    }

    public function getItemAdmin()
    {
        $this->itemAdmin->removeListFieldDescription('batch');

        return parent::getItemAdmin();
    }

    /**
     * @param MonitoringReport $object
     */
    public function setObject($object)
    {
        $this->object = $object;
    }
}
