<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/25/19
 * Time: 5:34 PM
 */

namespace NKO\OrderBundle\Factory\AdminListView\Generator;

interface GeneratorInterface
{
    /**
     * @param $object mixed
     */
    public function setObject($object);

    /**
     * @return array
     */
    public function getTargets();

    /**
     *
     * @return string
     */
    public function getTemplate();

    /**
     *
     * @return array
     */
    public function getParameters();
}
