<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 3/27/19
 * Time: 12:42 PM
 */

namespace NKO\OrderBundle\Factory\AdminListView\Generator;

use Sonata\AdminBundle\Admin\AbstractAdmin;

abstract class BaseGenerator
{
    protected $itemAdmin;

    public function __construct(AbstractAdmin $documentAdmin)
    {
        $this->itemAdmin = $documentAdmin;
    }

    public function getTemplate()
    {
        return 'NKOOrderBundle:CRUD:custom\base_list.html.twig';
    }

    public function getItemAdmin()
    {
        $this->itemAdmin->setTemplate(
            'outer_list_rows_list',
            'NKOOrderBundle:CRUD:custom\list_outer_rows_list.html.twig'
        );

        return $this->itemAdmin;
    }

    public function getParameters()
    {
        return [
            'admin' => $this->getItemAdmin(),
            'items' => $this->getItems()
        ];
    }

    abstract public function getItems();
}
