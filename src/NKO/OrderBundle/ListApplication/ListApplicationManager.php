<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/13/17
 * Time: 10:59 AM
 */

namespace NKO\OrderBundle\ListApplication;


class ListApplicationManager
{
    private $applications;

    public function __construct()
    {
        $this->applications = array();
    }

    public function addApplicationClass($application, $alias)
    {
        $this->applications[$alias] = get_class($application);
    }

    public function getApplicationsClass()
    {
        return $this->applications;
    }

}