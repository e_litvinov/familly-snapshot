<?php
namespace NKO\OrderBundle\ListApplication;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/9/17
 * Time: 5:24 PM
 */
class ListApplicationPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('nko.list_application')) {
            return;
        }

        $definition = $container->findDefinition('nko.list_application');

        $taggedServices = $container->findTaggedServiceIds('nko.order_type');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addApplicationClass', array(
                    new Reference($id),
                    $attributes["alias"]
                ));
            }
        }
    }
}
