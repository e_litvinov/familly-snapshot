<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report as MixedReport;
use NKO\UserBundle\Entity\NKOUser;

class ChangeTraningGroundMixedReportCommand extends ContainerAwareCommand
{
    const PSRN = 1025403208830;

    protected function configure()
    {
        $this
            ->setName('nko:change-training-ground')
            ->setDescription('Change training ground')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $author = $em->getRepository(NKOUser::class)->findOneBy(['psrn' => self::PSRN]);
        $report = $em->getRepository(MixedReport::class)->findOneBy(['author' => $author]);

        $applicationHistory = $report->getApplicationHistory();
        $application = unserialize($applicationHistory->getData());
        $competition = $application->getCompetition();

        $latestApplication = $em->getRepository(BaseApplication::class)->findOneBy([
            'author' => $author,
            'competition' => $competition,
        ]);

        foreach ($application->getTrainingGrounds() as $ground) {
            $application->removeTrainingGround($ground);
        }

        foreach ($latestApplication->getTrainingGrounds() as $ground) {
            $application->addTrainingGround($ground);
        }

        $serializedObject = serialize($application);
        $applicationHistory->setData($serializedObject);

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        } catch (Exception $e) {
            $output->writeln("Error");
        }
    }
}
