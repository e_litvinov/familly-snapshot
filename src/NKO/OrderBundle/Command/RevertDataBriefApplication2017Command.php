<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use Doctrine\ORM\ORMException as Exception;

class RevertDataBriefApplication2017Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:revert-data-farvater-2017')
            ->setDescription('revert data farvater 2017');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $applications = $em->getRepository(BriefApplication::class)->findBy(['isSend'=>true]);
        $competition = $em->getRepository(Competition::class)->findOneBy(['applicationClass'=> BriefApplication::class]);
        $applicationHistories = $em->getRepository(ApplicationHistory::class)->findBy(['competition'=> $competition, 'isSend' => true]);

        $output->writeln('Begin to process');

        foreach ($applicationHistories as $history) {
            $author = $history->getAuthor();
            $application = array_filter($applications, function ($application) use ($author) {
                return $application->getAuthor() === $author;
            });

            if ($application) {
                $application = array_shift($application);
                $data = unserialize($history->getData());

                $application->setName($data->getName());
                $application->setAbbreviation($data->getAbbreviation());
                $application->setPSRN($data->getPSRN());

                $application->setLegalPostCode($data->getLegalPostCode());
                $application->setLegalRegion($data->getLegalRegion());
                $application->setLegalCity($data->getLegalCity());

                $application->setLegalStreet($data->getLegalStreet());
                $application->setIsAddressesEqual($data->getIsAddressesEqual());
                $application->setActualPostCode($data->getActualPostCode());

                $application->setActualRegion($data->getActualRegion());
                $application->setActualCity($data->getActualCity());
                $application->setActualStreet($data->getActualStreet());

                $application->setDateRegistrationOfOrganization($data->getDateRegistrationOfOrganization());
                $application->setPrimaryActivity($data->getPrimaryActivity());
                $application->setMission($data->getMission());

                $application->setCountStaffEmployees($data->getCountStaffEmployees());
                $application->setCountInvolvingEmployees($data->getCountInvolvingEmployees());
                $application->setCountVolunteerEmployees($data->getCountVolunteerEmployees());

                $application->setEmail($data->getEmail());
                $application->setPhoneCode($data->getPhoneCode());
                $application->setPhone($data->getPhone());
            }
        }

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        } catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }

        $output->writeln("Success.");
    }
}
