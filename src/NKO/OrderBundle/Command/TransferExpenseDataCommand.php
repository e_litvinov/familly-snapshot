<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.01.17
 * Time: 10:28
 */

namespace NKO\OrderBundle\Command;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\Expense;
use NKO\OrderBundle\Entity\ExpenseType;
use NKO\OrderBundle\Entity\ExpenseTypeInterface;
use NKO\OrderBundle\Entity\FinanceSpent;
use NKO\OrderBundle\Entity\Register;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\SiteLink;
use NKO\OrderBundle\Entity\TransportCost;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseApplication;

class TransferExpenseDataCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function configure()
    {
        $this
            ->setName('nko:transfer-expense-data')
            ->setDescription('Transfer expense from cost-like tables to new structure')
            ->setHelp('Rewrite report history with correct expense register');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->entityManager = $this->getContainer()->get('doctrine')->getManager();

        $output->writeln("");
        $output->writeln("Transferring expenses for each report...");

        $output->writeln("Searching for all the reports...");
        $reports = $this->entityManager->getRepository('NKOOrderBundle:BaseReport')->findAll();
        $output->writeln("Found " . count($reports) . " reports");

        foreach ($reports as $report) {
            $this->transferExpenses($output, $report);
        }
        $output->writeln("");
        $output->writeln("Writing reports to database...");
        $this->entityManager->flush();
        $output->writeln("Expense data successfully transferred to reports.");

        $output->writeln("");
        $output->writeln("Transferring expenses for each report history...");

        $output->writeln("Searching for all report histories...");
        $reportHistories = $this->entityManager->getRepository('NKOOrderBundle:ReportHistory')->findAll();
        $output->writeln("Found " . count($reportHistories) . " report histories");

        foreach ($reportHistories as $reportHistory) {
            $report = unserialize($reportHistory->getData());
            $this->transferExpenses($output, $report);
            $output->writeln("Saving modified report to report history with id = " . $reportHistory->getId());
            $serialized = serialize($report);
            $reportHistory->setData($serialized);
        }
        $output->writeln("");
        $output->writeln("Writing report histories to database...");
        $this->entityManager->flush();
        $output->writeln("\nSUCCESS: Expense data transferred.");
    }

    private function transferExpenses(OutputInterface $output, Report $report)
    {
        $output->writeln("");
        $output->writeln("Searching for transport expense type...");
        $transportExpenseType = $this->entityManager->getRepository('NKOOrderBundle:ExpenseType')->findOneBy(['code' => 'transport']);
        $output->writeln("Found expense type with title : " . $transportExpenseType->getTitle());
        $costs = $report->getTransportCosts();
        $this->fillRegister($output, $report, $transportExpenseType, $costs);

        $output->writeln("");
        $output->writeln("Searching for involved expense type...");
        $involvedExpenseType = $this->entityManager->getRepository('NKOOrderBundle:ExpenseType')->findOneBy(['code' => 'involved']);
        $output->writeln("Found expense type with title : " . $involvedExpenseType->getTitle());
        $costs = $report->getInvolvedCosts();
        $this->fillRegister($output, $report, $involvedExpenseType, $costs);

        $output->writeln("");
        $output->writeln("Searching for event expense type...");
        $eventExpenseType = $this->entityManager->getRepository('NKOOrderBundle:ExpenseType')->findOneBy(['code' => 'event']);
        $output->writeln("Found expense type with title : " . $eventExpenseType->getTitle());
        $costs = $report->getEventCosts();
        $this->fillRegister($output, $report, $eventExpenseType, $costs);

        $output->writeln("");
        $output->writeln("Searching for event other type...");
        $otherExpenseType = $this->entityManager->getRepository('NKOOrderBundle:ExpenseType')->findOneBy(['code' => 'other']);
        $output->writeln("Found expense type with title : " . $otherExpenseType->getTitle());
        $costs = $report->getOtherCosts();
        $this->fillRegister($output, $report, $otherExpenseType, $costs);

        $output->writeln("");
        $output->writeln("Adding expense types to financeSpent object...");
        $financeSpent = $report->getFinanceSpent();
        foreach ($financeSpent as $finance) {
            $value = '1.Транспортные и прочие расходы на поездку к месту стажировки/обучающего мероприятия';
            if ($finance->getCostItem() == $value) {
                $finance->setExpenseType($transportExpenseType);
                $output->writeln("Transport type expense added.");
            }

            $value = '2. Оплата привлеченных организаций/ специалистов (включая налоги и страховые взносы) и сопутствующие расходы';
            if ($finance->getCostItem() == $value) {
                $finance->setExpenseType($involvedExpenseType);
                $output->writeln("Involved type expense added.");
            }

            $value = '3. Расходы на последующие мероприятия по внедрению полученных знаний и опыта';
            if ($finance->getCostItem() == $value) {
                $finance->setExpenseType($eventExpenseType);
                $output->writeln("Event type expense added.");
            }

            $value = '4. Иные расходы';
            if ($finance->getCostItem() == $value) {
                $finance->setExpenseType($otherExpenseType);
                $output->writeln("Other type expense added.");
            }

            $value = 'ВСЕГО:';
            if ($finance->getCostItem() == $value) {
                $report->removeFinanceSpent($finance);
                $report->setTotalApprovedSum($finance->getApprovedSum());
                $report->setTotalPeriodExpense($finance->getPeriodCosts());
                $report->setTotalBalance($finance->getBalance());
                $output->writeln("Total finance values added.");
            }
        }
        $output->writeln("Added.");

    }

    private function addRegister(Report $report, $expenseType)
    {
        $register = new Register();
        $register->setExpenseType($expenseType);
        $report->addRegister($register);
        $register->setReport($report);

        return $register;
    }

    private function fillRegister(OutputInterface $output, Report $report, $expenseType, $costs)
    {
        $output->writeln("Adding register to report...");
        $register = $this->addRegister($report, $expenseType);
        $output->writeln("Rewriting data from cost collection to expense collection.");
        $output->writeln("Report id = " . $report->getId() . " expenseType = " . $expenseType->getTitle());

        if (!$costs->isEmpty()) {
            foreach ($costs as $cost) {
                $expense = new Expense();
                $expense->setName($cost->getName());
                $expense->setProofPaidGoodsDocument($cost->getProofPaidGoodsDocument());
                $expense->setProofProductExpenseDocument($cost->getProofProductCostDocument());
                $expense->setSum($cost->getSum());
                $register->addExpense($expense);
                $expense->setRegister($register);
            }
        }

        /**
         * @var ExpenseType $expenseType
         */
        if ($expenseType->getCode() == ExpenseTypeInterface::TRANSPORT_CODE) {
            $register->setTotalExpenseValue($report->getTotalTransportCost());
        }
        if ($expenseType->getCode() == ExpenseTypeInterface::INVOLVED_CODE) {
            $register->setTotalExpenseValue($report->getTotalInvolvedCost());
        }
        if ($expenseType->getCode() == ExpenseTypeInterface::EVENT_CODE) {
            $register->setTotalExpenseValue($report->getTotalInvolvedCost());
        }
        if ($expenseType->getCode() == ExpenseTypeInterface::OTHER_CODE) {
            $register->setTotalExpenseValue($report->getTotalOtherCost());
        }
    }
}