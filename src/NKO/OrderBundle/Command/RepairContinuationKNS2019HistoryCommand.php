<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNS2019Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RepairContinuationKNS2019HistoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:repair')
            ->setDescription('Reset is_send field for not valid application histories')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var  EntityManager $em*/
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for applications");
        $applications = $em->getRepository(ContinuationKNS2019Application::class)->findBy(['isSend' => true]);
        if (!count($applications)) {
            return;
        }

        $count = 0;
        foreach ($applications as $object) {
            /** @var $object ContinuationKNS2019Application */
            $author = $object->getAuthor();
            $competition = $object->getCompetition();

            $history = $em->getRepository(ApplicationHistory::class)->findSendByAuthor($author, $competition);

            $em->detach($object);
            $object = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($object);
            $object = $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($object);
            $object = $this->resolveNestedCollections($object);

            $correctData = serialize($object);
            $history->setData($correctData);

            $em->flush($history);
            $em->clear();
            $count++;
            $output->writeln("Repair ". $count. " histories");
        }
    }


    private function resolveNestedCollections($application)
    {
        $this->updateNestedCollection($application->getProjects());
        $this->updateNestedCollection($application->getBeneficiaryProblems());
        $this->updateNestedCollection($application->getEffectivenessKNSItems());
        $this->updateNestedCollection($application->getIndividualSocialResults());

        $this->removeLinkedHistoryData($application);

        return $application;
    }


    /** @var $object ContinuationKNS2019Application */
    private function removeLinkedHistoryData($object)
    {
        $linkedHistory = $object->getLinkedApplicationHistory();

        if ($linkedHistory) {
            $linkedHistory->setData(null);
        }
    }

    private function updateNestedCollection($collection)
    {
        foreach ($collection as $item) {
            $item = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);
        }
    }
}
