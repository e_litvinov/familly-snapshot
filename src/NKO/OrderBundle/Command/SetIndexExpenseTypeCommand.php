<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ExpenseType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetIndexExpenseTypeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:set-index-expense-type')
            ->setDescription('Set index for expense type');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numbering = ['total' => 0];

        $em = $this->getContainer()->get('doctrine')->getManager();

        $expenseTypes = $em->getRepository(ExpenseType::class)->findAll();
        foreach ($expenseTypes as $expenseType) {
            if (!$expenseType->getParent()){
                $expenseType->setIndexNumber(++ $numbering['total']);
            } else {
                $subItem = key_exists($expenseType->getParentCode(), $numbering) ?
                    ++$numbering[$expenseType->getParentCode()] :
                    ($numbering[$expenseType->getParentCode()] = 1);
                $expenseType->setIndexNumber($subItem);
            }
        }

        $output->writeln("");
        $output->writeln("Writing to database... ");

        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }
}