<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\FinanceReportManager\FinanceSpentManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report;

class UpdateFinanceReportCommand extends ContainerAwareCommand
{
    /** @var EntityManager */
    private $em;

    /**
     * @var FinanceSpentManager
     */
    private $manager;
    /**
     * @var OutputInterface
     */
    private $output;

    protected function configure()
    {
        $this
            ->setName('nko:update-total-values-finance-2019')
            ->setDescription('Update total values of finance report')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->manager = $this->getContainer()->get('nko_order.finance_report_manager.finance_spent');
        $this->output = $output;
        $reportForms = $this->em->getRepository(ReportForm::class)->findBy(['reportClass' => Report::class]);

        foreach ($reportForms as $reportForm) {
            $this->output->writeln('Start '.$reportForm->getId(). ' reportForm');
            $this->fixReportForm($reportForm);
            $this->output->writeln('End '.$reportForm->getId(). ' reportForm');
        }
    }

    private function fixReportForm(ReportForm $reportForm)
    {
        $reports = $this->em->getRepository(BaseReport::class)->findReportsByReportForm($reportForm);
        foreach ($reports as $report) {
            $this->output->writeln('Start '.$report->getId(). ' report');
            $this->fixReportWithHistory($report);
            $this->output->writeln('End '.$report->getId(). ' report');
        }
    }

    private function fixReportWithHistory(Report $report)
    {
        $report = $this->em->getRepository(Report::class)->find($report->getId());
        $template = $report->getReportTemplate();
        $this->manager->updateTotalValues([$report]);
        $history = $this->em->getRepository(ReportHistory::class)->findByReport($report, true);
        if (count($history) == 1) {
            $history = reset($history);
            $historyReport = unserialize($history->getData());
            $historyReport->setReportTemplate(unserialize(serialize($template)));
            $this->manager->updateReportFinanceSpentByTemplate($historyReport, $template);
            $this->manager->updateBalance($historyReport);
            $this->manager->updateTotalValues([$historyReport]);
            $history->setData(serialize($historyReport));
        }

        $this->em->flush();
        $this->em->clear();
    }
}
