<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/23/17
 * Time: 10:26 AM
 */

namespace NKO\OrderBundle\Command;


use Doctrine\Common\Collections\ArrayCollection;
use NKO\OrderBundle\Entity\FinalDecision;
use NKO\OrderBundle\Entity\MarkCriteria;
use NKO\OrderBundle\Entity\QuestionMark;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransferCourseMarksCommand extends ContainerAwareCommand
{
    const MARK_VALUE = [
        ['criterion' => 'Соответствие проекта цели, приоритетным направлениям и условиям Конкурса',
            'questions' => "В какой мере проект в целом соответствует условиям Конкурса – его цели и задачам, приоритетному направлению стажировочной площадки?\nВ случае полного несоответствия проекта условиям Конкурса, делающие проект непригодным для реализации – крайне низкий уровень проработанности проекта, полное несоответствие мероприятий проекта целям и задачам Конкурса, абсолютная неадекватность механизма реализации проекта, его бюджета или другие грубейшие ошибки в заявке – поставьте НОЛЬ баллов.
             В этом случае экспертиза проекта по другим критериям НЕ ТРЕБУЕТСЯ.
             Подробно обоснуйте свое решение в итоговой оценке проекта.",
            'range' => 10
        ],
        ['criterion' => 'Нацеленность организации на внедрение знаний и навыков, полученных благодаря участию в проекте',
            'questions' => "В какой мере организация стремится к последующему практическому использованию знаний и навыков, полученных в ходе проекта; к внедрению в деятельность организации практики (услуги, модели, технологии и пр.) выбранной стажировочной площадки?
            Учитывая сроки проекта, Эксперту следует обратить внимание в большей степени на нацеленность Организации на внедрение полученных знаний по практике стажировочной площадки не столько во время проекта, но в большей степени уже после окончания реализации проекта (до июня 2017 г.).",
            'range' => 15
        ],
        ['criterion' => 'Продуманность и адекватность предложенного механизма внедрения знаний и навыков, полученных в ходе проекта',
            'questions' => "Насколько удачен (продуман, корректен, адекватен) выбор механизма внедрения полученных знаний и навыков (после окончания реализации проекта – до июня 2017 г.)?
             В какой мере предложенный механизм внедрения актуален, оптимален и соответствует возможностям, опыту организации?",
            'range' => 15
        ],
        ['criterion' => 'Продуманность плана-графика мероприятий проекта',
            'questions' => "Насколько продуман план-график мероприятий проекта?
             Насколько оптимален формат проведения обучающего мероприятия (стажировки) и мероприятий по распространению и внедрению полученных знаний и навыков?
             В какой мере в проекте учтены возможные риски и продуманы меры, направленные на их снижение?",
            'range' => 15
        ],
        ['criterion' => 'Значимость результатов проекта для организации-заявителя',
            'questions' => "В какой мере обоснована актуальность и важность реализации проекта для организации-заявителя?
             Насколько потенциально значимы для организации ожидаемые результаты проекта (в т.ч. число сотрудников, которые получат знания и навыки; повышение доступности и качества оказываемых организацией услуг для целевых групп, введение новых услуг и пр.)? Максимальный балл 10.",
            'range' => 10
        ],
        ['criterion' => 'Вклад проекта в достижение ожидаемых результатов Конкурса и Программы «Семья и дети»',
            'questions' =>  "В какой мере полученные в ходе проекта результаты (в долгосрочной перспективе) соотносятся с достижением ожидаемых результатов Конкурса и Программы «Семья и дети»?
             Насколько реалистичны ожидаемые результаты и их целевые значения – в краткосрочной (до июня 2017 г.) и долгосрочной перспективе (на 5 лет)?",
            'range' => 10
        ],
        ['criterion' => 'Рациональность, прозрачность и обоснованность бюджета проекта',
            'questions' => "Насколько обоснован и экономичен бюджет проекта (отсутствие лишних затрат, завышенных расходов и пр.)?
             Насколько прозрачен бюджет проекта, понятны статьи расходов?",
            'range' => 5
        ],
        ['criterion' => 'Софинансирование',
            'questions' =>  "Наличие софинансирования (из собственных и привлеченных средств) в смете расходов по проекту – 20-30% софинансирования – 2 балла, 31-50% – 4 балла, 51% и более – 5 баллов. Максимальный балл 5.",
            'range' => 5
        ],
        ['criterion' => 'Соответствие цели проекта миссии и/или направлениям деятельности организации',
            'questions' =>   "В какой мере проект и выбранное организацией приоритетное направление стажировочной площадки соотносятся с миссией и основными направлениями деятельности организации?
            В какой мере обоснован выбор стажировочной площадки?",
            'range' => 5
        ],

    ];

    const FINAL_DECISIONS = ['Поддержать', 'Поддержать с доработкой', 'Не поддерживать' ];

    protected function configure()
    {
        $this
            ->setName('nko:transfer-marks-of-course-of-family')
            ->setDescription('Create criteria and mark for kns mark in mark_criteria table and delete criteria form mark table ')
            ->setHelp("Do nko:backup-table mark");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(  "Create criteria in new table");
        /**
         * @var EntityManager $entityManager
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $connection = $em->getConnection();

        $query = 'select id, criterion, questions, range_old_mark, old_mark_list_id from old_mark;';
        $statement = $connection->prepare($query);
        $statement->execute();
        $old_marks = $statement->fetchAll();
        $query = 'select id, final_decision from old_mark_list';
        $statement = $connection->prepare($query);
        $statement->execute();
        $old_mark_list = $statement->fetchAll();

        $markList = $em->getRepository('NKOOrderBundle:MarkList')->find($old_marks[0]['old_mark_list_id']);
        $competition = $markList->getApplication()->getCompetition();
        $output->writeln("Create new questions and criteria...");
        $this->createQuestion($em, $competition);
        $output->writeln("\nSUCCESS: Criteria and questions created.");
        $output->writeln("\nCreate new final decisions ...");
        $this->createFinalDecisions($em, $competition);
        $output->writeln("\nSUCCESS: Final decisions created.");
        $em->flush();

        $output->writeln("\nSet criteria and questions for mark.");

        foreach($old_marks as $old_mark){
            $mark = $em->getRepository('NKOOrderBundle:Mark')->find($old_mark['id']);
            $question = $em->getRepository('NKOOrderBundle:QuestionMark')->find($old_mark['questions'] + 1 );
            $mark->setQuestionMark($question);
        }

        $output->writeln("\nSet final decision for mark list.");

        foreach($old_mark_list as $list){
            $mark_list = $em->getRepository('NKOOrderBundle:MarkList')->find($list['id']);
            $finalDecision = $em->getRepository('NKOOrderBundle:FinalDecision')->find($list['final_decision'] + 1 );
            $mark_list->setFinalDecision($finalDecision);
        }
        $output->writeln("\nSUCCESS: Marks transfered");
        $em->flush();

    }

    private function createFinalDecisions($em, $competition){
        foreach (self::FINAL_DECISIONS as $decision){
            $finalDecision = new FinalDecision();
            $finalDecision->addCompetition($competition);
            $finalDecision->setName($decision);
            $em->persist($finalDecision);
        }

    }

    private function createQuestion($em, $competition){
        for($i = 0; $i < count(self::MARK_VALUE); $i++){
            $criterion  = new MarkCriteria();
            $criterion->setCriterion(self::MARK_VALUE[$i]['criterion']);
            $question = new QuestionMark();
            $question->setCompetition($competition);
            $question->setRangeMark(self::MARK_VALUE[$i]['range']);
            $question->setQuestion(self::MARK_VALUE[$i]['questions']);
            $question->setCriteria($criterion);
            $criterion->addQuestion($question);
            $em->persist($criterion);
            $em->persist($question);
        }
    }

}