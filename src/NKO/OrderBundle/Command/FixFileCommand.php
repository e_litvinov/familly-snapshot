<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 9/25/17
 * Time: 4:58 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixFileCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:fix-file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $competition = $em->getRepository('NKOOrderBundle:Competition')->find(13);
        $appHistories = $em->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findBy(['competition' => $competition, 'username' => 'nadegdakarelia@mail.ru']);
        foreach ($appHistories as $appHistory) {
            /**
             * @var SecondStageApplication $app
             */
            $app = unserialize($appHistory->getData());

            if($app->getBudget()) {
                $app->setBudget('5567cca9c22e21702b3d91cfe4768a5e0a55cb76.xls');
                $appHistory->setData(serialize($app));
            }
        }
        $em->flush();

        $applicationHistories = $em->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findBy(['competition' => $competition, 'username' => 'ddvl4@mail.ru']);
        foreach ($applicationHistories as $applicationHistory) {
            /**
             * @var SecondStageApplication $app
             */
            $app = unserialize($applicationHistory->getData());

            if($app->getBudget() == '5ed56d4ade7af1514124dffbcd8b225a1af612d4.zip') {
                $app->setBudget('5ed56d4ade7af1514124dffbcd8b225a1af612d4.xls');
                $applicationHistory->setData(serialize($app));
            }

        }
        $em->flush();
    }

}