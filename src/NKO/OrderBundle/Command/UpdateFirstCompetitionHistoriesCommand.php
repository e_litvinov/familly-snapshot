<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFirstCompetitionHistoriesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-first-competition-history')
            ->setDescription('Update extra spread application history of first dompetition')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $competition = $em->getRepository(Competition::class)->find(3);
        $output->writeln("Competition: " . $competition->getName());

        $histories = $em->getRepository(ApplicationHistory::class)->findNotUsedByCompetition($competition, false, true);
        $output->writeln("Spread apps found: " . count($histories));

        foreach ($histories as $history) {
            $output->writeln("");
            $output->writeln("History id: " . $history->getId());
            $output->writeln("History username: " . $history->getUsername());

            /**
             * @var ApplicationHistory $history
             * @var ApplicationHistory $sendHistory
             */
            $sendHistory = $em->getRepository(ApplicationHistory::class)->findOneBy([
                'username' => $history->getUsername(),
                'competition' => $competition,
                'isSend' => true,
                'isSpread' => true,
            ]);

            if (!$sendHistory) {
                continue;
            }

            $output->writeln("Found send app: " . $sendHistory);
            $output->writeln("Updating history ...");
            $history = $em->getRepository(ApplicationHistory::class)->find($history->getId());

            $sendHistory->setIsSend(false);
            $sendHistory->setIsSpread(false);

            $history->setIsSend(true);
            $history->setIsSpread(true);

            $grants = $em->getRepository(GrantConfig::class)->findBy([
                'applicationHistory' => $sendHistory->getId()
            ]);

            foreach ($grants as $grant) {
                $output->writeln('Updating grant ' . $grant);
                $grant->setApplicationHistory($history);
            }

            $reports = $em->getRepository(BaseReport::class)->findBy([
                'applicationHistory' => $sendHistory->getId()
            ]);

            foreach ($reports as $report) {
                $output->writeln('Updating report ' . $report);
                $report->setApplicationHistory($history);
            }

            $reportTemplates = $em->getRepository(BaseReportTemplate::class)->findBy([
                'applicationHistory' => $sendHistory->getId()
            ]);

            foreach ($reportTemplates as $reportTemplate) {
                $output->writeln('Updating report template ' . $reportTemplate);
                $reportTemplate->setApplicationHistory($history);
            }
        }


        $histories = $em->getRepository(ApplicationHistory::class)->findNotUsedByCompetition($competition, true, true);
        $output->writeln("Apps found: " . count($histories));

        foreach ($histories as $history) {
            $output->writeln("");
            $output->writeln("Username: " . $history->getUsername());
            $applicationHistories = $em->getRepository(ApplicationHistory::class)->findBy([
                'username' => $history->getUsername(),
                'competition' => $history->getCompetition(),
            ]);
            $output->writeln("Apps found: " . count($applicationHistories));

            $i = 0;
            foreach ($applicationHistories as $applicationHistory) {
                $output->writeln("History: " . $applicationHistory->getId());
                if ($applicationHistory->getId() != $history->getId()) {
                    $applicationHistory->setIsSend(false);
                    $applicationHistory->setIsSpread(false);
                    $applicationHistory->setNumber(null);
                    $i++;
                }
            }
            $output->writeln("Updated: " . $i);
        }

        $output->writeln("");
        $output->writeln("Writing to database... ");

        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }

}