<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 12/19/17
 * Time: 2:04 PM
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class LoadMonitoringDocumentCommand extends ContainerAwareCommand
{
    const EMAIL = 'email';
    const REPORT_HISTORY_ID = 'report_history_id';
    const REPORT_FORM_ID = 'report_form_id';
    const INDIVIDUAL_RESULT_COUNT = 10;

    /**
     * @var PropertyAccess
     */
    protected $accessor;

    /**
     * @var EntityManager
     */
    protected $em;

    protected function configure()
    {
        $this
            ->addArgument(self::EMAIL, InputArgument::REQUIRED)
            ->addArgument(self::REPORT_HISTORY_ID, InputArgument::REQUIRED)
            ->addArgument(self::REPORT_FORM_ID, InputArgument::REQUIRED)
            ->setName('nko:load-monitoring-documents')
            ->setDescription('Load monitoring report of deleted report with params email and report_form_id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument(self::EMAIL);
        $reportHistoryId = $input->getArgument(self::REPORT_HISTORY_ID);
        $reportFormId = $input->getArgument(self::REPORT_FORM_ID);

        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();

        $monitoringResultProperties = $this->em->getClassMetadata(MonitoringResult::class)->getFieldNames();
        $periodResultProperties = $this->em->getClassMetadata(PeriodResult::class)->getFieldNames();
        $indicatorProperties = $this->em->getClassMetadata(Indicator::class)->getFieldNames();

        array_shift($periodResultProperties);
        array_shift($monitoringResultProperties);
        array_shift($indicatorProperties);
        $this->accessor = PropertyAccess::createPropertyAccessor();

        $output->writeln("Getting user...");
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        $output->writeln('User found: ' . $user->getNkoName());

        $output->writeln("Searching for report...");
        $report = $this->em->getRepository(Report::class)->findOneBy([
            'reportForm' => $reportFormId,
            'author' => $user
        ]);

        if (!$report) {
            $output->writeln('Can not find a report.');
            return;
        }

        $output->writeln('Report found.');

        $this->em->getFilters()->disable('softdeleteable');

        $deletedReports = $this->em->getRepository(Report::class)->createQueryBuilder('r')
            ->select('r')
            ->andWhere('r.author = :author')
            ->andWhere('r.reportForm = :reportForm')
            ->andWhere('r.deletedAt is not null')
            ->orderBy('r.deletedAt', 'DESC')
            ->setParameters([
                'author' => $user,
                'reportForm' => $reportFormId
            ])
            ->getQuery()
            ->getResult();

        $deletedReport = reset($deletedReports);

        $output->writeln('Report found.');
        $monitoringDocuments = $this->em->getRepository(MonitoringDocument::class)->findBy([
            'report' => $deletedReport
        ]);

        if (empty($monitoringDocuments)) {
            $output->writeln('No monitoring documents were found.');
        }

        $monitoringResults = $report->getMonitoringResults();
        $deletedMonitoringResults = $deletedReport->getMonitoringResults();

        foreach ($deletedMonitoringResults as $deletedResult) {

            $result = $monitoringResults->filter(function ($object) use ($deletedResult) {
                return $object->getIndicator()->getTitle() == $deletedResult->getIndicator()->getTitle();
            })->first();

            $this->copyValues($monitoringResultProperties, $result, $deletedResult);
            $reportPeriodResults = $result->getPeriodResults();
            $deletedPeriodResults = $deletedResult->getPeriodResults();

            foreach ($deletedPeriodResults as $result) {
                $found = $reportPeriodResults->filter(function ($object) use ($result) {
                    return ($result->getMonitoringResult()->getIndicator()->getTitle() == $object->getMonitoringResult()->getIndicator()->getTitle() &&
                        $result->getPeriod() == $object->getPeriod() &&  $result->getPeriod() &&  $object->getPeriod());
                })->first();

                if ($found) {
                    $output->writeln("Copy period result values " . $found->getId());
                    $this->copyValues($periodResultProperties, $found, $result);
                }

                $found = $reportPeriodResults->filter(function ($object) use ($result) {
                    return ($result->getMonitoringResult()->getIndicator()->getTitle() == $object->getMonitoringResult()->getIndicator()->getTitle() &&
                        !$result->getPeriod() &&  !$object->getPeriod());
                })->first();

                if ($found) {
                    $output->writeln("Copy period result values " . $found->getId());
                    $this->copyValues($periodResultProperties, $found, $result);
                }
            }
        }

        $reportHistory = $this->em->getRepository(ReportHistory::class)->find($reportHistoryId);
        $storedReport = unserialize($reportHistory->getData());
        $storedMonitoringDocuments = $storedReport->getDocuments();

        /**
         * @var MonitoringDocument $monitoringDocument
         */
        foreach ($monitoringDocuments as $monitoringDocument) {
            $this->createMonitoringDocument($report, $monitoringDocument, $monitoringResults);
        }

        foreach ($storedMonitoringDocuments as $document) {
            $exist = false;

            foreach ($monitoringDocuments as $monitoringDocument) {
                if ($monitoringDocument->getId() == $document->getId()) {
                    $exist = true;
                }
            }

            if (!$exist) {
                $this->createMonitoringDocument($report, $document, $monitoringResults);
            }
        }

        foreach ($deletedReport->getPeriods() as $period) {
            $report->addPeriod($period);
        }

        $this->em->getFilters()->enable('softdeleteable');

        $output->writeln("Writing to db...");
        $this->em->flush();
        $output->writeln("SUCCESS");
    }

    private function copyValues($properties, $targetObject, $object)
    {
        foreach ($properties as $property) {
            $value = $this->accessor->getValue($object, $property);
            if ($property != 'deletedAt') {
                $this->accessor->setValue($targetObject, $property, $value);
            }
        }
    }

    private function createMonitoringDocument($report, $monitoringDocument, $monitoringResults)
    {
        $document = new MonitoringDocument();
        $this->em->persist($document);
        $document->setReport($report);
        $document->setFile($monitoringDocument->getFile());
        $document->setPeriod($monitoringDocument->getPeriod());
        $documentResult = $monitoringDocument->getMonitoringResult();
        $result = $monitoringResults->filter(function ($object) use ($documentResult) {
            if (!$documentResult) {
                return false;
            }

            return $object->getIndicator()->getTitle() == $documentResult->getIndicator()->getTitle();
        })->first();

        if ($result) {
            $document->setMonitoringResult($result);
        }
    }
}