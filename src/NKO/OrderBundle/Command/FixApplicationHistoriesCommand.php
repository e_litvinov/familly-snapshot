<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application\KNS\Application2019\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BeneficiaryResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\MeasurementMethod;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixApplicationHistoriesCommand extends ContainerAwareCommand
{
    const HISTORIES = [
        57009,
        57045

    ];

    /** @var EntityManager */
    private $em;

    protected function configure()
    {
        $this
            ->setName('nko:fix-histories')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();

        $output->writeln("Searching for applications");
        $applicationHistoriess = $this->em->getRepository(ApplicationHistory::class)->findByIds(self::HISTORIES);

        $count = 0;
        foreach ($applicationHistoriess as $history) {
            $id = unserialize($history->getData())->getId();
            $object = $this->em->getRepository(Application::class)->find($id);

            $this->em->detach($object);
            $object = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($object);
            $object = $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($object);
            $object = $this->resolveNestedCollections($object);

            $correctData = serialize($object);
            $history->setData($correctData);

            $this->em->flush($history);
            $count++;
            $output->writeln("Repair ". $count. " histories");
        }
    }

    private function resolveNestedCollections(Application $application)
    {

        $this->updateNestedProject($application->getProjects());
        $this->updateNestedCollection($application->getProjectResults());
        $this->updateNestedCollection($application->getEmployeeResults());
        $this->updateNestedCollection($application->getBeneficiaryResults());

        return $application;
    }

    private function updateNestedCollection($collection)
    {
        foreach ($collection as $item) {
            $this->em->detach($item->getLinkedMethod());
            $linkedMethod = $this->em->find(MeasurementMethod::class, $item->getLinkedMethod()->getId());
            $item->setLinkedMethod($linkedMethod);
        }
    }

    private function updateNestedProject($collection)
    {
        foreach ($collection as $item) {
            $item = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);
        }
    }
}
