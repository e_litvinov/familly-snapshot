<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteReportsOfOrganizationCommand extends ContainerAwareCommand
{
    const GRANT_CONFIG_IDS = [
        1243,
        1399,
        1400
    ];

    /** @var EntityManagerInterface */
    private $em;

    protected function configure()
    {
        $this
            ->setName('nko:fix-NKO-2632')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();

        foreach (self::GRANT_CONFIG_IDS as $id) {
            $grantConfig = $this->em->getRepository(GrantConfig::class)->find($id);
            if (!$grantConfig instanceof GrantConfig) {
                continue;
            }
            $this->removeAllConnectedWithThisGrantConfig($grantConfig);
            $this->em->flush();
            $this->em->clear();
        }
    }

    private function removeAllConnectedWithThisGrantConfig(GrantConfig $grantConfig)
    {
        $reportForm = $grantConfig->getReportForm();
        $applicationHistory = $grantConfig->getApplicationHistory();
        $psrm = $applicationHistory->getPsrn();
        $this->removeReportTemplates($reportForm, $applicationHistory);
        $this->removeReportHistories($reportForm, $psrm);
        $this->removeReports($reportForm, $psrm);
        $this->em->remove($grantConfig);
    }

    private function removeReports(ReportForm $reportForm, $psrn)
    {
        $reports = $this->em->getRepository(BaseReport::class)->findBy(['reportForm' => $reportForm, 'psrn' => $psrn]);

        foreach ($reports as $report) {
            $this->em->remove($report);
        }
    }

    private function removeReportTemplates(ReportForm $reportForm, ApplicationHistory $applicationHistory)
    {
        $reportTemplates = $this->em->getRepository(BaseReportTemplate::class)->findBy(['reportForm' => $reportForm, 'applicationHistory' => $applicationHistory]);

        foreach ($reportTemplates as $template) {
            $this->em->remove($template);
        }
    }

    private function removeReportHistories(ReportForm $reportForm, $psrn)
    {
        $reportHistories = $this->em->getRepository(ReportHistory::class)->findBy(['reportForm' => $reportForm, 'psrn' => $psrn]);

        foreach ($reportHistories as $history) {
            $this->em->remove($history);
        }
    }
}
