<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.8.18
 * Time: 16.54
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\KNS2017\Application;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\UserBundle\Entity\NKOUser;

class SetLinkedApplicationHistoryToReportCommand extends ContainerAwareCommand
{
    const REPORT_ID = 'report_id';
    const APPLICATION_HISTORY_ID = 'application_history_id';

    protected function configure()
    {
        $this
            ->addArgument(self::REPORT_ID, InputArgument::REQUIRED)
            ->addArgument(self::APPLICATION_HISTORY_ID, InputArgument::REQUIRED)
            ->setName('nko:set-linked-application-history-to-report')
            ->setDescription('set missing application history to report')
            ->setHelp("set missing application history to report 
                with report_id and linked application_history_id params")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reportId = $input->getArgument(self::REPORT_ID);
        $appHistoryid = $input->getArgument(self::APPLICATION_HISTORY_ID);

        $em = $this->getContainer()->get('doctrine')->getManager();

        /**
         * @var $report BaseReport
         */
        $output->writeln('Finding report...');
        $report = $em->getRepository(BaseReport::class)->find($reportId);

        $output->writeln('Finding application history...');
        $applicationHistory = $em->getRepository(ApplicationHistory::class)->find($appHistoryid);

        $report->setApplicationHistory($applicationHistory);

        $output->writeln('Finding report histories...');
        $reportHistories = $em->getRepository(ReportHistory::class)->findBy([
            'psrn' => $report->getAuthor()->getPsrn(),
            'reportForm' => $report->getReportForm(),
        ]);
        $output->writeln('Found ' . count($reportHistories) . ' report histories.');

        foreach ($reportHistories as $reportHistory) {
            $app = unserialize($reportHistory->getData());
            $app->setApplicationHistory($applicationHistory);
            $data = serialize($app);
            $reportHistory->setData($data);
            $output->writeln('Updated application history: ' . $reportHistory->getId());
        }


        $output->writeln("Writing to db...");

        try {
            $em->flush();
            $output->writeln("SUCCESS");
        } catch (Exception $e) {
            $output->writeln("Oops... database update FAILED!");
        }
    }
}
