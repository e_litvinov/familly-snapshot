<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class UpdateBriefApplication2018HistoryCommand extends ContainerAwareCommand
{
    /**
     * @var PropertyAccessor $accessor
     */
    private $accessor;

    protected function configure()
    {
        $this
            ->setName('nko:update-brief-history')
            ->setDescription('Save data with nested collections at spread report histories for brief farvater 2018')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for spread application histories...");
        $histories = $em->getRepository(ApplicationHistory::class)->findByIsSpreadByCompetition(15);
        $output->writeln("\t found " . count($histories) . " spread histories.");

        $this->accessor = PropertyAccess::createPropertyAccessor();
        /**
         * @var ApplicationHistory $history
         */
        foreach ($histories as $history) {
            $applicationId = unserialize($history->getData())->getId();
            $application = $em->getRepository(Application::class)->find($applicationId);
            $output->writeln("Application history id " . $history->getId() . ", application id " . $application->getId() . " user " . $application->getAuthor());

            /**
             * @var Application $application
             */
            $application = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($application);
            $application = $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($application);
            $this->updateNestedCollection($application->getFinancingSources());
            $this->updateNestedCollection($application->getEffectivenessItems());
            $this->updateNestedCollection($application->getBeneficiaryProblems());
            $serializedObject = serialize($application);
            $history->setData($serializedObject);
        }

        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("Success.");
    }

    private function updateNestedCollection($collection)
    {
        foreach ($collection as $item) {
            $item = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);

            if ($this->accessor->isReadable($item, 'type')) {
               $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item->getType());
            }
            if ($this->accessor->isReadable($item, 'indicator')) {
                $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item->getIndicator());
            }
            if ($this->accessor->isReadable($item, 'socialResults')) {
                foreach ($item->getSocialResults() as $result) {
                    $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($result);
                }
            }
        }
    }
}