<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveDoubleRegistersFinanceReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:remove-double-registers')
            ->setDescription('Remove double registers')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $reports = $em
            ->createQueryBuilder()
            ->select('p')
            ->from(Report::class, 'p')
            ->where('p.createdAt between :start and :finish')
            ->setParameters([
                'start' => new \DateTime('2018-11-29'),
                'finish' => new \DateTime(),
            ])
            ->getQuery()
            ->getResult();

        $output->writeln("Found ". count($reports). " reports");
        $output->writeln("----------------------------");

        foreach ($reports as $key => $report) {
            $uniqueRegisters = [];
            $uniqueSpents = [];

            $registers = $report->getRegisters();
            $removedReports = 0;
            $output->writeln("\nReport ".($key+1)." \n\n". count($registers). " registers found");
            foreach ($registers as $register) {
                $expenseType = $register->getExpenseType();
                if (!in_array($expenseType, $uniqueRegisters)) {
                    $uniqueRegisters[] = $expenseType;
                    continue;
                }
                $removedReports++;
                $report->removeRegister($register);
            }

            $output->writeln("Remove: ". $removedReports. " registers");

            $removedSpents = 0;
            $financeSpents = $report->getFinanceSpent();
            $output->writeln(count($financeSpents). " spents found");
            foreach ($financeSpents as $spent) {
                $expenseType = $spent->getExpenseType();
                if (!in_array($expenseType, $uniqueSpents)) {
                    $uniqueSpents[] = $expenseType;
                    continue;
                }
                $removedSpents++;
                $report->removeFinanceSpent($spent);
            }
            $output->writeln("Remove: ". $removedSpents. " spents");
        }
        $em->flush();
    }
}
