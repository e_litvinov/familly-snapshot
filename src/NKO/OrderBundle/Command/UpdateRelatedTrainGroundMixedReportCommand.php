<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application;

class UpdateRelatedTrainGroundMixedReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-related-training-groun-mix')
            ->setDescription('Update relatedtraining ground mixed report')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $user = $em->getRepository(NKOUser::class)->findBy(['psrn'=> 1110266001515]);
        $report = $em->getRepository(Report::class)->findOneBy(['author'=> $user]);

        if (!$report) {
            $output->writeln("Report not found");
            return;
        }

        $applicationHistory = $report->getApplicationHistory();
        $revertApplication = unserialize($applicationHistory->getData());
        $competition = $applicationHistory->getCompetition();

        $lastApplication = $em->getRepository(Application::class)->findOneBy([
            'competition'=> $competition,
            'author' => $user
        ]);

        if (!$lastApplication) {
            $output->writeln("Application not found");
            return;
        }

        $revertApplication->setProjectName($lastApplication->getProjectName());
        $serializedReport = serialize($revertApplication);
        $applicationHistory->setData($serializedReport);

        try {
            $em->flush();
            $output->writeln("Success");
        } catch (Exception $e) {
            $output->writeln("NOT successful.");
        }
    }
}
