<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 27.01.17
 * Time: 16:06
 */

namespace NKO\OrderBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\SiteLink;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseApplication;

class TransformJsonDataCommand extends ContainerAwareCommand
{
    const COLUMNS_TO_REPLACE = [
        'id',
        'legalRegion',
        'actualRegion',
        'name',
        'abbreviation',
        'pSRN',
        'legalPostCode',
        'legalCity',
        'legalStreet',
        'isAddressesEqual',
        'actualPostCode',
        'actualCity',
        'actualStreet',
        'phoneCode',
        'phone',
        'headOfOrganizationFullName',
        'headOfOrganizationPhoneCode',
        'headOfOrganizationPhone',
        'headOfOrganizationMobilePhone',
        'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationEmeil',
        'headOfProjectFullName',
        'headOfProjectPhoneCode',
        'headOfProjectPhone',
        'headOfProjectMobilePhoneCode',
        'headOfProjectMobilePhone',
        'headOfProjectEmeil',
        'headOfAccountingFullName',
        'headOfAccountingPhoneCode',
        'headOfAccountingPhone',
        'headOfAccountingMobilePhoneCode',
        'headOfAccountingMobilePhone',
        'headOfAccountingEmeil',
        'dateRegistrationOfOrganization',
        'primaryActivity',
        'mission',
        'iNN',
        'kPP',
        'oKPO',
        'oKVED',
        'bankINN',
        'bankKPP',
        'bIK',
        'projectName',
        'headOfOrganizationPosition',
        'countStaffEmployees',
        'countInvolvingEmployees',
        'bankName',
        'bankLocation',
        'correspondentAccount',
        'paymentAccount',
        'deadLineStart',
        'deadLineFinish',
        'email',
        'siteLinks',
        'socialNetworkLinks',
        'beneficiaryResults',
        'employeeResults',
        'risks',
        'headOfProjectPosition',
        'childrenCategoryName',
        'childrenCategories',
        'readyToSent',
        'isSend',
        'competition',
        'author',
        'sendVersion',
        'regulation',
        'version'
    ];

    protected function configure()
    {
        $this
            ->setName('nko:transform-json')
            ->setDescription('Transform application_history data')
            ->setHelp('Transform application_history data to fit base_application ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $application_history = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findAll();

        $output->writeln("Transforming json data...");
        foreach ($application_history as $app) {
            /**
             * @var ApplicationHistory $app
             */
            $transformed = $this->jsonTransform($app->getData());
            $app->setData($transformed);
            $em->persist($app);
        }
        $em->flush();
        $output->writeln("\t data transformed");
        $output->writeln("\nSUCCESS: ".count($application_history)." values transformed");
    }

    public function jsonTransform($data)
    {
        $fullPattern = '/(s:)([0-9]+)(:\"[^a-zA-Z0-9_]+)([A-Za-z0-9_+\\\\]+Application)+[^a-zA-Z0-9_]+([A-Za-z0-9_]+)\"/u';
        preg_match_all($fullPattern, $data, $matches);
        $fullMatch = $matches[0];
        $length = $matches[2];
        $appTypes = $matches[4];
        $vars = $matches[5];

        foreach ($vars as $key => $var) {
            if(in_array($var, self::COLUMNS_TO_REPLACE)) {
                $fullMatch[$key] = str_replace([
                    $length[$key],
                    $appTypes[$key]
                ], [
                    strlen($var) + 3,
                    '*'
                ], $fullMatch[$key]);
            }
        }
        return str_replace($matches[0], $fullMatch, $data);
    }

}