<?php

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\KNS2017\TrainingGround;
use NKO\OrderBundle\Utils\ApplicationTypes;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application as KNSApplication;

class UpdateTrainingGroundCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-training-ground')
            ->setDescription('Update total values of finance report')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $parent = $em->getRepository(TrainingGround::class)
            ->findOneBy([
                'name' => 'Постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет- выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека в замещающей семье',
                'competition' => ApplicationTypes::KNS_APPLICATION_2018
            ]);

        $child = $em->getRepository(TrainingGround::class)
            ->findOneBy([
                'name' => 'Государственное бюджетное общеобразовательное учреждение «Мензелинская школа-интернат для детей-сирот и детей, оставшихся без попечения родителей, с ограниченными возможностями здоровья»',
                'competition' => ApplicationTypes::KNS_APPLICATION_2018
            ]);

        $child->setParent($parent);

        $ground = $em->getRepository(TrainingGround::class)
            ->findOneBy([
                'name' => 'Постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет – выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека (попечительство) в замещающей семье.',
                'competition' => ApplicationTypes::KNS_APPLICATION_2018
            ]);

        $applications = $em
            ->createQueryBuilder()
            ->select('a')
            ->from(KNSApplication::class, 'a')
            ->where(':ground MEMBER OF a.trainingGrounds')
            ->setParameters([
                'ground' => $ground,
            ])
            ->getQuery()
            ->getResult();

        foreach ($applications as $application) {
            $application->removeTrainingGround($ground);
            $application->addTrainingGround($parent);
        }

        if ($ground) {
            $em->remove($ground);
        }

        try {
            $em->flush();
            $output->writeln("Success");
        } catch (Exception $e) {
            $output->writeln("NOT successful.");
        }
    }
}
