<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveAppHistoryByLastHeldCompetitionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:remove-history')
            ->setDescription('Send mark')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $competition = $em->getRepository('NKOOrderBundle:Competition')->findLastHeldCompetition();
        $output->writeln("Found last held competition: ".$competition->getName());

        $sendAppHistory = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findByLastCreatedAt($competition->getId());
        $output->writeln("Found ".count($sendAppHistory)." valid send applications");
        $appHistory = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findBy(['competition' => $competition->getId()]);
        $output->writeln("Found ".count($appHistory)." application histories");

        $i = 0;
        $flag = true;
        foreach ($appHistory as $history) {
            foreach ($sendAppHistory as $sendApp) {
                if ($history->getId() == $sendApp->getId()) {
                    $flag = false;
                }
            }
            if ($flag) {
                $i++;
                $output->writeln("Remove history with id = ".$history->getId());
                $em->remove($history);
            }
            $flag = true;
        }
        $output->writeln("Writing to database...");
        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }

}