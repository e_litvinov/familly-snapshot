<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\DocumentReport;
use NKO\OrderBundle\Entity\DocumentType;
use NKO\OrderBundle\Entity\Folder;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Resolver\CollectionResolver;
use NKO\OrderBundle\Resolver\ProxyResolver;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

class UpdateReportHistoryCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * @var ProxyResolver $proxyResolver
     */
    private $proxyResolver;

    /**
     * @var CollectionResolver $collectionResolver
     */
    private $collectionResolver;

    protected function configure()
    {
        $this
            ->setName('nko:update-report-history')
            ->setDescription('Reset is_send field for not valid report histories for KNS-2016 reports')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $output->writeln("Searching for competitions...");
        $competition = $this->em->getRepository(Competition::class)->findOneBy([
            'applicationClass' => Application::class
        ]);

        /**
         * @var Competition $competition
         */
        $output->writeln("");
        $output->writeln("Start working with competition: " . $competition->getName());
        $this->updateSendReportHistory($competition, $output);
        $output->writeln("Finish working with competition: " . $competition->getName());

        $this->flush($output);

        $this->em->clear();

        $output->writeln("");
        $output->writeln("Start updating documents data.");
        $output->writeln("Searching for reportForm... ");
        $reportForm = $this->em->getRepository(Report\ReportForm::class)->findOneBy([
            'competition' => $competition,
            'reportClass' => Report::class
        ]);
        $output->writeln("\t report form '" . $reportForm->getTitle() . "' found.");

        $output->write("Searching for report histories...");
        $reportHistories = $this->em->getRepository(ReportHistory::class)->findBy([
            'isSend' => true,
            'reportForm' => $reportForm
        ]);
        $output->writeln("\t" . count($reportHistories) . " report histories was found.");

        $output->writeln("");

        foreach ($reportHistories as $history) {
            $output->writeln("Report history: " . $history->getId() . ' ' . $history->getNkoName());
            $this->updateDocumentsData($history, $output);
            $output->writeln("Finished.");
        }

        $output->writeln("End of report history scope.");

        $this->flush($output);

        $this->em->clear();

        $output->writeln("");
        $output->writeln("Start updating report history data.");
        $output->writeln("Searching for reportForm... ");

        $output->write("Searching for report histories...");
        $reportHistories = $this->em->getRepository(ReportHistory::class)->findBy([
            'isSend' => true,
            'reportForm' => $reportForm
        ]);
        $output->writeln("\t" . count($reportHistories) . " report histories was found.");

        $output->writeln("");
        $this->proxyResolver = $this->getContainer()->get('nko.resolver.proxy_resolver');
        $this->collectionResolver = $this->getContainer()->get('nko.resolver.collection_resolver');
        foreach ($reportHistories as $history) {
            $output->writeln("Report history: " . $history->getId() . ' ' . $history->getNkoName());
            $this->updateHistoryData($history, $output);
            $output->writeln("Data updated.");
        }

        $this->flush($output);
    }

    private function flush(OutputInterface $output)
    {
        $output->writeln("");
        $output->writeln("Writing to database... ");

        $this->em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }

    private function updateHistoryData(ReportHistory $history)
    {
        /**
         * @var Report $report
         */
        $report = $this->em->getRepository(Report::class)->findOneBy([
            'psrn' => $history->getPsrn()
        ]);

        $report = $this->proxyResolver->resolveProxies($report);
        $report = $this->collectionResolver->fetchCollections($report);

        $data = serialize($report);
        $history->setData($data);
    }

    private function updateDocumentsData(ReportHistory $history, OutputInterface $output)
    {
        /**
         * @var Report $data
         */
        $data = unserialize($history->getData());

        /**
         * @var Report $report
         */
        $report = $this->em->getRepository(Report::class)->findOneBy([
            'psrn' => $data->getPsrn()
        ]);

        foreach ($report->getDocuments() as $document) {
            $report->removeDocument($document);
            $output->writeln("Removed document with id: " . $document->getId());
        }

        $documents = $data->getDocuments();

        $output->writeln("Total document count: " . $documents->count());
        foreach ($documents as $document) {
            if (!$document->getDeletedAt()) {
                $newDocument = $this->createDocument($document);
                $report->addDocument($newDocument);
                $output->writeln("Add new document restored from: " . $document->getId());
            }
        }
    }

    private function updateSendReportHistory(Competition $competition, OutputInterface $output)
    {
        $output->writeln("Searching for reportForm... ");
        $reportForm = $this->em->getRepository(Report\ReportForm::class)->findOneBy([
            'competition' => $competition,
            'reportClass' => Report::class
        ]);
        $output->writeln("\t report form '" . $reportForm->getTitle() . "' found.");

        $output->write("Searching for report histories...");
        $histories = $this->em
            ->createQueryBuilder()
            ->select('partial h.{id, isSend}')
            ->from(ReportHistory::class, 'h')
            ->where('h.reportForm = :reportForm')
            ->setParameters([
                'reportForm' => $reportForm
            ])
            ->getQuery()
            ->getResult();
        $output->writeln("\t" . count($histories) . " report histories was found.");

        $output->writeln("Searching for histories to reset ... ");
        $validHistories = $this->em
            ->getRepository(ReportHistory::class)
            ->findSendByLastCreatedAtByReportForm($reportForm->getId());

        $readyToReset = true;
        foreach ($histories as $history) {
            foreach ($validHistories as $validHistory) {
                if ($history->getId() == $validHistory->getId()) {
                    $readyToReset = false;
                }
            }
            if ($readyToReset) {
                $output->writeln("Reset history with id = " . $history->getId());
                $history->setIsSend(false);
            }
            $readyToReset = true;
        }
    }

    private function createDocument(DocumentReport $documentToRestore)
    {
        $document = new DocumentReport();
        $this->em->persist($document);
        $document->setDate($documentToRestore->getDate());

        $folder = $this->em->getRepository(Folder::class)->find($documentToRestore->getFolder()->getId());
        $document->setFolder($folder);

        $documentType = $this->em->getRepository(DocumentType::class)->find($documentToRestore->getDocumentType()->getId());
        $document->setDocumentType($documentType);

        $document->setTableType($documentToRestore->getTableType());
        $document->setRow($documentToRestore->getRow());
        $document->setFile($documentToRestore->getFile());

        return $document;
    }
}
