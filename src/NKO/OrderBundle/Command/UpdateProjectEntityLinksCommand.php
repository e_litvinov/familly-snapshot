<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 25.10.18
 * Time: 17.30
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class UpdateProjectEntityLinksCommand extends ContainerAwareCommand
{
    const APPLICATION_FIELDS = [
        'briefApplication',
        'KNS2017Application',
        'farvater2017Application'
    ];

    /** @var EntityManager $em */
    private $em;

    /** @var PropertyAccessor $em */
    private $propertyAccessor;

    protected function configure()
    {
        $this
            ->setName('nko:update-project-links')
            ->setDescription('Update project entity links to application')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();

        $this->em->getFilters()->disable('softdeleteable');

        $projects = $this->em->getRepository(Project::class)->findAll();

        foreach (self::APPLICATION_FIELDS as $field) {
            $output->writeln("Transferring {$field} to application...");
            $count = $this->updateLinkedApplication($projects, $field);
            $output->writeln("{$count} applications were updated.");
        }

        $this->em->getFilters()->enable('softdeleteable');

        $output->writeln("Writing to db...");
        $this->em->flush();
        $output->writeln("SUCCESS");
    }

    private function updateLinkedApplication($projects, $applicationField)
    {
        /**
         * @var Project $project
         */
        $count = 0;
        foreach ($projects as $project) {
            $application = $this->propertyAccessor->getValue($project, $applicationField);
            if (is_object($application)) {
                $count++;
                $project->setApplication($application);
                $this->propertyAccessor->setValue($project, $applicationField, null);
            }
        }

        return $count;
    }
}
