<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class FixLackOfRegionInApplicationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:fix-lack-of-region')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $applicationHistorys = $em->getRepository(ApplicationHistory::class)->findByNameOfApplication("NKO\OrderBundle\Entity\Application\KNS\Application2018\Application");
        $repositoryOfRegion = $em->getRepository(Region::class);
        /**@var $applicationHistory \NKO\OrderBundle\Entity\ApplicationHistory */
        /**@var $app \NKO\OrderBundle\Entity\Application\KNS\Application2018\Application */
        foreach ($applicationHistorys as $applicationHistory){
            $app  = unserialize($applicationHistory->getData());
            $legalRegion = $repositoryOfRegion->find($app->getLegalRegion()->getId());
            $actualRegion = $repositoryOfRegion->find($app->getActualRegion()->getId());
            $app->setLegalRegion($legalRegion);
            $app->setActualRegion($actualRegion);
            $applicationHistory->setLegalRegion($legalRegion->getName());
            $applicationHistory->setData(serialize($app));
        }
        $em->flush();
        $output->writeln("OK ".count($applicationHistorys)." historys is changed." );
    }
}