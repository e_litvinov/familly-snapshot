<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;

use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\Farvater2017\Application;
use NKO\OrderBundle\Utils\Report\PublicationIndicators;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFarvater2017ApplicationCommand extends ContainerAwareCommand
{
    const ADDITIONAL_INDICATOR_INDEXES = [
        'Количество детей, повысивших уровень готовности к самостоятельной жизни в обществе',
        'Количество кровных кризисных семей, повысивших родительские / профессиональные компетенции',
        'Количество замещающих семей, повысивших родительские  / профессиональные компетенции',
        'Количество семей, у которых наблюдается улучшение детско-родительских отношений',
        'Количество замещающих семей, в отношении которых повышен уровень поддержки со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)',
        'Количество кризисных кровных семей, в отношении которых  повышен уровень поддержки со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.)',
        'Число новых запросов на услуги (в области профилактики сиротства) со стороны семей целевых групп'
    ];

    const SOCIAL_RESULT_INDEXES = [
        '1. Количество детей-сирот и детей, оставшихся без попечения родителей, переданных на семейные формы устройства',
        '1.1. в том числе подростков',
        '1.2. в том числе детей с ОВЗ',
        '1.3. в том числе сиблингов',
        '2. Количество детей, возвращённых в кровные семьи',
        '2.1. в том числе подростков',
        '2.2. в том числе детей с ОВЗ',
        '3. Количество предотвращённых случаев отобрания (изъятий) / отказов детей из кровных семей',
        '4. Количество предотвращённых случаев отобрания (изъятий) / отказов из замещающих семей',
    ];

    const BENEFICIARY_IMPROVEMENT_INDEXES = [
        'улучшение психического состояния (снижение уровня тревожности, агрессии, страха и т.п.)',
        'улучшение физического состояния',
        'повышение уровня развития, навыков',
        'улучшение детско-родительских отношений',
        'улучшение показателей успеваемости'
    ];

    const PUBLICATION_INDEXES = [
        'Число сообщений в СМИ, инициированных в рамках проекта',
        'Число изданных информационных / методических материалов',
        'Тираж печатных информационных / методических материалов',
        'Количество распространенных экземпляров информационных / методических материалов по проекту'
    ];

    protected function configure()
    {
        $this
            ->setName('nko:update-farvater-app')
            ->setDescription('Set disappeared indicators at farvater 2017 app')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for competition...");
        $competition = $em->getRepository(Competition::class)->findOneBy([
            'applicationClass' => Application::class
        ]);
        $output->writeln("Competition found: " . $competition->getName());

        $output->writeln("Searching for applications...");
        $applications = $em->getRepository(Application::class)->findBy([
            'competition' => $competition
        ]);

        $output->writeln("Found " . count($applications) . " apps.");

        /**
         * @var $application Application
         */
        foreach ($applications as $application) {
            $output->writeln("Application " . $application->getId() . " updating");
            $this->updateIndexes($application, $output);
        }

        $output->writeln("Searching for SEND application histories...");
        $histories = $em->getRepository(ApplicationHistory::class)->findBy([
            'competition' => $competition,
            'isSend' => true,
        ]);

        foreach ($histories as $history) {
            $output->writeln("History " . $history->getId() . " updating");
            $data = $history->getData();
            $application = unserialize($data);
            $this->updateIndexes($application);
            $history->setData(serialize($application));
        }

        $output->writeln("Searching for SPREAD application histories...");
        $histories = $em->getRepository(ApplicationHistory::class)->findBy([
            'competition' => $competition,
            'isSpread' => true,
        ]);

        foreach ($histories as $history) {
            $output->writeln("History " . $history->getId() . " updating");
            $data = $history->getData();
            $application = unserialize($data);
            $this->updateIndexes($application);
            $history->setData(serialize($application));
        }

        $output->writeln("");
        $output->writeln("Writing to database... ");

        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }

    private function updateIndexes($application)
    {
        $this->setIndexName($application->getAdditionalIndicatorIndexes(), self::ADDITIONAL_INDICATOR_INDEXES);
        $this->setIndexName($application->getSocialResultIndexes(), self::SOCIAL_RESULT_INDEXES);
        $this->setIndexName($application->getBeneficiaryImprovementIndexes(), self::BENEFICIARY_IMPROVEMENT_INDEXES);
        $this->setIndexName($application->getPublicationIndexes(), self::PUBLICATION_INDEXES);
    }

    private function setIndexName($collection, $values)
    {
        foreach ($collection as $key => $item) {
            $item->setIndexName($values[$key]);
        }
    }

}