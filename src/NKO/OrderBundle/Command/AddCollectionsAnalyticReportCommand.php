<?php

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report;
use NKO\OrderBundle\Entity\Report\AnalyticReport\HumanResource;
use NKO\OrderBundle\Entity\Report\AnalyticReport\QuadrupleContext;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Publication;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Territory;
use NKO\OrderBundle\Entity\Report\AnalyticReport\DirectResult;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Attachment;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool;

class AddCollectionsAnalyticReportCommand extends ContainerAwareCommand
{
    const PSRN = [
        1034533000138,
        1036216001997
    ];

    const PSRN_SECOND = [
        1111600005417,
        1022202577210
    ];

    protected function configure()
    {
        $this
            ->setName('nko:add-collections-analytic-report');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        foreach (self::PSRN as $psrn) {
            $report = $em->createQueryBuilder()
                ->select('p')
                ->from(Report::class, 'p')
                ->innerjoin('p.author', 'a')
                ->where('a.psrn =:psrn')
                ->setParameter('psrn', $psrn)
                ->getQuery()
                ->getOneOrNullResult();

            if (!$report) {
                continue;
            }

            if (!count($report->getHumanResources())) {
                $report->addHumanResource(new HumanResource());
            }

            if (!count($report->getPartnerActivities())) {
                $report->addPartnerActivity(new QuadrupleContext());
            }

            if (!count($report->getPublications())) {
                $report->addPublication(new Publication());
            }

            if (!count($report->getTerritories())) {
                $report->addTerritory(new Territory());
            }

            if (!count($report->getExpectedAnalyticResults())) {
                $report->addExpectedAnalyticResult(new DirectResult());
            }

            if (!count($report->getPracticeAnalyticResults())) {
                $report->addPracticeAnalyticResult(new DirectResult());
            }

            if (!count($report->getPracticeFeedbackAttachments())) {
                $report->addPracticeFeedbackAttachment(new Feedback());
            }

            if (!count($report->getPracticeSuccessStoryAttachments())) {
                $report->addPracticeSuccessStoryAttachment(new Attachment());
            }
        }

        foreach (self::PSRN_SECOND as $psrn) {
            $report = $em->createQueryBuilder()
                ->select('p')
                ->from(Report::class, 'p')
                ->innerjoin('p.author', 'a')
                ->where('a.psrn =:psrn')
                ->setParameter('psrn', $psrn)
                ->getQuery()
                ->getOneOrNullResult();

            if (!$report) {
                continue;
            }

            $feedbacks = $report->getPracticeFeedbackAttachments();

            foreach ($feedbacks as $feedback) {
                $tools = $feedback->getTools();

                if (!$tools) {
                    for ($i = 0; $i < 3; $i++) {
                        $feedback->addTool(new FeedbackTool());
                    }
                }

                $iter = 0;
                foreach ($tools as $tool) {

                    if ($iter > 3) {
                        $feedback->removeTool($tool);
                    }

                    $iter++;
                }
            }
        }

        try {
            $em->flush();
            $output->writeln("Success");
        } catch (Exception $e) {
            $output->writeln("NOT successful.");
        }
    }
}
