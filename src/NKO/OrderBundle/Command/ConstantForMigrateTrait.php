<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 10.9.18
 * Time: 10.44
 */

namespace NKO\OrderBundle\Command;

use DateTime;
use Doctrine\DBAL\Driver\Connection;
use NKO\OrderBundle\Entity\InvolvedCost;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\BeneficiaryGroup;
use NKO\OrderBundle\Entity\BeneficiaryReportResult;
use NKO\OrderBundle\Entity\DocumentReport;
use NKO\OrderBundle\Entity\EmployeeReportResult;
use NKO\OrderBundle\Entity\EventCost;
use NKO\OrderBundle\Entity\FinanceSpent;
use NKO\OrderBundle\Entity\ImmediateResult;
use NKO\OrderBundle\Entity\OtherCost;
use NKO\OrderBundle\Entity\TrainingEvent;
use NKO\OrderBundle\Entity\TransportCost;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\TraineeshipTopic;
use NKO\OrderBundle\Entity\Organization;

trait ConstantForMigrateTrait
{
    private function changeId($history)
    {
        $pathern = '/(NKO\\\\OrderBundle\\\\Entity\\\\Report\x00id";i:)([0-9]+)/u';
        preg_match_all($pathern, $history, $matches);
        return str_replace(
            $matches[0][0],
            $matches[1][0] . $this->relationIds[$matches[2][0]],
            $history
        );
    }

    private function createIdFile()
    {
        foreach ($this->relationIds as $oldId => $newId) {
            $this->relationIds[$oldId] = $newId->getId();
        }

        $str_value = serialize($this->relationIds);
        $f = fopen(self::FILE_NAME, 'w');
        fwrite($f, $str_value);
        fclose($f);
    }

    private function getIdsFromFile()
    {
        $file = file_get_contents(self::FILE_NAME);
        $this->relationIds = unserialize($file);
    }

    private function getReportForm()
    {
        return $this->em->getRepository(Report\ReportForm::class)->findBy(['reportClass' => Report::class])[0];
    }

    private function findApplicationHistory($id)
    {
        return $this->em->getRepository(ApplicationHistory::class)->find($id);
    }

    private function findUser($psrn)
    {
        return $this->em->getRepository(NKOUser::class)->findOneBy(['psrn' => $psrn]);
    }

    /**
     * @param $class string
     * @param $id int
     * @param $connection Connection
     * @param $name string
     * @param $paramAS string
     * @param $paramJoin string
     *
     * @return array
     */
    private function findEntitiesInBD($class, $id, $connection, $name = 'report_id', $paramAS = '', $paramJoin = '')
    {
        return $connection->fetchAll('select *' .$paramAS. 'from '.$class. ' e '.$paramJoin.' where e.'.$name.' = '.$id, [
            $paramAS,
            $class,
            $paramJoin,
            $name,
            $id,
        ]);
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addOneToMany($newReport, $id, $connection)
    {
        $this->addBenificateReportResults($newReport, $id, $connection);
        $this->addEmployeeReportResults($newReport, $id, $connection);
        $this->addEventCost($newReport, $id, $connection);
        $this->addOtherCost($newReport, $id, $connection);
        $this->addInvolvedCost($newReport, $id, $connection);
        $this->addTransportCost($newReport, $id, $connection);
        $this->addTrainingEvent($newReport, $id, $connection);
        $this->addImmediateResult($newReport, $id, $connection);
        $this->addFinanceSpent($newReport, $id, $connection);
        $this->addDocumentReport($newReport, $id, $connection);
        $this->addBenifaciaryGroup($newReport, $id, $connection);
    }

    // Тут benificiary group совпадают же id старой бд и новой?(просто у меня совпадает
    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addBenifaciaryGroup($newReport, $id, $connection)
    {

        $items = $this->findEntitiesInBD('report_beneficiary_group', $id, $connection);
        foreach ($items as $item) {
            $group =$this->em->getRepository(BeneficiaryGroup::class)->find($item['beneficiary_group']);
            $newReport->addBeneficiaryGroup($group);
        }
    }

    /*TODO*/
    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addDocumentReport($newReport, $id, $connection)
    {
        $builder = $this->em->createQueryBuilder()
            ->select("d")
            ->from(DocumentReport::class, 'd')
            ->where('d.id = :id');     //Т.к. файлы хранятся не в бд, то переносятся документы, которые не связаны с отчётом(

        $items = $this->findEntitiesInBD('document_report', $id, $connection, 'document_id');
        foreach ($items as $item) {
            /** $existingDocument DocumentReport */
            $existingDocument = $builder
                ->setParameter('id', $item['id'])
                ->getQuery()->getOneOrNullResult()
            ;
            if ($existingDocument && $existingDocument->getReport() == null) {
                $newReport->addDocument($existingDocument);
                $existingDocument->setReport($newReport);
            }
            // Теперь документы рип?, но всё равно они когда-то были
        }
    }


    /*TODO*/
    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addFinanceSpent($newReport, $id, $connection)
    {
        $items = $this->findEntitiesInBD('finance_spent', $id, $connection);
        foreach ($items as $item) {
            $newItem = new FinanceSpent();
            $newItem->setCostItem($item['cost_item']);
            $newItem->setApprovedSum($item['approved_sum']);
            $newItem->setPeriodCosts($item['period_costs']);
            $newItem->setBalance($item['balance']);



            /*TODO в NKO-802 было убрано из financeSpent все total*/
            $newItem->setReport($newReport);

            $this->em->persist($newItem);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addImmediateResult($newReport, $id, $connection)
    {
        $items = $this->findEntitiesInBD('immediate_result', $id, $connection);
        foreach ($items as $item) {
            $newItem = new ImmediateResult();
            $newItem->setPlanValue($item['plan_value']);
            $newItem->setFactValue($item['fact_value']);
            $newItem->setExpectedValue($item['expected_value']);
            $newItem->setMethodMeasurement($item['method_measurement']);
            $newItem->setComment($item['comment']);
            $newItem->setResultCriteria($item['result_criteria']);

            $newItem->setReport($newReport);

            $this->em->persist($newItem);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addTrainingEvent($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('training_event', $id, $connection);
        foreach ($results as $result) {
            $newEvent = new TrainingEvent();
            $newEvent->setEventName($result['event_name']);
            $newEvent->setFormat(($result['format']));
            $newEvent->setTime($result['time']);
            $newEvent->setPlace($result['place']);
            $newEvent->setCountMember($result['count_member']);

            $newEvent->setReport($newReport);

            $this->em->persist($newEvent);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addTransportCost($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('transport_cost', $id, $connection);
        foreach ($results as $result) {
            $newResult = new TransportCost();
            $newResult->setName($result['name']);
            $newResult->setSum(($result['sum']));
            $newResult->setProofProductCostDocument($result['proof_product_cost_document']);
            $newResult->setProofPaidGoodsDocument($result['proof_paid_goods_document']);
            $newResult->setCostKey($result['cost_key']);

            $newResult->setReport($newReport);

            $this->em->persist($newResult);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addInvolvedCost($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('involved_cost', $id, $connection);
        foreach ($results as $result) {
            $newResult = new InvolvedCost();
            $newResult->setName($result['name']);
            $newResult->setSum(($result['sum']));
            $newResult->setProofProductCostDocument($result['proof_product_cost_document']);
            $newResult->setProofPaidGoodsDocument($result['proof_paid_goods_document']);
            $newResult->setCostKey($result['cost_key']);

            $newResult->setReport($newReport);

            $this->em->persist($newResult);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addEventCost($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('event_cost', $id, $connection);
        foreach ($results as $result) {
            $newCost = new EventCost();
            $newCost->setName($result['name']);
            $newCost->setSum(($result['sum']));
            $newCost->setProofProductCostDocument($result['proof_product_cost_document']);
            $newCost->setProofPaidGoodsDocument($result['proof_paid_goods_document']);
            $newCost->setCostKey($result['cost_key']);

            $newCost->setReport($newReport);

            $this->em->persist($newCost);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addEmployeeReportResults($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('employee_report_result', $id, $connection);
        foreach ($results as $result) {
            $newResult = new EmployeeReportResult();
            $newResult->setComments($result['comments']);
            $newResult->setExpectedValue(($result['expected_value']));
            $newResult->setFactValue($result['fact_value']);
            $newResult->setIndicator($result['indicator']);
            $newResult->setMethodMeasurement($result['method_measurement']);
            $newResult->setPlanValue($result['plan_value']);
            $newResult->setResult($result['result_criteria']);

            $newResult->setReport($newReport);

            $this->em->persist($newResult);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addBenificateReportResults($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('beneficiary_report_result', $id, $connection);
        foreach ($results as $result) {
            $newResult = new BeneficiaryReportResult();
            $newResult->setComments($result['comments']);
            $newResult->setExpectedValue(($result['expected_value']));
            $newResult->setFactValue($result['fact_value']);
            $newResult->setIndicator($result['indicator']);
            $newResult->setMethodMeasurement($result['method_measurement']);
            $newResult->setPlanValue($result['plan_value']);
            $newResult->setResult($result['result_criteria']);

            $newResult->setReport($newReport);

            $this->em->persist($newResult);
        }
    }

    /**
     *@param $newReport Report
     *@param $id int
     *@param $connection Connection
     *
     */
    private function addOtherCost($newReport, $id, $connection)
    {
        $results = $this->findEntitiesInBD('other_cost', $id, $connection);
        foreach ($results as $result) {
            $newCost = new OtherCost();
            $newCost->setName($result['name']);
            $newCost->setSum(($result['sum']));
            $newCost->setProofProductCostDocument($result['proof_product_cost_document']);
            $newCost->setProofPaidGoodsDocument($result['proof_paid_goods_document']);
            $newCost->setCostKey($result['cost_key']);

            $newCost->setReport($newReport);

            $this->em->persist($newCost);
        }
    }

    /**
     *@param $report array
     *
     *@return  Report
     */
    private function setConstants(array $report)
    {
        $newReport = new Report();
        $newReport->setKnowledgeUsage($report['knowledge_usage']);
        $newReport->setRecipient($report['recipient']);
        $newReport->setPlanOfKnowledgeUsage($report['plan_of_knowledge_usage']);
        $newReport->setPastEvents($report['past_events']);
        $newReport->setDifficulties($report['difficulties']);
        $newReport->setSolvingProblemsWays($report['solving_problems_ways']);
        $newReport->setRecommendations($report['recommendations']);
        $newReport->setSuccessStory($report['successStory']);
        $newReport->setContract($report['contract']);
        $newReport->setProjectName($report['project_name']);
        $newReport->setAuthorizedName($report['$authorized_name']);
        $newReport->setAuthorizedPosition($report['$authorized_position']);
        $newReport->setAuthorizedPhoneCode($report['authorized_phone_code']);
        $newReport->setAuthorizedPhone($report['authorized_phone']);
        $newReport->setAuthorizedMobilePhoneCode($report['authorized_mobile_phone_code']);
        $newReport->setAuthorizedMobilePhone($report['authorized_mobile_phone']);
        $newReport->setFeedback($report['feedback']);
        $newReport->setAuthorizedEmail($report['authorized_email']);
        $newReport->setPurpose($report['purpose']);
        $newReport->setTotalTransportCost($report['total_transport_cost']);
        $newReport->setTotalInvolvedCost($report['total_involved_cost']);
        $newReport->setTotalEventCost($report['total_event_cost']);
        $newReport->setTotalOtherCost($report['total_other_cost']);
        $newReport->setReceiptOfDonations($report['receipt_of_donations']);
        $newReport->setPeriodCosts($report['period_costs']);
        $newReport->setDonationBalance($report['donation_balance']);
        $newReport->setConfirmingDocument($report['confirming_document']);
        $newReport->setBeneficiaryGroupName($report['beneficiary_group_name']);
        $newReport->setIsAccepted($report['is_accepted']);
        $newReport->setIsValid($report['is_valid']);
        $newReport->setIsSend($report['is_send']);
        $newReport->setPsrn($report['psrn']);
        $newReport->setCreatedAt(new DateTime($report['created_at']));
        $newReport->setUpdatedAt(new DateTime($report['updated_at']));
        $newReport->setDeadLineStart(new DateTime($report['deadLineStart']));
        $newReport->setDeadLineFinish(new DateTime($report['deadLineFinish']));

        foreach ($newReport->getImmediateResults() as $pseudoResults) {
            $newReport->removeImmediateResult($pseudoResults);
        }

        return  $newReport;
    }

    /**
     *@param $report array
     *
     *@return  Organization
     */
    private function getOrganization(array $report)
    {
        $organization = null;
        if ($report['organization_id']) {
            $name = $report['organization_name'];
            $topicName = $report[self::ORGANIZATION_SHIP_NAME];
            $traineeship = $this->getTraineeship($report, self::ORGANIZATION_TOPIC_ID, self::ORGANIZATION_SHIP_NAME);

            $organization = $this->em->getRepository(Organization::class)->findOneBy([
                'organizationName' => $name,
                'traineeshipTopicId' => $traineeship,
            ]);
            if (!$organization) {
                $organization = new Organization();
                $organization->setTraineeshipTopicId($traineeship);
                $organization->setOrganizationName($name);
                $this->em->persist($organization);
            }
        }
        return $organization;
    }

    /**
     *@param $report array
     *@param $keyId string
     *@param $keyName string
     *
     *@return  TraineeshipTopic
     */
    private function getTraineeship(array $report, $keyId = "topic_id", $keyName = 'topic_name')
    {
        $traineeship = null;

        if ($report[$keyId]) {
            $name = $report[$keyName];
            /** @var TraineeshipTopic $ship */
            foreach ($this->traineeships as $ship) {
                if ($ship->getTopicName() == $name) {
                    $traineeship = $ship;
                    break;
                }
            }

            if (!$traineeship) {
                $traineeship = $this->em->getRepository(TraineeshipTopic::class)->findOneBy([
                    'topicName' => $name
                ]);
            }

            if (!$traineeship) {
                $traineeship = new TraineeshipTopic();
                $traineeship->setTopicName($name);
                $this->em->persist($traineeship);
                $this->traineeships->add($traineeship);
            }
        }

        return $traineeship;
    }
}
