<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RestoreOldKnsCommand extends ContainerAwareCommand
{
    const SPREAD_HISTORIES = [

























        'КНС-2017-1' => [
            15223,
            15218,
            15211,
            15196,
            15165,
            15164,
            15138,
            15135,
            15122,
            15109,
            15103,
            15091,
            15075,
            15046,
            15042,
            15041,
            15033,
            15027,
            15011,
            14991
        ]
    ];

    protected function configure()
    {
        $this->setName('nko:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $repository = $em->getRepository(ApplicationHistory::class);
        foreach (self::SPREAD_HISTORIES as $competitionHistoryIds) {
            foreach ($competitionHistoryIds as $historyId) {
                /** @var ApplicationHistory $history */
                $history = $repository->find($historyId);
                $application = $this->getContainer()->get('nko.order.revert_manager')->revert($history);
                $application->setIsSend(true);
                $history->setIsSend(true);
                $em->flush();

                $output->writeln($historyId . ' restored.');
            }
        }
    }
}
