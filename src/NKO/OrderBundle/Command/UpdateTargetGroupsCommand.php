<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 4/4/18
 * Time: 2:50 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\RelatedTargetGroup;
use NKO\OrderBundle\Traits\CollectionHandler;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateTargetGroupsCommand extends ContainerAwareCommand
{
    use CollectionHandler;

    protected function configure()
    {
        $this
            ->setName('nko:update-target-groups')
            ->setDescription('Update target group for 3.8.1 table at farvater application 2018')
            ->setHelp("'Update target group: set values from brief application 2018 people categories
                to linked entity for the 3.8.1 table at farvater application 2018");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $applications = $em->getRepository(Application::class)->findAll();

        if (!$applications) {
            $output->writeln('Applications for farvater 2018 application wasnt found!');
            return null;
        }

        $output->writeln('Number of Applications: ' . count($applications));

        foreach ($applications as $application) {
            $targetGroups = $em->getRepository(RelatedTargetGroup::class)->findBy([
                'projectApplication' => $application
            ]);
            $targetGroups = $this->modifyToStringArray($targetGroups);

            $relatedApplicationHistory = $this->getContainer()->get('nko_order.resolver.related_competition_resolver')->getApplicationHistory($application);

            if (!$relatedApplicationHistory) {
                $output->writeln('Cant find send brief application 2018 with author ' .
                    $application->getAuthor()->getId() . ' ' . $applications->getAuthor()->getEmail());
                return null;
            }

            $relatedApplication = unserialize($relatedApplicationHistory->getData());
            $relatedTargetGroups = $this->modifyToStringArray($relatedApplication->getPeopleCategories());

            $newTargetGroups = array_diff($relatedTargetGroups, $targetGroups);

            if (!$newTargetGroups) {
                continue;
            }

            foreach ($newTargetGroups as $group) {
                $output->writeln('Create new group ' . $group . ' for application ' . $application->getId());
                $newGroup = new RelatedTargetGroup();
                $newGroup->setProjectApplication($application);
                $newGroup->setTitle($group);
                $em->persist($newGroup);
            }

        }

        $output->writeln("");
        $output->writeln("Writing to database... ");

        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }
}