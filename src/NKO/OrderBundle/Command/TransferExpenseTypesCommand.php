<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.8.18
 * Time: 14.00
 */

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\ExpenseTypeConfig;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as FinanceReport2018;

class TransferExpenseTypesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:transfer-expense-type')
            ->setDescription('Transfer expense type to numbered expense types');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $reportForms = $em->getRepository(ReportForm::class)->findBy(['reportClass'=>[FinanceReport::class, FinanceReport2018::class]]);

        $output->writeln("Found ".count($reportForms)." report forms");

        foreach ($reportForms as $reportForm) {
            $expenseTypes = $reportForm->getExpenseTypes();
            $number = 0;

            foreach ($expenseTypes as $expenseType) {
                ++$number;
                $expenseTypeConfig = new ExpenseTypeConfig();
                $expenseTypeConfig->setNumber($number);
                $expenseTypeConfig->setExpenseType($expenseType);
                $reportForm->addExpenseTypeConfig($expenseTypeConfig);
                $em->persist($expenseTypeConfig);
            }
        }

        $output->writeln("");
        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("\nSUCCESS: ".count($reportForms)." report forms changed");
    }
}
