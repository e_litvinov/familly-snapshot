<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 27.01.17
 * Time: 11:39
 */

namespace NKO\OrderBundle\Command;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\SiteLink;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseApplication;



class TransferMissingAppFieldsCommand extends ContainerAwareCommand
{
    const COLUMN_NAMES = [
        'head_of_project_position',
        'children_category_name',
        'ready_to_sent',
        'is_send',
        'competition_id',
        'author',
        'sendVersion',
        'regulation',
        'version'

    ];

    const BACKUP_TABLE_NAME = 'old_application';

    protected function configure()
    {
        $this
            ->setName('nko:transfer-fields');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var EntityManager $entityManager
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $connection = $em->getConnection();

        $output->writeln("Transferring data to base_application...");
        $columns = implode(',', self::COLUMN_NAMES);
        $query = 'select '.$columns.',id from '.self::BACKUP_TABLE_NAME. ";";
        $statement = $connection->prepare($query);
        $statement->execute();
        $selections = $statement->fetchAll();

        foreach ($selections as $selection) {
            foreach ($selection as $key => $value) {
                if($key != 'id') {
                    $query = " update base_application set {$key} = '{$value}' where base_application.id =  {$selection['id']};";
                    $connection->query($query)->execute();
                }
            }
        }
        $output->writeln("\t data transferred.");

        $output->writeln("\nSUCCESS: Fields transferred.");
    }

}