<?php
namespace NKO\OrderBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;


class SetApplicationNumberCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:set-number')
            ->setDescription('Set number for application')
            ->setHelp("Set number")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em= $this->getContainer()->get('doctrine')->getManager();
        $applicationHistory = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findByIsSpread();

        $i = 0;
        foreach ($applicationHistory as $app) {
            $app->setNumber(++$i);
        }

        try {
            $em->flush();
            $output->writeln($i.' rows updated successfully.');
        }
        catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }
    }
}