<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 9/26/17
 * Time: 2:51 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication;
use NKO\OrderBundle\Entity\MarkList;
use NKO\UserBundle\Entity\ExpertUser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanExpertActivityCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->addArgument('competition_id', InputArgument::REQUIRED, 'Competition ID')
            ->addArgument('date', InputArgument::OPTIONAL, 'date format d-m-Y')
            ->setName('nko:clean-expert-activity');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $competition = $em->getRepository(Competition::class)->find((int)$input->getArgument('competition_id'));
        /**
         * @var ExpertUser[] $users
         */
        $users = $em->getRepository(ExpertUser::class)->findAll();
        /**
         * $@var ExpertUser $user
         */
        foreach($users as $user) {
            if($user->getCompetitions()->contains($competition)) {
                $date = $input->getArgument('date');
                $dateTime = \DateTime::createFromFormat('d-m-Y', $date);
                $date ? $user->setLastLogin($dateTime) : $user->setLastLogin($date);
            }
        }
        $em->flush();
    }
}