<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Farvater2017\BaseActivity;
use NKO\OrderBundle\Entity\Farvater2017\PracticeImplementationActivity;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application;

class FixBaseActivityHistoryIdsCommand extends ContainerAwareCommand
{
    const ID = 3136;

    /** @var EntityManager*/
    private $em;

    protected function configure()
    {
        $this
            ->setName('nko:fix-base-activity')
            ->addArgument('id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id') ? (int) $input->getArgument('id') : self::ID;

        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();
        /** @var Application $application */
        $application = $this->em->getRepository(BaseApplication::class)->find($id);

        $history = $this->em->getRepository(ApplicationHistory::class)->findSpreadByAuthor($application->getAuthor(), $application->getCompetition()->getId());
        $historyApplication = unserialize($history->getData());
        $this->fixApplicationActivity($application, $historyApplication);
        $history->setData(serialize($historyApplication));
        $this->em->flush();
    }
    protected function fixApplicationActivity(Application $application, Application $historyApplication)
    {
        $activities = $application->getPracticeImplementationActivities();
        $historyActivities = $historyApplication->getPracticeImplementationActivities()->map(function (BaseActivity $activity) use ($activities, $application) {
            $resultActivities = $activities->filter(function (BaseActivity $baseActivity) use ($activity) {
                return (
                    $activity->getActivity() == $baseActivity->getActivity() &&
                    $activity->getDeadLine() == $baseActivity->getDeadLine() &&
                    $activity->getExpectedResults() == $baseActivity->getExpectedResults()
                );
            });

            if (count($resultActivities) == 1) {
                $activity->setStaticId($resultActivities->first()->getStaticId());
            } else {
                $newActivity = new PracticeImplementationActivity();
                $this->em->persist($newActivity);
                $newActivity->setActivity($activity->getActivity());
                $newActivity->setDeadLine($activity->getDeadLine());
                $newActivity->setExpectedResults($activity->getExpectedResults());
                $newActivity->setFarvater2017Application($application);
                $newActivity->setDeletedAt(new \DateTime());
                $this->em->flush();
                $this->em->detach($newActivity);

                $activity = $newActivity;
            }

            return $activity;
        });
        $historyApplication->setPracticeImplementationActivities($historyActivities);
    }
}
