<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetApplicationHistoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:reset-history')
            ->setDescription('Reset is_send field for not valid application histories')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for competitions...");
        $competitions = $em->getRepository('NKOOrderBundle:Competition')->findAll();

        foreach ($competitions as $competition) {
            $output->writeln("");
            $output->writeln("Found competition: " . $competition->getName());

            $histories = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findBy(['competition' => $competition->getId()]);
            $output->writeln("Histories for " . $competition->getName() . " : " . count($histories));

            $validHistories = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findByLastCreatedAt($competition->getId());
            $output->writeln("Valid histories for " . $competition->getName() . " : " . count($validHistories));
            $output->writeln("Searching for histories to reset ... ");
            $readyToReset = true;
            foreach ($histories as $history) {
                foreach ($validHistories as $validHistory) {
                    if ($history->getId() == $validHistory->getId()) {
                        $readyToReset = false;
                    }
                }
                if ($readyToReset) {
                    $output->writeln("Reset history with id = ".$history->getId());
                    $history->setIsSend(false);
                }
                $readyToReset = true;
            }
        }

        $output->writeln("");
        $output->writeln("Writing to database... ");

        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");

        $output->writeln("");
        foreach ($competitions as $competition) {
            $validHistories = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findBy(['isSend' => true, 'competition' => $competition->getId()]);
            $output->writeln("Sent histories (is_send = true) for ".$competition->getName()." : ".count($validHistories));
        }
    }

}