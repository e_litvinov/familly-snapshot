<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 06.01.17
 * Time: 13:51
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class BackupTableCommand extends ContainerAwareCommand
{
    const TABLE_NAME = 'table_name';
    const FK = 'FK';
    const FK_TEMP = 'FK_TEMP';

    protected function configure()
    {
        $this
            ->addArgument(self::TABLE_NAME, InputArgument::REQUIRED)
            ->setName('nko:backup-table')
            ->setDescription('Create backup table')
            ->setHelp("Create backup table from your_table with your_table_backup name");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = $input->getArgument(self::TABLE_NAME);
        $backupTable = 'old_'.$table;

        /**
         * @var EntityManager $entityManager
         */
        $entityManager= $this->getContainer()->get('doctrine')->getManager();
        $connection = $entityManager->getConnection();

        $output->writeln("Removing ".$backupTable." table if exists... ");
        $query = 'drop table if EXISTS '.$backupTable.';';
        $connection->prepare($query)->execute();
        $output->writeln("\t table removed.");

        $output->writeln("Copying ".$table." table... ");
        $query = 'show create table '.$table.';';
        $result = $connection->prepare($query);
        $result->execute();
        $createTableQuery = $result->fetch()['Create Table'];
        $output->writeln("\t table copied.");

        $output->writeln("Creating ".$backupTable." table...");
        $createTableQuery = preg_replace('/'.$table.'/', $backupTable, $createTableQuery);
        $createTableQuery = preg_replace('/'.self::FK.'/', self::FK_TEMP, $createTableQuery);
        $createTableQuery = $connection->prepare($createTableQuery);
        $createTableQuery->execute();
        $output->writeln("\t table created.");

        $output->writeln("Transferring data from ".$table." to ".$backupTable."...");
        $query = 'insert into '.$backupTable.' select * from '.$table.';';
        $transferData = $connection->prepare($query);
        $transferData->execute();
        $output->writeln("\t data transferred.");

        $output->writeln("\nIMPORTANT: Backup table name is '".$backupTable."'");
        $output->writeln("\nSUCCESS: Backup created.");
    }

}