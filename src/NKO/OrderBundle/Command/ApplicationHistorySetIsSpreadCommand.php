<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 12.9.18
 * Time: 11.29
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ApplicationHistorySetIsSpreadCommand extends ContainerAwareCommand
{
    const COMPETITIONS = [
        16 => [
            1072723007422,
            5087746371510,
            1022401160462,
            1021801668063,
        ],
        17 => [
            1034316536320,
            1022500861701,
        ]
    ];

    /** @var $em  EntityManager*/
    private $em;

    protected function configure()
    {
        $this
            ->setName('nko:setIsSpread')
            ->setDescription('set isSpread');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        foreach (self::COMPETITIONS as $key => $competition) {
            $maxNumber = $this->em->getRepository(ApplicationHistory::class)->findMaxNumber($key) ?: 0;
            $sendApplications = $this->em->createQueryBuilder()
                ->select('a')
                ->from(ApplicationHistory::class, 'a')
                ->innerJoin('a.competition', 'c')
                ->innerJoin('a.author', 'aut')
                ->where('c.id = :competitionId')
                ->andWhere('aut.psrn IN (:competition)')
                ->andWhere('a.isSend = :isSend')
                ->setParameters(['competitionId' => $key, 'competition' => $competition, 'isSend' => true])
                ->getQuery()
                ->getResult();

            $output->writeln('Found '. count($sendApplications).' applications');

            if ($sendApplications) {
                foreach ($sendApplications as $application) {
                    $application->setIsSpread(true);
                    $application->setNumber(++$maxNumber);
                }
            }
        }

        $output->writeln('Write to bd');
        $this->em->flush();
        $output->writeln('End');
    }
}
