<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Admin\BaseReportAdmin;
use NKO\OrderBundle\Entity\ApplicationHistory;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\UserBundle\Entity\NKOUser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SaveReportHistoryCommand extends ContainerAwareCommand
{
    const REPORT_FORM_ID = 'report_form_id';

    protected function configure()
    {
        $this
            ->addArgument(self::REPORT_FORM_ID, InputArgument::REQUIRED)
            ->setName('nko:create-report-history')
            ->setDescription('Create send report histories for send reports')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reportFormId = $input->getArgument(self::REPORT_FORM_ID);

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $reportForm = $em->getRepository(Report\ReportForm::class)->find($reportFormId);
        $output->writeln("Found report form " . $reportForm->getTitle());

        $output->writeln("Searching for send report histories...");
        $reportHistories = $em->getRepository(ReportHistory::class)->findBy([
            'isSend' => true,
            'reportForm' => $reportFormId
        ]);

        foreach ($reportHistories as $history) {
            $output->writeln("");
            $output->writeln("Found history with id = " . $history->getId());

            $user = $em->getRepository(NKOUser::class)->findOneBy([
                'psrn' => $history->getPsrn()
            ]);
            $output->writeln("Found user " . $user->getEmail() . ' ' . $user->getPsrn());

            $report = $em->getRepository(BaseReport::class)->findOneBy([
                'author' => $user,
                'reportForm' => $reportFormId
            ]);
            $output->writeln("Found report with id = " . $report->getId());

            $output->writeln("Prepare report for serialization.");
            $report = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($report);
            $report = $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($report);
            $report = $this->resolveNestedCollections($report);
            $output->writeln("Prepared");

            $newHistory = $this->getContainer()->get('nko.archiver.report_archiver')->archive($report);
            $output->writeln("Created new history");

            $em->persist($newHistory);

            $newHistory->setIsSend(false);
            $newHistory->setIsSend(true);
            $newHistory->setReportForm($reportForm);
            $newHistory->setPeriod($history->getPeriod());
        }

        $output->writeln("");
        $output->writeln("Writing to database... ");

        $em->flush();

        $output->writeln("");
        $output->writeln("SUCCESS");
    }

    private function resolveNestedCollections($report)
    {
        switch (get_class($report)) {
            case Report\FinanceReport\Report::class:
                foreach ($report->getRegisters() as $register) {
                    $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($register);
                }
                break;
            case Report\MonitoringReport\Report::class:
                foreach ($report->getMonitoringResults() as $monitoringResult) {
                    $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($monitoringResult);
                }
                break;
            case Report\AnalyticReport\Report::class:
                foreach ($report->getPracticeFeedbackAttachments() as $feedbackAttachment) {
                    $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($feedbackAttachment);
                }

                foreach ($report->getPracticeSpreadFeedbackAttachments() as $feedbackAttachment) {
                    $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($feedbackAttachment);
                }
                break;
        }

        return $report;
    }
}