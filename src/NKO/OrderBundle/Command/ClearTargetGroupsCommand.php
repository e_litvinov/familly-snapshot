<?php

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\Application\KNS\SecondStage2018\Application;

class ClearTargetGroupsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:clear-target-groups')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $application = $em->getRepository(Application::class)->find(3135);

        if (!$application) {
            $output->writeln("Application not found");
            return;
        }

        $peopleCategories = $application->getPeopleCategories();
        foreach ($peopleCategories as $category) {
            $application->removePeopleCategory($category);
        }

        $beneficiaryProblems = $application->getBeneficiaryProblems();
        foreach ($beneficiaryProblems as $problem) {
            $application->removeBeneficiaryProblem($problem);
        }

        try {
            $em->flush();
            $output->writeln("Success");
        } catch (Exception $e) {
            $output->writeln("NOT successful.");
        }
    }
}
