<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;

class UpdateTotalValuesFinanceReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-total-values')
            ->setDescription('Update total values of finance report')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $reports = $em
            ->createQueryBuilder()
            ->select('p')
            ->from(Report::class, 'p')
            ->innerjoin('p.reportForm', 'rf')
            ->innerjoin('p.period', 'period')
            ->where('rf.id = :rf')
            ->andWhere('period.id = :period')
            ->setParameters([
                'rf' => 39,
                'period' => 1047,
            ])
            ->getQuery()
            ->getResult();

        $output->writeln("Found ". count($reports). " reports");

        $this->getContainer()->get('nko_order.finance_report_manager.finance_spent')->updateTotalValues($reports);

        try {
            $em->flush();
            $output->writeln("Success");
        } catch (Exception $e) {
            $output->writeln("NOT successful.");
        }
    }
}
