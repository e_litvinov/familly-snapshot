<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/23/17
 * Time: 2:56 PM
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2016\Application;
use NKO\OrderBundle\Entity\Mark;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\QuestionMark;
use NKO\UserBundle\Entity\ExpertUser;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\ORMException as Exception;

class TransferBriefFarvaterMarksCommand extends ContainerAwareCommand
{
    private $em;

    const QUESTIONS = [ 'COMMENT1' => 'Эффективность практики (убедительность доказательств, включая непосредственный практический опыт Заявителя)',
                        'COMMENT2' =>'Результативность практики для Конкурса и Программы',
                        'COMMENT3' => 'Преимущественная ориентация практики на «сложные» категории детей',
                        'COMMENT4' => 'Ориентация проекта на внедрение практики другими заинтересованными организациями и специалистами; на формирование отраслевых ресурсных центров – стажировочных площадок',
                        'COMMENT5' => 'Потенциал практики с точки зрения ее внедрения в деятельность других организаций и специалистов сферы защиты детства',
                        'COMMENT6' => 'Заинтересованность организации-заявителя в повышении эффективности практики и своей деятельности за счет мониторинга и оценки результатов',
                        'COMMENT7' => 'Продуманность системы мониторинга и оценки результатов проекта',
                        'COMMENT8' => 'Логичность и реалистичность проекта',
                        'COMMENT9' => 'Устойчивость результатов проекта',
                        'COMMENT10' => 'Обоснованность и эффективность бюджета проекта',
                        'COMMENT11' => 'Доля со-финансирования (за счет собственных средств и средств организаций-партнеров) в бюджете проекта',
                        'COMMENT12' => 'Организационный потенциал Заявителя'
    ];

    const MARK_QUESTION = [
        'COMMENT1' => 'VOTE1',
        'COMMENT2' => 'VOTE2',
        'COMMENT3' => 'VOTE3',
        'COMMENT4' => 'VOTE4',
        'COMMENT5' => 'VOTE5',
        'COMMENT6' => 'VOTE6',
        'COMMENT7' => 'VOTE7',
        'COMMENT8' => 'VOTE8',
        'COMMENT9' => 'VOTE9',
        'COMMENT10' => 'VOTE10',
        'COMMENT11' => 'VOTE11',
        'COMMENT12' => 'VOTE12'
    ];

    protected function configure()
    {
        $this
            ->setName('nko:transfer-brief-farvater-marks')
            ->setDescription('Transfer marks from brief farvater 2016')
            ->setHelp("Transfer marks from brief farvater 2016");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $io = new SymfonyStyle($input, $output);
        $io->title('Import marks start');
        $dbName = $io->ask('Database name');
        $dbHost = $io->ask('Host', '127.0.0.1');
        $dbUser = $io->ask('User', 'root');
        $dbPassword = $io->askHidden('Password');

        $connectionFactory = $this->getContainer()->get('doctrine.dbal.connection_factory');
        $dsn = 'mysql:dbname=' . $dbName . ';host=' . $dbHost . ';charset=UTF8';
        $connection = $connectionFactory->createConnection(
            array('pdo' => new \PDO($dsn, $dbUser, $dbPassword))
        );
        $output->writeln('Get all marks');
        $farvaterFields = $connection->fetchAll('
            select
     	        b_iblock_element_property.VALUE,
                IBLOCK_PROPERTY_ID,
                b_iblock_property.CODE,
                b_iblock_property.NAME,
                b_iblock_element.NAME,
                b_iblock_element.ID,
                b_iblock_element.NAME,
                user.EMAIL,
                b_iblock_property_enum.VALUE  MARK
            from b_iblock_element_property
            inner join b_iblock_property
            on b_iblock_property.ID = b_iblock_element_property.IBLOCK_PROPERTY_ID
            inner join b_iblock_element       
            on b_iblock_element.ID = b_iblock_element_property.IBLOCK_ELEMENT_ID
			inner join b_user  user on b_iblock_element.MODIFIED_BY = user.id
            left join b_iblock_property_enum
            on b_iblock_element_property.VALUE = b_iblock_property_enum.ID
            where b_iblock_property.IBLOCK_ID = 6
            order by b_iblock_element.ID');

        $competition = $this->em
            ->getRepository('NKOOrderBundle:Competition')
            ->findOneBy(array('name' => 'Краткая заявка СФ-2016'));

        $applicationHistories = $this->em
            ->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findBy(array('competition' => $competition));
        $i = 0;
        $appNumber = 1;

        $this->createDefaultExpert($competition);

        $minIds = [];

        foreach($farvaterFields as $field){
            if ($field['CODE'] == 'ORDER'){
                foreach($farvaterFields as $item){
                    if($item['CODE'] == 'ORDER'
                        && $field['VALUE'] == $item['VALUE']
                        && $field['EMAIL'] == $item['EMAIL']
                        && $field['ID'] != $item['ID']){
                        $minIds[] = min([$field['ID'],$item['ID']]);
                    }
                }
            }
        }

        foreach($farvaterFields as $key => $value){
            foreach($minIds as $id){
                if($value['ID'] == $id){

                    unset($farvaterFields[$key]);
                }
            }
        }

        $output->writeln("Delete duplicated marks");

        foreach($farvaterFields as $field) {
            if ($field['CODE'] == 'ORDER') { //$field['VALUE'] id application

                /**
                 * @var ApplicationHistory $applicationHistory
                 */
                $applicationHistory = $this->findApplicationHistory($applicationHistories, $field['VALUE']);

                if($applicationHistory){

                    $markList = new MarkList();

                    /**
                     * @var ExpertUser $expert
                     */
                    $expert = $this->findExpert($field['EMAIL']);

                    if(!$expert->getCompetitions()->contains($competition)){
                        $expert->addCompetition($competition);
                    }
                    $markList->setExpert($expert);
                    $expert->addMarkList($markList);

                    $markList->setApplication($applicationHistory);
                    if(!$applicationHistory->getExperts()->contains($expert)){
                        $applicationHistory->addExpert($expert);
                    }

                    $markList->setCoefficient(1);

                    $id = $field['ID'];
                    $this->createMarks($farvaterFields, $field, $markList);
                    $this->setMarkListValues($farvaterFields, $id, $markList);
                    $this->setFinalMark($markList);

                    $appNumber = $this->spreadApplicationHistory($applicationHistory, $appNumber);
                    $this->sendApplication($applicationHistory);
                    $this->setTotalMark($markList);
                    $this->em->persist($markList);
                    $i++;
                    $output->writeln("Create new mark List.Application history {$markList->getApplication()->getId()} Expert {$markList->getExpert()}");

                }
                else {
                    $output->writeln("Application with id {$field['VALUE']} not found");

                }
            }

        }

        try {
            $this->em->flush();
            $output->writeln($i.' marks created successfully.');
        }
        catch (Exception $e) {
            $output->writeln(" marks create was NOT successful.");
        }
    }

    private function findApplicationHistory($applicationHistories, $id)
    {
        foreach($applicationHistories as $applicationHistory){
            if(unserialize($applicationHistory->getData())->getOldId() == $id){
                return $applicationHistory;
            }
        }

        return null;
    }

    /**
     * @param $farvaterFields
     * @param $id
     * @param $markList MarkList
     */
    private function setMarkListValues($farvaterFields, $id,$markList )
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $markList->setIsValid(true);
        $markList->setIsFirstTimeSend(true);
        $markList->setIsSend(true);
        foreach($farvaterFields as $field){
            if($field['ID'] == $id){
                switch ($field['CODE']){
                    case 'RESULT_CONDITION':{
                        $markList->setRationaleFinalDecision($field['VALUE']);
                        break;
                    }
                    case 'RESULT':{
                        $finalDecision = $em->getRepository('NKOOrderBundle:FinalDecision')->findOneBy(array('name' => $field['MARK']));
                        $markList->setFinalDecision($finalDecision);
                        break;
                    }
                    default:
                        break;
                }
            }

        }

    }

    private function createMarks($farvaterFields, $field, $markList)
    {
        foreach($farvaterFields as $item){
            if($field['ID'] == $item['ID'] && $item['CODE'] != 'ORDER'){
                $mark = new Mark();
                if(array_key_exists($item['CODE'], self::QUESTIONS)){
                    $question = $this->em
                        ->getRepository('NKOOrderBundle:QuestionMark')
                        ->findOneBy(array('question' => self::QUESTIONS[$item['CODE']]));
                    $mark->setQuestionMark($question); //set question
                    $mark->setRationale($item['VALUE']);
                    $mark->setMark($this->findVote($item, $farvaterFields));
                    $markList->addMark(clone $mark);
                }
            }
        }
    }

    private function findVote($item, $farvaterFields)
    {
        if((array_key_exists($item['CODE'], self::MARK_QUESTION))){
            $code = self::MARK_QUESTION[$item['CODE']];
            $id = $item['ID'];
            foreach ($farvaterFields as $vote){
                if($vote['CODE'] == $code && $vote['ID'] == $id){
                    return $vote['MARK'];
                }
            }
        }

        return null;
    }

    private function setFinalMark($markList)
    {
        $finalMark = 0;
        if($markList) {
            foreach ($markList->getMarks() as $mark) {
                $finalMark += $mark->getMark();
            }
        }
        $markList->setFinalMark($finalMark);
    }

    private function findExpert($email)
    {
        switch ($email) {
            case 'fsa.sos-russia@mail.ru':{
                $email = 'Igor.georgi@mail.ru';
                break;
            }

            case 'natalia@processconsulting.ru':{
                $email = 'nkochele@yandex.ru';
                break;
            }

            case 'gkurganova@fondkluch.ru':{
                $email = 'gkurganova@yandex.ru';
                break;
            }

            case 'aborovykh@yandex.ru':{
                $email = 'a.borovykh@yandex.ru';
                break;
            }

            case 'ntyushkevich@gmail.com':{
                $email = 'natashat@list.ru';
                break;
            }

            case 'mnesterova@fondkluch.ru':{
                $email = 'expert@admin.com';
                break;
            }

        }
        $expert = $this->em->getRepository('NKOUserBundle:ExpertUser')->findOneBy(array('email' => $email));
        return $expert;
    }

    /**
     * @param $applicationHistory ApplicationHistory
     * @param $em
     * @param $appNumber
     * @return mixed
     */
    private function spreadApplicationHistory($applicationHistory, $appNumber)
    {
        $applicationHistory->setIsSpread(true);
        $applicationHistory->setIsAppropriate(true);
        $applicationHistory->setIsSend(true);
        if(!$applicationHistory->getNumber()){
            $applicationHistory->setNumber($appNumber++);
        }

        return $appNumber;

    }

    /**
     * @param $applicationHistory
     * @param $em EntityManager
     */
    private function sendApplication($applicationHistory)
    {
        $applicationId = unserialize($applicationHistory->getData())->getId();
        /**
         * @var Application $application
         */
        $application = $this->em
            ->getRepository('NKOOrderBundle:BriefApplication2016\Application')
            ->find($applicationId);
        $application->setIsSend(true);
        $application->setIsFirstTimeSent(true);
        $application->setReadyToSent(true);
    }

    private function createDefaultExpert($competition)
    {
        if(!$this->em->getRepository('NKOUserBundle:ExpertUser')->findOneBy(['email' =>'expert@admin.com' ])){
            $adminExpert  = new ExpertUser();
            $adminExpert->setEmail('expert@admin.com');
            $adminExpert->setPassword('admin');
            $adminExpert->setUsernameCanonical('expert@admin.com');
            $adminExpert->setEmailCanonical('expert@admin.com');
            $adminExpert->setSurname('Эксперт');
            $adminExpert->setName('Эксперт');
            $adminExpert->setUsername('expert@admin.com');
            $adminExpert->addRole('ROLE_EXPERT');
            $adminExpert->setEnabled(true);
            $adminExpert->addCompetition($competition);
            $this->em->persist($adminExpert);
            $this->em->flush();
        }
    }

    /**
     * @param MarkList $markList
     */
    private function setTotalMark($markList) {
        $total = 0;
        /**
         * @var Mark $mark
         */
        foreach ($markList->getMarks() as $mark) {
            $total += $mark->getMark();
        }

        $markList->setFinalMark($total);
    }

}