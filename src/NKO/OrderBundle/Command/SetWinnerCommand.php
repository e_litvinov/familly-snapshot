<?php
namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;

class SetWinnerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:set-winner')
            ->setDescription('Set winner for linked brief application')
            ->setHelp("Set winner")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em= $this->getContainer()->get('doctrine')->getManager();
        $applicationHistories = $em->getRepository(ApplicationHistory::class)->findBy([
            'isSpread' => true,
            'competition' => 23,
            'winner' => true
        ]);

        foreach ($applicationHistories as $history) {
            $unserializedObject = unserialize($history->getData());
            $output->writeln("Application history id " . $history->getId() . ", application id " . $unserializedObject->getId() . " user " . $history->getNkoName());

            $briefApplicationHistory = $this->getContainer()
                ->get('nko_order.resolver.related_competition_resolver')
                ->getApplicationHistory($unserializedObject, $history->getIsSpread());
            $briefApplicationHistory->setWinner($history->getWinner());

            $briefApplication = unserialize($briefApplicationHistory->getData());
            $briefApplication = $em->getRepository(BaseApplication::class)
                ->find($briefApplication->getId());
            $briefApplication->setWinner($history->getWinner());
        }

        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("Success.");
    }
}