<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Doctrine\ORM\ORMException as Exception;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UpdateExcelHarobCommand extends ContainerAwareCommand
{
    const PATTERN = '/([a-zA-Z0-9]+)\./';
    const PATTERN_EXTENSION = '/\.([a-z0-9]+)/';

    protected function configure()
    {
        $this->setName('nko:update-files-harbor');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $applications = $em->getRepository(Application::class)->findAll();
        $this->process($applications, 'budget', 'xls', $output);
        $this->process($applications, 'subjectRegulation', 'doc', $output);

        $applications = $em->getRepository(ApplicationHistory::class)->findByNameOfApplication(Application::class);
        $this->process($applications, 'budget', 'xls', $output);
        $this->process($applications, 'subjectRegulation', 'doc', $output);

        try {
            $em->flush();
            $output->writeln("\nSuccess");
        } catch (Exception $e) {
            $output->writeln("\nNOT successful.");
        }
    }

    protected function process($applications, $field, $defaultFormat, OutputInterface $output)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if (reset($applications) instanceof ApplicationHistory) {
            $output->writeln("\nChange file format -- ". $defaultFormat.":"."\n");
        }

        foreach ($applications as $application) {
            $isHistory = $application instanceof ApplicationHistory;
            $unserialized = $isHistory ? unserialize($application->getData()) : $application;

            $value = $accessor->getValue($unserialized, $field);

            if (!$value) {
                continue;
            }

            preg_match(self::PATTERN_EXTENSION, $value, $matches_extension);
            preg_match(self::PATTERN, $value, $matches_name);

            $fileName = $matches_name[1];
            $fileExtension = !empty($matches_extension[1]) ? $matches_extension[1] : null;

            $changeFormat = in_array($fileExtension, ['rtf', 'bin', 'zip']);
            if (!$fileExtension || $changeFormat) {
                $accessor->setValue($unserialized, $field, $fileName.'.'.$defaultFormat);

                if ($isHistory) {
                    $output->writeln($value);
                    $application->setData(serialize($unserialized));
                }
            }
        }
    }
}
