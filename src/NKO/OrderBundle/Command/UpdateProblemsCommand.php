<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 9/25/17
 * Time: 4:58 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Application;
use NKO\OrderBundle\Entity\Application\Farvater\BriefApplication2018\Problem;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateProblemsCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-problems')
            ->setDescription('Update beneficiary problems table, add values with parent people categories in brief application 2018')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $apps = $em->getRepository(Application::class)->findBy([
            'winner' => true
        ]);

        $output->writeln(count($apps) . " applications found.");


        /**
         * @var Application $app
         */
        foreach ($apps as $app) {
            $output->writeln("Application: id " . $app->getId() . " author " .  $app->getAuthor());
            $categories = $app->getPeopleCategories();
            $problems = $app->getBeneficiaryProblems();
            foreach ($categories as $category) {
                $problem = $problems->filter(function ($object) use ($category) {
                    return $object->getTargetGroup() == $category->getName();
                })->first();

                if (!$problem) {
                    $output->writeln("Create new beneficiary group with target group " . $category->getName());
                    $problem = new Problem();
                    $em->persist($problem);
                    $problem->setApplication($app);
                    $problem->setTargetGroup($category->getName());
                }
            }
        }


        $output->writeln("Writing to db...");
        $em->flush();
        $output->writeln("SUCCESS");
    }
}