<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Repository\ReportHistoryRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixReceiptOfDonationsCommand extends ContainerAwareCommand
{
    const REPORT_ID = 2147;
    const COUNT_PER_ITERATION = 10;

    /**
     * @var object
     */
    private $manager;


    protected function configure()
    {
        $this
            ->setName('nko:fix-receipt-of-donation')
            ->addArgument('id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        /** @var EntityManager $em */
        $em = $doctrine->getManager();
        $this->manager = $this->getContainer()->get('nko_order.finance_report_manager.finance_spent');

        $report = $em->getRepository(BaseReport::class)->find($input->getArgument('id'));
        $template = $report->getReportTemplate();

        $this->updateReport($report, $template);
        $em->flush();
        $output->writeln("OK");

        $histories = true;
        $offset = 0;
        while ($histories) {
            $histories = $em->getRepository(ReportHistory::class)->findByReportWithLimitAndOffset($report, null, self::COUNT_PER_ITERATION, $offset);
            $offset += self::COUNT_PER_ITERATION;

            foreach ($histories as $history) {
                $report = unserialize($history->getData());

                $this->updateReport($report, $template);
                $history->setData(serialize($report));

                $output->writeln("OK ". $history->getId());
            }

            $em->flush();
            $em->clear();
        }

        $output->writeln("OK");
    }

    private function updateReport(BaseReport $report, BaseReportTemplate $template)
    {
        $this->setApprovalSpent($report, $template);
        $this->manager->updateTotalValues([$report]);
    }

    private function setApprovalSpent(BaseReport $report, BaseReportTemplate $template)
    {
        $financeSpent = $template->getFinanceSpent();

        foreach ($report->getFinanceSpent() as $finance) {
            $expenseType = $finance->getExpenseType();
            $finObject = $financeSpent->filter(function ($object) use ($expenseType) {
                return $expenseType->getId() == $object->getExpenseType()->getId();
            });
            $finance->setApprovedSum($finObject->first()->getApprovedSum());
        }
    }
}
