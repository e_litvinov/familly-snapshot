<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Harbor\Application2019\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindNotSendHarborCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:not-send-harbor-apps')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $competition = $em->getRepository(Competition::class)->findOneBy([
            'applicationClass' => Application::class
        ]);

        $applications = $em->getRepository(Application::class)->findBy([
            'isSend' => true,
        ]);

        $ids = [];
        foreach ($applications as $application) {
            $ids[] = $application->getId();
        }

        $histories = $em->getRepository(ApplicationHistory::class)->findBy([
            'isSend' => true,
            'competition' => $competition,
        ]);

        foreach ($histories as $history) {
            $application = unserialize($history->getData());
            if (!in_array($application->getId(), $ids)) {
                $output->writeln('Send history: ' . $history->getId());
                $output->writeln('Not send application: ' . $application->getId());
                $output->writeln('');
            }
        }
    }
}