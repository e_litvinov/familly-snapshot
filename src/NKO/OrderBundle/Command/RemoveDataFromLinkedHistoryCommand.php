<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 14.12.18
 * Time: 13.12
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Application\Continuation\KNS\Application as ContinuationKNS2019Application;
use NKO\OrderBundle\Entity\Application\Continuation\SecondStage\Application as ContinuationApplicationSeconStage;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication2018;

class RemoveDataFromLinkedHistoryCommand extends ContainerAwareCommand
{
    const APPLICATION_TYPE = [
        ContinuationKNS2019Application::class,
        ContinuationApplicationSeconStage::class,
        ContinuationApplication2018::class
    ];
    const LIMIT = 10;

    /** @var $em EntityManager */
    private $em;

    /** @var OutputInterface $out */
    private $out;

    protected function configure()
    {
        $this
            ->setName('nko:remove-data-from-linked-history')
            ->setDescription("'Delete extra data from application history");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->out = $output;
        $this->em = $this->getContainer()->get('doctrine')->getManager();


        $competitionIds = $this->getCompetitionsIds();

        $this->clearHistorys($competitionIds);
        $this->out->writeln('ok');
    }

    private function getCompetitionsIds()
    {
        $competitionRepository = $this->em->getRepository(Competition::class);

        $competitionIds = [];

        foreach (self::APPLICATION_TYPE as $applicationClass) {
            $competitions = $competitionRepository->findBy(['applicationClass' => $applicationClass]);
            foreach ($competitions as $competition) {
                $competitionIds[] = $competition->getId();
            }
        }

        return $competitionIds;
    }

    private function clearHistorys($competitionIds)
    {
        $this->out->writeln('Start this');
        foreach ($competitionIds as $competitionId) {
            $offset = 0;
            $this->out->writeln('Start competition with id '. (string) ($competitionId));

            while (true) {
                $this->out->writeln('Start history with offset '. (string) ($offset));

                $historys = $this->getHistoryByCompetitionFromIndex($competitionId, $offset, self::LIMIT);

                if (!$historys) {
                    //end of current cometition
                    break;
                }

                $this->clearPortionOfHistories($historys);
                unset($historys);
                $this->em->clear();
                $offset += self::LIMIT;
            }
        }
    }

    private function getHistoryByCompetitionFromIndex($competitionId, $start, $limit = 10)
    {
        return $this->em->createQueryBuilder()
            ->select('h')
            ->from(ApplicationHistory::class, 'h')
            ->where('h.competition = :competitionId')
            ->setParameter('competitionId', $competitionId)
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }

    private function clearPortionOfHistories($histories)
    {
        foreach ($histories as $history) {
            /** @var ContinuationKNS2019Application $object */
            $object = unserialize($history->getData());
            if ($object->getLinkedApplicationHistory()) {
                $this->out->writeln('cleared '. (string) (strlen($object->getLinkedApplicationHistory()->getData())). ' bytes');
                $object->getLinkedApplicationHistory()->setData(null);
            } else {
                $this->out->writeln("i'm useless");
            }
            $history->setData(serialize($object));
        }

        $this->em->flush();
    }
}
