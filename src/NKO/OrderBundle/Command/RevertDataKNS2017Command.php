<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\KNS2017\SecondStageApplication as Application;
use Symfony\Component\PropertyAccess\PropertyAccess;

class RevertDataKNS2017Command extends ContainerAwareCommand
{
    const APPLICATIONS = [
        Application::class,
    ];

    const FIELDS = [
        'abbreviation', 'legalPostCode', 'legalCity', 'legalStreet', 'isAddressesEqual', 'mission',
        'actualPostCode', 'actualCity', 'actualStreet', 'dateRegistrationOfOrganization', 'email',
        'phoneCode', 'phone', 'headOfOrganizationFullName', 'headOfOrganizationPosition',
        'headOfOrganizationPhoneCode', 'headOfOrganizationPhone', 'headOfOrganizationMobilePhoneCode',
        'headOfOrganizationMobilePhone', 'headOfOrganizationEmeil', 'headOfProjectFullName',
        'headOfProjectPosition', 'headOfProjectPhoneCode', 'headOfProjectPhone', 'countVolunteerEmployees',
        'headOfProjectMobilePhoneCode', 'headOfProjectMobilePhone', 'headOfProjectEmeil',
        'headOfAccountingFullName', 'headOfAccountingPhoneCode', 'headOfAccountingPhone',
        'headOfAccountingMobilePhoneCode', 'headOfAccountingMobilePhone', 'linkToAnnualReport',
        'headOfAccountingEmeil', 'primaryActivity',  'countStaffEmployees', 'countInvolvingEmployees',
        'legalRegion', 'actualRegion', 'name', 'pSRN'
    ];

    protected function configure()
    {
        $this
            ->setName('nko:revert-data-application-kns')
            ->setDescription('revert data application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach (self::APPLICATIONS as $application) {
            $applications = $em->getRepository($application)->findBy(['isSend' => true]);
            $competition = $em->getRepository(Competition::class)->findOneBy(['applicationClass' => $application]);
            $applicationHistories = $em->getRepository(ApplicationHistory::class)->findBy(['competition' => $competition, 'isSend' => true]);
            $coincidence = 0;

            $output->writeln('Count applications ' . count($applications));
            $output->writeln('Count applicationHistories ' . count($applicationHistories));
            $output->writeln('Begin to process ' . $competition->getName());

            foreach ($applicationHistories as $history) {
                $author = $history->getAuthor();
                $application = array_filter($applications, function ($application) use ($author) {
                    return $application->getAuthor() === $author;
                });

                if ($application) {
                    $coincidence++;
                    $application = array_shift($application);
                    $data = unserialize($history->getData());

                    foreach (self::FIELDS as $key) {
                        if ($accessor->isWritable($data, $key)) {
                            $value = $accessor->getValue($data, $key);
                            if (is_object($value) && !$value instanceof \DateTime) {
                                $value = $em->getRepository(get_class($value))->find($value);
                            }
                            $accessor->setValue($application, $key, $value);
                        }
                    }
                }
            }

            $output->writeln('Changed applications : '. $coincidence);
            $output->writeln('');
        }

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        } catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }

        $output->writeln("Success.");
    }
}
