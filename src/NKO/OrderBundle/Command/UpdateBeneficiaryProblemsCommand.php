<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 30/08/18
 * Time: 15:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Competition;
use Doctrine\ORM\ORMException as Exception;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UpdateBeneficiaryProblemsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-beneficiary-problems')
            ->setDescription('Update beneficiary problems')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $competitions = $em->getRepository(Competition::class)->findBy(['applicationClass' => ContinuationApplication::class]);
        $output->writeln("\nFound " . count($competitions) . " competitions.");

        foreach ($competitions as $competition) {
            $iteration = 0;
            $notFoundApplication = 0;
            $applicationHistories = $em->getRepository(ApplicationHistory::class)->findSendOrSpreadApplication($competition);
            $output->writeln("\nFound " . count($applicationHistories) . " histories.");

            foreach ($applicationHistories as $history) {
                $unserializedData = unserialize($history->getData());
                $applicationId = $unserializedData->getId();

                $application = $em->getRepository(ContinuationApplication::class)->find($applicationId);

                if (!$application) {
                    $notFoundApplication++;
                    continue;
                }

                $application = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($application);
                $application = $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($application);
                $this->updateNestedCollection($application->getBeneficiaryProblems());
                $serializedObject = serialize($application);
                $history->setData($serializedObject);
                $iteration ++;
            }

            $output->writeln("Updated " . $iteration . " applicationHistories, not found ". $notFoundApplication ." related continuation applications for update. \nCurrent competition -". $competition);
        }


        try {
            $em->flush();
            $output->writeln("\nFlushed successfully!");
        } catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }

        $output->writeln("Success.");
    }

    private function updateNestedCollection($collection)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($collection as $item) {
            $item = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);

            if ($accessor->isReadable($item, 'socialResults')) {
                foreach ($item->getSocialResults() as $result) {
                    $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($result);
                }
            }
        }
    }
}
