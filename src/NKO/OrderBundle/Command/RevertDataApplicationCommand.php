<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use NKO\OrderBundle\Entity\ApplicationHistory;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\Farvater\FarvaterApplication as FarvaterApplication;
use NKO\OrderBundle\Entity\BriefApplication2016\Application as BriefApplication;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;

class RevertDataApplicationCommand extends ContainerAwareCommand
{
    const APPLICATIONS = [
        FarvaterApplication::class,
        BriefApplication::class
    ];

    protected function configure()
    {
        $this
            ->setName('nko:revert-data-application')
            ->setDescription('revert data application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach (self::APPLICATIONS as $application) {
            $applications = $em->getRepository($application)->findBy(['isSend' => true]);
            $competition = $em->getRepository(Competition::class)->findOneBy(['applicationClass' => $application]);
            $applicationHistories = $em->getRepository(ApplicationHistory::class)->findBy(['competition' => $competition, 'isSend' => true]);
            $properties = (new \ReflectionClass($application))->getDefaultProperties();
            $coincidence = 0;

            $output->writeln('Count applications ' . count($applications));
            $output->writeln('Count applicationHistories ' . count($applicationHistories));
            $output->writeln('Begin to process ' . $competition->getName());

            foreach ($applicationHistories as $history) {
                $id = unserialize($history->getData())->getId();
                $application = array_filter($applications, function ($application) use ($id) {
                    return $application->getId() === $id;
                });

                if ($application) {
                    $coincidence++;
                    $application = array_shift($application);
                    $data = unserialize($history->getData());

                    foreach ($properties as $key => $property) {
                        if ($accessor->isWritable($data, $key)) {
                            $value = $accessor->getValue($data, $key);
                            if ($value && !$value instanceof ArrayCollection && !$value instanceof PersistentCollection && !is_object($value)) {
                                $accessor->setValue($application, $key, $value);
                            }
                        }
                    }
                    $application->setAuthor($history->getAuthor());
                }
            }
            $output->writeln('Changed applications : '. $coincidence);
            $output->writeln('');
        }

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        } catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }

        $output->writeln("Success.");
    }
}
