<?php


namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class FixFarvater2018PeriodsCommand extends ContainerAwareCommand
{
    const REPORT_FORM_ID = 62;
    const PERIOD_ID = 1069;

    protected $em;

    protected function configure()
    {
        $this
            ->setName('nko:fix-period-in')
            ->addOption('sended', 'Only send histories', InputOption::VALUE_REQUIRED, 'input n to false', 1)
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();
        $onlySended = $input->getOption('sended') == 'n' ? false : true;

        $reportHistoryRepository = $this->em->getRepository(ReportHistory::class);
        $reportRepository = $this->em->getRepository(BaseReport::class);

        $reports = $reportRepository->findReportsByReportForm(self::REPORT_FORM_ID);

        foreach ($reports as $report) {
            $period = $this->em->getRepository(PeriodReport::class)->find(self::PERIOD_ID);
            $report = $reportRepository->find($report->getId());
            $neededCuntOfPeriods = count($report->getReportForm()->getReportPeriods()) + 1;

            $periods = [];

            $this->fixReport($report, $periods, $neededCuntOfPeriods, $period);

            $output->writeln('start '. $report->getId());
            $this->em->flush();
            $output->writeln('Ok - '. $report->getId());

            if ($periods) {
                $reportHistories = $reportHistoryRepository->findByReport($report, $onlySended);

                foreach ($reportHistories as $reportHistory) {
                    $historyReport = unserialize($reportHistory->getData());
                    $this->fixReport($historyReport, $periods, $neededCuntOfPeriods, $period);
                    $reportHistory->setData(serialize($historyReport));

                    $this->em->flush($reportHistory);
                }
            }
            $output->writeln('history Ok - '. $report->getId());

            $this->em->clear();
        }
    }


    private function findInPeriods(MonitoringResult $monitoringResult, array $periods)
    {
        return key_exists($monitoringResult->getId(), $periods) ? $periods[$monitoringResult->getId()] : null;
    }

    private function fixReport(Report $report, array &$periods, $neededCuntOfPeriods, $period)
    {
        /** @var   MonitoringResult $monitoringResult */
        foreach ($report->getMonitoringResults() as $monitoringResult) {
            $realCountOfPeriods = count($monitoringResult->getPeriodResults());
            if ($realCountOfPeriods != 1 && $realCountOfPeriods != $neededCuntOfPeriods) {
                /** @var PeriodResult $oldLastPeriod */
                $oldLastPeriod = $monitoringResult->getPeriodResults()->last();
                $newLastPeriod = $this->findInPeriods($monitoringResult, $periods);
                if (!$newLastPeriod) {
                    $newLastPeriod = (new PeriodResult());
                    $this->em->persist($newLastPeriod);
                    $periods[$monitoringResult->getId()] = $newLastPeriod;
                }
                $newLastPeriod
                    ->setIsFinalResult($oldLastPeriod->getIsFinalResult())
                    ->setNewAmount($oldLastPeriod->getNewAmount())
                    ->setTotalAmount($oldLastPeriod->getTotalAmount())
                    ->setPeriod($oldLastPeriod->getPeriod())
                ;
                $oldLastPeriod
                    ->setPeriod($period)
                    ->setTotalAmount(0)
                    ->setNewAmount(0)
                    ->setIsFinalResult(false)
                ;

                $monitoringResult->addPeriodResult($newLastPeriod);
            }
        }
    }
}
