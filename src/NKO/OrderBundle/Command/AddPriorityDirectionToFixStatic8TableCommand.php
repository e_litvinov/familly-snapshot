<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Farvater\PriorityDirection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;

class AddPriorityDirectionToFixStatic8TableCommand extends ContainerAwareCommand
{
    const PSRN = [
        1147700000370 => 'подготовка детей, воспитываемых в организациях для детей-сирот и детей, оставшихся без попечения родителей, к семейному устройству',
        1027725001732 => 'постинтернатное сопровождение, подготовка к самостоятельной жизни детей в возрасте до 23 лет – выпускников организаций для детей-сирот и детей, оставшихся без попечения родителей, а также детей, в отношении которых прекращена опека (попечительство) в замещающей семье',
    ];

    const COMPETITION = 6;

    protected function configure()
    {
        $this
            ->setName('nko:fix-static8-priority-direction');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository(ApplicationHistory::class);
        $priorityRepository = $em->getRepository(PriorityDirection::class);

        foreach (self::PSRN as $psrn => $priorityName) {
            $history = $repository->findOneBy([
               'psrn' => $psrn,
               'isSend' => true,
               'competition' => self::COMPETITION,
            ]);
            $priority = $priorityRepository->findOneBy([
                'name' => $priorityName
            ]);

            /** @var BriefApplication $application */
            $application = unserialize($history->getData());
            $application->setPriorityDirection($priority);

            $history->setData(serialize($application));
        }

        $em->flush();
    }
}
