<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Utils\Application\Farvater2018ApplicationUtils;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFarvater2018HistoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-farvater-history')
            ->setDescription('Save data with nested collections at spread report histories for farvater 2018')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for spread application histories...");
        $histories = $em->getRepository(ApplicationHistory::class)->findSpreadByCompetition(23);
        $output->writeln("\t found " . count($histories) . " spread histories.");

        /**
         * @var ApplicationHistory $history
         */
        foreach ($histories as $history) {
            $unserialized = unserialize($history->getData());
            foreach ($unserialized->getIntroductionPractices() as $practice) {
                $practice->setIndexName(Farvater2018ApplicationUtils::INTRODUCTION_INDEX);
            }

            $applicationId = $unserialized->getId();
            $application = $em->getRepository(Application::class)->find($applicationId);
            $output->writeln("Application history id " . $history->getId() . ", application id " . $application->getId() . " user " . $application->getAuthor());

            /**
             * @var Application $application
             */
            $application = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($application);
            $application = $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($application);
            foreach ($application->getSpecialistProblems() as $item) {
                $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);
            }
            $this->updateNestedCollection($application->getProjectSocialResults());
            $this->updateNestedCollection($application->getProjectSocialIndividualResults());
            $this->updateNestedCollection($application->getProjectDirectResults());
            $this->updateNestedCollection($application->getProjectDirectIndividualResults());
            $this->updateNestedCollection($application->getPracticeSocialResults());
            $this->updateNestedCollection($application->getPracticeDirectResults());
            $this->updateNestedCollection($application->getPracticeDirectIndividualResults());

            $this->updateNestedItems($application->getExperienceItems());
            $this->updateNestedItems($application->getProcessItems());
            $this->updateNestedItems($application->getQualificationItems());
            $this->updateNestedItems($application->getResourceBlockItems());
            $serializedObject = serialize($application);
            $history->setData($serializedObject);
        }

        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("Success.");
    }

    private function updateNestedCollection($collection)
    {
        foreach ($collection as $item) {
            $item = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);

            if ($item->getTargetGroup()) {
                $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item->getTargetGroup());
            }

            if ($item->getSocialResult()) {
                $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item->getSocialResult());
            }

            if ($item->getIndicator()) {
                $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item->getIndicator());
            }
        }
    }

    private function updateNestedItems($collection)
    {
        foreach ($collection as $item) {
            $item = $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item);
            $this->getContainer()->get('nko.resolver.collection_resolver')->fetchCollections($item);

            if ($item->getDirection()) {
                $this->getContainer()->get('nko.resolver.proxy_resolver')->resolveProxies($item->getDirection());
            }
        }
    }
}