<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateGrantsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-grants')
            ->setDescription('Update application history sum grant and contract with a new structure')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for application histories...");
        $appHistories = $em->getRepository('NKOOrderBundle:ApplicationHistory')->findBy(['isSend' => true]);
        $output->writeln("\t found " . count($appHistories) . " application histories.");

        /**
         * @var ApplicationHistory $appHistory
         */
        $output->writeln("Updating grants ...");
        foreach ($appHistories as $appHistory) {

                $output->writeln("Found grant for ap history with id " . $appHistory->getId() . " : " . $appHistory->getSumGrant() . " " . $appHistory->getContract());

                $competition = $em->getRepository('NKOOrderBundle:Competition')->find($appHistory->getCompetition()->getId());
                switch ($competition->getApplicationClass()) {
                    case Application::class:
                        $reportForm = $em->getRepository('NKOOrderBundle:Report\ReportForm')->findOneBy(['reportClass' => Report::class]);
                        break;
                    default:
                        $reportForm = $em->getRepository('NKOOrderBundle:Report\ReportForm')->findOneBy(['competition' => $competition]);
                }

                if ($reportForm) {
                    $grant = new GrantConfig();
                    $em->persist($grant);
                    $grant->setApplicationHistory($appHistory);
                    $grant->setContract($appHistory->getContract());
                    $grant->setSumGrant($appHistory->getSumGrant());
                    $grant->setReportForm($reportForm);
                    $output->writeln("Create grant config with values: ");
                    $output->writeln("\t app history id: \t" . $grant->getApplicationHistory()->getId());
                    $output->writeln("\t report: \t" . $grant->getReportForm()->getTitle());
                    $output->writeln("\t sum grant: \t" . $grant->getSumGrant());
                    $output->writeln("\t contract: \t" . $grant->getContract());
                }

        }

        $output->writeln("");
        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("SUCCESS!");
    }
}