<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 5/11/17
 * Time: 11:45 AM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\KNS2017\Application;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetInvalidApplicationKNSCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:set-invalid-flag')
            ->setDescription('Set invalid and not send flag for KNS competition')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $applications = $em->getRepository('NKOOrderBundle:KNS2017\Application')->findAll();
        $count = count($applications);
        $output->writeln("I find {$count} applications KNS-2017");
        $i = 0;
        /**
         * @var Application $application
         */
        foreach($applications as $application) {
            $application->setIsSend(false);
            $application->setReadyToSent(false);
            $i++;
        }

        $applicationHistories = $em
            ->getRepository('NKOOrderBundle:ApplicationHistory')
            ->findBy([ 'competition' => 7]);
        $countHistory = count($applicationHistories);

        $output->writeln("I change {$i} applications KNS-2017");
        $output->writeln("I find {$countHistory} application histories KNS-2017");
        $i = 0;

        /**
         * @var ApplicationHistory $applicationHistory
         */
        foreach($applicationHistories as $applicationHistory) {
            $i++;
            $applicationHistory->setIsSend(false);
            $applicationHistory->setIsSpread(false);
            $applicationHistory->setNumber(null);
        }
        $output->writeln("I change {$i} application histories KNS-2017");
        $em->flush();
        $output->writeln("\nI save change in database");
    }

}