<?php


namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Proxy\Proxy;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InicializeIndicatorInReportHistoryesCommand extends ContainerAwareCommand
{
    /** @var EntityManagerInterface */
    private $em;

    protected function configure()
    {
        $this
            ->setName('nko:init-monitoring-indicator');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();
        $repository = $this->em->getRepository(ReportHistory::class);
        $reportRepository = $this->em->getRepository(Report::class);


        $histories = $repository->findIdsByReportClass(Report::class);
        foreach ($histories as $historyId) {
            $history = $repository->find($historyId);
            /** @var $report Report */
            $report = unserialize($history->getData());
            if ($this->checkMonitoringIndicator($report)) {
                $this->fixMonitoringIndicator($report);
                $history->setData(serialize($report));
                $this->em->flush($history);
            }
            $this->em->clear();
        }

        $this->em->flush();
    }

    private function checkMonitoringIndicator(Report $report)
    {
        /** @var MonitoringResult $result */
        $result = $report->getMonitoringResults()->first();
        if (!$result) {
            return false;
        }

        return $result->getIndicator() instanceof Proxy;
    }

    private function fixMonitoringIndicator(Report $report)
    {
        /** @var MonitoringResult $result */
        foreach ($report->getMonitoringResults() as $result) {
            $indicator = $result->getIndicator();
            $result->setIndicator($this->em->getRepository(Indicator::class)->find($indicator->getId()));
        }
    }
}
