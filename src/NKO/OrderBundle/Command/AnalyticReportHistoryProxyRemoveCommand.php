<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as FarvaterApplication;
use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;

class AnalyticReportHistoryProxyRemoveCommand extends ContainerAwareCommand
{
    const MANY = 'many';
    const ONE = 'one';

    const FINISH = 'finish';
    const NOT_YET = 'not yet';

    const TARGET_GROUP_FIELDS = [
        'title',
        'code',
    ];

    const INDICATOR_FIELDS = [
        'title',
        'code',
    ];

    const PROBLEM_FIELDS = [
        'targetGroup',
        'content',
        'socialResults',
        'customSocialResult',
    ];

    const TYPES = [
        Report::class => [
            FarvaterApplication::class,
            [
                [
                    self::MANY => [
                        self::NOT_YET,
                        'expectedAnalyticResults' => [
                            [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedFarvaterResult.targetGroup' => self::TARGET_GROUP_FIELDS
                                ]
                            ], [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedFarvaterResult.indicator' => self::INDICATOR_FIELDS
                                ]
                            ],
                        ]
                    ]
                ], [
                    self::MANY => [
                        self::NOT_YET,
                        'practiceAnalyticResults' => [
                            [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedFarvaterResult.targetGroup' => self::TARGET_GROUP_FIELDS
                                ]
                            ], [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedFarvaterResult.indicator' => self::INDICATOR_FIELDS
                                ]
                            ],
                        ]
                    ]
                ], [
                    self::MANY => [
                        self::NOT_YET,
                        'expectedAnalyticIndividualResults' => [
                            [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedFarvaterResult.targetGroup' => self::TARGET_GROUP_FIELDS
                                ]
                            ]
                        ]
                    ]
                ], [
                    self::MANY => [
                        self::NOT_YET,
                        'practiceAnalyticIndividualResults' => [
                            [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedFarvaterResult.targetGroup' => self::TARGET_GROUP_FIELDS
                                ]
                            ]
                        ]
                    ]
                ],
            ]
        ],
        Report::class => [
            ContinuationApplication::class,
            [
                [
                    self::MANY => [
                        self::NOT_YET,
                        'expectedAnalyticResults' => [
                            [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedResult.problem' => self::PROBLEM_FIELDS
                                ]
                            ]
                        ]
                    ]
                ], [
                    self::MANY => [
                        self::NOT_YET,
                        'practiceAnalyticResults' => [
                            [
                                self::ONE => [
                                    self::FINISH,
                                    'linkedResult.problem' => self::PROBLEM_FIELDS
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];

    const LIMIT = 10;
    
    /** @var OutputInterface $out */
    private $out;
    
    /** @var $em  EntityManager*/
    private $em;

    /**
     * @var PropertyAccessor
     */
    protected $accessor;

    protected function configure()
    {
        $this
            ->setName('nko:remove-proxy-fields-from-analytic-report-histories')
            ->setDescription('remove proxy fields from analytic report histories');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->out = $output;
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->accessor = PropertyAccess::createPropertyAccessor();

        $this->remakeHistorys();
    }


    private function remakeHistorys()
    {
        $this->out->writeln('Start this');
        foreach (self::TYPES as $reportClass => $configuration) {
            $offset = 0;
            $this->out->writeln('Start competition with id '. $reportClass);

            while (true) {
                $this->out->writeln('Start history with offset '. (string) ($offset));

                $historys = $this->getHistoryByCompetitionFromIndex($reportClass, $configuration[0], $offset, self::LIMIT);

                if (!$historys) {
                    //end of current Type
                    break;
                }


                $this->remakePortionOfHistories($historys, $configuration[1]);

                unset($historys);
                $this->em->clear();
                $offset += self::LIMIT;
            }
        }
    }

    private function remakePortionOfHistories($histories, $configuration)
    {
        foreach ($histories as $history) {
            $object = unserialize($history->getData());
            $this->remakeHistory($object, $configuration);
            $history->setData(serialize($object));
        }
        $this->em->flush();
    }

    private function remakeHistory($object, $configuration)
    {
        foreach ($configuration as $internalConf) {
            $mode = array_keys($internalConf)[0];
            $internalConf = array_values($internalConf)[0];

            if ($this->accessor->isReadable($object, array_keys($internalConf)[1])) {
                $newObject = $this->accessor->getValue($object, array_keys($internalConf)[1]);

                $arrayObjects = [];
                if ($mode == self::MANY) {
                    foreach ($newObject as $oneObject) {
                        $arrayObjects[] = $oneObject;
                    }
                } elseif ($mode == self::ONE) {
                    $arrayObjects[] = $newObject;
                }

                foreach ($arrayObjects as $oneObject) {
                    $this->recursion($oneObject, $internalConf);
                }


                if ($mode == self::MANY) {
                    $this->accessor->setValue($object, array_keys($internalConf)[1], $arrayObjects);
                } elseif ($mode == self::ONE) {
                    $this->accessor->setValue($object, array_keys($internalConf)[1], array_pop($arrayObjects));
                }
            }
        }
    }

    private function recursion($object, $internalConf)
    {

        $mode = $internalConf[0];
        $value = array_values($internalConf)[1];

        if ($mode == self::FINISH && $object) {
            $fullObject = $this->em->find(get_class($object), $object->getId());
            $this->setValuesFromRealObjects($object, $fullObject, $value);
        } elseif ($mode == self::NOT_YET) {
            $this->remakeHistory($object, $value);
        }
    }

    private function setValuesFromRealObjects($object, $realObject, $fields)
    {

        foreach ($fields as $field) {
            $value = $this->accessor->getValue($realObject, $field);
            $this->accessor->setValue($object, $field, $value);
        }
    }

    private function getHistoryByCompetitionFromIndex($reportClass, $applicationClass, $start, $limit = 10)
    {
        return $this->em->createQueryBuilder()
            ->select('r')
            ->from(ReportHistory::class, 'r')
            ->leftJoin('r.reportForm', 'rf')
            ->leftJoin('rf.competition', 'c')
            ->where('rf.reportClass = :reportClass')
            ->andWhere('c.applicationClass = :applicationClass')
            ->setParameters(['reportClass' => $reportClass, 'applicationClass' => $applicationClass])
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
            ;
    }
}
