<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 12/19/17
 * Time: 2:04 PM
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Indicator;
use NKO\OrderBundle\Entity\Report\MonitoringReport\IndicatorInterface;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\PeriodResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class RevertMonitoringReportCommand extends ContainerAwareCommand
{
    const EMAIL = 'email';
    const REPORT_HISTORY_ID = 'report_history_id';
    const REPORT_FORM_ID = 'report_form_id';
    const INDIVIDUAL_RESULT_COUNT = 10;

    /**
     * @var PropertyAccess
     */
    protected $accessor;

    /**
     * @var EntityManager
     */
    protected $em;

    protected $monitoringResultProperties;

    protected $periodResultProperties;

    protected function configure()
    {
        $this
            ->addArgument(self::EMAIL, InputArgument::REQUIRED)
            ->addArgument(self::REPORT_HISTORY_ID, InputArgument::REQUIRED)
            ->addArgument(self::REPORT_FORM_ID, InputArgument::REQUIRED)
            ->setName('nko:revert-report')
            ->setDescription('Revert invalid monitoring report with email report_history_id and report_form_id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getArgument(self::EMAIL);
        $reportHistoryId = $input->getArgument(self::REPORT_HISTORY_ID);
        $reportFormId = $input->getArgument(self::REPORT_FORM_ID);

        $doctrine = $this->getContainer()->get('doctrine');
        $this->em = $doctrine->getManager();

        $output->writeln("Getting user...");
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        $output->writeln('User found: ' . $user->getNkoName());

        $output->writeln("Searching for report...");
        $reportForm = $this->em->getRepository(ReportForm::class)->find($reportFormId);
        $report = $this->em->getRepository(Report::class)->findOneBy([
            'reportForm' => $reportForm,
            'author' => $user
        ]);

        $applicationHistory = null;

        if (!$report) {
            $output->writeln('Can not find the report.');
        } else {
            $output->writeln('Report found.');

            $applicationHistory = $report->getApplicationHistory();

            $output->writeln("Removing report...");
            $this->em->remove($report);
            $this->em->flush();
            $output->writeln("Report removed.");
        }

        $output->writeln("Searching for report history...");
        $reportHistory = $this->em->getRepository(ReportHistory::class)->find($reportHistoryId);

        if (!$reportHistory) {
            $output->writeln('Can not find report history.');
            return;
        }

        $revertReport = unserialize($reportHistory->getData());

        $output->writeln("New report creation.");
        $newReport = new Report();
        $newReport->setReportForm($reportForm);
        $newReport->setAuthor($user);
        $user->addReport($newReport);
        $newReport->setPsrn($user->getPsrn());

        if ($applicationHistory) {
            $newReport->setApplicationHistory($applicationHistory);
        }

        $periods = $reportForm->getReportPeriods();
        $indicators = $this->em->getRepository(Indicator::class)->findAllExceptCode(IndicatorInterface::CUSTOM_VALUE);
        foreach($indicators as $indicator) {
            $monitoringResult = $this->createMonitoringResult($indicator, $newReport, $periods);

            if ($indicator->getParent()) {
                $this->createPeriodResult($monitoringResult, true, null);
            }

            if ($indicator->getCode() == IndicatorInterface::PRE_CUSTOM_VALUE) {
                $index = $indicator->getIndexNumber();
                $customIndicator = $this->createCustomIndicator($indicator->getParent(), ++$index);
                $customMonitoringResult = $this->createMonitoringResult($customIndicator, $newReport, $periods);
                $this->createPeriodResult($customMonitoringResult, true, null);
            }

            if ($indicator->getCode() == IndicatorInterface::INDIVIDUAL_RES_CODE) {
                $index = IndicatorInterface::INDIVIDUAL_RES_COUNT;
                for ($i = 0; $i < self::INDIVIDUAL_RESULT_COUNT; $i++) {
                    $customIndicator = $this->createCustomIndicator($indicator, $index + $i);
                    $customMonitoringResult = $this->createMonitoringResult($customIndicator, $newReport, $periods);
                    $this->createPeriodResult($customMonitoringResult, true, null);
                }
            }
        }

        $output->writeln("Merging new report with report history...");

        $this->monitoringResultProperties = $this->em->getClassMetadata(MonitoringResult::class)->getFieldNames();
        $this->periodResultProperties = $this->em->getClassMetadata(PeriodResult::class)->getFieldNames();
        $indicatorProperties = $this->em->getClassMetadata(Indicator::class)->getFieldNames();

        array_shift($this->periodResultProperties);
        array_shift($this->monitoringResultProperties);
        array_shift($indicatorProperties);
        $this->accessor = PropertyAccess::createPropertyAccessor();

        $output->writeln("Got properties");

        $reportMonitoringResults = $newReport->getMonitoringResults();
        $revertReportMonitoringResults = $revertReport->getMonitoringResults();

        $output->writeln("Got monitoring results");

        foreach ($reportMonitoringResults as $key => $result) {
            $output->writeln("Copy monit result values " . $revertReportMonitoringResults[$key]->getId());
            $this->copyValues($indicatorProperties, $result->getIndicator(), $revertReportMonitoringResults[$key]->getIndicator());
            $this->copyValues($this->monitoringResultProperties, $result, $revertReportMonitoringResults[$key]);
            $reportPeriodResults = $result->getPeriodResults();
            $revertReportPeriodResults = $revertReportMonitoringResults[$key]->getPeriodResults();
            foreach ($reportPeriodResults as $periodKey => $periodResult) {
                $output->writeln("Copy period result values " . $revertReportPeriodResults[$periodKey]->getId());
                $this->copyValues($this->periodResultProperties, $periodResult, $revertReportPeriodResults[$periodKey]);
            }
        }

        $output->writeln("Writing to db...");
        $this->em->persist($newReport);
        $this->em->flush();
        $output->writeln("SUCCESS");
    }

    private function createMonitoringResult($indicator, $object, $periods)
    {
        $monitoringResult = new MonitoringResult();
        $monitoringResult->setIndicator($indicator);
        $object->addMonitoringResult($monitoringResult);
        if ($indicator->getParent()) {
            foreach($periods as $period) {
                $this->createPeriodResult($monitoringResult, false, $period);
            }
        }

        return $monitoringResult;
    }

    private function createPeriodResult($monitoringResult, $isFinalResult, $period = null)
    {
        $periodResult = new PeriodResult();
        $periodResult->setPeriod($period);
        $periodResult->setIsFinalResult($isFinalResult);
        $monitoringResult->addPeriodResult($periodResult);
    }

    private function copyValues($properties, $targetObject, $object)
    {
        foreach ($properties as $property) {
            $value = $this->accessor->getValue($object, $property);
            $this->accessor->setValue($targetObject, $property, $value);
        }
    }

    private function createCustomIndicator($parent, $index)
    {
        $indicator = new Indicator();
        $indicator->setCode(IndicatorInterface::CUSTOM_VALUE);
        $indicator->setParent($parent);
        $indicator->setIndexNumber($index);
        return $indicator;
    }
}