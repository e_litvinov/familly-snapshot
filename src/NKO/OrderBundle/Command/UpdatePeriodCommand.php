<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\KNS\Application2018\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report;
use NKO\OrderBundle\Entity\Report\ReportForm;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class UpdatePeriodCommand extends ContainerAwareCommand
{
    /**
     * @var PropertyAccessor $accessor
     */
    private $accessor;

    protected function configure()
    {
        $this
            ->setName('nko:update-period')
            ->setDescription('Update existing December 2017 period to December 2017 - January 2018, considering linked reports')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Searching for period.");

        $period = $em->getRepository(PeriodReport::class)->find(24);

        $output->writeln("Found period " . $period->getId() . ' ' . $period->getName());

        $period->setName('Декабрь 2017 - Январь 2018');
        $period->setFinishDate(new \DateTime('2018-01-31'));

        $output->writeln("Update period values");
        $output->writeln("\t Name           " . $period->getName());
        $output->writeln("\t FinishDate     " . $period->getFinishDate()->format('Y-m-d'));
        $output->writeln("Create new period");

        $newPeriod = new PeriodReport();
        $newPeriod->setName('Декабрь 2017');
        $newPeriod->setStartDate(new \DateTime('2017-12-01'));
        $newPeriod->setFinishDate(new \DateTime('2017-12-31'));
        $em->persist($newPeriod);

        $output->writeln("New period values");
        $output->writeln("\t Name           " . $period->getName());
        $output->writeln("\t FinishDate     " . $period->getStartDate()->format('Y-m-d'));
        $output->writeln("\t FinishDate     " . $period->getFinishDate()->format('Y-m-d'));
        $output->writeln("Searching for report forms.");

        $reportForms = $em
            ->createQueryBuilder()
            ->select('rf')
            ->from(ReportForm::class, 'rf')
            ->join('rf.reportPeriods', 'p')
            ->where('p.id = :periodId')
            ->andWhere('rf.reportClass = :reportClass')
            ->setParameters([
                'periodId' => 24,
                'reportClass' => Report::class
            ])
            ->getQuery()
            ->getResult();

        /**
         * @var ReportForm $reportForm
         * @var PeriodReport $period
         * @var PeriodReport $newPeriod
         */
        foreach ($reportForms as $reportForm) {
            $output->writeln("Change period for report form " . $reportForm->getId() . ' ' . $reportForm->getTitle());
            $reportForm->removeReportPeriod($period);
            $period->removeReportForm($reportForm);
            $reportForm->addReportPeriod($newPeriod);
            $newPeriod->addReportForm($reportForm);
        }

        $output->writeln('');
        $output->writeln("Searching for report histories.");

        $reportHistories = $em
            ->createQueryBuilder()
            ->select('partial h.{id, nkoName, period}')
            ->from(ReportHistory::class, 'h')
            ->where('h.period = :periodId')
            ->setParameters([
                'periodId' => 24
            ])
            ->getQuery()
            ->getResult();

        $output->writeln("Found: " . count($reportHistories));
        $output->writeln('');

        /**
         * @var ReportHistory $history
         */
        foreach ($reportHistories as $history) {
            $output->writeln("Change period for report history " . $history->getId() . ' ' . $history->getNkoName());

            $history->setPeriod($newPeriod);
        }

        $output->writeln('');
        $output->writeln("Searching for reports.");

        $reports = $em->getRepository(Report::class)->findBy([
            'period' => $period
        ]);

        $output->writeln("Found: " . count($reports));
        $output->writeln('');

        /**
         * @var Report $report
         */
        foreach ($reports as $report) {
            $output->writeln("Change period for report history " . $report->getId() . ' ' . $report->getAuthor());
            $report->setPeriod($newPeriod);
        }

        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("Success.");
    }
}