<?php

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Report2018\KNS\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Report\AnalyticReport\Feedback;
use NKO\OrderBundle\Entity\Report\AnalyticReport\FeedbackTool;

class AnalyticReprotAttachmentFixerCommand extends ContainerAwareCommand
{
    const REPORT_IDS = [
        1384
    ];

    /** @var EntityManager */
    private $em;


    protected function configure()
    {
        $this
            ->setName('nko:add-tools')
            ->setDescription('add tools for broken 5.1 table.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $reportsitory = $this->em->getRepository(Report::class);
        foreach (self::REPORT_IDS as $reportId) {
            /** @var Report $report */
            $report = $reportsitory->find($reportId);
            foreach ($report->getPracticeFeedbackAttachments() as $attachment) {
                /** @var $attachment Feedback */
                while ($attachment->getTools()->count() != 3) {
                    $attachment->addTool(new FeedbackTool());
                }

            }
        }
        $this->em->flush();

        $output->writeln("Add FeedBackTools");
    }
}
