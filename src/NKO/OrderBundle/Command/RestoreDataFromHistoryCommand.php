<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 25.10.18
 * Time: 17.30
 */

namespace NKO\OrderBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;
use NKO\OrderBundle\Entity\Region;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class RestoreDataFromHistoryCommand extends ContainerAwareCommand
{
    const NAME_TABLE = [
        'name',
        'Abbreviation',
        'PSRN',
        'LegalPostCode',
        'LegalStreet',
        'LegalRegion',
        'LegalCity',
        'IsAddressesEqual',
        'ActualPostCode',
        'ActualCity',
        'ActualRegion',
        'ActualStreet',
        'Email',
        'PhoneCode',
        'Phone',
        'DateRegistrationOfOrganization',
        'PrimaryActivity',
        'Mission',
        'CountStaffEmployees',
        'CountInvolvingEmployees',

    ];


    /** @var EntityManager $em */
    private $em;

    protected function configure()
    {
        $this
            ->setName('nko:restore-data-from-history')
            ->setDescription('Take data from histories')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('Doctrine')->getEntityManager();

        $applications = $this->em->getRepository(BriefApplication::class)->findBy([], ['id' => 'ASC']);

        $applicationsWithoutHistory = [];
        $applicationHaveHistory = [];

        $accessor = PropertyAccess::createPropertyAccessor();

        /** @var BriefApplication $application */
        foreach ($applications as $application) {
            $history = $this->em->getRepository(ApplicationHistory::class)->findOneBy([
                'author' => $application->getAuthor(),
                'competition' => $application->getCompetition()
            ], [
                'id' => 'DESC'
            ]);

            if ($history) {
                $historyApplication = unserialize($history->getData());
                foreach (self::NAME_TABLE as $field) {
                    if (!$accessor->getValue($application, $field)) {
                        $fieldValue = $accessor->getValue($historyApplication, $field);

                        if (is_object($fieldValue) && !($fieldValue instanceof \DateTime)) {
                            $fieldValue = $this->em->merge($fieldValue);
                        }

                        $accessor->setValue($application, $field, $fieldValue);
                    }
                }

                $region = $this->em->getRepository(Region::class)->find($historyApplication->getActualRegion()->getId());
                $application->setActualRegion($region);
                $region = $this->em->getRepository(Region::class)->find($historyApplication->getLegalRegion()->getId());
                $application->setLegalRegion($region);
                /** @var BriefApplication $historyApplication */
                $applicationHaveHistory[] = $application->getId();
            } else {
                $applicationsWithoutHistory[] = $application->getId();
            }
        }

        $output->writeln('Application restored from history ('. count($applicationHaveHistory). ')- ' . implode(', ', $applicationHaveHistory));

        $output->writeln('Applications that have no historyes('. count($applicationsWithoutHistory). ') are '. implode(', ', $applicationsWithoutHistory));

        $this->em->flush();

        $output->writeln('ALL done');
    }
}
