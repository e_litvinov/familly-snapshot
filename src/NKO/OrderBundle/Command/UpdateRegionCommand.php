<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/10/17
 * Time: 9:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\Report;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class UpdateRegionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-region')
            ->setDescription('Update application history legal region')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $output->writeln("Fetching competition.");
        $competition = $em->getRepository(Competition::class)->findOneBy([
            'applicationClass' => Application\Farvater\Application2018\Application::class
        ]);
        $output->writeln("Fetched competition " . $competition->getName());

        $output->writeln("Fetching application histories.");
        $applicationHistories = $em->getRepository(ApplicationHistory::class)->findBy([
            'competition' => $competition,
            'isSpread' => true
        ]);
        $output->writeln("Fetched" . count($applicationHistories) . " application histories.");

        $relatedCompetitionResolver = $this->getContainer()->get('nko_order.resolver.related_competition_resolver');
        foreach ($applicationHistories as $applicationHistory) {
            $application = unserialize($applicationHistory->getData());
            $relatedApplication = $relatedCompetitionResolver->getApplicationHistory($application);
            $applicationHistory->setLegalRegion($relatedApplication->getLegalRegion());
            $output->writeln("Updated " . $applicationHistory->getNkoName() . " set region " . $relatedApplication->getLegalRegion());
        }

        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("SUCCESS!");
    }
}