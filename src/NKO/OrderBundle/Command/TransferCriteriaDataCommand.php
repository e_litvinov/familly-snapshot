<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.01.17
 * Time: 10:28
 */

namespace NKO\OrderBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\SiteLink;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseApplication;

class TransferCriteriaDataCommand extends ContainerAwareCommand
{
    const QUESTION_TABLE = 'question_mark';
    const OLD_QUESTION_TABLE = 'old_question_mark';
    const RELATIONS_CRITERIA_TABLE = 'mark_criteria';
    const OLD_CRITERIA_MARK = 'old_mark_criteria';

    protected function configure()
    {
        $this
            ->setName('nko:transfer-criteria-data')
            ->setDescription('Transfer criteria and question data')
            ->setHelp('Transfer data from old criteria and question tables to new linked tables');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager= $this->getContainer()->get('doctrine')->getManager();
        $connection = $entityManager->getConnection();

        $output->writeln("Transferring data to criteria_competitions...");

        $query = 'insert into criteria_competitions (mark_criteria_id, competition_id) 
                  select id, competition_id from old_mark_criteria 
                  where old_mark_criteria.competition_id is not null;';

        $statement = $connection->prepare($query);
        $statement->execute();
        $output->writeln("\t data transferred.");

        $output->writeln("Transferring data to questions_competitions...");
        $query = 'insert into questions_competitions (question_id, competition_id) 
                  select id, competition_id from old_question_mark
                  where old_question_mark.competition_id is not null;';
        $statement = $connection->prepare($query);
        $statement->execute();
        $output->writeln("\t data transferred.");

        $output->writeln("\nSUCCESS: Data transferred.");
    }
}