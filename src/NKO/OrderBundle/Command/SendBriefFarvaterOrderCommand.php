<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/28/17
 * Time: 10:52 AM
 */

namespace NKO\OrderBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;

class SendBriefFarvaterOrderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:send-brief-farvater-application')
            ->setDescription('Send brief farvater application 2016')
            ->setHelp("Send brief farvater application 2016")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em= $this->getContainer()->get('doctrine')->getManager();
        $competition = $em
            ->getRepository('NKOOrderBundle:Competition')
            ->findOneBy(array('name' => 'Краткая заявка СФ-2016'));
        if($competition == null) {
            $output->writeln('Competition not found');
        }
        else{
            $output->writeln('Competition is found!');
            $applicationHistories = $em
                ->getRepository('NKOOrderBundle:ApplicationHistory')
                ->findBy(array('competition' => $competition));
            $i = 0;

            foreach ($applicationHistories as $applicationHistory){
                $i++;
                $applicationHistory->setIsSend(true);
                $applicationHistory->setIsAppropriate(true);
                $applicationHistory->setNumber($i);
                $applicationHistory->setIsSpread(true);

            }
            try {
                $em->flush();
                $output->writeln($i.' rows updated successfully.');
            }
            catch (Exception $e) {
                $output->writeln("Database update was NOT successful.");
            }

        }

    }

}