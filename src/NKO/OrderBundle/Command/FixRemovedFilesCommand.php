<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixRemovedFilesCommand extends ContainerAwareCommand
{
    const HISTORY_IDS = [
        75533,
        76102,
        76069,
    ];

    protected function configure()
    {
        $this
            ->setName('nko:fix-file-disappearing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $repository = $em->getRepository(ReportHistory::class);
        $reportRepository = $em->getRepository(Report::class);


        foreach (self::HISTORY_IDS as $id) {
            $history = $repository->find($id);

            /** @var Report $reportRestored */
            $reportRestored = unserialize($history->getData());

            /** @var Report $report */
            $report = $reportRepository->find($reportRestored->getId());

            foreach ($report->getDocuments() as $document) {
                /** @var MonitoringDocument $documentRestored */
                /** @var MonitoringDocument $document */
                $documents = $reportRestored->getDocuments();
                $documentRestored = $documents->filter(function ($documentRestored) use ($document) {
                    return $document->getId() == $documentRestored->getId();
                });

                if (!$documentRestored->isEmpty()) {
                    $documentRestored = $documentRestored->first();
                    $document->setFile($documentRestored->getFile());
                    $output->writeln('Restore document of report '. $report->getId(). ' with id '. $document->getId());
                }
            }
        }

        $em->flush();
    }
}
