<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 27.01.17
 * Time: 16:06
 */

namespace NKO\OrderBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\OrderBundle\Entity\SiteLink;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseApplication;

class TransformJsonReportDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:transform-report-json')
            ->setDescription('Transform report_history data')
            ->setHelp('Transform report_history data to fit base_report ');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var EntityManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        $output->writeln("Searching for all report histories...");
        $reportHistories = $em->getRepository('NKOOrderBundle:ReportHistory')->findAll();

        $output->writeln("Transforming json data...");
        foreach ($reportHistories as $reportHistory) {
            /**
             * @var ReportHistory $reportHistory
             */
            $output->writeln("Transforming report history json with id = " . $reportHistory->getId());
            $transformed = $this->jsonTransform($output, $reportHistory->getData());
            $output->writeln("Success. Json transformed.");
            $reportHistory->setData($transformed);
            $em->merge($reportHistory);
        }

        $output->writeln("");
        $output->writeln("Writing to database...");
        $em->flush();
        $output->writeln("\nSUCCESS: ".count($reportHistories)." values transformed and saved");
    }

    public function jsonTransform(OutputInterface $output, $data)
    {
        $fullPattern = '/(s:)([0-9]+)(:\"[^a-zA-Z0-9_]+)([A-Za-z0-9_+\\\\]+Report)+[^a-zA-Z0-9_]+([A-Za-z0-9_]+)\"/u';
        preg_match_all($fullPattern, $data, $matches);
        $fullMatch = $matches[0];
        $length = $matches[2];
        $appTypes = $matches[4];
        $vars = $matches[5];
        foreach ($vars as $key => $var) {
            $fullMatch[$key] = str_replace([
                $length[$key],
                $appTypes[$key]
            ], [
                strlen($var) + 3,
                '*'
            ], $fullMatch[$key]);
        }
        return str_replace($matches[0], $fullMatch, $data);
    }

}