<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\PeriodReport;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveReportPeriodsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:remove-report-periods')
            ->setDescription('Remove excess report periods');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $periodResult = $em
            ->createQueryBuilder()
            ->select('p')
            ->from(PeriodReport::class, 'p')
            ->leftJoin('p.reportForms', 'r')
            ->where('r.id IS NOT NULL')
            ->getQuery()
            ->getResult();

        $removePeriodResults = $em->createQueryBuilder()
            ->select('p')
            ->from(PeriodReport::class, 'p')
            ->where ('p NOT IN (:periodResult)')
            ->setParameters(['periodResult'=>$periodResult])
            ->getQuery()
            ->getResult();

        foreach ($removePeriodResults as $period) {
            $em->remove($period);
        }
        $em->flush();
    }
}