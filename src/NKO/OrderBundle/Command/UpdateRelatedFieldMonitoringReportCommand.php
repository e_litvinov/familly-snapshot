<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 30/08/18
 * Time: 15:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Farvater\Application2018\Application as Farvater;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Competition;
use Doctrine\ORM\ORMException as Exception;

class UpdateRelatedFieldMonitoringReportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-related-field-monitoring-report')
            ->setDescription('Update project name and organization name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $competition = $em->getRepository(Competition::class)->findOneBy(['applicationClass' => Farvater::class]);
        $applications = $em->getRepository(BaseApplication::class)->findBy(['competition' => $competition]);
        $relatedCompetition = $competition->getRelatedCompetition();

        $countApplications = count($applications);
        $output->writeln("Found " . $countApplications . " application Farvater 2018.");

        $relatedApplicationHistory = $em->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->where('a.isSpread = :isSpread')
            ->andWhere('a.competition = :competition')
            ->setParameters([
                'competition' => $relatedCompetition,
                'isSpread' => true
            ])
            ->getQuery()
            ->getResult();

        $output->writeln("Found " . count($relatedApplicationHistory) . " application histories Brief 2018.");

        $applicationHistories = $em->createQueryBuilder()
            ->select('a')
            ->from(ApplicationHistory::class, 'a')
            ->where('a.isSend = :isSend AND a.competition = :competition')
            ->orWhere('a.isSpread = :isSpread AND a.competition = :competition')
            ->setParameters([
                'competition' => $competition,
                'isSpread' => true,
                'isSend' => true
            ])
            ->getQuery()
            ->getResult();

        $output->writeln("Found " . count($applicationHistories) . " application histories Farvater 2018.");

        $iteration = 0;
        $percent = 25;
        $countUpdateApplications = 0;
        $countUpdateHistories = 0;

        $output->writeln('Begin to process.. 0%');

        foreach ($applications as $application) {
            $iteration++;
            $author = $application->getAuthor();
            $lastHistory = array_filter($relatedApplicationHistory, function ($applicationHistory) use ($author) {
                return $applicationHistory->getAuthor() === $author;
            });
            if ($lastHistory) {
                $projectName = unserialize(array_shift($lastHistory)->getData())->getProjectName();
                $application->setProjectName($projectName);
                $countUpdateApplications++;

                $currentHistories = array_filter($applicationHistories, function ($applicationHistory) use ($author) {
                    return $applicationHistory->getAuthor() === $author;
                });

                if ($currentHistories) {
                    foreach ($currentHistories as $history) {
                        $application = unserialize($history->getData());
                        $application->setProjectName($projectName);
                        $history->setData(serialize($application));
                        $countUpdateHistories++;
                    }
                }
            }

            if ((($iteration / $countApplications) * 100) > $percent) {
                $output->writeln('Processed '. $percent . '%');
                $percent += 25;
            }
        }

        $output->writeln('Processed 100%... Writing to database...');

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        } catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }

        $output->writeln("Updated " . $countUpdateApplications . " applications and ".$countUpdateHistories. " application histories");
        $output->writeln("Success.");
    }
}
