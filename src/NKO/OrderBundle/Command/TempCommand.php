<?php

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\Report\FinanceReport\ReportTemplate;

class TempCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:temp_command')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $template = $em->getRepository(ReportTemplate::class)->find(253);
        $em->remove($template);

        try {
            $em->flush();
            $output->writeln("Success");
        } catch (Exception $e) {
            $output->writeln("NOT successful.");
        }
    }
}
