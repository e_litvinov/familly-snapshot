<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.8.18
 * Time: 16.54
 */

namespace NKO\OrderBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\UserBundle\Entity\NKOUser;

class SetRelationApplicationHistoryAuthorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:set_relation_between_application_history_author')
            ->setDescription('set relation between application history and author')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $applications = $em
            ->createQueryBuilder()
            ->select('partial a.{id, psrn, author}')
            ->from(ApplicationHistory::class, 'a')
            ->where('a.isSend = :isSend')
            ->orWhere('a.isSpread = :isSpread')
            ->setParameters([
                'isSend' => true,
                'isSpread' => true
            ])
            ->getQuery()
            ->getResult();

        $countApplications = count($applications);
        $output->writeln('Found '. $countApplications .' application histories');
        $output->writeln('Begin to set author to application..');

        $percent = 25;
        $iteration = 0;

        foreach ($applications as $application) {
            $iteration++;
            $author = $em->getRepository(NKOUser::class)->findBy([
                'psrn' => $application->getPsrn(),
            ]);

            $application->setAuthor($author[0]);

            if ((($iteration / $countApplications) * 100) > $percent) {
                $output->writeln('Processed '. $percent . '%');
                $percent += 25;
            }
        }
        $output->writeln('Processed 100%');
        $output->writeln('Begin flush..');

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        }
        catch (Exception $e) {
            $output->writeln("Database update was NOT successful.");
        }

    }
}