<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 30/08/18
 * Time: 15:15 PM
 */

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\Application\Continuation\Application as ContinuationApplication;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\PeriodReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringDocument;
use NKO\OrderBundle\Entity\Report\MonitoringReport\MonitoringResult;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\Competition;
use Doctrine\ORM\ORMException as Exception;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PropertyAccess\PropertyAccess;

class UpdateMonitoringDocumentsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:update-monitoring-document')
            ->setDescription('Update monitoring document before revert')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $em->getFilters()->disable('softdeleteable');

        $io = new SymfonyStyle($input, $output);
        $io->title('Update monitoring documents');

        $reportHistoryId = $io->ask('Report history ID', 'nko');

        $reportHistory = $em->getRepository(ReportHistory::class)->find($reportHistoryId);

        if (!$reportHistory) {
            $io->writeln('Report history with id ' . $reportHistoryId . ' not found.');
            return;
        }

        $revertReport = unserialize($reportHistory->getData());
        $report = $em->getRepository(Report::class)->find($revertReport->getId());

        if (!$report) {
            $io->writeln('Report with id ' . $revertReport->getId() . ' not found.');
            return;
        }

        $currentDocuments = $report->getDocuments();

        foreach ($revertReport->getDocuments() as $document) {
            $documents = $currentDocuments->filter(function ($object) use ($document) {
                return $object->getId() === $document->getId();
            });

            if ($documents->count()) {
                $io->writeln('Skip document with id ' . $document->getId());
                continue;
            }

            $io->writeln('Updating document with id ' . $document->getId());

            $revertDocument = $em->getRepository(MonitoringDocument::class)->find($document->getId());

            if (!$revertDocument) {
                $revertDocument = new MonitoringDocument();
            }

            if ($document->getMonitoringResult()->getId()) {
                $monitoringResult = $em->getRepository(MonitoringResult::class)->find($document->getMonitoringResult()->getId());
                $revertDocument->setMonitoringResult($monitoringResult);
            }

            if ($document->getPeriod()->getId()) {
                $period = $em->getRepository(PeriodReport::class)->find($document->getPeriod()->getId());
                $revertDocument->setPeriod($period);
            }

            $revertDocument->setFile($document->getFile());
            $revertDocument->setReport($report);
            $revertDocument->setDeletedAt(new \DateTime());
        }

        $em->getFilters()->enable('softdeleteable');

        $output->writeln("Writing to db...");
        $em->flush();
        $output->writeln("SUCCESS");
    }
}
