<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.01.17
 * Time: 10:28
 */

namespace NKO\OrderBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\SiteLink;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\ORMException as Exception;
use NKO\OrderBundle\Entity\BaseApplication;

class TransferReportDataCommand extends ContainerAwareCommand
{
    const COLUMN_NAMES = ['id',
        'application_history_id',
        'is_accepted',
        'is_valid',
        'is_send',
        'psrn',
        'receipt_of_donations',
        'period_costs',
        'donation_balance',
        'confirming_document',
        'created_at',
        'updated_at',
    ];

    const LINKED_TABLES = [
        'document_report',
        'finance_spent'
    ];

    const TABLE_NAME = 'base_report';
    const BACKUP_TABLE_NAME = 'old_report';
    const TYPE_COLUMN_NAME = 'report_type';
    const TYPE_COLUMN_VALUE = 'kns2016';
    const FK_NAME = 'report_id';
    const REF_COLUMN = 'id';

    protected function configure()
    {
        $this
            ->setName('nko:transfer-report-data')
            ->setDescription('Transfer data to base_application')
            ->setHelp('Transfer data from old_application to base_application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager= $this->getContainer()->get('doctrine')->getManager();
        $connection = $entityManager->getConnection();

        $output->writeln("Transferring data to base_report...");
        $columns = implode(',', self::COLUMN_NAMES);
        $query = 'insert into '.self::TABLE_NAME.' ('.$columns.','.self::TYPE_COLUMN_NAME.')
                select '.$columns.',\''. self::TYPE_COLUMN_VALUE .'\' from '.self::BACKUP_TABLE_NAME.';';
        $statement = $connection->prepare($query);
        $statement->execute();
        $output->writeln("\t data transferred.");

        $output->writeln("Alter linked tables foreign keys...");
        foreach (self::LINKED_TABLES as $table) {
            $query = 'alter table '.$table.'
                add constraint foreign key ('.self::FK_NAME.')
                references '.self::TABLE_NAME.' ('.self::REF_COLUMN.') on delete cascade';
            $statement = $connection->prepare($query);
            $statement->execute();
            $output->writeln("\t ".$table." updated.");
        }

        $output->writeln("\nSUCCESS: Data transferred.");
    }
}