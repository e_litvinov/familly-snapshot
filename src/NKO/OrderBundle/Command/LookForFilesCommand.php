<?php

namespace NKO\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NKO\OrderBundle\Entity\ReportHistory;

class LookForFilesCommand extends ContainerAwareCommand
{
    const PSRN = 1023800918712;
    const REPORT_FORM_ID = 64;

    protected function configure()
    {
        $this
            ->setName('nko:look_files_monitoring');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $reportHistories = $em->getRepository(ReportHistory::class)->findBy([
            'psrn' => self::PSRN ,
            'reportForm' => self::REPORT_FORM_ID,
        ]);

        foreach ($reportHistories as $history) {
            $output->writeln("-----------------------------------------------------------------------------------------");
            $unserialized = unserialize($history->getData());
            $output->writeln($unserialized->getCreatedAt());

            $documents = $unserialized->getDocuments();
            foreach ($documents as $document) {
                $title = $document->getMonitoringResult()->getIndicator()->getTitle();
                $period = $document->getPeriod()->getName();
                $file = $document->getFile();

                $output->write($title." ".$period." ".$file."\n");
            }
        }
    }
}
