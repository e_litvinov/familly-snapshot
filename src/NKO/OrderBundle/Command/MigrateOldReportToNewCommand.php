<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 4.9.18
 * Time: 11.33
 */

namespace NKO\OrderBundle\Command;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\Report;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\Bundle\DoctrineBundle\ConnectionFactory;
use Symfony\Component\Stopwatch\Stopwatch;

class MigrateOldReportToNewCommand extends ContainerAwareCommand
{
    use ConstantForMigrateTrait;

    const REPORT_ID = 'report_id';
    const SHIP_NAME = 'topic_name';
    const ORGANIZATION_SHIP_NAME = 'app_name';
    const TOPIC_ID = 'topic_iD';
    const ORGANIZATION_TOPIC_ID = 'organization_topic_id';
    const HISTORY_ID = 'history_id';

    const FILE_NAME = 'IDS.txt';

    const COUNT_PER_CYCLE = 20;

    /** @var EntityManager $em */
    private $em;

    /** @var $traineeships ArrayCollection */
    private $traineeships;

    /** @var  $relationIds array*/
    private $relationIds;


    protected function configure()
    {
        $this
            ->setName('nko:migrate-old-reports-to-new')
            ->setDescription('Migrate reports')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->traineeships = new ArrayCollection();

        $io = new SymfonyStyle($input, $output);
        $io->title('Import applications start');

        $dbName = $io->ask('Database name', 'nko');
        $dbHost = $io->ask('Host', '172.18.0.2');
        $dbUser = $io->ask('User', 'root');
        $dbPassword = $io->askHidden('Password');






        $isRestoreReports = $io->ask('Restore reports or histories (R(r) / H(h))', 'h');

        /** @var ConnectionFactory $connectionFactory */
        $connectionFactory = $this->getContainer()->get('doctrine.dbal.connection_factory');
        $dsn = 'mysql:dbname=' . $dbName . ';host=' . $dbHost . ';charset=UTF8';
        $connection = $connectionFactory->createConnection(array('pdo' => new \PDO($dsn, $dbUser, $dbPassword)));

        if ($isRestoreReports == "R" || $isRestoreReports == 'r') {
            if (file_exists(self::FILE_NAME)) {
                $io->writeln('Reports already restored.');
                return;
            }
            $this->restoreReports($connection, $io);
        } else {
            $this->restoreHistories($connection, $io);
        }
    }

    private function restoreHistory($connection, $io, $count, $offset)
    {
        /** @var Connection $connection */
        /** @var SymfonyStyle $io */
        $histories = $connection->fetchAll('select * from report_history limit '.$count . ' offset ' . $offset);
        $number = 0;

        foreach ($histories as $history) {
            $this->changeId($history['data']);
            $io->writeln("Start change " .(string) ($offset + $number));
            $newHistory = $this->takeHistory($history, $io);
            $this->em->persist($newHistory);
            $io->writeln("End of changing " .(string) ($offset + $number));
            $number++;
        }

        $io->writeln("Start store from  " .(string) ($offset) . " to " . (string) ($offset + $count));

        $this->em->flush();

        $this->em->clear();

        $returnedCount = count($histories);
        $histories = null;

        $io->writeln("End store");

        return $returnedCount;
    }

    private function restoreHistories($connection, $io)
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->getIdsFromFile();
        $offsetString = $io->ask('From what offset start', '0');



        $constCount = self::COUNT_PER_CYCLE;
        $count = $constCount;
        $offset = intval($offsetString);


        while ($count == $constCount) {
            $count = $this->restoreHistory($connection, $io, $constCount, $offset);
            $offset += $constCount;
        }
        $io->writeln("End of all histories");

        if (!unlink(self::FILE_NAME)) {
            $io->writeln("Please delete file '" . self::FILE_NAME . "''");
        }
    }


    private function restoreReports($connection, $io)
    {
        $reports = $connection->fetchAll('
            select *,
             r.id as ?,
             t2.topic_name as ?,
             t3.topic_name as ?,
             t2.id as ?,
             t3.id as ?,
             a.id as ?
             from report r
                left join traineeship_topic t2 on r.topic_id = t2.id
                left join organization o on r.organization_id = o.id
                left join traineeship_topic t3 on o.application_id = t3.id
                left join application_history a on r.application_history_id = a.id
            ', [                    // Переименовываются имена для того, чтобы не перетирались друг другом
            self::REPORT_ID,
            self::SHIP_NAME,
            self::ORGANIZATION_SHIP_NAME,
            self::TOPIC_ID,
            self::ORGANIZATION_TOPIC_ID,
            self::HISTORY_ID,
         ]);
        //                left join application_history a on r.application_history_id = a.id
        foreach ($reports as $report) {
            $io->writeln("Restore report with id = " . $report[self::REPORT_ID]);

            $trainershipTopic = $this->getTraineeship($report, self::TOPIC_ID, self::SHIP_NAME);
            $organization = $this->getOrganization($report);
            $history = $this->findApplicationHistory($report[self::HISTORY_ID]);


            $newReport = $this->setConstants($report);
            $newReport->setAuthor($this->findUser($report['psrn']));
            $this->addOneToMany($newReport, $report[self::REPORT_ID], $connection);
            $newReport->setReportForm($this->getReportForm());
            $newReport->setTraineeshipTopic($trainershipTopic);
            $newReport->setOrganization($organization);
            $newReport->setApplicationHistory($history);
            $this->em->persist($newReport);
            $this->relationIds[$report[self::REPORT_ID]] = $newReport;
        }


        $io->writeln("Writing to database...");
        $this->em->flush();
        $io->writeln('create file with ids');
        $this->createIdFile();
        $io->writeln('end.');
    }


    public function takeHistory($history, $io)
    {
        $newHistory = new ReportHistory();
        $command = new TransformJsonReportDataCommand();
        $data = $command->jsonTransform($io, $history['data']);
        $newHistory->setData($data);
        $newHistory->setPsrn($history['psrn']);
        $newHistory->setRecipient($history['recipient']);   //или так
        $newHistory->setNkoName($history['recipient']); //мб так??
        $newHistory->setIsAccepted($history['is_accepted']);
        $newHistory->setIsSend($history['is_send']);
        $newHistory->setIsValid($history['is_valid']);
        $newHistory->setCreatedAt(new DateTime($history['created_at']));
        $newHistory->setUpdatedAt(new DateTime($history['updated_at']));
        $newHistory->setReportForm($this->getReportForm());

        return $newHistory;
    }
}
