<?php

namespace NKO\OrderBundle\Command;

use NKO\OrderBundle\Entity\BaseApplication;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\ORMException as Exception;
use NKO\UserBundle\Entity\NKOUser;
use NKO\OrderBundle\Entity\Competition;
use NKO\OrderBundle\Entity\ApplicationHistory;

class UpdateFieldsRelatedAppHistoryCommand extends ContainerAwareCommand
{
    const PSRN = 1025401493182;
    const COMPETITION_ID = 43;

    protected function configure()
    {
        $this
            ->setName('nko:update-fields-related-app-history')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * -------------------------------------------------------------------------------------------------------------
         * получаем объекты
         */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $author = $em->getRepository(NKOUser::class)->findOneBy(['psrn' => self::PSRN]);
        $competition = $em->getRepository(Competition::class)->find(self::COMPETITION_ID);

        $baseApplication = $em->getRepository(BaseApplication::class)->findOneBy([
            'author' => $author,
            'competition' => $competition,
        ]);

        $applicationHistory = $em->getRepository(ApplicationHistory::class)->findSendByAuthor($author, $competition);
        $unserialized = unserialize($applicationHistory->getData());

        /**
         * -------------------------------------------------------------------------------------------------------------
         * изменения
         */
        $name = $baseApplication->getHeadOfAccountingFullName();
        $unserialized->setHeadOfAccountingFullName($name);

        /**
         * -------------------------------------------------------------------------------------------------------------
         * блок применения измений
         */
        $applicationHistory->setData(serialize($unserialized));

        try {
            $em->flush();
            $output->writeln('Flushed successfully!');
        } catch (Exception $e) {
            $output->writeln("Error");
        }
    }
}
