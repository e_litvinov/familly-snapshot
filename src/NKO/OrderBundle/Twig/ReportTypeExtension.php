<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/9/17
 * Time: 12:19 PM
 */

namespace NKO\OrderBundle\Twig;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report as FinanceReport;
use NKO\OrderBundle\Entity\Report\MonitoringReport\Report as MonitoringReport;
use NKO\OrderBundle\Entity\ReportHistory;

class ReportTypeExtension extends \Twig_Extension
{
    public function getTests ()
    {
        return [
            new \Twig_SimpleTest('FinanceReport',
                function ($report) {
                    return $report->getReportForm()->getReportClass() == FinanceReport::class;
                }),
            new \Twig_SimpleTest('MonitoringReport',
                function ($report) {
                    return $report->getReportForm()->getReportClass() == MonitoringReport::class;
                })
        ];
    }
}