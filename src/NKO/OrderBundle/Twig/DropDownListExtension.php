<?php

namespace NKO\OrderBundle\Twig;

use NKO\OrderBundle\Entity\ParentInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class DropDownListExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('drop_down_list', array($this, 'group')),
            );
    }

    public function group($data)
    {
/*        usort($data, function($a, $b){
            return $a->data->getId() > $b->data->getId();
        });*/

        $grouped = [];
        foreach($data as $item){
            if(!$item->data instanceof ParentInterface){
                throw new \RuntimeException('Array item must implements ParentInterface!');
            }

            $new_node = [
                'item' => $item,
                'children' => [],
            ];
            $parent = $item->data->getParent();
            if(!$parent){
                $grouped[] = $new_node;
            } else {
                $this->insertNode($grouped, $parent->getId(), $new_node);
            }
        }

        return $grouped;
    }

    private function insertNode(&$grouped, $needle_id, $new_node)
    {
        foreach($grouped as &$item){

            if(!empty($item['children'])){
                $this->insertNode($item['children'], $needle_id, $new_node);
            }

            if($item['item']->data->getId() == $needle_id){
                $item['children'][] = $new_node;
            }
        }

        return false;
    }

    public function getName()
    {
        return 'array_group_extension';
    }
}