<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 9.7.18
 * Time: 1.57
 */

namespace NKO\OrderBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use NKO\OrderBundle\Utils\RenderTables\ReportTableWithFile;
use NKO\OrderBundle\Entity\OriginFileName;
use Symfony\Component\PropertyAccess\PropertyAccess;

class FileExtension extends \Twig_Extension
{
    use ContainerAwareTrait;
    const NON_FILE_NAME = 'Не определено';

    const DEFAULT_ENABLED = [
        'number' => true,
        'button' => true
    ];

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('tableFile', array($this, 'tableFileAction')),
            new \Twig_SimpleFilter('origin_file_name', array($this, 'originFileName')),
        );
    }

    public function tableFileAction($object, $tableName)
    {
        $options = key_exists(get_class($object), ReportTableWithFile::TABLES) ?
            ReportTableWithFile::TABLES[get_class($object)][$tableName] : null;

        $options['level'] = isset($options['level']) ? $options['level'] : 1;

        $options['enabled'] = isset($options['enabled']) ? array_merge(self::DEFAULT_ENABLED, $options['enabled']) : self::DEFAULT_ENABLED;

        return $options;
    }

    public function originFileName($object, $fileName)
    {
        if (!$object || !$fileName) {
            return self::NON_FILE_NAME;
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        $em = $this->container->get('Doctrine')->getManager();
        $value = $accessor->getValue($object, $fileName);

        $originFileName = $em->getRepository(OriginFileName::class)->findOneBy(['autoName' => $value]);

        return  $originFileName ? $originFileName->getOriginName() : self::NON_FILE_NAME;
    }
}