<?php

namespace NKO\OrderBundle\Twig;

use NKO\OrderBundle\Entity\Report\FinanceReport\Report2018\Report as Report2018;
use NKO\OrderBundle\Entity\Report\FinanceReport\Report2019\Report as Report2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2019\Report as MixedReport2019;
use NKO\OrderBundle\Entity\Report\MixedReport\KNS\Report2020\Report as MixedReport2020;
use NKO\OrderBundle\Utils\Numbers;

class ExpenseTypeExtension extends \Twig_Extension
{
    const NUMBERS_OF_DESCRIPTION = [
        Report2018::class => Numbers::ONE,
        Report2019::class => Numbers::ONE,
        MixedReport2019::class => Numbers::TWO,
        MixedReport2020::class => Numbers::TWO,
    ];

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('getTypeOfExpenseLabel', array($this, 'getTypeOfExpenseLabel')),
        );
    }

    public function getTypeOfExpenseLabel($data)
    {
        $class = get_class($data);

        return key_exists($class, self::NUMBERS_OF_DESCRIPTION) ? self::NUMBERS_OF_DESCRIPTION[$class] : null;
    }

    public function getName()
    {
        return 'getTypeOfExpenseLabel';
    }
}
