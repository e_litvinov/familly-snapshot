<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/29/17
 * Time: 1:08 PM
 */

namespace NKO\OrderBundle\Twig;


use NKO\OrderBundle\Entity\BriefApplication2017\PeopleCategory;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ParentNodesChainExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('parent_nodes_chain', array($this, 'getParentNodesChain')),
        );
    }

    public function getParentNodesChain($groupName)
    {
        $em = $this->container->get('Doctrine')->getManager();

        $group = $em
            ->getRepository('NKOOrderBundle:BriefApplication2017\PeopleCategory')
            ->findOneBy(array('name' => $groupName));

        if(!$group) {
            return $groupName;
        }
        else {
            return $this->createChain($group);
        }
    }

    public function createChain($group)
    {
        /**
         * @var PeopleCategory $group
         */
        if($group->getParent()) {
            return $group->getName() . ' => ' . $this->createChain($group->getParent());
        }
        else {
            return $group->getName();
        }
    }
}