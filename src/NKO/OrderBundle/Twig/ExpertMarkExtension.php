<?php

namespace NKO\OrderBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use NKO\OrderBundle\Entity\QuestionMark;
use NKO\OrderBundle\Entity\MarkListSequence\Sequence;

class ExpertMarkExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('generate_sequence_marks', array($this, 'marksAction')),
        ];
    }

    public function marksAction($object)
    {
        $application = unserialize($object->getApplication()->getData());
        $competition = $application->getCompetition();

        $em = $this->container->get('Doctrine')->getManager();
        $questionsQuery = $em->getRepository(QuestionMark::class)->findByCompetition($competition);
        $sequence = $em->getRepository(Sequence::class)->findByCompetition($competition);

        if (count($sequence) > 1) {
            throw new \RuntimeException('Detect more than one sequence for current competition!!!!');
        }

        if ($sequence) {
            $sequence = reset($sequence);

            $criteriaSequence = [];
            $criteries = $sequence->getSections();

            foreach ($criteries as $item) {
                $criteriaSequence[] = $item->getCriteria()->getId();
            }

            $questionsSequence =[];
            $sections = $sequence->getDoubleSections();
            foreach ($sections as $item) {
                $subSections = $item->getSubSections();
                foreach ($subSections as $subSection) {
                    $questionsSequence[$item->getCriteria()->getId()][] = $subSection->getQuestion()->getId();
                }
            }
        }

        $unitsGenerator = $this->container->get('expert_mark_units_generator');
        $baseGround = $unitsGenerator->generate(array_merge(
            ['query' => $questionsQuery],
            ['sequence' => $criteriaSequence],
            ['nestedSequence' => $questionsSequence]
        ));

        return $baseGround->generate();
    }
}
