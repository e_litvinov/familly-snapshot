<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 11/14/17
 * Time: 12:14 PM
 */

namespace NKO\OrderBundle\Twig;

use NKO\OrderBundle\Entity\ApplicationHistory;

class InstanceOfExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('instanceOf', array($this, 'instanceOfAction')),
        );
    }

    public function instanceOfAction($object, $targetClass)
    {
        if ($object instanceof ApplicationHistory) {
            $object = unserialize($object->getData());
        }
        $class = new $targetClass();
        if($object instanceof  $class) {
            return true;
        }

        return false;
    }
}