<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 3/9/17
 * Time: 12:19 PM
 */

namespace NKO\OrderBundle\Twig;


class WordDividerExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('word_divider', array($this, 'wordDivider')),
        );
    }

    public function wordDivider($text)
    {
        $words = explode(" ", $text);

        foreach ($words as $key => $word) {
            if (strlen($word) > 50) {
                for ($position = 0; $position < strlen($word); $position += 50) {
                    $word = substr_replace($word, '-', $position, 0);
                    $words[$key] = $word;
                }
            }
        }

        $text = implode(" ", $words);

        return $text;
    }
}