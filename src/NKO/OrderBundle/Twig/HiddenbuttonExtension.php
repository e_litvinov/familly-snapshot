<?php

namespace NKO\OrderBundle\Twig;

use NKO\OrderBundle\Entity\Report\AnalyticReport;
use NKO\OrderBundle\Entity\Report\AnalyticReport\KNS\Stage1;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class HiddenbuttonExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('hiddenbutton', array($this,'hiddenButtonFilter') ),
        );
    }

    public function hiddenButtonFilter($report)
    {
        $reportClass = $report->getReportForm()->getReportClass();
        return (($reportClass == Stage1\Report::class) || ($reportClass == AnalyticReport\Report::class) || ($reportClass == AnalyticReport\Report2018\Report::class));
    }
}
