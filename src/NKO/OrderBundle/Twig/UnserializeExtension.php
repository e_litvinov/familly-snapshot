<?php

namespace NKO\OrderBundle\Twig;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class UnserializeExtension extends \Twig_Extension
{
    use ContainerAwareTrait;
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('unserialize', array($this, 'unserializeFilter')),
            );
    }

    public function unserializeFilter($data)
    {
        return unserialize($data->getData());
    }

    public function getName()
    {
    return 'unserilize_extension';
    }
}