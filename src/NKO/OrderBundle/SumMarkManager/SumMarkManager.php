<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 3/13/17
 * Time: 1:11 PM
 */

namespace NKO\OrderBundle\SumMarkManager;

use Doctrine\ORM\EntityManager;
use NKO\OrderBundle\Entity\BriefApplication2017\BriefApplication;

class SumMarkManager
{
    const HEIGHTENING_COEFF = 1.5;
    const COEFF = 1;

    /**
     *
     * @var EntityManager
     */
    protected $em;

    public function __constructor(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getCoefficient($appHistory)
    {
        if($appHistory){
            $application = unserialize($appHistory->getData());
            if($application instanceof BriefApplication){
                if($application->getPriorityDirection() && $application->getPriorityDirection()->getName() == 'активизация поддержки замещающих и «кризисных» кровных семей со стороны окружения (родственники, друзья, школы, детские сады, соседи и пр.); развитие взаимоподдержки, консолидации сообществ членов замещающих семей (родителей, «выпускников» замещающих семей)'){

                    return  self::HEIGHTENING_COEFF;
                }
            }
        }
        return self::COEFF;
    }

}