<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.7.18
 * Time: 11.55
 */

namespace NKO\OrderBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\PropertyAccess\PropertyAccess;

class TargetGroupListener implements EventSubscriberInterface
{
    private $tables;

    public function __construct(array $tables)
    {
        $this->tables = $tables;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
        ];
    }

    public function onPreSubmit(FormEvent $event)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        $report = $event->getData();

        foreach ($this->tables as $iteration => $table) {
            if (!isset($report[$table])) {
                continue;
            }

            $startRows = [];
            $result = [];
            static $comparedId = [];

            foreach ($accessor->getValue($event->getForm()->getData(), $table) as $item) {
                $startRows[] = $item->getId();
            }

            foreach ($report[$table] as $key => $row) {
                if (!array_key_exists('_delete', $row)) {
                    $result[] = $row;
                    $index = count($result)-1;

                    if ($key !== $index && isset($startRows[$key])) {
                        $comparedId[$iteration][$startRows[$key]] = $startRows[$index];
                    }
                }
            }

            $report[$table] = $result;
            $currentTableIds = isset($comparedId[$iteration]) ? $comparedId[$iteration] : null;

            foreach ($report[$iteration] as $secondKey => $row) {
                if ($currentTableIds && array_key_exists($row['problem'], $currentTableIds)) {
                    $report[$iteration][$secondKey]['problem'] = (string) $currentTableIds[$row['problem']];
                }
            }
        }
        $event->setData($report);
    }
}
