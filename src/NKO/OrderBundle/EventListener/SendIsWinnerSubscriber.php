<?php

namespace NKO\OrderBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use NKO\OrderBundle\Entity\Application;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Repository\ApplicationHistoryRepository;
use NKO\OrderBundle\Resolver\CacheResolver;
use NKO\UserBundle\Entity\NKOUser;
use Sonata\AdminBundle\Admin\Pool;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class SendIsWinnerSubscriber implements EventSubscriber
{
    /**
     * @var Pool
     */
    private $adminPool;
    /**
     * @var CacheResolver
     */
    private $cache;
    /**
     * @var ApplicationHistoryRepository
     */
    private $historyRepository;
    /**
     * @var Container
     */
    private $container;
    /**
     * @var ApplicationHistory
     */
    private $history;
    /**
     * @var Application
     */
    private $object;
    /**
     * @var NKOUser
     */
    private $author;

    private $isAlreadyUsedPreUpdate = false;
    private $isAlreadyUsedPostFlush = false;

    private $winner = false;

    private $isAccepted = false;

    /**
     * LoaderInterface constructor.
     */
    public function __construct(Pool $adminPool, Container $container)
    {
        $this->adminPool = $adminPool;
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
            'postFlush',
        );
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        if (!$this->isAlreadyUsedPreUpdate) {
            $this->isAlreadyUsedPreUpdate = true;

            $this->object = $args->getEntity();

            if (!($this->object instanceof BaseApplication) || count($args->getEntityChangeSet()) != 1 || !key_exists('winner', $args->getEntityChangeSet())) {
                return;
            }

            $this->winner = $this->object->getWinner();
            $this->isAccepted = $this->object->getIsAccepted();

            $this->cache = $this->container->get('nko_order.resolver.cache_resolver');
            $this->historyRepository = $args->getEntityManager()->getRepository(ApplicationHistory::class);

            $this->author = $this->object->getAuthor();

            $admin = $this->adminPool->getAdminByClass(get_class($this->object));
            $this->cache->generateItem($admin, $this->object->getId(), $this->author->getId());


            $this->history = $this->historyRepository->findSpreadByAuthor($this->author, $this->object->getCompetition()->getId());
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        if ($this->history && !$this->isAlreadyUsedPostFlush) {
            $this->isAlreadyUsedPostFlush = true;
            $this->cache->delete();

            $this->object->setIsAutosaved(false);

            $this->container->get('nko.order.revert_manager')->revert($this->history);

            $this->object->setWinner($this->winner);
            $this->object->setIsAccepted($this->isAccepted);

            $em = $args->getEntityManager();
            $em->flush();
        }
    }
}
