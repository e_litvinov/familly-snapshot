<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 27.7.18
 * Time: 0.31
 */

namespace NKO\OrderBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class ChangeFeedbackAttachmentListener implements EventSubscriberInterface
{
    private $tables;

    public function __construct(array $tables)
    {
        $this->tables = $tables;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
        ];
    }

    public function onPreSubmit(FormEvent $event)
    {
        $report = $event->getData();

        foreach ($this->tables as $key => $attachment) {
            $unChecked = [];

            foreach ($report[$attachment] as $row) {
                if (array_key_exists('_delete', $row)) {
                    continue;
                }

                if ($row['practice']) {
                    $practice[] = $row['practice'];
                }
                $practice = isset($practice) ? array_unique($practice) : null;

                if ($practice) {
                    foreach ($report[$key] as $row) {
                        if (in_array($row['idDirectResult'], $practice) && !$row['isFeedback']) {
                            $unChecked[] = $row['idDirectResult'];
                        }
                    }
                }
            }

            if ($unChecked) {
                foreach ($report[$attachment] as $key => $row) {
                    $this->deleteRowsWithoutPractice($report[$attachment], $key, $unChecked);
                }
            }
        }

        $event->setData($report);
    }

    private function deleteRowsWithoutPractice(&$table, $keyRow, $unChecked)
    {
        if (count($table) === 1 && in_array($table[$keyRow]['practice'], $unChecked)) {
            $this->clearArray($table[$keyRow]);
        } elseif (in_array($table[$keyRow]['practice'], $unChecked)) {
            unset($table[$keyRow]);
        }
    }

    private function clearArray(&$row)
    {
        foreach ($row as $key => $field) {
            !is_array($field) ? ($row[$key] = null) : ($this->clearArray($field));
        }
    }
}
