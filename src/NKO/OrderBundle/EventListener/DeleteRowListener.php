<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 17.7.18
 * Time: 11.55
 */

namespace NKO\OrderBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DeleteRowListener implements EventSubscriberInterface
{
    private $tables;

    public function __construct(array $tables)
    {
        $this->tables = $tables;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
        ];
    }

    public function onPreSubmit(FormEvent $event)
    {
        $report = $event->getData();

        foreach ($this->tables as $table) {
            if (!isset($report[$table])) {
                continue;
            }
            $report = $this->deleteRows($report, $table);
        }

        $event->setData($report);
    }

    private function deleteRows($report, $table)
    {
        foreach ($report[$table] as $row => $value) {
            foreach ($report[$table][$row] as $key => $item) {
                if ($key == '_delete') {
                    unset($report[$table][$row]);
                }
            }
        }
        return $report;
    }
}
