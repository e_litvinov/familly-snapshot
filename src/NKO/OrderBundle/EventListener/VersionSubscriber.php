<?php

namespace NKO\OrderBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use NKO\OrderBundle\Entity\BaseApplication;

class VersionSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $application = $args->getObject();

        if(!$application instanceof BaseApplication) {
            return;
        }

        $this->formVersion($args, $application, '1');
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $application = $args->getObject();

        if(!$application instanceof BaseApplication) {
            return;
        }

        preg_match('/([0-9]+)(?=-)/', $application->getVersion(), $matches);
        $prev_version = $matches[0];

        $this->formVersion($args, $application, $prev_version + 1);
    }

    public function formVersion(LifecycleEventArgs $args, BaseApplication $application, $save_version)
    {
        $hyphen = '-';

        $competition = $this->getCompetition($application);
        $version = $save_version . $hyphen . $application->getAuthor()->getPSRN() . $hyphen . $competition . $hyphen . $application->getSendVersion();

        $application->setVersion($version);
    }

    public function getCompetition(BaseApplication $application)
    {
        if(!$application->getCompetition()) {
            return '0';
        }

        return $application->getCompetition()->getId();
    }
}