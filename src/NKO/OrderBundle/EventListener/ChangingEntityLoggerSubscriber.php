<?php

namespace NKO\OrderBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\GrantConfig;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ChangingEntityLoggerSubscriber implements EventSubscriber
{
    const BOOL_FIELDS_MAP = [
        ApplicationHistory::class => [
            'isAppropriate' => 'Изменение статуса “Соответствует”',
            'isAccepted' => 'Изменение статуса “Принять”',
            'winner' => 'Изменение статуса “Победитель”',
        ],
        ReportHistory::class => [
            'underConsideration' => 'Изменение статуса “На рассмотрении”',
            'isAccepted' => 'Изменение статуса “Принять”',
        ]
    ];

    /**
     * @var Container
     */
    private $container;

    private $serviceName;
    private $nameOfAction;
    private $object;

    private $isAlreadyUsedPreUpdate = false;
    private $isAlreadyUsedPostFlush = false;

    /**
     * LoaderInterface constructor.
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return [
            'preUpdate',
            'postFlush',
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        if (!$this->isAlreadyUsedPreUpdate) {
            $this->isAlreadyUsedPreUpdate = true;

            $object = $args->getEntity();

            if (count($args->getEntityChangeSet()) != 1) {
                return;
            }

            $key = key($args->getEntityChangeSet());
            $className = get_class($object);

            switch ($className) {
                case ApplicationHistory::class:
                    if (!key_exists($key, self::BOOL_FIELDS_MAP[$className])) {
                        return;
                    }

                    $this->serviceName = 'NKO\LoggerBundle\Loggers\ApplicationLogger';
                    $this->nameOfAction = self::BOOL_FIELDS_MAP[$className][$key] . $this->convertBoolValue($args->getEntityChangeSet()[$key][1]);
                    $this->object = unserialize($object->getData());
                    break;
                case ReportHistory::class:
                    if (!key_exists($key, self::BOOL_FIELDS_MAP[$className])) {
                        return;
                    }

                    $this->serviceName = 'NKO\LoggerBundle\Loggers\ReportLogger';
                    $this->nameOfAction = self::BOOL_FIELDS_MAP[$className][$key] . $this->convertBoolValue($args->getEntityChangeSet()[$key][1]);
                    $this->object = unserialize($object->getData());
                    break;
                case GrantConfig::class:
                    if ('sumGrant' != $key) {
                        return;
                    }

                    if (!$nameOfAction = $this->getChangingActionOfGrantConfig($args->getEntityChangeSet()[$key][0], $args->getEntityChangeSet()[$key][1])) {
                        return;
                    }

                    $this->serviceName = 'NKO\LoggerBundle\Loggers\ApplicationLogger';
                    $this->nameOfAction = $nameOfAction . $object->getReportForm()->getTitle();
                    $this->object = unserialize($object->getApplicationHistory()->getData());
                    break;

                default:
                    $this->isAlreadyUsedPreUpdate = false;
                    return;
            }
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        if ($this->serviceName && !$this->isAlreadyUsedPostFlush) {
            $this->isAlreadyUsedPostFlush = true;
            $this->container->get($this->serviceName)->logAction($this->nameOfAction, $this->object);
        }
    }

    protected function convertBoolValue($bool)
    {
        return 'на '. ($bool ? '"Да"' : '"Нет"');
    }

    protected function getChangingActionOfGrantConfig($wasSum, $becameSum)
    {
        //Для открытия отчёт нужно проставить сумму гранта(До этого не было суммы гранта, а теперь стала)
        if (!$wasSum && $becameSum) {
            return 'Открытие отчёта ';
        } elseif ($wasSum && $becameSum) {
            return 'Изменение "Суммы гранта" для отчёта ';
        } elseif ($wasSum && !$becameSum) {
            return 'Удаление "Суммы гранта" для отчёта ';
        }

        return null;
    }
}
