<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 5/21/18
 * Time: 5:25 PM
 */

namespace NKO\OrderBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DocumentListener implements EventSubscriberInterface
{
    private $collectionName;
    private $fileName;

    public function __construct(array $collectionName, array $fileName = null)
    {
        $this->collectionName = $collectionName;
        $this->fileName = $fileName;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit',
        ];
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();

        foreach ($this->collectionName as $key => $targetFieldName) {
            $isTwoCollections = is_numeric($key) ? null : true;
            $firstFieldName = $isTwoCollections ? $key : $targetFieldName;

            if (!array_key_exists($firstFieldName, $data)) {
                continue;
            }

            foreach ($data[$firstFieldName] as $firstFieldKey => &$firstField) {
                if (isset($firstField[$targetFieldName]) and $isTwoCollections) {
                    foreach ($firstField[$targetFieldName] as $secondFieldName => &$document) {
                        $this->copyField($document, $data[$firstFieldName][$firstFieldKey][$targetFieldName][$secondFieldName], $secondFieldName);
                    }
                } else {
                    $this->copyField($firstField, $data[$firstFieldName][$firstFieldKey], $firstFieldKey);
                }
            }
        }

        $event->setData($data);
    }

    public function copyField(&$document, $docs, $key)
    {
        $fieldName = 'file';

        if ($this->fileName && array_key_exists($key, $this->fileName)) {
            $fieldName = $this->fileName[$key];
        }

        if (array_key_exists($fieldName, $document) && !($document[$fieldName] instanceof UploadedFile)) {
            if (empty($docs['fileName'])) {
                $document[$fieldName] = true;    // передаем любое не пустое значение, иначе подгрузится старый файл. но удален он тоже не должен быть
            } else {
                $document[$fieldName] = $docs['fileName'];
            }
        }
    }
}
