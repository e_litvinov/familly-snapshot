<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 7/18/18
 * Time: 4:13 PM
 */

namespace NKO\OrderBundle\EventListener;

use NKO\OrderBundle\Utils\Report\AttachmentListenerUtils;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AnalyticPublicationsListener implements EventSubscriberInterface
{
    private $data;

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'onPreSubmit'
        ];
    }

    public function onPreSubmit(FormEvent $event)
    {
        $this->data = $event->getData();

        $this->updateFormAttachments($this->data['analyticPublications'][0]['fact'], AttachmentListenerUtils::PUBLICATION_ATTACHMENTS, AttachmentListenerUtils::PUBLICATION_ATTACHMENTS_DATA);
        $this->updateFormAttachments($this->data['analyticPublications'][1]['fact'], AttachmentListenerUtils::MATERIAL_ATTACHMENTS, AttachmentListenerUtils::MATERIAL_ATTACHMENTS_DATA);

        $event->setData($this->data);
    }

    private function updateFormAttachments($factCount, $field, $dataValues)
    {
        $existCount = array_key_exists($field, $this->data) ? count($this->data[$field]) : 0;
        $count =  ((int) $factCount);
        $difference = $count - $existCount;

        if ($difference > 0) {
            for ($i = $existCount; $i < $count; $i++) {
                $this->data[$field][] = $dataValues;
            }
        } elseif ($difference < 0) {
            for ($i = $count; $i < $existCount; $i++) {
                unset($this->data[$field][$i]);
            }
        }
    }
}
