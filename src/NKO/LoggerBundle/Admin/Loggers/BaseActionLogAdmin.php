<?php

namespace NKO\LoggerBundle\Admin\Loggers;

use NKO\LoggerBundle\Entity\ApplicationActionLogger;
use NKO\LoggerBundle\Entity\BaseActionLog;
use NKO\LoggerBundle\Entity\MarkListActionLogger;
use NKO\LoggerBundle\Entity\MethodicalMaterialsActionLogger;
use NKO\LoggerBundle\Entity\ReportActionLogger;
use NKO\LoggerBundle\Entity\ReportTemplateActionLogger;
use NKO\LoggerBundle\Entity\UserActionLogger;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DateTimeRangePickerType;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BaseActionLogAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('action')
            ->add('userName')
            ->add('clientIp')
            ->add('objectName')
            ->add('methodicalMaterialsName', CallbackFilter::class, [
                'callback' => function ($queryBuilder, $alias, $field, $value) {
                    if (!$value['value'] && !is_string($value['value'])) {
                        return;
                    }
                    $lowerCase = strtolower($value['value']);
                    $methodicalMaterialsLoggerClass = MethodicalMaterialsActionLogger::class;
                    $queryBuilder
                        ->andWhere("{$alias} instance of {$methodicalMaterialsLoggerClass}")
                        ->andWhere("lower({$alias}.objectName) like '%{$lowerCase}%'")
                    ;

                    return true;
                },
                'label' => 'Название методического материала',
            ])
            ->add('email')
            ->add('createdAt', 'doctrine_orm_date_range', ['field_type'=> DateTimeRangePickerType::class])
            ->add('objectType', CallbackFilter::class, [
                'callback' => function ($queryBuilder, $alias, $field, $value) {
                    if (!$value['value'] || !is_array($value['value'])) {
                        return;
                    }

                    $sqls = [];
                    foreach ($value['value'] as $subClass) {
                            $sqls[] = "{$alias} instance of {$subClass}";
                    }

                    if ($sqls) {
                        $queryBuilder->andWhere(implode(' or ', $sqls));
                    }

                    return true;
                },
                'asd' => 'asd',
                'field_type' => ChoiceType::class,
                'field_options' => [
                    'multiple' => true,
                    'choice_translation_domain' => false,
                    'choices' => BaseActionLog::getSubclassesTypes(),
                ]
            ])
        ;
    }

    public function toString($object)
    {
        return 'Запись журнала';
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('action')
            ->add('userName')
            ->add('clientIp')
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                ]
            ])
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection
            ->remove('edit')
            ->remove('delete')
            ->remove('create')
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->tab('Общая Информация')
                ->add('action')
                ->add('userName')
                ->add('browser')
                ->add('supposedBrowser')
                ->add('clientIp')
                ->add('roles')
                ->add('createdAt')
            ->end()
        ;
        $show->end();

        $show->tab('Информация об объекте');
        $this->addTypeFields($show);
        $show->end();
    }

    protected function addTypeFields(ShowMapper $show)
    {
        $subject = $this->getSubject();

        switch (true) {
            case $subject instanceof ReportActionLogger:
                $this->addReportFields($show);
                break;
            case $subject instanceof ReportTemplateActionLogger:
                $this->addReportTemplateFields($show);
                break;
            case $subject instanceof ApplicationActionLogger:
                $this->addApplicationFields($show);
                break;
            case $subject instanceof MarkListActionLogger:
                $this->addMarkListFields($show);
                break;
            case $subject instanceof UserActionLogger:
                $this->addUserFields($show);
                break;
            //Last case
            case $subject instanceof BaseActionLog:
                $this->addUntypedFields($show);
                break;
        }
    }

    private function addReportFields(ShowMapper $show)
    {
        $show
            ->add('reportName')
            ->add('period', 'text')
            ->add('organizationName')
            ->add('psrn')
            ->add('organizationLogin')
        ;
    }

    private function addReportTemplateFields(ShowMapper $show)
    {
        $show
            ->add('reportName')
            ->add('organizationName')
            ->add('psrn')
            ->add('organizationLogin')
        ;
    }

    private function addApplicationFields(ShowMapper $show)
    {
        $show
            ->add('isSend')
            ->add('isSpread')
            ->add('applicationName')
            ->add('organizationName')
            ->add('psrn')
            ->add('organizationLogin')
        ;
    }

    private function addMarkListFields(ShowMapper $show)
    {
        $show
            ->add('competitionName')
            ->add('expert')
            ->add('organizationName')
            ->add('psrn')
            ->add('organizationLogin')
        ;
    }

    private function addUserFields(ShowMapper $show)
    {
        $show
            ->add('name')
            ->add('organizationLogin')
        ;

        //For NKOUser
        if ($this->getSubject()->getPsrn()) {
            $show->add('psrn');
        } else {
            $show
                ->add('userOldRoles')
                ->add('userNewRoles')
            ;
        }
    }

    private function addUntypedFields(ShowMapper $show)
    {
        $show->add('objectName');
    }
}
