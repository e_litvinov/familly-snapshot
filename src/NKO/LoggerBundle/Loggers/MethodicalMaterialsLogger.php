<?php

namespace NKO\LoggerBundle\Loggers;

use NKO\InformationMaterialsBundle\Entity\MethodicalMaterials;
use NKO\LoggerBundle\Entity\MethodicalMaterialsActionLogger;
use Symfony\Component\HttpFoundation\RequestStack;

class MethodicalMaterialsLogger extends BaseLogger implements LoggerInterface
{
    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->log = new MethodicalMaterialsActionLogger();
        parent::__construct($em, $request, $tokenStorage);
    }

    public function logAction($action, $object = null)
    {
        if (!$object instanceof MethodicalMaterials) {
            return;
        }

        $this->log->setAction($action);
        $this->log->setMethodicMaterial($object);
        $this->em->flush();
    }
}
