<?php


namespace NKO\LoggerBundle\Loggers;

use NKO\LoggerBundle\Entity\UserActionLogger;
use NKO\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;

class UserLogger extends BaseLogger implements LoggerInterface
{
    protected $isNewRoleNeed = true;

    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->log = new UserActionLogger();
        parent::__construct($em, $request, $tokenStorage);
    }

    public function setStartedInfo($object)
    {
        if (!$object instanceof User) {
            return;
        }

        $this->log->setOldRoles($object);
    }

    public function setIsNewRoleNeed($flag)
    {
        $this->isNewRoleNeed = $flag;
    }

    public function logAction($action, $object = null)
    {
        if (!$object instanceof User) {
            return;
        }

        $this->log->setAction($action);
        $this->log->setUserObject($object);
        $this->removeNewRoles();

        $this->em->flush();
    }

    public function logFullAction($action, $object = null)
    {
        if (!$object instanceof User) {
            return;
        }

        $this->setStartedInfo($object);

        $this->log->setAction($action);
        $this->log->setUserObject($object);
        $this->removeNewRoles();

        $this->em->flush();
    }
    
    private function removeNewRoles()
    {
        if (!$this->isNewRoleNeed) {
            $this->log->setNewRolesFromArray([]);
        }
    }
}
