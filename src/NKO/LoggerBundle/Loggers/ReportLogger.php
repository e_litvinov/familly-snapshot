<?php

namespace NKO\LoggerBundle\Loggers;

use NKO\LoggerBundle\Entity\ReportActionLogger;
use NKO\OrderBundle\Entity\BaseReport;
use Symfony\Component\HttpFoundation\RequestStack;

class ReportLogger extends BaseLogger implements LoggerInterface
{
    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->log = new ReportActionLogger();
        parent::__construct($em, $request, $tokenStorage);
    }

    public function logAction($action, $object = null)
    {
        if (!$object instanceof BaseReport) {
            return;
        }

        $object->setAuthor($this->em->find(get_class($object->getAuthor()), $object->getAuthor()->getId()));
        if ($object->getPeriod()) {
            $object->setPeriod($this->em->find(get_class($object->getPeriod()), $object->getPeriod()->getId()));
        }

        $this->log->setAction($action);
        $this->log->setReport($object);
        $this->em->flush();
    }
}
