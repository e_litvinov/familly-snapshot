<?php

namespace NKO\LoggerBundle\Loggers;

use Doctrine\ORM\EntityManager;
use NKO\LoggerBundle\Entity\BaseActionLog;
use NKO\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class BaseLogger implements LoggerInterface
{
    /** @var EntityManager
     *
     */
    protected $em;

    /** @var Request
     *
     */
    protected $request;

    /**
     * @var TokenStorage $tokenStorage
     */
    protected $tokenStorage;

    /**
     * @var BaseActionLog
     */
    protected $log;

    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->em = $em;
        $this->request = $request->getCurrentRequest();
        $this->tokenStorage = $tokenStorage;

        $this->logBaseInfo();
    }

    public function logBaseInfo()
    {
        if (!$this->log) {
            $this->log = new BaseActionLog();
        }

        $this->em->persist($this->log);

        $this->log->setCreatedAt(new \DateTime());
        $this->log->setClientAgent($this->request->headers->get('user-agent'));
        $this->log->setClientIp($this->request->getClientIp());
        $user = $this->tokenStorage->getToken()->getUser() instanceof User ? $this->tokenStorage->getToken()->getUser() : null;
        $this->log->setUser($user);
        $this->log->setUrl($this->request->getUri());
    }

    /**
     * Сохраняет только последний лог за один Запрос(процесс)
     */
    public function logAction($action, $object = null)
    {
        if (!is_string($object)) {
            return;
        }

        $this->log->setAction($action);
        $this->log->setObjectName($object);
        $this->em->flush();
    }

    public function setUser(User $user)
    {
        $this->log->setUser($user);

        return $this;
    }
}
