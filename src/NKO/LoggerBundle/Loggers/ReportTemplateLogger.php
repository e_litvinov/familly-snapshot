<?php

namespace NKO\LoggerBundle\Loggers;

use NKO\LoggerBundle\Entity\ReportTemplateActionLogger;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use Symfony\Component\HttpFoundation\RequestStack;

class ReportTemplateLogger extends BaseLogger implements LoggerInterface
{
    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->log = new ReportTemplateActionLogger();
        parent::__construct($em, $request, $tokenStorage);
    }

    public function logAction($action, $object = null)
    {
        if (!$object instanceof BaseReportTemplate) {
            return;
        }

        $this->log->setAction($action);
        $this->log->setReport($object);
        $this->em->flush();
    }
}
