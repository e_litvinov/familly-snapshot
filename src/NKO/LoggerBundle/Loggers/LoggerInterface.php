<?php

namespace NKO\LoggerBundle\Loggers;

interface LoggerInterface
{
    public function logAction($action, $object);
}