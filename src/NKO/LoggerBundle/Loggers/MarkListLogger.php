<?php

namespace NKO\LoggerBundle\Loggers;

use NKO\LoggerBundle\Entity\MarkListActionLogger;
use NKO\OrderBundle\Entity\MarkList;
use Symfony\Component\HttpFoundation\RequestStack;

class MarkListLogger extends BaseLogger implements LoggerInterface
{
    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->log = new MarkListActionLogger();
        parent::__construct($em, $request, $tokenStorage);
    }

    public function logAction($action, $object = null)
    {
        if (!$object instanceof MarkList) {
            return;
        }

        $object->setExpert($this->em->find(get_class($object->getExpert()), $object->getExpert()->getId()));

        $application = $object->getApplication();
        $application->setAuthor($this->em->find(get_class($application->getAuthor()), $application->getAuthor()->getId()));

        $this->log->setAction($action);
        $this->log->setMarkList($object);
        $this->em->flush();
    }
}
