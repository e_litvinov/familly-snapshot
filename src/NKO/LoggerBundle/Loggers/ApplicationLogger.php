<?php

namespace NKO\LoggerBundle\Loggers;

use NKO\LoggerBundle\Entity\ApplicationActionLogger;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Component\HttpFoundation\RequestStack;

class ApplicationLogger extends BaseLogger implements LoggerInterface
{

    public function __construct($em, RequestStack $request, $tokenStorage)
    {
        $this->log = new ApplicationActionLogger();
        parent::__construct($em, $request, $tokenStorage);
    }

    public function logAction($action, $object = null)
    {
        if (!$object instanceof BaseApplication) {
            return;
        }

        $object->setAuthor($this->em->find(get_class($object->getAuthor()), $object->getAuthor()->getId()));
        $this->setIsSpread($object);
        $this->log->setAction($action);
        $this->log->setApplication($this->getApplication($object));
        $this->em->flush();
    }

    private function getApplication(BaseApplication $application)
    {
        $application->setCompetition($this->em->getRepository(Competition::class)->find($application->getCompetition()));

        return $application;
    }

    public function setIsSpread($object)
    {
        $this->log->setIsSpread(
            (bool) $this->em->getRepository(ApplicationHistory::class)->findSpreadedByPsrns(
                $object->getAuthor()->getPsrn(),
                $object->getCompetition()->getId()
            )
        );
    }
}
