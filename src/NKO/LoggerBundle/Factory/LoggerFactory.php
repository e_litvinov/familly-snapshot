<?php

namespace NKO\LoggerBundle\Factory;

use NKO\InformationMaterialsBundle\Entity\MethodicalMaterials;
use NKO\OrderBundle\Entity\BaseApplication;
use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\MarkList;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;
use NKO\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoggerFactory
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function generateLogger($object)
    {
        if (is_string($object)) {
            $loggerName = 'NKO\LoggerBundle\Loggers\BaseLogger';
        } else {
            switch (true) {
                case $object instanceof BaseApplication:
                    $loggerName = 'NKO\LoggerBundle\Loggers\ApplicationLogger';
                    break;
                case $object instanceof BaseReport:
                    $loggerName = 'NKO\LoggerBundle\Loggers\ReportLogger';
                    break;
                case $object instanceof MarkList:
                    $loggerName = 'NKO\LoggerBundle\Loggers\MarkListLogger';
                    break;
                case $object instanceof BaseReportTemplate:
                    $loggerName = 'NKO\LoggerBundle\Loggers\ReportTemplateLogger';
                    break;
                case $object instanceof User:
                    $loggerName = 'NKO\LoggerBundle\Loggers\UserLogger';
                    break;
                case $object instanceof MethodicalMaterials:
                    $loggerName = 'NKO\LoggerBundle\Loggers\MethodicalMaterialsLogger';
                    break;
                default:
                    throw new \Exception('Try to generate logger with not known object.');
            }
        }

        return $this->container->get($loggerName);
    }
}
