<?php

namespace NKO\LoggerBundle\Entity;

use NKO\InformationMaterialsBundle\Entity\MethodicalMaterials;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="methodic_materials_action_logger")
 */
class MethodicalMaterialsActionLogger extends BaseActionLog
{
    public function setMethodicMaterial(MethodicalMaterials $materials)
    {
        $this->setObjectName($materials->getName());
    }
}
