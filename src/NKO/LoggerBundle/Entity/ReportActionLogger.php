<?php

namespace NKO\LoggerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseReport;

/**
 * @ORM\Entity()
 * @ORM\Table(name="report_action_logger")
 */
class ReportActionLogger extends BaseActionLog
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $report;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\OrderBundle\Entity\PeriodReport")
     */
    protected $period;

    /**
     * @ORM\Column(type="string")
     */
    protected $reportName;

    /**
     * @ORM\Column(type="text")
     */
    protected $organizationName;

    /**
     * @ORM\Column(type="string")
     */
    protected $organizationLogin;

    /**
     * @ORM\Column(type="string")
     */
    protected $psrn;

    public function setReport(BaseReport $report)
    {
        $this->report = $report->getId();
        $this->period = $report->getPeriod();
        $this->organizationName = $report->getAuthor()->getNkoName();
        $this->organizationLogin = $report->getAuthor()->getEmail();
        $this->psrn = $report->getAuthor()->getPsrn();
        $this->reportName = $report->getReportForm()->getTitle();

        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function getReportName()
    {
        return $this->reportName;
    }

    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    public function getOrganizationLogin()
    {
        return $this->organizationLogin;
    }

    public function getPsrn()
    {
        return $this->psrn;
    }
}
