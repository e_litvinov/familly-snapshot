<?php

namespace NKO\LoggerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Jenssegers\Agent\Agent;
use NKO\UserBundle\Entity\User;
use NKO\UserBundle\Utils\RolesUtils;

/**
 * @ORM\Entity()
 * @ORM\Table(name="base_action_log")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * При добавлении нового класса, добавить его в метод getSubclassesTypes, он используется в фильтрации.
 * @ORM\DiscriminatorMap({
 *     "base" = "BaseActionLog",
 *     "report" = "ReportActionLogger",
 *     "report_template" = "ReportTemplateActionLogger",
 *     "mark_list" = "MarkListActionLogger",
 *     "user" = "UserActionLogger",
 *     "application" = "ApplicationActionLogger",
 *     "methodicMaterials" = "MethodicalMaterialsActionLogger",
 * })
 */
class BaseActionLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $clientIp;

    /**
     * @ORM\Column(type="text")
     */
    protected $action;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $objectName;

    /**
     * @ORM\Column(type="text")
     */
    protected $url;

    /**
     * @ORM\Column(type="string")
     */
    protected $clientAgent;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    protected $user;

    /**
     * @ORM\Column(type="string", )
     */
    protected $email = '';

    /**
     * @ORM\Column(type="integer", name="user_constant_id")
     */
    protected $userConstantId;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $roles;

    /**
     * @ORM\Column(type="text")
     */
    protected $userName;

    public static function getSubclassesTypes()
    {
        return [
            'Другое' => self::class,
            'Отчёт' => ReportActionLogger::class,
            'Плановые показатели' => ReportTemplateActionLogger::class,
            'Оценка' => MarkListActionLogger::class,
            'Пользователь' => UserActionLogger::class,
            'Заявка' => ApplicationActionLogger::class,
            'Методические материалы' => MethodicalMaterialsActionLogger::class,
        ];
    }

    public function getId()
    {
        return $this->id;
    }


    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    public function getClientIp()
    {
        return $this->clientIp;
    }

    public function setClientIp($clientIp)
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    public function getClientAgent()
    {
        return $this->clientAgent;
    }

    public function setClientAgent($clientAgent)
    {
        $this->clientAgent = $clientAgent;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setUser(User $user = null)
    {
        if (!$user) {
            return $this;
        }
        $this->user = null;
        $this->userConstantId = $user->getId();
        $this->email = $user->getEmail();

        $roles = $this->convertRoles($user->getRoles());
        $this->roles = $roles;

        $this->userName = $user->getFullName();

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function getRoles()
    {
        return implode(', ', $this->roles);
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getObjectName()
    {
        return $this->objectName;
    }

    public function setObjectName($objectName)
    {
        $this->objectName = $objectName;

        return $this;
    }

    protected function convertRoles($roles)
    {
        foreach ($roles as $key => $role) {
            if (!($role == 'ROLE_USER')) {
                $roles[$key] = RolesUtils::ROLES[$role];
            } else {
                unset($roles[$key]);
            }
        }

        return array_unique($roles);
    }

    public function getSupposedBrowser()
    {
        $agent = new Agent();
        $agent->setUserAgent($this->clientAgent);

        return $agent->browser(). ' '. $agent->version($agent->browser());
    }

    public function getBrowser()
    {
        return sprintf('Инструменты определения браузера: %s;
            Инструмент определения ip: стандартный метод фреймворка Symfony getClientIp.', $this->clientAgent);
    }
}
