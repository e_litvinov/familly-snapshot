<?php


namespace NKO\LoggerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\Report\BaseReportTemplate;

/**
 * @ORM\Entity()
 * @ORM\Table(name="report_template_action_logger")
 */
class ReportTemplateActionLogger extends BaseActionLog
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $reportTemplate;

    /**
     * @ORM\Column(type="string")
     */
    protected $reportName;

    /**
     * @ORM\Column(type="text")
     */
    protected $organizationName;

    /**
     * @ORM\Column(type="string")
     */
    protected $organizationLogin;

    /**
     * @ORM\Column(type="string")
     */
    protected $psrn;

    public function setReport(BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplate = $reportTemplate->getId();
        $this->organizationName = $reportTemplate->getAuthor()->getNkoName();
        $this->organizationLogin = $reportTemplate->getAuthor()->getEmail();
        $this->psrn = $reportTemplate->getAuthor()->getPsrn();
        $this->reportName = $reportTemplate->getReportForm()->getTitle();

        return $this;
    }

    public function getReportTemplate()
    {
        return $this->reportTemplate;
    }


    public function getReportName()
    {
        return $this->reportName;
    }

    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    public function getOrganizationLogin()
    {
        return $this->organizationLogin;
    }

    public function getPsrn()
    {
        return $this->psrn;
    }
}
