<?php

namespace NKO\LoggerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\BaseApplication;

/**
 * @ORM\Entity()
 * @ORM\Table(name="application_action_logger")
 */
class ApplicationActionLogger extends BaseActionLog
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $application;

    /**
     * @ORM\Column(type="text")
     */
    protected $applicationName;

    /**
     * @ORM\Column(type="text")
     */
    protected $organizationName;

    /**
     * @ORM\Column(type="string")
     */
    protected $organizationLogin;

    /**
     * @ORM\Column(type="string")
     */
    protected $psrn;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isSend;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isSpread;

    public function setApplication(BaseApplication $application)
    {
        $this->application = $application->getId();

        $this->applicationName = $application->getCompetition()->getName();
        $this->organizationName = $application->getAuthor()->getNkoName();
        $this->organizationLogin = $application->getAuthor()->getEmail();
        $this->psrn = $application->getAuthor()->getPsrn();
        $this->isSend = $application->getIsSend() ? $application->getIsSend() : false;

        return $this;
    }

    public function getApplication()
    {
        return $this->application;
    }

    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    public function getOrganizationLogin()
    {
        return $this->organizationLogin;
    }

    public function getPsrn()
    {
        return $this->psrn;
    }

    public function getApplicationName()
    {
        return $this->applicationName;
    }

    public function getIsSend()
    {
        return $this->isSend;
    }

    public function getIsSpread()
    {
        return $this->isSpread;
    }

    public function setIsSpread($isSpread)
    {
        $this->isSpread = $isSpread;

        return $this;
    }
}
