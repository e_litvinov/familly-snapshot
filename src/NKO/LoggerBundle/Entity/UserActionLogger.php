<?php

namespace NKO\LoggerBundle\Entity;

use NKO\UserBundle\Entity\NKOUser;
use NKO\UserBundle\Entity\User;
use NKO\UserBundle\Utils\RolesUtils;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="user_action_logger")
 */
class UserActionLogger extends BaseActionLog
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $organizationLogin;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $userOldRoles;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $userNewRoles;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $psrn;

    public function setUserObject(User $user)
    {
        $this->userId = $user->getId();
        $this->organizationLogin = $user->getEmail();

        if ($user instanceof NKOUser) {
            $this->name = $user->getNkoName();
            $this->psrn = $user->getPsrn();
        } else {
            $this->name = $user->getFullName();
        }

        $this->setNewRoles($user);

        return $this;
    }

    public function setOldRoles($user)
    {
        $this->userOldRoles = $this->convertRoles($user->getRoles());
    }

    public function setNewRoles($user)
    {
        $this->userNewRoles = $this->convertRoles($user->getRoles());
    }

    public function setNewRolesFromArray($array)
    {
        $this->userNewRoles = $this->convertRoles($array);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOrganizationLogin()
    {
        return $this->organizationLogin;
    }

    public function getPsrn()
    {
        return $this->psrn;
    }

    public function getUserOldRoles()
    {
        return implode(', ', $this->userOldRoles);
    }

    public function getUserNewRoles()
    {
        return implode(', ', $this->userNewRoles);
    }
}
