<?php

namespace NKO\LoggerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\MarkList;

/**
 * @ORM\Entity()
 * @ORM\Table(name="mark_lsit_action_logger")
 */
class MarkListActionLogger extends BaseActionLog
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $markList;

    /**
     * @ORM\Column(type="string")
     */
    protected $competitionName;

    /**
     * @ORM\Column(type="text")
     */
    protected $organizationName;

    /**
     * @ORM\Column(type="string")
     */
    protected $organizationLogin;

    /**
     * @ORM\Column(type="string")
     */
    protected $psrn;

    /**
     * @ORM\Column(type="string")
     */
    protected $expert;

    public function setMarkList(MarkList $markList)
    {
        $this->markList = $markList->getId();
        $this->organizationName = $markList->getApplication()->getAuthor()->getNkoName();
        $this->organizationLogin = $markList->getApplication()->getAuthor()->getEmail();
        $this->psrn = $markList->getApplication()->getAuthor()->getPsrn();
        $this->competitionName = $markList->getApplication()->getCompetition()->getName();
        $this->expert = $markList->getExpert();

        return $this;
    }

    public function getReport()
    {
        return $this->report;
    }

    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    public function getOrganizationLogin()
    {
        return $this->organizationLogin;
    }

    public function getPsrn()
    {
        return $this->psrn;
    }

    public function getExpert()
    {
        return $this->expert;
    }

    public function getCompetitionName()
    {
        return $this->competitionName;
    }
}
