<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/1/18
 * Time: 8:10 PM
 */

namespace NKO\UserBundle\Services;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CurrentUserService
{
    /**
     * @var TokenStorage $tokenStorage
     */
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

}