<?php

namespace NKO\UserBundle\Services;

use NKO\UserBundle\Entity\NKOUser;
use NKO\UserBundle\Entity\User;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class LogoutSuccessHandlerService implements LogoutSuccessHandlerInterface
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function onLogoutSuccess(Request $request)
    {
        $headSite = $this->container->getParameter('custom_site_domain');
        $token = $this->container->get('security.token_storage')->getToken();
        if ($token) {
            $service = $this->container->get('NKO\LoggerBundle\Loggers\BaseLogger');
            $service->logAction('Выход из аккаунта', 'Выход из аккаунта'.$this->getUserIndo($token->getUser()));
        }

        if (!$headSite) {
            return new RedirectResponse($this->container->get('router')->generate('homepage'));
        }

        return new RedirectResponse($headSite);
    }

    private function getUserIndo(User $user)
    {
        if ($user instanceof NKOUser) {
            return " организации с ОГРН ". $user->getPsrn();
        }

        return '';
    }
}
