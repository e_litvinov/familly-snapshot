<?php


namespace NKO\UserBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;

class LoginSuccessHandler extends DefaultAuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $container;

    public function __construct(HttpUtils $httpUtils, Container $container)
    {
        $this->container = $container;
        parent::__construct($httpUtils, []);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $service = $this->container->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Вход в аккаунт', 'Вход в аккаунт');

        return parent::onAuthenticationSuccess($request, $token);
    }
}
