<?php

namespace NKO\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationNKOUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('psrn', null,
                array(
                    'label' => 'PSRN',
                    'attr' => array('maxlength' => 13, 'help'=>'PSRNHelp')
                ))
            ->add('nko_name', null,
                array(
                    'label' => 'NKO_name'
                ))

            ->add('phoneCode', null,
                array(
                    'attr'=>array('maxlength' => 3, 'data' => '+7', 'help' => 'PhoneHelp', "placeholder" => "Код"),
                ))
            ->add('phone', null,
                array(
                    'label' =>false,
                    'attr'=>array('maxlength' => 7, 'placeholder' => "Номер")
                ))
        ;

        $builder->remove('username');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'NKO_user_registration_form';
    }

}