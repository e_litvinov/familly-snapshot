<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 7/17/17
 * Time: 1:27 PM
 */

namespace NKO\UserBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\UserBundle\Entity\OrganizationStatus;

class LoadOrganizationStatus implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $this->createStatus($manager, 'Государственная/муниципальная организация');
        $this->createStatus($manager, 'Негосударственная организация');

        $manager->flush();
    }

    /**
     * @param $manager ObjectManager
     * @param $statusName
     */
    private function createStatus($manager, $statusName)
    {
        if(!$manager->getRepository('NKOUserBundle:OrganizationStatus')->findBy(['status' => $statusName])) {
            $status = new OrganizationStatus();
            $status->setStatus($statusName);

            $manager->persist($status);
        }
    }


}