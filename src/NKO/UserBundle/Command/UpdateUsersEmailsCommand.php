<?php

namespace NKO\UserBundle\Command;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use NKO\UserBundle\Entity\User;
use NKO\UserBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUsersEmailsCommand extends ContainerAwareCommand
{
    const EMPLOYEERS = [
        1260 => [
            'email' => 'romanova@brigantine.su',
            'password' => '6eRYe|fva',
            ],
        1259 => [
            'email' => 'kuteynikova@brigantine.su',
            'password' => 'OeT19kOWL',
            ],
        1241 => [
            'email' => 'andreyeva@brigantine.su',
            'password' => 'kw6c7R|JI',
            ],
        1233 => [
            'email' => 'lyanguzova@brigantine.su',
            'password' => 'ya#c5j%oC',
            ],
        1232 => [
            'email' => 'celebrovskaya@brigantine.su',
            'password' => 'dPhkyo0sn',
        ],
        1231 => [
            'email' => 'goryacheva@brigantine.su',
            'password' => 'KbeNibN*g',
        ],
        1229 => [
            'email' => 'sukhorukova@brigantine.su',
            'password' => 'M045kXgdJ',
        ],
        1227 => [
            'email' => 'trosman@brigantine.su',
            'password' => 'Dg0*H}~Fx',
        ],
        1176 => [
            'email' => 'konstantinov@brigantine.su',
            'password' => '3%D}~FXz2',
        ],
        1035 => [
            'email' => 'guseva@brigantine.su',
            'password' => 'C3BKOgT5q',
        ],
        1023 => [
            'email' => 'kurganova@brigantine.su',
            'password' => 'Ua0y~fRfT',
        ],
        884 => [
            'email' => 'osipova@brigantine.su',
            'password' => 'ORc#bCiGG',
        ],
        883 => [
            'email' => 'shelpakova@brigantine.su',
            'password' => 'XmFLb9Ykr',
        ],
        882 => [
            'email' => 'leonova@brigantine.su',
            'password' => 'iPBbk%yFW',
        ],
        881 => [
            'email' => 'gontarenko@brigantine.su',
            'password' => '#0PX~o5pi',
        ],
        880 => [
            'email' => 'gnutova@brigantine.su',
            'password' => 'ydq9w{dCL',
        ],
        860 => [
            'email' => 'omelchenko@brigantine.su',
            'password' => 'HtK#hHaVJ',
        ],
        678 => [
            'email' => 'golenya@brigantine.su',
            'password' => '|LmhOX%bW',
        ],
        658 => [
            'email' => 'freik@brigantine.su',
            'password' => 'UGfimV@y~',
        ],
        650 => [
            'email' => 'georgi@brigantine.su',
            'password' => 'FBQldxpie',
        ],
        648 => [
            'email' => 'maydan@brigantine.su',
            'password' => 'ZjgKqg55V',
        ],
        632 => [
            'email' => 'archakova@brigantine.su',
            'password' => 'M$LkP?~|}',
        ],
        630 => [
            'email' => 'yevdokimova@brigantine.su',
            'password' => '50fD?}SQs',
        ],
        366 => [
            'email' => 'kirillova@brigantine.su',
            'password' => '9%kvRoCv1',
        ],
        59 => [
            'email' => 'bortsova@brigantine.su',
            'password' => 'jpNS#OfRF',
        ],
        12 => [
            'email' => 'nesterova@brigantine.su',
            'password' => 'yJ%ir6dQi',
        ],
    ];

    const EXPERTS = [
        1177 => [
            'email' => 'e.yatsenko@brigantine.su',
            'password' => 'pmJJS3f#V',
        ],
        1174 => [
            'email' => 'a.konstantinov@brigantine.su',
            'password' => 'y2t?iz0x6',
        ],
        1173 => [
            'email' => 'n.zaytseva@brigantine.su',
            'password' => 'IV6{ldYf%',
        ],
        1172 => [
            'email' => 'n.gusarova@brigantine.su',
            'password' => '?0FWDrsh#',
        ],
        1170 => [
            'email' => 't.shulga@brigantine.su',
            'password' => '~HIXXYNVl',
        ],
        1169 => [
            'email' => 'e.taranchenko@brigantine.su',
            'password' => 'y5ddfEWO*',
        ],
        1074 => [
            'email' => 'o.lim@brigantine.su',
            'password' => 'OSUg20#AV',
        ],
        1073 => [
            'email' => 'e.lyanguzova@brigantine.su',
            'password' => '@XAY7u?FL',
        ],
        947 => [
            'email' => 'a.kuzmin@brigantine.su',
            'password' => 'kC7hCJ4Ny',
        ],
        876 => [
            'email' => 'a.osipova@brigantine.su',
            'password' => 'rQgdn~~gr',
        ],
        875 => [
            'email' => 'i.shelpakova@brigantine.su',
            'password' => 'WbXo*ivW4',
        ],
        852 => [
            'email' => 'k.frank@brigantine.su',
            'password' => '*ry$AOaXY',
        ],
        851 => [
            'email' => 'm.morozova@brigantine.su',
            'password' => 'qnMVE$SEG',
        ],
        836 => [
            'email' => 'm.nesterova@brigantine.su',
            'password' => 'lU|A9edF~',
        ],
        835 => [
            'email' => 'd.magnat@brigantine.su',
            'password' => 'bQ3rW{IlG',
        ],
        834 => [
            'email' => 'i.zinchenko@brigantine.su',
            'password' => 'mzDZ$ikmN',
        ],
        831 => [
            'email' => 'u.gontarenko@brigantine.su',
            'password' => 'vF8CN~p%p',
        ],
        829 => [
            'email' => 'a.telitsyna@brigantine.su',
            'password' => 'Tw~{*B3JW',
        ],
        828 => [
            'email' => 'e.pechurichko@brigantine.su',
            'password' => 'K$z$|@tGR',
        ],
        827 => [
            'email' => 'm.lashkul@brigantine.su',
            'password' => '3R2Y1ZnFv',
        ],
        825 => [
            'email' => 't.archakova@brigantine.su',
            'password' => 'QdvCiiuro',
        ],
        626 => [
            'email' => 'o.yevdokimova@brigantine.su',
            'password' => 'B0kbD4vrE',
        ],
        622 => [
            'email' => 'a.smirnova@brigantine.su',
            'password' => 'isMil|QDv',
        ],
        621 => [
            'email' => 'n.kaminarskaya@brigantine.su',
            'password' => 'DA%oluSQ0',
        ],
        620 => [
            'email' => 't.inozemtseva@brigantine.su',
            'password' => '28~0zlM9?',
        ],
        619 => [
            'email' => 's.rychikhin@brigantine.su',
            'password' => 'JKWLRIL6?',
        ],
        617 => [
            'email' => 't.khizhnyakova@brigantine.su',
            'password' => 'E@O4z7MI{',
        ],
        616 => [
            'email' => 'd.buyanova@brigantine.su',
            'password' => '$yxItf~jN',
        ],
        615 => [
            'email' => 'o.petrova@brigantine.su',
            'password' => 'XMi4et5Ad',
        ],
        614 => [
            'email' => 'n.makarova@brigantine.su',
            'password' => '3R2Y1ZnFv',
        ],
        613 => [
            'email' => 'e.kudakov@brigantine.su',
            'password' => 'QdvCiiuro',
        ],
        612 => [
            'email' => 'o.kosteychuk@brigantine.su',
            'password' => 'B0kbD4vrE',
        ],
        611 => [
            'email' => 'u.maltseva@brigantine.su',
            'password' => 'isMil|QDv',
        ],
        610 => [
            'email' => 'a.lazibnaya@brigantine.su',
            'password' => 'DA%oluSQ0',
        ],
        609 => [
            'email' => 's.dremlyuga@brigantine.su',
            'password' => '28~0zlM9?',
        ],
        608 => [
            'email' => 'i.yefremova_gart@brigantine.su',
            'password' => 'JKWLRIL6?',
        ],
        606 => [
            'email' => 'a.abramova@brigantine.su',
            'password' => 'E@O4z7MI{',
        ],
        605 => [
            'email' => 'v.barova@brigantine.su',
            'password' => '$yxItf~jN',
        ],
        604 => [
            'email' => 'n.panchenko@brigantine.su',
            'password' => 'XMi4et5Ad',
        ],
        603 => [
            'email' => 'e.abrosimova@brigantine.su',
            'password' => '200k5J~2p',
        ],
        602 => [
            'email' => 'o.kislitsyna@brigantine.su',
            'password' => 'hpDAJWs}~',
        ],
        601 => [
            'email' => 'o.drozdova@brigantine.su',
            'password' => 'Vbea0DiNN',
        ],
        600 => [
            'email' => 'o.remenets@brigantine.su',
            'password' => 'nT2TUncSh',
        ],
        599 => [
            'email' => 'e.greshnova@brigantine.su',
            'password' => 'mZX~x%T}e',
        ],
        598 => [
            'email' => 'e.obuhova@brigantine.su',
            'password' => '{YdWnqTou',
        ],
        395 => [
            'email' => 'a.goryacheva@brigantine.su',
            'password' => 'Fs@8gwD0H',
        ],
        394 => [
            'email' => 'i.shpitalskaya@brigantine.su',
            'password' => '|Y9w$}4n~',
        ],
        393 => [
            'email' => 'd.shamrova@brigantine.su',
            'password' => 'EO7JGYYVc',
        ],
        392 => [
            'email' => 'a.tikhomirova@brigantine.su',
            'password' => 'GPjLI%TOp',
        ],
        391 => [
            'email' => 'm.ternovskaya@brigantine.su',
            'password' => '7mHv?~1Lo',
        ],
        390 => [
            'email' => 'i.subbotina@brigantine.su',
            'password' => '8L2dvKG9{',
        ],
        389 => [
            'email' => 'a.popov@brigantine.su',
            'password' => 'cCicsvr{i',
        ],
        388 => [
            'email' => 't.podushkina@brigantine.su',
            'password' => 'xO7oIwIwF',
        ],
        387 => [
            'email' => 'a.omelchenko@brigantine.su',
            'password' => 'r%ZDyfN3K',
        ],
        386 => [
            'email' => 'n.khananashvili@brigantine.su',
            'password' => 'Ll7Epgjn{',
        ],
        385 => [
            'email' => 'e.romanova@brigantine.su',
            'password' => 'j@~v?1Puc',
        ],
        384 => [
            'email' => 't.drobyshevskaya@brigantine.su',
            'password' => '3tputgssV',
        ],
        206 => [
            'email' => 'a.volkova@brigantine.su',
            'password' => 'N@D@*7EQw',
        ],
        205 => [
            'email' => 'o.shirokikh@brigantine.su',
            'password' => 'GEEZtdb~a',
        ],
        204 => [
            'email' => 't.shinina@brigantine.su',
            'password' => 'ADXC$L#D}',
        ],
        203 => [
            'email' => 'o.shatkova@brigantine.su',
            'password' => 'QzNdn$ND1',
        ],
        202 => [
            'email' => 'm.chuprova@brigantine.su',
            'password' => 'Db84*N}ke',
        ],
        201 => [
            'email' => 'n.tyushkevich@brigantine.su',
            'password' => '1j5a~EU6w',
        ],
        200 => [
            'email' => 'm.tembotova@brigantine.su',
            'password' => 'x|dQu1WxC',
        ],
        199 => [
            'email' => 'e.sukhorukova@brigantine.su',
            'password' => 'SvQNAVSp5',
        ],
        198 => [
            'email' => 'm.stepanenko@brigantine.su',
            'password' => 'nLtW#Hg5Q',
        ],
        197 => [
            'email' => 'a.spivak@brigantine.su',
            'password' => 'X1kN5kqF~',
        ],
        196 => [
            'email' => 'n.slabzhanin@brigantine.su',
            'password' => '%R8z5I0gi',
        ],
        195 => [
            'email' => 'g.semya@brigantine.su',
            'password' => '@jayKpELY',
        ],
        194 => [
            'email' => 'l.sandanova@brigantine.su',
            'password' => 'UrdR2dgY|',
        ],
        193 => [
            'email' => 'm.rodyushkina@brigantine.su',
            'password' => 'uAAxbqwYI',
        ],
        192 => [
            'email' => 't.razumovskaya@brigantine.su',
            'password' => 'b0ffCYtx8',
        ],
        191 => [
            'email' => 'v.penzova@brigantine.su',
            'password' => 'EqDtKllDp',
        ],
        190 => [
            'email' => 'y.novozhilova@brigantine.su',
            'password' => '}ul33ZNtp',
        ],
        189 => [
            'email' => 'm.mukhina@brigantine.su',
            'password' => 'HiHJ7tcJu',
        ],
        188 => [
            'email' => 'n.morzhin@brigantine.su',
            'password' => 't3Ws{IFAB',
        ],
        187 => [
            'email' => 'i.merkul@brigantine.su',
            'password' => 'pJL@UCs*S',
        ],
        186 => [
            'email' => 'o.matveyeva@brigantine.su',
            'password' => 'eiD?E2A$b',
        ],
        185 => [
            'email' => 'a.marova@brigantine.su',
            'password' => 'XRjct?Gs4',
        ],
        184 => [
            'email' => 'e.leonova@brigantine.su',
            'password' => 'b*hHFfe?w',
        ],
        183 => [
            'email' => 'm.levina@brigantine.su',
            'password' => 'Tng6S$#eh',
        ],
        182 => [
            'email' => 'l.lazareva@brigantine.su',
            'password' => '3K}R|$96@',
        ],
        181 => [
            'email' => 'g.kurganova@brigantine.su',
            'password' => 'dR*W#doJu',
        ],
        180 => [
            'email' => 'u.kuokkanen@brigantine.su',
            'password' => 'qy5JC?DDC',
        ],
        178 => [
            'email' => 'v.kozharskaya@brigantine.su',
            'password' => 'faqilp1If',
        ],
        177 => [
            'email' => 'v.ivinskiy@brigantine.su',
            'password' => 'xf|3dFO4~',
        ],
        176 => [
            'email' => 'i.golenya@brigantine.su',
            'password' => '8~~Q#ziL1',
        ],
        175 => [
            'email' => 'i.galasyuk@brigantine.su',
            'password' => '1{h$Lp{8I',
        ],
        174 => [
            'email' => 'a.vikulov@brigantine.su',
            'password' => 'H#G|%Vqb5',
        ],
        173 => [
            'email' => 'm.bunyak@brigantine.su',
            'password' => 'EWWzdsv$p',
        ],
        172 => [
            'email' => 'a.borovykh@brigantine.su',
            'password' => 'giDHqK~ei',
        ],
        171 => [
            'email' => 's.borzov@brigantine.su',
            'password' => 'R2NL*Pxjn',
        ],
        170 => [
            'email' => 'e.beregovaya@brigantine.su',
            'password' => 'G?~sw8f||',
        ],
        169 => [
            'email' => 'g.bardiyer@brigantine.su',
            'password' => 'H9vkn9oIV',
        ],
        168 => [
            'email' => 'e.astrakhantseva@brigantine.su',
            'password' => 'RKNRfMKB6',
        ],
        167 => [
            'email' => 'e.alshanskaya@brigantine.su',
            'password' => 'qRMGXS~IM',
        ],
        57 => [
            'email' => 'n.freik@brigantine.su',
            'password' => 'wMlmfYxXf',
        ],
        56 => [
            'email' => 'l.sulima@brigantine.su',
            'password' => 'VoE~C{$ST',
        ],
        55 => [
            'email' => 'e.ozerova@brigantine.su',
            'password' => 'ubm@KrLXe',
        ],
        54 => [
            'email' => 'e.makushina@brigantine.su',
            'password' => 'nd{1iwHcf',
        ],
        53 => [
            'email' => 'n.kosheleva@brigantine.su',
            'password' => '}~@wA%~o$',
        ],
        52 => [
            'email' => 'o.kazakov@brigantine.su',
            'password' => 'EmqIraigD',
        ],
        51 => [
            'email' => 'u.ivashkina@brigantine.su',
            'password' => 'nEBnoz45x',
        ],
        49 => [
            'email' => 'i.georgi@brigantine.su',
            'password' => 'KmcVNI~*C',
        ],
        ];


    const LIM_EMPLOYEE = [
        1230 => [
            'email' => 'lim@brigantine.su',
            'password' => '65^5w?MYx',
        ],
    ];

    protected function configure()
    {
        $this
            ->setName('nko:rename_users');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /** @var UserRepository $repository */
        $repository = $em->getRepository(User::class);
        /** @var UserManagerInterface $um */
        $um = $this->getContainer()->get('fos_user.user_manager');

        /** @var User $user */

        /*foreach (self::EMPLOYEERS as $id => $newFields) {
            $output->writeln($id);
            $user = $repository->find($id);

            $user->setEmail($newFields['email']);
            $user->setEmailCanonical($newFields['email']);
            $user->setPassword($newFields['password']);
            $user->setPlainPassword($newFields['password']);

            $um->updatePassword($user);
        }

        foreach (self::EXPERTS as $id => $newFields) {
            $output->writeln($id);
            $user = $repository->find($id);

            $user->setEmail($newFields['email']);
            $user->setEmailCanonical($newFields['email']);
            $user->setPassword($newFields['password']);
            $user->setPlainPassword($newFields['password']);

            $um->updatePassword($user);
        }*/

        foreach (self::LIM_EMPLOYEE as $id => $newFields) {
            $output->writeln($id);
            $user = $repository->find($id);

            $user->setEmail($newFields['email']);
            $user->setEmailCanonical($newFields['email']);
            $user->setPassword($newFields['password']);
            $user->setPlainPassword($newFields['password']);

            $um->updatePassword($user);
        }
        $em->flush();
        $output->writeln('ok');
    }
}
