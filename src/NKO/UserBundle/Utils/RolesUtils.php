<?php

namespace NKO\UserBundle\Utils;

class RolesUtils
{
    const EMPLOYEE = 'Представитель фонда';
    const MODERATOR = 'Редактор сайта';
    const FOUNDER = 'Учредитель';
    const EMPLOYEE_ADMIN = 'Администратор';
    const SUPER_ADMIN = 'Супер администратор';
    const MAIN_EXPERT = 'Ведущий эксперт';
    const EXPERT = 'Эксперт';

    const USER = 'Организация';

    const ROLES = [
        'ROLE_EMPLOYEE' => self::EMPLOYEE,
        'ROLE_MODERATOR' => self::MODERATOR,
        'ROLE_FOUNDER' => self::FOUNDER,
        'ROLE_EMPLOYEE_ADMIN' => self::EMPLOYEE_ADMIN,
        'ROLE_SUPER_ADMIN' => self::SUPER_ADMIN,
        'ROLE_MAIN_EXPERT' => self::MAIN_EXPERT,
        'ROLE_EXPERT' => self::EXPERT,
        'ROLE_USER' => self::USER,
        'ROLE_NKO' => self::USER,
        'ROLE_SONATA_ADMIN' => self::USER,
    ];
}
