<?php

namespace NKO\UserBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;

abstract class BaseUserAdmin extends AbstractAdmin
{
    public function preRemove($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\UserLogger');
        $service->setIsNewRoleNeed(false);

        $service->logFullAction('Удаление пользователя', $object);
    }

    public function create($object)
    {
        $subject = parent::create($object);

        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\UserLogger');
        $service->logFullAction('Создание пользователя', $subject);

        return $subject;
    }
}
