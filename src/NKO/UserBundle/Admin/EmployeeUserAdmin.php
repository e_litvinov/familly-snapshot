<?php
namespace NKO\UserBundle\Admin;

use FOS\UserBundle\Model\UserInterface;
use NKO\UserBundle\Entity\EmployeeUser;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Mailer as Mailer;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use NKO\UserBundle\Utils\RolesUtils;

class EmployeeUserAdmin extends BaseUserAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
        '_sort_by' => 'createdAt'

    );

    private function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $isCurrentSuperAdmin = in_array('ROLE_SUPER_ADMIN', $this->getCurrentUser()->getRoles());
        $roles = [
            RolesUtils::EMPLOYEE_ADMIN => 'ROLE_EMPLOYEE_ADMIN',
            RolesUtils::EMPLOYEE => 'ROLE_EMPLOYEE',
            RolesUtils::MODERATOR => 'ROLE_MODERATOR',
            RolesUtils::FOUNDER => 'ROLE_FOUNDER'
        ];
        if ($this->getSubject() &&
            in_array('ROLE_SUPER_ADMIN', $this->getSubject()->getRoles()) &&
            $isCurrentSuperAdmin
        ) {
            $roles[RolesUtils::SUPER_ADMIN] = 'ROLE_SUPER_ADMIN';
        }

        $formMapper
            ->add('surname', 'text', array('label' => 'Surname'))
            ->add('name', 'text', array('label' => 'Name'))
            ->add('patronymic', 'text', array('label' => 'Patronymic'))
            ->add('username', 'text', array('label' => 'Username'))
            ->add('email', 'email', array('label' => 'Email'));
        if ($isCurrentSuperAdmin) {
            $formMapper->add('roles', 'choice', array(
                'choices' => $roles, 'multiple' => true));
        }

        //TODO
        /*
        if(in_array('ROLE_MODERATOR', $this->getCurrentUser()->getRoles())) {
            $roles = [
                RolesUtils::EMPLOYEE => 'ROLE_EMPLOYEE',
                RolesUtils::MODERATOR => 'ROLE_MODERATOR',
                RolesUtils::FOUNDER => 'ROLE_FOUNDER'
            ];
            $formMapper->add('roles', 'choice', array(
                'choices' => $roles, 'multiple' => true));
        }*/
        //Запрещаем менять пароль супер админитратору и пароль не себе(суперадмин может менять всем кроме себя)
        if (!$this->getSubject() ||
            ($isCurrentSuperAdmin && !in_array('ROLE_SUPER_ADMIN', $this->getSubject()->getRoles())) ||
            (!$isCurrentSuperAdmin && ($this->getCurrentUser()->getId() ==  $this->getSubject()->getId()))
        ) {
            $formMapper
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch'))
                ->add('enabled', 'checkbox', array('required' => false));
        }
        if ($this->isCurrentRoute('edit')) {
            if (!$this->getSubject() ||
                ($isCurrentSuperAdmin && !in_array('ROLE_SUPER_ADMIN', $this->getSubject()->getRoles())) ||
                (!$isCurrentSuperAdmin && ($this->getCurrentUser()->getId() ==  $this->getSubject()->getId()))
            ) {
                $formMapper
                    ->add('plainPassword', 'repeated', array(
                        'required' => false,
                        'type' => 'password',
                        'options' => array('translation_domain' => 'FOSUserBundle'),
                        'first_options' => array('label' => 'form.password'),
                        'second_options' => array('label' => 'form.password_confirmation'),
                        'invalid_message' => 'fos_user.password.mismatch'));
            }

            if (in_array('ROLE_EMPLOYEE_ADMIN', $this->getCurrentUser()->getRoles()) ||
                $isCurrentSuperAdmin
            ) {
                $formMapper
                    ->add('surname', 'text', array('attr' => array(
                        'readonly' => true)))
                    ->add('name', 'text', array('attr' => array(
                        'readonly' => true)))
                    ->add('patronymic', 'text', array('attr' => array(
                        'readonly' => true)))
                    ->add('username', 'text', array('attr' => array(
                        'readonly' => !$isCurrentSuperAdmin)))
                    ->add('email', 'email', array('attr' => array(
                        'readonly' => true)));
            }
        }
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('name')
            ->add('surname')
            ->add('patronymic')
            ->add('roles', 'doctrine_orm_string', [], 'choice', [
                'choices' => [
                    RolesUtils::EMPLOYEE => 'ROLE_EMPLOYEE',
                    RolesUtils::SUPER_ADMIN => 'ROLE_SUPER_ADMIN',
                    RolesUtils::MODERATOR => 'ROLE_MODERATOR',
                    RolesUtils::MAIN_EXPERT => 'ROLE_MAIN_EXPERT',
                    RolesUtils::FOUNDER => 'ROLE_FOUNDER',
                    RolesUtils::EXPERT => 'ROLE_EXPERT',
                    RolesUtils::EMPLOYEE_ADMIN => 'ROLE_EMPLOYEE_ADMIN',
                ]
            ])
            ;
    }
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('surname')
            ->add('name')
            ->add('patronymic');

        if (in_array('ROLE_SUPER_ADMIN', $this->getCurrentUser()->getRoles())) {
            $listMapper
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )));
        } else {
            $listMapper
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                    )));
        }
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username')
            ->add('surname')
            ->add('name')
            ->add('patronymic')
            ->add('email')
        ;
    }

    public function preUpdate($object)
    {
        $um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $um->updateCanonicalFields($object);
        $um->updatePassword($object);
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        if (in_array('ROLE_SUPER_ADMIN', $this->getCurrentUser()->getRoles())) {
            return $query;
        } elseif (in_array('ROLE_MODERATOR', $this->getCurrentUser()->getRoles())) {
            $query
                ->andWhere($query->getRootAlias() . '.roles not like \'%_ADMIN%\'');
        } elseif (in_array('ROLE_EMPLOYEE_ADMIN', $this->getCurrentUser()->getRoles())) {
            $query
                ->andWhere($query->getRootAlias() . '.roles  like \'%ROLE_EMPLOYEE%\'');
        } elseif (in_array('ROLE_EMPLOYEE', $this->getCurrentUser()->getRoles())) {
            $query
                ->andWhere($query->getRootAlias() . '.username LIKE :user')
                ->setParameter('user', $this->getCurrentUser()->getUsername());
        } elseif (in_array('ROLE_FOUNDER', $this->getCurrentUser()->getRoles())) {
            $query
                ->andWhere($query->getRootAlias() . '.username LIKE :user')
                ->setParameter('user', $this->getCurrentUser()->getUsername());
        }

        return $query;
    }
}
