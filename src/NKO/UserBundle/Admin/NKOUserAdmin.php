<?php

namespace NKO\UserBundle\Admin;

use FOS\UserBundle\Model\UserInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class NKOUserAdmin extends BaseUserAdmin
{

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
       '_sort_by' => 'createdAt',
    );

    private function getCurrentUser()
    {
        /**
         * @var UserInterface $user
         */
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $isCurrentSuperAdmin = in_array('ROLE_SUPER_ADMIN', $this->getCurrentUser()->getRoles());
        $formMapper
            ->add('psrn', 'text', array('label' => 'PSRN'))
            ->add('nko_name', 'text', array('label' => 'nko Name'))
            ->add('phoneCode', 'text', array('label' => 'Phone Code'))
            ->add('phone', 'text', array('label' => 'Phone'))
            ->add('email', 'email', array('label' => 'Email'));
        if (!$this->getSubject() ||
            $isCurrentSuperAdmin ||
            (!$isCurrentSuperAdmin && ($this->getCurrentUser()->getId() ==  $this->getSubject()->getId()))
        ) {
            $formMapper
                ->add('plainPassword', 'repeated', array(
                    'required' => false,
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch' ));
        }

        if (in_array('ROLE_EMPLOYEE_ADMIN', $this->getCurrentUser()->getRoles())) {
            $formMapper
                ->add('psrn', 'text', array('attr' => array(
                    'readonly' => true)))
                ->add('nko_name', 'text', array('attr' => array(
                    'readonly' => true)))
                ;
        }
        if ($this->isCurrentRoute('edit')) {
            $formMapper
                ->add('email', 'email', array('attr' => ['readonly' => !$isCurrentSuperAdmin]));
        }

        if (in_array('ROLE_EMPLOYEE_ADMIN', $this->getCurrentUser()->getRoles())
                    || $isCurrentSuperAdmin) {
            $formMapper
                ->add('enabled', 'checkbox', array('required' => false))
                ->add('status', 'sonata_type_model', [
                    'expanded' => true,
                    'required' => false,
                    'btn_add' => false]);
        }

        if (in_array('ROLE_NKO', $this->getCurrentUser()->getRoles())) {
            $formMapper
                ->add('psrn', 'text', array('attr' => array(
                    'readonly' => true)))
                ->add('email', 'email', array('attr' => array(
                    'readonly' => true)));
        }

        $currentRoles = $this->getCurrentUser()->getRoles();
        if (in_array('ROLE_NKO', $currentRoles) || in_array('ROLE_EMPLOYEE', $currentRoles)) {
            $formMapper
                ->add('status', 'sonata_type_model', [
                    'expanded' => true,
                    'required' => false,
                    'btn_add' => false,
                ]);
        }
    }


    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('psrn')
            ->add('nko_name')
            ->add('phone')
            ->add('email')
            ->add('status')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('psrn')
            ->add('nko_name')
            ->add('email')
            ->add('status')
            ;
        if (in_array('ROLE_EMPLOYEE_ADMIN', $this->getCurrentUser()->getRoles())
            ||in_array('ROLE_SUPER_ADMIN', $this->getCurrentUser()->getRoles())) {
            $listMapper
                ->add('isAcceptAgreement');
        }
        $listMapper
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )))
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('psrn')
            ->add('nko_name')
            ->add('phone')
            ->add('email')
            ->add('phoneCode')
            ->add('isAcceptAgreement')
        ;
    }

    public function preUpdate($object)
    {
        $um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $um->updateCanonicalFields($object);
        $um->updatePassword($object);
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if (in_array('ROLE_NKO', $this->getCurrentUser()->getRoles())) {
            $query
                ->andWhere($query->getRootAlias() . '.username LIKE :user')
                ->setParameter('user', $this->getCurrentUser()->getUsername());
        }

        return $query;
    }
}
