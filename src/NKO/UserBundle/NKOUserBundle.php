<?php

namespace NKO\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NKOUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
