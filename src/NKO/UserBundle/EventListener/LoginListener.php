<?php

namespace NKO\UserBundle\EventListener;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class LoginListener
{
    protected $authorizationChecker;

    /**
     * Список IP для админимистраторов
     *
     * 46.39.250.105 - IP Тёти Лены
     * 37.45.34.135 & 172.18.0.1 - IP's navasiolau(Новосёлов Никита)
     *
     * @var string[]
     */
    protected $allowed_admin_ips = [
        '46.39.250.105',
        '37.45.34.135',
        '172.18.0.1'
    ];

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $baseUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        /**
         * Проверка на соотвествие запрашиваемого адреса базовому url, чтобы исключить ошибку 400
         * которая возникает в случае отсутствия проверки.
         */
        if ($baseUrl != $request->getUri()) {
            /**
             * Проверка на наличие роли супер администратора и разрешенного ip адреса для админа
             * Проверка на IP необходима в связи с активностью злоумышленников
             *
             * В случае несовпадения одного из параметров пользователь получает 400 ошибку с сообщением
             * Если пользователь не принадлежит к группе супер админов - его не косаются эти проверки
             */
            if ($this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN') && !in_array($request->getClientIp(), $this->allowed_admin_ips)) {
                $message = 'Sorry, but it\'s incorrect route';

                $event->setController(
                    function () use ($message) {
                        return new Response($message, 400);
                    }
                );
            }
        }
    }
}
