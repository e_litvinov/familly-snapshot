<?php

namespace NKO\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="NKO\UserBundle\Repository\NKOUserRepository")
 * @ORM\Table(name="NKO_user")
 * @UniqueEntity(fields = "email", targetClass = "NKO\UserBundle\Entity\User", message="fos_user.email.already_used")
 * @UniqueEntity(fields = "psrn", targetClass = "NKO\UserBundle\Entity\NKOUser")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class NKOUser extends User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     *
     * @Assert\Regex(
     *     pattern="/^\d{13}$/")
     *
     * @Assert\NotBlank()
     *
     */

    protected $psrn;

    /**
     * @ORM\Column(type="string", length=500)
     *
     * @Assert\NotBlank()
     *
     */
    protected $nko_name;

    /**
     * @var string
     *
     */
    private $internationalCode;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "phone code must be at least 3 characters long",
     * )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     )
     *
     * @ORM\Column(name="phone_code", type="string", length=255, nullable=true)
     */
    private $phoneCode;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message="value is invalid(field must be non empty)",
     *     )
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     message="value is invalid(field must contain only numbers),",
     *     )
     * @Assert\Length(
     *      min = 7,
     *      minMessage = "Номер должен содержать 7 цифр",
     * )
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition", inversedBy="organizations")
     * @ORM\JoinTable(name="competition_organization")
     */
    private $competitions;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\BaseReport", mappedBy="author")
     */
    protected $reports;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\Report\BaseReportTemplate", mappedBy="author")
     */
    protected $reportTemplates;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="NKO\UserBundle\Entity\OrganizationStatus", inversedBy="organizations", cascade={"all"})
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true, onDelete="cascade")
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", mappedBy="author")
     */
    protected $applicationHistories;

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return NKOUser
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    public function setEmail($email)
    {
        $this->setUsername($email);
        return parent::setEmail($email);
    }

    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_NKO', "ROLE_SONATA_ADMIN");
        $this->competitions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->applicationHistories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set psrn
     *
     * @param string $psrn
     *
     * @return NKOUser
     */
    public function setPsrn($psrn)
    {
        $this->psrn = $psrn;

        return $this;
    }

    /**
     * Get psrn
     *
     * @return string
     */
    public function getPsrn()
    {
        return $this->psrn;
    }

    /**
     * Set nkoName
     *
     * @param string $nkoName
     *
     * @return NKOUser
     */
    public function setNkoName($nkoName)
    {
        $this->nko_name = $nkoName;

        return $this;
    }

    /**
     * Get nkoName
     *
     * @return string
     */
    public function getNkoName()
    {
        return (string)$this->nko_name;
    }

    /**
     * Set phoneCode
     *
     * @param string $phoneCode
     *
     * @return NKOUser
     */
    public function setPhoneCode($phoneCode)
    {
        $this->phoneCode = $phoneCode;

        return $this;
    }

    /**
     * Get phoneCode
     *
     * @return string
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * Add competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return NKOUser
     */
    public function addCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions[] = $competition;

        return $this;
    }

    /**
     * Remove competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     */
    public function removeCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions->removeElement($competition);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * Add report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     *
     * @return NKOUser
     */
    public function addReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param \NKO\OrderBundle\Entity\BaseReport $report
     */
    public function removeReport(\NKO\OrderBundle\Entity\BaseReport $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    public function __toString()
    {
        return $this->getNkoName();
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return NKOUser
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     *
     * @return NKOUser
     */
    public function addReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplates[] = $reportTemplate;

        return $this;
    }

    /**
     * Remove reportTemplate
     *
     * @param \NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate
     */
    public function removeReportTemplate(\NKO\OrderBundle\Entity\Report\BaseReportTemplate $reportTemplate)
    {
        $this->reportTemplates->removeElement($reportTemplate);
    }

    /**
     * Get reportTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportTemplates()
    {
        return $this->reportTemplates;
    }

    /**
     * Set status
     *
     * @param \NKO\UserBundle\Entity\OrganizationStatus $status
     *
     * @return NKOUser
     */
    public function setStatus(\NKO\UserBundle\Entity\OrganizationStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \NKO\UserBundle\Entity\OrganizationStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     *
     * @return NKOUser
     */
    public function addApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory)
    {
        $this->applicationHistories[] = $applicationHistory;
        $applicationHistory->setAuthor($this);

        return $this;
    }

    /**
     * Remove applicationHistory
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory
     */
    public function removeApplicationHistory(\NKO\OrderBundle\Entity\ApplicationHistory $applicationHistory)
    {
        $this->applicationHistories->removeElement($applicationHistory);
    }

    /**
     * Get applicationHistories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicationHistories()
    {
        return $this->applicationHistories;
    }
}
