<?php

namespace NKO\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Mark;
use NKO\OrderBundle\Entity\MarkList;
use PUGX\MultiUserBundle\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="NKO\UserBundle\Repository\ExpertUserRepository")
 * @ORM\Table(name="expert_user")
 * @UniqueEntity(fields = "username", targetClass = "NKO\UserBundle\Entity\User", message="fos_user.username.already_used")
 * @UniqueEntity(fields = "email", targetClass = "NKO\UserBundle\Entity\User", message="fos_user.email.already_used")
 */
class ExpertUser extends User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $patronymic;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\ApplicationHistory", mappedBy="experts")
     * @ORM\JoinColumn(name="applications", nullable=true)
     */
    private $applications;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\MarkList", mappedBy="expert")
     */
    private $markLists;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Competition", inversedBy="experts")
     * @ORM\OrderBy({"finish_date" = "DESC"})
     * @ORM\JoinTable(name="competition_expert")
     */
    private $competitions;

    /**
     * @ORM\ManyToMany(targetEntity="NKO\OrderBundle\Entity\Report\ReportForm", inversedBy="experts")
     * @ORM\JoinTable(name="report_forms_experts")
     */
    private $reportForms;

    private $status;

    /**
     * @ORM\OneToMany(targetEntity="NKO\OrderBundle\Entity\MarkListHistory", mappedBy="expert")
     */
    private $markListHistories;


    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_EXPERT', 'ROLE_SONATA_ADMIN');
        $this->enabled = true;
        $this->isAcceptAgreement = true;
        $this->markListHistories = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ExpertUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getFullName()
    {
        return $this->surname . " ". $this->name . " " . $this->patronymic;
    }

    public function __toString()
    {
        return $this->surname . " " . $this->name . " " .$this->patronymic;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return ExpertUser
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set patronymic
     *
     * @param string $patronymic
     *
     * @return ExpertUser
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * Get patronymic
     *
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * Add application
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $application
     *
     * @return ExpertUser
     */
    public function addApplication(\NKO\OrderBundle\Entity\ApplicationHistory $application)
    {
        $this->applications[] = $application;

        return $this;
    }

    /**
     * Remove application
     *
     * @param \NKO\OrderBundle\Entity\ApplicationHistory $application
     */
    public function removeApplication(\NKO\OrderBundle\Entity\ApplicationHistory $application)
    {
        $this->applications->removeElement($application);
    }

    /**
     * Get applications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Add markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     *
     * @return ExpertUser
     */
    public function addMarkList(\NKO\OrderBundle\Entity\MarkList $markList)
    {
        $this->markLists[] = $markList;
        $markList->setExpert($this);

        return $this;
    }

    /**
     * Remove markList
     *
     * @param \NKO\OrderBundle\Entity\MarkList $markList
     */
    public function removeMarkList(\NKO\OrderBundle\Entity\MarkList $markList)
    {
        $this->markLists->removeElement($markList);
    }

    /**
     * Get markLists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarkLists()
    {
        return $this->markLists;
    }

    /**
     * Add competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     *
     * @return ExpertUser
     */
    public function addCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions[] = $competition;

        return $this;
    }

    /**
     * Remove competition
     *
     * @param \NKO\OrderBundle\Entity\Competition $competition
     */
    public function removeCompetition(\NKO\OrderBundle\Entity\Competition $competition)
    {
        $this->competitions->removeElement($competition);
    }

    /**
     * Get competitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * @param $competition
     * @return $this
     */
    public function setStatus($competition)
    {
        $countValidMarkLists = 0;
        $countMarkList = 0;

        if ($this->getMarkLists()) {
            /**
             * @var ApplicationHistory $application
             */
            foreach ($this->getApplications() as $application) {
                if ($application->getCompetition() == $competition) {
                    $countMarkList ++;
                    /**
                     * @var MarkList $markList
                     */
                    foreach ($this->getMarkLists() as $markList) {
                        if ($markList->getApplication() == $application && $markList->getIsSend()) {
                            $countValidMarkLists ++;
                        }
                    }
                }
            }

            if ($countValidMarkLists && $countValidMarkLists == $countMarkList) {
                $this->status = 'allValidMarkLists';

                return $this;
            } elseif ($countValidMarkLists > 0) {
                $this->status = 'oneValidMarkList';

                return $this;
            }
        }

        if ($competition->getFinishDate() && $this->lastLogin > $competition->getFinishDate()
            && $competition->getFinishEstimate() && $this->lastLogin < $competition->getFinishEstimate()) {
                $this->status = 'lastLogin';
        }

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCountSentMark($competition = null)
    {
        $count = 0;

        if ($competition) {
            /**
             * @var MarkList $markList
             */
            foreach ($this->getMarkLists() as $markList) {
                if ($markList->getIsValid()
                    && $markList->getApplication()->getCompetition() == $competition) {
                    $count ++;
                }
            }
        } else {
            /**
             * @var MarkList $markList
             */
            foreach ($this->getMarkLists() as $markList) {
                if ($markList->getIsValid()) {
                    $count ++;
                }
            }
        }

        return $count;
    }

    public function getApplicationsCount($competition = null)
    {
        $count = 0;
        if ($competition) {
            foreach ($this->applications as $application) {
                if ($application->getCompetition() == $competition) {
                    $count ++;
                }
            }
        } else {
            $count = count($this->applications);
        }

        return $count;
    }

    /**
     * Add reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     *
     * @return ExpertUser
     */
    public function addReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms[] = $reportForm;

        return $this;
    }

    /**
     * Remove reportForm
     *
     * @param \NKO\OrderBundle\Entity\Report\ReportForm $reportForm
     */
    public function removeReportForm(\NKO\OrderBundle\Entity\Report\ReportForm $reportForm)
    {
        $this->reportForms->removeElement($reportForm);
    }

    /**
     * Get reportForms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportForms()
    {
        return $this->reportForms;
    }

    /**
     * Add markListHistory
     *
     * @param \NKO\OrderBundle\Entity\MarkListHistory $markListHistory
     *
     * @return ExpertUser
     */
    public function addMarkListHistory(\NKO\OrderBundle\Entity\MarkListHistory $markListHistory)
    {
        $this->markListHistories[] = $markListHistory;
        $markListHistory->setExpert($this);

        return $this;
    }

    /**
     * Remove markListHistory
     *
     * @param \NKO\OrderBundle\Entity\MarkListHistory $markListHistory
     */
    public function removeMarkListHistory(\NKO\OrderBundle\Entity\MarkListHistory $markListHistory)
    {
        $this->markListHistories->removeElement($markListHistory);
    }

    /**
     * Get markListHistories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarkListHistories()
    {
        return $this->markListHistories;
    }
}
