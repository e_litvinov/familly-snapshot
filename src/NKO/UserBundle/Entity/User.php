<?php
namespace NKO\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="NKO\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"NKO_user" = "NKOUser", "employee_user" = "EmployeeUser", "expert_user" = "ExpertUser"})
 */
abstract class User extends BaseUser
{
    use ORMBehaviors\Timestampable\Timestampable;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isAcceptAgreement;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set isAcceptAgreement
     *
     * @param boolean $isAcceptAgreement
     *
     * @return User
     */
    public function setIsAcceptAgreement($isAcceptAgreement)
    {
        $this->isAcceptAgreement = $isAcceptAgreement;

        return $this;
    }

    /**
     * Get isAcceptAgreement
     *
     * @return boolean
     */
    public function getIsAcceptAgreement()
    {
        return $this->isAcceptAgreement;
    }

    public function getName()
    {
        if ($this instanceof NKOUser) {
            return $this->getNkoName();
        } elseif ($this instanceof ExpertUser) {
            return $this->getFullName();
        } elseif ($this instanceof EmployeeUser) {
            return $this->getFullName();
        }

        return 'Without name';
    }

    public function getFullName()
    {
        if ($this instanceof NKOUser) {
            return $this->getNkoName();
        }

        return 'Without name';
    }
}
