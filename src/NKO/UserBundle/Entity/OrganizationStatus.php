<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 7/17/17
 * Time: 1:18 PM
 */

namespace NKO\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\Table(name="organization_status")
 */
class OrganizationStatus
{
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $status;


    /**
     * @ORM\OneToMany(targetEntity="NKO\UserBundle\Entity\NKOUser", mappedBy="status")
     */
    protected $organizations;

    public function __toString()
    {
        return (string) $this->status;
    }

    public function getSluggableFields()
    {
        return [ 'status' ];
    }

    private function getRegenerateSlugOnUpdate()
    {
        return false;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return OrganizationStatus
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
