<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 12/8/17
 * Time: 1:02 PM
 */

namespace NKO\UserBundle\Traits;

trait CurrentUserTrait
{
    public function getCurrentUser()
    {
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $user;
    }
}