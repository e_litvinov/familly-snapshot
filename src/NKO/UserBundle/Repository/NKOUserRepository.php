<?php

namespace NKO\UserBundle\Repository;

use NKO\OrderBundle\Entity\BaseReport;
use NKO\OrderBundle\Entity\ReportHistory;
use NKO\UserBundle\Entity\NKOUser;

/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/8/17
 * Time: 12:17 AM
 */
class NKOUserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllPsrns()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT u.psrn FROM NKOUserBundle:NKOUser u')
            ->getResult();
    }

    public function findWinnersOfCompetitions($competitions, $class)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('u.id, u.nko_name as name, u.psrn')
            ->from(NKOUser::class, 'u')
            ->leftJoin('u.reports', 'r')
            ->leftJoin('r.reportForm', 'rf')
            ->leftJoin('rf.competition', 'c')
            ->join(ReportHistory::class, 'rh', 'WITH', 'rh.psrn = u.psrn and rh.reportForm = rf.id')
            ->where('rh.isSend = true')
            ->andWhere('rf.reportClass = :class')
            ->andWhere('c.applicationClass in (:competitions)')
            ->groupBy('u.id')
            ->setParameters([
                'competitions' => $competitions,
                'class' => $class
            ])
            ->getQuery()->getResult();
    }

    public function findWinnersOfCompetitionsByPSRN($psrn)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('u.id, u.nko_name as name, u.psrn')
            ->from(NKOUser::class, 'u')
            ->andWhere('u.psrn = :psrn')
            ->setParameters([
                'psrn' => $psrn,
            ])
            ->getQuery()->getResult();
    }
}
