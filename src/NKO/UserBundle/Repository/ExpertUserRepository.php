<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.7.18
 * Time: 17.30
 */

namespace NKO\UserBundle\Repository;

use NKO\UserBundle\Entity\ExpertUser;

class ExpertUserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findExpertsByCompetition($competition)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from(ExpertUser::class, 'e')
            ->where (':competition MEMBER OF e.competitions')
            ->setParameters(array('competition' => $competition))
            ->getQuery()
            ->getResult();
    }
}