<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 2/8/17
 * Time: 1:27 AM
 */

namespace NKO\UserBundle\Repository;


class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllEmails()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT u.email FROM NKOUserBundle:User u')
            ->getResult();
    }
}