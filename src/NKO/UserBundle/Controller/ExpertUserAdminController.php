<?php


namespace NKO\UserBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class ExpertUserAdminController extends BaseUserAdminController
{
    public function listAction()
    {
        if (get_class($this->getUser()) == 'NKO\UserBundle\Entity\ExpertUser') {
            return new RedirectResponse($this->admin->generateUrl(
                'edit',
                array('filter' => $this->admin->getFilterParameters(),
                    'id' => $this->getUser()->getId())
            ));
        }
        else
        {
            $request = $this->getRequest();

            $this->admin->checkAccess('list');

            $preResponse = $this->preList($request);
            if ($preResponse !== null) {
                return $preResponse;
            }

            if ($listMode = $request->get('_list_mode')) {
                $this->admin->setListMode($listMode);
            }

            $datagrid = $this->admin->getDatagrid();
            $formView = $datagrid->getForm()->createView();

            // set the theme for the current Admin Form
            $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

            return $this->render($this->admin->getTemplate('list'), array(
                'action' => 'list',
                'form' => $formView,
                'datagrid' => $datagrid,
                'csrf_token' => $this->getCsrfToken('sonata.batch'),
            ), null, $request);
        }
    }
}