<?php

namespace NKO\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegistrationUserController extends Controller
{
    public function registerAction(Request $request)
    {
        $form = $request->get('NKO_user_registration_form');
        $form['email'] = idn_to_ascii($form['email']);
        $request->request->set('NKO_user_registration_form', $form);

        return $this
            ->get('pugx_multi_user.registration_manager')
            ->register('NKO\UserBundle\Entity\NKOUser');
    }
}