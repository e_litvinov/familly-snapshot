<?php

namespace NKO\MenuBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Knp\DoctrineBehaviors\ORM as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;

class MenuRepository extends EntityRepository
{
    use ORMBehaviors\Tree\Tree;

    public function getRootLevelNodes()
    {
        $qb = $this->createQueryBuilder('t');

        return $qb
            ->where($qb->expr()->eq('t.materializedPath', '?1'))
            ->orderBy('t.sequence', 'ASC')
            ->setParameter(1, '');
    }

    public function getChildrenByParentID($parentId)
    {
        $parentPath = $this->find($parentId)->getRealMaterializedPath();

        return $this->getTree($parentPath)->getChildNodes();

    }



}