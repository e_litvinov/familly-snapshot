<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/1/17
 * Time: 5:51 PM
 */

namespace NKO\MenuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;

class MenuController extends Controller
{
    public function createMenuAction()
    {
        //проверка внесения изменений в код на продакшн сервере
        $process = new Process('git diff');
        $process->run();
        $isChangedCode = $process->getOutput();

        $em = $this->getDoctrine()->getManager();
        $menuRoots = $em->getRepository("NKOMenuBundle:Menu")
            ->getRootLevelNodes()
            ->getQuery()
            ->getSingleResult();

        $em->getRepository("NKOMenuBundle:Menu")
                ->getChildrenByParentID($menuRoots->getId());

        return $this->render('NKODefaultBundle:layout:main_menu.html.twig', array(
            'menuItems' => $menuRoots->getChildNodes(), 'isChangedCode' => $isChangedCode ));
    }

    /**
     *
     * @Route("/menu/{id}", name="show_menu_items")
     * @Method("GET")
     */
    public function showMenuItemAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $menuRoot = $em->getRepository("NKOMenuBundle:Menu")->find($request->get('id'));
        $em->getRepository("NKOMenuBundle:Menu")
            ->getChildrenByParentID($menuRoot->getId());
        $childNodes = $menuRoot->getChildNodes();

        return $this->render('NKOMenuBundle:Menu:show_menu_items.html.twig', array(
            'childNodes' => $childNodes));
    }

}