<?php

namespace NKO\MenuBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use NKO\MenuBundle\Entity\Menu;
use NKO\OrderBundle\Entity\Costs;
use NKO\OrderBundle\Form\CostType;
use NKO\UserBundle\Entity\NKOUser;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use NKO\OrderBundle\Entity\ReportHistory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Doctrine\ORM\Cache\LockException;
use Knp\DoctrineBehaviors\Model\Tree\Node;


class MenuAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function listAction(Request $request = null)
    {
        if ($request->get('filter') || $request->get('filters')) {
            return new RedirectResponse($this->admin->generateUrl('list', $request->query->all()));
        }

        $repository = $this->getDoctrine()
            ->getManager()
            ->getRepository('NKOMenuBundle:Menu');

        $menuItems = $repository
            ->getRootNodes()
        ;

        foreach ($menuItems as &$item) {
            $item = $repository->getTree($item->getRootMaterializedPath());
        }
        unset($item);

        $datagrid = $this->admin->getDatagrid();

        $formView = $datagrid->getForm()->createView();

        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());

        return $this->render('NKOMenuBundle:MenuAdmin:tree.html.twig', array(
            'action' => 'tree',
            'menu' => $menuItems,
            'form' => $formView,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ));
    }

    public function createAction()
    {
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $this->admin->checkAccess('create');

        $class = new \ReflectionClass($this->admin->hasActiveSubClass() ? $this->admin->getActiveSubClass() : $this->admin->getClass());

        if ($class->isAbstract()) {
            return $this->render(
                'SonataAdminBundle:CRUD:select_subclass.html.twig',
                array(
                    'base_template' => $this->getBaseTemplate(),
                    'admin' => $this->admin,
                    'action' => 'create',
                ),
                null,
                $request
            );
        }

        $object = $this->admin->getNewInstance();

        $preResponse = $this->preCreate($request, $object);

        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);


        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($request->isMethod('POST')) {
            $uniqid = $request->get('uniqid');
            $object->setTitle($request->get($uniqid)['title']);

            $object = $this->admin->create($object);


            $form->submit($request->request->get($form->getName()));
            if ($form->isSubmitted()) {
                //TODO: remove this check for 4.0
                if (method_exists($this->admin, 'preValidate')) {
                    $this->admin->preValidate($object);
                }
                $isFormValid = $form->isValid();


                // persist if the form was valid and if in preview mode the preview was approved
                if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                    $this->admin->checkAccess('create', $object);

                    try {
                        $object = $this->admin->update($object);

                        if ($this->isXmlHttpRequest()) {
                            return $this->renderJson(array(
                                'result' => 'ok',
                                'objectId' => $this->admin->getNormalizedIdentifier($object),
                            ), 200, array());
                        }

                        $this->addFlash(
                            'sonata_flash_success',
                            $this->trans(
                                'flash_create_success',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );

                        // redirect to edit mode
                        return $this->redirectTo($object);
                    } catch (ModelManagerException $e) {
                        $this->handleModelManagerException($e);

                        $isFormValid = false;
                    }
                }

                // show an error message if the form failed validation
                if (!$isFormValid) {
                    if (!$this->isXmlHttpRequest()) {
                        $this->addFlash(
                            'sonata_flash_error',
                            $this->trans(
                                'flash_create_error_menu',
                                array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                                'SonataAdminBundle'
                            )
                        );
                    }
                } elseif ($this->isPreviewRequested()) {
                    // pick the preview template if the form was valid and preview was requested
                    $templateKey = 'preview';
                    $this->admin->getShow();
                }
            }
        }
            $formView = $form->createView();

            return $this->render($this->admin->getTemplate($templateKey), array(
                'action' => 'create',
                'form' => $formView,
                'object' => $object,
            ), null);

    }

}
