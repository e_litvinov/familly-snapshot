<?php

namespace NKO\MenuBundle\Twig;

use NKO\MenuBundle\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MenuExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('list_parentNode_view', array($this, 'listParentNode')),
        );
    }

    /**
     * Get menu with parent number
     * @param Menu $menu
     * @return string
     */
    public function listParentNode(Menu $menu)
    {
       $menuTree = $this->container->get('doctrine')
            ->getRepository("NKOMenuBundle:Menu")
            ->getTree($menu->getRootMaterializedPath());

        $menu = $this->findInTree($menuTree, $menu);

        return "".$menu->getParentNode();
    }

    private function findInTree(Menu $tree, Menu $menu)
    {
        if ( $tree->getId() == $menu->getId() ) {
            return $tree;
        }
        else {
            foreach ($tree->getChildNodes() as $node) {
                if ( $result = $this->findInTree($node, $menu) ) {
                    return $result;
                }
            }
        }

        return false;
    }

    public function getName()
    {
        return 'menu_extension';
    }

}
