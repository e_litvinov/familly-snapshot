<?php
namespace NKO\MenuBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\MenuBundle\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadMenuItem implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function createMenuParentItem(ObjectManager &$manager, $title, $sequence, $url = null)
    {
        $menu= new Menu();
        $menu->setTitle($title);
        $menu->setSequence($sequence);
        $manager->persist($menu);
        if($url){
            $menu->setUrl($url);
        }
    }

    private function createChildItem(ObjectManager &$manager, $title, $sequence, $parentNode, $url)
    {
        $menu= new Menu();
        $menu->setTitle($title);
        $menu->setUrl($url);
        $menu->setSequence($sequence);
        $manager->persist($menu);
        $manager->flush();
        $menu->setParentNode($parentNode);
    }


    public function load(ObjectManager $manager)
    {
        $this->createMenuParentItem($manager, 'Верхний уровень меню', 1);
        $manager->flush();
        $this->createChildItem($manager, 'Фонд Тимченко', 1, $this->getParentItem($manager, 'Верхний уровень меню' ), '');
        $this->createChildItem($manager, 'Семейный фарватер', 2, $this->getParentItem($manager, 'Верхний уровень меню' ), '');
        $this->createChildItem($manager, 'Курс на семью', 3, $this->getParentItem($manager, 'Верхний уровень меню' ), '');
        $this->createChildItem($manager, 'Методические материалы', 4,$this->getParentItem($manager, 'Верхний уровень меню' ),  '/methodicalmaterials');
        $this->createChildItem($manager, 'Контакты', 5, $this->getParentItem($manager, 'Верхний уровень меню' ), '/pagepage/kontakty');
        $manager->flush();
        $this->createChildItem($manager, 'О фонде', 1, $this->getParentItem($manager, 'Фонд Тимченко'),
            '/pagepage/o-fondie');
        $this->createChildItem($manager, 'Новости', 2, $this->getParentItem($manager, 'Фонд Тимченко'),
            '/fondnews');

        $this->createChildItem($manager, 'Конкурс 2016', 1, $this->getParentItem($manager, 'Семейный фарватер'),
            '/informationmaterials/materials_category/siemieinyi-farvatier-konkurs-2016');
        $this->createChildItem($manager, 'Конкурс 2017', 2, $this->getParentItem($manager, 'Семейный фарватер'),
            '/informationmaterials/materials_category/siemieinyi-farvatier-konkurs-2017');

        $this->createChildItem($manager, 'Конкурс 2016', 1, $this->getParentItem($manager, 'Курс на семью'),
            '/informationmaterials/materials_category/kurs-na-siem-iu-2016');
        $manager->flush();

    }

    private function getParentItem(&$manager, $title)
    {
        $parentItem = $manager->getRepository('NKOMenuBundle:Menu')
        ->findOneBy(array('title'=> $title));
        return $parentItem;

    }
}