<?php

namespace NKO\MenuBundle\Admin;

use NKO\MenuBundle\Entity\Menu;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MenuAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', TextType::class)
            ->add('url', TextType::class, ['required' => false])
            ->add('sequence', IntegerType::class, ['required' => false]);

        if ($this->getSubject() && $this->getSubject()->getId()) {
            $this->buildTree($this->getSubject());

            $formMapper
                ->add('parentNode', 'sonata_type_model', [
                    'multiple' => false,
                    'required' => false,
                    'class'   => 'NKOMenuBundle:Menu',
                    'attr' => [
                        'data-sonata-select2-allow-clear' => 'false',
                    ]
                ]);
        } else {
            $formMapper
                ->add('parentNode', 'sonata_type_model', [
                    'multiple' => false,
                    'required' => false,
                    'class'    => 'NKOMenuBundle:Menu',
                    'attr' => [
                        'data-sonata-select2-allow-clear' => 'false'
                    ]
                ]);
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title')
            ->add('parentNode', 'string', [
                'template' => 'NKOMenuBundle:MenuAdmin:list_parentNode.html.twig'
            ]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('sequence');
    }

    public function buildTree(Menu $menu)
    {
        $this->getConfigurationPool()->getContainer()->get('doctrine')
            ->getRepository("NKOMenuBundle:Menu")
            ->getTree($menu->getRootMaterializedPath());

        return $menu->getParentNode();
    }
}
