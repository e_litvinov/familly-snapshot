<?php

namespace NKO\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="menu")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="NKO\MenuBundle\Repository\MenuRepository")
 */
class Menu implements ORMBehaviors\Tree\NodeInterface, \ArrayAccess
{
    use ORMBehaviors\Tree\Node;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Assert\NotNull(message = "sequence can not be null")
     */
    protected $sequence;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $url;

    /**
     * @param Collection the children in the tree
     */
    private $children;

    /**
     * @param NodeInterface the parent in the tree
     */
    private $parent;

    public function __construct()
    {
        $this->children = new ArrayCollection;
    }

    public function __toString()
    {
        return (string)$this->title;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     *
     * @return Menu
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * {@inheritdoc}
     **/
    public function getChildNodes()
    {
        if ($this->childNodes) {
            $iterator = $this->childNodes->getIterator();
            $iterator->uasort(function ($a, $b) {

                return ($a->getSequence() < $b->getSequence()) ? -1 : 1;
            });
            
            return $this->childNodes = new ArrayCollection(iterator_to_array($iterator));
        } else {
            return $this->childNodes = new ArrayCollection();
        }
    }


    /**
     * Set url
     *
     * @param string $url
     *
     * @return Menu
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
