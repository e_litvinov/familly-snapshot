'use strict';

$(window).unbind('beforeunload');

$(document).ready(function ($) {
     $('div[timestamp]').each(function() {
        var timestamp = $(this).attr('timestamp');
        var d = new Date(timestamp * 1000);
        $(this).html(moment(d).lang('ru').format('lll'));
    });

    $('.help-block').insertAfter('div.select2-container[id*="_experts"]');

    if(window.location.pathname.indexOf("documentreport") != -1 ||
        window.location.pathname.indexOf("send_report") != -1 ||
        window.location.pathname.indexOf("reporthistory") != -1
    )
    {
        $("a.sonata-action-element").remove();
    }

    if(window.location.pathname.indexOf("edit") == -1
        && window.location.pathname.indexOf("create") == -1) {
        $.removeCookie('tabBarIndex', { path: '/' });
    }
});