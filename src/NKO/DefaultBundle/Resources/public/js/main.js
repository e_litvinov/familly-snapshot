$(document).ready(function () {
    $("a.list-group-item").each(function() {
        if(window.location.pathname == $(this).attr("href"))
            $(this).addClass("active");
    });

    if (!navigator.cookieEnabled){
        $(".ct_auth_form").hide();
    }
    else{
        $(".cookie-error").hide();
    }

    $(".fancybox").fancybox();

    $("#search_form input.datepicker").datetimepicker({
        language: 'ru',
        pickTime: false
    }).on('change', function(){
        $('.bootstrap-datetimepicker-widget').hide();
    });

    $(".search_panel").on("click", function(){
        var $searchForm = $("#search_form");
        if ($searchForm.is(":hidden")) {
            $searchForm.slideDown("slow");
        } else {
            $searchForm.slideUp("slow");
        }
    });

    $("#clear-form").on("click", function(event){
        var $form = $(this).closest("form");

        event.preventDefault();

        $form.find("input").val("");
        $form.find("select").val("");

        $form.submit();
    });

    $("select#material-category").on("change", function(){

        var categorySelectVal = $(this).val(),
            url =  Routing.generate('materials_category_type_path'),
            $criteriaSelect = $("select#material-criteria");

        $.ajax({
            url: url,
            type: 'post',
            data: categorySelectVal,
            success: function (html) {

                $criteriaSelect.html('');

                var newOption = new Option("", "", true, true);
                $criteriaSelect.append(newOption);

                for (var key in html) {
                    if (html.hasOwnProperty(key)) {
                        newOption = new Option(html[key], key, true, true);
                        $criteriaSelect.append(newOption);
                    }
                }

                $criteriaSelect.val("").trigger("change");
            }
        });
    });

    if($("select[id*='_parentNode']").length > 0){
        $("select[id*='_parentNode']").select2({
            "width": "100%",
            "allow-clear": "false"
        });
    }

    (function(){
        var filterField = $("div[id^='filter'][id$='competition'] .col-sm-4");
        if($('.advanced-filter').css('display') == 'none')
            filterField.addClass('filter-max-width');

        $('[data-toggle="advanced-filter"]').on('click', function(){
            if($('.advanced-filter').css('display') == 'none') {
                filterField.addClass('filter-max-width');
            }
            else {
                filterField.removeClass('filter-max-width');
            }
        });
    })();
});


