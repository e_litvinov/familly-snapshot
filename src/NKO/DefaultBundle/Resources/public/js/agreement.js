$(document).ready(function () {
    console.log('ready');

    $('.agreement-check-input').on('ifChecked', function(){
        $.ajax( {

            type: "POST",
            url: Routing.generate('agreement_check'),
            data: {'value': $(this).val() },
            success: function( response ) {
            },
            complete: function(response)
            {
                if (response.status == 302) {
                    window.location.href = response.responseJSON;
                }
            }
        });

    });
});
