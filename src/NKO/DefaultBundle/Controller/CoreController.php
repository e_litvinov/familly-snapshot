<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 4/28/17
 * Time: 11:47 AM
 */

namespace NKO\DefaultBundle\Controller;

use NKO\OrderBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CoreController as BaseCoreContBroller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CoreController extends BaseCoreContBroller
{
    public function dashboardAction()
    {
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        if (!$currentUser->getIsAcceptAgreement()) {
            return $this->render('agreement.html.twig');
        }

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_EXPERT')) {
            return parent::dashboardAction();
        }

        $em = $this->getDoctrine();
        $competitions = $currentUser->getCompetitions();
        $currentCompetitions = $competitions->filter(function ($object) {
            $today = new \DateTime();

            return $object->getFinishDate() < $today && $object->getFinishEstimate() > $today;
        });

        if (!$currentCompetitions->getValues()) {
            $currentCompetitions = $competitions;
        }

        $values = $currentCompetitions->getValues();
        $currentCompetition = reset($values);



        if (!$values) {
            $currentCompetition = $em
                ->getManager()
                ->createQueryBuilder()
                ->select('e')
                ->from('NKOOrderBundle:Competition', 'e')
                ->orderBy('e.id', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }

        $blocks = array(
            'top' => array(),
            'left' => array(),
            'center' => array(),
            'right' => array(),
            'bottom' => array(),
        );

        foreach ($this->container->getParameter('sonata.admin.configuration.dashboard_blocks') as $block) {
            $blocks[$block['position']][] = $block;
        }

        $parameters = array(
            'base_template' => $this->getBaseTemplate(),
            'admin_pool' => $this->container->get('sonata.admin.pool'),
            'blocks' => $blocks,
            'competitions' => $competitions,
            'current_competition' => $currentCompetition,
        );

        return $this->render($this->getAdminPool()->getTemplate('dashboard'), $parameters);
    }

    public function expertStatisticAction(Request $request)
    {
        $competitionId = $request->get('id');
        $em = $this->getDoctrine();
        $currentCompetitions = $em->getRepository("NKOOrderBundle:Competition")->find($competitionId);

        return new JsonResponse(
            ['view'=>$this->renderView(
                ':layout:_expert_information.html.twig',
                array('current_competition' => $currentCompetitions)
            )],
            200
        );
    }

    public function agreementCheckAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();

        $value = $request->get('value');

        if ($value == 'accept') {
            $currentUser->setIsAcceptAgreement(true);
            $em->flush($currentUser);

            return new JsonResponse($this->generateUrl('default_admin_path'), 302);
        } else {
            $currentUser->setIsAcceptAgreement(false);
            $em->flush($currentUser);

            return new JsonResponse([], 200);
        }
    }
}
