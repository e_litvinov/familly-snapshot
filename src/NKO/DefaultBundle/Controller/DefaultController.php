<?php

namespace NKO\DefaultBundle\Controller;

use NKO\InformationMaterialsBundle\Entity\FondNews;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository("NKOInformationMaterialsBundle:FondNews")
            ->findBy([], ['createdAt' => 'DESC'], 5);

        return $this->render('NKODefaultBundle:Default:index.html.twig',
            array('news' => $news));
    }
}
