<?php

namespace NKO\InformationMaterialsBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use NKO\InformationMaterialsBundle\Entity\FondNews;
use NKO\InformationMaterialsBundle\Form\PublicationStatusType;
use NKO\InformationMaterialsBundle\NKOInformationMaterialsBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use ITM\ImagePreviewBundle\Form\Type\ImagePreviewType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class FondNewsAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Fond News')
                ->add('categories', 'sonata_type_model', [
                         'multiple' =>true,
                        'required' => true,
                        'btn_add' => false,
                        'label'=>'Категория'
                ])
                ->add('name', TextType::class, array('label' => 'Name'))
                ->add('annotation', TextType::class, array('required'=>false))
                ->add('text', CKEditorType::class)
                ->add('createdAt', 'sonata_type_date_picker', array(
                    'format' => 'dd.MM.yyyy'
                 ))
                ->add('isPublished', null, array(
                    'attr' => array(
                        'checked'=> true
                    )
                ))
                ->add('image', ImagePreviewType::class, ['data_class' => null, 'required'=>false])
                ->add('deleteImage', CheckboxType::class, array(
                    'required' => false
                ))
            ->end()
            ->with('Meta-data')
                ->add('title', null, array('required'=>false))
                ->add('meta_description', TextareaType::class, ['required'=>false])
                ->add('meta_keywords', null, array('required'=>false))
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('annotation')
            ->add('categories', null, array('label' => ' Категория'))
            ->add('image', 'string', ['template' => 'NKOInformationMaterialsBundle::show_preview.html.twig'])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ));
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('categories', null, array('label' => ' Категория'))
            ->add('name')
            ->add('annotation')
            ->add('image', null, ['template' => 'NKOInformationMaterialsBundle::show_preview.html.twig'])
            ->add('text', 'html')
            ->add('createdAt', 'date')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('categories', null, array('label' => ' Категория'));
    }

    private function getMetaData($code)
    {
        $metaData =  $this->getConfigurationPool()->getContainer()
            ->get('Doctrine')->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(['code'=>'fond_news']);
        $function = "get{$code}";
        if ($metaData && method_exists($metaData, $function)) {
            return $metaData->$function();
        }
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setTitle($this->getMetaData('Title'));
        $instance->setMetaDescription($this->getMetaData('MetaDescription'));
        $instance->setMetaKeywords($this->getMetaData('MetaKeywords'));

        return $instance;
    }

    public function postPersist($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Добавление новости', $object->getName());
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Редактирование новости', $object->getName());
    }

    public function postRemove($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Удаление новости', $object->getName());
    }
}
