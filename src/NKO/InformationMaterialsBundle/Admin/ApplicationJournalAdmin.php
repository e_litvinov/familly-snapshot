<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 07.02.19
 * Time: 10:20
 */

namespace NKO\InformationMaterialsBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ApplicationJournalAdmin extends AbstractAdmin
{
    const NAME = 'Название';
    const APPLICATION_CLASS = 'Тип заявки';
    const URL = 'Ссылка';


    protected function configureFormFields(FormMapper $formMapper)
    {



        $types = $this->getConfigurationPool()->getContainer()
            ->get('nko.list_application')
            ->getApplicationsClass();

        $formMapper
            ->add('name', TextType::class, [
                'label' => self::NAME
            ])
            ->add('applicationClass', ChoiceType::class, [
                'label' => self::APPLICATION_CLASS,
                'translation_domain' => 'NKOOrderBundle',
                'choices' => $types,
            ])
            ->add('url', TextType::class, [
                'label' => self::URL,
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly'
                ]
            ])








        ;
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('url', null, [
                'label' => self::URL,
            ])



            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));

        ;
    }
}
