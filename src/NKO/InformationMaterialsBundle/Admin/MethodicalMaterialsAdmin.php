<?php

namespace NKO\InformationMaterialsBundle\Admin;

use NKO\InformationMaterialsBundle\Entity\PublicationStatusInterface;
use NKO\InformationMaterialsBundle\Form\PublicationStatusType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class MethodicalMaterialsAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Methodical Materials')
            ->add('baseCategory', EntityType::class, array(
                'class' => 'NKOInformationMaterialsBundle:MethodicalBaseCategory',
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'mapped' => false,
            ))
            ->add('categories', EntityType::class, array(
                'class' => 'NKOInformationMaterialsBundle:MethodicalCategory',
                'expanded' => false,
                'multiple' => true,
                'required' => true,
                'by_reference' => false
            ))
            ->add('name', TextType::class, array('label' => 'Name'))
                ->add('annotation', TextType::class, array('required'=>false))
                ->add('text', CKEditorType::class)
                ->add('createdAt', 'sonata_type_date_picker', array(
                    'format' => 'dd.MM.yyyy'
                ))
                ->add('isPublished', null, array(
                    'attr' => array(
                        'checked'=> true
                    )
                ))
                ->add('publicationStatus', PublicationStatusType::class)
                ->add('methodicalMaterialsFiles', 'sonata_type_collection', [
                        'required' => false,
                        'btn_add' => 'Добавить',
                        'type_options' => array('delete' => true),
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
            ->end()
            ->with('Meta-data')
                ->add('title', null, array('required'=>false))
                ->add('meta_description', TextareaType::class, array('required'=>false))
                ->add('meta_keywords', null, array('required'=>false))
            ->end();
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('annotation')
            ->add('categories')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('annotation')
            ->add('text', 'html')
            ->add('createdAt', 'date')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('categories')
            ->add('name')
            ->add('createdAt', 'doctrine_orm_date_range');
    }

    public function prePersist($materials)
    {
        $this->preUpdate($materials);
    }


    public function preUpdate($materials)
    {
        $materials->setFiles($materials->getMethodicalMaterialsFiles());
        $this->checkFileName($materials);
    }

    private function checkFileName($object)
    {
        foreach ($object->getMethodicalMaterialsFiles() as $file) {
            if ($file->getName() == null) {
                $file->setName('methodical materials file');
            }
        }
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'NKOInformationMaterialsBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery();

        if (!($this->getCurrentUser()->hasRole('ROLE_EMPLOYEE_ADMIN')
            || $this->getCurrentUser()->hasRole('ROLE_MODERATOR')
            || $this->getCurrentUser()->hasRole('ROLE_SUPER_ADMIN'))
        ) {
            $query
                ->andWhere($query->getRootAlias() . '.publicationStatus != ' . PublicationStatusInterface::CUSTOMER_PUBLICATION);
        }
        return $query;
    }

    public function getCurrentUser()
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();
    }

    private function getMetaData($code)
    {
        $metaData =  $this->getConfigurationPool()->getContainer()
            ->get('Doctrine')->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(['code'=>'methodical_materials']);
        $function = "get{$code}";
        if ($metaData && method_exists($metaData, $function)) {
            return $metaData->$function();
        }
    }
    protected function configureRoutes(RouteCollection $collection)
    {
        parent::configureRoutes($collection);
        $collection->add('log-watching-video', $this->getRouterIdParameter().'/log-watching-video');
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setTitle($this->getMetaData('Title'));
        $instance->setMetaDescription($this->getMetaData('MetaDescription'));
        $instance->setMetaKeywords($this->getMetaData('MetaKeywords'));

        return $instance;
    }

    public function postPersist($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Добавление методического материала', $object->getName());
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Редактирование методического материала', $object->getName());
    }

    public function postRemove($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Удаление методического материала', $object->getName());
    }
}
