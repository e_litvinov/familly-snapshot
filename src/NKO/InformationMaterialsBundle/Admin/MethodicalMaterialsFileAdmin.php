<?php

namespace NKO\InformationMaterialsBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;


class MethodicalMaterialsFileAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('file', FilePreviewType::class,
                ['data_class' => null, 'required' => false]);

    }

}