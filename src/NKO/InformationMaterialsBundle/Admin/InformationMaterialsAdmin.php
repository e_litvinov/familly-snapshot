<?php

namespace NKO\InformationMaterialsBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use ITM\FilePreviewBundle\Form\Type\FilePreviewType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class InformationMaterialsAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $status = [
            'Стартовые' => '1',
            'Итоговые' => '2',
        ];

        $formMapper
            ->with('Information Materials')
                ->add('category', 'sonata_type_model', [
                        'required' => false,
                        'btn_add' =>false,
                        'label' => 'Категория'
                    ])
                ->add('name', TextType::class, array('label' => 'Name'))
                ->add('annotation', TextType::class, array('required'=>false))
                ->add('text', CKEditorType::class)
                ->add('createdAt', 'sonata_type_datetime_picker', array(
                    'format' => 'dd.MM.yyyy HH:MM'
                ))
                ->add('isPublished', null, array(
                    'attr' => array(
                        'checked'=> true
                    )
                ))
                ->add('informationMaterialsFiles', 'sonata_type_collection', [
                    'required' => false,
                    'btn_add' => 'Добавить',
                    'type_options' => array('delete' => true),
                ], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->add('status', 'choice', [
                'choices' => $status,
                'multiple' => false,
                ])
            ->end()
            ->with('Meta-data')
                ->add('title', null, array('required'=>false))
                ->add('meta_description', TextareaType::class, [
                    'required'=>false
                ])
                ->add('meta_keywords', null, array('required'=>false))
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('annotation')
            ->add('category', null, array('label' => ' Категория'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array())));
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('category', null, array('label' => ' Категория'));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('annotation')
            ->add('text', 'html')
            ->add('createdAt', 'date')
            ->add('title', null)
            ->add('meta_description')
            ->add('meta_keywords')
            ->add('isPublished');
    }

    public function prePersist($informationMaterials)
    {
        $user = $this->getConfigurationPool()
            ->getContainer()->get('security.token_storage')
            ->getToken()
            ->getUser()
            ->getUsername();
        $informationMaterials->setAuthor($user);
        $this->preUpdate($informationMaterials);
    }

    public function preUpdate($informationMaterials)
    {
        $informationMaterials->setFiles($informationMaterials->getInformationMaterialsFiles());
        $this->checkFileName($informationMaterials);
    }

    private function checkFileName($object)
    {
        foreach ($object->getInformationMaterialsFiles() as $file) {
            if ($file->getName() == null) {
                $file->setName('information materials file');
            }
        }
    }

    private function getMetaData($code)
    {
        $metaData =  $this->getConfigurationPool()->getContainer()
            ->get('Doctrine')->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(['code'=>'information_materials']);
        $function = "get{$code}";
        if ($metaData && method_exists($metaData, $function)) {
            return $metaData->$function();
        }
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'NKOInformationMaterialsBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setTitle($this->getMetaData('Title'));
        $instance->setMetaDescription($this->getMetaData('MetaDescription'));
        $instance->setMetaKeywords($this->getMetaData('MetaKeywords'));

        return $instance;
    }

    public function postPersist($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Добавление информационного материала', $object->getName());
    }

    public function postUpdate($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Редактирование информационного материала', $object->getName());
    }

    public function postRemove($object)
    {
        $service = $this->getConfigurationPool()->getContainer()->get('NKO\LoggerBundle\Loggers\BaseLogger');
        $service->logAction('Удаление информационного материала', $object->getName());
    }
}
