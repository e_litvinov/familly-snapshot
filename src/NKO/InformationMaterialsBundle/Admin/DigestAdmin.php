<?php

namespace NKO\InformationMaterialsBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DigestAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );



    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('name', TextType::class, array('label' => 'Name'))
            ->add('annotation', TextType::class, array('required' => false))
            ->add('text', CKEditorType::class)
            ->add('createdAt', 'sonata_type_date_picker', array(
                'format' => 'dd.MM.yyyy'
            ))
            ->add('isPublished', null, array(
                'attr' => array(
                    'checked'=> true
                )
            ))
            ->add('digestFiles',  'sonata_type_collection',
                array(
                    'required' => false,
                    'btn_add' => 'Добавить',
                    'type_options' => array('delete' => true),
                ),
                array('edit' => 'inline',
                    'inline' => 'table',
                ))

            ->end()
            ->with('Meta-data')
            ->add('title', null, array('required' => false))
            ->add('meta_description', TextareaType::class, array('required' => false))
            ->add('meta_keywords', null, array('required' => false))
            ->end();

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('annotation')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array())));
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('annotation')
            ->add('text', 'html')
            ->add('createdAt', 'date')
            ->add('title', null)
            ->add('meta_description')
            ->add('meta_keywords')
            ->add('isPublished');
    }

    public function prePersist($news)
    {
        $this->preUpdate($news);
    }

    public function preUpdate($digest)
    {
        $digest->setFiles($digest->getDigestFiles());
        $this->checkFileName($digest);
    }

    private function checkFileName($object){
        foreach ($object->getDigestFiles() as $file){
            if($file->getName() == null){
                $file->setName('digest file');
            }
        }

    }

    private function getMetaData($code){
        $metaData =  $this->getConfigurationPool()->getContainer()
            ->get('Doctrine')->getRepository('NKODesignBlocksBundle:MetaData')
            ->findOneBy(['code'=>'digest']);
        $function = "get{$code}";
        if($metaData && method_exists($metaData, $function)){
            return $metaData->$function();
        }
    }

    public function getFormTheme()
    {
        return array_merge(
            parent::getFormTheme(),
            array(
                'NKOInformationMaterialsBundle:Form:form_admin_fields.html.twig',
            )
        );
    }

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setTitle($this->getMetaData('Title'));
        $instance->setMetaDescription($this->getMetaData('MetaDescription'));
        $instance->setMetaKeywords($this->getMetaData('MetaKeywords'));

        return $instance;
    }
}