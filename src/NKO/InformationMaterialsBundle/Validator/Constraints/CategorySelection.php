<?php

namespace NKO\InformationMaterialsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CategorySelection extends Constraint
{
    public $messageDuplicateBaseCategory = "Duplicate base category";
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}