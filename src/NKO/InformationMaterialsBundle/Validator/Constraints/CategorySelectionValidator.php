<?php

namespace NKO\InformationMaterialsBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CategorySelectionValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    protected $container;

    public function __construct(EntityManager $entityManager, Container $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function validate($object, Constraint $constraint)
    {
        $baseCategories = [];
        if (!is_array($object->getCategories())) {
            return;
        }

        foreach ($object->getCategories() as $category) {
            if (in_array($category->getBaseCategory(), $baseCategories)) {
                $this->context->buildViolation($constraint->messageDuplicateBaseCategory)
                    ->atPath('methodical_materials')
                    ->addViolation();
            }
            $baseCategories[] = $category->getBaseCategory();
        }
    }
}
