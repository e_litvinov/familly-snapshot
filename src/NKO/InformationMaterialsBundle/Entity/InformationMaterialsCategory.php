<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="information_materials_category")
 */
class InformationMaterialsCategory
{
    use ORMBehaviors\Sluggable\Sluggable;

    const BASE_URL = '/informationmaterials/materials_category/';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="InformationMaterials", mappedBy="category", cascade={"persist", "remove"})
     */
    protected $materials;

    protected $url;

    public function getUrl()
    {
        return self::BASE_URL . $this->getSlug();
    }


    public function getSluggableFields()
    {
        return ['name'];
    }

    public function getRegenerateSlugOnUpdate(){

        return false;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InformationMaterialsCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materials = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add material
     *
     * @param \NKO\InformationMaterialsBundle\Entity\InformationMaterials $material
     *
     * @return InformationMaterialsCategory
     */
    public function addMaterial(\NKO\InformationMaterialsBundle\Entity\InformationMaterials $material)
    {
        $this->materials[] = $material;

        return $this;
    }

    /**
     * Remove material
     *
     * @param \NKO\InformationMaterialsBundle\Entity\InformationMaterials $material
     */
    public function removeMaterial(\NKO\InformationMaterialsBundle\Entity\InformationMaterials $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }
}
