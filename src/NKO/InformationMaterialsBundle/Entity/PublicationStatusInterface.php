<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 02.02.17
 * Time: 10:49
 */

namespace NKO\InformationMaterialsBundle\Entity;

interface PublicationStatusInterface
{
    const ANYWHERE_PUBLICATION = 0;
    const CUSTOMER_PUBLICATION = 1;
    const ADMIN_PUBLICATION = 2;

    public function setPublicationStatus($publicationStatus);

    public function getPublicationStatus();
}