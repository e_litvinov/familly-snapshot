<?php


namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\InformationMaterialsBundle\Traits\MaterialAuthorTrait;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="information_materials")
 */
class InformationMaterials
{
    use ORMBehaviors\Timestampable\Timestampable,
        MaterialAuthorTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", length=500)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $annotation;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $meta_description;

    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    protected $meta_keywords;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isPublished;

    /**
     * @ORM\OneToMany(targetEntity="InformationMaterialsFile", mappedBy="informationMaterials",  cascade={"all"}, orphanRemoval=true)
     */
    protected $informationMaterialsFiles;

    /**
     * @ORM\ManyToOne(targetEntity="InformationMaterialsCategory", inversedBy="materials")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $category;


    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $status;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->informationMaterialsFiles = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InformationMaterials
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set annotation
     *
     * @param string $annotation
     *
     * @return InformationMaterials
     */
    public function setAnnotation($annotation)
    {
        $this->annotation = $annotation;

        return $this;
    }

    /**
     * Get annotation
     *
     * @return string
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return InformationMaterials
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return InformationMaterials
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return InformationMaterials
     */
    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return InformationMaterials
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->meta_keywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return InformationMaterials
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set category
     *
     * @param \NKO\InformationMaterialsBundle\Entity\InformationMaterialsCategory $category
     *
     * @return InformationMaterials
     */
    public function setCategory(\NKO\InformationMaterialsBundle\Entity\InformationMaterialsCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \NKO\InformationMaterialsBundle\Entity\InformationMaterialsCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add informationMaterialsFile
     *
     * @param \NKO\InformationMaterialsBundle\Entity\InformationMaterialsFile $informationMaterialsFile
     *
     * @return InformationMaterials
     */
    public function addInformationMaterialsFile(\NKO\InformationMaterialsBundle\Entity\InformationMaterialsFile $informationMaterialsFile)
    {
        $this->informationMaterialsFiles[] = $informationMaterialsFile;
        $informationMaterialsFile->setInformationMaterials($this);

        return $this;
    }

    /**
     * Remove informationMaterialsFile
     *
     * @param \NKO\InformationMaterialsBundle\Entity\InformationMaterialsFile $informationMaterialsFile
     */
    public function removeInformationMaterialsFile(\NKO\InformationMaterialsBundle\Entity\InformationMaterialsFile $informationMaterialsFile)
    {
        $this->informationMaterialsFiles->removeElement($informationMaterialsFile);
    }

    /**
     * Get informationMaterialsFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInformationMaterialsFiles()
    {
        return $this->informationMaterialsFiles;
    }

    public function setFiles($files){
        if (count($files) > 0) {
            foreach ($files as $i) {
                $this->addInformationMaterialsFile($i);
            }
        }

        return $this;

    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return InformationMaterials
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
