<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="category_news")
 */
class CategoryNews
{
    use ORMBehaviors\Sluggable\Sluggable;

    const   BASE_URL = '/fondnewscategory/';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="FondNews", mappedBy="categories")
     */
    protected $news;

    protected $url;

    public function getRegenerateSlugOnUpdate(){

        return false;
    }

    public function getUrl()
    {
        return self::BASE_URL . $this->getSlug();
    }



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->news = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    public function getSluggableFields()
    {
        return ['name'];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CategoryNews
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Add news
     *
     * @param \NKO\InformationMaterialsBundle\Entity\FondNews $news
     *
     * @return CategoryNews
     */
    public function addNews(\NKO\InformationMaterialsBundle\Entity\FondNews $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \NKO\InformationMaterialsBundle\Entity\FondNews $news
     */
    public function removeNews(\NKO\InformationMaterialsBundle\Entity\FondNews $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews()
    {
        return $this->news;
    }
}
