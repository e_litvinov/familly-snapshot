<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="information_materials_file")
 */
class InformationMaterialsFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $file;

    /**
     * @ORM\Column(type="text", length=500)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="InformationMaterials", inversedBy="informationMaterialsFiles", cascade={"persist"})
     * @ORM\JoinColumn(name="information_materials_id", referencedColumnName="id")
     */
    protected $informationMaterials;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return InformationMaterialsFile
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set informationMaterials
     *
     * @param \NKO\InformationMaterialsBundle\Entity\InformationMaterials $informationMaterials
     *
     * @return InformationMaterialsFile
     */
    public function setInformationMaterials(\NKO\InformationMaterialsBundle\Entity\InformationMaterials $informationMaterials = null)
    {
        $this->informationMaterials = $informationMaterials;

        return $this;
    }

    /**
     * Get informationMaterials
     *
     * @return \NKO\InformationMaterialsBundle\Entity\InformationMaterials
     */
    public function getInformationMaterials()
    {
        return $this->informationMaterials;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InformationMaterialsFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
