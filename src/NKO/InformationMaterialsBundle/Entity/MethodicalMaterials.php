<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use NKO\InformationMaterialsBundle\Traits\PublicationStatusTrait;
use Symfony\Component\Validator\Constraints as Assert;
use NKO\InformationMaterialsBundle\Validator\Constraints as CategoryAssert;


/**
 * @CategoryAssert\CategorySelection
 * @ORM\Entity("")
 * @ORM\Table(name="methodical_materials")
 */
class MethodicalMaterials implements PublicationStatusInterface
{
    use ORMBehaviors\Timestampable\Timestampable;
    use PublicationStatusTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $annotation;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $meta_description;

    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    protected $meta_keywords;

    /**
     * @ORM\ManyToMany(targetEntity="MethodicalCategory", mappedBy="materials", cascade={"all"})
     */
    protected $categories;



    /**
     * @ORM\OneToMany(targetEntity="MethodicalMaterialsFile", mappedBy="methodicalMaterials",  cascade={"all"}, orphanRemoval=true)
     */
    protected $methodicalMaterialsFiles;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isPublished;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->methodicalMaterialsFiles = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MethodicalMaterials
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set annotation
     *
     * @param string $annotation
     *
     * @return MethodicalMaterials
     */
    public function setAnnotation($annotation)
    {
        $this->annotation = $annotation;

        return $this;
    }

    /**
     * Get annotation
     *
     * @return string
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return MethodicalMaterials
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return MethodicalMaterials
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return MethodicalMaterials
     */
    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return MethodicalMaterials
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->meta_keywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return MethodicalMaterials
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }
    
    /**
     * Set file
     *
     * @param string $file
     *
     * @return MethodicalMaterials
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }



    /**
     * Add category
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category
     *
     * @return MethodicalMaterials
     */
    public function addCategory(\NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category)
    {
        $this->categories[] = $category;
        $category->addMaterial($this);

        return $this;
    }

    /**
     * Remove category
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category
     */
    public function removeCategory(\NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */

    public function getCategories()
    {
        return $this->categories;
    }


    public function addMethodicalMaterialsFile(\NKO\InformationMaterialsBundle\Entity\MethodicalMaterialsFile $methodicalMaterialsFile)
    {
        $this->methodicalMaterialsFiles[] = $methodicalMaterialsFile;
        $methodicalMaterialsFile->setMethodicalMaterials($this);

        return $this;
    }

    /**
     * Remove methodicalMaterialsFile
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalMaterialsFile $methodicalMaterialsFile
     */
    public function removeMethodicalMaterialsFile(\NKO\InformationMaterialsBundle\Entity\MethodicalMaterialsFile $methodicalMaterialsFile)
    {
        $this->methodicalMaterialsFiles->removeElement($methodicalMaterialsFile);
    }

    /**
     * Get methodicalMaterialsFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMethodicalMaterialsFiles()
    {
        return $this->methodicalMaterialsFiles;
    }

    public function setFiles($files){
        if (count($files) > 0) {
            foreach ($files as $i) {
                $this->addMethodicalMaterialsFile($i);
            }
        }

        return $this;

    }

}
