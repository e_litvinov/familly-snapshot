<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="methodical_materials_file")
 */
class MethodicalMaterialsFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $file;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;


    /**
     * @ORM\ManyToOne(targetEntity="MethodicalMaterials", inversedBy="methodicalMaterialsFiles", cascade={"persist"})
     * @ORM\JoinColumn(name="methodical_materials_id", referencedColumnName="id")
     */
    protected $methodicalMaterials;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return MethodicalMaterialsFile
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set methodicalMaterials
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalMaterials $methodicalMaterials
     *
     * @return MethodicalMaterialsFile
     */
    public function setMethodicalMaterials(\NKO\InformationMaterialsBundle\Entity\MethodicalMaterials $methodicalMaterials = null)
    {
        $this->methodicalMaterials = $methodicalMaterials;

        return $this;
    }

    /**
     * Get methodicalMaterials
     *
     * @return \NKO\InformationMaterialsBundle\Entity\MethodicalMaterials
     */
    public function getMethodicalMaterials()
    {
        return $this->methodicalMaterials;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MethodicalMaterialsFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
