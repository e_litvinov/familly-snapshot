<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="digest")
 */
class Digest
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $annotation;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $meta_description;

    /**
     * @ORM\Column(type="string",  nullable=true)
     */
    protected $meta_keywords;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $isPublished;

    /**
     * @ORM\OneToMany(targetEntity="DigestFile", mappedBy="digest",  cascade={"all"}, orphanRemoval=true)
     */
    protected $digestFiles;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->digestFiles = new ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Digest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set annotation
     *
     * @param string $annotation
     *
     * @return Digest
     */
    public function setAnnotation($annotation)
    {
        $this->annotation = $annotation;

        return $this;
    }

    /**
     * Get annotation
     *
     * @return string
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Digest
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Digest
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return Digest
     */
    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     *
     * @return Digest
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->meta_keywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return Digest
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }


    /**
     * Add digestFile
     *
     * @param \NKO\InformationMaterialsBundle\Entity\DigestFile $digestFile
     *
     * @return Digest
     */
    public function addDigestFile(\NKO\InformationMaterialsBundle\Entity\DigestFile $digestFile)
    {
        $this->digestFiles[] = $digestFile;
        $digestFile->setDigest($this);

        return $this;
    }

    /**
     * Remove digestFile
     *
     * @param \NKO\InformationMaterialsBundle\Entity\DigestFile $digestFile
     */
    public function removeDigestFile(\NKO\InformationMaterialsBundle\Entity\DigestFile $digestFile)
    {
        $this->digestFiles->removeElement($digestFile);
    }

    /**
     * Get digestFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDigestFiles()
    {
        return $this->digestFiles;
    }

    public function setFiles($files){
        if (count($files) > 0) {
            foreach ($files as $i) {
                $this->addDigestFile($i);
            }
        }

        return $this;

    }
}
