<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="methodical_category")
 */
class MethodicalCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="MethodicalBaseCategory", inversedBy="categories")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="cascade")
     */
    protected $baseCategory;

    /**

     * @ORM\ManyToMany(targetEntity="MethodicalMaterials", inversedBy="categories", cascade={"persist"})
     * @ORM\JoinTable(name="category_material",
     *     joinColumns={@ORM\JoinColumn(name="material_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")})
     */
    protected $materials;


    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MethodicalCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set baseCategory
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalBaseCategory $baseCategory
     *
     * @return MethodicalCategory
     */
    public function setBaseCategory(\NKO\InformationMaterialsBundle\Entity\MethodicalBaseCategory $baseCategory = null)
    {
        $this->baseCategory = $baseCategory;

        return $this;
    }

    /**
     * Get baseCategory
     *
     * @return \NKO\InformationMaterialsBundle\Entity\MethodicalBaseCategory
     */
    public function getBaseCategory()
    {
        return $this->baseCategory;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materials = new ArrayCollection();
    }



    /**
     * Add material
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalMaterials $material
     *
     * @return MethodicalCategory
     */
    public function addMaterial(\NKO\InformationMaterialsBundle\Entity\MethodicalMaterials $material)
    {
        $this->materials[] = $material;

        return $this;
    }

    /**
     * Remove material
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalMaterials $material
     */
    public function removeMaterial(\NKO\InformationMaterialsBundle\Entity\MethodicalMaterials $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }
}
