<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="digest_file")
 */
class DigestFile
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $file;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Digest", inversedBy="digestFiles", cascade={"persist"})
     * @ORM\JoinColumn(name="digest_id", referencedColumnName="id")
     */
    protected $digest;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return DigestFile
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    public function __toString()
    {
        return 'file';
    }


    /**
     * Set digest
     *
     * @param \NKO\InformationMaterialsBundle\Entity\Digest $digest
     *
     * @return DigestFile
     */
    public function setDigest(\NKO\InformationMaterialsBundle\Entity\Digest $digest = null)
    {
        $this->digest = $digest;

        return $this;
    }

    /**
     * Get digest
     *
     * @return \NKO\InformationMaterialsBundle\Entity\Digest
     */
    public function getDigest()
    {
        return $this->digest;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DigestFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
