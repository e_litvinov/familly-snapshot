<?php

namespace NKO\InformationMaterialsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\Entity("")
 * @ORM\Table(name="methodical_base_category")
 */
class MethodicalBaseCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $name;


    /**
     * @ORM\OneToMany(targetEntity="MethodicalCategory", mappedBy="baseCategory", cascade={"persist", "remove"})
     */
    protected $categories;


    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MethodicalBaseCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

 

    /**
     * Add category
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category
     *
     * @return MethodicalBaseCategory
     */
    public function addCategory(\NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category
     */
    public function removeCategory(\NKO\InformationMaterialsBundle\Entity\MethodicalCategory $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

}
