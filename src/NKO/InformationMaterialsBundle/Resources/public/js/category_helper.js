'use strict';

$(document).ready(function ($) {

    var $checkbox = $("div[id*='_baseCategory'] .iCheck-helper"),
        $checkboxLabel = $checkbox.closest("label"),
        categorySelect = "select[id$='_categories']",
        removeIcon = ".category-value .glyphicon-remove",
        attrName = "base-category",
        $body = $("body"),
        needToInsert = false,
        $selectSelector = $("select[id$='categories']"),
        url =  Routing.generate('materials_category_type_path'),
        removeItemUrl = Routing.generate('materials_category_type_remove_path');


    displaySelectedValues($selectSelector, $("input[id*='baseCategory']:checked").attr("id"));

    $checkbox.on("click", function()
    {
        ajaxForCategoryType($(this).siblings("input").attr('id'));
    });

    $checkboxLabel.on("click", function()
    {
        ajaxForCategoryType($(this).find("input").attr('id'));
    });

    $(categorySelect).on("change", function()
    {
        var checkedBaseCategory = $("input[id*='baseCategory']:checked"),
            checkedBaseCategoryId = checkedBaseCategory.attr("id"),
            needToChange = false;

        $(".category-value").each(function()
        {
            if($(this).attr(attrName) === checkedBaseCategoryId){
                needToChange = true;
                return false;
            }
            else needToChange = false;
        });

        if(needToInsert)
            insertCategoryValue($selectSelector, checkedBaseCategoryId, needToChange);
    });

    $body.on("click", removeIcon, function()
    {
        var $label = $(this).closest("label"),
            actionArr = $("form[role='form']").attr("action").split("/"),
            objId = actionArr[actionArr.length - 2],
            dataForSend = {"objId" : objId, "categoryId" : $label.attr("data-val")};

        $.ajax({
            url: removeItemUrl,
            type: 'post',
            data: dataForSend,
            success: function (html) {
                // console.log(html);
            }
        });

        $label.siblings("input[value='" + $label.attr("data-val") + "']").remove();
        $label.remove();
    });

    function ajaxForCategoryType(baseCategoryId)
    {
        var baseCategoryVal = $("#"+baseCategoryId).val(),
            categorySelectVal = $(categorySelect).val();

        $.ajax({
            url: url,
            type: 'post',
            data: baseCategoryVal,
            success: function (html) {
                needToInsert = false;

                $body.find(categorySelect).html('');

                for (var key in html) {
                    if (html.hasOwnProperty(key)) {
                        var newOption = new Option(html[key], key, true, true);
                        $body.find(categorySelect).append(newOption).trigger('change');
                    }
                }

                $body.find(categorySelect).attr(attrName, baseCategoryId);

                if(categorySelectVal !== undefined)
                    $(categorySelect).val(categorySelectVal).trigger("change");
                else
                    $(categorySelect).val(0);

                needToInsert = true;
            }
        });

    }

    function insertCategoryValue($selector,checkedBaseCategoryId, needToChange)
    {
        var categoryContainer = ".box-body > .sonata-ba-collapsed-fields > div[id$='_categories'].form-group",
            $categoriesSelect = $("div[id$='_categories'].select2-container-multi"),

            removeIcon = "<span class='glyphicon glyphicon-remove'></span>",
            selectText = $selector.find("option:selected").text(),
            baseCategoryAttr = $selector.attr("name"),
            newLabel = "<label class='category-value'"
                        + " " + attrName + "='"
                        + checkedBaseCategoryId+"'>"
                        + removeIcon
                        + selectText
                        + "</label>",
            hiddenField = "<input type='hidden' name='" + baseCategoryAttr + "'" +
                " base-category='" + checkedBaseCategoryId + "' value='" + $selector.val() + "'>"
        ;

        if(needToChange) {
            $("label[base-category='" + checkedBaseCategoryId + "']").html(removeIcon + selectText);
            $("input[base-category='" + checkedBaseCategoryId + "']").val($selector.val());
        }
        else {
            $(newLabel).appendTo(categoryContainer);
            $(hiddenField).appendTo(categoryContainer);
        }
        $categoriesSelect.css("margin-bottom", "15px");
        $selector.find("option:selected").removeAttr("selected");
        $(".select2-search-choice").hide();
    }

    function displaySelectedValues($selector, checkedBaseCategoryId)
    {
        var categoryContainer = ".box-body > .sonata-ba-collapsed-fields > div[id$='_categories'].form-group",
            $categoriesSelect = $("div[id$='_categories'].select2-container-multi"),
            categoryAttr = $selector.attr("name")
        ;

        $selector.find("option:selected").each(function(){
            var selectText = $(this).text(),
                removeIcon = "<span class='glyphicon glyphicon-remove'></span>",
                newLabel = "<label class='category-value'"
                    + " " + attrName + "='"
                    + checkedBaseCategoryId+"' data-val='"+ $(this).val()+"'>"
                    + removeIcon
                    + selectText
                    +"</label>",
                hiddenField = "<input type='hidden' name='" + categoryAttr + "'" +
                    " base-category='" + checkedBaseCategoryId + "' value='" + $(this).val() + "'>";

            $(newLabel).appendTo(categoryContainer);
            $(hiddenField).appendTo(categoryContainer);
        });

        $categoriesSelect.css("margin-bottom", "15px");
        $selector.html("");
        $(".select2-search-choice").hide();
    }
});



