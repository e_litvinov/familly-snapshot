$(document).ready(function ($) {

    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
});

function onYouTubeIframeAPIReady() {
    var player;
    player = new YT.Player('player', {
        events: {
            'onStateChange': onPlayerStateChange
        }
    });
}

function onPlayerStateChange(event) {
    if (event.data == 1 && !isVideoAlreadyWatched) {
        isVideoAlreadyWatched = true;
        console.log(event.target.f.src);
        var youtubeSRC = event.target.f.src.replace(/(^\w+:|^)\/\//, '');

        $.ajax({
            url:     logRoute + '?youtubeSRC' + youtubeSRC ,
            type:     "POST",
            success: function(response) {
                console.log(response);
            }
        });

    }
}