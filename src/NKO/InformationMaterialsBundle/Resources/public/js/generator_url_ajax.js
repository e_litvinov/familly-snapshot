'use strict';

var generatorUrl;

$(document).ready(function ($) {
    generatorUrl = Routing.generate('generate_url', {}, true);
    var name = $('select[id$="_applicationClass"]');
    var url = $('input[id$="_url"]');
    var createdAt = $('input[id$="_createdAt"]');
    name.on('change', function () {
        var applicationClass = $(this).val();
        ajaxSender(applicationClass, url, createdAt);
    });


});


function ajaxSender(applicationClass, urlSelector, createdAtSelector) {
    $.ajax({
        url: generatorUrl,
        type: "GET",
        dataType: "json",
        data: {'applicationClass' : applicationClass},
        async: false,
        success: function(response) {
            var generatedUrlLabel = '';
            var generatedCreatedAt = response['createdAt'];

            if (response['url']) {
                generatedUrlLabel = Routing.generate('get_application_journal', {ids : response['url']}, true);
            } else {
                generatedUrlLabel = response['errorMessage'];
            }

            urlSelector.val(generatedUrlLabel);
            createdAtSelector.val(generatedCreatedAt);
        }
    });
}