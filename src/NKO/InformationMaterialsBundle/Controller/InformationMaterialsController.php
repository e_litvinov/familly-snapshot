<?php

namespace NKO\InformationMaterialsBundle\Controller;

use NKO\InformationMaterialsBundle\Entity\InformationMaterials;
use NKO\InformationMaterialsBundle\Entity\PublicationStatusInterface;
use NKO\MenuBundle\Entity\Menu;
use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Sonata\AdminBundle\Admin\AdminInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use NKO\InformationMaterialsBundle\Entity\InformationMaterialsFile;

/**
 * Informationmaterial controller.
 *
 * @Route("informationmaterials")
 */
class InformationMaterialsController extends Controller
{
    const  STATUS = ["Все материалы", 'Стартовые', 'Итоговые' ];

    /**
     * Finds and displays a informationMaterial entity.
     *
     * @Route("/{id}", name="informationmaterials_show")
     * @Method("GET")
     */
    public function showAction(InformationMaterials $informationMaterial)
    {
        return $this->render('NKOInformationMaterialsBundle:informationmaterials:show.html.twig', array(
            'informationMaterial' => $informationMaterial,
        ));
    }
    /**
     * Finds and displays a informationMaterial entity.
     *
     * @Route("/materials_category/{slug}", name="informationmaterials_category")
     * @Method("GET")
     */
    public function showMaterialsByCategory(Request $request)
    {
        $statusId = $request->get('material_id');
        $em = $this->getDoctrine()->getManager();

        $url = '/informationmaterials/materials_category/'. $request->get('slug');
        /** @var Menu $menuItem */
        $menuItem = $em->getRepository('NKOMenuBundle:Menu')->findOneBy(['url' => $url]);
        $pageTitle = $menuItem ? $menuItem->getTitle() : 'Конкурсы';

        if ($statusId) {
            $materials = $em->createQueryBuilder()
                ->select('i')
                ->from('NKOInformationMaterialsBundle:InformationMaterials', 'i')
                ->join('NKOInformationMaterialsBundle:InformationMaterialsCategory', 'c', 'WITH', 'c.id=i.category')
                ->where('c.slug = :slug')
                ->andWhere('i.status = :status')
                ->andWhere('i.isPublished=1')
                ->orderBy('i.createdAt', "DESC")
                ->setParameters(['slug' => $request->get('slug'), 'status' => $statusId]);
        } else {
            $materials = $em->createQueryBuilder()
                ->select('i')
                ->from('NKOInformationMaterialsBundle:InformationMaterials', 'i')
                ->join('NKOInformationMaterialsBundle:InformationMaterialsCategory', 'c', 'WITH', 'c.id=i.category')
                ->where('c.slug = ?1')
                ->andWhere('i.isPublished=1')
                ->orderBy('i.createdAt', "DESC")
                ->setParameter(1, $request->get('slug'));
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $materials,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('NKOInformationMaterialsBundle:informationmaterials:index.html.twig', array(
            'informationMaterials' => $materials, 'pagination' => $pagination,
            'status' => self::STATUS,
            'pageTitle' => $pageTitle,
        ));
    }

    /**
     *
     * @Route("download/{id}", name="informationmaterials_download")
     * @Method("GET")
     */
    public function downloadFileAction(InformationMaterialsFile $informationMaterialFile)
    {
        if (!$informationMaterialFile) {
            throw $this->createNotFoundException('unable to find the object');
        }
        $resolver = $this->get('itm.file.preview.path.resolver');
        $path = $resolver->getPath($informationMaterialFile, 'file');

        if ($path!= null) {
            $response = new StreamedResponse();
            $response->setCallback(function () use ($path) {
                echo file_get_contents($path);
            });
            $content_type = mime_content_type($path);

            $response->headers->set('Content-Type', $content_type);
            $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, basename($path));
            $response->headers->set('Content-Disposition', $contentDisposition);

            return $response;
        }

        return new $this->redirectToRoute('digest_show', array('informationMaterial' => $informationMaterialFile->getInformationMaterials()));
    }
}
