<?php

namespace NKO\InformationMaterialsBundle\Controller;

use NKO\InformationMaterialsBundle\Entity\MethodicalMaterials;
use NKO\InformationMaterialsBundle\Entity\PublicationStatusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use NKO\InformationMaterialsBundle\Entity\MethodicalMaterialsFile;


/**
 * Methodicalmaterial controller.
 *
 * @Route("methodicalmaterials")
 */
class MethodicalMaterialsController extends Controller
{
    /**
     * Lists all methodicalMaterial entities.
     *
     * @Route("/", name="methodicalmaterials_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('NKOInformationMaterialsBundle:MethodicalBaseCategory')
            ->findAll();
            $query = $em->createQueryBuilder()
                ->select('d')
                ->from('NKOInformationMaterialsBundle:MethodicalMaterials', 'd')
                ->andWhere('d.isPublished = 1')
                ->andWhere('d.publicationStatus != '.PublicationStatusInterface::ADMIN_PUBLICATION)
                ->orderBy('d.createdAt', 'DESC');

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('NKOInformationMaterialsBundle:methodicalmaterials:index.html.twig', array(
            'pagination' => $pagination, 'categories' => $categories,));

    }

    /**
     *
     * @Route("/", name="methodicalmaterials_search")
     * @Method("POST")
     */
    public function searchAction(Request $request)
    {
        $formData = $request->request->get('material');

        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('NKOInformationMaterialsBundle:MethodicalBaseCategory')
            ->findAll();
        $criterias = $em->getRepository('NKOInformationMaterialsBundle:MethodicalCategory')
            ->findAll();

        $query = $this->getQuery($em);
        $query = $this->buildQuery($formData, $em, $query);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('NKOInformationMaterialsBundle:methodicalmaterials:index.html.twig', array(

            'pagination' => $pagination, 'categories'=>$categories, 'criterias' =>$criterias));

    }
    private function buildQuery($formData, $em, $query)
    {
        $queryParameters = [];
        if($formData['category'] && $formData['criteria']){
            $criteria = $em->getRepository('NKOInformationMaterialsBundle:MethodicalCategory')
                ->find($formData['criteria']);
            $query->andWhere(':criteria MEMBER OF d.categories');
            $queryParameters['criteria'] = $criteria;
        }
        if($formData['category'] && !$formData['criteria']){
            $category = $em->getRepository('NKOInformationMaterialsBundle:MethodicalBaseCategory')
            ->find($formData['category']);
            $criterias = $em->getRepository('NKOInformationMaterialsBundle:MethodicalCategory')
                ->findBy(['baseCategory' => $category]);
            $query->andWhere(':criterias MEMBER OF d.categories');
            $queryParameters['criterias'] = $criterias;

        }
        if( $formData['name']){
            $query->andWhere("d.name LIKE :name");
            $queryParameters['name'] = "%{$formData['name']}%";
        };
        if($formData['date_from']){
            $query->andWhere("d.updatedAt >= :date_from");
            $date = new \DateTime($formData['date_from']);
            $queryParameters['date_from'] = $date;
        }
        if($formData['date_to']){
            $query->andWhere("d.updatedAt <= :date_to");
            $date = new \DateTime($formData['date_to']);
            $queryParameters['date_to'] = $date;
        }
        $query->orderBy('d.createdAt', 'DESC')->setParameters($queryParameters);
        return $query;


    }

    private function getQuery($em)
    {
        return $query = $em->createQueryBuilder()
            ->select('d')
            ->from('NKOInformationMaterialsBundle:MethodicalMaterials', 'd')
            ->andWhere('d.isPublished = 1')
            ->orderBy('d.createdAt', 'DESC');

    }

    /**
     * Finds and displays a methodicalMaterial entity.
     *
     * @Route("/{id}", name="methodicalmaterials_show")
     * @Method("GET")
     */
    public function showAction(MethodicalMaterials $methodicalMaterial)
    {
        return $this->render('NKOInformationMaterialsBundle:methodicalmaterials:show.html.twig', array(
            'methodicalMaterial' => $methodicalMaterial ));
    }

    /**
     *
     * @Route("download/{id}", name="methodicalmaterials_download")
     * @Method("GET")
     */
    public function downloadFileAction(MethodicalMaterialsFile $methodicalMaterialFile)
    {
        if (!$methodicalMaterialFile) {
            throw $this->createNotFoundException('unable to find the object');
        }
        $resolver = $this->get('itm.file.preview.path.resolver');
        $path = $resolver->getPath($methodicalMaterialFile, 'file');

        if($path!= null){

            $response = new StreamedResponse();
            $response->setCallback(function() use($path) {
                echo file_get_contents($path);
            });
            $content_type = mime_content_type($path);
            $response->headers->set('Content-Type', $content_type);
            $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, basename($path));
            $response->headers->set('Content-Disposition', $contentDisposition);

            return $response;
        }

        return new $this->redirectToRoute('methodicalmaterials_show',
            array('methodicalMaterial' => $methodicalMaterialFile->getMethodicalMaterials()));
    }
}
