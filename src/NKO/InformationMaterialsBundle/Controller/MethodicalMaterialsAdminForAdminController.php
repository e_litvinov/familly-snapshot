<?php

namespace NKO\InformationMaterialsBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Response;

class MethodicalMaterialsAdminForAdminController extends Controller
{
    public function showAction($id = null)
    {
        $service = $this->get('NKO\LoggerBundle\Loggers\MethodicalMaterialsLogger');
        $service->logAction('Просмотр методического материала', $this->admin->getSubject());

        return parent::showAction($id);
    }

    public function logWatchingVideoAction($id = null)
    {
        $service = $this->get('NKO\LoggerBundle\Loggers\MethodicalMaterialsLogger');
        $service->logAction('Просмотр видео', $this->admin->getSubject());

        return new Response('Log is ok');
    }
}
