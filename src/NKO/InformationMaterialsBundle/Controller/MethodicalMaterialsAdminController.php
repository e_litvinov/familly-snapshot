<?php

namespace NKO\InformationMaterialsBundle\Controller;

use NKO\InformationMaterialsBundle\Entity\MethodicalCategory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class MethodicalMaterialsAdminController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function getCategoryAction(Request $request)
    {
        $baseCategoryId = $request->getContent();

        $query = $this->getDoctrine()->getRepository('NKOInformationMaterialsBundle:MethodicalCategory')
            ->createQueryBuilder('p')
            ->where('p.baseCategory = :id')
            ->setParameter('id', $baseCategoryId)
            ->getQuery();

        $tempCategories = $query->getResult();
        $categories = [];
        foreach($tempCategories as $category)
        {
            $categories[$category->getId()] = $category->getName();
        }

        return new JsonResponse($categories);
    }


    public function deleteCategoryAction(Request $request){
        $objId =  $request->get('objId');
        $categoryId = $request->get('categoryId');
        $em = $this->getDoctrine()->getManager();
        if($objId){
            $obj = $em->getRepository("NKOInformationMaterialsBundle:MethodicalMaterials")->find($objId);
            $category = $em->getRepository("NKOInformationMaterialsBundle:MethodicalCategory")->find($categoryId);

            if($obj->getCategories()->contains($category)){
                $obj->removeCategory($category);
                $category->removeMaterial($obj);
                $em->persist($obj);
                $em->persist($category);
                $em->flush();
            }

        }
        return new Response("OK");
    }

}