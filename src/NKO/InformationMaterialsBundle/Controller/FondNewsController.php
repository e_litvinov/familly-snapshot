<?php

namespace NKO\InformationMaterialsBundle\Controller;

use NKO\InformationMaterialsBundle\Entity\CategoryNews;
use NKO\InformationMaterialsBundle\Entity\FondNews;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Fondnews controller.
 *
 * @Route("fondnews")
 */
class FondNewsController extends Controller
{
    /**
     * Lists all fondNews entities.
     *
     * @Route("/", name="fondnews_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('d')
            ->from('NKOInformationMaterialsBundle:FondNews', 'd')
            ->orderBy('d.createdAt', 'DESC')
        ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        $categories = $this->getCategoryNews();

        return $this->render('NKOInformationMaterialsBundle:fondnews:index.html.twig', array(
            'pagination' => $pagination, 'categories' => $categories
        ));

    }

    /**
     * Finds and displays a fondNews entity.
     *
     * @Route("/{id}", name="fondnews_show")
     * @Method("GET")
     */
    public function showAction(FondNews $fondNews)
    {
        $categories = $this->getCategoryNews();

        return $this->render('NKOInformationMaterialsBundle:fondnews:show.html.twig', array(
            'fondNews' => $fondNews, 'categories' => $categories ));
    }

    /**
     *
     * @Route("category/{slug}", name="fondnews_category")
     * @Method("GET")
     */
    public function showNewsByCategoryAction(CategoryNews $categoryNews, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('NKOInformationMaterialsBundle:CategoryNews')
            ->findOneBy(['slug' => $request->get('slug')]);
        $query = $em->createQueryBuilder()
            ->select('d')
            ->from('NKOInformationMaterialsBundle:FondNews', 'd')
            ->join('d.categories', 'c', 'WITH', 'c.id = ?1')
            ->orderBy('d.createdAt', 'DESC')
            ->setParameter(1, $category->getId());
        ;

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );
        $categories = $this->getCategoryNews();

        return $this->render('NKOInformationMaterialsBundle:fondnews:index.html.twig', array(
            'pagination' => $pagination, 'categories' => $categories));
    }

    private function getCategoryNews(){
        $em = $this->getDoctrine()->getManager();
        return $em->getRepository('NKOInformationMaterialsBundle:CategoryNews')->findAll();
    }

    public function getCategoryNewsAction(){
        $categories =  $this->getCategoryNews();

        return $this->render('NKOInformationMaterialsBundle:fondnews:_news_links.html.twig', array(
             'categories' => $categories));
    }
}
