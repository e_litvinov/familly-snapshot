<?php

namespace NKO\InformationMaterialsBundle\Controller;

use NKO\InformationMaterialsBundle\Entity\Digest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use NKO\InformationMaterialsBundle\Entity\DigestFile;

/**
 * Digest controller.
 *
 * @Route("digest")
 */
class DigestController extends Controller
{
    /**
     * Lists all digest entities.
     *
     * @Route("/", name="digest_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('d')
            ->from('NKOInformationMaterialsBundle:Digest', 'd')
            ->where('d.isPublished = 1')
            ->orderBy('d.createdAt', 'DESC')
            ;
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('NKOInformationMaterialsBundle:digest:index.html.twig', array(
            'pagination' => $pagination ));

    }

    /**
     * Finds and displays a digest entity.
     *
     * @Route("/{id}", name="digest_show")
     * @Method("GET")
     */
    public function showAction(Digest $digest)
    {
        return $this->render('NKOInformationMaterialsBundle:digest:show.html.twig', array(
            'digest' => $digest ));
    }


    /**
     *
     * @Route("download/{id}", name="digest_download")
     * @Method("GET")
     */
    public function downloadFileAction(DigestFile $digestFile)
    {

        if (!$digestFile) {
            throw $this->createNotFoundException('unable to find the object');
        }
        $resolver = $this->get('itm.file.preview.path.resolver');
        $path = $resolver->getPath($digestFile, 'file');

        if($path!= null){

            $response = new StreamedResponse();
            $response->setCallback(function() use($path) {
                echo file_get_contents($path);
            });
            $content_type = mime_content_type($path);

            $response->headers->set('Content-Type', $content_type);
            $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, basename($path));
            $response->headers->set('Content-Disposition', $contentDisposition);

            return $response;
        }

        return new $this->redirectToRoute('digest_show', array('digest' => $digestFile->getDigest()));
    }
}
