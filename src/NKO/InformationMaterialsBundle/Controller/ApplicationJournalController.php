<?php
/**
 * Created by PhpStorm.
 * User: nickolka
 * Date: 06.02.19
 * Time: 18:17
 */

namespace NKO\InformationMaterialsBundle\Controller;

use NKO\OrderBundle\Entity\ApplicationHistory;
use NKO\OrderBundle\Entity\Competition;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Controller of .
 *
 * @Route("/journal")
 */
class ApplicationJournalController extends Controller
{
    /**
     * Generate url for journal.
     *
     * @Route("/generateUrl", name="generate_url")
     * @Method("GET")
     */
    public function generateUrlAction(Request $request)
    {
        $response = [
            'createdAt' => (new \DateTime())->format('Y-m-d')
        ];

        $generatedUrl = $this->generateUrlForApplicationJournal($request->query->get('applicationClass'));

        if ($generatedUrl) {
            $response['url'] = $generatedUrl;
        } else {
            $response['errorMessage'] = 'Конкурсы по этому типу заявок временно не проводятся';
        }

        return new JsonResponse($response);
    }

    /**
     * Finds and displays a applications for competition.
     *
     * @Route("/journal/{ids}/application", name="get_application_journal")
     * @Method("GET")
     */
    public function getApplicationJournalAction($ids)
    {
        $competitionsIds = explode('_', $ids);

        $this->em = $this->getDoctrine()->getManager();
        $histories = $this->getDoctrine()->getRepository(ApplicationHistory::class)
            ->findSendByCompetitions($competitionsIds);
        $competitions = $this->getDoctrine()->getRepository(Competition::class)->getCompetitionsByIds($competitionsIds);
        $competitionNames = $this->generateNameOfCompetitions($competitions);

        $journalDataGenerator = $this->get('journal_data_generator');
        $journalDataGenerator->setData($histories);

        return $this->render('NKOOrderBundle:CRUD:application_journal.html.twig', [
            'journalData' => $journalDataGenerator->generateData(),
            'journalHead' => $journalDataGenerator->getHead(),
            'competitionNames' => $competitionNames
        ]);
    }

    /**
     * @param string $applicationClass
     *
     * @return string
     */
    private function generateUrlForApplicationJournal($applicationClass)
    {
        if (!$applicationClass) {
            return [];
        }

        $competitions =  $this->getDoctrine()->getRepository(Competition::class)
            ->findCurrentCompetitions($applicationClass);

        if (!$competitions) {
            return '';
        }

        $idsArray = array_map(
            function (Competition $competition) {
                return $competition->getId();
            },
            $competitions
        );

        $ids = implode('_', $idsArray);

        return $ids;
    }

    private function generateNameOfCompetitions($competitions)
    {
        $competitionNames = [];
        $startOfLabel = 'ЖУРНАЛ ПРИЕМА ЗАЯВОК ';

        foreach ($competitions as $competition) {
            $competitionNames[] = mb_strtoupper($competition->getName());
        }
        $startOfLabel .= implode(", ", $competitionNames);

        return $startOfLabel;
    }
}
