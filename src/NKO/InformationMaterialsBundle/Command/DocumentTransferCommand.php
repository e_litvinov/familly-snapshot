<?php
namespace NKO\InformationMaterialsBundle\Command;

use Doctrine\ORM\EntityManager;
use NKO\InformationMaterialsBundle\Entity\InformationMaterials;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\DBAL\DriverManager;



/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/2/17
 * Time: 2:06 PM
 */
class DocumentTransferCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('nko:transfer-documents')
            ->setDescription('Transfer documents from document bundle to information materials bundle')
            ->setHelp("Transfer documents to Competition 2016");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(  "Transfer documents");
         /**
         * @var EntityManager $entityManager
         */
        $entityManager= $this->getContainer()->get('doctrine')->getManager();
        $connection = $this->getContainer()->get('database_connection');
        $sql = 'SELECT * FROM old_document';
        $oldDocuments = $connection->query($sql)->fetchAll();
        $documentsCount = count($oldDocuments);

        if( $documentsCount == 0){
            $output->writeln(  "You don't have documents");
        }
        else{
            $output->writeln(  "Get {$documentsCount}  documents");
            $output->writeln( "Every document get 'Курс на семью 2016' category");
            $category = $entityManager->getRepository("NKOInformationMaterialsBundle:InformationMaterialsCategory")
                ->findOneBy(['slug' => 'kurs-na-siem-iu-2016']);

            $informationMaterials  = [];
            $i = 1;
            foreach($oldDocuments as $document){
                $informationMaterial =  new InformationMaterials();
                $informationMaterial->setName($document['title']);
                $informationMaterial->setAnnotation($document['description']);
                $informationMaterial->setText($document['content']);
                $informationMaterial->setTitle($document['meta_title']);
                $informationMaterial->setMetaDescription($document['meta_description']);
                $informationMaterial->setMetaKeywords($document['meta_keywords']);
                $informationMaterial->setIsPublished($document['is_published']);
                $informationMaterial->setCategory($category);
                $informationMaterial->setStatus(1);
                $informationMaterial->setCreatedAt(new \DateTime($document['created_at']));
                $informationMaterial->setUpdatedAt(new \DateTime($document['updated_at']));
                $informationMaterial->setAuthor($document['author']);
                $informationMaterials[] = ($informationMaterial);
                $entityManager->persist($informationMaterial);
                $output->writeln( "Complete {$i} documents");
                $i ++;

            }
            $entityManager->flush($informationMaterials);
            $output->writeln( "Success");
        }
    }

}