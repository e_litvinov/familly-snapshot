<?php

namespace NKO\InformationMaterialsBundle\Form;

use NKO\InformationMaterialsBundle\Entity\PublicationStatusInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PublicationStatusType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'customer_publication' => PublicationStatusInterface::CUSTOMER_PUBLICATION,
                'admin_publication' => PublicationStatusInterface::ADMIN_PUBLICATION,
                'both' => PublicationStatusInterface::ANYWHERE_PUBLICATION
            ],
            'translation_domain' => 'NKOInformationMaterialsBundle'
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}