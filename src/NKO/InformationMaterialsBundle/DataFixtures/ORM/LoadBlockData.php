<?php
namespace NKO\InformationMaterialsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\InformationMaterialsBundle\Entity\CategoryNews;

class LoadBlockData implements FixtureInterface
{
    private function createCategoryNews(ObjectManager &$manager, $name)
    {
        $category = new CategoryNews();
        $category->setName($name);

        $manager->persist($category);
    }


    public function load(ObjectManager $manager)
    {
        $this->createCategoryNews($manager, 'СФ');
        $this->createCategoryNews($manager, 'КНС');
        $this->createCategoryNews($manager, 'другое');
        $manager->flush();
    }

}