<?php

namespace NKO\InformationMaterialsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\InformationMaterialsBundle\Entity\MethodicalBaseCategory;

class LoadMethodicalBaseCategory implements FixtureInterface
{
    private function createBaseCategory(ObjectManager &$manager, $name)
    {
        $baseCategory = new MethodicalBaseCategory();
        $baseCategory->setName($name);

        $manager->persist($baseCategory);
    }


    public function load(ObjectManager $manager)
    {
        $this->createBaseCategory($manager, 'По видам');
        $this->createBaseCategory($manager, 'Обучение');
        $this->createBaseCategory($manager, 'Благополучатели');
        $this->createBaseCategory($manager, 'Результаты');
        $this->createBaseCategory($manager, 'География');
        $manager->flush();
    }

}