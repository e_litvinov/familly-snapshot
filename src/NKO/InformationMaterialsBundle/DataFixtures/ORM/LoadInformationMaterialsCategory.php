<?php

namespace NKO\InformationMaterialsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\InformationMaterialsBundle\Entity\InformationMaterialsCategory;

class LoadInformationMaterialsCategory implements FixtureInterface
{
    private function createInformationMaterialsCategory(ObjectManager &$manager, $name)
    {
        $category = new InformationMaterialsCategory();
        $category->setName($name);

        $manager->persist($category);
    }

    public function load(ObjectManager $manager)
    {
        $this->createInformationMaterialsCategory($manager, 'Семейный фарватер Конкурс 2016');
        $this->createInformationMaterialsCategory($manager, 'Семейный фарватер Конкурс 2017');
        $this->createInformationMaterialsCategory($manager, 'Курс на семью 2016');
        $manager->flush();
    }

}