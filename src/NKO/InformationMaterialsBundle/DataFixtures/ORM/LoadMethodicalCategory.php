<?php

namespace NKO\InformationMaterialsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\InformationMaterialsBundle\Entity\MethodicalCategory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadMethodicalCategory implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function createCategory(ObjectManager &$manager, $name, $baseCategoty)
    {
        $category = new MethodicalCategory();
        $category->setName($name);
        $category->setBaseCategory($baseCategoty);

        $manager->persist($category);
    }


    public function load(ObjectManager $manager)
    {
        $em = $this->container->get('doctrine')->getManager()
            ->getRepository('NKOInformationMaterialsBundle:MethodicalBaseCategory');
        $baseCategory = $em->findOneBy(array('name'=> 'По видам'));

        $this->createCategory($manager, 'инструменты', $baseCategory);
        $this->createCategory($manager, 'исследования', $baseCategory);
        $this->createCategory($manager, 'видео', $baseCategory);
        $this->createCategory($manager, 'книги', $baseCategory);
        $this->createCategory($manager, 'статьи', $baseCategory);
        $this->createCategory($manager, 'кейс', $baseCategory);
        $this->createCategory($manager, 'руководства', $baseCategory);
        $this->createCategory($manager, 'НПБ', $baseCategory);
        $this->createCategory($manager, 'статистика', $baseCategory);

        $baseCategory = $em->findOneBy(array('name'=> 'Обучение'));

        $this->createCategory($manager, 'вебинары', $baseCategory);
        $this->createCategory($manager, 'презентации', $baseCategory);

        $baseCategory = $em->findOneBy(array('name'=> 'Благополучатели'));

        $this->createCategory($manager, 'кризисные кровные семьи', $baseCategory);
        $this->createCategory($manager, 'приемные семьи', $baseCategory);
        $this->createCategory($manager, 'дети 0-3 года', $baseCategory);
        $this->createCategory($manager, 'дети 3-7 лет', $baseCategory);
        $this->createCategory($manager, 'дети 7-11 лет', $baseCategory);
        $this->createCategory($manager, 'подростки (12-17 лет)', $baseCategory);
        $this->createCategory($manager, 'дети с ОВЗ', $baseCategory);
        $this->createCategory($manager, 'выпускники', $baseCategory);
        $this->createCategory($manager, 'специалисты сферы защиты детства', $baseCategory);

        $baseCategory = $em->findOneBy(array('name'=> 'Результаты'));

        $this->createCategory($manager, 'СФУ', $baseCategory);
        $this->createCategory($manager, 'Школы приемного родительства', $baseCategory);
        $this->createCategory($manager, 'Отказы от детей (изъятие)', $baseCategory);
        $this->createCategory($manager, 'Готовность детей к самостоятельной жизни в обществе', $baseCategory);
        $this->createCategory($manager, 'Родительские компетенции', $baseCategory);
        $this->createCategory($manager, 'Детско-родительские отношения', $baseCategory);
        $this->createCategory($manager, 'Психическое состояние детей', $baseCategory);
        $this->createCategory($manager, 'Физическое состояние детей', $baseCategory);
        $this->createCategory($manager, 'Развитие детей', $baseCategory);
        $this->createCategory($manager, 'Поддержка семей со стороны окружения', $baseCategory);

        $baseCategory = $em->findOneBy(array('name'=> 'География'));

        $this->createCategory($manager, 'международный опыт', $baseCategory);
        $this->createCategory($manager, 'российская практика', $baseCategory);
        $this->createCategory($manager, 'название конкретного региона', $baseCategory);

        $manager->flush();
    }


}