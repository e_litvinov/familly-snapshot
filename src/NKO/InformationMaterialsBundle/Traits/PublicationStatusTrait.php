<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 02.02.17
 * Time: 10:25
 */

namespace NKO\InformationMaterialsBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait PublicationStatusTrait
{
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $publicationStatus;

    public function setPublicationStatus($publicationStatus)
    {
        $this->publicationStatus = $publicationStatus;

        return $this;
    }

    public function getPublicationStatus()
    {
        return $this->publicationStatus;
    }
}