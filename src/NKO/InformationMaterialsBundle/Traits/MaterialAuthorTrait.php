<?php
/**
 * Created by PhpStorm.
 * User: oxana
 * Date: 2/3/17
 * Time: 12:21 PM
 */
namespace NKO\InformationMaterialsBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

trait MaterialAuthorTrait
{
    /**
     * @ORM\Column(type="string")
     */
    protected $author;

    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthor()
    {
        return $this->author;
    }

}