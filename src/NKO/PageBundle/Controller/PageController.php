<?php

namespace NKO\PageBundle\Controller;

use NKO\PageBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Page controller.
 *
 * @Route("page")
 */
class PageController extends Controller
{
    /**
     * Finds and displays a page entity.
     *
     * @Route("page/{slug}", name="page_show")
     * @Method("GET")
     */
    public function showAction(Page $page)
    {
        return $this->render('NKOPageBundle:page:show.html.twig', array(
            'page' => $page,
        ));
    }
}
