<?php
namespace NKO\InformationMaterialsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NKO\PageBundle\Entity\Page;

class LoadPageData implements FixtureInterface
{
    private function createPage(ObjectManager &$manager, $title, $text)
    {
        $page = new Page();
        $page->setTitle($title);
        $page->setText($text);

        $manager->persist($page);
    }


    public function load(ObjectManager $manager)
    {
        $this->createPage($manager, 'О фонде', "content");
        $this->createPage($manager, 'Контакты', 'contacts');

        $manager->flush();
    }

}