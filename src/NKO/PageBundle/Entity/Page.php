<?php

namespace NKO\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Page
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="")
 */
class Page
{
    use ORMBehaviors\Sluggable\Sluggable;
    const BASE_URL = '/pagepage/';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;

    protected $url;


    public function getUrl()
    {
        return self::BASE_URL . $this->getSlug();
    }

    public function __toString()
    {
        return (string)$this->title;
    }

    public function getSluggableFields()
    {
        return ['title'];
    }

    public function getRegenerateSlugOnUpdate(){

        return false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Page
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

}
